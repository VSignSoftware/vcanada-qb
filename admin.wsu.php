<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

if( ($_SESSION['username']!="Administrator") && ($_SESSION['username']!="todd") )
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$max = $_POST['max'];
	if(!ctype_digit($max))
	{
		$max=0;
	}
	if(empty($max))
	{
		$max=0;
	}
	$s = $vujade->update_row('misc_config',1,'max_site_users',$max);

	// wipe any pre-existing access data
	$sql = "UPDATE `employees` SET `website_user` = 0";
	$s2=$vujade->generic_query($sql,0,2);

	$s3 = $s+$s2;
	if($s3==2)
	{
		$vujade->messages[]="Changes Saved.";
	}
	else
	{
		$vujade->errors[]="Could not update the number of permitted users. Please contact the system administrator.";
	}

}

if($action==0)
{
	$setup = $vujade->get_setup();
	$max = $setup['max_site_users'];
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Administrator Settings - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=12;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<h3>Maximum number of website users:</h3>
                	<form method = "post" action = "admin.wsu.php">
                	<input type = "hidden" name = "action" value = "1">
                		<table>
                			<tr>
                				<td>
                					<input type = "text" name = "max" value = "<?php print $max; ?>" class = "form-control" style = "width:100px;">
                				</td>
                				<td>&nbsp;
                				</td>
                				<td>
                					<input type = "submit" value = "SAVE" class = "btn btn-primary">
                				</td>
                			</tr>
                		</table>
					</form>

				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
