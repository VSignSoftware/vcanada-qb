<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// setup
$setup = $vujade->get_setup();

if(!isset($_REQUEST['date']))
{
	// start and end of the current month
	$test_date = date('m/d/Y');
	$date = date('m/01/Y');
	$report_t=date('M Y');
}
else
{
	// user selected month and year or fiscal year
	$test_date = $_REQUEST['date'];
	$date = $_REQUEST['date'];
	
	// fiscal year
	if($test_date[0]=="F")
	{
		$year_value = $test_date[3].$test_date[4].$test_date[5].$test_date[6];
		$report_t="Fiscal Year ".$year_value;
		$fiscal_year_search=true;
	}
	
	// year and month
	if(!isset($report_t))
	{
		//$report_t=$date;
		$td = strtotime($test_date);
		$td = date('M Y',$td);
		$report_t=$td;
	}
}

// show as datatable
if($_REQUEST['dt']==1)
{
	$dt=1;
}
else
{
	$dt=0;
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$title = 'Monthly Sales - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Monthly Sales - <?php print $report_t; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "reports.php" class = "btn btn-primary btn-sm" style = "width:100px;">Back</a>
						<select name = "date" id = "date" class = "pull-right form-control" style = "width:200px;">
							<option value = "">-Change Month-</option>
							<?php
							// list of months

							$cy = date("Y");
							$cm = date("m");
							for ($i=0; $i<=12; $i++)
							{
							    
							    if ($cm < 1) { $cm = 12; $cy--; }
							    echo '<option value="'.date('Y-m-d', strtotime($cy."-".$cm."-01")).'">'.date('m-Y', strtotime($cy."-".$cm."-01")).'</option>';
							    $cm--;
							}

							//for ($i=0; $i<=12; $i++) 
							//{ 
							//	print '<option value="'.date('Y-m-d', strtotime("-$i month")).'">'.date('m-Y', strtotime("-$i month")).'</option>';
							//}
							/*
							$cy = date('Y');
							$cm = date('m');

							$xy=1;
							while($xy <= 12)
							{
								//print '<option value="'.date('Y-m-d', strtotime("-$xy month")).'">'.date('m-Y', strtotime("-$xy month")).'</option>';

								$d = $cy.'-'.$cm.'-01';
								print '<option value="'.$d.'">'.date('m-Y', strtotime($d)).'</option>';
								$cm--;
								$xy++;
							}
							*/

							/*
							for ($i=0; $i<=12; $i++) 
							{ 
    							print '<option value="'.date('Y-m-d', strtotime("-$i month")).'">'.date('Y-m', strtotime("-$i month")).'</option>';
							}
							*/
							// fiscal years
							if( (!empty($setup['fiscal_year_start'])) && (!empty($setup['fiscal_year_end'])) ) 
							{
								print '<option value = "">------------</option>';
								$fystartmonth = $vujade->get_month_name($setup['fiscal_year_start']);
								$fyendmonth = $vujade->get_month_name($setup['fiscal_year_end']);

								// past fiscal years
								// start at 2014
								$option_start = 2014;
								$option_current_year = date('Y');
								while($option_start<$option_current_year)
								{
									print '<option value = "FY-'.$option_start.'">Fiscal Year '.$option_start.' ('.$fystartmonth.' - '.$fyendmonth.')</option>';

									$option_start=$option_start+1;
								}

								// current fiscal year
								print '<option value = "FY-'.(date('Y')).'">Fiscal Year '.(date('Y')).' ('.$fystartmonth.' - '.$fyendmonth.')</option>';

								// next fiscal year
								//print '<option value = "FY-'.(date('Y')+1).'">Fiscal Year '.(date('Y')+1).' ('.$fystartmonth.' - '.$fyendmonth.')</option>';
							}
							?>
						</select>
					</div>
				</div>
	        	<div class="panel-body bg-light">
	        		
	        		<?php 
	        		// this needs to be run when the new theme is installed 
	        		//$vujade->update_projects_ts(3);

	        		// sales people
	        		//$names = array('Fidel Zelaya','John Ray','Aaron Clippinger','Joe Hoffman','Curt Bauer');
	        		//$salespeople = $vujade->get_salespeople(2,$names);
	        		//$salespeople = $vujade->get_salespeople(2);
	        		if(!$fiscal_year_search)
	        		{
	        			// first day of the month
						$first = date('Y-m-01', strtotime($test_date));

						// last day of the month
						$last = date('Y-m-t', strtotime($test_date));

		        		$start = strtotime($first);
		        		$end = strtotime($last);
	        		}
	        		else
	        		{
	        			// get by fiscal year

						// fiscal year does not start on Jan. 1
						if($setup['fiscal_year_start']!=1)
						{	
							// any year in the past 
							if($year_value<$option_current_year)
							{
								// start
								//$y=$year_value;
								$y = $year_value-1;

								// end
								//$y2=$year_value+1;
								$y2=$year_value;

							}

							// current year
							if($year_value==$option_current_year)
							{
								// start
								$y=$option_current_year-1;

								// end
								$y2=$year_value;
							}

							// next year
							if($year_value>$option_current_year)
							{
								// start
								$y=$option_current_year;

								// end
								$y2=$year_value;
							}
	
							$m=$setup['fiscal_year_start'];
							$m2=$setup['fiscal_year_end'];
							$start = strtotime($y.'-'.$m.'-01');
							$date1 = $y2.'-'.$m2; 
							$d = date_create_from_format('Y-m',$date1); 
							$last_day = date_format($d, 't');
							$end = strtotime($y2.'-'.$m2.'-'.$last_day);
						}
						else
						{
							$y=$year_value;
							$m=$setup['fiscal_year_start'];
							$m2=$setup['fiscal_year_end'];
							$start = strtotime($y.'-'.$m.'-01');
							$date1 = $y.'-'.$m2; 
							$d = date_create_from_format('Y-m',$date1); 
							$last_day = date_format($d, 't');
							$end = strtotime($y.'-'.$m2.'-'.$last_day);

							//print $start.'<br>'.$end;
							//die;

						}
	        		}

	        		$projects = $vujade->get_projects_for_monthly_sales_report($start,$end);
					if($projects['error']=="0")
					{
						unset($projects['error']);

						// determine which sales people to use
						$salespeople = array();
						foreach($projects as $project)
						{
							$salespeople[]=$project['salesperson'];
						}	
						$salespeople=array_unique($salespeople);		
						$salespeople['error']=0;			
						?>
			        		<!-- data table -->
			        		<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
									<thead>
										<tr style = "border-bottom:1px solid black;">
											<td valign = "top"><strong>Open Date</strong></td>
											<td valign = "top"><strong>Job #</strong></td>
											<td valign = "top"><strong>Type</strong></td>
											<td valign = "top"><strong>Job Name</strong></td>

											<!-- salesmen -->
											<?php
											//$spsarray = array();
											if($salespeople['error']=="0")
											{
												unset($salespeople['error']);
												foreach($salespeople as $sp)
												{
													//$spsarray[$k]=$sp['fullname'];
													?>
													<td valign = "top"><strong><?php print $sp; ?></strong></td>
													<?php
												}
											}
											else
											{
												print '<td valign = "top"><strong><font color = "red">No sales people</font></strong></td>';
											}
											?>
										</tr>
									</thead>

								    <tbody style = "font-size:14px;">
									<?php
									foreach($projects as $i)
									{

										$line_total=$i['line_total'];

										$link = 'project.php?id='.$i['project_id'];

								        print '<tr class = "clickableRow-row">';

								        // date
								        print '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><a href = "'.$link.'">'.$i['open_date'].'</a></td>';

								        // job number
								        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = ""><a href = "'.$link.'">'.$i['project_id'].'</a></td>';

								        // type
								        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = ""><a href = "'.$link.'">'.$i['type'].'</a></td>';

								        // job name
								        $job_name=$vujade->trim_string($i['site'],30);
								        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = ""><a href = "'.$link.'">'.$job_name.'</a></td>';

								        // salespeople
								        if(count($salespeople)>0)
										{
											foreach($salespeople as $sp)
											{
												print '<td class="clickableRow line-total" id = '.$sp.' href="'.$link.'" valign = "top" style = "">';
												if($i['salesperson']==$sp)
												{
													$spsarray[]=$sp.'^'.$line_total;
													print '$'.$line_total;
												}
								        		print '</td>';
											}
										}
										else
										{
											print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
											//print $line_total;
								        	print '</td>';
										}
									}
									?>
									</tbody>
							</table>

							<table class = "table table-bordered">
								<tr>
									<td colspan="5" width = "30%">
									&nbsp;
									</td>
							<?php
							$total = 0;
							if(count($salespeople)>0)
							{
								foreach($salespeople as $sp)
								{
									$n = $sp;
									if(!empty($n))
									{
										print '<td><div style = "float:right;">';
										print '<a href = "report_individual_sales.php?sp='.$n.'">';
										print $n.': $';
						        		$sp_total = 0;
						        		foreach($spsarray as $sps)
						        		{
						        			$line = explode('^',$sps);
						        			if($line[0]==$n)
						        			{
						        				$line[1]=str_replace(',','',$line[1]);
						        				$sp_total+=$line[1];
						        			}
						        		}
						        		print number_format($sp_total,2);
						        		print '</a>';
						        		$total+=$sp_total;
						        		print '</div></td>';
						        	}
								}
							}
							print '</tr>';
							$cols = 5+count($salespeople);
							print '<tr><td colspan ="'.$cols.'"><div style = "float:right;">Total: $'.number_format($total,2).'</td></tr>';
							?>
							</table>
							<!-- -->
							<center>
								<?php
								if($dt==0)
								{
									?>
									<a href = "report_monthly_sales.php?date=<?php print $date; ?>&dt=1" class = "btn btn-primary">Enable Sorting</a>
								<?php
								}
								if($dt==1)
								{
									?>
									<a href = "report_monthly_sales.php?date=<?php print $date; ?>&dt=0" class = "btn btn-primary">Disable Sorting</a>
									<?php
								}
								?>
								<a target="_blank" href = "print_report_monthly_sales.php?date=<?php print $date; ?>" class = "btn btn-primary">Print</a>
							</center>
							
						<?php
					}
					else
					{
						$vujade->set_error('No projects found. ');
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- sum plugin 
<script src="https://cdn.datatables.net/plug-ins/1.10.10/api/sum().js"></script>
-->

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // date was changed
    $('#date').change(function()
    {
    	var date = $(this).val();
    	if(date!='')
    	{
    		var href = "report_monthly_sales.php?date="+date;
    		window.location.href=href;
    	}
    });

    <?php
    if($dt==1)
    {
    ?>
    // Init DataTables
    $('#datatable').dataTable({

    	//"bsort": false
    	
      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 3000,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      },
      "order": [[ 0, "desc" ]]
      
    });
    <?php } ?>

    $('.clickableRow').click(function(e) 
	{
		e.preventDefault();
		var href = $(this).attr('href');
		window.location.href=href;
	});

    /*
	$('.line-total').each(function()
	{
		var sname = this.id;
		var amount = $(this).text();
	});
	*/

});
</script>

</body>
</html>