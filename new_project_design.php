<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$project_id = $_REQUEST['project_id'];
$id = $project_id;

# permissions
$designs_permissions = $vujade->get_permission($_SESSION['user_id'],'Designs');
if($designs_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($designs_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$designs = $vujade->get_designs_for_project($project_id);
if($designs['error']!='0')
{
	$nextid=$project_id."-01";
}
else
{
	$nextid=$project_id."-".$designs['next_id'];
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# new design
if($action==1)
{
	$project_id=$_POST['project_id'];
	//$date=$_POST['date'];
	$date=date('m/d/Y');
	$designer=$_POST['designer'];
	$required_date=$_POST['required_date'];
	$status=$_POST['status'];
	$priority=$_POST['priority'];
	$company=$_POST['company'];
	$contact=$_POST['contact'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	$comments=$_POST['comments'];
	$description=$_POST['description'];
	$designer=$_POST['designer'];
	$t=$_POST['type'];
	$type_1=$t[0];
	$type_2=$t[1];
	$type_3=$t[2];
	$type_4=$t[3];
	$f=$_POST['format'];
	$format_1=$f[0];
	$format_2=$f[1];
	$format_3=$f[3]; 
	$format_4=$f[4];
	$format_5=$f[2];
	# create new design
	$vujade->create_row('designs');
	$row_id = $vujade->row_id;
	$s=array();
	$s[]=$vujade->update_row('designs',$row_id,'design_id',$nextid);
	$s[]=$vujade->update_row('designs',$row_id,'project_id',$project_id);
	$s[]=$vujade->update_row('designs',$row_id,'request_date',$date);
	$s[]=$vujade->update_row('designs',$row_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('designs',$row_id,'request_required_date',$required_date);
	$s[]=$vujade->update_row('designs',$row_id,'request_description',$description);
	$s[]=$vujade->update_row('designs',$row_id,'priority',$priority);
	$s[]=$vujade->update_row('designs',$row_id,'type',$type_1.' '.$type_2.' '.$type_3.' '.$type_4);
	$s[]=$vujade->update_row('designs',$row_id,'type_sub',$format_1.' '.$format_2.' '.$format_3.' '.$format_4.' '.$format_5);
	$s[]=$vujade->update_row('designs',$row_id,'status',$status);
	$s[]=$vujade->update_row('designs',$row_id,'employee_id',$designer);	
	$s[]=$vujade->update_row('designs',$row_id,'designer_company',$company);
	$s[]=$vujade->update_row('designs',$row_id,'designer_contact',$contact);
	$s[]=$vujade->update_row('designs',$row_id,'designer_phone',$phone);	
	$s[]=$vujade->update_row('designs',$row_id,'designer_email',$email);	
	$s[]=$vujade->update_row('designs',$row_id,'designer_comments',$comments);

	$vujade->page_redirect('project_designs.php?id='.$project_id.'&designid='.$row_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=2;

$title = "New Design - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            $vujade->show_messages();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">
                  New Design #<?php print $nextid; ?>
                  <form method="post" action="new_project_design.php" id="form">

                    <input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

                    <div style = "padding:10px;height:470px;width:900px;">
		<table width = "100%">
			<tr>
				<td>Date:</td>
				<td><?php print date('m/d/Y'); ?></td>
				<td>
					Assign to: 
				</td>
				<td>
					<select name = "designer">
						<option value = "">-Select-</option>
						<?php
						$designers = $vujade->get_designers();
						if($designers['error']=="0")
						{
							unset($designers['error']);
							foreach($designers as $designer)
							{
								print '<option value = "'.$designer['employee_id'].'">'.$designer['fullname'].'</option>';
							}
						}
						?>
					</select>
				</td>
			</tr>
			<tr>
				<td>Required: </td>
				<td><input type = "text" name = "required_date" id = "required_date" class = "dp" value = "ASAP"></td>
				<td>
					Status: 
				</td>
				<td>
					<select name = "status">
						<option value = "">-Select-</option>
						<option value = "Submitted">Submitted</option>
						<option value = "Complete">Complete</option>
						<option value = "In Progress">In Progress</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>Priority: </td>
				<td>
					<select name = "priority">
						<option value = "">-Select-</option>
						<option value = "Urgent">Urgent</option>
						<option value = "Standard" selected = "selected">Standard</option>
						<option value = "Low">Low</option>
					</select>
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>Type: </td>
				<td>
					<input type = "checkbox" name = "type[]" value = "Presentation" style = "margin-right:5px;">Presentation <input type = "checkbox" name = "type[]" value = "Const. Dwgs." style = "margin-left:30px;margin-right:5px;">Const. Dwgs. <input type = "checkbox" name = "type[]" value = "Production" style = "margin-left:30px;margin-right:5px;">Production <input type = "checkbox" name = "type[]" value = "Permits Required" style = "margin-left:30px;margin-right:5px;">Permits Required
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td>Format: </td>
				<td>
					<input type = "checkbox" name = "format[]" value = "11x17" style = "margin-left:0px;margin-right:5px;">11x17 <input type = "checkbox" name = "format[]" value = "8.5x11" style = "margin-left:30px;margin-right:5px;">8.5x11 
					<input type = "checkbox" name = "format[]" value = "8.5x14" style = "margin-left:30px;margin-right:5px;">8.5x14 
					<input type = "checkbox" name = "format[]" value = "photocomp" style = "margin-left:30px;margin-right:5px;">photocomp <input type = "checkbox" name = "format[]" value = "thumbnail" style = "margin-left:30px;margin-right:5px;">thumbnail
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan = "4">
					&nbsp;
				</td>
			</tr>
			<tr>
				<td colspan = "4">
					Architect/Designer
				</td>
			</tr>
		</table>

		<div style = "float:left;width:370px;height:125px;">
			<table>
				<tr>
					<td>Company:
					</td>
					<td><input type = "text" name = "company" style = "width:250px;">
					</td>
				</tr>
				<tr>
					<td>Contact: 
					</td>
					<td><input type = "text" name = "contact" style = "width:250px;">
					</td>
				</tr>
				<tr>
					<td>Phone:
					</td>
					<td><input type = "text" name = "phone" style = "width:250px;">
					</td>
				</tr>
				<tr>
					<td>Email:
					</td>
					<td><input type = "text" name = "email" style = "width:250px;">
					</td>
				</tr>
			</table>
		</div>

		<div style = "float:right;width:500px;">
			<div style = "float:left;">
				Comments: 
			</div>
			<textarea name = "comments" style = "float:right;width:400px;height:90px;"></textarea>
		</div>

		<br style = "clear:both;">
		
			<table width = "100%">
				<tr>
					<td>Description:
					</td>
					<td>
						<a href "#" id = "pastescope" class = "button200" style = "float:right;text-align:center;">Insert Project Description</a>
					</td>
				</tr>
				<tr>
					<td colspan = "2">
						
						<textarea name = "description" id = "description"></textarea>

						
							<!-- ckeditor -->
							<script src="ckeditor/ckeditor.js"></script>
							<script>
						    CKEDITOR.replace('description');
							CKEDITOR.config.toolbar = [
								{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
								{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
								'/',
								{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
							];
							CKEDITOR.config.fontSize_sizes = '7/7pt;8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;';
						    CKEDITOR.config.removePlugins = 'elementspath';
						    CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
						    CKEDITOR.config.tabSpaces = 5;
						    CKEDITOR.config.height = 400;
						    CKEDITOR.config.disableNativeSpellChecker = false;
						    </script>

					</td>
				</tr>
			</table>

		<div>
			<div style = "float:right;">
				<input type = "submit" class = "sbt200" value = "DONE" id = "done"> 
				</form>
			</div>
		</div>


                    <div class="row">

                      <div class="col-md-12">
                      
                      		<textarea name = "description" id = "description">
							</textarea>
							
							<!-- ckeditor -->
							<script src="vendor/plugins/ckeditor/ckeditor.js"></script>
							<script>
					        CKEDITOR.replace('description');
							CKEDITOR.config.toolbar = [
								{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
								{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
								'/',
								{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
								{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
							];
							CKEDITOR.config.fontSize_sizes = '7/7pt;8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;';
					        CKEDITOR.config.removePlugins = 'elementspath';
					        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
					        CKEDITOR.config.tabSpaces = 5;
					        CKEDITOR.config.height = 550;
					        CKEDITOR.config.disableNativeSpellChecker = false;
					        </script>

                      </div>
                    </div>

                    <input type = "hidden" name = "id" value = "<?php print $id; ?>">
					<input type = "hidden" name = "action" id = "action" value = "1">

                    <div style = "margin-top:15px;">
                      <div style = "float:left">
                        <a href = "#" id = "back" class = "btn btn-lg btn-primary" style = "width:100px;">CANCEL</a>
                      </div>
                      <div style = "float:right;">
                        <a href = "#" id = "done" class = "btn btn-lg btn-primary" style = "width:100px;">DONE</a>
                      </div>  
                    </div>

                  </form>
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    $('#back').click(function(e)
	{
		e.preventDefault();
		$('#action').val(2);
		$('#form').submit();
	});
	$('#done').click(function(e)
	{
		e.preventDefault();
		$('#action').val(1);
		$('#form').submit();
	});

	// fix the indent on the text area
    var d = $('#description').html();
    $("#description").html($.trim(d));
 
});
</script>

</body>

</html>