<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$setup = $vujade->get_setup();
$state_sales_tax=$setup['state_sales_tax'];
$invoice_setup = $vujade->get_invoice_setup();

$s = array();

// shop order
$so = $vujade->get_shop_order($id,$idcol="project_id");

// accepted proposal
$proposal = $vujade->get_proposal($so['proposal'],'proposal_id');

if($proposal['error']!='0')
{
	die('Proposal must be selected on shop order page.');
}

# create a new invoice
if(!isset($_REQUEST['invoice_id']))
{
	# determine what the invoice number is
	$invoices = $vujade->get_invoices_for_project($project_id);
	if($invoices['error']!='0')
	{
		$next_id=$project_id."-1";
	}
	else
	{
		$next_id=$project_id."-".$invoices['next_id'];
	}
	$vujade->create_row('quickbooks_invoice');
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'project_id',$project_id);
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'RefNumber',$next_id);
	
	# get correct customer info for this project
	$listiderror=0;
	$localiderror=0;
	// try to get by list id
    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
    if($customer1['error']!="0")
    {
    	$listiderror++;
    	unset($customer1);
    }
    else
    {
    	$customer=$customer1;
    }
    // try to get by local id
    $customer2 = $vujade->get_customer($project['client_id'],'ID');
    if($customer2['error']!="0")
    {
    	$localiderror++;
    	unset($customer2);
    }
    else
    {
    	$customer=$customer2;
    }

    $iderror=$listiderror+$localiderror;

    if($iderror<2)
    {
    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
    }
    else
    {
    	// can't find customer or no customer on file
    }

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'CustomerRef_ListID',$customer['database_id']);

	// get and set the sales tax code 
	$tax_data = $vujade->QB_get_sales_tax($project['city']);
	if($tax_data['error']=="0")
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data['ListID']);
	}
	else
	{
		$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data2['ListID']);
	}

	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'TxnID',$fakeid);

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_Addr1',$project['site']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_Addr2',$project['address_1']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_City',$project['city']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_State',$project['state']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_PostalCode',$project['zip']);

	// advanced deposit
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'advanced_deposit',1);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'advanced_deposit_rate',$setup['advanced_deposit']);

	$invoice_id = $next_id;
	$invoice_db_id = $vujade->row_id;
}
else
{
	$invoice_id = $_REQUEST['invoice_id'];
	$invoice_db_id = $_REQUEST['invoice_db_id'];
}

// page has been posted for processing
if(isset($_REQUEST['action']))
{

	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	$memo = $_POST['description'];
	$deposit=str_replace(',', '', $_POST['deposit']);
	$deposit=str_replace('-', '', $deposit);
	//$deposit=$deposit*$setup['advanced_deposit'];
	//$deposit='-'.$deposit;
	$po_id = $_POST['po_id'];
	$invoice_id=$_POST['invoice_id'];
	$invoice_db_id = $_REQUEST['invoice_db_id'];

	$invoice=$vujade->get_invoice($invoice_db_id);
	$dbid = $invoice_db_id;

	// update the parent record
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'TxnDate',$qbdate);
			$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ts',strtotime($date));
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_description',$memo);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'po_number',$po_id);

	// create line item deposit
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Deposit','$deposit',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// send the invoice data to quickbooks
	// qb config
	require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
	$dsn = $vujade->qbdsn;

	// create item on qb server
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('America/Los_Angeles');
	}
	
	if (!QuickBooks_Utilities::initialized($dsn))
	{	
		// Initialize creates the neccessary database schema for queueing up requests and logging
		QuickBooks_Utilities::initialize($dsn);
		// This creates a username and password which is used by the Web Connector to authenticate
		QuickBooks_Utilities::createUser($dsn, $user, $pass);
	}

	// insert into the quickbooks vendor table
	$Queue = new QuickBooks_WebConnector_Queue($dsn);
	$Queue->enqueue(QUICKBOOKS_ADD_INVOICE, $invoice_db_id);

	$vujade->page_redirect('project_invoices.php?id='.$id.'&invoiceid='.$invoice_db_id);
}
else
{
	// page has not yet been posted for processing

	// set all to zero/null except where noted
	//$deposit=$proposal['amounts']-$proposal['tax_total'];
	$deposit = ($so['selling_price']+$so['tax_amount'])*$setup['advanced_deposit'];
	$date='';
	$memo="";
	$total_due=$deposit;
	$total_payments=0;
	$balance_due=$deposit;
	$po_id = $so['po_number'];
}

$section=3;
$menu=17;
$cmenu=1;
$title = 'New Advanced Deposit Invoice - '.$project['project_id'] . ' - ';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		<form method = "post" action = "new_ad_invoice.php" id = "np">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				<strong>Number: <?php print $invoice_id; ?> (Proposal: 
				<?php print $shop_order['proposal']; ?>)
				</strong>
			</div>
			<div class = "col-md-8 pull-right">
				<strong>Date: <input type = "text" name = "date" value = "<?php print $date; ?>" class = "dp" style = "width:150px;margin-rigth:10px;"></strong> 
				<strong>PO #: <input type = "text" name = "po_id" id = "po_id" value = "<?php print $po_id; ?>" class = "" style = "width:150px;"></strong> 
			</div>
		</div>

		<div class = "row">
			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Billing Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
					# get correct customer info for this project
					$listiderror=0;
					$localiderror=0;
					// try to get by list id
				    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
				    if($customer1['error']!="0")
				    {
				    	$listiderror++;
				    	unset($customer1);
				    }
				    else
				    {
				    	$customer=$customer1;
				    }
				    // try to get by local id
				    $customer2 = $vujade->get_customer($project['client_id'],'ID');
				    if($customer2['error']!="0")
				    {
				    	$localiderror++;
				    	unset($customer2);
				    }
				    else
				    {
				    	$customer=$customer2;
				    }

				    $iderror=$listiderror+$localiderror;

				    if($iderror<2)
				    {
				    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
				    }
				    else
				    {
				    	// can't find customer or no customer on file

				    }
		            if($customer['error']=="0")
		            {
		            	print $customer['name'].'<br>';
		            	print $customer['address_1'].'<br>';
		            	if(!empty($customer['address_2']))
		            	{
		            		print $customer['address_2'].'<br>';
		            	}
		            	print $customer['city'].', '.$customer['state'].' '.$customer['zip'].'<br>';
		            }
					?>
				  </div>
				</div>
			</div>

			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Job Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
		            # get site location for this project
					print $project['site'].'<br>';
					print $project['address_1'].'<br>';
					print $project['city'].', ';
					print $project['state'].' ';
					print $project['zip'].'<br>';;
					// county
					$county=$vujade->get_county($project['city']);
					if($county['error']=="0")
					{
						print $county['county'];
					}
					?>
				  </div>
				</div>
			</div>
		</div>
	
		<input type = "hidden" id = "action" value = "1" name = "action">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

		<div class = "row">
			<div class = "col-md-12">
			<strong>Description:</strong>
			<br>
			
			<center>
			<textarea id = "description" name = "description">
			<?php
			print $project['description'];
			?>
			</textarea>
			
			<!-- ckeditor -->
			<script src="vendor/plugins/ckeditor/ckeditor.js"></script>
			<script>
	        CKEDITOR.replace('description');
			CKEDITOR.config.toolbar = [
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
				'/',
				{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
				{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
			];
			CKEDITOR.config.fontSize_sizes = '7/7pt;8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;';
	        CKEDITOR.config.removePlugins = 'elementspath';
	        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
	        CKEDITOR.config.tabSpaces = 5;
	        CKEDITOR.config.height = 550;
	        CKEDITOR.config.disableNativeSpellChecker = false;
	        </script>
			</div>
		</div>

		<p style = "height:15px;">&nbsp;</p>

		<!-- bottom block -->
		<div style = "clear:both;width:100%;margin-top:15px;margin-left:10px;margin-right:10px;">

			<!-- left block: input boxes -->
			<div class = "row">

				<div style = "clear:both;width:100%;margin-top:15px;margin-bottom:5px;margin-left:10px;">

					Deposit Amount: $<input type = "text" name = "deposit" id = "deposit" value = "<?php print @number_format($deposit,2); ?>"> 

					<input type = "hidden" name = "invoice_id" id = "invoice_id" value = "<?php print $invoice_id; ?>">

					<input type = "hidden" name = "invoice_db_id" id = "invoice_db_id" value = "<?php print $invoice_db_id; ?>">

					<br>

					<input type = "submit" value = "SAVE AND CLOSE" id = "done" class = "btn btn-primary" style = "">
					<br>
					

					</form>

				</div>
			</div>

			</div>	
			</div>

		</div>

    </div>
</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	$('.dp').datepicker();

    var d = $('#description').html();
    $("#description").html($.trim(d));

});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
