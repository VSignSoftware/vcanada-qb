<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

# active tab
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
	if(!in_array($tab, array(0,1,2)))
	{
		$tab=0;
	}
}
else
{
	$tab=0;
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# delete a photo
if($action==1)
{
	if($projects_permissions['delete']==1)
	{
		$vujade->site_photos_delete($_REQUEST['photo_id'],$id);
	}
}
if($action==2)
{
	$photo_id = $_REQUEST['photo_id'];
	$category = $_REQUEST['category'];
	$s[]=$vujade->update_row('site_photos',$photo_id,'category',$category);
	if($category==1)
	{
		$tab=0;
	}
	if($category==2)
	{
		$tab=1;
	}
	if($category==3)
	{
		$tab=2;
	}
}

$shop_order = $vujade->get_shop_order($id, 'project_id');

$site_photos = $vujade->get_site_photos($id,0,1);
$survey_photos = $vujade->get_site_photos($id,1);
$manufacturing_photos = $vujade->get_site_photos($id,2);
$finished_photos = $vujade->get_site_photos($id,3);

$show=0;     
if($survey_photos['error'] == "0") 
{
	$show++;
}
if($survey_photos['error'] == "0") 
{
	$manufacturing++;
}
if($finished_photos['error'] == "0") 
{
	$show++;
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 11;
$nav = 3;
$title = "Site Photos - ";
require_once('tray_header.php'); 
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper"><!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn"><!-- begin: .tray-left -->
<?php require_once('project_left_tray.php'); ?><!-- end: .tray-left --><!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;">
<div class="pl15 pr15" style = "width:100%;">
<?php require_once('project_right_tray.php'); ?>
<div id="" style = "">
<!-- middle of page -->
<div class="panel panel-primary panel-border top">

<!--heading-->
<div class="panel-heading">
<span class="panel-title">Site Photos</span>
<div class="widget-menu pull-right">

<?php
if($projects_permissions['create']==1)
{
	?>
	<a class="btn-sm btn-primary" href="new_site_photo.php?project_id=<?php print $id; ?>">Add Photos</a> 
<?php
}
// send photos by email button
// this button is only visible if logged in user has a valid
// email reply to address on file
// additional permissions?
if( (!empty($emp['email_reply_to'])) && ($show>0) )
{
	print '<a id = "email" class = "btn-sm btn-primary" href = "#email-photo-modal">Email</a> ';
}			
if($show>0)
{
	print ' <a href = "download_photo.php?project_id='.$id.'&zip=1" class = "btn-sm btn-primary">Export All</a> ';
}
?>
</div>
</div>
<!--close heading-->

<!--body-->
<div class="panel-body">

<!-- Nav tabs -->
<ul class="nav nav-tabs" role="tablist">
<li role="presentation" class="<?php if($tab==0){ print 'active'; } ?>"><a href="#Survey" aria-controls="Survey" role="tab" data-toggle="tab">Survey Photos</a></li>
<li role="presentation" class="<?php if($tab==1){ print 'active'; } ?>"><a href="#Manufacturing" aria-controls="Manufacturing" role="tab" data-toggle="tab">Manufacturing Photos</a></li>
<li role="presentation" class="<?php if($tab==2){ print 'active'; } ?>"><a href="#Completion" aria-controls="Completion" role="tab" data-toggle="tab">Completion Photos</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
	<div role="tabpanel" class="tab-pane <?php if($tab==0){ print 'active'; } ?>" id="Survey">
		<div id="tabs-1" style = "">
			<div class="panel-body">
				<div style = "">
					<table class = "table">
					<tbody>
					<tr style="font-weight:bold;">
						<td width="">Thumbnail</td>
						<td width="">Filename</td>
						<td width="">Keywords</td>
						<td width="">Category</td>
						<td width="">Date/Time</td>
						<td width="">
							&nbsp;
						</td>
						<td width="">&nbsp;</td>
					</tr>
					<?php 	                
					if($survey_photos['error'] == 0)
					{
						unset($survey_photos['error']);

						foreach($survey_photos as $item)
						{	                        
							$keywords = '';
						    if(!empty($item['keywords']))
						    {
						        foreach($item['keywords'] as $key)
						        {
						            $keywords .= $key['keyword'] . ', ';
						        }
						    }
						    print '<tr>';

					// determine if file is image or other
					$img = 0;
					$f = "uploads/images/".$id."/".$item['filename'];
					$fdata = @pathinfo($f);
					$imgtypes = array('jpg','jpeg','png','JPG','JPEG');
					if(in_array($fdata['extension'], $imgtypes))
					{
						$img=1;
					}
					if($fdata['extension']=='pdf')
					{
						$img=2;
					}
					if($fdata['extension']=='PDF')
					{
						$img=2;
					}
					if($fdata['extension']=='gif')
					{
						$img=3;
					}
					if($fdata['extension']=='GIF')
					{
						$img=3;
					}

					# image
					if($img == 1)
					{

						//$thumb='thumbnail.php?file=uploads/images/'.$id.'/'.$item['filename'].'&width=80&height=80';
						$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';
						print '<td style="" valign = "top"><a href="uploads/images/'.$id.'/'.$item['filename'].'" title="'.$item['filename'].'" class="fancybox" rel="group"><img src="'.$thumb.'" border="0"></a><br>';
						//print_r($exif).'<br>';
					}

					# pdf
					if($img == 2)
					{
						print '<td style="" valign = "top"><a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'"><img src="images/pdf.png" border="0" width = "80" height = "80"></a><br>';
					}

					// gif
					if($img == 3)
					{
						//$thumb='thumbnail.php?file=uploads/images/'.$id.'/'.$item['filename'].'&width=80&height=80';
						$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';
						print '<td style="" valign = "top"><a href="uploads/images/'.$id.'/'.$item['filename'].'" title="'.$item['filename'].'" class="fancybox" rel="group"><img src="'.$thumb.'" border="0"></a><br>';
					}

					# something else
					if($img == 0)
					{
						print '<td style="" valign = "top"><a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'"><img src="images/nopreview.png" border="0" width = "80" height = "80"></a><br>';
					}

					// download button
					print '<a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'" class = "plus-photo btn btn-primary btn-xs">Export</a>';

					print '</td>

					<td style="" valign = "top">'.$item['display_name'].'</td>

					<td valign = "top">'.rtrim($keywords, ', ');

						if($projects_permissions['edit']==1)
						{
							print '<a href = "edit_photo_keywords.php?project_id='.$id.'&photo_id='.$item['photo_id'].'" class = "plus btn btn-primary btn-xs">Edit</a></td><td valign = "top">';

							print '<select id = "'.$item['photo_id'].'" class = "category" style = "">';
							print '<option value = "'.$item['category'].'">';
								if($item['category']==1)
								{
									print 'Survey';
								}
								if($item['category']==2)
								{
									print 'Manuf.';
								}
								if($item['category']==3)
								{
									print 'Completion';
								}
								print '</option>';
								print '<option value = "" disabled="disabled">----</option>';
								print '<option value = "1">Survey</option>';
								print '<option value = "2">Manuf.</option>';
								print '<option value = "3">Completion</option>';
								print '</select></td>';
						}
						else
						{
							print '</td><td valign = "top"></td>';
						}
					$ts = strtotime($item['ts']);
					$ts = date('m/d/Y',$ts);
					print '<td valign = "top">'.$ts.'</td>

					<td style="text-align:left;" valign = "top">';
						if($projects_permissions['delete']==1)
						{
							print '	<a class = "btn btn-xs btn-danger delete-photo   " href = "#delete-photo-form" title = "Delete photo" data-href="project_site_photos.php?id='.$id.'&action=1&photo_id='.$item['photo_id'].'" >-</a>';
						}
						print '</td>
					<td valign = "top">&nbsp;</td>
					</tr>';
					}
					}
				
					?>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<!-- tab 2 -->
	<div role="tabpanel" class="tab-pane <?php if($tab==1){ print 'active'; } ?>" id="Manufacturing">
		<div id="tabs-2" style = "">
			<table class = "table">
				<tbody>
				<tr style="font-weight:bold;">
					<td>Thumbnail</td>
					<td>Filename</td>
					<td>Keywords</td>
					<td>Category</td>
					<td>Date/Time</td>
					<td>
						&nbsp;
					</td>
					<td>&nbsp;</td>
				</tr>
				<?php
if($manufacturing_photos['error'] == 0)
{
	unset($manufacturing_photos['error']);

	foreach($manufacturing_photos as $item)
	{
	    $keywords = '';
	    if(!empty($item['keywords']))
	    {
	        foreach($item['keywords'] as $key)
	        {
	            $keywords .= $key['keyword'] . ', ';
	        }
	    }
	    print '<tr>';

		// determine if file is image or other
		$img = 0;
		$f = "uploads/images/".$id."/".$item['filename'];
		$fdata = @pathinfo($f);
		$imgtypes = array('jpg','jpeg','png','JPG','JPEG');
		if(in_array($fdata['extension'], $imgtypes))
		{
		$img=1;
		}
		if($fdata['extension']=='pdf')
		{
		$img=2;
		}
		if($fdata['extension']=='PDF')
		{
		$img=2;
		}
		if($fdata['extension']=='gif')
		{
		$img=3;
		}
		if($fdata['extension']=='GIF')
		{
		$img=3;
		}

		# image
		if($img == 1)
		{                                
			//$thumb='thumbnail.php?file=uploads/images/'.$id.'/'.$item['filename'].'&width=80&height=80';

			$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';

			print '<td style=";" valign = "top"><a href="uploads/images/'.$id.'/'.$item['filename'].'" title="'.$item['filename'].'" class="fancybox" rel="group"><img src="'.$thumb.'" border="0"></a><br>';
			//print_r($exif).'<br>';
		}

		# pdf
		if($img == 2)
		{
			print '<td style="" valign = "top"><a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'"><img src="images/pdf.png" width = "80" height = "80" border="0"></a><br>';
		}

		// gif
		if($img == 3)
		{
			//$thumb='thumbnail.php?file=uploads/images/'.$id.'/'.$item['filename'].'&width=80&height=80';

			$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';

			print '<td style="" valign = "top"><a href="uploads/images/'.$id.'/'.$item['filename'].'" title="'.$item['filename'].'" class="fancybox" rel="group"><img src="'.$thumb.'" border="0"></a><br>';
		}

		# something else
		if($img == 0)
		{
			print '<td style="" valign = "top"><a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'"><img src="images/nopreview.png" width = "80" height = "80" border="0"></a><br>';
		}

		// download button
		print '<a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'" class = "plus-photo btn btn-primary btn-xs">Export</a>';

		print '</td><td style="" valign = "top">'.$item['display_name'].'</td><td valign = "top">'.rtrim($keywords, ', ');

		if($projects_permissions['edit']==1)
		{
			print '<br><a href = "edit_photo_keywords.php?project_id='.$id.'&photo_id='.$item['photo_id'].'" class = "plus btn btn-primary btn-xs">Edit</a></td>

			<td valign = "top">';

			print '<select id = "'.$item['photo_id'].'" class = "category" style = "">';
			print '<option value = "'.$item['category'].'">';
			if($item['category']==1)
			{
				print 'Survey';
			}
			if($item['category']==2)
			{
				print 'Manuf.';
			}
			if($item['category']==3)
			{
				print 'Completion';
			}
			print '</option>';
			print '<option value = "" disabled="disabled">----</option>';
			print '<option value = "1">Survey</option>';
			print '<option value = "2">Manuf.</option>';
			print '<option value = "3">Completion</option>';
			print '</select></td>';
		}
		else
		{
			print '</td><td valign = "top" width=""></td>';
		}
		$ts = strtotime($item['ts']);
		$ts = date('m/d/Y',$ts);
		print '<td valign = "top">'.$ts.'</td><td style="text-align:right;" valign = "top">';

		// delete button
		if($projects_permissions['delete']==1)
		{
			//print '<a href="project_site_photos.php?id='.$id.'&action=1&photo_id='.$item['photo_id'].'" class = "plus-delete btn-danger btn-xs">-</a>';

			print '	<a class = "btn btn-xs btn-danger delete-photo   " href = "#delete-photo-form" title = "Delete photo" data-href="project_site_photos.php?tab=1&id='.$id.'&action=1&photo_id='.$item['photo_id'].'" >-</a>';

		}
		print '</td><td valign = "top">&nbsp;</td></tr>';
	}
}
?>
</tbody>
</table>
</div>
</div>

							<!-- tab 3 -->
							<div role="tabpanel" class="tab-pane <?php if($tab==2){ print 'active'; } ?>" id="Completion">
								<div id="tabs-3" style = "">
									<table class = "table">
										<tbody>
										<tr style="font-weight:bold;">
											<td>Thumbnail</td>
											<td>Filename</td>
											<td>Keywords</td>
											<td>&nbsp;</td>
											<td>Date/Time</td>
											<td>
												&nbsp;
											</td>
											<td>&nbsp;</td>
										</tr>
										<?php
	                if($finished_photos['error'] == 0)
	                {
	                    unset($finished_photos['error']);

	                    foreach($finished_photos as $item)
	                    {
	                        $keywords = '';
	                        if(!empty($item['keywords']))
	                        {
	                            foreach($item['keywords'] as $key)
	                            {
	                                $keywords .= $key['keyword'] . ', ';
	                            }
	                        }
	                        print '<tr>';

										// determine if file is image or other
										$img = 0;
										$f = "uploads/images/".$id."/".$item['filename'];
										$fdata = @pathinfo($f);
										$imgtypes = array('jpg','jpeg','png','JPG','JPEG');
										if(in_array($fdata['extension'], $imgtypes))
										{
											$img=1;
										}
										if($fdata['extension']=='pdf')
										{
											$img=2;
										}
										if($fdata['extension']=='PDF')
										{
											$img=2;
										}
										if($fdata['extension']=='gif')
										{
											$img=3;
										}
										if($fdata['extension']=='GIF')
										{
											$img=3;
										}

										# image
										if($img == 1)
										{                                
											//$thumb='thumbnail.php?file=uploads/images/'.$id.'/'.$item['filename'].'&width=80&height=80';   

											$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';

											print '<td style="" valign = "top"><a href="uploads/images/'.$id.'/'.$item['filename'].'" title="'.$item['filename'].'" class="fancybox" rel="group"><img src="'.$thumb.'" border="0"></a><br>';
											//print_r($exif).'<br>';
										}

											# pdf
											if($img == 2)
											{
												print '<td style="" valign = "top"><a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'"><img src="images/pdf.png" width = "80" height = "80" border="0"></a><br>';
											}

											// gif
											if($img == 3)
											{
												//$thumb='thumbnail.php?file=uploads/images/'.$id.'/'.$item['filename'].'&w=80&h=80';

												$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';
												
												print '<td style="" valign = "top"><a href="uploads/images/'.$id.'/'.$item['filename'].'" title="'.$item['filename'].'" class="fancybox" rel="group"><img src="'.$thumb.'" border="0"></a><br>';
											}

											# something else
											if($img == 0)
											{
												print '<td style="" valign = "top"><a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'"><img src="images/nopreview.png" width = "80" height = "80" border="0"></a><br>';
											}

											// download button
											print '<a href = "download_photo.php?project_id='.$id.'&id='.$item['photo_id'].'" class = "plus-photo btn btn-primary btn-xs">Export</a>';

											print '</td>

										<td style="" valign = "top">'.$item['display_name'].'</td>

										<td valign = "top">'.rtrim($keywords, ', ').'<br>';

											if($projects_permissions['edit']==1)
											{
											print '<a href = "edit_photo_keywords.php?project_id='.$id.'&photo_id='.$item['photo_id'].'" class = "plus btn btn-primary btn-xs">Edit</a></td>

										    <td valign = "top">';

											print '<select id = "'.$item['photo_id'].'" class = "category" style = "margin-left:5px;">';
												print '<option value = "'.$item['category'].'">';
													if($item['category']==1)
													{
													print 'Survey';
													}
													if($item['category']==2)
													{
													print 'Manuf.';
													}
													if($item['category']==3)
													{
													print 'Completion';
													}
													print '</option>';
												print '<option value = "" disabled="disabled">----</option>';
												print '<option value = "1">Survey</option>';
												print '<option value = "2">Manuf.</option>';
												print '<option value = "3">Completion</option>';
												print '</select></td>';

											}
											else
											{
												print '<td></td>';
											}

										$ts = strtotime($item['ts']);
										$ts = date('m/d/Y',$ts);
										print '<td valign = "top">'.$ts.'</td>

										<td style="text-align:left;" valign = "top">';
											if($projects_permissions['delete']==1)
											{

												//print '<a href="project_site_photos.php?id='.$id.'&action=1&photo_id='.$item['photo_id'].'" class = "plus-delete btn-xs btn-danger">-</a>';

												print '	<a class = "btn btn-xs btn-danger delete-photo   " href = "#delete-photo-form" title = "Delete photo" data-href="project_site_photos.php?tab=2&id='.$id.'&action=1&photo_id='.$item['photo_id'].'" >-</a>';

											}
											print '</td>
										<td valign = "top">&nbsp;</td>
										</tr>';
										}
										}
										?>
										</tbody>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div><!--close panel-->
			<div id="contentBottom" style = "margin-top:10px;height:560px;">


</div>

<div class="push">
</div>

<div id="dialog">
</div>

<style>
table select{
	border: 1px solid #ccc;
	height: auto;
}
.nav-tabs > li > a{
	border-top: 5px solid transparent;
	-webkit-border-radius: 0;
	-moz-border-radius: 0;
	border-radius: 0;
	margin-right: 0;
}
.nav-tabs .active{
	border-top: 5px solid #4a89dc;
}
</style>

</section>
</section>

<div id="delete-photo-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Photo</h1>
	<p>Are you sure you want to delete this site photo?</p>
	<p><a id = "delete-photo-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<div id="email-photo-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<div id = "pform_result" class = "alert alert-info" style = "display:none;">
	</div>
	<?php
	if($show > 0) 
    {
        unset($site_photos['error']);
        print '<form id = "pform">';
        print '<table class = "table">';
        /*
        print '<tr style = "margin-bottom:10px;">';
        print '<td>Send To:';
        print '</td>';
        print '<td><input type = "text" name = "send_to_name" id = "send_to_name" style = "width:400px;">';
        print '</td>';
        print '</tr>';
        */
        print '<tr>';
        print '<td>Email:<br><input type = "text" name = "send_to" id = "send_to" value = "'.$project['project_contact_email'].'" class = "form-control">';
        print '</td>';
        print '<td>';
        print '</td>';
        print '</tr>';
        print '<tr>';
        print '<td>Subject:<br><input type = "text" name = "subject" id = "subject" class = "form-control">';
        print '</td>';
        print '<td>';
        print '</td>';
        print '</tr>';
        print '<tr>';
        print '<td>Message:<br><textarea name = "message" id = "message" class = "form-control"></textarea>';
        print '</td>';
        print '<td>';
        print '</td>';
        print '</tr>';
        print '<tr>';
        print '<td colspan = "2">Select Photos:';
        print '</td>';
        print '</tr>';        

        foreach($site_photos as $item) 
        {
            print '<tr>';

            // determine if file is image or other
			$img = 0;
			$f = "uploads/images/".$id."/".$item['filename'];
			$fdata = @pathinfo($f);
			$imgtypes = array('jpg','jpeg','png','JPG','JPEG');
			if(in_array($fdata['extension'], $imgtypes))
			{
				$img=1;
			}
			if($fdata['extension']=='pdf')
			{
				$img=2;
			}
			if($fdata['extension']=='PDF')
			{
				$img=2;
			}
			if($fdata['extension']=='gif')
			{
				$img=3;
			}
			if($fdata['extension']=='GIF')
			{
				$img=3;
			}

			# image
			if($img == 1)
			{                                
				$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';
				print '<td style="padding:5px;" valign = "top"><img src="'.$thumb.'" border="0"></td>';
				print '<td style="padding:5px;" valign = "top"><input type = "checkbox" name = "pix_to_send[]" value = "'.$f.'"></td>';
			}

			# pdf
			if($img == 2)
			{
				print '<td style="padding:5px;" valign = "top"><img src="images/pdf.png" border="0" width = "80" height = "80"></td>';
				print '<td style="padding:5px;" valign = "top"><input type = "checkbox" name = "pix_to_send[]" value = "'.$f.'"></td>';
			}

			// gif
			if($img == 3)
			{
				$thumb = 'thumb.php?src=uploads/images/'.$id.'/'.$item['filename'].'&size=80x80';
				print '<td style="padding:5px;" valign = "top"><img src="'.$thumb.'" border="0"></td>';
				print '<td style="padding:5px;" valign = "top"><input type = "checkbox" name = "pix_to_send[]" value = "'.$f.'"></td>';
			}

			# something else
			if($img == 0)
			{
				print '<td style="padding:5px;" valign = "top"><img src="images/nopreview.png" width = "80" height = "80" border="0"></td>';
				print '<td style="padding:5px;" valign = "top"><input type = "checkbox" name = "pix_to_send[]" value = "'.$f.'"></td>';
			}

            print '</tr>';
        }
        print '</table>';

        print '<p><a id = "send-pix" class="btn btn-lg btn-success" href = "#">Send</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">Cancel</a></p>';

        print '</form>';
    }
    else
    {
    	print 'No photos to send.';
    }
	?>	
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<!--<script src="vendor/plugins/bootstrap/bootstrap.min.js"></script>-->
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
<script type="text/javascript" src="vendor/plugins/fancybox/source/jquery.fancybox.js"></script>
<link rel="stylesheet" type="text/css" href="vendor/plugins/fancybox/source/jquery.fancybox.css" media="screen" />

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function()
{

	"use strict";

	// Init Theme Core
	Core.init();

	$(".fancybox").fancybox(
	{

		afterShow: function()
		{
		    var click = 1;
		    $('.fancybox-wrap').append('<a href = "#" id="rotate_button" class = "btn btn-success btn-lg">Rotate</a>')
		        .on('click', '#rotate_button', function(e)
		        {
		        	e.preventDefault();
		            var n = 90 * ++click;
		            $('.fancybox-skin').css('webkitTransform', 'rotate(-' + n + 'deg)');
		            $('.fancybox-skin').css('mozTransform', 'rotate(-' + n + 'deg)');
		        });
		}

	});

	// send pix by email
	// ajax function to send selected photos by email
	$('#send-pix').click(function(e) 
	{
		e.preventDefault();
		$.post("jq.send_pix.php", $("#pform").serialize())
		  .done(function( result ) 
		  {
		  		//$("#pform").reset();
		  		$("#pform_result").show();
		  		$("#pform_result").html(result);
		  });
	});

	$('.delete-photo').click(function() 
	{
		$('#delete-photo-yes').attr('href', $(this).data('href'));
		}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-photo-form',
		modal: true
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) 
	{
		e.preventDefault();
		$.magnificPopup.close();
	});

	$('.category').on('change', function()
	{
		var category = $(this).val();
		var id = this.id;
		var url = "project_site_photos.php?action=2&photo_id="+id+"&id="+<?php print $id; ?>+"&category="+category;
		window.location.href=url;
	});

	$('.category').each(function()
	{
		$(this).css('width','120px');
		//$(this).css('border','1px solid red');
	});

	// email button was clicked
	$('#email').click(function() 
	{
		//$('#delete-photo-yes').attr('href', $(this).data('href'));
		}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#email-photo-modal',
		modal: true
	});
});

/*
$(document).ready(function()
{
	var params = getUrlVars(location.href);

	function getUrlVars(url) {
		var hash;
		var myJson = {};
		var hashes = url.slice(url.indexOf('?') + 1).split('&');
		for (var i = 0; i < hashes.length; i++) {
			hash = hashes[i].split('=');
			myJson[hash[0]] = hash[1];
		}
		return myJson;
	}

	var tabNumber = params.category;
	console.log(tabNumber, $('.nav-tabs li:nth-child('+ tabNumber +')'));
	$('.nav-tabs li:nth-child('+ tabNumber +')').tab('show');
});
*/

</script>

</body>
</html>