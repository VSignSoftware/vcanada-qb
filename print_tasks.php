<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$user = $vujade->get_user($_SESSION['user_id']);
$tasks = $vujade->get_tasks_for_user($user['employee_id'],2);

if(isset($_REQUEST['task_id']))
{
	$all=false;
	$task_id=$_REQUEST['task_id'];
	$task = $vujade->get_task($task_id);
	$title = $task['task_subject'];
	//$content = '<h1>'.$title.'</h1>';
	//$content.= '<div style = "">';
	$content= '<p>Subject: <strong>'.$task['task_subject'].'
	</strong></p>';
	$content.= '<p>Due Date: <strong>'.$task['task_due_date'].'
	</strong></p><p>Completed: <strong>'.$completed_date.'</strong></p>';
	$content.= '<p>Created By: <strong>';
	if($firsttask['is_sales_task']==1)
	{
		$cb = $vujade->get_employee($task['created_by'],1);
	}
	else
	{
		$cb = $vujade->get_employee($task['created_by'],2);
	}
	
	$content.=  $cb['fullname'];
	$content.=  '</strong></p>';
	$content.=  '<p>Assigned To: <strong><br>';
	@$employee_ids = explode(',',$task['employee_ids']);
	$ceids = count($employee_ids);
	//print $ceids;
	if($ceids>0)
	{
		foreach($employee_ids as $eid)
		{
			$at = $vujade->get_employee($eid,2);
			if($at['error']=='0')
			{
				$content.=  $at['fullname'];

				// check if user has completed it and get completed date
				// only for manager permissions users
				if($manager_permissions['read']==1)
				{
					$ct2 = $vujade->get_user_task($task['database_id'],$eid);
					if($ct2['error']=="0")
					{
						if($ct2['is_complete']==1)
						{
							$content.=  ' - <font color = "red">Completed on '.date('m/d/Y', $ct2['complete_ts']).'</font>';
						}
					}
				}
				$content.= '<br>';
			}
		}
	}
	$content.=  '</strong></p>';
	$content.=  'Message: <strong>'.$task['task_message'].'</strong></p>';
	$content.=  '</div>';
}
else
{
	$all=true;
	$title = 'Tasks: '.$user['first_name'].' '.$user['last_name'];
	$tasks = $vujade->get_tasks_for_user($user['employee_id'],2);
	$content = '';
	if($tasks['error']=='0')
    {
        unset($tasks['error']);
        $content .= '<table width = "870">';
        $content .= '<tbody>';
        foreach($tasks as $task)
        {
            $tdd = strtotime($task['task_due_date']);
            //$link = 'project_tasks.php?id='.$task['project_id'].'&taskid='.$task['database_id'];
            // sales task should only appear if current date = task date or after
            if($task['is_sales_task']==1)
            {
                
                if($today>=$tdd)
                {
                    
                    $content .= '<tr class="clickableRow" href="'.$link.'">';
                    $content .= '<td width = "" valign = "top" style = "">';
                    if($today>$tdd)
                    {
                        $content .= '<font color = "red">';
                    }
                    $content .= $task['task_due_date'];
                    if($today>$tdd)
                    {
                        $content .= '</font>';
                    }
                    $content .= '</td>';
                    $content .= '<td width = "" valign = "top" style = "">';
                    $content .= $task['project_id'];
                    $content .= '</td>';
                    $content .= '<td width = "" valign = "top" style = "">';
                    $project = $vujade->get_project($task['project_id'],2);
                    $content .= $project['site'];
                    $content .= '</td>';
                    $content .= '<td width = "" valign = "top" style = "">';
                    $content .= $task['task_subject'];
                    $content .= '</td>';
                    $content .= '</tr>';
                    $task_count++;
                }
            }
            else
            {
                // not a sales task and it should appear until it is done
                $content .= '<tr class="clickableRow" href="'.$link.'">';
                $content .= '<td width = "" valign = "top" style = "">';
                if($tdd<$today)
                {
                    $content .= '<font color = "red">';
                }
                $content .= $task['task_due_date'];
                if($tdd<$today)
                {
                    $content .= '</font>';
                }
                $content .= '</td>';
                $content .= '<td width = "" valign = "top" style = "">';
                $content .= $task['project_id'];
                $content .= '</td>';
                $content .= '<td width = "" valign = "top" style = "">';
                $project = $vujade->get_project($task['project_id'],2);
                $content .= $project['site'];
                $content .= '</td>';
                $content .= '<td width = "" valign = "top" style = "">';
                $content .= $task['task_subject'];
                $content .= '</td>';
                $content .= '</tr>';
                $task_count++;
            }
            
        }
        $content .= '</tbody>';
        $content .= '</table>';
    }
}

$charset="ISO-8859-1";
$html = '<!DOCTYPE html>
    <head>
    <meta charset="'.$charset.'">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>'.$title.'</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>
    <body>';
$html.=$content;
$html.='</body></html>';

$header_1 = '<htmlpageheader name="header_1">'.$title;
$header_1 .= '</htmlpageheader>';

$footer_1 = '<htmlpagefooter name="footer_1"><div class = "footer">Page {PAGENO}</div></htmlpagefooter>';

//print $html;
//die;

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$margin_left="10";
$margin_right="10";
$margin_top="10";
$margin_bottom="5";
$margin_header="5";
$margin_footer="5";
$orientation="";
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);

//new mPDF($mode, $format, $font_size, $font, $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);

//$mpdf->setAutoTopMargin = 'stretch';
//$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
//$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
//$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->defaultfooterline = 0;
//$page_total = $mpdf->AliasNbPages('[pagetotal]');
$mpdf->setFooter('Page {PAGENO} of {nb}');
$mpdf->WriteHTML($html);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-tasks.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output('tasks.pdf','I'); 
}
?>