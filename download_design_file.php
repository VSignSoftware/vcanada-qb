<?php
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$file_id = $_REQUEST['file_id'];
$project_id = $_REQUEST['project_id'];
$vujade->connect();
$file = $vujade->get_design_file($file_id);
if($file['error']=='0')
{
	$file_url = 'uploads/design_files/'.$project_id.'/'.$file['file_name'];
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
	readfile($file_url);
}
else
{
	print $file['error'];
	die;
}
?>