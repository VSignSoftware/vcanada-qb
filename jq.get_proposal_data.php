<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$setup = $vujade->get_setup();
$proposal_id = $_POST['proposal_id'];
$type = $_POST['type'];
$json = array();

# data from the proposal 
if($type==1)
{
	$proposal = $vujade->get_proposal($proposal_id,$idcol="proposal_id");
	if($proposal['error']=="0")
	{
		$items = $vujade->get_items_for_proposal($proposal_id);
		if($items['error']=="0")
		{
			unset($items['error']);
			$sp = 0;
			foreach($items as $item)
			{
				$sp+=$item['amount'];
			}
		}

		# selling price [0]
		//$json[]=$proposal['amounts'];
		$json[]=number_format($sp,2,'.',',');
		
		# tax rate [1]
		$json[]=$proposal['tax_rate'];

		if($setup['non_illuminated_sales_tax']==1)
		{

			if($proposal['is_illuminated']==1)
			{
				$first_tax_amount = $sp * 1;
			}
			else
			{
				$first_tax_amount = $sp * $setup['state_sales_tax'];
			}
		}

		# tax amount [2]
		$proposal['tax_total'] = $first_tax_amount * $proposal['tax_rate'];

		$json[]=number_format($proposal['tax_total'],2,'.',',');

		# total [3]
		$t = $sp+$proposal['tax_total'];
		$json[]=number_format($t,2,'.',',');

		// non-illuminated sign option [4]
		if($setup['non_illuminated_sales_tax']==1)
		{
			$json[]=$proposal['is_illuminated'];
		}

	}
	else
	{
		$json[]='Error';
	}
	print json_encode($json);
}

# proposal item data
if($type==2)
{
	$items = $vujade->get_items_for_proposal($proposal_id);
	if($items['error']=="0")
	{
		unset($items['error']);
		foreach($items as $item)
		{
			$f = preg_replace('/[\r\n]+/', '', $item['item']);
			$json[]=$f.'<br>';
			//print $item['item'].'<br>';
		}
	}
	else
	{
		$json[]='Error';
		//print 'Error';
	}
	print json_encode($json);
}
?>