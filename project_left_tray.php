<?php
$site = $vujade->trim_text($project['site'],25,18,0);

# set up for the numbers (project x of y) under the site name in the top left
# user came to this page from the project search 
# or user came from the queues page 
//$vujade->debug_array($_SESSION);
if( (isset($_SESSION['search_id'])) && ($_SESSION['search_id']!='') )
{
	# project ids in the found set
	// $pids = $_SESSION['project_ids'];

	// new method: get search from stored results in database
	$params = $vujade->get_search_params($_SESSION['search_id']);

	if($params['error']=="0")
	{
		$cpids=$params['pids'];
	}

	//print $cpids;
	//die;

	if(!empty($cpids))
	{
		$pids=explode('^',$cpids);
	}

	# total found
	$count = count($pids);

	if($count>0)
	{
		//foreach($pids as $k => $dbid) 
		foreach($pids as $k => $dbid)
		{
			//$temp_project = $vujade->get_project($dbid);
			// $npids[]=$temp_project['project_id'];
			$npids[]=$dbid;
		}
		sort($npids);
		//print_r($npids);
		//die;

		# first id
		$first['project_id']=current($npids);

		# last id
		$last['project_id']=end($npids);
		
		// go back to first one
		reset($npids);

		# current place in the found set
		$place = 0;
		foreach($npids as $k => $v)
		{
			//$tp = $vujade->get_project($v);
			if($v==$id)
			{
				$place = $k;
				$place_text=$k+1;
			}
		}

		if($place_text>$count)
		{
			$place_text=$count;
		}

		# determine where user is in the project id array 
		# and set the previous and next ids correctly
		if(($place-1)==0)
		{
			$previous['project_id'] = $first['project_id'];
		}
		else
		{
			$previous['project_id'] = $npids[$place-1];
			if(empty($previous['project_id']))
			{
				$previous['project_id']=$first['project_id'];
			}
		}

		if(($place+1)>$count)
		{
			$next['project_id']=$last['project_id'];
		}
		else
		{
			$next['project_id'] = $npids[$place+1];
			if(empty($next['project_id']))
			{
				$next['project_id']=$last['project_id'];
			}
		}
	}
}
else
{
	# they did not come to this page from the search 
	# and they did not come from the queues
	$number=date('y');
	$search = array('number'=>$number);
	$found_projects=$vujade->find_projects($search,0,0,true,'project_id');
	$count = $found_projects['count'];

	if($count>0)
	{
		unset($found_projects['count']);
		unset($found_projects['sql']);
		unset($found_projects['sql2']);
		unset($found_projects['error']);
		foreach($found_projects as $dbid) 
		{
			//$temp_project = $vujade->get_project($dbid);
			//$npids[]=$temp_project['project_id'];
			$npids[]=$dbid;
		}
		sort($npids);
		//print_r($npids);

		# first id
		$first['project_id']=current($npids);

		# last id
		$last['project_id']=end($npids);
		
		// go back to first one
		reset($npids);

		# current place in the found set
		$place = 0;
		foreach($npids as $k => $v)
		{
			//$tp = $vujade->get_project($v);
			if($v==$id)
			{
				$place = $k;
				$place_text=$k+1;
			}
		}

		if($place_text>$count)
		{
			$place_text=$count;
		}

		# determine where user is in the project id array 
		# and set the previous and next ids correctly
		if(($place-1)==0)
		{
			$previous['project_id'] = $first['project_id'];
		}
		else
		{
			$previous['project_id'] = $npids[$place-1];
			if(empty($previous['project_id']))
			{
				$previous['project_id']=$first['project_id'];
			}
		}

		if(($place+1)>$count)
		{
			$next['project_id']=$last['project_id'];
		}
		else
		{
			$next['project_id'] = $npids[$place+1];
			if(empty($next['project_id']))
			{
				$next['project_id']=$last['project_id'];
			}
		}
	}
	$list_url='projects.php?action=1';
}
?>

<!-- begin: .tray-left -->
<div id = "pmlt" class="tray /*tray-left*/ tray220 p20" style = "width:210px;">

	<h1 style = "margin-top:0px;padding-top:0px;margin-bottom:0px;padding-bottom:0px;font-size:48px;"><?php print $project['project_id']; ?></h1>
	<h2><small><?php print $site; ?></small></h2>

	<div style = "margin-bottom:15px;">
		<div style = "float:left;">
			<a style = "" href="project.php?id=<?php print $first['project_id']; ?>">
			<i class="glyphicon glyphicon-fast-backward"></i>
			</a> 
			<a href="project.php?id=<?php print $previous['project_id']; ?>"><i class="glyphicon glyphicon-backward"></i></a> 
		</div>
		<div style = "border-radius: 2px;float:left;text-align:center;color:white;background-color:#4A89DC;padding:2px;margin-left:5px;margin-right:5px;">
		<?php print $place_text." of ".$count; ?>
		</div> 
		<div style = "float:left;">
			<a href="project.php?id=<?php print $next['project_id']; ?>"><i class="glyphicon glyphicon-forward"></i></a> 
			<a href="project.php?id=<?php print $last['project_id']; ?>"><i class="glyphicon glyphicon-fast-forward"></i></a>
		</div> 
	</div>
	
	<?php
	if($projects_permissions['edit']==1)
	{
		?>
		<div style = "float:left;clear:both;margin-top:15px;width:100%;">
		<a href = "edit_project.php?id=<?php print $id; ?>" class = "btn btn-xs btn-primary">EDIT</a> 
	<?php
	}

	// list button
	// user search but did not come from the queues page
	if( (isset($_SESSION['search_id'])) && (!isset($_SESSION['list_url'])) )
	{
		# takes them back to the projects page with the search already performed for them and results shown
		print '<a class = "btn btn-xs btn-primary" href = "projects.php?action=2">LIST</a>';
	}
	// user came from the queues
	if( (isset($_SESSION['search_id'])) && (isset($_SESSION['list_url'])) )
	{
		# takes them back to the projects page with the search already performed for them and results shown
		print '<a class = "btn btn-xs btn-primary" href = "'.$_SESSION['list_url'].'">LIST</a>';
	}
	// user did not search or come from queues
	if( (!isset($_SESSION['search_id'])) && (!isset($_SESSION['list_url'])) )
	{
		print '<a href = "'.$list_url.'" class = "btn btn-xs btn-primary">LIST</a>';
	}
	if($projects_permissions['edit']==1)
	{
		?>
		<a id = "duplicate" href = "#" class = "btn btn-xs btn-primary">DUPLICATE</a> 
		</div>
	<?php
	}
	?>	

	<div style = "margin-top:15px;float:left;clear:both;font-size:14px;">
	  	Sales - <br>
		<?php print '<strong>'.$project['salesperson'].'</strong><br>'; ?>
		PM - <br>
		<?php print '<strong>'.$project['project_manager'].'</strong>'; ?>
	</div>

	<!-- project menu -->
	<style>
	#project_menu a
	{
		font-size: 14px;
		display:block;
	}
	a.selected 
	{
		color:#E9573F;
		text-decoration: none;
		display:block;
	}
	</style>
	<div style = "margin-top:15px;padding-top:15px;border-top:1px dashed #cecece;float:left;clear:both;width:100%;" id = "project_menu">
		
		<a href="project_tasks.php?id=<?php print $id; ?>" <?php if($menu==2){ print 'class = "selected"';}?>>TASKS</a>
		

		<a href="contacts.php?id=<?php print $id; ?>" <?php if($menu==16){ print 'class = "selected"';}?>>CONTACTS</a>
		

		<a href="sales.php?id=<?php print $id; ?>" <?php if($menu==13){ print 'class = "selected"';}?>>SALES</a>
		
		<a href="project.php?id=<?php print $id; ?>" <?php if($menu==1){ print 'class = "selected"';}?>>SCOPE OF WORK</a>
		
		<a href="project_designs.php?id=<?php print $id; ?>" <?php if($menu==3){ print 'class = "selected"';}?>>DESIGN</a>
		
		<a href="project_estimates.php?id=<?php print $id; ?>" <?php if($menu==4){ print 'class = "selected"';}?>>ESTIMATES</a>
		
		<a href="project_proposals.php?id=<?php print $id; ?>" <?php if($menu==5){ print 'class = "selected"';}?>>PROPOSAL</a>
		
		<a href="project_shop_orders.php?id=<?php print $id; ?>" <?php if($menu==6){ print 'class = "selected"';}?>>SHOP ORDER</a>
		
		<a href="project_job_status.php?id=<?php print $id; ?>" <?php if($menu==7){ print 'class = "selected"';}?>>PROJECT STATUS</a>
		
		<a href="project_cost_to_date.php?id=<?php print $id; ?>" <?php if($menu==14){ print 'class = "selected"';}?>>COST TO DATE</a>
		
		<a href="project_purchase_orders.php?id=<?php print $id; ?>" <?php if($menu==8){ print 'class = "selected"';}?>>PURCHASE ORDER</a>
		
		<a href="project_invoices.php?id=<?php print $id; ?>" <?php if($menu==12){ print 'class = "selected"';}?>>INVOICE</a>
		
		<?php
		$show_costing = 0;
		$costing_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
		if($costing_permissions['read']==1)
		{
			$show_costing=$show_costing+1;
		}
		if($show_costing==1)
		{
		?>	
			<a href = "costing_overview.php?id=<?php print $id; ?>" <?php if($menu==17){ print 'class = "selected"';}?>>COSTING</a>

			<?php 
			// costing sub menu
			if($menu==17)
			{ 
				?>
				<div class = "well">
					<a href = "costing_overview.php?id=<?php print $id; ?>" <?php if($cmenu==1){ print 'class = "selected"';}?>>OVERVIEW</a>

					<a href = "costing_labor.php?id=<?php print $id; ?>" <?php if($cmenu==2){ print 'class = "selected"';}?>>LABOR</a>

					<a href = "costing_ledger.php?id=<?php print $id; ?>" <?php if($cmenu==3){ print 'class = "selected"';}?>>LEDGER</a>

					<a href = "costing_inventory.php?id=<?php print $id; ?>" <?php if($cmenu==4){ print 'class = "selected"';}?>>INVENTORY</a>

					<a href = "costing_purchase_orders.php?id=<?php print $id; ?>" <?php if($cmenu==5){ print 'class = "selected"';}?>>PURCHASE ORDERS</a>

					<a href = "costing_machines.php?id=<?php print $id; ?>" <?php if($cmenu==8){ print 'class = "selected"';}?>>MACHINES</a>

					<a href = "costing_invoices.php?id=<?php print $id; ?>" <?php if($cmenu==6){ print 'class = "selected"';}?>>INVOICES</a>

					<a href = "costing_markup.php?id=<?php print $id; ?>" <?php if($cmenu==7){ print 'class = "selected"';}?>>MARKUP</a>
				</div>

				<?php
			}
			?>

		<?php } ?>
		<a href="project_documents.php?id=<?php print $id; ?>" <?php if($menu==10){ print 'class = "selected"';}?>>DOCUMENTS</a>

		<a href="project_site_photos.php?id=<?php print $id; ?>" <?php if($menu==11){ print 'class = "selected"';}?>>PHOTOS</a>

		<a href="project_production_files.php?id=<?php print $id; ?>" <?php if($menu==9){ print 'class = "selected"';}?>>PRODUCTION FILES</a>

	</div>

	<style>
	.duplicate-modal 
	{
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}

	/* Modal Content/Box */
	.duplicate-modal-content 
	{
	    background-color: #fefefe;
	    margin: 15% auto; /* 15% from the top and centered */
	    padding: 20px;
	    border: 1px solid #888;
	    width: 40%; /* Could be more or less, depending on screen size */
	}

	/* The Close Button */
	.duplicate-modal-close 
	{
	    color: #aaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}

	.duplicate-modal-close:hover,
	.duplicate-modal-close:focus 
	{
	    color: black;
	    text-decoration: none;
	    cursor: pointer;
	}
	</style>

	<?php
	if($projects_permissions['edit']==1)
	{
		?>

		<div id="duplicate-modal" class = "duplicate-modal">
		  <div class = "duplicate-modal-content">
		  	<span class="duplicate-modal-close">x</span>
		      <p>
		      	<form method = "post" action = "project.php">
		      		<input type = "hidden" name = "action" value = "3">
		      		<input type = "hidden" name = "id" value = "<?php print $id; ?>">
			      	You are about to duplicate this job. In addition to the basic<br>job elements, you can also duplicate the following: 
			      	<br>
			      	<br>
			      	<input type = "checkbox" name = "scope" id = "scope" value = "1">Scope of work
			      	<br>
			      	<input type = "checkbox" name = "designs" id = "designs" value = "1">Designs
			      	<br>
			      	<input type = "checkbox" name = "estimates" id = "estimates" value = "1">Estimates
			      	<br>
			      	<input type = "checkbox" name = "proposals" id = "proposals" value = "1">Proposals
			      	<br>
			      	<br>
			      	<input type = "submit" class = "btn btn-success" value = "Continue"> <input type = "button" id = "cancel_btn" class = "btn btn-danger" value = "Cancel">
			      	</form>
		      </p>
		  </div>
		</div>

		<script>

			// Get the modal
			var modal = document.getElementById('duplicate-modal');

			// Get the button that opens the modal
			var btn = document.getElementById("duplicate");

			// Get the <span> element that closes the modal
			var span = document.getElementsByClassName("duplicate-modal-close")[0];

			// cancel button
			var cancel_btn = document.getElementById("cancel_btn");

			// When the user clicks on the button, open the modal 
			btn.onclick = function() {
			    modal.style.display = "block";
			}

			// When the user clicks on <span> (x), close the modal
			span.onclick = function() 
			{
			    modal.style.display = "none";
			}
			cancel_btn.onclick = function() 
			{
			    modal.style.display = "none";
			}

			// When the user clicks anywhere outside of the modal, close it
			window.onclick = function(event) 
			{
			    if (event.target == modal) {
			        modal.style.display = "none";
			    }
			}
		</script>

	<?php } ?>

</div>