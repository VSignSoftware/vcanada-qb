<?php 
//session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();

$setup=$vujade->get_setup();

error_reporting(0);

// only for qb servers
if($setup['is_qb']==1)
{
	require_once('/var/www/qb-bridge/qbbridgeimportfunctions.php');
	_queueImports();
	//mail('toddhamm30@gmail.com','qb cron executed '.date('m/d/Y h:i:s'),'it was executed');

	// delete from the qb log anything older than 5 days
	$vujade->delete_qb_log();

	// deletes from the qb queue
	// $vujade->delete_qb_queue();
}

// all other servers

// delete from the user activity log older than 
$vujade->delete_log();

// delete from the search log
$vujade->delete_search_log();
?>