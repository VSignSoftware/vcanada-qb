<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
# new
if($action==1)
{
	$type=$_POST['type'];
	$vujade->check_empty($type,'Type');
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		$vujade->create_row('document_types');
		$row_id = $vujade->row_id;
		$s=$vujade->update_row('document_types',$row_id,'name',$type);
		if($s==1)
		{
			$vujade->messages[]="Type added.";
		}
		else
		{
			$vujade->errors[]="Type could not be added. ";
		}
	}
}
# update
if($action==2)
{
	$type=$_POST['type'];
	$type_id = $_POST['type_id'];
	$vujade->check_empty($type,'Type');
	$vujade->check_empty($type_id,'Type ID');
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		$s=$vujade->update_row('document_types',$type_id,'name',$type);
		if($s==1)
		{
			$vujade->messages[]="Type updated.";
		}
		else
		{
			$vujade->errors[]="Type could not be updated. ";
		}
	}
}
# delete
if($action==3)
{
	$type_id = $_POST['type_id'];
	$vujade->check_empty($type_id,'Type ID');
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		$s=$vujade->delete_row('document_types',$type_id);
		if($s==1)
		{
			$vujade->messages[]="Type deleted.";
		}
		else
		{
			$vujade->errors[]="Type could not be deleted. ";
		}
	}
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Document Types Setup - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">
        	<?php
        	$ss_menu=4;
        	require_once('site_setup_menu.php');
        	?>
        </aside>
        <!-- end: .tray-left -->
        <!-- begin: .tray-center -->
        <div class="tray tray-center">
            <div class="pl20 pr50">
			<div class="panel panel-primary panel-border top">
            <div class="panel-body bg-light">
			<?php
			$vujade->show_errors();
			$vujade->show_messages();
			$types = $vujade->get_document_types();
			if($types['error']=="0")
			{
				unset($types['error']);
				print '<table style="border-spacing: 4px; border-collapse: separate;">';
				print '<tr>';
				print '<td valign = "top">Document Type</td>';
				print '<td valign = "top">&nbsp;</td>';
				print '<td valign = "top">&nbsp;</td>';
				print '</tr>';
				foreach($types as $type)
				{
					print '<tr>';
					print '<form method = "post" action = "document_type_setup.php">';
					print '<input type = "hidden" name = "action" value = "2">';
					print '<input type = "hidden" name = "type_id" value = "'.$type['database_id'].'">';
					print '<td valign = "top" style="width:400px;"><input type = "text" class="form-control" name = "type" value = "'.$type['name'].'"></td>';
					print '<td valign = "top"><input type = "submit" class="btn btn-primary" value = "Update" class = "sbt200"></td>';
					print '</form>';
					print '<form method = "post" action = "document_type_setup.php">';
					print '<input type = "hidden" name = "action" value = "3">';
					print '<input type = "hidden" name = "type_id" value = "'.$type['database_id'].'">';
					print '<td valign = "top"><input type = "submit" class="btn btn-primary" value = "Delete" class = "sbt200"></td>';
					print '</form>';
					print '</tr>';
				}
			}
			print '<tr>';
			print '<form method = "post" action = "document_type_setup.php">';
			print '<input type = "hidden" name = "action" value = "1">';
			print '<td valign = "top"><input type = "text" class="form-control" name = "type" value = ""></td>';
			print '<td valign = "top"><input type = "submit" class="btn btn-primary" value = "Save" class = "sbt200"></td>';
			print '</form>';
			print '<td valign = "top">&nbsp;</td>';
			print '</tr>';
			print '</table>';
			?>
		</div>
            </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->
 <!-- BEGIN: PAGE SCRIPTS -->
 <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {
    "use strict";
    // Init Theme Core    
    Core.init();
    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });
    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();
    	// reset all buttons to primary
   	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});
    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');
    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});
    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>
