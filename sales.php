<?php
//*
// function for testing and debugging
function ddd($elem, $die=false)
{
	echo "<pre>";

	if($elem) print_r($elem);
	else var_dump($elem);

	echo "</pre>";

	if($die) die();

}
//*/
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);

if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

# active tab
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
	if(!in_array($tab, array(0,1,2)))
	{
		$tab=0;
	}
}
else
{
	$tab=0;
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# delete a conversation
if($action==1)
{
	$conversation_id=$_REQUEST['conversation_id'];
	$s = $vujade->delete_row('sales_conversations',$conversation_id);
	$tab=0;
}

// add a followup task
if($action==2)
{
	$date = $_POST['date'];
	$details = $_POST['details'];

	$vujade->create_row('tasks');
	$taskid = $vujade->row_id;
	$s=array();
	$s[]=$vujade->update_row('tasks',$taskid,'project_id',$id);
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$employee['employee_id'].',');
	$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',$date);
	$s[]=$vujade->update_row('tasks',$taskid,'task_message',$details);
	$s[]=$vujade->update_row('tasks',$taskid,'task_subject',$details);
	$s[]=$vujade->update_row('tasks',$taskid,'created_by',$_SESSION['user_id']);
	//$s[]=$vujade->update_row('tasks',$taskid,'is_follow_up',1);
	//$s[]=$vujade->update_row('tasks',$taskid,'is_regular_task',0);
	$ts = strtotime($date);
	$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);
	$s[]=$vujade->update_row('tasks',$taskid,'is_sales_task',1);

	// user task row
	$vujade->create_row('user_tasks');
	$nid = $vujade->row_id;
	$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
	$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$employee['employee_id']);
	$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$project_id);

	$vujade->messages[]='Task created.';
	$tab=1;
}

// change the job status
if($action==3)
{
	$status = $_REQUEST['status'];
	$update_value = "";
	$uv="Pending";
	$num = 1;
	if($status==2)
	{
		$update_value = "Drawings";
		$uv.= " ";
		$num = 2;
	}
	if($status==3)
	{
		$update_value = "Estimates";
		$uv.= " ";
		$num = 3;
	}
	if($status==4)
	{
		$update_value = "Proposal";
		$uv.= " ";
		$num = 4;
	}
	if($status==5)
	{
		$update_value = "Proposal Sent";
		$uv.= " ";
		$num = 5;
	}
	$s = array();
	$js = $vujade->get_job_status($id);
	if($js['error']!="0")
	{
		// create a new job status record for this project
		$vujade->create_row('job_status');
		$s[]=$vujade->update_row('job_status',$vujade->row_id,'project_id',$id);
		$s[]=$vujade->update_row('job_status',$vujade->row_id,'pending_alt',$update_value);
	}
	else
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt',$update_value);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'city_permits_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'survey_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'ll_approval_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'manufacturing_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'shipping_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_date','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'turned_in_for_billing',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'process_order',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'is_closed',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'did_not_sell',0);

		$shop_order = $vujade->get_shop_order($id, 'project_id');
		$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'type','');
	}

	$status = "Pending ".$update_value;
	$status = trim($status);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status',$status);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',$num);
	$tab = 0;
	$project=$vujade->get_project($id,2);
}

// show the status button error
$show_status_error=0;

if(!preg_match('/Pending/',$project['status']))
{
	$show_status_error=1;
}
else
{
	$show_status_error=0;
}

$shop_order = $vujade->get_shop_order($id, 'project_id');
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$setup = $vujade->get_setup(1);

$section=3;
$menu=13;
$title = 'Sales - ' . $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

	<!-- Begin: Content -->
	<section id="content" class="table-layout animated fadeIn">

		<!-- begin: .tray-left -->
		<?php require_once('project_left_tray.php'); ?>
		<!-- end: .tray-left -->

		<!-- begin: .tray-center -->
		<div class="tray tray-center" style = "width:100%;">

			<div class="pl15 pr15" style = "width:100%;">

				<?php require_once('project_right_tray.php'); ?>

				<!-- main content for this page -->
				<div style="width: 100%">
<!--					<div class="alert alert-micro alert-primary">-->
<!--						Sales-->
<!--					</div>-->

					<style>
						#show_status_error
						{
							padding:5px;
							border:1px solid red;
							color: red;
						}

						.conversation-date.active,
						.contact-name.active{
							background: #e5e5e5;
							text-decoration: none !important;
						}
					</style>

					<?php
					if($show_status_error==1)
					{
						?>

						<p id = "show_status_error">
							<strong>This page cannot control the status of this job because the job is active. To change to pending, please select 'Change Order' in the shop orders section.</strong>
						</p>

						<?php
					}
					else
					{
						?>
						<div style = "margin-top:10px;">
							<a class = "<?php if($project['status']=="Pending"){ print 'btn btn-primary';}else{ print 'btn btn-default';}?>" href = "sales.php?id=<?php print $id; ?>&action=3&status=1">PENDING</a>
							<a class = "<?php if($project['status']=="Pending Drawings"){ print 'btn btn-primary';}else{ print 'btn btn-default';}?>" href = "sales.php?id=<?php print $id; ?>&action=3&status=2">PENDING DRAWINGS</a>
							<a class = "<?php if($project['status']=="Pending Estimates"){ print 'btn btn-primary';}else{ print 'btn btn-default';}?>" href = "sales.php?id=<?php print $id; ?>&action=3&status=3">PENDING ESTIMATES</a>
							<a class = "<?php if($project['status']=="Pending Proposal Sent"){ print 'btn btn-primary';}else{ print 'btn btn-default';}?>" href = "sales.php?id=<?php print $id; ?>&action=3&status=5" style = "width:200px;">PENDING PROPOSAL SENT</a>
						</div>

					<?php } ?>

					<div class="panel" style="margin-top: 19px;">
						<?php $tabIndex = $_GET["tab"]; ?>
						<div class="panel-heading">
							<ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
								<li class="<?php if($tabIndex < 2 || $tabIndex > 3) { print 'active'; } ?>"><a href="#tabs-1" data-toggle="tab" aria-expanded="false">Conversations</a></li>
								<li class="<?php if($tabIndex == 2) { print 'active'; } ?>"><a href="#tabs-2" data-toggle="tab" aria-expanded="false">Follow-up</a></li>
							</ul>
						</div>

						<div class="panel-body">
							<div class="tab-content pn br-n">

								<!-- conversations -->
								<div id="tabs-1" class="tab-pane <?php if($tabIndex < 2 || $tabIndex > 3) { print 'active'; } ?>">

									<div class="row">
										<div class="col-md-2" style="width:200px;">
											<div class="panel panel-primary panel-border top">
												<div class="panel-heading">
													<span class="panel-title">Conversations</span>
													<div class="widget-menu pull-right">
														<a href="new_sales_conversation.php?project_id=<?php print $id; ?>" class="btn btn-primary btn-xs">NEW</a>
													</div>
												</div>
												<div class="panel-body">
													<?php
													$dates = $vujade->get_conversations($id);
													if($dates['error']=="0")
													{
														$show_conversations=1;
														unset($dates['error']);
														foreach($dates as $date)
														{
															print '<a class = "conversation-date" href = "conversation-'.$date['id'].'" id = "'.$date['id'].'">';
															print $date['date'].' ';
															//print $design['design_id'];
															print '</a><br>';
														}
													}
													?>
												</div>
											</div>
										</div>

										<div id = "conversation-list" class="col-md-9" style="margin-left:5px;border:0px solid red;padding:0px;">

											<?php
											if($show_conversations==1)
											{
												foreach($dates as $date)
												{
													print '<div class="panel-conversation panel panel-primary panel-border top" id = "conversation-'.$date['id'].'">';

													print '<div class="panel-heading">';
													print '<div style = "float:left;width:200px;color:#2876A5;font-weight:bold;">';
													print $date['date'];
													print '</div>';
													print '<div style = "float:left;margin-right:5px;color:#2876A5;font-weight:bold;width:275px;">';
													print $date['name_display'];
													print '</div>';

													print '<div class="widget-menu pull-right">';
													?>
													<a href = "edit_sales_conversation.php?cid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>" class="btn btn-xs btn-primary">EDIT</a>
													<a class = "btn btn-xs btn-danger delete-conversation" id="delete_conversation" href="#delete_conversation_form_<?= $date['id']; ?>" title = "Delete Conversation">Delete</a>
													<!-- modal for delete conversation -->
													<div id="delete_conversation_form_<?= $date['id']; ?>" class="delete-conversation-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
														<h1>Delete Conversation</h1>
														<p>Are you sure you want to delete this conversation?</p>
														<p>
															<a id = "" class="btn btn-lg btn-danger"
															   href="sales.php?action=1&conversation_id=<?= $date['id']; ?>&id=<?= $id; ?>">YES</a>
															<a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>
														</p>
													</div>
													<?php
													print '</div>';

													print '</div>';

													print '<div class = "panel-body" id = "cb-'.$date['id'].'">';
													print $date['body'];

													print '</div>';
													print '</div>';
												}
											}
											?>
										</div>

									</div>
								</div>

								<!-- followup -->
								<div id="tabs-2" class="tab-pane <?php if($tabIndex == 2) { print 'active'; } ?>">
									<?php
									$vujade->show_messages();
									?>
									<form method = "post" action = "sales.php">

										<p>Use this form to add a follow up task on the specified date. The task will appear in your task list on the dashboard on the date you enter below.</p>

										<p>
											<label for="name">Date task will appear on your dashboard: </label>
											<input type = "text" name = "date" id="date" class = "form-control" style="width: 300px;">
										</p>

										<p>
											<label for="details">Task Details: </label>
											<textarea name = "details" id = "details" class="form-control ckeditor"></textarea>
										</p>

										<input type = "hidden" name = "action" value = "2">
										<input type = "hidden" name = "id" value = "<?php print $id; ?>">
										<input type = "submit" value = "SAVE" class = "btn btn-success btn-lg">
									</form>

									<!-- ckeditor new version 4.5x -->
									<?php require_once('ckeditor.php'); ?>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(function() {
		// date picker
		$('#date').datepicker();
	});
</script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
	$(function()
	{
		"use strict";

		// Init Theme Core
		Core.init();

		// focus on a conversation
		var HEADER = 60; // header height
		$('.conversation-date').click(function(e){
			e.preventDefault();

			var conv = $("#conversation-"+this.id);

			if( $(this).hasClass('active') ){
				$(this).removeClass('active');
				conv.removeClass('panel-warning');
			} else {
				$('.conversation-date').not( $(this) ).removeClass('active');
				$(this).addClass('active');

				$('.panel-conversation').not( conv ).removeClass('panel-warning');
				conv.addClass('panel-warning');

				$('html, body').animate({
					scrollTop: conv.offset().top - HEADER
				}, 500);
			}
		});

		// focus on a contact
		$('.contact-name').click(function(e){
			e.preventDefault();

			var contact = $("#contact-"+this.id);

			if( $(this).hasClass('active') ){
				$(this).removeClass('active');
				contact.removeClass('panel-warning');
			} else {
				$('.contact-name').not( $(this) ).removeClass('active');
				$(this).addClass('active');

				$('.panel-contact').not( contact ).removeClass('panel-warning');
				contact.addClass('panel-warning');

				$('html, body').animate({
					scrollTop: contact.offset().top - HEADER
				}, 500);
			}
		});

		// modals
		$('.delete-conversation').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '.delete-conversation-form',
			modal: true
		});

		$('.delete-contact').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '.delete-contact-form',
			modal: true
		});

		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
