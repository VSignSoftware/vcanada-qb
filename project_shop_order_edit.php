<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$so_permissions = $vujade->get_permission($_SESSION['user_id'],'Shop Orders');
if($so_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($so_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$setup = $vujade->get_setup();
$invoice_setup = $vujade->get_invoice_setup();
$s=array();
# determine if shop order exists
$shop_order = $vujade->get_shop_order($id,"project_id");
if($shop_order['error']!="0")
{
	unset($shop_order);
	# does not exist, create
	$vujade->create_row('shop_orders');
	$shop_order=$vujade->get_shop_order($vujade->row_id);
	$s[]=$vujade->update_row('shop_orders',$vujade->row_id,'project_id',$id);
}

// clean up legacy shop orders that don't have line items
// this converts this shop order to the new format
// it creates one line item from the desc. and clears the desc. field
if(!empty($shop_order['description']))
{
	// create 1 new line item from legacy
    $vujade->create_row('proposal_items');
    $row_id = $vujade->row_id;
    $s[] = $vujade->update_row('proposal_items',$row_id,'proposal_id',$id);
    $s[] = $vujade->update_row('proposal_items',$row_id,'item',$shop_order['description']);
    $s[] = $vujade->update_row('proposal_items',$row_id,'amount',0);
    $s[] = $vujade->update_row('proposal_items',$row_id,'sort',1);

    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name','');
    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate','');

    // clear the description
    $s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'description','');

    // get the shop order again
    unset($shop_order);
    $shop_order = $vujade->get_shop_order($id,"project_id");

	// get line items saved to this shop order again to include the new one from legacy
	$proposal_items = $vujade->get_items_for_proposal($id);
}

// set an initial value for the tax rate
$pcity = $project['city'];
$pstate = $project['state'];
if((!empty($pcity)) && (!empty($pstate)) )
{
	if($setup['is_qb']==1)
	{
		//$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);

		// check for group sales tax
		$td = $vujade->get_tax_groups($project['city'].', '.$project['state']);
		if($td['count']==1)
		{
			$tax_data['rate'] = $td['rate'];
		}
		else
		{
			// does not have group sales tax
			$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
		}

	}
	else
	{
		$tax_data = $vujade->get_tax_for_city($project['city'], $project['state']);
	}
}
if(empty($shop_order['tax_rate']))
{
	if($setup['country']!='Canada')
	{
		$tax_rate=$tax_data['rate'];
		$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'tax_rate',$tax_rate);
	}
	else
	{
		$tax_rate=1;
		$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'tax_rate',1);
	}
}
else
{
	$tax_rate=$shop_order['tax_rate'];
}

// test if salesperson has been assigned
// if has, get sales person employee data
$semployee = $vujade->get_employee_by_name($project['salesperson']);

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

// update items button pressed 
// or transfer from proposal 
// or recalculate or save button pressed
if(in_array($action,array(1,2,5,6,7,8)))
{

	$sales_price=str_replace(',', '', $_POST['sales_price']);
	$tax_rate=$_POST['tax_rate'];
	$tax_amount=str_replace(',', '', $_POST['tax_amount']);
	$custom_tax_amount=$_POST['custom_tax_amount'];
	$shop_order_total=0;
	$line_total=0;
	$tax_line_total=0;
	$s = array();

	// update the line items
	if($action==2)
	{
		// line items
	    $skipped = array('action','project_id','type','design','date_opened','expiration_date','design','proposal','date_revised','po_number','or_week_of','due_date','description','pm','estimate_1','estimate_2','estimate_3','estimate_4','estimate_5','estimate_6','estimate_7','estimate_8','estimate_9','estimate_10');
	    foreach($_POST as $key => $value)
	    {
	        if(!in_array($key, $skipped))
	        {
	        	//print $key.' : '.$value.'<br>';
	            # key will either be
	            // [desc-150]
	            // or
	            // [amount-150]
	            // or
	            // [sort-150]
	            // or
	            // [tax-.33]
	            # determine which to update
	            $test_key = explode('-',$key);
	            $row_id=$test_key[1];
	            if($test_key[0]=="desc")
	            {
	                $description=$value;
	                $s[] = $vujade->update_row('proposal_items',$row_id,'item',$description);
	            }
	            if($test_key[0]=="amount")
	            {
	                $amount=str_replace(",", "", $value);
	                $s[] = $vujade->update_row('proposal_items',$row_id,'amount',$amount);
	                $line_total+=$amount;
	            }
	            if($test_key[0]=="sort")
	            {
	                $sort=str_replace(",", "", $value);
	                $s[] = $vujade->update_row('proposal_items',$row_id,'sort',$sort);
	            }
	            if($test_key[0]=="tax")
	            {
	                $value = explode('^',$value);
	                $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name',$value[0]);
	                $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate',$value[1]);
	            }
	        }
	    }

	    // new one
	    $description=$_POST['description'];
	    $amount=str_replace(',', '',$_POST['amount']);
	    $sort=str_replace(',', '',$_POST['sort']);
	    $line_total+=$amount;
	    if(!empty($description))
	    {
	        $vujade->create_row('proposal_items');
	        $row_id = $vujade->row_id;
	        $s[] = $vujade->update_row('proposal_items',$row_id,'proposal_id',$id);
	        $s[] = $vujade->update_row('proposal_items',$row_id,'item',$description);
	        $s[] = $vujade->update_row('proposal_items',$row_id,'amount',$amount);
	        $s[] = $vujade->update_row('proposal_items',$row_id,'sort',$sort);

	        $tax_type = explode('^',$_POST['tax_type']);
	        $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name',$tax_type[0]);
	        $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate',$tax_type[1]);
	    }
	}

	// transfer from proposal
	if($action==6)
	{
		// import line items from proposal if the exists
		// delete existing
		$s[]=$vujade->delete_row('proposal_items',$id,1,'proposal_id');

		// get existing line items from the proposal
		// and copy to shop order
		$proposal=$_POST['proposal'];

		// get the proposal
		$proposal_data=$vujade->get_proposal($proposal,'proposal_id');

		$test_proposal_items = $vujade->get_items_for_proposal($proposal);
		if($test_proposal_items['error']=="0")
		{
			unset($test_proposal_items['error']);
			foreach($test_proposal_items as $item)
			{
				$vujade->create_row('proposal_items');
			    $row_id = $vujade->row_id;
			    $s[] = $vujade->update_row('proposal_items',$row_id,'proposal_id',$id);
			    $s[] = $vujade->update_row('proposal_items',$row_id,'item',$item['item']);
			    $s[] = $vujade->update_row('proposal_items',$row_id,'amount',$item['amount']);
			    $s[] = $vujade->update_row('proposal_items',$row_id,'sort',$item['sort']);
			    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name',$item['tax_label_name']);
			    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate',$item['tax_label_rate']);
			}
		}
		unset($test_proposal_items);
		$sales_price=0;
		$tax_rate=$proposal_data['tax_rate'];
		$tax_amount=0;
	}

	// transfer from scope of work
	if($action==8)
	{

		// get tax rate from job location
		// qb tax state is the same as shipping address
		if($setup['is_qb']==1)
		{
			if($project['state']==$setup['qb_tax_state'])
			{
				$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
			}
			else
			{
				// not the same
				$tax_data = $vujade->QB_get_sales_tax('Out of State');
			}
			if($tax_data['error']=="0")
			{
				$tax_rate=$tax_data['rate']; 
			}
			else
			{
				$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
				$tax_rate=$tax_data2['rate'];
			}
		}
		else
		{
			$tax_data = $vujade->get_tax_for_city($project['city'],$project['state']);
		}

		// delete existing
		$s[]=$vujade->delete_row('proposal_items',$id,1,'proposal_id');

		$vujade->create_row('proposal_items');
	    $row_id = $vujade->row_id;
	    $s[] = $vujade->update_row('proposal_items',$row_id,'proposal_id',$id);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'item',$project['description']);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'amount',0);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'sort',0);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name','');
	    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate','');
		
		$sales_price=0;
		$tax_rate=$tax_data['rate'];
		$tax_amount=0;
	}

	if($setup['country']=='Canada')
	{
		$tax_rate=1;
	}
	
	// get all line items and determine tax amount
    // add to line_total
    // update the project scope of work
    $project_scope_of_work = '';
    $line_total=0;
    $tax_line=0;
    $tax_line_total=0;
    $proposal_items = $vujade->get_items_for_proposal($id);
    if($proposal_items['error'] == "0") 
    {
        unset($proposal_items['error']);
        $tax_line=0;
        foreach($proposal_items as $item) 
        {
        	$line_total+=$item['amount'];
        	$tax_line=$item['amount']*$item['tax_label_rate']*$tax_rate;
        	$tax_line_total+=$tax_line;
        	$tax_line=0;
        	$project_scope_of_work.=$item['item'].'<br>';
        }
    }

    // update scope of work
    $s[] = $vujade->update_row('projects',$project['database_id'],'description',$project_scope_of_work);

	// cannot be 0
	if(empty($sales_price))
	{
		$sales_price=$line_total;
		$tax_amount=$tax_line_total;
	}

	// if there is a tax rate, calculate the tax amount
	//if(!empty($tax_rate))
	//{
	//	$tax_amount=$sales_price*$tax_rate;
	//}

	// user entered custom tax amount
	//print $custom_tax_amount;
	//print '<br>';
	if($custom_tax_amount==1)
	{
		$tax_amount=str_replace(',', '', $_POST['tax_amount']);
	}
	else
	{
		// automatically calculate it as the sum of the tax on the line items
		$tax_amount=$tax_line_total;
	}

	//print $tax_amount;
	//die;

	$shop_order_total=$sales_price+$tax_amount;

	// explicitly reset amounts to sum of line items
	if($action==2)
	{
		$sales_price=$line_total;
		$tax_amount=$tax_line_total;
		$shop_order_total=$sales_price+$tax_amount;
	}
	if($action==6)
	{
		$sales_price=$line_total;
		$tax_amount=$tax_line_total;
		$shop_order_total=$sales_price+$tax_amount;
	}

    // update everything else
	$due_date=$_POST['due_date'];
	$type=$_POST['type'];
	$design=$_POST['design'];
	$or_week_of=$_POST['or_week_of'];
	$date_opened=$_POST['date_opened'];
	$proposal=$_POST['proposal'];
	$invoice_date=$_POST['invoice_date'];
	$date_revised=$_POST['date_revised'];
	$po_number=$_POST['po_number'];
	$closed_date=$_POST['closed_date'];

	$estimate_1=$_POST['estimate_1'];
	$estimate_2=$_POST['estimate_2'];
	$estimate_3=$_POST['estimate_3'];
	$estimate_4=$_POST['estimate_4'];
	$estimate_5=$_POST['estimate_5'];
	$estimate_6=$_POST['estimate_6'];
	$estimate_7=$_POST['estimate_7'];
	$estimate_8=$_POST['estimate_8'];
	$estimate_9=$_POST['estimate_9'];
	$estimate_10=$_POST['estimate_10'];
	$estimate_11=$_POST['estimate_11'];
	$estimate_12=$_POST['estimate_12'];
	$estimate_13=$_POST['estimate_13'];
	$estimate_14=$_POST['estimate_14'];

	$pm_id = $_POST['pm'];
	$project_manager = $vujade->get_employee($pm_id,2);

	$estimates = "";
	if(!empty($estimate_1))
	{
		$estimates .= $estimate_1.'^';
	}
	if(!empty($estimate_2))
	{
		$estimates .= $estimate_2.'^';
	}
	if(!empty($estimate_3))
	{
		$estimates .= $estimate_3.'^';
	}
	if(!empty($estimate_4))
	{
		$estimates .= $estimate_4.'^';
	}
	if(!empty($estimate_5))
	{
		$estimates .= $estimate_5.'^';
	}
	if(!empty($estimate_6))
	{
		$estimates .= $estimate_6.'^';
	}
	if(!empty($estimate_7))
	{
		$estimates .= $estimate_7.'^';
	}
	if(!empty($estimate_8))
	{
		$estimates .= $estimate_8.'^';
	}
	if(!empty($estimate_9))
	{
		$estimates .= $estimate_9.'^';
	}
	if(!empty($estimate_10))
	{
		$estimates .= $estimate_10.'^';
	}
	if(!empty($estimate_11))
	{
		$estimates .= $estimate_11.'^';
	}
	if(!empty($estimate_12))
	{
		$estimates .= $estimate_12.'^';
	}
	if(!empty($estimate_13))
	{
		$estimates .= $estimate_13.'^';
	}
	if(!empty($estimate_14))
	{
		$estimates .= $estimate_14.'^';
	}

	# update
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'type',$type);
	
	if(empty($shop_order['date_opened']))
	{
		$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'date_opened',$date_opened);
		$ts = strtotime($date_opened);
		$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'ts',$ts);
	}

	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'date_revised',$date_revised);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'estimates',$estimates);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'design',$design);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'proposal',$proposal);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'po_number',$po_number);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'due_date',$due_date);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'or_week_of',$or_week_of);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'invoice_date',$invoice_date);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'closed_date',$closed_date);
	$s[] = $vujade->update_row('projects',$project['database_id'],'project_manager',$project_manager['fullname']);

	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'selling_price',$sales_price);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'tax_rate',$tax_rate);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'tax_amount',$tax_amount);
	$s[]=$vujade->update_row('shop_orders',$shop_order['database_id'],'custom_tax_amount',$custom_tax_amount);

	# update the project scope of work to the new description
	//$s[]=$vujade->update_row('projects',$project['database_id'],'description',$description);

	# determine what the active status is
	/*
	If we have a due date done in the permits section, but not in the LL, then it will need to read " Active LL approval".  If there are done dates in the LL approval, and permits, then it will read " Active Manufacturing" 

	# get the done dates, if any
	*/

	$js = $vujade->get_job_status($id);
	if($js['error']!="0")
	{
		$vujade->create_row('job_status');
		$row_id = $vujade->row_id;
		$s[]=$vujade->update_row('job_status',$row_id,'project_id',$id);
		$js = $vujade->get_job_status($id);
	}

	# clear dates and status
	$s[]=$vujade->update_row('job_status',$js['database_id'],'ll_approval_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'city_permits_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'survey_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'manufacturing_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'shipping_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_end_date','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'turned_in_for_billing',0);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'process_order',1);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date_ts','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'service_done','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'service_date','');
	$s[]=$vujade->update_row('job_status',$js['database_id'],'is_closed',0);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'did_not_sell',0);
	//$s[]=$vujade->update_row('job_status',$js['database_id'],'','');

	$project = $vujade->get_project($id,2);
	if( ($type=="Service") || ($type=="Install") )
	{
		if($type=="Service")
		{
			$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Service');
			$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',12);
		}
		if($type=="Install")
		{
			$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Active Install');
			$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',11);
		}

		// determine which date to use
		if(!empty($due_date))
		{
			$service_date=$due_date;
		}

		// as long as there is a date...
		if(!empty($service_date))
		{
			if($type=="Service")
			{
				// update the job status date
				$s[]=$vujade->update_row('job_status',$js['database_id'],'service_date',$service_date);

				// delete existing events
			    $dsql = "DELETE FROM `calendar` WHERE `project_id` = '$id' AND `type` IN ('installation','service')";
			   	$try = $vujade->generic_query($dsql,0,1);

				// generate a calendar event
			   	$vujade->create_row('calendar');
				$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$service_date);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$service_date);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','service');
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
	        }
	        if($type=="Install")
	        {
	        	// update the job status date
				$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date',$service_date);
				$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date_ts',strtotime($service_date));

				// delete existing events
			    $dsql = "DELETE FROM `calendar` WHERE `project_id` = '$id' AND `type` IN ('installation','service')";
			   	$try = $vujade->generic_query($dsql,0,1);

				// generate a calendar event
			   	$vujade->create_row('calendar');
				$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$service_date);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$service_date);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','installation');
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
	        }
		}
	}
	else
	{
		$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Active LL Approval');
		$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',6);

		// determine which date to use
		if(!empty($due_date))
		{
			$install_date=$due_date;
		}

		// as long as there is a date...
		if(!empty($install_date))
		{
			// update the job status date
			$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date',$install_date);
			$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date_ts',strtotime($install_date));

			// delete existing events
		    $dsql = "DELETE FROM `calendar` WHERE `project_id` = '$id' AND `type` IN ('installation','service')";
		   	$try = $vujade->generic_query($dsql,0,1);

			// generate a calendar event
		   	$vujade->create_row('calendar');
			$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$install_date);
            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$install_date);
            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','installation');
            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
		}
	}

	# send a task to every person who is supposed to receive notices of jobs processed
	if($action==7)
	{
		$emp1 = $vujade->get_employee($_SESSION['user_id']);
		$emps = $vujade->get_employees(4);
		if($emps['error']=="0")
		{
			unset($emps['error']);
			$vujade->create_row('tasks');
			$taskid = $vujade->row_id;
			$s=array();
			$s[]=$vujade->update_row('tasks',$taskid,'project_id',$project['project_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',date('m/d/Y'));
			
			$s[]=$vujade->update_row('tasks',$taskid,'task_message','Project #'.$project['project_id'].' has been processed and is ready for printing.');
			$s[]=$vujade->update_row('tasks',$taskid,'task_subject','Ready for processing');
			$s[]=$vujade->update_row('tasks',$taskid,'created_by',$emp1['employee_id']);
			$ts = strtotime('now');
			$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);
			$empsarr = '';
			foreach($emps as $gt)
			{
				$empsarr.=$gt['employee_id'].',';

				// user task row
				$vujade->create_row('user_tasks');
				$nid = $vujade->row_id;
				$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
				$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$gt['employee_id']);
				$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$project['project_id']);
				
			}
			$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$empsarr);
		}
	}

	if($type=="Service")
	{
		$status = "Service";
	}
	else
	{
		$status = "Active LL Approval";
	}
	if($action==7)
	{
		$vujade->page_redirect('project_shop_orders.php?id='.$id);
	}
}

// delete line item 
if($action==3)
{
    $s = array();
    $row_id = $_POST['item_id'];
    $s[]=$vujade->delete_row('proposal_items',$row_id); 
}

// adds a new blank item in the place where the user clicked the button
// re-sorts the existing line items
if($action==4)
{
	$position=$_REQUEST['position'];
	$after_id = $_REQUEST['after_id'];
	$vujade->update_proposal_items_sort($after_id,$id,$position);
}

// get the shop order again
$shop_order = $vujade->get_shop_order($id,"project_id");
$sales_price=$shop_order['selling_price'];
$tax_amount=$shop_order['tax_amount'];
$custom_tax_amount=$shop_order['custom_tax_amount'];
$shop_order_total=$sales_price+$tax_amount;

// line items saved to this shop order
$proposal_items = $vujade->get_items_for_proposal($id);

// line items saved to the selected proposal
if(!empty($shop_order['proposal']))
{
	$test_proposal_items = $vujade->get_items_for_proposal($shop_order['proposal']);
}

$show_designs=0; // default (project does not have designs)
$designs = $vujade->get_designs_for_project($id);
if($designs['error']=='0')
{
	unset($designs['error']);
	unset($designs['next_id']);
	$show_designs=1; // has at least one design
}

$estimate_ids = $vujade->get_estimate_ids_for_project($id);
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$menu=6;
$title = $project['project_id'].' - '.$project['site'].' - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Start: Topbar -->
<header id="topbar">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-active">
                <a href = "#"><?php print 'Shop Order: '.$id; ?></a>
            </li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->
<!-- Begin: Content -->
<section id="content" class="">
<div class="theme-primary">
<div class="panel heading-border panel-primary">
<div class="panel-body bg-light">
<div class="row">
    <!-- work location -->
    <div class="col-md-6" style = "width:49%;">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 0 8px">
                <span class="panel-title" style="font-size: 13px; font-weight:600">Work Location</span>
            </div>
            <div class="panel-body">
                <p>
                    <?php
                    # get site location for this project
                    print $project['site'] . '<br>';
                    print $project['address_1'] . '<br>';
                    print $project['city'] . ', ';
                    print $project['state'] . ' ';
                    print $project['zip'];
                    ?>
                </p>
            </div>
        </div>
    </div>
    <!-- billing info box -->
    <div class="col-md-6" style = "width:49%;">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 0 8px">
                <span class="panel-title" style="font-size: 13px; font-weight:600">Billing Information</span>
            </div>
            <div class="panel-body">
                <p>
                    <?php
                    # get billing info for this project
                    //print $project['client_id'].'<br>';
                    if($setup['is_qb']==1)
                    {
                    	$customer1 = $vujade->get_customer($project['client_id'], 'ListID');
                    	if($customer1['error']=="0")
                    	{
                    		$customer=$customer1;
                    	}
                    	$customer2 = $vujade->get_customer($project['client_id'], 'ID');
                    	if($customer2['error']=="0")
                    	{
                    		$customer=$customer2;
                    	}
                    }
                    else
                    {
                    	$customer = $vujade->get_customer($project['client_id']);
                    }
                    // print_r($customer);
                    $customer_contact = $vujade->get_contact($project['client_contact_id']);
                    if($customer['error'] == "0") 
                    {
                        print $customer['name'] . '<br>';
                        if ($customer_contact['error'] == "0") {
                            print $customer_contact['full_name'] . '<br>';
                        }
                        print $customer['address_1'] . '<br>';
                        if (!empty($customer['address_2'])) {
                            print $customer['address_2'] . '<br>';
                        }
                        print $customer['city'] . ', ' . $customer['state'] . ' ' . $customer['zip'] . '<br>';
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>

<form method = "post" action = "project_shop_order_edit.php" id = "np">
<input type = "hidden" id = "action" value = "1" name = "action">
<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

<!-- first section input boxes -->
<div class="panel panel-primary panel-border top">
	<div class="panel-heading" style = "padding:0px;">
	</div>
	<div class="panel-body" style = "padding-top:0px;">
		<table class = "table" style = "padding-top:0px;margin-top:0px;">
			<tr>
				<td>
					<strong>Project #:</strong>
				</td>
				<td>
					<?php print $project_id; ?>
				</td>
				<td>
					<strong>Salesperson:</strong>
				</td>
				<td>
					<?php print $project['salesperson']; ?>
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>

			<tr>
				<td>
					<strong>Type:</strong>
				</td>
				<td>
					<select name = "type" id = "type" class = "form-control">
					<?php 
					$vujade->get_so_options($shop_order['type']);
					?>
					</select>
				</td>
				<td>
					<strong>Design:</strong>
				</td>
				<td>
					<select name = "design" class = "form-control">
					<?php
					if(!empty($shop_order['design']))
					{
						print '<option value = "'.$shop_order['design'].'" selected = "selected">'.$shop_order['design'].'</option>';
					}
					print '<option value = "">-Select-</option>';
					$designs = $vujade->get_designs_for_project($project_id);
					if($designs['error']=="0")
					{
						unset($designs['error']);
						unset($designs['next_id']);
						foreach($designs as $design)
						{
							print '<option value = "'.$design['design_id'].'">'.$design['design_id'].'</option>';
						}
					}
					?>
					</select>
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
			</tr>

			<tr>
				<td>
					<strong>Date Opened:</strong>
				</td>
				<td>
					<input type = "text" class = "dp form-control" name = "date_opened" id = "date_opened" style = "width:150px;" value = "<?php if(!empty($shop_order['date_opened'])){ print $shop_order['date_opened']; }else{print date('m/d/Y');} ?>" <?php if(!empty($shop_order['date_opened'])){ print 'disabled'; } ?>>
				</td>
				<td>
					<strong>Proposal:</strong>
				</td>
				<td>
					<select name = "proposal" id = "proposal" class = "form-control">
					<?php
					if(!empty($shop_order['proposal']))
					{
						print '<option value = "'.$shop_order['proposal'].'" selected = "selected">'.$shop_order['proposal'].'</option>';
					}
					print '<option value = "">-Select-</option>';

					$proposals = $vujade->get_proposals_for_project($project_id);
					if($proposals['error']=="0")
					{
						unset($proposals['error']);
						unset($proposals['next_id']);
						foreach($proposals as $proposal)
						{
							print '<option value = "'.$proposal['proposal_id'].'">'.$proposal['proposal_id'].'</option>';
						}
					}
					?>
					</select>
				</td>
				<td>
					<!--<strong>Invoice Date:</strong>-->
				</td>
				<td>
					<?php //print $shop_order['invoice_date']; ?>
				</td>
				
			</tr>

			<tr>
				<td>
					<strong>Date Revised:</strong>
				</td>
				<td>
					<input type = "text" style = "width:150px;" class = "form-control dp" name = "date_revised" id = "date_revised" value = "<?php if(!empty($shop_order['date_revised'])){ print $shop_order['date_revised']; } ?>">
				</td>
				<td>
					<strong>PO Number:</strong>
				</td>
				<td>
					<input type = "text" class = "form-control" name = "po_number" style = "width:150px;" id = "po_number" value = "<?php if(!empty($shop_order['po_number'])){ print $shop_order['po_number']; } ?>">
				</td>
				<td>
					<!--<strong>Date Closed:</strong>-->
				</td>
				<td>
					<?php //print $shop_order['closed_date']; ?>
				</td>
			</tr>

			<tr>
				<td>
					<strong>Due Date</strong>
				</td>
				<td>
					<select name = "or_week_of" class = "form-control">
						<?php
						if(!empty($shop_order['or_week_of']))
						{ 
							print '<option selected value = "'.$shop_order['or_week_of'].'">'.$shop_order['or_week_of'].'</option>'; 
						}
						?>
						<option value = "">-Select-</option>
						<option value = "Firm Date">Firm Date</option>
						<option value = "Or Week Of">Or Week Of</option>
					</select>
				</td>
				<td>
					<input type = "text" class = "dp form-control" name = "due_date" id = "due_date" style = "width:150px;" value = "<?php if(!empty($shop_order['due_date'])){ print $shop_order['due_date']; } ?>">
				</td>
				<td colspan = "2">&nbsp;</td>
			</tr>

			<tr>
				<td colspan = "6">&nbsp;</td>
			</tr>
		</table>
		
		<table class = "table" style = "padding-top:0px;margin-top:0px;">
			<tr>
				<td>
					<strong>Estimates:</strong>
				</td>
				<td style = "padding-left:38px;">
					<?php
					# set up for estimates
					$estimates = $vujade->get_estimates_for_project($project_id);
					if($estimates['error']=="0")
					{
					$show_estimates = 1;
					unset($estimates['error']);
					unset($estimates['next_id']);
					}
					@$ests = explode('^',$shop_order['estimates']);
					?>
					<select name = "estimate_1" class = "form-control">
					<?php
					if(!empty($ests[0]))
					{
					print '<option value = "'.$ests[0].'">'.$ests[0].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_2" class = "form-control">
					<?php
					if(!empty($ests[1]))
					{
					print '<option value = "'.$ests[1].'">'.$ests[1].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_3" class = "form-control">
					<?php
					if(!empty($ests[2]))
					{
					print '<option value = "'.$ests[2].'">'.$ests[2].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_4" class = "form-control">
					<?php
					if(!empty($ests[3]))
					{
					print '<option value = "'.$ests[3].'">'.$ests[3].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_5" class = "form-control">
					<?php
					if(!empty($ests[4]))
					{
					print '<option value = "'.$ests[4].'">'.$ests[4].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select> 
				</td>
			</tr>
		
			<tr>
				<td>
					&nbsp;
				</td>
				<td style = "padding-left:38px;">
					<select name = "estimate_6" class = "form-control input-sm">
					<?php
					if(!empty($ests[5]))
					{
					print '<option value = "'.$ests[5].'">'.$ests[5].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_7" class = "form-control input-sm">
					<?php
					if(!empty($ests[6]))
					{
					print '<option value = "'.$ests[6].'">'.$ests[6].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_8" class = "form-control input-sm">
					<?php
					if(!empty($ests[7]))
					{
					print '<option value = "'.$ests[7].'">'.$ests[7].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
				<td>
					<select name = "estimate_9" class = "form-control">
					<?php
					if(!empty($ests[8]))
					{
					print '<option value = "'.$ests[8].'">'.$ests[8].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select> 
				</td>
				<td>
					<select name = "estimate_10" class = "form-control">
					<?php
					if(!empty($ests[9]))
					{
					print '<option value = "'.$ests[9].'">'.$ests[9].'</option>';
					}
					print '<option value = "">-Select-</option>';
					if($show_estimates==1)
					{
					foreach($estimates as $estimate)
					{
					print '<option value = "'.$estimate['estimate_id'].'">'.$estimate['estimate_id'].'</option>';
					}
					}
					?>	
					</select>
				</td>
			</tr>

			<tr>
				<td colspan = "6">&nbsp;</td>
			</tr>
			
		</table>

		<div class = "row">
			<div style = "float:left;width:119px;margin-left:17px;padding-top:10px;">
				<strong>Project Manager: </strong>
			</div>
			<div style = "float:left;width:200px;">
				<select name = "pm" class = "form-control" style = "">
				<?php
				if(!empty($project['project_manager']))
				{
					$emp2 = $vujade->get_employee_by_name($project['project_manager']);
					print '<option value = "'.$emp2['employee_id'].'" selected = "selected">'.$project['project_manager'].'</option>';
					print '<option value = "">-select-</option>';
				}
				else
				{
					print '<option value = "">-select-</option>';
				}
				$pms = $vujade->get_project_managers();
				if($pms['error']=='0')
				{
					unset($pms['error']);
					foreach($pms as $pm)
					{
						print '<option value = "'.$pm['employee_id'].'">'.$pm['fullname'].'</option>';
					}
				}
				?>
				</select>
			</div>
			<div style = "float:right;margin-right:15px;">
				
			</div>
		</div>

	</div>
</div>

<!-- transfer button and modal -->	
<div class = "row">
	<div class = "pull-right" style = "margin-right:15px;">
		
		<a class = "btn btn-xs btn-primary pull-right" id = "transfer" href = "#transfer-form" title = "Transfer from other" style = "margin-left:5px;">Transfer</a> 
		<a class = "btn btn-xs btn-primary pull-right" id = "preview" href = "#" title = "Print Preview">Print Preview</a>

		<!-- transfer modal -->
		<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
			<table width = "100%">
				<tr>
					<td>
						<center><label>Proposal Number</label></center>
					</td>
					<td>
						<center><select name = "transfer_proposal" id = "transfer_proposal" class = "form-control">
							<?php
							if(!empty($shop_order['proposal']))
							{
								print '<option value = "'.$shop_order['proposal'].'" selected = "selected">'.$shop_order['proposal'].'</option>';
							}
							print '<option value = "">-Select-</option>';

							$proposals = $vujade->get_proposals_for_project($project_id);
							if($proposals['error']=="0")
							{
								unset($proposals['error']);
								unset($proposals['next_id']);
								foreach($proposals as $proposal)
								{
									if(!empty($proposal['proposal_id']))
									{
										print '<option value = "'.$proposal['proposal_id'].'">'.$proposal['proposal_id'].'</option>';
									}
								}
							}
							?>
						</select></center>	
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2"><center><label>OR</label></center></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2">
						<center>
							<a href = "#" id = "use-scope-of-work" class = "btn btn-success">SCOPE OF WORK</a>
						</center>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>

			</table>

			<input type = "hidden" id = "scope_of_work" name = "scope_of_work" value = "0">

			<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
			
			<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please choose a proposal number.</div>
			<div id = "error_2" style = "display:none;" class = "alert alert-danger">This proposal number is invalid.</div>

			<div class = "" style = "width:100%;margin-top:15px;text-align:center;">
				<a id = "transfer_btn" href = "#" class = "btn btn-lg btn-success" style = "margin-right:15px;">SAVE</a><a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
			</div>
		</div>

	</div>
</div>

<!-- line items -->	
<div style="margin-bottom: 20px; overflow: hidden">
<div style="width: 100%; overflow: hidden;">
<div class="panel panel-primary">
<div class="panel-heading" style="padding: 0 11px">
    <div class="row">
        <div class="col-lg-9">Description</div>
        <div class="col-lg-3">Amount</div>
    </div>
</div>
<div class="panel-body">
<div class="row">
<div class="col-lg-12">
    <!-- header row -->
    <!-- any existing items will be displayed here -->
    <?php
    $st = 0;
    $next_sort=1;
    $current_sorts = array();
    $tax_total=0;
    $tax_line=0;
    $li_count=0;
    if($proposal_items['error'] == "0") 
    {
        unset($proposal_items['error']);
        print '<table width = "100%">';
        foreach ($proposal_items as $item) 
        {
        	$next_sort++;
        	$li_count++;
            $st += $item['amount'];
            $current_sorts[]=$item['sort'];
            ?>
            <div class="row">
                <div>
                    <div class="ta_container col-lg-9 col-xs-8">
                        <?php
                        $rand = mt_rand();
                        ?>
                        
                        <textarea style="margin-bottom: 15px; width: 100%" class="ckeditor ars existing-line-item form-control" id="desc-<?php print $item['database_id']; ?>" name="desc-<?php print $item['database_id']; ?>"><?php print $item['item'];?></textarea>   
                                            
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <input type="hidden" class="rand" value="<?php print $rand; ?>">
                        <input type="text" class="dollar-amount amount form-control" value="<?php print $item['amount']; ?>"
                               name="amount-<?php print $item['database_id']; ?>"
                               id="amount-<?php print $item['database_id']; ?>" data-dbid="<?php print $item['database_id']; ?>">

                        Tax Type: 
	                    <select class="tt-select tax form-control" name="tax-<?php print $item['database_id']; ?>" id="tax-<?php print $item['database_id']; ?>">
	                     	<?php
	                     	if(!empty($item['tax_label_name']))
	                     	{
	                     		print '<option value = "'.$item['tax_label_name'].'^'.$item['tax_label_rate'].'">'.$item['tax_label_name'].'</option>';
	                     	}
	                     	print '<option value = "">-Select-</option>';
	                     	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
	                     	?>
	                    </select> 
	                    Tax Amount: $

	                    <?php
	                    if(!empty($item['tax_label_rate']))
                     	{
                     		$tax_line=$item['amount']*$item['tax_label_rate']*$tax_rate;
                     		print @number_format($tax_line,2);
                     	}
                     	else
                     	{
                     		print '0.00';
                     	}
                     	$tax_total+=$tax_line;
                     	$tax_line=0;
	                    ?>

                    </div>

					<div class="col-lg-1 col-xs-12">
					<a href = "project_shop_order_edit.php?project_id=<?php print $id; ?>&action=4&after_id=<?php print $item['database_id']; ?>&position=<?php print $item['sort']; ?>" id = "" class = "add-new-row btn btn-xs btn-success" title = "Add New Item After This Item">+</a> 
					<span style="line-height: 39px"><a title = "Delete Item" class="plus-delete btn btn-xs btn-danger" href="#"
					id="<?php print $item['database_id']; ?>">X</a></span>
					<br>
					<input type="hidden" class="sort" value="<?php print $item['sort']; ?>"
                               name="sort-<?php print $item['database_id']; ?>"
                               id="sort-<?php print $item['database_id']; ?>" style = "">
					</div>
				</div>
			</div>
			<br>
        <?php
        }

        if($shop_order['tax_amount']>0)
        {
        	$tax_total=$shop_order['tax_amount'];
        }

        $shop_order_total=$sales_price+$tax_total;

        // highest current sort
        $highest = max($current_sorts);
        if($next_sort<=$highest)
        {
        	$next_sort=$highest+1;
        }
    }
	?>

    <!-- blank input row -->
    <div class="row">
        <div class="col-lg-9 col-xs-8" style="">
            <textarea name="description" id="description" style="margin-bottom: 15px; width: 100%" class="ckeditor ars form-control"></textarea>
        </div>
        <div class="col-lg-2 col-xs-4" style="">
            <input class="dollar-amount form-control" type="text" name="amount" id="amount" style="">
            <span style="margin-right:55px;width:148px;height:35px;">&nbsp;</span>
            <br>
            <input class="" type="hidden" name="sort" id="sort" style="" value = "<?php print $next_sort; ?>">
            Tax Type: 
            <select class="tt-select tax form-control" name="tax_type" id="tax_type">
             	<?php
             	print '<option value = "">-Select-</option>';
             	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
             	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
             	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
             	?>
            </select> 
        </div>
    </div>

</div>
    <div style="margin-bottom: 20px;">
        <div class="row">
            <div class="col-md-12">
                <a class="plus-update btn btn-success btn-xs pull-right" id="update-items" href="#" style="">Update Items</a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<!-- update boxes for selling price, tax rate and tax amount -->
<div class="container">
    <div class="row">
    	<table width = "100%;">

    	<?php
    	if($setup['country']=="Canada")
    	{
    		?>
    		<tr>
	        	<td>
					Sales Price: 
				</td>

				<td>
					<input class="dollar-amount form-control" type="text" id="sales_price" name="sales_price" value="<?php print $sales_price; ?>" style = "width:150px;">
				</td>

				<td colspan = "2"></td>

				<td>
					Tax Amount: 
				</td>

				<td>
					<input type = "text" class = "dollar-amount form-control" id = "tax_amount" disabled name = "tax_amount" value = "<?php print @number_format($tax_total, 2, '.', ','); ?>" style = "width:150px;">
				</td>

				<td colspan = "1"></td>

				<!-- 
				<td>
					<input type = "checkbox" name = "custom_tax_amount" value = "1" id = "custom_tax_amount" <?php //if($custom_tax_amount==1){ print 'checked'; } ?> > Use custom tax amount <a href="#" data-toggle="tooltip" title="Enter your custom tax amount in the box to the left and then check this box to have the system override and save the tax amount to your custom value."><span class = "glyphicon glyphicon-info-sign">&nbsp;</span></a>
				</td>
				-->
			</tr>
    		<?php
    	}
    	else
    	{	
    		?>
	        <tr>
	        	<td>
					Sales Price: 
				</td>

				<td>
					<input class="dollar-amount form-control" type="text" id="sales_price" name="sales_price" value="<?php print $sales_price; ?>" style = "width:150px;">
				</td>

				<td>
					Sales Tax (decimal): 
				</td>

				<td>
					<input class="dollar-amount form-control" type="text" id="tax_rate" name="tax_rate" value="<?php print $tax_rate; ?>" style = "width:150px;">
				</td>

				<td>
					Tax Amount: 
				</td>

				<td>
					<input type = "text" class = "dollar-amount form-control" id = "tax_amount" name = "tax_amount" value = "<?php print @number_format($tax_total, 2, '.', ','); ?>" style = "width:150px;">
				</td>

				<td>
					<input type = "checkbox" name = "custom_tax_amount" value = "1" id = "custom_tax_amount" <?php if($custom_tax_amount==1){ print 'checked'; } ?> > Use custom tax amount <a href="#" data-toggle="tooltip" title="Enter your custom tax amount in the box to the left and then check this box to have the system override and save the tax amount to your custom value."><span class = "glyphicon glyphicon-info-sign">&nbsp;</span></a>
				</td>
			</tr>
		<?php } ?>
		<tr>
			<td colspan = "6" style = "">&nbsp;
			</td>
			<td>
				<h3>Grand Total: $<?php print number_format($shop_order_total,2); ?></h3>
			</td>
		</tr>

		<tr>
			<td>
				<a href="#" id="recalculate" class="btn btn-xs btn-primary">Recalculate</a>
			</td>
			<td colspan = "6" style = "">&nbsp;
			</td>
		</tr>

	</table>
    </div>
</div>

<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>

</div>

<p style = "height:15px;">&nbsp;</p>

<!-- done button -->
<div class = "container" style = "margin-left:15px;">
	<div class = "row">

		<a href = "#" id = "save-2" class = "btn btn-success">SAVE AND COMPLETE</a>
		</form>

	</div>
</div>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

<div style = "" id = "hidden_desc2">
</div>

</div>

</form>
</div>

</div>


</div>

</section>
<!-- End: Content -->

</section>

</div>

<!-- modal for tax type selection errors -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id = "tax_type_error_modal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style = "padding:15px;">
    <h1>Warning</h1>

    <p id = "js-values"></p>

	<p>
		<a id = "review" class="popup-modal-dismiss btn btn-lg btn-success" href="#">Review</a>
		<a id = "proceed" class="btn btn-lg btn-primary" href="#">Proceed as is</a>
	</p>
    </div>
  </div>
</div>

<style>
/* The side navigation menu */
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 0; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111; /* Black*/
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>
<div id="print_preview" class="sidenav">
  	<a href="#" class="closebtn" id="close_preview" style = "margin-top:50px;clear:both;">X</a><br>
  	<div id = "preview_container"></div>
</div>

<!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // print preview
	$('#preview').click(function(e)
	{
		e.preventDefault();
		$('#topbar').hide();
		$('#print_preview').css('width','100%');
		$('#preview_container').html('');

		// load as iframe
		var id = "<?php print $project_id; ?>";
		var iframesrc = "print_shop_order.php?project_id="+id;
		var iframe = '<iframe width = "1000" height="1200" src="'+iframesrc+'"></iframe>';
		$('#preview_container').html(iframe);
        //$('#preview-modal').modal('show');
	});
	
	// close preview
	$('#close_preview').click(function(e)
	{
		e.preventDefault();
		$('#preview-container').html('');
        //$('#preview-modal').modal('hide');
        $('#print_preview').css('width','0');
        $('#topbar').show();
	});

    var n = $('#notes').html();
    $("#notes").html($.trim(n));
    var d = $('#description').html();
    $("#description").html($.trim(d));

    $('.dp').datepicker();

	// change in tax rate = reset tax amount box
	$('#tax_rate').on('input',function()
  	{
        //$('#tax_amount').val(0);
    });

    // change in the tax amount box = reset tax rate box
    $('#tax_amount').on('input',function()
    {
        //$('#tax_rate').val(0);
    });

    // auto resize text areas to fit the height of proposal item text
    $('.existing-line-item').each(function()
    {
    	$(this).height( $(this)[0].scrollHeight );
    });

    $('.existing-line-item').change(function()
    {
    	$(this).height( $(this)[0].scrollHeight );
    });

    // delete an item
    $('.plus-delete').click(function(e)
    {
        e.preventDefault();
        $('#tax_amount_box').val(0);
        var item_id = this.id;
        $('<input>').attr({
            type: 'hidden',
            id: 'item_id',
            name: 'item_id',
            value: item_id
        }).appendTo('#np');
        $('#action').val('3');
        $('#np').submit();
    });

    // update items button
    $('#update-items').click(function(e)
    {
        e.preventDefault();

        var error_count=0;

        // loop through every tax amount box
        // if something is in this box, check the tax type
        // if tax type is empty increase error count
        $('.amount').each(function()
        {
        	var v=$(this).val();
        	if( (v!=0) && (v!='') )
        	{
        		var dbid = $(this).data('dbid');
        		var sel = $('#tax-'+dbid).val();
        		if(sel=='')
        		{
        			error_count++;
        			$('#tax-'+dbid).css('border','1px solid red');
        		}
        	}
        });

        // default blank row
        var da = $('#amount').val();
        if( (da!=0) && (da!='') )
        {
        	var ta = $('#tax_type').val();
        	if(ta=='')
        	{
        		error_count++;
        		$('#tax_type').css('border','1px solid red');
        	}
        }

        if(error_count>0)
        {
        	var error_msg='One or more line items was not assigned a tax rate. Do you wish to proceed anyway?';

			//tax_type_error_modal
			$('#js-values').html(error_msg);
			$('#tax_type_error_modal').modal('show');
			return false;
        }

        $('#action').val('2');
        $('#np').submit();

    });

	// proceed button was pressed
    $('#proceed').click(function(e)
    {
        e.preventDefault();

        // submit form
        $('#action').val('2');
        $('#np').submit();
    });

    // review button 
    $('#review').click(function(e)
    {
        e.preventDefault();
        $('#tax_type_error_modal').modal('hide');
    });

    $('#save-2').click(function(e)
    {
        e.preventDefault();
        $('#action').val('7');
        $('#np').submit();
    });

    // recalculate button
    $('#recalculate').click(function(e)
    {
        e.preventDefault();
		$('#action').val('5');
		$('#np').submit();
    });

    // transfer button : show modal
    $('#transfer').magnificPopup(
    {
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,
		callbacks: 
		{
		    open: function() 
		    {
		    	// modal is opened; 
		    	// set the option list to select no value
		    	// set the button light gray
		    	$('#scope_of_work').val(0);
				$('#transfer_proposal').val('');
				$('#use-scope-of-work').css('opacity','.33');
		    },
		    close: function() 
		    {
		      // Will fire when popup is closed
		    }
		}		
	});

    // user pressed scope of work button
    // set option list to no value
	// remove the opacity of this button
	$('#use-scope-of-work').click(function(e)
	{
		e.preventDefault();
		$('#scope_of_work').val(1);
		$('#transfer_proposal').val('');
		$('#use-scope-of-work').css('opacity','1');
	});

	// user changed the proposal number in the modal to 
	// something other than no value
	// set the opacity fo the use scope of work button to 33%
	$('#transfer_proposal').change(function()
	{
		var pid = $('#transfer_proposal').val();
		if(pid!='')
		{
			$('#scope_of_work').val(0);
			$('#use-scope-of-work').css('opacity','.33');
		}
	});

    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();

    	$('#error_1').hide();

    	var sow = $('#scope_of_work').val();

    	// use scope of work
    	if(sow==1)
    	{
    		
    		// get by ajax
    		//$.post( "jq.get_project_data.php", { id: "<?php print $project_id; ?>", column: "description" })
			//  .done(function(r) 
			//  {
			//  	alert(r);
			  	//CKEDITOR.instances['description'].setData(r);
			//  });
			$('#action').val('8');
			$('#np').submit();
    	}
    	else
    	{
    		// user selected a proposal
    		var pid = $('#transfer_proposal').val();
    		//alert(pid);
    		if(pid=='')
    		{
    			$('#error_1').show();
    			return false;
    		}
    		else
    		{
    			//$('#proposal').val(pid).change();
    			$('#proposal').remove();
    			$('<input>').attr({
		            type: 'hidden',
		            id: 'proposal',
		            name: 'proposal',
		            value: pid
		        }).appendTo('#np');
    			$('#action').val('6');
				$('#np').submit();
    		}
    	}

    	// close the modal
    	$.magnificPopup.close();

    	$('#scope_of_work').val(0);

    });

    // modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    // change proposal
    $('#proposal').change(function()
    {
		$('#action').val('6');
		$('#np').submit();
    });

    // dollar amount formatting
    $('.dollar-amount').keyup(function() 
    { 
	    this.value = this.value.replace(/[^0-9\.]/g,'');
	});

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
