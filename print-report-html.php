<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$type = $_REQUEST['type'];
$sort=$_REQUEST['sort1'];
$salesperson=$_REQUEST['sort2'];
$status=$_REQUEST['sort3'];
if( ($type==16) || ($type==17) )
{
	$options=array('sort'=>$sort, 'salesperson'=>$salesperson, 'status'=>$status);
	$data = $vujade->get_projects_by_status($type,$options);
}
else
{
	$data = $vujade->get_projects_by_status($type);
}

$title = "Report: ".$data['type'];

$pateHTML = '<html><head>';
$pateHTML.='<style>';
$pateHTML.='
body
{
	font-size:12px;
	font-family:arial;
	color:black;
}
.clickableRow
{
	border-bottom:1px solid #cecece;
	padding:3px;
}
';
$pateHTML.='</style>';
$pateHTML.='</head><body>';
$pateHTML.='
<h2 class="signInText">'.$title.'</h2>
	<table width = "100%">
	<thead>
		<tr style = "border-bottom:1px solid black;">
			<td width = "10%"><strong>Project #</strong></td>
			<td width = "20%"><strong>Name</strong></td>
			<td width = "30%"><strong>Location</strong></td>
			<td width = "20%"><strong>Salesperson</strong></td>
			<td width = "20%"><strong>Status</strong></td>';

			if(in_array($type,array(3,4,5,6,9,15,16)))
			{
				$pateHTML.='<td width = "20%"><strong>Ship Date</strong></td>';
				$pateHTML.='<td width = "20%"><strong>Due Date</strong></td>';
			}

			$pateHTML.='
			</td>
		</tr>
	</thead>
	<tbody>';
		unset($data['error']);
		unset($data['type']);
		foreach($data as $project)
		{
	        $pateHTML.='<tr class="clickableRow">';
	        $pateHTML.='<td valign = "top" style = "">'.$project['project_id'].'</td>';
	        $pateHTML.='<td valign = "top" style = "">'.$project['site'].'</td>';
	        $pateHTML.='<td valign = "top" style = "">'.$project['location'].'</td>';
	        $pateHTML.='<td valign = "top" style = "">'.$project['salesperson'].'</td>';
			$pateHTML.='<td valign = "top" style = "">'.$project['project_status'].'</td>';

			if(in_array($type,array(3,4,5,6,9,15,16)))
			{
				$js = $vujade->get_job_status($project['project_id']);
				$pateHTML.='<td width = "20%">'.$js['shipping_date'].'</td>';
				$pateHTML.='<td width = "20%">'.$js['installation_date'].'</td>';
			}

			$pateHTML.='</tr>';
			//$pateHTML.='<tr><td colspan = "4">&nbsp;</td></tr>';
		}
$pateHTML.='
	</tbody>
	</table>
</div></body></html>';
?>