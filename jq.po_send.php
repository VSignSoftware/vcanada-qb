<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// employee info
$employee=$vujade->get_employee($_SESSION['user_id']);

// the proposal
$id = $_REQUEST['id'];
$po = $vujade->get_purchase_order($id);
// can't be invalid
if($po['error']!="0")
{
	$vujade->errors[]="Invalid purchase order.";
	$vujade->show_errors();
	die;
}

$setup = $vujade->get_setup(1);
if($setup['purchase_order_email_message']=="")
{
	$default_msg = "Please find attached Proposal ".$proposal['proposal_id']." for your review and approval.";
}
else
{
	$default_msg = $setup['purchase_order_email_message'];
}
?>

<!-- form -->
<form id = "form">
	<table class = "project_search_results" style = "width: 785px;">
		<tr class = "project_search_results">
			<td class = "project_search_results">Send To</td>
			<td class = "project_search_results">
				<input type = "text" name = "send_to" id = "send_to" style = "width:400px;">
			<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
			</td>
		</tr>

		<tr class = "project_search_results">
			<td class = "project_search_results">Subject</td>
			<td class = "project_search_results">
				<input type = "text" name = "subject" id = "subject" value = "<?php print $po['project_id']; ?> Purchase Order" style = "width:400px;">
				<div id = "error_2" style = "display:none;">This field cannot be empty.</div>
			</td>
		</tr>

		<tr class = "project_search_results">
			<td class = "project_search_results">Message</td>
			<td class = "project_search_results">
				<textarea name = "msg" id = "msg" style = "width:400px;height:100px;">
					<?php print $default_msg; ?>
				</textarea><div id = "error_3" style = "display:none;">This field cannot be empty.</div>
			</td>
		</tr>

		<tr class = "project_search_results">
			<td class = "project_search_results">&nbsp;</td>
			<td class = "project_search_results">
				<div id = "working"></div>
				<input type = "submit" id = "send" value = "SEND" class = "sbt200">
			</td>
		</tr>
	</table>
</form>

<!-- jquery to send proposal via ajax -->
<script src="js/jquery1.11.0.js"></script>
<script>
$(function() 
{

	var msg = $('#msg').html();
    $("#msg").html($.trim(msg));

    $('#send').click(function(e)
    {
    	e.preventDefault();
	
		// send to, subject and message must not be blank
		var send_to = $('#send_to').val();
		var subject = $('#subject').val();
		var message = $('#msg').val();
		var error = 0;
		if(send_to=="")
		{	
			$('#error_1').css('background-color','#C60F13');
			$('#error_1').css('color','white');
			$('#error_1').css('font-weight','bold');
			$('#error_1').css('font-size','16px');
			$('#error_1').css('padding','3px');
			//$('#error_1').css('width','200px');
			//$('#error_1').css('float','left');
			$('#error_1').show();
			error++;
		}
		else
		{
			$('#error_1').hide();
		}
		if(subject=="")
		{
			$('#error_2').css('background-color','#C60F13');
			$('#error_2').css('color','white');
			$('#error_2').css('font-weight','bold');
			$('#error_2').css('font-size','16px');
			$('#error_2').css('padding','3px');
			$('#error_2').show();
			error++;
		}
		else
		{
			$('#error_2').hide();
		}
		if(message=="")
		{
			$('#error_3').css('background-color','#C60F13');
			$('#error_3').css('color','white');
			$('#error_3').css('font-weight','bold');
			$('#error_3').css('font-size','16px');
			$('#error_3').css('padding','3px');
			$('#error_3').show();
			error++;
		}
		else
		{
			$('#error_3').hide();
		}

		if(error==0)
		{
			//$('#send').hide();
			$('#working').css('padding','3px');
			$('#working').css('background-color','#E6A728');
			//$('#working').css('background-color','#36D86C');
			$('#working').css('color','white');
			$('#working').css('font-weight','bold');
			$('#working').css('font-size','16px');
			$('#working').html('Working...');
			$.post( "jq.send.php", { id: <?php print $id; ?>, type: "purchase_order", send_to: send_to, subject: subject, message: message })
			.done(function( response ) 
			{
			    $('#working').css('background-color','#36D86C');
			    $('#working').html('');
			    $('#working').html(response);
			});
		}
	});

});

</script>