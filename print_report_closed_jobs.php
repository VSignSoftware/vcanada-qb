<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();
if($setup['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
// buyout / outsource
$buyout_overhead_percentage=$setup['buyout_overhead'];

// labor
$general_overhead_percentage=$setup['general_overhead'];

// materials
$materials_overhead_percentage=$setup['materials_overhead'];

// machines

// date range
if((!empty($_REQUEST['date1'])) && (!empty($_REQUEST['date2'])))
{
	$date1=$_REQUEST['date1'];
	$date2=$_REQUEST['date2'];
	$data = $vujade->get_closed_projects($_REQUEST['date1'],$_REQUEST['date2']);
}
else
{
	$data['error']=1;
	die("Invalid date format. You must select a start and end date in a valid format.");
}

//print $data['sql'];
//die;

// html doc setup
$html = '<html><head><title>Closed Projects Report</title>';
$html.='<style>';
$html.='
body
{
	font-size:12px;
	font-family:arial;
	color:black;
}
table 
{
    border-collapse: collapse;
}
table, td, th 
{
    border: 1px solid #cecece;
}
td
{

}
';
$html.='</style>';
$html.='</head><body>';
$header_1='
<h2>Closed Projects Report - '.$date1.' to '.$date2.'</h2>';

// table setup
if($data['error']=="0")
{
	$html.='
		<table width = "100%">
		<thead>
			<tr>
				<td style="border-bottom:1px solid black;"><strong>Project #</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Name</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Materials</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Labor</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Outsource</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Subcontract</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Overhead</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Total Costs</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Gross MGN</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Sale Price</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Gross M/U</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Comm PD</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Net Profit</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Net M/U</strong></td>
				<td style="border-bottom:1px solid black;"><strong>Salesperson</strong></td>
				</td>
			</tr>
		</thead>
		<tbody>';
			unset($data['error']);
			$current_salesman = $data[0]['salesperson'];

			//print $current_salesman;
			//die;

			// summary totals for each grouping
			$group_materials=0;
			$group_direct_charge=0;
			$group_vendors=0;
			$group_labor=0;
			$group_overhead=0;
			$group_total_costs=0;
			$group_gross_margin=0;
			$group_sale_price=0;
			$group_gross_markup=array();
			$group_commission_paid=0;
			$group_net_profit=0;
			$group_net_markup=array();

			// grand totals
			$g_materials=0;
			$g_direct_charge=0;
			$g_vendors=0;
			$g_labor=0;
			$g_overhead=0;
			$g_total_costs=0;
			$g_gross_margin=0;
			$g_sale_price=0;
			$g_gross_markup=array();
			$g_commission_paid=0;
			$g_net_profit=0;
			$g_net_markup=array();

			// get the last key of the data array
			//end($data);         
			//$last_key = key($data);
			//reset($data);

			$count = count($data);
			$last = $count;
			$x=1;
			foreach($data as $project)
			{
				// check if salesman has changed
				if($project['salesperson']!=$current_salesman)
				{
					// display salesman summary totals 
					$html.= '<tr class="clickableRow" href="'.$link.'">';
			        $html.= '<td valign = "top">&nbsp;</td>';
			        $html.= '<td valign = "top" style = "">&nbsp;</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($group_materials,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($group_labor,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($group_direct_charge,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($group_vendors,2,'.',',').'</td>';
			        
			        $html.= '<td valign = "top" style = "">'.@number_format($group_overhead,2,'.',',').'</td>';
			        $html.= '<td valign = "top" style = "">'.@number_format($group_total_costs,2,'.',',').'</td>';
			        $html.= '<td valign = "top" style = "">'.@number_format($group_gross_margin,2,'.',',').'</td>';
			        $html.= '<td valign = "top" style = "">'.@number_format($group_sale_price,2,'.',',').'</td>';

			        // group gross markup
			        $group_gross_markup=array_sum($group_gross_markup) / count($group_gross_markup);
			        $html.= '<td valign = "top" style = "">'.round($group_gross_markup,2).'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($group_commission_paid,2,'.',',').'</td>';
			        $html.= '<td valign = "top" style = "">'.@number_format($group_net_profit,2,'.',',').'</td>';

			        // group net markup
			        $group_net_markup=array_sum($group_net_markup) / count($group_net_markup);
			        $html.= '<td valign = "top" style = "">'.round($group_net_markup,2).'</td>';

			        $html.= '<td valign = "top" style = "">&nbsp;</td>';
					$html.= '</tr>';

					$html.= '<tr>';
			        $html.= '<td colspan = "15" style = "border-bottom:1px solid black;">&nbsp;</td>';
					$html.= '</tr>';

					/* 
					$html.= '<tr>';
			        $html.= '<td colspan = "15" style = "border-bottom:1px solid black;">Project Salesman: '.$project['salesperson'].' <br>Current Salesman: '.$current_salesman.'</td>';
					$html.= '</tr>';
					*/

					// unset summary totals
					$group_materials=0;
					$group_direct_charge=0;
					$group_vendors=0;
					$group_labor=0;
					$group_overhead=0;
					$group_total_costs=0;
					$group_gross_margin=0;
					$group_sale_price=0;
					$group_gross_markup=array();
					$group_commission_paid=0;
					$group_net_profit=0;
					$group_net_markup=array();

					// reset to next group of projects
					$current_salesman=$project['salesperson'];

					// first project for new salesman
					$shop_order = $vujade->get_shop_order($project['project_id'], 'project_id');
					foreach($shop_order as $k => $v)
					{
						if($k=="is_illuminated")
						{
							$lt = $v;
						}
					}

					$id = $project['project_id'];

					$pos = $vujade->get_costing_purchase_orders($id);
					if($pos['error']=="0")
					{
						$subtotal = 0;
						$total_dp = 0;
						$total_outsource = 0;
						$total_subcontract = 0;
						unset($pos['error']);
						foreach($pos as $po)
						{
							$subtotal+=$po['cost'];
							if($po['type']=="Materials")
							{
								$total_dp+=$po['cost'];
							}
							if($po['type']=="Outsource")
							{
								$total_outsource+=$po['cost'];
							}
							if($po['type']=="Subcontract")
							{
								$total_subcontract+=$po['cost'];
							}
						}
					}

					$inventory_cost = 0;
					$items = $vujade->get_materials_for_costing($id);
					if($items['error']=="0")
					{
						unset($items['error']);
						foreach($items as $i)
						{
							$line = $i['cost']*$i['qty'];
							$inventory_cost += $line;
						}
						$indt = $inventory_cost * $setup['indeterminant'];
						//$inventory_cost += $indt;
					}
					$tm = $total_dp+$inventory_cost+$indt;
					$total_dp=0;
					$inventory_cost=0;
					$indt=0;
					
					# labor
					$raw_labor = 0;
					$labor_rider = 0;
					$total_labor = 0;
					$tcdata = $vujade->get_timecards_for_project($id);
					if($tcdata['error']=="0")
					{
						unset($tcdata['error']);
						foreach($tcdata as $tc)
						{
							# employee 
							$employee_id = $tc['employee_id'];

							# employee's rate
							$current_rate = $vujade->get_employee_current_rate($employee_id);

							# type of work
							$work_type = $tc['type'];

							# rate for type of work
							$rate_data = $vujade->get_labor_rate($work_type,'id');

							# hours worked
							$total_time = 0;
							$st_total = 0;
							$ot_total = 0;
							$dt_total = 0;
							if(!empty($tc['start']))
							{
								$ts = $tc['date'];
								$begints = strtotime($ts.' '.$tc['start']);
								$endts = strtotime($ts.' '.$tc['end']);
								$diff = $endts-$begints;
								$diff = $diff / 60;
								$diff = $diff / 60;
								$line_total=$diff;
								//$st_total+=$diff;
								if($diff<=8)
								{
									$st_total=$diff;
								}
								if( ($diff > 8) && ($diff <= 12) )
								{
									$ot_total = $diff - 8;
									$st_total = 8;
								}

								if($diff > 12)
								{
									$ot_total = 4;
									$st_total = 8;
									$dt_total = $diff - 12;
								}
							}
							else
							{
								$line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
								$st_total+=$tc['standard_time'];
								$ot_total+=$tc['over_time'];
								$dt_total+=$tc['double_time'];

							}
							$total_time+=$line_total;

							# raw labor 
							$st_rate_total = $st_total * $current_rate['rate'];
							$ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
							$dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
							$raw_labor += $st_rate_total + $ot_rate_total + $dt_rate_total;

							# labor rider
							$labor_rider_rate = $rate_data['rate']-$current_rate['rate'];
							$labor_rider += $labor_rider_rate * $total_time;
						}
						$total_labor += $labor_rider + $raw_labor;
					}

					$col1_subtotal = $tm + $total_labor + $total_outsource + $total_subcontract;

					# general overhead
					$general_overhead = 0;
					$general_overhead = $tm + $total_labor;
					$general_overhead = $general_overhead * $general_overhead_percentage;

					// 48.5% of Inventory + Indeterminent + Total Labor Cost
					// $go = $subtotal + $indet + $labor_subtotal;
					// $go = $go * $setup['general_overhead'];

					# buyout overhead
					$buyout_overhead = ($total_outsource + $total_subcontract) * $buyout_overhead_percentage;
					
					# total cost
					$total_cost = 0;
					$total_cost = $col1_subtotal + $general_overhead + $buyout_overhead;

					# base sale, additional billing, permit, permit acquisition, totla sale price
					$base_sale = 0;
					$additional_billing = 0;
					$permit = 0;
					$permit_acquisition = 0;
					$total_sale_price = 0;
					$bsa=array();
					// Additional billing = Engineering + Change Orders + Shipping - discount. 
					$project_invoices = $vujade->get_invoices_for_project($id);
					if($project_invoices['error']==0)
					{
						unset($project_invoices['error']);
						foreach($project_invoices as $pi)
						{
							if($pi['costing']==1)
							{
								$invoiceid=$pi['database_id'];
								
								$firstinvoice=$pi;

								// not legacy
								if($pi['is_legacy']==0)
								{
									$st=0;

									// invoice types
									// 1: illuminated and lump sum
									if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==1) )
									{
										// labor
										$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// total
										//$st = $labor+$parts; 

										//$parts=0;

										// selling price text
										$selling_price_text = "Selling Price";
									}

									// 2: illuminated sign parts and labor
									if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==2) )
									{
										$show_parts=true;

										// labor
										$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// selling price text
										$selling_price_text = "Labor Only Price";
									}

									// 3: non-illuminated lump sum
									if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==1) )
									{
										$show_parts=false;

										$st = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

										$parts=0;

										// selling price text
										$selling_price_text = "Selling Price";
									}

									// 4: non-illuminated sign parts and labor
									if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==2) )
									{
										$show_parts=true;

										// labor
										$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// selling price text
										$selling_price_text = "Labor Only Price";
									}

									// 5: service with parts and labor
									if( ($firstinvoice['type_1']==3) && ($firstinvoice['type_2']==2) )
									{
										$show_parts=true;

										// labor
										$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// selling price text
										$selling_price_text = "Labor Only Price";
									}
								
									// payments
									$payments = $vujade->get_invoice_payments($invoiceid);
									if($payments['error']=="0")
									{
										unset($payments['error']);
										$pt = 0;
										foreach($payments as $payment)
										{
											$pt+=$payment['amount'];
										}
									}
									else
									{
										$pt=0;
									}

									// tax amount
									// this will be changing in the future
									//$tax_amount += $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
									//$st+=$tax_amount;

									$tax_amount=$pi['sales_tax'];

									$tax_rate=$pi['tax_rate'];

									// change orders
									$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

									// non-taxable co
									$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);
									$co+=$ntco;

									// engineering
									$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

									// permits
									$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

									// permit acq
									$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

									// shipping
									$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

									// discount
									//$discount+=$pi['discount'];
									//$discount = str_replace("-","",$discount);

									$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

									// retention
									$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
									$retention=str_replace("-","",$retention);

									// deposit
									$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
									//$deposit = str_replace("-","",$deposit);

									$base_sale += $st;
									$base_sale += $parts;
									$base_sale += $labor;
									$bsa[]=$st;
									$bsa[]=$parts;
									$bsa[]=$labor;
									$ab = $engineering+$co+$shipping;
									//$ab=$ab+$discount;
									$additional_billing += $ab;
									$permit += $permits;
									$permit_acquisition += $pa;

									$st=0;
									$labor=0;
									$parts = 0;
								}
								else
								{
									// legacy
									$st=0;

									// legacy invoices will have: manufacture & install, labor, parts

									// check for manufacture and install
									$manufacture =  $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

									// labor
									$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

									// parts 
									$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

									// payments
									$payments = $vujade->get_invoice_payments($invoiceid);
									if($payments['error']=="0")
									{
										unset($payments['error']);
										$pt = 0;
										foreach($payments as $payment)
										{
											$pt+=$payment['amount'];
										}
									}
									else
									{
										$pt=0;
									}

									// tax amount
									$tax_amount=$firstinvoice['sales_tax'];

									$tax_rate=$pi['tax_rate'];

									// change orders
									$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

									// engineering
									$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

									// permits
									$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

									// permit acq
									$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

									// shipping
									$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

									// discount
									$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

									// retention
									$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
									$retention=str_replace("-","",$retention);

									// deposit
									$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
									$deposit = str_replace("-","",$deposit);

									$base_sale += $manufacture;
									$base_sale += $labor;
									$base_sale += $parts;
									$bsa[]=$manufacture;
									$bsa[]=$labor;
									$bsa[]=$parts;
									$ab = $engineering+$co+$shipping;
									//$ab=$ab+$discount;
									$additional_billing += $ab;
									$permit += $permits;
									$permit_acquisition += $pa;

									$manufacture=0;
									$labor=0;
									$parts=0;
								}
							}
						}

						//print $project['project_id'].'<br>';
						//print 'tsp: '.$total_sale_price.'<br>';
						foreach($bsa as $bamnt)
						{
							$total_sale_price+=$bamnt;
							//print 'amounts: '.$bamnt.'<br>';
						}

						//print 'add bill: '.$ab.'<br>';
						//print 'permits: '.$permits.'<br>';
						//print 'pa: '.$pa.'<br>';
						//print 'discount: '.$discount.'<br>';

						unset($bsa);
						$base_sale=0;

						if($discount<0)
						{
							$total_sale_price += $ab+$permits+$pa+$discount;
						}
						else
						{
							$total_sale_price += $ab+$permits+$pa-$discount;
							$discount = 0-$discount;
						}
						$net_profit = $total_sale_price - $total_cost - $project['commission'];

						// $markup = $total_sale_price/$total_cost;
						//print 'tsp: '.$total_sale_price.'<hr>';

						$markup = $net_profit / $total_sale_price;
						$markup = floor($markup*100)/100;

						//print 'net profit: '.$net_profit.'<br>';
						//print 'tsp: '.$total_sale_price.'<br>';
						//print $markup;
						//print '<hr>';

						$ab=0;
						$permits=0;
						$pa=0;
						$discount=0;
						$engineering=0;
						$co=0;
						$shipping=0;

					}

					$oh = $general_overhead + $buyout_overhead;

					$tc = $tm+$total_outsource+$total_subcontract+$total_labor+$oh;

					$g_m = $total_sale_price-$tc;

					if($tc>0)
					{
						// Gross mark up is Gross margin / Sales price. 
						$g_mu = $g_m / $total_sale_price;
					}
					else
					{
						$g_mu = 0;
					}

			        $html.= '<tr class="clickableRow">';
			        $html.= '<td valign = "top">'.$project['project_id'].'</td>';
			        $html.= '<td valign = "top" style = "">'.$project['site'].'</td>';
			        $html.= '<td valign = "top" style = "">'.@number_format($tm,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($total_labor,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($total_outsource,2,'.',',').'</td>';
			        
			        $html.= '<td valign = "top" style = "">'.@number_format($total_subcontract,2,'.',',').'</td>';
			        
			        $html.= '<td valign = "top" style = "">'.@number_format($oh,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($tc,2,'.',',').'</td>'; 

			        $html.= '<td valign = "top" style = "">'.@number_format($g_m,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($total_sale_price,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.round($g_mu,2).'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($project['commission'],2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($net_profit,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.$markup.'</td>';

			        $html.= '<td valign = "top" style = "">'.$project['salesperson'].'</td>';

					$html.= '</tr>';
					
					// update summary totals
					$group_materials+=$tm;
					$group_direct_charge+=$total_outsource;
					$group_vendors+=$total_subcontract;
					$group_labor+=$total_labor;
					$group_overhead+=$oh;
					$group_total_costs+=$tc;
					$group_gross_margin+=$g_m;
					$group_sale_price+=$total_sale_price;
					$group_gross_markup[]=$g_mu;
					$group_commission_paid+=$project['commission'];
					$group_net_profit+=$net_profit;
					$group_net_markup[]=$markup;

					// update grand totals
					$g_materials+=$tm;
					$g_direct_charge+=$total_outsource;
					$g_vendors+=$total_subcontract;
					$g_labor+=$total_labor;
					$g_overhead+=$oh;
					$g_total_costs+=$tc;
					$g_gross_margin+=$g_m;
					$g_sale_price+=$total_sale_price;
					$g_gross_markup[]=$g_mu;
					$g_commission_paid+=$project['commission'];
					$g_net_profit+=$net_profit;
					$g_net_markup[]=$markup;

					$total_sale_price=0;
					$markup = 0;
					$tm=0;
					$oh=0;
			        $g_m=0;
			        $g_mu=0;
			        $tc=0;
			        $total_labor=0;
			        $total_outsource=0;
			        $total_subcontract=0;
			        $net_profit=0;

				}
				else
				{
					
					$subtotal = 0;
					$total_dp = 0;
					$total_outsource = 0;
					$total_subcontract = 0;

					$id = $project['project_id'];
					$pos = $vujade->get_costing_purchase_orders($id);
					if($pos['error']=="0")
					{
						unset($pos['error']);
						foreach($pos as $po)
						{
							$subtotal+=$po['cost'];
							if($po['type']=="Materials")
							{
								$total_dp+=$po['cost'];
							}
							if($po['type']=="Outsource")
							{
								$total_outsource+=$po['cost'];
							}
							if($po['type']=="Subcontract")
							{
								$total_subcontract+=$po['cost'];
							}
						}
					}

					//print $id.'<br>';
					//print 'total direct purchases: '.$total_dp.'<br>';
					//print 'total outsource: '.$total_outsource.'<br>';
					//print 'total subcontract: '.$total_subcontract.'<br>';
					//print 'ledger total: '.$subtotal.'<br>';

					//$subtotal = 0;
					//$total_dp = 0;
					//$total_outsource = 0;
					//$total_subcontract = 0;

					$inventory_cost = 0;
					$items = $vujade->get_materials_for_costing($id);
					if($items['error']=="0")
					{
						unset($items['error']);
						foreach($items as $i)
						{
							$line = $i['cost']*$i['qty'];
							$inventory_cost = $line+$inventory_cost;
							//print $i['inventory_id'].' '.$i['description'].' '.$i['cost'].' '.$i['qty'].' '.$line.'<br>';
							$line=0;
							unset($i);
						}
						$indt = $inventory_cost * .1;
						//$inventory_cost += $indt;
					}
					$tm = $total_dp+$inventory_cost+$indt;
					$isubtotal=$inventory_cost+$indt;
					$inventory_cost=0;
					//print 'indt: '.$indt.'<br>';
					//print 'inventory total: '.$isubtotal.'<br>';
					//print 'total materials: '.$tm.'<hr>';

					$isubtotal=0;

					$total_dp=0;
					$inventory_cost=0;
					$indt=0;

					# labor
					$raw_labor = 0;
					$labor_rider = 0;
					$total_labor = 0;
					$tcdata = $vujade->get_timecards_for_project($id);
					if($tcdata['error']=="0")
					{
						unset($tcdata['error']);
						foreach($tcdata as $tc)
						{
							# employee 
							$employee_id = $tc['employee_id'];

							# employee's rate
							$current_rate = $vujade->get_employee_current_rate($employee_id);

							# type of work
							$work_type = $tc['type'];

							# rate for type of work
							$rate_data = $vujade->get_labor_rate($work_type,'id');

							# hours worked
							$total_time = 0;
							$st_total = 0;
							$ot_total = 0;
							$dt_total = 0;
							if(!empty($tc['start']))
							{
								$ts = $tc['date'];
								$begints = strtotime($ts.' '.$tc['start']);
								$endts = strtotime($ts.' '.$tc['end']);
								$diff = $endts-$begints;
								$diff = $diff / 60;
								$diff = $diff / 60;
								$line_total=$diff;
								//$st_total+=$diff;
								if($diff<=8)
								{
									$st_total=$diff;
								}
								if( ($diff > 8) && ($diff <= 12) )
								{
									$ot_total = $diff - 8;
									$st_total = 8;
								}

								if($diff > 12)
								{
									$ot_total = 4;
									$st_total = 8;
									$dt_total = $diff - 12;
								}
							}
							else
							{
								$line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
								$st_total+=$tc['standard_time'];
								$ot_total+=$tc['over_time'];
								$dt_total+=$tc['double_time'];

							}
							$total_time+=$line_total;
							$line_total=0;

							# raw labor 
							$st_rate_total = $st_total * $current_rate['rate'];
							$ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
							$dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
							$raw_labor += $st_rate_total + $ot_rate_total + $dt_rate_total;

							# labor rider
							$labor_rider_rate = $rate_data['rate']-$current_rate['rate'];
							$labor_rider += $labor_rider_rate * $total_time;
						}
						$total_labor += $labor_rider + $raw_labor;
					}

					$col1_subtotal = $tm + $total_labor + $total_outsource + $total_subcontract;

					# general overhead
					$general_overhead = 0;
					$general_overhead = $tm + $total_labor;
					$general_overhead = $general_overhead * $general_overhead_percentage;

					// 48.5% of Inventory + Indeterminent + Total Labor Cost
					// $go = $subtotal + $indet + $labor_subtotal;
					// $go = $go * $setup['general_overhead'];

					# buyout overhead
					$buyout_overhead = ($total_outsource + $total_subcontract) * $buyout_overhead_percentage;
					
					# total cost
					$total_cost = 0;
					$total_cost = $col1_subtotal + $general_overhead + $buyout_overhead;

					# base sale, additional billing, permit, permit acquisition, totla sale price
					$base_sale = 0;
					$additional_billing = 0;
					$permit = 0;
					$permit_acquisition = 0;
					$total_sale_price = 0;
					$bsa=array();
					// Additional billing = Engineering + Change Orders + Shipping - discount. 
					$project_invoices = $vujade->get_invoices_for_project($id);
					if($project_invoices['error']==0)
					{
						unset($project_invoices['error']);

						foreach($project_invoices as $pi)
						{
							if($pi['costing']==1)
							{
								$invoiceid=$pi['database_id'];
								
								$firstinvoice=$pi;

								// not legacy
								if($pi['is_legacy']==1)
								{
									$st=0;

									// invoice types
									// 1: illuminated and lump sum
									if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==1) )
									{
										// labor
										$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// total
										$st = $labor+$parts; 

										$parts=0;

										// selling price text
										$selling_price_text = "Selling Price";
									}

									// 2: illuminated sign parts and labor
									if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==2) )
									{
										$show_parts=true;

										// labor
										$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// selling price text
										$selling_price_text = "Labor Only Price";
									}

									// 3: non-illuminated lump sum
									if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==1) )
									{
										$show_parts=false;

										$st = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

										$parts=0;

										// selling price text
										$selling_price_text = "Selling Price";
									}

									// 4: non-illuminated sign parts and labor
									if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==2) )
									{
										$show_parts=true;

										// labor
										$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// selling price text
										$selling_price_text = "Labor Only Price";
									}

									// 5: service with parts and labor
									if( ($firstinvoice['type_1']==3) && ($firstinvoice['type_2']==2) )
									{
										$show_parts=true;

										// labor
										$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

										// parts 
										$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

										// selling price text
										$selling_price_text = "Labor Only Price";
									}
								
									// payments
									$payments = $vujade->get_invoice_payments($invoiceid);
									if($payments['error']=="0")
									{
										unset($payments['error']);
										$pt = 0;
										foreach($payments as $payment)
										{
											$pt+=$payment['amount'];
										}
									}
									else
									{
										$pt=0;
									}

									// tax amount
									// this will be changing in the future
									//$tax_amount += $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
									//$st+=$tax_amount;

									$tax_amount=$pi['sales_tax'];

									$tax_rate=$pi['tax_rate'];

									// change orders
									$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

									// non-taxable co
									$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);
									$co+=$ntco;

									// engineering
									$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

									// permits
									$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

									// permit acq
									$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

									// shipping
									$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

									// discount
									//$discount+=$pi['discount'];
									//$discount = str_replace("-","",$discount);

									$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

									// retention
									$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
									$retention=str_replace("-","",$retention);

									// deposit
									$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
									//$deposit = str_replace("-","",$deposit);

									$base_sale += $st;
									$base_sale += $parts;
									$bsa[]=$st;
									$bsa[]=$parts;
									$ab = $engineering+$co+$shipping;
									//$ab=$ab+$discount;
									$additional_billing += $ab;
									$permit += $permits;
									$permit_acquisition += $pa;
								}
								else
								{
									// not legacy

									// payments
									$payments = $vujade->get_invoice_payments($invoiceid);
									if($payments['error']=="0")
									{
										unset($payments['error']);
										$pt = 0;
										foreach($payments as $payment)
										{
											$pt+=$payment['amount'];
										}
									}
									else
									{
										$pt=0;
									}

									// tax amount
									$tax_amount=$pi['v_sales_tax'];

									$tax_rate=$pi['v_tax_rate'];

									// i4 amount
									$i4_amount+=$pi['i4_amount'];

									// engineering
									$engineering += $pi['v_engineering'];

									// permits
									$permits += $pi['v_permits'];

									// permit acq
									$pa += $pi['v_permit_acquisition'];

									// shipping
									$shipping += $pi['v_shipping'];

									// discount
									$discount += $pi['v_discount'];

									// retention
									$retention += $pi['v_retention'];
									$retention=str_replace("-","",$retention);

									// deposit
									$deposit += $pi['v_deposit'];
									$deposit = str_replace("-","",$deposit);

									$base_sale += $pi['v_sales_price'];
									$bsa[]=$pi['v_sales_price'];
									$ab = $engineering+$co+$shipping;
									$additional_billing += $ab;
									$permit += $permits;
									$permit_acquisition += $pa;
								}
							}
						}

						//print $project['project_id'].'<br>';
						//print 'tsp: '.$total_sale_price.'<br>';
						foreach($bsa as $bamnt)
						{
							$total_sale_price+=$bamnt;
							//print 'amounts: '.$bamnt.'<br>';
						}

						//print 'add bill: '.$ab.'<br>';
						//print 'permits: '.$permits.'<br>';
						//print 'pa: '.$pa.'<br>';
						//print 'discount: '.$discount.'<br>';

						unset($bsa);
						$base_sale=0;

						if($discount<0)
						{
							$total_sale_price += $ab+$permits+$pa+$discount;
						}
						else
						{
							$total_sale_price += $ab+$permits+$pa-$discount;
							$discount = 0-$discount;
						}
						$net_profit = $total_sale_price - $total_cost - $project['commission'];

						// $markup = $total_sale_price/$total_cost;
						//print 'tsp: '.$total_sale_price.'<hr>';
						if($total_sale_price>0)
						{
							$markup = $net_profit / $total_sale_price;
							$markup = floor($markup*100)/100;
						}
						else
						{
							$markup=0;
						}

						//print 'net profit: '.$net_profit.'<br>';
						//print 'tsp: '.$total_sale_price.'<br>';
						//print $markup;
						//print '<hr>';

						$ab=0;
						$permits=0;
						$pa=0;
						$discount=0;
						$engineering=0;
						$co=0;
						$shipping=0;

					}

					$oh=0;
					$tc=0;
					$gm=0; 

					$oh = $general_overhead + $buyout_overhead;
					$tc = $tm+$total_outsource+$total_subcontract+$total_labor+$oh;
					$g_m = $total_sale_price-$tc;
					if( ($tc>0) && ($total_sale_price>0) )
					{
						// Gross mark up is Gross margin / Sales price. 
						$g_mu = $g_m / $total_sale_price;
					}
					else
					{
						$g_mu = 0;
					}

					// row output
			        $html.= '<tr class="clickableRow">';
			        $html.= '<td valign = "top">'.$project['project_id'].'</td>';
			        $html.= '<td valign = "top" style = "">'.$project['site'].'</td>';
			        $html.= '<td valign = "top" style = "">'.@number_format($tm,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($total_labor,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($total_outsource,2,'.',',').'</td>';
			        
			        $html.= '<td valign = "top" style = "">'.@number_format($total_subcontract,2,'.',',').'</td>';
			        
			        $html.= '<td valign = "top" style = "">'.@number_format($oh,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($tc,2,'.',',').'</td>'; 

			        $html.= '<td valign = "top" style = "">'.@number_format($g_m,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($total_sale_price,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.round($g_mu,2).'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($project['commission'],2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.@number_format($net_profit,2,'.',',').'</td>';

			        $html.= '<td valign = "top" style = "">'.$markup.'</td>';

			        $html.= '<td valign = "top" style = "">'.$project['salesperson'].'</td>';

					$html.= '</tr>';
					
					// update summary totals
					$group_materials+=$tm;
					$group_direct_charge+=$total_outsource;
					$group_vendors+=$total_subcontract;
					$group_labor+=$total_labor;
					$group_overhead+=$oh;
					$group_total_costs+=$tc;
					$group_gross_margin+=$g_m;
					$group_sale_price+=$total_sale_price;
					$group_gross_markup[]=$g_mu;
					$group_commission_paid+=$project['commission'];
					$group_net_profit+=$net_profit;
					$group_net_markup[]=$markup;

					// last key?
			        //$current_key = key($project);
			        if($x==$last)
			        {
				       	// display salesman summary totals 
						$html.= '<tr class="clickableRow" href="'.$link.'">';
				        $html.= '<td valign = "top">&nbsp;</td>';
				        $html.= '<td valign = "top" style = "">&nbsp;</td>';
				        $html.= '<td valign = "top" style = "">'.@number_format($group_materials,2,'.',',').'</td>';

				        $html.= '<td valign = "top" style = "">'.@number_format($group_labor,2,'.',',').'</td>';

				        $html.= '<td valign = "top" style = "">'.@number_format($group_direct_charge,2,'.',',').'</td>';
				        
				        $html.= '<td valign = "top" style = "">'.@number_format($group_vendors,2,'.',',').'</td>';
				        
				        $html.= '<td valign = "top" style = "">'.@number_format($group_overhead,2,'.',',').'</td>';
				        $html.= '<td valign = "top" style = "">'.@number_format($group_total_costs,2,'.',',').'</td>';
				        $html.= '<td valign = "top" style = "">'.@number_format($group_gross_margin,2,'.',',').'</td>';
				        $html.= '<td valign = "top" style = "">'.@number_format($group_sale_price,2,'.',',').'</td>';

				        // group gross markup
				        $group_gross_markup=array_sum($group_gross_markup) / count($group_gross_markup);
				        $html.= '<td valign = "top" style = "">'.round($group_gross_markup,2).'</td>';

				        $html.= '<td valign = "top" style = "">'.@number_format($group_commission_paid,2,'.',',').'</td>';
				        $html.= '<td valign = "top" style = "">'.@number_format($group_net_profit,2,'.',',').'</td>';

				        // group net markup
				        $group_net_markup=array_sum($group_net_markup) / count($group_net_markup);
				        $html.= '<td valign = "top" style = "">'.round($group_net_markup,2).'</td>';

						$html.= '<tr>';
				        $html.= '<td colspan = "15" style = "border-bottom:1px solid black;">&nbsp;</td>';
						$html.= '</tr>';
					}

					// update grand totals
					$g_materials+=$tm;
					$g_direct_charge+=$total_outsource;
					$g_vendors+=$total_subcontract;
					$g_labor+=$total_labor;
					$g_overhead+=$oh;
					$g_total_costs+=$tc;
					$g_gross_margin+=$g_m;
					$g_sale_price+=$total_sale_price;
					$g_gross_markup[]=$g_mu;
					$g_commission_paid+=$project['commission'];
					$g_net_profit+=$net_profit;
					$g_net_markup[]=$markup;

					$total_sale_price=0;
					$markup = 0;
					$tm=0;
					$oh=0;
			        $g_m=0;
			        $g_mu=0;
			        $tc=0;
			        $total_labor=0;
			        $total_outsource=0;
			        $total_subcontract=0;
			        $net_profit=0;
			        //$group_gross_markup=array();
					//$group_net_markup=array();
				}
				$x++;
			}

			//die;

			$avg_net_markup=array_sum($g_net_markup) / count($g_net_markup);
			$avg_gross_markup=array_sum($g_gross_markup) / count($g_gross_markup);
			$avg_gross_markup=round($avg_gross_markup,2);
			$avg_net_markup=round($avg_net_markup,2);

			// shop orders and tm

			// display grand totals
			/*
			$html.= '<tr>';
	        $html.= '<td colspan = "15" style = "">&nbsp;</td>';
			$html.= '</tr>';
			*/
			$html.= '<tr class="">';
	        $html.= '<td valign = "top">&nbsp;</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">Totals</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_materials,2,'.',',').'</td>';

	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_labor,2,'.',',').'</td>';

	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_direct_charge,2,'.',',').'</td>';
	        
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_vendors,2,'.',',').'</td>';
	        
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_overhead,2,'.',',').'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_total_costs,2,'.',',').'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_gross_margin,2,'.',',').'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_sale_price,2,'.',',').'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.$avg_gross_markup.'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_commission_paid,2,'.',',').'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.@number_format($g_net_profit,2,'.',',').'</td>';
	        $html.= '<td valign = "top" style = "font-weight:bold;">'.$avg_net_markup.'</td>';
	        $html.= '<td valign = "top" style = "">&nbsp;</td>';
			$html.= '</tr>';

			$html.= '<tr>';
	        $html.= '<td colspan = "15" style = "border-bottom:1px solid black;">&nbsp;</td>';
			$html.= '</tr>';
			
			$html.="</tbody></table></body></html>";

$footer_1 = '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';

//die;

# mpdf class (pdf output)
include("mpdf60/mpdf.php");

/*
 string $mode [, 
 mixed $format [, 
 float $default_font_size [, 
 string $default_font [, 
 float $margin_left , 
 float $margin_right , 
 float $margin_top , 
 float $margin_bottom , 
 float $margin_header , 
 float $margin_footer [, 
 string $orientation ]
*/

$mpdf = new mPDF('', 'LETTER-L', 0, 'Helvetica', 10, 10, 20, 10, 10);
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($html);

// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-closed_report.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output('closed_reports.pdf','I'); 
}

// print $html;

}
else
{
	/*
	print 'Post: ';
	print_r($_POST);
	print '<hr>';
	print 'Request: ';
	print_r($_REQUEST);
	print '<hr>';
	print 'data array: ';
	print_r($data);
	print '<hr>';
	*/
	//die('No projects matched your search.');
	?>
	<!DOCTYPE html>
		<html lang="en">
		<head>
		  <title>ERROR</title>
		  <meta charset="utf-8">
		  <meta name="viewport" content="width=device-width, initial-scale=1">
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
		  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		</head>
		<body>

			<div class="container">
			  <div class="alert alert-danger">
			    <strong>ERROR</strong> No projects matched your date range search.
			  </div>
			</div>

		</body>
		</html>
	<?php
}
?>