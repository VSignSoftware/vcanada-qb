<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

if(isset($_POST['img']))
{
	//save file and return the file path
	$data = $_POST['img'];
	$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $data));
	$file_name = 'img_' . rand(1, 100000) . '.png';
	file_put_contents('images/' . $file_name, $data);
	echo $file_name;
	exit();
}
else if(isset($_GET['file']))
{

	$html='';

	# mpdf class (pdf output)
	include("mpdf60/mpdf.php");
	$margin_left="10";
	$margin_right="10";
	$margin_top="10";
	$margin_bottom="5";
	$margin_header="5";
	$margin_footer="5";
	$orientation="";
	$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);

	$html .= '<style>.table{border-collapse: collapse;}</style>';
	
	$html .= '<div class="container">';
	$html .= '<div class="full-wide"><center>';
	$html .= ('<img src="images/' . $_GET['file'] . '" class="pie-chart"/>');	
	$html .= '</center>';

	$html .= '</div>';
	
	$setup = $vujade->get_setup();

	// set to pacific time zone
	date_default_timezone_set('America/Los_Angeles');

	//set request date
	$fiscal_year_search = false;
	if(!isset($_REQUEST['date'])) 
	{
		// start and end of the current month
		$test_date = date('m/d/Y');
		$date = date('m/01/Y');
		$report_t=date('M Y');
	}
	else 
	{
		// user selected month and year or fiscal year
		$test_date = $_REQUEST['date'];
		$date = $_REQUEST['date'];
		
		// fiscal year
		if($test_date[0]=="F") 
		{
			$year_value = $test_date[3].$test_date[4].$test_date[5].$test_date[6];
			$report_t="Fiscal Year ".$year_value;
			$fiscal_year_search=true;
		}
		
		// year and month
		if(!isset($report_t)) 
		{
			//$report_t=$date;
			$td = strtotime($test_date);
			$year_value=date('Y',$td);
			$td = date('M Y',$td);
			$report_t=$td;
		}
	}	

	// get employees of vujade
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$emp=$employee;
	$section=6;

	if(!$fiscal_year_search) 
	{
	    // first day of the month
		$tds = strtotime($test_date);
		$first = date('Y-m-01',$tds);

		// last day of the month

		$last = date('Y-m-t',strtotime($first));
		$start = strtotime($first);
		$end = strtotime($last);

		// debug
		/* 
		print 'first day: '.$first;
		print '<br>';
		print 'last day: '.$last.'<br>';
		print 'start and end ts: ';
		print $start.' '.$end.'<br>';
		die;
		*/
	}
	else 
	{
		// get by fiscal year

		// defaults
		$option_start = 2014;
		$option_current_year = date('Y');

		// fiscal year does not start on Jan. 1
		if($setup['fiscal_year_start']!=1) 
		{	
			// any year in the past 
			if($year_value<$option_current_year) 
			{
				// start
				$y=$year_value;

				// end
				$y2=$year_value+1;
			}

			// current year
			if($year_value==$option_current_year) 
			{
				// start
				$y=$option_current_year-1;

				// end
				$y2=$year_value;
			}

			// next year
			if($year_value>$option_current_year) 
			{
				// start
				$y=$option_current_year;

				// end
				$y2=$year_value;
			}

			$m=$setup['fiscal_year_start'];
			$m2=$setup['fiscal_year_end'];
			$start = strtotime($y.'-'.$m.'-01');
			$date1 = $y.'-'.$m2.'-01'; 
			$last_day = date('t',strtotime($date1));
			$end = strtotime($y2.'-'.$m2.'-'.$last_day);
		}
		else 
		{
			$y=$year_value;
			$y2=$year_value;
			$m=$setup['fiscal_year_start'];
			$m2=$setup['fiscal_year_end'];
			$start = strtotime($y.'-'.$m.'-01');
			$date1 = $y.'-'.$m2.'-01'; 
			$last_day = date('t',strtotime($date1));
			$end = strtotime($y2.'-'.$m2.'-'.$last_day);
		}
	}

	$total_opened = 0;
	$total_processed = 0;
	$total_did_not_sell = 0;
	$total_closed = 0;

	// total projects opened
	$tpo=0;

	// total projects closed
	$tpc=0;

	// total projects processed
	$tpp=0;

	// closed percentage (avg)
	$avgc=array();

	// proposal volume total
	$tpv=0;

	// sales volume total
	$tsv=0;

	$salespeople=$vujade->get_salespeople(1);
	
	if($salespeople['error']=="0") 
	{
		unset($salespeople['error']);
		
		$html .= '<table class="table" width ="100%" border="1">';
		foreach($salespeople as $sp) 
		{			
			$projects = $vujade->get_dashboard_projects($start,$end,$sp['fullname']);

			$processed_jobs = $vujade->get_processed_projects($start,$end,$sp['fullname']);

			//print $processed_jobs['sql'];
			//print '<br>';

			$proposal_data = $vujade->get_proposals_for_volume_report($start,$end,$sp['fullname']);

	        // row 1
			$html .= '<tr>';

			$html .= '<td width = "">';
			$html .= $sp['fullname'];
			$html .= '</td>';

			$html .= '<td width = "">';
			$html .= 'Projects Opened: ';
			$html .= '</td>';

			$html .= '<td>';
			$html .= $projects['count'];
			$tpo += $projects['count'];
			$html .= '</td>';

			$html .= '<td>Proposal Volume: ';
			$html .= '</td>';

			$html .= '<td>';
			$html .= @number_format($proposal_data['proposal_volume'],2);
			$tpv += $proposal_data['proposal_volume'];
			$html .= '</td>';

			$html .= '</tr>';

	        // row 2
			$html .= '<tr>';

			$html .= '<td>&nbsp;';
			$html .= '</td>';

			$html .= '<td>';
			$html .= 'Projects Processed: ';
			$html .= '</td>';

			$html .= '<td>';
			$html .= $processed_jobs['count'];
			$tpp += $processed_jobs['count'];
			$html .= '</td>';

			// sales volume
			$html .= '<td>';
			$html .= 'Sales Volume: ';
			$html .= '</td>';

			$html .= '<td>';
			$rd = $vujade->get_individual_yearly_sales_report($start,$end,$sp['fullname']);

			//print_r($rd);
			//print '<hr>';

			$dv = 0;
			unset($rd['error']);
			unset($rd['sql']);
			foreach($rd as $prd)
			{
				$do=$prd['open_date'];
				$do=strtotime($do);
				if(($do>=$start) && ($do<=$end))
				{
					$lt = str_replace(',','',$prd['line_total']);
					$dv+=$lt;
				}
				unset($lt);
				unset($do);
			}
			unset($rd);
			$html.=@number_format($dv,2);
			$tsv+=$dv;
			$dv=0;
			$html .= '</td>';
			$html .= '</tr>';

	        // row 3
			$html .= '<tr>';

			$html .= '<td>&nbsp;';
			$html .= '</td>';

			$html .= '<td>';
			$html .= 'Closed %: ';
			$html .= '</td>';

			$html .= '<td>';
	        // closed rate = processed jobs / total opened jobs
			if( ($projects['opened_jobs']>0) && ($processed_jobs['count']>0) )
			{
				$number_closed = $processed_jobs['count']/$projects['opened_jobs'];
				$number_closed = round($number_closed,2);
				$number_closed = $number_closed*100;
				$html .= $number_closed;
			}
			else 
			{
				$html .= '0';
			}
			$html .= '</td>';

			$html .= '<td>&nbsp;';
			$html .= '</td>';

			$html .= '<td>&nbsp;';
			$html .= '</td>';

			$html .= '</tr>';

			$html .= '<tr>';
			$html .= '<td colspan = "5">&nbsp;';
			$html .= '</td>';
			$html .= '</tr>';

			// counters
			$total_opened+=$projects['opened_jobs'];
			$total_processed+=$processed_jobs['count'];
			$total_did_not_sell+=$projects['did_not_sell_jobs'];
			$total_opened+=$projects['did_not_sell_jobs'];
			$total_closed+=$projects['has_closed_date'];
		}
		
		if($total_opened>0) 
		{
			$total_closed_rate = @$total_processed/$total_opened;
			$total_closed_rate = round($total_closed_rate,2);
			$total_closed_rate=$total_closed_rate*100;
		}
		else 
		{
			$total_closed_rate=0;
		}
		
		// summary
		$html .= '<tr>';

		$html .= ('<td>Total Opened: '.$tpo);
		$html .= '</td>';

		$html .= '<td>';
		$html .= ('Total Processed: ' . $tpp);
		$html .= '</td>';

		$html .= '<td>';
		$html .= ('Total Closed: '. round( ($tpp/$tpo)*100 ) . '%');
		$html .= '</td>';

		$html .= ('<td>Proposal Volume: $' . number_format($tpv,2) );
		$html .= '</td>';

		$html .= ('<td>Sales Volume: $' . number_format($tsv,2) );
		$html .= '</td>';
		$html .= '</tr>';
		$html .= '</table>';	
	}	
	
	$html .= '</div>';

	//remove the file
	//die;

	$mpdf->WriteHTML($html);
	unlink('images/' . $_GET['file']);
	// download the pdf if phone or tablet
	/*require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) {
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-shop-order.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
		$mpdf->Output($pdfname,'F');
		$html '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}else {
		$mpdf->Output('Shop Order.pdf','I'); 
	}*/
	$mpdf->Output('Shop Order.pdf','I'); 
}