<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
$proposals_permissions = $vujade->get_permission($_SESSION['user_id'],'Proposals');
if($proposals_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
    $vujade->page_redirect('error.php?m=3');
}
$setup = $vujade->get_setup();
$invoice_setup = $vujade->get_invoice_setup();
$proposalid = $_REQUEST['proposalid'];
$proposal_database_id=$proposalid;
$s = array();

// brand new proposals can be deleted from this page
// proposals that have been saved at least once cannot be deleted from here
$is_new=$_REQUEST['is_new'];

$action = 0;
if(isset($_REQUEST['action']))
{
    $action = $_REQUEST['action'];
}

# delete proposal item
if($action==3)
{
    $project_id=$_POST['project_id'];
    $proposal_id=$_POST['proposal_id'];
    $proposal_database_id=$_POST['proposal_database_id'];
    
    $s = array();
    $row_id = $_POST['item_id'];
    $s[]=$vujade->delete_row('proposal_items',$row_id); 
}

# update deposit
if($action==4)
{
    $project_id=$_POST['project_id'];
    $proposal_id=$_POST['proposal_id'];
    $proposal_database_id=$_POST['proposal_database_id'];
    $deposit_rate=$_POST['deposit_rate'];

    $s = array();
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'deposit_rate',$deposit_rate);
}

# done button
if($action==5)
{
    $project_id=$_POST['project_id'];
    $proposal_id=$_POST['proposal_id'];
    $proposal_database_id=$_POST['proposal_database_id'];
    $expiration_date=$_POST['expiration_date'];
    $design=$_POST['design'];
    $terms = trim($_POST['terms']);
    $send_to_name=$_POST['send_to_name'];
    $send_to_phone=$_POST['send_to_phone'];

    $s = array();
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'expiration_date',$expiration_date);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'ts',strtotime('now'));
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'proposal_date',date('m/d/Y'));
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'drawing_numbers',$design);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'terms',$terms);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'send_to_name',$send_to_name);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'send_to_phone',$send_to_phone);

    # redirect
    $vujade->page_redirect('project_proposals.php?id='.$project_id.'&proposalid='.$proposal_database_id);
}

# update items button was pressed
if($action==6)
{
    $project_id=$_POST['project_id'];
    $proposal_id=$_POST['proposal_id'];
    $proposal_database_id=$_POST['proposal_database_id'];

    $s = array();
    $skipped = array('action','project_id','proposal_id','proposalid','proposal_database_id','expiration_date','design','send_to_name','send_to_phone','description','terms','update_amount','update_description','item_id','amount','tax_rate','tax_total','total','deposit_rate','tax_type');
    foreach($_POST as $key => $value)
    {
        if(!in_array($key, $skipped))
        {
        	//print $key.' : '.$value.'<br>';
            # key will either be
            // [desc-150]
            // or
            // [amount-150]
            // or
            // [sort-150]
            // or
            // [tax-.33]
            # determine which to update
            $test_key = explode('-',$key);
            $row_id=$test_key[1];
            if($test_key[0]=="desc")
            {
                $description=$value;
                //$description = str_replace("Â","&nbsp;", $description);
                
                //$description['item'] = str_replace(array("\n", "\r"), array('\n', '\r'), $item['item']);


                //$description=str_replace("\r"," \r",$description);
		        //$description=str_replace('\r',' \r',$description);
		        //$item['item']=str_replace('\r','<br>',$item['item']);

                $s[] = $vujade->update_row('proposal_items',$row_id,'item',$description);
            }
            if($test_key[0]=="amount")
            {
                $amount=str_replace(",", "", $value);
                $s[] = $vujade->update_row('proposal_items',$row_id,'amount',$amount);
            }
            if($test_key[0]=="sort")
            {
                $sort=str_replace(",", "", $value);
                $s[] = $vujade->update_row('proposal_items',$row_id,'sort',$sort);
            }
            if($test_key[0]=="tax")
            {
                $value = explode('^',$value);
                $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name',$value[0]);
                $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate',$value[1]);
            }
        }
    }

    // new one
    $description=$_POST['description'];
    $amount=str_replace(',', '',$_POST['amount']);
    $sort=str_replace(',', '',$_POST['sort']);

    if(!empty($description))
    {

	    $vujade->create_row('proposal_items');
	    $row_id = $vujade->row_id;
	    $s[] = $vujade->update_row('proposal_items',$row_id,'proposal_id',$proposal_id);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'item',$description);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'amount',$amount);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'sort',$sort);

	    $tax_type = explode('^',$_POST['tax_type']);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_name',$tax_type[0]);
	    $s[] = $vujade->update_row('proposal_items',$row_id,'tax_label_rate',$tax_type[1]);
	}

    $project_id=$_POST['project_id'];
    $proposal_id=$_POST['proposal_id'];
    $proposal_database_id=$_POST['proposal_database_id'];
    $expiration_date=$_POST['expiration_date'];
    $design=$_POST['design'];
    $terms = trim($_POST['terms']);
    $send_to_name=$_POST['send_to_name'];
    $send_to_phone=$_POST['send_to_phone'];

    $s = array();
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'expiration_date',$expiration_date);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'ts',strtotime('now'));
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'proposal_date',date('m/d/Y'));
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'drawing_numbers',$design);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'terms',$terms);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'send_to_name',$send_to_name);
    $s[]=$vujade->update_row('proposals',$proposal_database_id,'send_to_phone',$send_to_phone);
    
}

// change tax rate
if($action==9)
{
	$project_id=$_POST['project_id'];
    $proposal_id=$_POST['proposal_id'];
    $proposal_database_id=$_POST['proposal_database_id'];
    $tax_rate=$_POST['tax_rate'];
    $tax_amount=$_POST['tax_amount'];
    $custom_tax_amount=$_POST['custom_tax_amount'];

    if( ($tax_rate>0) && ($tax_rate<1) )
    {
    	# update the proposal
    	$s = array();
    	$s[]=$vujade->update_row('proposals',$proposal_database_id,'tax_rate',$tax_rate);
    }

    // user entered 0
    if( ($tax_rate=="0") || (empty($tax_rate)) )
    {
    	# update the proposal
    	$s = array();
    	$s[]=$vujade->update_row('proposals',$proposal_database_id,'tax_rate','0');
    }

    if($tax_amount>0)
    {
    	# update the proposal
    	if($custom_tax_amount==1)
		{
			$tax_amount=str_replace(',', '', $_POST['tax_amount']);
		}
		else
		{
			// automatically calculate 
			// $tax_amount=$sales_price*$tax_rate;

			$proposal_items = $vujade->get_items_for_proposal($proposal_id);
			$tax_total=0;
		    $tax_line=0;
		    if($proposal_items['error'] == "0") 
		    {
		        unset($proposal_items['error']);
		        foreach($proposal_items as $item) 
		        {
                    if(!empty($item['tax_label_rate']))
                 	{
                 		$tax_line=$item['amount']*$item['tax_label_rate']*$tax_rate;
                 	}
                 	$tax_total+=$tax_line;
                 	$tax_line=0;       
		        }
		        $tax_amount=$tax_total;
		        $tax_total=0;
		   		$tax_line=0;
		    }
		}

    	$s[]=$vujade->update_row('proposals',$proposal_database_id,'tax_total',$tax_amount);
    }
    else
    {
    	# update the proposal
    	$s[]=$vujade->update_row('proposals',$proposal_database_id,'tax_total','');
    }

    $s[]=$vujade->update_row('proposals',$proposal_database_id,'custom_tax_amount',$custom_tax_amount);

    // canada servers
    if($setup['country']=="Canada")
    {
    	# update the proposal
    	$s = array();
    	$s[]=$vujade->update_row('proposals',$proposal_database_id,'tax_rate','1');
    }

}

# get proposal details
$proposal = $vujade->get_proposal($proposalid);
if($proposal['error']!="0")
{
    print 'Proposal could not be found.';
    die;
}
else
{
    $proposal_id=$proposal['proposal_id'];
}

// adds a new blank item in the place where the user clicked the button
// re-sorts the existing line items
if($action==8)
{
	$position=$_REQUEST['position'];
	$after_id = $_REQUEST['after_id'];
	$vujade->update_proposal_items_sort($after_id,$proposal_id,$position);
}

# get all items for this proposal
$proposal_items = $vujade->get_items_for_proposal($proposal_id);

// get send to name and phone if 
if( (!isset($send_to_name)) || (empty($send_to_name)) )
{
    $send_to_name=$project['project_contact'];
}
if( (!isset($send_to_phone)) || (empty($send_to_phone)) )
{
    $send_to_phone=$project['project_contact_phone'];
}

if($action==7)
{
    $project_id=$_POST['project_id'];
    $proposal_db_id=$_POST['proposalid'];
    $proposal_id=$_POST['proposal_id'];

    // delete the proposal
    $s[]=$vujade->delete_row('proposals',$proposal_db_id);

    // delete all the line items
    $s[]=$vujade->delete_row('proposal_items',$proposal_id,1,'proposal_id');

    $vujade->page_redirect('project_proposals.php?id='.$project_id);
}

// tax rate for work location
if($setup['country']=="Canada")
{
	$tax_rate=1;
}
else
{
	$tax_rate=$proposal['tax_rate'];
}

// current terms
$terms1=$proposal['terms'];
// terms option 1 
$terms2=$vujade->get_terms(1);
// terms option 2
$terms3=$vujade->get_terms(7);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$nav = 3;
//$charset="ISO-8859-1";
$title = "Edit Proposal - ";
require_once('h.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Start: Topbar -->
<header id="topbar">
    <div class="topbar-left">
        <ol class="breadcrumb">
            <li class="crumb-active">
                <a href = "#"><?php print $proposal_id; ?></a>
            </li>
        </ol>
    </div>
</header>
<!-- End: Topbar -->
<!-- Begin: Content -->
<section id="content" class="">
<div class="theme-primary">
<div class="panel heading-border panel-primary">
<div class="panel-body bg-light">
<strong><?php print $nextid; ?></strong>
<div class="row">
    <!-- work location -->
    <div class="col-md-6" style = "width:49%;">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 0 8px">
                <span class="panel-title" style="font-size: 13px; font-weight:600">Work Location</span>
            </div>
            <div class="panel-body">
                <p>
                    <?php
                    # get site location for this project
                    print $project['site'] . '<br>';
                    print $project['address_1'] . '<br>';
                    print $project['city'] . ', ';
                    print $project['state'] . ' ';
                    print $project['zip'];
                    ?>
                </p>
            </div>
        </div>
    </div>
    <!-- billing info box -->
    <div class="col-md-6" style = "width:49%;">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 0 8px">
                <span class="panel-title" style="font-size: 13px; font-weight:600">Billing Information</span>
            </div>
            <div class="panel-body">
                <p>
                    <?php
                    # get billing info for this project
                    //print $project['client_id'].'<br>';
                    if($setup['is_qb']==1)
                    {
                    	$customer1 = $vujade->get_customer($project['client_id'], 'ListID');
                    	if($customer1['error']=="0")
                    	{
                    		$customer=$customer1;
                    	}
                    	$customer2 = $vujade->get_customer($project['client_id'], 'ID');
                    	if($customer2['error']=="0")
                    	{
                    		$customer=$customer2;
                    	}
                    }
                    else
                    {
                    	$customer = $vujade->get_customer($project['client_id']);
                    }
                    // print_r($customer);
                    $customer_contact = $vujade->get_contact($project['client_contact_id']);
                    if($customer['error'] == "0") 
                    {
                        print $customer['name'] . '<br>';
                        if ($customer_contact['error'] == "0") {
                            print $customer_contact['full_name'] . '<br>';
                        }
                        print $customer['address_1'] . '<br>';
                        if (!empty($customer['address_2'])) {
                            print $customer['address_2'] . '<br>';
                        }
                        print $customer['city'] . ', ' . $customer['state'] . ' ' . $customer['zip'] . '<br>';
                    }
                    ?>
                </p>
            </div>
        </div>
    </div>
</div>
<form method = "post" action = "edit_proposal.php" id = "np">
<input type = "hidden" id = "action" value = "" name = "action">
<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
<input type = "hidden" name = "proposal_id" value = "<?php print $proposal_id; ?>">
<input type = "hidden" name = "proposalid" value = "<?php print $proposal_database_id; ?>">
<input type = "hidden" name = "proposal_database_id" value = "<?php print $proposal_database_id; ?>">
<div style="margin-bottom: 20px; overflow: hidden;">
<div class="row">
        <div class="col-lg-4 col-sm-6 col-lg-offset-0 col-sm-offset-0">
            <div class="row">
                <div class="col-lg-4">
                    <span style="line-height: 39px">Project #:</span>
                </div>
                <div class="col-lg-8">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px"><?php print $project_id; ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Salesperson:</span>
                </div>
                <div class="col-lg-8">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px"><?php print $project['salesperson']; ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Date:</span>
                </div>
                <div class="col-lg-8">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px"><?php print $proposal['proposal_date']; ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Expiration Date:</span>
                </div>
                <div class="col-lg-8">
                    <input type="text" class="form-control dp" name="expiration_date" id="expiration_date"
                           value="<?php print $proposal['expiration_date']; ?>">
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-sm-6 col-lg-offset-4 col-sm-offset-0">
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Drawing #s:</span>
                </div>
                <div class="col-lg-8">
                    <select name = "design" class = "form-control" style = "width: 100%;margin-bottom:5px;">
                        <?php
                        if(!empty($proposal['drawing_numbers']))
                        {
                            print '<option value = "'.$proposal['drawing_numbers'].'" selected = "selected">'.$proposal['drawing_numbers'].'</option>';
                        }
                        print '<option value = "">-Select-</option>';
                        $designs = $vujade->get_designs_for_project($project_id);
                        if($designs['error']=="0")
                        {
                            unset($designs['error']);
                            unset($designs['next_id']);
                            foreach($designs as $design)
                            {
                                print '<option value = "'.$design['design_id'].'">'.$design['design_id'].'</option>';
                            }
                        }
                        ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Send To:</span>
                </div>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name="send_to_name"
                           value="<?php if (!empty($proposal['send_to_name'])) {print $proposal['send_to_name'];}else{print $project['project_contact']; } ?>">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Phone:</span>
                </div>
                <div class="col-lg-8">
                    <input type="text" name="send_to_phone"
                           value="<?php if (!empty($proposal['send_to_phone'])) {print $proposal['send_to_phone']; }else{print $project['project_contact_phone']; } ?>"
                           class="form-control" style="">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <a href="#" id="ps" class="btn btn-xs btn-primary" style="float:right;text-align:center;width:216px;margin-left:5px;">Paste Scope Into New Line Item</a> 
                    <a class = "btn btn-xs btn-primary pull-right" id = "preview" href = "#" title = "Print Preview">Print Preview</a>
                </div>
            </div>
        </div>
        <!---->
    </div>
</div>
   <!---->
<div style="margin-bottom: 20px; overflow: hidden">
<div style="width: 100%; overflow: hidden;">
<div class="panel panel-primary">
<div class="panel-heading" style="padding: 0 11px">
    <div class="row">
        <div class="col-lg-9">Description</div>
        <div class="col-lg-3">Amount</div>
    </div>
</div>
<div class="panel-body">
<div class="row">
<div class="col-lg-12">
    <!-- header row -->
    <!-- any existing items will be displayed here -->
    <?php
    $st = 0;
    $next_sort=1;
    $current_sorts = array();
    $tax_total=0;
    $tax_line=0;
    $li_count=0;
    if($proposal_items['error'] == "0") 
    {
        unset($proposal_items['error']);
        print '<table width = "100%">';
        foreach($proposal_items as $item) 
        {
        	$li_count++;
        	$next_sort++;
            $st += $item['amount'];
            $current_sorts[]=$item['sort'];
            ?>
            <div class="row">
                <div>
                    <div class="ta_container col-lg-9 col-xs-8">
                        <?php
                        $rand = mt_rand(); 
                        ?>
                        <textarea style="margin-bottom: 15px; width: 100%" class="ckeditor ars existing-line-item form-control" id="desc-<?php print $item['database_id']; ?>" name="desc-<?php print $item['database_id']; ?>"><?php print $item['item'];?></textarea>                    
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <input type="hidden" class="rand" value="<?php print $rand; ?>">
                        <input type="text" class="dollar-only amount form-control" value="<?php print $item['amount']; ?>"
                               name="amount-<?php print $item['database_id']; ?>"
                               id="amount-<?php print $item['database_id']; ?>" data-dbid="<?php print $item['database_id']; ?>">

                        Tax Type: 
	                    <select class="tt-select tax form-control" name="tax-<?php print $item['database_id']; ?>" id="tax-<?php print $item['database_id']; ?>">
	                     	<?php
	                     	if(!empty($item['tax_label_name']))
	                     	{
	                     		print '<option value = "'.$item['tax_label_name'].'^'.$item['tax_label_rate'].'">'.$item['tax_label_name'].'</option>';
	                     	}
	                     	print '<option value = "">-Select-</option>';
	                     	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
	                     	?>
	                    </select> 
	                    Tax Amount: $

	                    <?php
	                    //$vujade->debug_array($item);
	                    if(!empty($item['tax_label_rate']))
                     	{
                     		$tax_line=$item['amount']*$item['tax_label_rate']*$tax_rate;
                     		print @number_format($tax_line,2);
                     	}
                     	else
                     	{
                     		print '0.00';
                     	}
                     	$tax_total+=$tax_line;
                     	$tax_line=0;
	                    ?>

                    </div>
					<div class="col-lg-1 col-xs-12">
					<a href = "edit_proposal.php?project_id=<?php print $project_id; ?>&proposalid=<?php print $proposalid; ?>&action=8&after_id=<?php print $item['database_id']; ?>&position=<?php print $item['sort']; ?>" id = "" class = "add-new-row btn btn-xs btn-success" title = "Add New Item After This Item">+</a> 
					<span style="line-height: 39px"><a title = "Delete Item" class="plus-delete btn btn-xs btn-danger" href="#"
					id="<?php print $item['database_id']; ?>">X</a></span>
					<br>
					<input type="hidden" class="sort" value="<?php print $item['sort']; ?>"
                               name="sort-<?php print $item['database_id']; ?>"
                               id="sort-<?php print $item['database_id']; ?>" style = "">
					</div>
				</div>
			</div>
			<br>
        <?php
        }

        if($proposal['tax_total']>0)
        {
        	$tax_total=$proposal['tax_total'];
        }

        $total=$st+$tax_total;

        // highest current sort
        $highest = max($current_sorts);
        if($next_sort<=$highest)
        {
        	$next_sort=$highest+1;
        }
    }
    ?>
    <!-- blank input row -->
    <div class="row">
        <div class="col-lg-9 col-xs-8" style="">
            <textarea name="description" id="description" style="margin-bottom: 15px; width: 100%" class="ckeditor ars form-control"></textarea>
        </div>
        <div class="col-lg-2 col-xs-4" style="">
            <input class="form-control dollar-only" type="text" name="amount" id="amount" style="">
            <span style="margin-right:55px;width:148px;height:35px;">&nbsp;</span>
            <br>
            <input class="" type="hidden" name="sort" id="sort" style="" value = "<?php print $next_sort; ?>">
            Tax Type: 
            <select class="tax form-control" name="tax_type" id="tax_type">
             	<?php
             	print '<option value = "">-Select-</option>';
             	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
             	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
             	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
             	?>
            </select> 
        </div>
    </div>
</div>
    <div style="margin-bottom: 20px;">
        <div class="row">
            <div class="col-md-12">
                <a class="plus-update btn btn-success btn-xs pull-right" id="update-items" href="#" style="">Update Items</a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
<div style="margin-bottom: 20px; overflow: hidden">
    <div class="row">
        <div class="col-lg-4 well">
            <span style="line-height:39px; font-weight:bold;">Subtotal: $<?php print number_format($st, 2, '.', ','); ?></span>
        </div>
        <div class="col-lg-4 well">
            <span style="line-height:39px; font-weight:bold;">Tax: $<?php print number_format($tax_total, 2, '.', ','); ?></span>
        </div>
        <div class="col-lg-4 well">
            <span style="line-height:39px; font-weight:bold;font-size:18px;">Total: $<?php print @number_format($total, 2, '.', ','); ?></span>
            <input type="hidden" name="tax_total" id="tax_total" value="<?php print $tax_total; ?>">
            <input type="hidden" name="total" value="<?php print $total; ?>">
        </div>
    </div>
</div>

<!-- tax rate, amount and proposal total -->
<?php
if (empty($proposal['deposit_rate'])) 
{
    $deposit_rate = $setup['advanced_deposit'];
    $deposit_amount = $total * $deposit_rate;
} 
else 
{
    $deposit_rate = $proposal['deposit_rate'];
    $deposit_amount = $total * $deposit_rate;
}
?>
<div style="margin-bottom: 20px; overflow: hidden">

	<table class = "" width = "100%;">
		<tr>
			<td valign = "top">
				Deposit Rate (decimal): 
			</td>

			<td valign = "top">
				<input class="dollar-only form-control" type="text" id="deposit_rate" name="deposit_rate" value="<?php print $deposit_rate; ?>" style = "width:150px;">
			</td>

			<td valign = "top">
				&nbsp;
			</td>

			<td valign = "top">
				Deposit: $<?php print @number_format($deposit_amount, 2, '.', ','); ?>
			</td>

			<td valign = "top">
				<a href="#" id="update_tax_deposit" class="btn btn-xs btn-primary">Update Deposit</a>
			</td>
		</tr>

		<tr><td colspan="5">&nbsp;</td></tr>

		<!-- taxes -->
		<?php
		if($setup['country']!="Canada")
		{
			?>
			<tr>
				<td valign = "top">
					Sales Tax (decimal): <br>
				</td>

				<td valign = "top">
					<input class="dollar-only form-control" type="text" id="tax_rate" name="tax_rate" value="<?php print $tax_rate; ?>" style = "width:150px;"><br>
				</td>

				<td valign = "top">
					Tax Amount: $<br>
				</td>

				<td>
					<input type = "text" class = "dollar-only form-control" id = "tax_amount" name = "tax_amount" value = "<?php print @number_format($tax_total, 2, '.', ','); ?>" style = "width:150px;">
					<br>
					<input type = "checkbox" name = "custom_tax_amount" value = "1" id = "custom_tax_amount" <?php if($custom_tax_amount==1){ print 'checked'; } ?> > Use custom tax amount <a href="#" data-toggle="tooltip" title="Enter your custom tax amount in the box to the left and then check this box to have the system override and save the tax amount to your custom value."><span class = "glyphicon glyphicon-info-sign">&nbsp;</span></a>
				</td>

				<td valign = "top">
					<a href="#" id="update_taxes" class="btn btn-xs btn-primary">Update Taxes</a>
				</td>
			</tr>
		<?php } ?>
		<tr><td colspan="5">&nbsp;</td></tr>
		<tr><td colspan="5">&nbsp;</td></tr>

	</table>
</div>

<p style="padding-top:10px;text-align:center;font-weight:bold;">Terms on Proposal</p>

<p>Switch to: 
	<!--<a href = "#" id = "st1" class = "change-terms">Current Saved Terms</a>-->
	<select name = "change-terms" id = "change-terms" class = "">
		<option value = "">-select-</option>
		<option value = "st2">Terms 1</option>
		<option value = "st3">Terms 2</option>
		<option value = "st1">Revert to Saved</option>
	</select> 
</p>
<div class="row" style="margin-top:15px;">
    <div class="col-md-12">
        <textarea name="terms" id="terms" class = "ckeditor">
            <?php
            print $proposal['terms'];
            ?>
        </textarea>

<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>

    </div>
</div>
<div style="margin-top:15px;">
	<?php
	if($is_new==1)
	{
    	?>
    	<a href="#cancel_modal" id="cancel" class="btn btn-lg btn-warning btn-danger" style="width:100px;float:left">CANCEL</a> 
    <?php 
	}
	else
	{
		print '<a href="project_proposals.php?id='.$project_id.'" id="" class="btn btn-lg btn-warning btn-danger" style="width:100px;float:left">CANCEL</a> ';
	}
	?>
     <a href="#" id="done" class="btn btn-lg btn-success" style="margin-left:5px;">SAVE AND COMPLETE</a>

<p>
<?php
$ps = strip_tags($project['description']);
$ps = str_replace('"', " ", $ps);
$ps = str_replace("'", " ", $ps);

// force recognize and display carriage returns
$ps= str_replace(array("\n", "\r"), array('\n', '\r'), $ps);
$ps=str_replace('\n','',$ps);
$ps=str_replace('\r','<br>',$ps);
//$ps=str_replace('\r','<font color = "white">r</font><br>',$ps);
//print $ps;
?>
<input type = "hidden" id = "hidden_desc" value = "<?php print $ps; ?>">
</p>

</div>
</div>
</form>
</div>
</div>
</div>
</section>
</section>

<!-- modal for delete employee -->
<div id="cancel_modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1>Delete Proposal</h1>
<p>Are you sure you want to delete this proposal?</p>
<p><a id = "cancel-yes" class="btn btn-lg btn-danger" href="#">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- modal for tax type selection errors -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id = "tax_type_error_modal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style = "padding:15px;">
    <h1>Warning</h1>

    <p id = "js-values"></p>

	<p>
		<a id = "review" class="popup-modal-dismiss btn btn-lg btn-success" href="#">Review</a>
		<a id = "proceed" class="btn btn-lg btn-primary" href="#">Proceed as is</a>
	</p>
    </div>
  </div>
</div>

<style>
/* The side navigation menu */
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 0; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111; /* Black*/
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>
<div id="print_preview" class="sidenav">
  	<a href="#" class="closebtn" id="close_preview" style = "margin-top:50px;clear:both;">X</a><br>
  	<div id = "preview_container"></div>
</div>

<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- modal -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function()
    {
        "use strict";

        // Init Theme Core
        Core.init();

        // print preview
		$('#preview').click(function(e)
		{
			e.preventDefault();
			$('#topbar').hide();
			$('#print_preview').css('width','100%');
			$('#preview_container').html('');

			// load as iframe
			var id = "<?php print $project_id; ?>";
			var pid = "<?php print $proposal_database_id; ?>";
			var iframesrc = "print_proposal.php?id="+id+"&proposalid="+pid;
			var iframe = '<iframe width = "1000" height="1200" src="'+iframesrc+'"></iframe>';
			$('#preview_container').html(iframe);
	        //$('#preview-modal').modal('show');
		});
		
		// close preview
		$('#close_preview').click(function(e)
		{
			e.preventDefault();
			$('#preview-container').html('');
	        //$('#preview-modal').modal('hide');
	        $('#print_preview').css('width','0');
	        $('#topbar').show();
		});

		// set tax amount to 0 on focus
        $('#tax_rate').focus(function(){
            $('#tax_amount').val(0);
        });

        // change in the tax amount box
        $('#tax_amount').focus(function(){
            $('#tax_rate').val(0);
        });

        // auto resize text areas to fit the height of proposal item text
        $('.existing-line-item').each(function()
        {
        	$(this).height( $(this)[0].scrollHeight );
        });

        $('.existing-line-item').change(function()
        {
        	$(this).height( $(this)[0].scrollHeight );
        });

        // paste the project scope into the first blank description box
        $('#ps').click(function(e)
        {
            e.preventDefault();
            var desc = $.trim($('#hidden_desc').val());
            // $('#description').html(desc);

            //var sd = CKEDITOR.instances['description'].setData(desc);
            CKEDITOR.instances['description'].setData(desc);
            $("#description").height( $("#description")[0].scrollHeight );
        	//alert(desc);
        	$("#update-items").focus();

        });

        // change the terms
        $('#change-terms').change(function(e)
        {
            e.preventDefault();
            var id = $(this).val();

            if(id=='st1')
            {
            	id='X';
            }
            if(id=='st2')
            {
            	id=1;
            }
            if(id=='st3')
            {
            	id=7;
            }

            $.post( "jq.get_terms.php", { id: id, proposalid: '<?php print $proposalid; ?>' })
			  .done(function(r) 
			  {
			    	CKEDITOR.instances['terms'].setData(r);
			  });

            /*
            if(id=='st1')
            {
            	var desc="<?php print $terms1['terms']; ?>";
            }
            if(id=='st2')
            {
            	var desc="<?php print $terms2['terms']; ?>";
            }
            if(id=='st3')
            {
            	var desc="<?php print $terms3['terms']; ?>";
            }
            */
            $("#terms").height( $("#terms")[0].scrollHeight );
        	$("#terms").focus();
        });

        // date pickers
        $('.dp').datepicker();

        // delete an item
        $('.plus-delete').click(function(e)
        {
            e.preventDefault();
            $('#tax_amount_box').val(0);
            var item_id = this.id;
            $('<input>').attr({
                type: 'hidden',
                id: 'item_id',
                name: 'item_id',
                value: item_id
            }).appendTo('#np');
            $('#action').val('3');
            $('#np').submit();
        });

        // update tax rate
        $('#update_tax_deposit').click(function(e){
            e.preventDefault();
            $('#action').val('4');
            $('#np').submit();
        });

        // update taxes button
        $('#update_taxes').click(function(e){
            e.preventDefault();
            $('#action').val('9');
            $('#np').submit();
        });

        $('#cancel-yes').click(function(e){
            e.preventDefault();
            $('#action').val('7');
            $('#np').submit();
        });

        // modal: cancel button
	    $('#cancel').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#cancel_modal',
			modal: true
		});

        // modal dismiss
	    $(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});

        // done button
        $('#done').click(function(e){
            e.preventDefault();
            $('#action').val('5');
            $('#np').submit();
        });

        // update items button
        $('#update-items').click(function(e)
        {
            e.preventDefault();
            // clear tax amount
            $('#tax_amount_box').val('');

            var error_count=0;

            // loop through every tax amount box
            // if something is in this box, check the tax type
            // if tax type is empty increase error count
            $('.amount').each(function()
            {
            	var v=$(this).val();
            	if( (v!=0) && (v!='') )
            	{
            		var dbid = $(this).data('dbid');
            		var sel = $('#tax-'+dbid).val();
            		if(sel=='')
            		{
            			error_count++;
            			$('#tax-'+dbid).css('border','1px solid red');
            		}
            	}
            });

            // default blank row
            var da = $('#amount').val();
            if( (da!=0) && (da!='') )
            {
            	var ta = $('#tax_type').val();
            	if(ta=='')
            	{
            		error_count++;
            		$('#tax_type').css('border','1px solid red');
            	}
            }

            if(error_count>0)
            {
            	var error_msg='One or more line items was not assigned a tax rate. Do you wish to proceed anyway?';

				//tax_type_error_modal
				$('#js-values').html(error_msg);
				$('#tax_type_error_modal').modal('show');
				return false;
            }

			// no errors
            // submit form
            $('#action').val('6');
            $('#np').submit();
        });

		// proceed button was pressed
        $('#proceed').click(function(e)
        {
            e.preventDefault();
            // clear tax amount
            $('#tax_amount_box').val('');

            // submit form
            $('#action').val('6');
            $('#np').submit();
        });
	
        // review button 
        $('#review').click(function(e)
        {
            e.preventDefault();
            $('#tax_type_error_modal').modal('hide');
        });

        // dollar amount formatting
        $('.dollar-only').keyup(function() 
        { 
		    this.value = this.value.replace(/[^0-9\.\-]/g,'');
		});

    });
</script>
</body>
</html>
