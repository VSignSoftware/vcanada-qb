<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// admin permission (count of accounting permissions)
$count_permissions = $projects_permissions['read']+$projects_permissions['edit']+$projects_permissions['delete'];

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 17;
$cmenu = 2;
$section = 3;
$title = "Costing Labor - ".$project['project_id'].' - '.$project['site'].' - ';;
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <?php require_once('project_left_tray.php'); ?>
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">
            <div class="pl15 pr15" style = "width:100%;">
              <?php require_once('project_right_tray.php'); ?>
              
              <div class="panel panel-primary panel-border top">
                <div class="panel-heading">
                  <span class="panel-title"></span>
                  <div class="widget-menu pull-right">
                    	<a href = "print_costing_labor.php?id=<?php print $id; ?>" target = "_blank" class = "btn btn-sm btn-primary">Print</a> 
						<?php
						if($count_permissions==3)
						{
							?>
							<a href = "print_costing_labor.php?admin=1&id=<?php print $id; ?>" target = "_blank" class = "btn btn-sm btn-primary">Admin Print</a>
						<?php
						}
						?>
                  </div>
                </div>
                <div class="panel-body bg-light">
                	<table class = "table">
						<tr style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">
							<td width = "15%">Date</td>
							<td width = "10%">Hours</td>
							<td width = "40%">Department</td>
							<td width = "25%">Employee</td>
							<td width = "10%">&nbsp;</td>
						</tr>

						<?php
						# labor
						$raw_labor = 0;
						$labor_rider = 0;
						$total_labor = 0;
						$tcdata = $vujade->get_timecards_for_project($id,"","","",1);
						if($tcdata['error']=="0")
						{
							unset($tcdata['error']);
							$last = end($tcdata);
							$current_labor_type = $tcdata[0]['type'];
							$labor_section_total = 0;
							$labor_section_hours_total = 0;
							foreach($tcdata as $tc)
							{
								# employee 
								$employee_id = $tc['employee_id'];
								$empl = $vujade->get_employee($employee_id);

								# employee's rate
								$current_rate = $vujade->get_employee_current_rate($employee_id);

								# type of work
								$work_type = $tc['type'];

								# rate for type of work
								$rate_data = $vujade->get_labor_rate($work_type,'id');

								# hours worked
								$total_time = 0;
								$st_total = 0;
								$ot_total = 0;
								$dt_total = 0;

								$line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
								$st_total+=$tc['standard_time'];
								$ot_total+=$tc['over_time'];
								$dt_total+=$tc['double_time'];

								$total_time+=$line_total;

								# raw labor 
								$st_rate_total = $st_total * $current_rate['rate'];
								$ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
								$dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
								$raw_labor = $st_rate_total + $ot_rate_total + $dt_rate_total;

								$total_labor += $raw_labor;

								//$labor_section_total+=$raw_labor;

								if($tc['type']==$current_labor_type)
								{
									$current_labor_type=$tc['type'];
									$labor_section_total+=$raw_labor;
									$labor_section_hours_total+=$line_total;
									//print '<tr><td colspan = "5">'.$raw_labor.'</td></td>';
									//print '</tr>';
								}
								else
								{
									/*
									print '<tr><td colspan = "3"></td><td><strong>Dept. Subtotal</strong></td>';
									print '<td><strong>'.@number_format($labor_section_total,2,'.',',').'</strong></td>';
									print '</tr>';
									print '<tr><td colspan = "5">&nbsp;</td></td>';
									print '</tr>';
									*/

									print '<tr><td><strong>Dept. Hours</strong></td>';
									print '<td><strong>'.$labor_section_hours_total.'</strong></td>';
									print '<td>&nbsp;</td>';

									print '<td>&nbsp;</td>';
									//print '<td><strong>Dept. Subtotal</strong></td>';
									if($admin_permissions['read']==1)
									{
										print '<td><strong>'.@number_format($labor_section_total,2,'.',',').'</strong></td>';
									}
									else
									{
										print '<td>&nbsp;</td>';
									}
									print '</tr>';
									print '<tr><td colspan = "5">&nbsp;</td></td>';
									print '</tr>';

									unset($current_labor_type);
									$current_labor_type=$tc['type'];
									$labor_section_total=0;
									$labor_section_total=$raw_labor;
									$labor_section_hours_total=$line_total;
								}

								print '<tr>';
								
								print '<td>';
								print $tc['date'];
								print '</td>';

								print '<td>';
								print $line_total;
								print '</td>';

								print '<td>';
								print $tc['type_name'];
								print '</td>';

								print '<td>';
								print $empl['fullname'];
								print '</td>';

								print '<td>';
								if($admin_permissions['read']==1)
								{
									print @number_format($raw_labor,2,'.',',');
								}
								else
								{
									print '&nbsp;';
								}
								print '</td>';

								print '</tr>';

								if($tc==$last)
								{
									print '<tr><td><strong>Dept. Hours</strong></td>';
									print '<td><strong>'.$labor_section_hours_total.'</strong></td>';
									print '<td>&nbsp;</td>';
									print '<td>&nbsp;</td>';
									//print '<td><strong>Dept. Subtotal</strong></td>';
									if($admin_permissions['read']==1)
									{
										print '<td><strong>'.@number_format($labor_section_total,2,'.',',').'</strong></td>';
									}
									else
									{
										print '<td>&nbsp;</td>';
									}
									print '</tr>';
									print '<tr><td colspan = "5">&nbsp;</td></td>';
									print '</tr>';
								}
							}
						}
						?>

						<tr>
							<td colspan = "5">&nbsp;</td>
						</tr>
						
						<tr>
							<td colspan = "5" >&nbsp;</td>
								<?php
								if($admin_permissions['read']==1)
								{
									?>
									<input type = "text" readonly = "readonly" style = "width:50px;" value = "<?php print @number_format($total_labor,2,'.',','); ?>">
									<?php
								}
								else
								{
									print '&nbsp;';
								}
								?>
							</td>
						</tr>

					</table>
                </div>
                </div>
            </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->
  <!-- BEGIN: PAGE SCRIPTS -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {
    	"use strict";
    
    	// Init Theme Core    
    	Core.init();
    	//var n = $('#notes').html();
    	//$("#notes").html($.trim(n));
    
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>