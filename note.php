<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$customer_permissions = $vujade->get_permission($_SESSION['user_id'],'Clients');
if($customer_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

$type=$_REQUEST['type'];
$nid=$_REQUEST['nid'];
$id=$_REQUEST['id'];

if($action==1)
{
	// new note; no action needed
	$title = "New Note - ";
	$page_header = 'New Note';
	if($type==1)
	{
		$action2=7;
	}
	else
	{
		$action2=4;
	}
	$note['name_display']=$employee['first_name'].' '.$employee['last_name'];
}

if($action==2)
{
	// existing note; get content for display
	$note = $vujade->get_note($nid);
	if($note['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$title = "Edit Note - ";
	$page_header = 'Edit Note';
	if($type==1)
	{
		$action2=10;
	}
	else
	{
		$action2=7;
	}
}

$emp=$employee;
$section=7;
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $page_header; ?></a>
    </li>
  </ol>
</div>

<?php
if($type==1)
{
	print '<a href = "customer.php?tab=3&id='.$id.'" class = "btn btn-primary btn-sm pull-right">&laquo; Back</a>';
	$form_action='customer.php';
	$hidden_fields = '<input type = "hidden" name = "action" value = "'.$action2.'">';
	$hidden_fields .= '<input type = "hidden" name = "id" value = "'.$id.'">';
	$hidden_fields .= '<input type = "hidden" name = "note_id" value = "'.$nid.'">';
}
if($type==2)
{
	print '<a href = "vendor.php?tab=3&id='.$id.'" class = "btn btn-primary btn-sm pull-right">&laquo; Back</a>';
	$form_action='vendor.php';
	$hidden_fields = '<input type = "hidden" name = "action" value = "'.$action2.'">';
	$hidden_fields .= '<input type = "hidden" name = "id" value = "'.$id.'">';
	$hidden_fields .= '<input type = "hidden" name = "note_id" value = "'.$nid.'">';
}
?>

</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">
<div class="admin-form theme-primary">
<div class="panel heading-border panel-primary">
<div class="panel-body bg-light">

<?php
$vujade->show_messages();
$vujade->show_errors();
?>

<div class = "row">
<div class = "col-md-12">

<form method = "post" action = "<?php print $form_action; ?>">
	<strong>Date:</strong><br>
	<input type = "text" name = "date" id = "date" style = "width:200px;" class = "form-control dp" value = "<?php print $note['date']; ?>">
	<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
	<br>
	<br>

	<!--
	<strong>Written By:</strong><br>
	<input type = "text" disabled style = "width:200px;" class = "form-control" value = "<?php //print $note['name_display']; ?>">
	<br>
	<br>
	-->

	<strong>Message:</strong><br>
	<textarea name = "notes" id = "notes" style = "width:400px;height:100px;" class = "form-control ckeditor"><?php print $note['body']; ?></textarea>
	<div id = "error_3" style = "display:none;">This field cannot be empty.</div>
	<br>
	<br>

	<?php print $hidden_fields; ?>

	<input type = "submit" id = "save" value = "SAVE" class = "btn btn-primary"> 
	<button id = "cancel" class = "btn btn-warning">CANCEL</button>
</form>

<?php 
require_once('ckeditor.php');
?>

</div>

</div>
</div>
</div>

</div>
</div>

</section>
</section>


<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    $('.dp').datepicker();

});
</script>

</body>
</html>