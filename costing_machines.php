<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($permissions['read']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}
if($permissions['edit']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
  	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);
$setup = $vujade->get_setup();
if($setup['error']!=0)
{
  	$vujade->page_redirect('error.php?m=3');
}
$buyout_overhead_percentage=$setup['buyout_overhead'];
$general_overhead_percentage=$setup['general_overhead'];
$machines_overhead_percentage=$setup['machine_overhead'];
$materials_overhead_percentage=$setup['materials_overhead'];

$action = 0;
if(isset($_REQUEST['action']))
{
  	$action = $_REQUEST['action'];
}

// update
if($action==1)
{

	//print_r($_REQUEST);
	//die;

	// determine if labor already exists (ie estimate not new)
	// or does not exist (ie estimate is new)
	$el_test = $vujade->get_machines_for_estimate($id);

	if($el_test['error']=="0")
	{
		// not new

		// combine row ids and hours (update value)
		$dbids = $_POST['dbids'];
		$hours = $_POST['hours'];
		$new_arr = @array_combine($dbids, $hours);

		foreach($new_arr as $row_id => $hours)
		{
			// update existing
			$s[]=$vujade->update_row('estimates_machines',$row_id,'time_hours',$hours);
		}

	}
	else
	{
		// estimate is new
		$types = $_POST['types'];
		$hours = $_POST['hours'];
		$rates = $_POST['rates'];
		$labor_ids = $_POST['labor_ids'];

		foreach($types as $key => $type)
		{
			// create new
			$vujade->create_row('estimates_machines');
			$row_id=$vujade->row_id;

			$s[]=$vujade->update_row('estimates_machines',$row_id,'estimate_id',$id);
			$s[]=$vujade->update_row('estimates_machines',$row_id,'time_type',$type);

			$rate = $rates[$key];
			$hour = $hours[$key];
			$labor_id = $labor_ids[$key];

			$s[]=$vujade->update_row('estimates_machines',$row_id,'rate',$rate);
			$s[]=$vujade->update_row('estimates_machines',$row_id,'time_hours',$hour);
			$s[]=$vujade->update_row('estimates_machines',$row_id,'labor_id',$labor_id);
		}
	}

	//print_r($s);
	//die;

	$vujade->messages[]='Updated';
}

$employee = $vujade->get_employee($_SESSION['user_id']);
if($employee['error']!="0")
{
  	$vujade->page_redirect('error.php?m=1');
}
$emp=$employee;
$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 17;
$cmenu = 8;
$section = 3;
$title = "Costing - ".$project['project_id'].' - '.$project['site'].' - ';;
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left </aside>-->
<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;border:0px solid red;">
<div class="pl15 pr15" style = "width:100%;">
<?php require_once('project_right_tray.php'); ?>
<div class="panel panel-primary panel-border top">
<div class="panel-heading">
<span class="panel-title"></span>
</div>
<div class="panel-body bg-light">
	<?php 
    $vujade->show_errors();
    $vujade->show_messages();
    ?>

    <form method = "post" action = "costing_machines.php" id = "nd" class = "form-inline">
	<input type = "hidden" name = "action" id = "action" value = "1">
	<input type = "hidden" name = "id" value = "<?php print $id; ?>">

	<div class = "row">
		<div class = "col-md-12">

			<table width = "100%" class = "table table-hover table-striped  table-bordered">
				<thead>
					<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
					<th width = "70%">
						Machine Description
					</th>
					<th width = "10%">
						Quantity
					</th>
					<th width = "10%">
						Rate
					</th>
					<th width = "10%">
						Amount
					</th>
					</tr>
					</thead>

					<tbody>
						<?php
						$labor_total=0;

						// estimate is not new
						// (ie, has labor added to it)
						$el_test = $vujade->get_machines_for_estimate($id);
						if($el_test['error']=="0")
						{
							unset($el_test['error']);
							foreach($el_test as $lt)
							{
								$line_total = $lt['rate']*$lt['hours'];
								$labor_total+=$line_total;
								print '<tr>';
								print '<td>';
								print $lt['type'];
								print '</td>';
								print '<td>';
								print '<input type = "hidden" name = "labor_types[]" value = "'.$lt['type'].'">';

								print '<input type = "text" name = "hours[]" class = "form-control" value = "'.$lt['hours'].'" style = "width:100px;">';

								print '<input type = "hidden" name = "dbids[]" value = "'.$lt['database_id'].'" style = "width:50px;">';

								print '</td>';
								print '<td>';
								print '$'.@number_format($lt['rate'],2,'.',',');
								print '</td>';
								print '<td>';
								print '$'.@number_format($line_total,2,'.',',');
								print '</td>';
								print '</tr>';
							}	
						}
						else
						{
							// is new
							$machine_types = $vujade->get_machine_types();
							if($machine_types['error']=="0")
							{
								unset($machine_types['error']);
								foreach($machine_types as $lt)
								{
									$line_hours = 0;
									$line_total = 0;
									$labor_total+=$line_total;
									print '<input type = "hidden" name = "types[]" value = "'.$lt['type'].'" style = "width:30px;">';
									print '<tr>';
									print '<td>';
									print $lt['type'];
									print '</td>';
									print '<td>';
									print '<input type = "text" name = "hours[]" class = "form-control" value = "'.$lt['hours'].'" style = "width:100px;">';
									print '</td>';
									print '<td>';
									print '$'.@number_format($lt['rate'],2,'.',',');
									print '<input type = "hidden" name = "rates[]" value = "'.$lt['rate'].'" style = "width:30px;">';
									print '<input type = "hidden" name = "labor_ids[]" value = "'.$lt['database_id'].'" style = "width:30px;">';
									print '</td>';
									print '<td>';
									print '$'.@number_format($line_total,2,'.',',');
									print '</td>';
									print '</tr>';
								}
							}
						}
						?>
						<tr class = "dark">
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>Subtotal: </td>
							<td>$<?php print @number_format($labor_total,2,'.',','); ?></td>
						</tr>
					</tbody>
			</table>
		</div>
	</div>

	<div style = "margin-top:15px;">
		<input type = "submit" class = "btn btn-success" value = "SAVE AND COMPLETE">  
    </div>

    </form>

</div>
                  
</section>
<!-- End: Content -->
</section>
</div>
<!-- End: Main -->
<!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {
    "use strict";
    
    // Init Theme Core    
    Core.init();
    var n = $('#notes').html();
    $("#notes").html($.trim(n));
    $('#date_billed').val('<?php print $project['date_billed']; ?>');
    $('#date_closed').val('<?php print $project['date_closed']; ?>');
    $('.datepicker-days').datetimepicker({
      datePickdate: true,
      pickTime: false
    });
    $('#close_order').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#close-order-form',
      modal: true
    });
    $('#open_order').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#open-order-form',
      modal: true
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>
