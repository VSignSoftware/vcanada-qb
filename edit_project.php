<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['create']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

// site setup
$setup=$vujade->get_setup();

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
# edit project
if($action==1)
{
    $site=$_POST['site'];
	$sp_name = $_POST['salesperson'];
	if($sp_name=="House")
	{
		$salesperson['fullname']="House";
	}
	else
	{
		$salesperson = $vujade->get_employee_by_name($sp_name);
	}

	//print $sp_name;
	//print '<br>';
	//$vujade->debug_array($salesperson);
	//die;

	$pm_id = $_POST['pm'];
	$project_manager = $vujade->get_employee($pm_id,2);

	$address_1=$_POST['address_1'];
	$address_2=$_POST['address_2'];
	$city=trim($_POST['city']);
	$state=trim($_POST['state']);
	$zip=$_POST['zip'];
	$site_contact=$_POST['site_contact'];
	$site_contact_phone=$_POST['site_contact_phone'];
	$site_contact_email=$_POST['site_contact_email'];
	$project_contact=$_POST['project_contact'];
	$project_contact_phone=$_POST['project_contact_phone'];
	$project_contact_email = $_POST['project_contact_email'];
	$s = array();
	$s[] = $vujade->update_row('projects',$id,'site',$site,'project_id');
	$s[] = $vujade->update_row('projects',$id,'salesperson',$salesperson['fullname'],'project_id');
	$s[] = $vujade->update_row('projects',$id,'project_manager',$project_manager['fullname'],'project_id');
	$s[] = $vujade->update_row('projects',$id,'address_1',$address_1,'project_id');
	$s[] = $vujade->update_row('projects',$id,'address_2',$address_2,'project_id');
	$s[] = $vujade->update_row('projects',$id,'city',$city,'project_id');
	$s[] = $vujade->update_row('projects',$id,'state',$state,'project_id');
	$s[] = $vujade->update_row('projects',$id,'zip',$zip,'project_id');
	$s[] = $vujade->update_row('projects',$id,'site_contact',$site_contact,'project_id');
	$s[] = $vujade->update_row('projects',$id,'site_contact_phone',$site_contact_phone,'project_id');
	$s[] = $vujade->update_row('projects',$id,'site_contact_email',$site_contact_email,'project_id');
	$s[] = $vujade->update_row('projects',$id,'project_contact',$project_contact,'project_id');
	$s[] = $vujade->update_row('projects',$id,'project_contact_phone',$project_contact_phone,'project_id');
	$s[] = $vujade->update_row('projects',$id,'project_contact_email',$project_contact_email,'project_id');

	// change the tax rate in all proposals

	// 2 get the current tax rate based on city, state
	if($setup['is_qb']==1)
	{
		// get from qb sales tax
		// get and set the sales tax code 
		// qb tax state is the same as shipping address
		if($project['state']==$setup['qb_tax_state'])
		{
			$tax_data = $vujade->QB_get_sales_tax($city.', '.$state);
		}
		else
		{
			// not the same
			$tax_data = $vujade->QB_get_sales_tax('Out of State');
		}
		if($tax_data['error']=="0")
		{
			$tax_rate=$tax_data['rate']; 
		}
		else
		{
			$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
			$tax_rate=$tax_data2['rate'];
		}
	}
	else
	{
		// not qb; get from tax table
		$tax_data=$vujade->get_tax_for_city($city,$state);
		$tax_rate=$tax_data['rate'];
	}

	// debug tax rate
	// print 'tax rate is: '.$tax_rate;
	// die;

	// update all proposals and line items for this job with the updated tax rate
	// use
	// $tax_rate

	// get all proposals for this project
	$proposals = $vujade->get_proposals_for_project($id);
	if($proposals['error']==0)
	{
		unset($proposals['error']);
		foreach($proposals as $proposal)
		{
			// update tax rate for each one
			$dbid = $proposal['database_id'];
			$s[]=$vujade->update_row('proposals',$dbid,'tax_rate',$tax_rate);

			// update proposal item tax rate?

		}
	}
	
	// additional debug
	//$vujade->print_r_array($proposals);
	//die;

	// redirect
	$vujade->page_redirect('edit_project_customer.php?id='.$id);
}
# back button was pressed
if($action==2)
{
    $vujade->page_redirect('project.php?id='.$id);
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=2;

$title = "Edit Project - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Edit Project</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                  <p>
                    <span class="label label-primary">Step 1: Project Setup</span> <span class="label label-default">Step 2: Customer Selection</span> <span class="label label-default">Step 3: Scope of Work</span>
                  </p>

                  <form method="post" action="edit_project.php" id="form">
                  	
                    <input type = "hidden" name = "action" id = "action" value = "1">
                    <input type = "hidden" name = "id" value = "<?php print $id; ?>">

                    <div class="row">

                      <!-- left side -->
                      <div class="col-md-6">

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "site" id = "site" value = "<?php print $project['site']; ?>" placeholder="Project Name">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field select">
                            <select id="salesperson" name="salesperson">
                              <?php
                              if(isset($project['salesperson']))
                              {
                                print '<option value = "'.$project['salesperson'].'" selected = "selected">'.$project['salesperson'].'</option>';
                                print '<option value = "">-Salesperson-</option>';
                              }
                              else
                              {
                                print '<option value = "">-Salesperson-</option>';
                              }
                              $sp = $vujade->get_salespeople();
                              if($sp['error']=='0')
                              {
	                                unset($sp['error']);
	                                foreach($sp as $s)
	                                {
	                                  	print '<option value = "'.$s['fullname'].'">'.$s['fullname'].'</option>';
	                                }
                              }
                              // house
                              print '<option value = "House">House</option>';
                              ?>
                            </select>
                            <i class="arrow double"></i>
                          </label>
                        </div>

                        <div class="section">
                          <label class="field select">
							<select name = "pm" id = "pm">
							<?php
							if(!empty($project['project_manager']))
							{
								$emp2 = $vujade->get_employee_by_name($project['project_manager']);
								print '<option value = "'.$emp2['employee_id'].'" selected = "selected">'.$project['project_manager'].'</option>';
								print '<option value = "">-Project Manager-</option>';
							}
							else
							{
								print '<option value = "">-Project Manager-</option>';
							}
							$pms = $vujade->get_project_managers();
							if($pms['error']=='0')
							{
								unset($pms['error']);
								foreach($pms as $pm)
								{
									print '<option value = "'.$pm['employee_id'].'">'.$pm['fullname'].'</option>';
								}
							}
							?>
							</select>
							<i class="arrow double"></i>
							</label>
						</div>

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "address_1" id = "address_1" value = "<?php print $project['address_1']; ?>" placeholder="Address Line 1">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "address_2" id = "address_2" value = "<?php print $project['address_2']; ?>" placeholder="Address Line 2">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "city" id = "city" value = "<?php print $project['city']; ?>" placeholder="City">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field select">
                            <select id="state" name="state">
                              <?php
                              if(isset($project['state']))
                              {
                                  print '<option value = "'.$project['state'].'" selected = "selected">'.$project['state'].'</option>';
                                  //print '<option value = "">-State-</option>';
                              }
                              else
                              {
                                  //print '<option value = "">-State-</option>';
                              }
                              if($setup['country']=="USA")
			              	  {
                              ?>
                              <option value="AL">Alabama</option>
                              <option value="AK">Alaska</option>
                              <option value="AZ">Arizona</option>
                              <option value="AR">Arkansas</option>
                              <option value="CA">California</option>
                              <option value="CO">Colorado</option>
                              <option value="CT">Connecticut</option>
                              <option value="DE">Delaware</option>
                              <option value="DC">District Of Columbia</option>
                              <option value="FL">Florida</option>
                              <option value="GA">Georgia</option>
                              <option value="HI">Hawaii</option>
                              <option value="ID">Idaho</option>
                              <option value="IL">Illinois</option>
                              <option value="IN">Indiana</option>
                              <option value="IA">Iowa</option>
                              <option value="KS">Kansas</option>
                              <option value="KY">Kentucky</option>
                              <option value="LA">Louisiana</option>
                              <option value="ME">Maine</option>
                              <option value="MD">Maryland</option>
                              <option value="MA">Massachusetts</option>
                              <option value="MI">Michigan</option>
                              <option value="MN">Minnesota</option>
                              <option value="MS">Mississippi</option>
                              <option value="MO">Missouri</option>
                              <option value="MT">Montana</option>
                              <option value="NE">Nebraska</option>
                              <option value="NV">Nevada</option>
                              <option value="NH">New Hampshire</option>
                              <option value="NJ">New Jersey</option>
                              <option value="NM">New Mexico</option>
                              <option value="NY">New York</option>
                              <option value="NC">North Carolina</option>
                              <option value="ND">North Dakota</option>
                              <option value="OH">Ohio</option>
                              <option value="OK">Oklahoma</option>
                              <option value="OR">Oregon</option>
                              <option value="PA">Pennsylvania</option>
                              <option value="RI">Rhode Island</option>
                              <option value="SC">South Carolina</option>
                              <option value="SD">South Dakota</option>
                              <option value="TN">Tennessee</option>
                              <option value="TX">Texas</option>
                              <option value="UT">Utah</option>
                              <option value="VT">Vermont</option>
                              <option value="VA">Virginia</option>
                              <option value="WA">Washington</option>
                              <option value="WV">West Virginia</option>
                              <option value="WI">Wisconsin</option>
                              <option value="WY">Wyoming</option>
                              <option value="">---------------</option>
				              <option value="">-Canadian Provinces-</option>
				              <option value="Alberta">Alberta</option>
				              <option value="British Columbia">British Columbia</option>
				              <option value="Manitoba">Manitoba</option>
				              <option value="New Brunswick">New Brunswick</option>
				              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
				              <option value="Northwest Territories">Northwest Territories</option>
				              <option value="Nova Scotia">Nova Scotia</option>
				              <option value="Nunavut">Nunavut</option>
				              <option value="Ontario">Ontario</option>
				              <option value="Prince Edward Island">Prince Edward Island</option>
				              <option value="Quebec">Quebec</option>
				              <option value="Saskatchewan">Saskatchewan</option>
				              <option value="Yukon">Yukon</option>
				            <?php 
					    	} 
					    	// canadian servers
					    	if($setup['country']=='Canada')
					    	{
					    		?>
					    		  <option value="">-Canadian Provinces-</option>
					              <option value="Alberta">Alberta</option>
					              <option value="British Columbia">British Columbia</option>
					              <option value="Manitoba">Manitoba</option>
					              <option value="New Brunswick">New Brunswick</option>
					              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
					              <option value="Northwest Territories">Northwest Territories</option>
					              <option value="Nova Scotia">Nova Scotia</option>
					              <option value="Nunavut">Nunavut</option>
					              <option value="Ontario">Ontario</option>
					              <option value="Prince Edward Island">Prince Edward Island</option>
					              <option value="Quebec">Quebec</option>
					              <option value="Saskatchewan">Saskatchewan</option>
					              <option value="Yukon">Yukon</option>
					              <option value="">---------------</option>
					              <option value="">-US States-</option>
					              <option value="AL">Alabama</option>
					              <option value="AK">Alaska</option>
					              <option value="AZ">Arizona</option>
					              <option value="AR">Arkansas</option>
					              <option value="CA">California</option>
					              <option value="CO">Colorado</option>
					              <option value="CT">Connecticut</option>
					              <option value="DE">Delaware</option>
					              <option value="DC">District Of Columbia</option>
					              <option value="FL">Florida</option>
					              <option value="GA">Georgia</option>
					              <option value="HI">Hawaii</option>
					              <option value="ID">Idaho</option>
					              <option value="IL">Illinois</option>
					              <option value="IN">Indiana</option>
					              <option value="IA">Iowa</option>
					              <option value="KS">Kansas</option>
					              <option value="KY">Kentucky</option>
					              <option value="LA">Louisiana</option>
					              <option value="ME">Maine</option>
					              <option value="MD">Maryland</option>
					              <option value="MA">Massachusetts</option>
					              <option value="MI">Michigan</option>
					              <option value="MN">Minnesota</option>
					              <option value="MS">Mississippi</option>
					              <option value="MO">Missouri</option>
					              <option value="MT">Montana</option>
					              <option value="NE">Nebraska</option>
					              <option value="NV">Nevada</option>
					              <option value="NH">New Hampshire</option>
					              <option value="NJ">New Jersey</option>
					              <option value="NM">New Mexico</option>
					              <option value="NY">New York</option>
					              <option value="NC">North Carolina</option>
					              <option value="ND">North Dakota</option>
					              <option value="OH">Ohio</option>
					              <option value="OK">Oklahoma</option>
					              <option value="OR">Oregon</option>
					              <option value="PA">Pennsylvania</option>
					              <option value="RI">Rhode Island</option>
					              <option value="SC">South Carolina</option>
					              <option value="SD">South Dakota</option>
					              <option value="TN">Tennessee</option>
					              <option value="TX">Texas</option>
					              <option value="UT">Utah</option>
					              <option value="VT">Vermont</option>
					              <option value="VA">Virginia</option>
					              <option value="WA">Washington</option>
					              <option value="WV">West Virginia</option>
					              <option value="WI">Wisconsin</option>
					              <option value="WY">Wyoming</option>
					    		<?php
					    	}
					    	?>
					    </select>
                            <i class="arrow double"></i>
                            </label>
                        </div>

                        <div class="section">
                          <label class="field">
                            <input type = "text" name = "zip" id = "zip" class="gui-input" value = "<?php print $project['zip']; ?>" maxlength="10" placeholder="Zip / Postal Code">
                          </label>
                        </div>

                      </div>

                      <!-- right side -->
                      <div class="col-md-6">
                        
                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "project_contact" name = "project_contact" value = "<?php print $project['project_contact']; ?>" placeholder="Project Contact">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "project_contact_phone" name = "project_contact_phone" value = "<?php print $project['project_contact_phone']; ?>" placeholder="Project Contact Phone">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "project_contact_email" name = "project_contact_email" value = "<?php print $project['project_contact_email']; ?>" placeholder="Project Contact Email">
                            </label>
                          </div>

                          <div class="section">
                            <div class="checkbox-custom mb5">
                              <input type = "checkbox" name = "copy" id = "copy">
                              <label for="copy">Copy Project Contact To Site Contact</label>
                            </div>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "site_contact" name = "site_contact" value = "<?php print $project['site_contact']; ?>" placeholder="Site Contact">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "site_contact_phone" name = "site_contact_phone" value = "<?php print $project['site_contact_phone']; ?>" placeholder="Site Contact Phone">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "site_contact_email" name = "site_contact_email" value = "<?php print $project['site_contact_email']; ?>" placeholder="Site Contact Email">
                            </label>
                          </div>

                      </div>

                    </div>

                    <div class="row">	


                      <div class="col-md-12">
                        	<a href = "#" id = "back" class = "btn btn-lg btn-danger" style = "width:100px;">CANCEL</a> 
                        	<a href = "#" id = "next" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND CONTINUE</a>
                        	<a href = "#" id = "delete" class = "btn btn-lg btn-danger pull-right">DELETE</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    $('#back').click(function(e)
	{
		e.preventDefault();
		$('#action').val(2);
		$('#form').submit();
	});

	$('#next').click(function(e)
	{
		e.preventDefault();
		$('#action').val(1);
		$('#form').submit();
	});
	
	$('#delete').click(function(e)
	{
		e.preventDefault();
		var href = "delete_project.php?id=<?php print $id; ?>";
		window.location.href = href;
	});

    $('#copy').change(function()
    {
	      var name = $('#project_contact').val();
	      var phone = $('#project_contact_phone').val();
	      var email = $('#project_contact_email').val();
	      if($(this).is(':checked'))
	      {
	        $('#site_contact').val(name);
	        $('#site_contact_phone').val(phone);
	        $('#site_contact_email').val(email);
	      }
	      else
	      {
	        $('#site_contact').val('');
	        $('#site_contact_phone').val('');
	        $('#site_contact_email').val('');
	      }
    });
 
});
</script>

</body>

</html>