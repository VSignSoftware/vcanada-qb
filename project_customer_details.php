<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$pid = $_REQUEST['pid'];
$cid = $_REQUEST['cid'];
$cname = $_REQUEST['cname'];
$setup=$vujade->get_setup();

//print_r($_REQUEST);
//print $cid.'<br>';
if(!empty($cid))
{
	// $customer = $vujade->get_customer($cid);

	// test 1: get by list id	
	/*
	$current_client = $vujade->get_customer($cid,'ListID');
	if($current_client['error']=="0")
	{
		$customer=$current_client;
		$id_type=1;
	}
	*/

	// test 2: get by ID
	$current_client = $vujade->get_customer($cid,'ID');
	if($current_client['error']=="0")
	{
		$customer=$current_client;
	}

	//print $id_type.'<br>';

}
if(!empty($cname))
{
	$customer = $vujade->get_customer_by_name($cname);
}
if($customer['error']==0)
{
?>

	<div class="section">
      <label class="field">
        <input class="gui-input" type = "text" name = "company" id = "company" value = "<?php print $customer['name']; ?>" placeholder="Customer Name">
      </label>
    </div>

    <div class="section">
      <label class="field">
        <input class="gui-input" type = "text" name = "b_address_1" id = "b_address_1" value = "<?php print $customer['address_1']; ?>" placeholder="Billing Address Line 1">
      </label>
    </div>

    <div class="section">
      <label class="field">
        <input class="gui-input" type = "text" name = "b_address_2" id = "b_address_2" value = "<?php print $customer['address_2']; ?>" placeholder="Billing Address Line 2">
      </label>
    </div>

    <div class="section">
      <label class="field">
        <input class="gui-input" type = "text" name = "b_city" id = "b_city" value = "<?php print $customer['city']; ?>" placeholder="Billing Address Line 2">
      </label>
    </div>

	<div class="section">
      <label class="field select">
        <select id="b_state" name="b_state">
          <?php
          if(isset($customer['state']))
          {
              print '<option value = "'.$customer['state'].'" selected = "selected">'.$customer['state'].'</option>';
              print '<option value = "">-State-</option>';
          }
          else
          {
              //print '<option value = "">-State-</option>';
          }
          if($setup['country']=="USA")
          {
              ?>
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District Of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
              <option value="">---------------</option>
              <option value="">-Canadian Provinces-</option>
              <option value="Alberta">Alberta</option>
              <option value="British Columbia">British Columbia</option>
              <option value="Manitoba">Manitoba</option>
              <option value="New Brunswick">New Brunswick</option>
              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
              <option value="Northwest Territories">Northwest Territories</option>
              <option value="Nova Scotia">Nova Scotia</option>
              <option value="Nunavut">Nunavut</option>
              <option value="Ontario">Ontario</option>
              <option value="Prince Edward Island">Prince Edward Island</option>
              <option value="Quebec">Quebec</option>
              <option value="Saskatchewan">Saskatchewan</option>
              <option value="Yukon">Yukon</option>
            <?php 
	    	} 
	    	// canadian servers
	    	if($setup['country']=='Canada')
	    	{
	    		?>
	    		  <option value="">-Canadian Provinces-</option>
	              <option value="Alberta">Alberta</option>
	              <option value="British Columbia">British Columbia</option>
	              <option value="Manitoba">Manitoba</option>
	              <option value="New Brunswick">New Brunswick</option>
	              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
	              <option value="Northwest Territories">Northwest Territories</option>
	              <option value="Nova Scotia">Nova Scotia</option>
	              <option value="Nunavut">Nunavut</option>
	              <option value="Ontario">Ontario</option>
	              <option value="Prince Edward Island">Prince Edward Island</option>
	              <option value="Quebec">Quebec</option>
	              <option value="Saskatchewan">Saskatchewan</option>
	              <option value="Yukon">Yukon</option>
	              <option value="">---------------</option>
	              <option value="">-US States-</option>
	              <option value="AL">Alabama</option>
	              <option value="AK">Alaska</option>
	              <option value="AZ">Arizona</option>
	              <option value="AR">Arkansas</option>
	              <option value="CA">California</option>
	              <option value="CO">Colorado</option>
	              <option value="CT">Connecticut</option>
	              <option value="DE">Delaware</option>
	              <option value="DC">District Of Columbia</option>
	              <option value="FL">Florida</option>
	              <option value="GA">Georgia</option>
	              <option value="HI">Hawaii</option>
	              <option value="ID">Idaho</option>
	              <option value="IL">Illinois</option>
	              <option value="IN">Indiana</option>
	              <option value="IA">Iowa</option>
	              <option value="KS">Kansas</option>
	              <option value="KY">Kentucky</option>
	              <option value="LA">Louisiana</option>
	              <option value="ME">Maine</option>
	              <option value="MD">Maryland</option>
	              <option value="MA">Massachusetts</option>
	              <option value="MI">Michigan</option>
	              <option value="MN">Minnesota</option>
	              <option value="MS">Mississippi</option>
	              <option value="MO">Missouri</option>
	              <option value="MT">Montana</option>
	              <option value="NE">Nebraska</option>
	              <option value="NV">Nevada</option>
	              <option value="NH">New Hampshire</option>
	              <option value="NJ">New Jersey</option>
	              <option value="NM">New Mexico</option>
	              <option value="NY">New York</option>
	              <option value="NC">North Carolina</option>
	              <option value="ND">North Dakota</option>
	              <option value="OH">Ohio</option>
	              <option value="OK">Oklahoma</option>
	              <option value="OR">Oregon</option>
	              <option value="PA">Pennsylvania</option>
	              <option value="RI">Rhode Island</option>
	              <option value="SC">South Carolina</option>
	              <option value="SD">South Dakota</option>
	              <option value="TN">Tennessee</option>
	              <option value="TX">Texas</option>
	              <option value="UT">Utah</option>
	              <option value="VT">Vermont</option>
	              <option value="VA">Virginia</option>
	              <option value="WA">Washington</option>
	              <option value="WV">West Virginia</option>
	              <option value="WI">Wisconsin</option>
	              <option value="WY">Wyoming</option>
	    		<?php
	    	}
	    	?>
        </select>
        <i class="arrow double"></i>
        </label>
    </div>

	<div class="section">
      <label class="field">
        <input class="gui-input" type = "text" name = "b_zip" id = "b_zip" value = "<?php print $customer['zip']; ?>" placeholder="Billing Zip / Province" maxlength="10">
      </label>
    </div>

	<hr>

	<h3>Contact Person</h3>
	<?php
	// get by ID
	//print $customer['local_id'].'<br>';
	$contacts = $vujade->get_customer_contacts($customer['local_id']);
	if($contacts['error']=="0")
	{
		unset($contacts['error']);
		foreach($contacts as $c)
		{

			if( ($c['first_name']!='') && ($c['customer_id']!=0) )
			{

				if($c['database_id']==$project['client_contact_id'])
				{
					$selected='checked = "checked"';
				}
				else
				{
					$selected='';
				}

				print '<div class="">';
				//print $c['database_id'].'<br>';
				print '<input type = "radio" name = "customer_contact_id" id = "customer_contact_id" value = "'.$c['database_id'].'" class = "existing_contact"'.' '.$selected.'>';
				print ' <label for="customer_contact_id">'.$c['first_name'].' '.$c['last_name'].'</label>
				</div>';
			}
		}
	}
	?>

	<div class="">
		<input type = "radio" name = "customer_contact_id" id = "new_contact" value = "0" class = ""> 
		<label for="new_contact">New</label>
	</div>

	<div id = "new_contact_container" style = "display:none;margin-top:5px;">
		
		<div class="section">
	      <label class="field">
	        <input class="gui-input" type = "text" name = "nc_fname" id = "nc_fname" value = "" placeholder="First Name">
	      </label>
	    </div>

	    <div class="section">
	      <label class="field">
	        <input class="gui-input" type = "text" name = "nc_lname" id = "nc_lname" value = "" placeholder="Last Name">
	      </label>
	    </div>

	    <div class="section">
	      <label class="field">
	        <input class="gui-input" type = "text" name = "nc_phone" id = "nc_phone" value = "" placeholder="Phone">
	      </label>
	    </div>

	    <div class="section">
	      <label class="field">
	        <input class="gui-input" type = "text" name = "nc_email" id = "nc_email" value = "" placeholder="Email">
	      </label>
	    </div>
	</div>

	<input type = "hidden" name = "client_contact" id = "client_contact" value = "">

<?php 
}
else
{
	# problem...
}
?>

<script type="text/javascript">
$(document).ready(function()
{
	$('#new_contact').change(function(e)
    {
    	e.preventDefault();
    	//alert($(this).is(':checked'));
    	if($(this).is(':checked'))
    	{
    		$('#new_contact_container').show();
    	}
    	else
    	{
    		$('#new_contact_container').hide();
    	}
    	//$('#new_contact').hide();
    	$('#client_contact').val('');
	});

	$('.existing_contact').change(function(e)
    {
    	e.preventDefault();
    	$('#new_contact_container').hide();
	});

});	
</script>