<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$title = 'Purchase Orders - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Purchase Orders</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "reports.php" class = "btn btn-primary btn-sm" style = "width:100px;">Back</a>
					</div>
				</div>
	        	<div class="panel-body bg-light">
	        		<a href = "report_pos.php?option=1" class = "btn btn-primary btn-sm">Past Day</a> <a href = "report_pos.php?option=2" class = "btn btn-primary btn-sm">Past Week</a> <a class = "btn btn-primary btn-sm" href = "report_pos.php?option=3">Past Month</a>  
	        		<?php 
	        		if(isset($_REQUEST['option']))
	        		{
	        			$option=$_REQUEST['option'];
	        			if(!in_array($option, array(1,2,3)))
	        			{
	        				$option=1;
	        			}
	        		}
	        		else
	        		{
	        			$option=1;
	        		}

	        		$pos = $vujade->get_all_blank_purchase_orders($option);
					//print $pos['sql'];
					//unset($pos['sql']);
					if($pos['error']=="0")
					{
						unset($pos['error']);
						?>
			        		<!-- data table -->
			        		<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
									<thead>
										<tr style = "border-bottom:1px solid black;">
											<td valign = "top"><strong>Date</strong></td>
											<td valign = "top"><strong>PO#</strong></td>
											<td valign = "top"><strong>Vendor</strong></td>
											<td valign = "top"><strong>Items</strong></td>
											<td valign = "top"><strong>Jobs</strong></td>
											<td valign = "top"><strong>Cost</strong></td>
										</tr>
									</thead>

								    <tbody style = "font-size:14px;">
									<?php
									foreach($pos as $i)
									{
										//$link = 'vendor.php?id='.$i['database_id'];
								        
										// has a job number
										if(!empty($i['project_id']))
										{
											$link = 'project_purchase_orders.php?id='.$i['project_id'];
										}
										else
										{
											// is a blank po
											$link = 'edit_blank_purchase_order.php?poid='.$i['database_id'];
										}
								        
								        print '<tr class = "clickableRow-row">';

								        // date
								        print '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><a href = "'.$link.'">'.$i['date'].'</a></td>';

								        // po id
								        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = ""><a href = "'.$link.'">'.$i['purchase_order_id'].'</a></td>';

								        // vendor
								        //$vendor = $vujade->get_vendor($i['vendor_id'],'ListID');
								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['vendor_name'].'</td>';

								        // items
								        $items = $vujade->get_materials_for_purchase_order($i['database_id']);
								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';

								        if($items['error']=="0")
								        {
								        	unset($items['error']);
								        	$project_ids = array();
								        	foreach($items as $item)
								        	{
								        		$project_ids[]=$item['project_id'];
								        		print $item['description'].'<br>';
								        	}
								        }

								        print '</td>';

								        // jobs 
								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
								        if(count($project_ids)>0)
								        {
								        	foreach($project_ids as $pid)
								        	{
								        		print $pid.'<br>';
								        	}
								        }
								        print '</td>';

								        // cost
								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
								        print $i['total'];
								        print '</td>';

										print '</tr>';
									}

									?>
									</tbody>
							</table>

							<center>
							<a target="_blank" href = "print_report_pos.php?option=<?php print $option; ?>" class = "btn btn-primary">Print</a>
							</center>
						<?php
					}
					else
					{
						$vujade->set_error('No purchase orders found. ');
						$vujade->set_error($pos['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      },
      "order": [[ 0, "desc" ]]
    });

    $('.clickableRow').click(function(e) 
	{
		e.preventDefault();
		var href = $(this).attr('href');
		window.location.href=href;
	});

});
</script>

</body>
</html>