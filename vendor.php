<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// qb config
require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
$dsn = $vujade->qbdsn;

$vendors_permissions = $vujade->get_permission($_SESSION['user_id'],'Vendors');
if($vendors_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($vendors_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($vendors_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// site setup
$setup=$vujade->get_setup();

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

// 
$is_new=0;

// new
if($action==1)
{
	$title = "New Vendor - ";
	$is_new=1;
	$item=array();
	$tab=1;
}

// save new / update
if($action==2)
{
	$is_new=$_REQUEST['is_new'];
	$name=trim($_POST['name']);
	$name_ok=0;
	if(empty($name))
	{
		die('Vendor name cannot be blank.');
	}

	// test the name
	// name must be unique
	if($is_new==1)
	{
		// its a new vendor; search for existing vendors with the same name
		$test_vendor = $vendor=$vujade->get_vendor($name,'Name');
		if($test_vendor['error']=="0")
		{
			// means it is a duplicate
			$vujade->errors[]="Vendor name must be unique. The name you entered already exists for another vendor.";
			$title = "New Vendor - ";
			$is_new=1;
			$item=array();
			$tab=1;
		}
		else
		{
			$name_ok=1;
		}
	}
	else
	{
		// not a new vendor; see if name supplied is same or different than existing name
		$id = $_REQUEST['id'];
		$test_vendor=$vujade->get_vendor($id);
		if($name!=$test_vendor['name'])
		{
			$test_vendor_2 = $vendor=$vujade->get_vendor($name,'Name');
			if($test_vendor_2['error']=="0")
			{
				$vujade->errors[]="Vendor name must be unique. The name you entered already exists for another vendor.";
				$title = "New Vendor - ";
				$is_new=1;
				$item=array();
				$tab=1;
			}
			else
			{
				$name_ok=1;
			}
		}
		else
		{
			$name_ok=1;
		}
	}

	unset($test_vendor);
	unset($test_vendor_2);

	if($name_ok==1)
	{

		$rating=$_POST['rating'];
		$w9_complete=$_POST['w9_complete'];
		$tax_id=$_POST['tax_id'];
		if($is_new==1)
		{
			$vujade->create_row('quickbooks_vendor');
			$id = $vujade->row_id;
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$success2 = $vujade->update_row('quickbooks_vendor',$id,'ListID',$fakeid,'ID');
			$success2 = $vujade->update_row('quickbooks_vendor',$id,'IsActive','true','ID');
		}
		else
		{
			$id = $_REQUEST['id'];
		}

		$s[] = $vujade->update_row('quickbooks_vendor',$id,'Name',$name,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'CompanyName',$name,'ID');

		/*
		Vendor Tax Ident has to be a nine digit number. 
		It can have any number of dashes in it, but the total number of digits HAS to be 9.
		example 11-1111111.
		*/
		if(!empty($tax_id))
		{
			$test_tax_id = str_replace('-','',$tax_id);
			if(ctype_digit($tax_id))
			{
				$tti_count = strlen($tax_id);
				if($tti_count==9)
				{
					//$tax_id_is_valid=1;
				}
				else
				{
					$tax_id="";
				}
			}
			else
			{
				$tax_id="";
			}
		}

		$insurance_expires=$_POST['insurance_expires'];
		$address_1=$_POST['address_1'];
		$address_2=$_POST['address_2'];
		$city=$_POST['city'];
		$state=$_POST['state'];
		$zip=$_POST['zip'];
		$country=$_POST['country'];
		$phone=$_POST['phone'];
		$email=$_POST['email'];
		$contact=$_POST['contact'];
		$fax=$_POST['fax'];
		$ten99=$_POST['ten99'];

		$s1 = $vujade->update_row('quickbooks_vendor',$id,'rating',$rating,'ID');
		$s2 = $vujade->update_row('quickbooks_vendor',$id,'w9_complete',$w9_complete,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'IsVendorEligibleFor1099',$ten99,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorTaxIdent',$tax_id,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'insurance_expires',$insurance_expires,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_Addr1',$name,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_Addr2',$address_1,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_Addr3',$address_2,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_City',$city,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_State',$state,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_PostalCode',$zip,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'VendorAddress_Country',$country,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'Phone',$phone,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'Email',$email,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'Contact',$contact,'ID');
		$s[] = $vujade->update_row('quickbooks_vendor',$id,'Fax',$fax,'ID');

		// lat and long
		// address
		$test_address = $address_1.'+'.$city.'+'.$state.'+'.$zip;

		$test_address=str_replace(" ","+",$test_address);

		// curl post get lat long
		$k='pk.eyJ1IjoidG9kZGhhbW0xIiwiYSI6ImNpdDFoMnJ4ODBxNzcyemtoMDF1enkzbDIifQ.bM2v1sdtB628GDiimJEH_Q';
    	$url = "https://api.mapbox.com/geocoding/v5/mapbox.places/".$test_address.".json?country=us&access_token=".$k;

    	$ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);   
        
        $json = json_decode($output);
        $new_lat_long=$json->features[0]->center[1].','.$json->features[0]->center[0];

        $s[]=$vujade->update_row('quickbooks_vendor',$id,'latlong',$new_lat_long,'ID');

		$vujade->messages[]="Vendor Updated";
		$vendor=$vujade->get_vendor($id);

		if($is_new==1)
		{
			// add to quickbooks
			if(function_exists('date_default_timezone_set'))
			{
				//IMPORTANT: Make sure this matches your correct time zone.
				date_default_timezone_set('America/Los_Angeles');
			}
			
		    /* */
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_ADD_VENDOR, $id);
		}
		else
		{
			// quickbooks mod (can only be called if there is an edit sequence)
			// there is not already an unsynced add in the queue 
			$is_queued=$vujade->check_for_qb_queue_object($id);
			if( (!empty($vendor['edit_sequence'])) && (!$is_queued) )
			{
				if(function_exists('date_default_timezone_set'))
				{
					//IMPORTANT: Make sure this matches your correct time zone.
					date_default_timezone_set('America/Los_Angeles');
				}
				$Queue = new QuickBooks_WebConnector_Queue($dsn);
				$Queue->enqueue(QUICKBOOKS_MOD_VENDOR, $id);	
			}
		}

		$vujade->messages[]="Vendor Updated";
		$vendor=$vujade->get_vendor($id);
		if($vendor['error']!=0)
		{
			$vujade->page_redirect('error.php?m=3');
		}
		$title = $vendor['name']." - ";
		$tab=1;
	}
}

// delete
if($action==3)
{
	$id = $_REQUEST['id'];

	// set status to inactive
	$s[] = $vujade->update_row('quickbooks_vendor',$id,'IsActive','false','ID');

	// get vendor data
	$vendor=$vujade->get_vendor($id);

	// quickbooks mod (can only be called if there is an edit sequence)
	// there is not already an unsynced add in the queue 
	$is_queued=$vujade->check_for_qb_queue_object($id);
	if( (!empty($vendor['edit_sequence'])) && (!$is_queued) )
	{
		// mod to quickbooks
		if(function_exists('date_default_timezone_set'))
		{
			//IMPORTANT: Make sure this matches your correct time zone.
			date_default_timezone_set('America/Los_Angeles');
		}
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_MOD_VENDOR, $id);	
	}

	$vujade->page_redirect('vendors.php?m=1');
}

# save new note
if($action==4)
{
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$id = $_REQUEST['id'];
	$notes=$_POST['notes'];
	$vujade->create_row('customer_vendor_notes');
	$note_id = $vujade->row_id;
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'customer_vendor_id',$id);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'customer_or_vendor',2);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'date',date('m/d/Y'));
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'ts',strtotime('now'));
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'body',$notes);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'user_id',$_SESSION['user_id']);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'name_display',$employee['first_name'].' '.$employee['last_name']);
	$vujade->messages[]="Note Added";
	$tab=3;
	$action=0;
}

# delete document
if($action==5)
{
	$id = $_REQUEST['id'];
	$file_id = $_REQUEST['docid'];
	if(ctype_digit($file_id))
	{
		$filedata = $vujade->get_document($file_id);
		@unlink('uploads/vendor_documents/'.$id.'/'.$filedata['file_name']);
		$s[] = $vujade->delete_row('documents',$file_id);
	}
	$vujade->messages[]="Document Deleted";
	$vendor=$vujade->get_vendor($id);
	$tab=4;
}

# delete vendor contact
if($action==6)
{
	$id = $_REQUEST['id'];
	$contact_id = $_REQUEST['contact_id'];
	$s[] = $vujade->delete_row('vendor_contact',$contact_id);
	$vujade->messages[]="Contact Deleted";
	$vendor=$vujade->get_vendor($id);
	$tab=2;
}

// update note
if($action==7)
{
	$notes=$_POST['notes'];
	$id = $_POST['id'];
	$note_id = $_POST['note_id'];
	$_REQUEST['id'] = $id;
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'date',date('m/d/Y'));
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'ts',strtotime('now'));
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'body',$notes);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'user_id',$_SESSION['user_id']);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'name_display',$employee['first_name'].' '.$employee['last_name']);
	$vujade->messages[]="Note Updated";
	$tab=3;
	$action=0;
}

// delete note
if($action==8)
{
	$nid = $_REQUEST['note_id'];
	if(ctype_digit($nid))
	{
		$s[] = $vujade->delete_row('customer_vendor_notes',$nid);
		$vujade->messages[]="Note Deleted";
	}
	$action=0;
	$tab=3;
}

// default
if($action==0)
{
	$id = $_REQUEST['id'];
	$vendor=$vujade->get_vendor($id);
	if($vendor['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$title = $vendor['name']." - ";

	// notes 
	$notes = $vujade->get_notes($id,2);
}

// active tab
$valid = array(1,2,3,4,5,6);
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
}
if(!in_array($tab,$valid))
{
	$tab=6;
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $title; ?></a>
            </li>
          </ol>
        </div>

        <a href = "vendors.php" class = "btn btn-primary btn-sm pull-right">&laquo; Back</a>

      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="">
        	<?php
        	if(isset($_REQUEST['m']))
        	{
        		$m=$_REQUEST['m'];
        	}
        	if($m==1)
        	{
        		$vujade->messages[]="Vendor Contact Saved.";
        	}
			$vujade->show_messages();
			$vujade->show_errors();
			?>

<div class = "row">
	<div class = "col-md-12">

		<div class="panel" id="wide-panel">
				<div class="panel-heading" style = "border-bottom:none;">
				<ul class="nav panel-tabs panel-tabs-border panel-tabs-left" style = "border-bottom:none;">

				  <li class="<?php if($tab==6){ print 'active'; } ?>">
			        <a href="#tab6" data-toggle="tab">Overview</a>
			      </li>

			      <li class="<?php if($tab==1){ print 'active'; } ?>">
			        <a href="#tab1" data-toggle="tab">Details</a>
			      </li>

			      <?php
			      if($is_new==0)
			      {
			      	?>

			      <li class="<?php if($tab==2){ print 'active'; } ?>">
			        <a href="#tab2" data-toggle="tab">Contacts</a>
			      </li>
			      <li class="<?php if($tab==3){ print 'active'; } ?>">
			        <a href="#tab3" data-toggle="tab">Notes</a>
			      </li>
			      <li class="<?php if($tab==4){ print 'active'; } ?>">
			        <a href="#tab4" data-toggle="tab">Documents</a>
			      </li>
			      <li class="<?php if($tab==5){ print 'active'; } ?>">
			        <a href="#tab5" data-toggle="tab">Purchase Orders</a>
			      </li>

			      <?php } ?>

			    </ul>
			</div>
		
			<div class="panel-body">
				<div class="tab-content pn br-n">

<!-- tab 1: overview -->
<div id="tab6" class="tab-pane <?php if($tab==6){ print 'active'; } ?>">
    <div class = "row">
		<div class = "col-md-12">
			<?php
			print $vendor['address_1'].'<br>';
			if(!empty($vendor['address_2']))
			{
				print $vendor['address_2'].'<br>'; 
			}
			print $vendor['city'].', '.$vendor['state']. ' '.$vendor['zip'].'<br>';
			if(!empty($vendor['phone']))
			{
				print $vendor['phone'];
				print '<br>';
			}
			print '<br>';
			print '<strong><u>Contacts</u></strong><br>';
			$contacts = $vujade->get_vendor_contacts($vendor['vendor_id'],"vendor_id");
			//print_r($contacts);
			if($contacts['error']=="0")
			{
				unset($contacts['error']);
				print '<div class = "row">';
				print '<div class = "col-md-1">&nbsp;</div>';
				print '<div class = "col-md-11">';
				print '<table width = "500">';
				foreach($contacts as $c)
				{
					print '<tr>';
					print '<td colspan = "2">';
					print $c['fullname'];
					print '</td>';
					print '</tr>';
					print '<tr>';
					print '<td width = "10%">&nbsp;';
					print '</td>';
					print '<td width = "90%" >';
					print $c['label_1'].': ';
					print $c['phone1'];
					print '</td>';
					print '</tr>';

					print '<tr>';
					print '<td>&nbsp;';
					print '</td>';
					print '<td>';
					print $c['label_2'].': ';
					print $c['phone2'];
					print '</td>';
					print '</tr>';

					print '<tr>';
					print '<td>&nbsp;';
					print '</td>';
					print '<td>';
					print $c['label_3'].': ';
					print $c['phone3'];
					print '</td>';
					print '</tr>';

					print '<tr>';
					print '<td>&nbsp;';
					print '</td>';
					print '<td>';
					print $c['label_4'].': ';
					print $c['fax1'];
					print '</td>';
					print '</tr>';

					print '<tr>';
					print '<td colspan = "2">&nbsp;';
					print '</td>';
					print '</tr>';

					print '<tr>';
					print '<td colspan = "2">&nbsp;';
					//print_r($c);
					print '</td>';
					print '</tr>';

				}
				print '</table>';
				print '</div>';
				print '</div>';
			}
			?>
		
			<div>
				<?php
				print '<br><strong><u>Notes</u></strong><br>';
				if($notes['count']>0)
				{
					$has_notes=true;
					print '<table class = "table">';
					unset($notes['count']);
					unset($notes['error']);
					foreach($notes as $note)
					{
						print '<tr>';
						print '<td width = "25%">';
						print $note['date'];
						print '</td>';
						print '<td td width = "25%">';
						print $note['name_display'];
						print '</td>';
						print '<td td width = "50%">';
						print $note['body'];
						print '</td>';
						print '</tr>';
					}
					print '</table>';
				}	
				?>
			</div>

			<div class = "well" style = "margin-top:15px;">
				<strong><u>Documents</u></strong>
				<table class = "table">
					<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
						<td>Name</td>
						<td width = "">&nbsp;</td>
						<td width = "">&nbsp;</td>
						<td width = "">Date/Time</td>
						<td>&nbsp;</td>
					</tr>
					<?php
					# get all docs for this employee and display as table row with delete button
					$docs = $vujade->get_documents($id);
					if($docs['error']=="0")
					{
						unset($docs['error']);
						foreach($docs as $doc)
						{
							# print out the file details; this one is not in a folder
							print '<tr>';

							// name
							print '<td valign = "top" width = "60%">';
							print '<a href = "download_document.php?vendor=1&vendor_id='.$id.'&file_id='.$doc['database_id'].'">';
							print $doc['file_name'];
							print '</a>';
							print '</td>';

							// type
							print '<td valign = "top">';
							$type_data = $vujade->get_document_type($doc['file_type']);
							print $type_data['name'];
							print '</td>';

							# edit button
							print '<td valign = "top">&nbsp;';
							print '</td>';

							// date
							print '<td valign = "top">';

							$ts = strtotime($doc['ts']);
							$formatted_ts = date('m/d/Y',$ts);
							print $formatted_ts;
							print '</td>';

							// delete
							print '<td style = "text-align:right;" valign = "top">';
							//print '<a class = "btn btn-danger btn-xs delete-document" data-href="vendor_overview.php?id='.$id.'&action=1&docid='.$doc['database_id'].'" href = "#delete-document-modal">X</a>';
							print '&nbsp;';
							print '</td>';
							print '</tr>';
						}
					}
				?>
				</table>
			</div>

		</div>
	</div>
</div>

<!-- tab 2: info -->
<div id="tab1" class="tab-pane <?php if($tab==1){ print 'active'; } ?>">
    <div class="row">
      <div class="col-md-12">
      	<form id = "tab1form" method = "post" action = "<?php print $_SERVER['PHP_SELF']; ?>">
			<table class = "table">
				<tr class = "">
				<td class = "">
					ID
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "vendor_id" readonly = "readonly" value = "<?php print $vendor['vendor_id']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Name
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "name" value = "<?php print $vendor['name']; ?>" id = "name">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Rating
				</td>
				<td class = "">
					<select name = "rating" class = "form-control">

					 	<?php
					 	if(!empty($vendor['rating']))
					 	{
					 		print '<option value = "'.$vendor['rating'].'" selected = "selected">'.$vendor['rating'].'</option>';
					 	}
					 	?>
					 	<option value = "">-Select-</option>
					 	<option value = "A - 5">A - 5</option>
					 	<option value = "B - 4">B - 4</option>
					 	<option value = "C - 3">C - 3</option>
					 	<option value = "D - 2">D - 2</option>
					 	<option value = "F - 1">F - 1</option>
					 	<option value = "0 - Do not Use">0 - Do not Use</option>

					 </select>
					 <br>
					 <em>0 means never use and 5 is for the best vendors.</em>
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Address Line 1
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "address_1" value = "<?php print $vendor['address_1']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Address Line 2
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "address_2" value = "<?php print $vendor['address_2']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					City
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "city" value = "<?php print $vendor['city']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					State / Province
				</td>
				<td class = "">
					<select name = "state" class = "form-control" id = "state">
						<?php
						if(!empty($vendor['state']))
						{
							print '<option selected value = "'.$vendor['state'].'">'.$vendor['state'].'</option>';
						}
						if($setup['country']=="USA")
		          		{
		              	?>
			              <option value="AL">Alabama</option>
			              <option value="AK">Alaska</option>
			              <option value="AZ">Arizona</option>
			              <option value="AR">Arkansas</option>
			              <option value="CA">California</option>
			              <option value="CO">Colorado</option>
			              <option value="CT">Connecticut</option>
			              <option value="DE">Delaware</option>
			              <option value="DC">District Of Columbia</option>
			              <option value="FL">Florida</option>
			              <option value="GA">Georgia</option>
			              <option value="HI">Hawaii</option>
			              <option value="ID">Idaho</option>
			              <option value="IL">Illinois</option>
			              <option value="IN">Indiana</option>
			              <option value="IA">Iowa</option>
			              <option value="KS">Kansas</option>
			              <option value="KY">Kentucky</option>
			              <option value="LA">Louisiana</option>
			              <option value="ME">Maine</option>
			              <option value="MD">Maryland</option>
			              <option value="MA">Massachusetts</option>
			              <option value="MI">Michigan</option>
			              <option value="MN">Minnesota</option>
			              <option value="MS">Mississippi</option>
			              <option value="MO">Missouri</option>
			              <option value="MT">Montana</option>
			              <option value="NE">Nebraska</option>
			              <option value="NV">Nevada</option>
			              <option value="NH">New Hampshire</option>
			              <option value="NJ">New Jersey</option>
			              <option value="NM">New Mexico</option>
			              <option value="NY">New York</option>
			              <option value="NC">North Carolina</option>
			              <option value="ND">North Dakota</option>
			              <option value="OH">Ohio</option>
			              <option value="OK">Oklahoma</option>
			              <option value="OR">Oregon</option>
			              <option value="PA">Pennsylvania</option>
			              <option value="RI">Rhode Island</option>
			              <option value="SC">South Carolina</option>
			              <option value="SD">South Dakota</option>
			              <option value="TN">Tennessee</option>
			              <option value="TX">Texas</option>
			              <option value="UT">Utah</option>
			              <option value="VT">Vermont</option>
			              <option value="VA">Virginia</option>
			              <option value="WA">Washington</option>
			              <option value="WV">West Virginia</option>
			              <option value="WI">Wisconsin</option>
			              <option value="WY">Wyoming</option>
			              <option value="">---------------</option>
			              <option value="">-Canadian Provinces-</option>
			              <option value="Alberta">Alberta</option>
			              <option value="British Columbia">British Columbia</option>
			              <option value="Manitoba">Manitoba</option>
			              <option value="New Brunswick">New Brunswick</option>
			              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
			              <option value="Northwest Territories">Northwest Territories</option>
			              <option value="Nova Scotia">Nova Scotia</option>
			              <option value="Nunavut">Nunavut</option>
			              <option value="Ontario">Ontario</option>
			              <option value="Prince Edward Island">Prince Edward Island</option>
			              <option value="Quebec">Quebec</option>
			              <option value="Saskatchewan">Saskatchewan</option>
			              <option value="Yukon">Yukon</option>
			            <?php 
				    	} 
				    	// canadian servers
				    	if($setup['country']=='Canada')
				    	{
				    		?>
				    		  <option value="">-Canadian Provinces-</option>
				              <option value="Alberta">Alberta</option>
				              <option value="British Columbia">British Columbia</option>
				              <option value="Manitoba">Manitoba</option>
				              <option value="New Brunswick">New Brunswick</option>
				              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
				              <option value="Northwest Territories">Northwest Territories</option>
				              <option value="Nova Scotia">Nova Scotia</option>
				              <option value="Nunavut">Nunavut</option>
				              <option value="Ontario">Ontario</option>
				              <option value="Prince Edward Island">Prince Edward Island</option>
				              <option value="Quebec">Quebec</option>
				              <option value="Saskatchewan">Saskatchewan</option>
				              <option value="Yukon">Yukon</option>
				              <option value="">---------------</option>
				              <option value="">-US States-</option>
				              <option value="AL">Alabama</option>
				              <option value="AK">Alaska</option>
				              <option value="AZ">Arizona</option>
				              <option value="AR">Arkansas</option>
				              <option value="CA">California</option>
				              <option value="CO">Colorado</option>
				              <option value="CT">Connecticut</option>
				              <option value="DE">Delaware</option>
				              <option value="DC">District Of Columbia</option>
				              <option value="FL">Florida</option>
				              <option value="GA">Georgia</option>
				              <option value="HI">Hawaii</option>
				              <option value="ID">Idaho</option>
				              <option value="IL">Illinois</option>
				              <option value="IN">Indiana</option>
				              <option value="IA">Iowa</option>
				              <option value="KS">Kansas</option>
				              <option value="KY">Kentucky</option>
				              <option value="LA">Louisiana</option>
				              <option value="ME">Maine</option>
				              <option value="MD">Maryland</option>
				              <option value="MA">Massachusetts</option>
				              <option value="MI">Michigan</option>
				              <option value="MN">Minnesota</option>
				              <option value="MS">Mississippi</option>
				              <option value="MO">Missouri</option>
				              <option value="MT">Montana</option>
				              <option value="NE">Nebraska</option>
				              <option value="NV">Nevada</option>
				              <option value="NH">New Hampshire</option>
				              <option value="NJ">New Jersey</option>
				              <option value="NM">New Mexico</option>
				              <option value="NY">New York</option>
				              <option value="NC">North Carolina</option>
				              <option value="ND">North Dakota</option>
				              <option value="OH">Ohio</option>
				              <option value="OK">Oklahoma</option>
				              <option value="OR">Oregon</option>
				              <option value="PA">Pennsylvania</option>
				              <option value="RI">Rhode Island</option>
				              <option value="SC">South Carolina</option>
				              <option value="SD">South Dakota</option>
				              <option value="TN">Tennessee</option>
				              <option value="TX">Texas</option>
				              <option value="UT">Utah</option>
				              <option value="VT">Vermont</option>
				              <option value="VA">Virginia</option>
				              <option value="WA">Washington</option>
				              <option value="WV">West Virginia</option>
				              <option value="WI">Wisconsin</option>
				              <option value="WY">Wyoming</option>
				    		<?php
				    	}	
				    	?>	
					</select>
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Zip / Postal Code
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "zip" value = "<?php print $vendor['zip']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Country
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "country" value = "<?php print $vendor['country']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Phone
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "phone" value = "<?php print $vendor['phone']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Email
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "email" value = "<?php print $vendor['email']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					FAX
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "fax" value = "<?php print $vendor['fax']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					W9 Complete
				</td>
				<td class = "">
					<input class = ""  type = "radio" name = "w9_complete" value = "1" <?php if($vendor['w9_complete']==1){print'checked="checked"';}?>>Yes <input class = ""  type = "radio" name = "w9_complete" value = "0" <?php if($vendor['w9_complete']==0){print'checked="checked"';}?>>No
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					1099
				</td>
				<td class = "">
					<input class = "f"  type = "radio" name = "ten99" value = "1" <?php if($vendor['1099']==1){print'checked="checked"';}?>>Yes <input class = ""  type = "radio" name = "ten99" value = "0" <?php if($vendor['1099']==0){print'checked="checked"';}?>>No
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Tax ID
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "tax_id" id = "tax_id" value = "<?php print $vendor['tax_id']; ?>">
				</td>
				</tr>

				<tr class = "">
				<td class = "">
					Insurance Exp:
				</td>
				<td class = "">
					<input class = "form-control"  type = "text" name = "insurance_expires" value = "<?php print $vendor['insurance_expires']; ?>">
				</td>
				</tr>
				
				<tr class = "">
				<td class = "">
				&nbsp;
				</td>
				<td class = "">
				<input class = ""  type = "hidden" name = "is_new" value = "<?php print $is_new; ?>">
				<input class = ""  type = "hidden" name = "id" value = "<?php print $id; ?>">
				<input class = ""  type = "hidden" name = "action" value = "2">
				<input type = "submit" value = "SAVE" id = "tab1sbt" class = "btn btn-primary">
				<?php
				if($is_new==0)
				{
					?>
					<a id = "delete-vendor" href = "#delete-vendor-modal" class = "btn btn-danger pull-right">Delete</a>
				<?php } ?>
				</td>
				</tr>
			</table> 
	    </form> 
      </div>
    </div>
</div>

<!-- tab 3: contacts -->
<div id="tab2" class="tab-pane <?php if($tab==2){ print 'active'; } ?>">
	<div class="row">
	  <div class="col-md-12">
	  <a href = "vendor_contact.php?vendor_id=<?php print $id; ?>" class = "btn btn-xs btn-primary pull-right">ADD NEW CONTACT</a>
	  <table class = "table">
			<?php
			$contacts = $vujade->get_vendor_contacts($id);
			if($contacts['error']=="0")
			{
				unset($contacts['error']);
				foreach($contacts as $c)
				{
				?>
				<tr>
					<td valign = "top" width = "60%">
						<a href = "vendor_contact.php?vendor_id=<?php print $id; ?>&contact_id=<?php print $c['database_id']; ?>" class = "linknostyle">
						<?php print $c['fullname']; ?>
						</a>
					</td>
					
					<td valign = "top" width = "20%">
						<a href = "vendor_contact.php?vendor_id=<?php print $id; ?>&contact_id=<?php print $c['database_id']; ?>" class = "btn btn-xs btn-primary">
						EDIT
						</a>
					</td>
					
					<td valign = "top" width = "20%">
						<a href = "#delete-contact-modal" data-href = "vendor.php?id=<?php print $id; ?>&contact_id=<?php print $c['database_id']; ?>&action=6&tab=2" class = "btn btn-xs btn-danger delete-vendor-contact">
						DELETE
						</a>
					</td>
				</tr>
				<?php 
				} 
			} 
		?>
		</table>

	  </div>
	</div>
</div>

<!-- tab 4: notes -->
<div id="tab3" class="tab-pane <?php if($tab==3){ print 'active'; } ?>">
	<div class="row">
		<style>
			.conversation-date.active,
			.contact-name.active{
				background: #e5e5e5;
				text-decoration: none !important;
			}
			@media(max-width: 1024px)
			{
				.tab-pane{
					width: 768px;
				}
			}
		</style>

		<!-- panel left (small) -->
		<div class = "col-md-2">
			<div class="panel panel-primary panel-border top">
			  <div class="panel-heading">
			    <span class="panel-title">Notes</span>
			    <div class="widget-menu pull-right">
			    <a href = "note.php?action=1&type=2&id=<?php print $id; ?>" class = "btn btn-xs btn-primary plus" style = "">+</a>  
			    </div>
			  </div>
			  <div class="panel-body">
				<?php
				if($has_notes)
				{
					$show=1;
					unset($notes['count']);
					unset($notes['error']);
					$first_note=$notes[0];
					foreach($notes as $note)
					{
						print '<a class = "contact-name" href = "contact-'.$note['id'].'" id = "'.$note['id'].'">';
						print $note['date'].'</a><br>';
					}
				}
				else
				{
					$show=0;
				}
				?>  
			  </div>
			</div>
		</div>

		<!-- panel right (wider) -->
		<div class = "col-md-10">

			<?php
			if($show==1)
			{
				foreach($notes as $note)
				{
					?>
					<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $note['id']; ?>">
					  <div class="panel-heading">
					    <span class="panel-title"><?php print $note['date'].' '.$note['name_display']; ?>
					    </span>
					    <div class="widget-menu pull-right">
					      	<a href = "note.php?action=2&type=2&nid=<?php print $note['id']; ?>&id=<?php print $id; ?>" class = "plus btn btn-xs btn-primary">EDIT</a> 

							<a href = "#delete-note-form-<?= $note['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
					    </div>
					  </div>
					  <div class="panel-body conversation-body" id = "cb-<?php print $note['id']; ?>">
					  <?php print $note['body']; ?>
					  </div>
					</div>

					<!-- modal for delete note -->
					<div id="delete-note-form-<?= $note['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
						<h1>Delete Note</h1>
						<p>Are you sure you want to delete this note?</p>
						<p>
							<a id = "" class="btn btn-lg btn-danger"
							   href="vendor.php?action=8&note_id=<?= $note['id']; ?>&id=<?= $id; ?>&tab=3">YES</a>
							<a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>
						</p>
					</div>

					<?php
				}
			}
			?>
		</div>
	</div>
</div>

<!-- tab 5: docs -->
<div id="tab4" class="tab-pane <?php if($tab==4){ print 'active'; } ?>">
	<div class="row">
	  <div class="col-md-12">
	  <a href = "vendor_document.php?vendor_id=<?php print $id; ?>" class = "btn btn-primary btn-xs pull-right">New Document</a>
		<table class = "table">

			<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
				<td>Name</td>
				<td width = "">&nbsp;</td>
				<td width = "">&nbsp;</td>
				<td width = "">Date/Time</td>
				<td>&nbsp;</td>
			</tr>
			<?php
			# get vendor docs
			if($is_new!=1)
			{
				$docs = $vujade->get_documents($id);
				if($docs['error']=="0")
				{
					unset($docs['error']);
					foreach($docs as $doc)
					{
						# print out the file details; this one is not in a folder
						print '<tr>';

						// name
						print '<td valign = "top" width = "60%">';
						print '<a href = "download_document.php?vendor=1&vendor_id='.$id.'&file_id='.$doc['database_id'].'">';
						print $doc['file_name'];
						print '</a>';
						print '</td>';

						// type
						print '<td valign = "top">';
						$type_data = $vujade->get_document_type($doc['file_type']);
						print $type_data['name'];
						print '</td>';

						# edit button
						print '<td valign = "top">&nbsp;';
						print '</td>';

						// date
						print '<td valign = "top">';

						$ts = strtotime($doc['ts']);
						$formatted_ts = date('m/d/Y',$ts);
						print $formatted_ts;
						print '</td>';

						// delete
						print '<td style = "text-align:right;" valign = "top">';
						print '<a class = "btn btn-danger btn-xs delete-document" data-href="vendor.php?id='.$id.'&action=5&docid='.$doc['database_id'].'" href = "#delete-document-modal">X</a>';
						print '</td>';

						print '</tr>';

					}
				}
			}
		?>
		</table>
	  </div>
	</div>
</div>

<!-- tab 6: po's -->
<div id="tab5" class="tab-pane <?php if($tab==5){ print 'active'; } ?>">
	<div class="row">
	  <div class="col-md-12">
	  <?php
		# get purchase orders for this vendor
	    if($is_new!=1)
		{
			$pos = $vujade->get_purchase_orders($vendor['qb_id'],"VendorRef_ListID",0);
		}
		else
		{
			$pos['error']=1;
		}
		if($pos['error']=="0")
		{
			?>

		<div style = "overflow:auto;height:500px">
			<table class = "table">
			<thead>
				<tr style = "border-bottom:1px solid black;">
					<td width = "20%"><strong>PO#</strong></td>
					<td width = "20%"><strong>Date</strong></td>
					<td width = "20%"><strong>Type</strong></td>
					<td width = "20%"><strong>Total</strong></td>
					<td width = "20%"><strong>Print</strong></td>
				</tr>
			</thead>

			<tbody>
			<?php
			unset($pos['error']);
			foreach($pos as $po)
			{
				/* */
				if(!empty($po['project_id']))
				{
					//$link = 'edit_purchase_order.php?project_id='.$po['project_id'].'&poid='.$po['database_id'];
					$link = 'project_purchase_orders.php?id='.$po['project_id'].'&poid='.$po['database_id'];
					$print_link = 'print_purchase_order.php?id='.$po['project_id'].'&poid='.$po['database_id'];
				}
				else
				{
					$link = 'edit_purchase_order.php?poid='.$po['database_id'].'&is_blank=1';
					$print_link = 'print_purchase_order.php?poid='.$po['database_id'].'&is_blank=1';
				}

		        print '<tr class = "clickableRow-tr">';

		        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$po['purchase_order_id'].'</td>';

		        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$po['date'].'</td>';

		        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$po['type'].'</td>';

		        $t = 0;
		        if(!empty($po['total']))
		        {
		        	$t=$po['total'];
		        }
		        if(!empty($po['price_quote']))
		        {
		        	$t=$po['price_quote'];
		        }
		        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.@number_format($t,2,'.',',').'</td>';
		        print '</td>';

		        // print column
		        print '<td valign = "top" style = "">';
		        print '<a href = "'.$print_link.'" class = "btn btn-xs btn-primary" target = "_blank">PRINT</a><br>';

		        print '</td>';

				print '</tr>';
			}
		}
		?>
		</tbody>
    	</table>
    </div>
	</div>

	  </div>
	</div>
</div>
										     
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</section>
</section>

<!-- delete vendor modal -->
<div id="delete-vendor-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Vendor</h1>
	<p>Are you sure you want to delete this vendor?</p>
	<p><a id = "delete-vendor-yes" class="btn btn-lg btn-danger" href="vendor.php?action=3&id=<?php print $id; ?>">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- delete contact modal -->
<div id="delete-contact-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Contact</h1>
	<p>Are you sure you want to delete this contact?</p>
	<p><a id = "delete-contact-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- delete document modal -->
<div id="delete-document-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Document</h1>
	<p>Are you sure you want to delete this document?</p>
	<p><a id = "delete-document-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    $('#tab1sbt').click(function(e)
    {
    	var error = 0;
    	var name = $('#name').val();
    	name = name.trim();
    	if(name=="")
    	{	
    		error++;
    		alert('Company name cannot be empty');
    		return false;
    	}
    	if(error==0)
    	{
    		$('#tab1form').submit();
    	}
    });

    $(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    });

    // delete vendor modal
    $('#delete-vendor').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-vendor-modal',
		modal: true
	});

    // delete contact
    $('.delete-vendor-contact').click(function() 
    {
		$('#delete-contact-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-contact-modal',
		modal: true
	});

    // delete document
    $('.delete-document').click(function() 
    {
		$('#delete-document-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-document-modal',
		modal: true
	});

    // dismiss any modal
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	var HEADER = 60; // header height

	// focus on a contact
	$('.contact-name').click(function(e){
		e.preventDefault();

		var contact = $("#contact-"+this.id);

		if( $(this).hasClass('active') ){
			$(this).removeClass('active');
			contact.removeClass('panel-warning');
		} else {
			$('.contact-name').not( $(this) ).removeClass('active');
			$(this).addClass('active');

			$('.panel-contact').not( contact ).removeClass('panel-warning');
			contact.addClass('panel-warning');

			$('html, body').animate({
				scrollTop: contact.offset().top - HEADER
			}, 500);
		}
	});

	$('.delete-note').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '.delete-note-form',
		modal: true
	});

});
</script>

</body>
</html>