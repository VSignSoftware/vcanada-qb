<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);
$setup = $vujade->get_setup();
if($setup['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$buyout_overhead_percentage=$setup['buyout_overhead'];
$general_overhead_percentage=$setup['general_overhead'];

$s=array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# add 1 item to the costing materials
if($action==1)
{
	# get item details
	$item_id=$_REQUEST['item_id'];
	$date_entered=$_REQUEST['date_entered'];
	$item = $vujade->get_item($item_id);
	if($item['error']=="0")
	{
		$vujade->create_row('costing_materials');
		$row_id = $vujade->row_id;
		$s[]=$vujade->update_row('costing_materials',$row_id,'costing_id',$id);
		$s[]=$vujade->update_row('costing_materials',$row_id,'inventory_id',$item['inventory_id']);
		$s[]=$vujade->update_row('costing_materials',$row_id,'cost',$item['cost']);
		$s[]=$vujade->update_row('costing_materials',$row_id,'description',$item['description']);
		$s[]=$vujade->update_row('costing_materials',$row_id,'qty',1);
		$s[]=$vujade->update_row('costing_materials',$row_id,'date_entered',$date_entered);
	}
}
# delete 1 item
if($action == 2)
{
	$row_id=$_REQUEST['row_id'];
	$s[] = $vujade->delete_row('costing_materials',$row_id);
}

# update the entire item set
if($action == 3)
{
	$items = $_POST['items'];
    foreach($items as $str)
    {
    	$pieces = explode('^', $str);
    	$db_id=$pieces[0];
    	$qty=$pieces[1];
    	# update the quantity for each item
    	$s[] = $vujade->update_row('costing_materials',$db_id,'qty',$qty);
    }
}

$shop_order = $vujade->get_shop_order($id, 'project_id');

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 17;
$cmenu = 4;
$section = 3;
$title = "Costing Inventory - ".$project['project_id'].' - '.$project['site'].' - ';
$charset = '<meta charset="ISO-8859-1">';
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <?php require_once('project_left_tray.php'); ?>
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">
            <div class="pl15 pr15" style = "width:100%;">
              <?php require_once('project_right_tray.php'); ?>
              
              <div class="panel panel-primary panel-border top">
                <div class="panel-heading">
                  <span class="panel-title"></span>
                  <div class="widget-menu pull-right">
                    	<a href = "print_costing_inventory.php?id=<?php print $id; ?>" target = "_blank" class = "plus-update-labor btn btn-primary btn-sm">Print</a> 
                  </div>
                </div>
                <div class="panel-body bg-light">
                	<!-- existing -->
                	<div style = "height:300px;overflow:auto;" id = "e_items">
						<table class = "table" id = "estimate_items" style = "">
							<tr>
							<td>
								<strong>Date</strong>
							</td>
							<td>
								<strong>Item No.</strong> 
							</td>
							<td >
								<strong>Item</strong>
							</td>
							<td>
								<strong>Amount</strong>
							</td>
							<td>
								<strong>Qty</strong>
							</td>
							<td>
								<strong>Line Total</strong>
							</td>
							</tr>
							<?php
							$materials_cost = 0;
							$items = $vujade->get_materials_for_costing($id);
							if($items['error']=="0")
							{
								unset($items['error']);
								foreach($items as $i)
								{
									print '<tr>';
									print '<td>';
									print $i['date_entered'];
									print '</td>';
									print '<td>';
									print $i['inventory_id'];
									print '</td>';
									print '<td>';
									print $i['description'];
									print '</td>';
									print '<td>';
									print '$'. number_format($i['cost'],2,'.',',');
									print '</td>';
									print '<td>';
									print '<input type = "text" name = "item_qty_'.$i['database_id'].'" id = "item_qty_'.$i['database_id'].'" value = "'.$i['qty'].'" style = "width:30px;" class = "quantity">';
									print '</td>';
									print '<td>';
									$line = $i['cost']*$i['qty'];
									print '$'.number_format($line,2,'.',',').' ';

									# must have delete permission
									if($projects_permissions['delete']==1)
									{
										print '<a href = "#" id = "'. $i['database_id'].'" class = "plus-delete btn btn-xs btn-danger pull-right">Delete</a>';
									}
									else
									{
										// fix; make it a span with same id to fix jquery issue
										print '<span id = "'. $i['database_id'].'" class = "plus-delete btn btn-xs btn-danger pull-right">&nbsp;</span>';
									}

									print '<input type = "hidden" name = "item_id_'.$i['database_id'].'" id = "item_id_'.$i['database_id'].'">';
									print '</td>';
									print '</tr>';
									
									$materials_cost = $materials_cost + $line;
								}
								$i = $materials_cost * $project['indeterminant'];
								$materials_total = $materials_cost + $i;
								print '</table>';
								print '</div>'; // end of scroller div
								print '<table class = "table" style = "margin-top:10px;">';
								print '<tr>';
								print '<td><strong>';
								print 'Materials Subtotal: $'.number_format($materials_cost,2,'.',',');
								print '</strong></td>';
								print '<td><strong>';
								print 'Indeterminate: $'.number_format($i,2,'.',',');
								print '</strong></td>';
								print '<td><strong>';
								print 'Total: $'.number_format($materials_total,2,'.',',');
								print '</strong></td>';
								print '<td colspan = "3">';
								# must have edit permission 
								if($projects_permissions['edit']==1)
								{
									print '<a href = "#" class = "plus-update btn btn-xs btn-primary pull-right">Recalculate</a>';
								}

								print '<form id = "form1" method = "post" action = "costing_inventory.php"><input type = "hidden" name = "action" id = "action" value = ""><input type = "hidden" name = "id" id = "id" value = "'.$id.'"></form>';

								print '</td></tr>';
								print '</table>';
							}
							else
							{
								print '<tr><td colspan = "6"><div class = "alert alert-danger">No materials have been added yet. Use the table below to add materials to this project.</div></td></tr>';
								print '</table>';
								print '</div>';
							}
							?>

					<!-- searchable list of materials -->
					<hr>

					<div style = "height:70px;">

						<table class = "table">
							<tr style = "">
							<td colspan = "6">
							<form id = "form2">
							<input type = "text" name = "filter" id = "filter" class = "form-control" style = "width:200px;float:left;margin-right:5px;"> 
							<select name = "filter_field" id = "filter_field" class = "form-control" style = "width:200px;float:left;margin-right:5px;">
								<option value = "5">ALL</option>
								<option value = "1">ID</option>
								<option value = "2">Description</option>
								<option value = "3">Size</option>
								<option value = "4">Cost</option>
							</select> 
							<input type = "button" value = "Reset" id = "filter_reset" class = "btn btn-xs btn-primary pull-right">
							</td>
							</form>
							</tr>
						</table>
					</div>

				<div style = "height:300px;overflow:auto;">
					<table class = "table">
						<tr>
						<td colspan = "5" id = "inventory_table" style = "overflow:auto;height:300px;width:100%;">
						<?php
						$materials = $vujade->get_inventory($sort=1,$start=1,$no_limit=1);
						if($materials['error']=='0')
						{
							print '<div style = "">';
							unset($materials['error']);
							print '<table id = "itable" class = "table">';
							print '<tr>';
							print '<td><strong>Date</strong>';
							print '</td>';
							print '<td><strong>ID</strong>';
							print '</td>';
							print '<td><strong>Description</strong>';
							print '</td>';
							print '<td><strong>Size</strong>';
							print '</td>';
							print '<td><strong>Cost</strong>';
							print '</td>';
							print '<td>';
							print '&nbsp;';
							print '</td>';
							print '</tr>';

							foreach($materials as $m)
							{
								if(empty($m['inventory_id']))
								{
									$m['inventory_id']=$m['database_id'];
								}
								print '<tr>';
								print '<td>';
								print '<input type = "text" id = "de_'.$m['database_id'].'" style = "width:75px;" class = "dp">';
								print '</td>';
								print '<td>';
								print $m['inventory_id'];
								print '</td>';
								print '<td>';
								print $m['description'];
								print '</td>';
								print '<td>';
								print $m['size'];
								print '</td>';
								print '<td>';
								$c = trim($m['cost']);
								@$c = number_format($c,2,'.',',');
								//print '$'.number_format($m['cost'],2,'.',',');
								print '$'.$c;
								print '</td>';
								print '<td>';
								# must have create permission 
								if($projects_permissions['create']==1)
								{
									print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-xs btn-primary pull-right">+</a>';
								}
								print '</td>';
								print '</tr>';
							}
							print '</table>';
						}
						?>
						</div>
						</td>
						</tr>
						</table>
						</div>

					</div>
               	</div>
                </div>
            </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->
  <!-- BEGIN: PAGE SCRIPTS -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {
    	"use strict";
    
    	// Init Theme Core    
    	Core.init();

    	//var n = $('#notes').html();
    	//$("#notes").html($.trim(n));

    	// datepickers
	    $(".dp").datepicker();

	    // pressing any plus button on the available inventory table
		$('.plus').click(function(e)
		{
			e.preventDefault();
			var id = "<?php print $id; ?>";
			var item_id = this.id;
			var de = $('#de_'+item_id).val();
			var href = "costing_inventory.php?id="+id+"&action=1&item_id="+item_id+"&date_entered="+de;
			window.location.href = href;
		});

		// pressing any delete button on the items saved to the estimate table
		$('.plus-delete').click(function(e)
		{
			e.preventDefault();
			var id = "<?php print $id; ?>";
			var row_id = this.id;
			var href = "costing_inventory.php?id="+id+"&action=2&row_id="+row_id;
			window.location.href = href;
		});

		// recalculate button
		$('.plus-update').click(function()
		{
			// this gets each item quantity
			var qtys = $('.quantity').map(function(index) 
			{
			    return this.value; 
			});
			
			// this gets each item id
			var ids = $('.plus-delete').map(function(index) 
			{
			    return this.id; 
			});

			// this combines both into an array with the id as the key, quantity as the value
			var array3 = {};
			$.each(ids, function(i) {
			    array3[ids[i]] = qtys[i];
			});

			// this loops through all then creates hidden form fields containing only the id and the qty
			$.each(array3, function( index, value ) 
			{
				//alert( index + ": " + value );
				//concat = concat+
				$('<input>').attr({
				    type: 'hidden',
				    id: 'items',
				    name: 'items[]',
				    value: index+"^"+value
				}).appendTo('#form1');
			});

			$('#action').val('3');
			$('#form1').submit();
		});

	    // filter the list of materials
		$("#form2").keypress(function (e) {
	        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) 
	        {
	            $('button[type=submit] .default').click();
				var filter = $('#filter').val();
				var filter_field = $('#filter_field').val();
				var loading = '<img src = "images/loading.gif" width = "100" height = "100" style = "margin-left:100px;margin-top:100px;">';
				$('#itable').remove();
				$('#inventory_table').html('');
				$('#inventory_table').html(loading);
				$.post( "jq.costing_materials_filter.php", { filter: filter, filter_field: filter_field, reset: 0, id: "<?php print $id; ?>"})
				  .done(function( data ) 
				  {
				  		$('#inventory_table').html(data);
				  });    
	            return false;
	        }
	    });

		$('#filter_reset').click(function(e)
		{
			e.preventDefault();
			var loading = '<img src = "images/loading.gif" width = "100" height = "100" style = "margin-left:100px;margin-top:100px;">';
			$('#itable').remove();
			$('#inventory_table').html('');
			$('#inventory_table').html(loading);
			$.post( "jq.costing_materials_filter.php", { reset: 1, id: "<?php print $id; ?>" })
			  .done(function( data ) 
			  {
			  		$('#inventory_table').html(data);
			  });
		});
    
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>
