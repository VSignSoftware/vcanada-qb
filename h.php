<?php
$logo = $vujade->get_logo();
$setup = $vujade->get_setup();
$title.=$setup['company_name'];
if(!isset($charset))
{
	$charset="utf-8";
}
?>

<!DOCTYPE html>
<html>

<head>
  <meta charset="<?php print $charset; ?>">
  <title><?php print $title; ?></title>
  <meta name="keywords" content="<?php print $title; ?>" />
  <meta name="description" content="<?php print $title; ?>">
  <meta name="author" content="V Sign Software">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600'>

  <!-- popup CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/magnific/magnific-popup.css">

  <!-- datepicker CSS -->
  <link rel="stylesheet" type="text/css" href="vendor/plugins/daterange/daterangepicker.css">
  <link rel="stylesheet" type="text/css" href="vendor/plugins/datepicker/css/bootstrap-datetimepicker.css">

	<script>
	function product_of_the_week()
	{
		window.open("http://vsignsoftware.com/product_of_the_week.php");
	}
	</script>

  <!-- Glyphicons Pro CSS(font) -->
  <link rel="stylesheet" type="text/css" href="assets/fonts/glyphicons-pro/glyphicons-pro.css">

  <!-- Icomoon CSS(font) -->
  <link rel="stylesheet" type="text/css" href="assets/fonts/icomoon/icomoon.css">
  
  <link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome/font-awesome.css">

  <link rel="stylesheet" type="text/css" href="vendor/plugins/slick/slick.css">

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <?php
  // add calendar css
  if($calendar)
  {
  		print '<link rel="stylesheet" type="text/css" href="vendor/plugins/fullcalendar/fullcalendar.min.css" media="screen">';
  }
  ?>

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

</head>

<?php
if($vujade->default_body_class)
{
	print '<body class="dashboard-page sb-l-o sb-r-c">';
}
else
{
	print '<body class="">';
}
?>

<!-------------------------------------------------------------+ 
  <body> Helper Classes: 
---------------------------------------------------------------+ 
  '.sb-l-o' - Sets Left Sidebar to "open"
  '.sb-l-m' - Sets Left Sidebar to "minified"
  '.sb-l-c' - Sets Left Sidebar to "closed"

  '.sb-r-o' - Sets Right Sidebar to "open"
  '.sb-r-c' - Sets Right Sidebar to "closed"
---------------------------------------------------------------+
 Example: <body class="example-page sb-l-o sb-r-c">
 Results: Sidebar left Open, Sidebar right Closed
--------------------------------------------------------------->

  <!-- Start: Main -->
  <div id="main">

    <!-----------------------------------------------------------------+ 
       ".navbar" Helper Classes: 
    -------------------------------------------------------------------+ 
       * Positioning Classes: 
        '.navbar-static-top' - Static top positioned navbar
        '.navbar-static-top' - Fixed top positioned navbar

       * Available Skin Classes:
         .bg-dark    .bg-primary   .bg-success   
         .bg-info    .bg-warning   .bg-danger
         .bg-alert   .bg-system 
    -------------------------------------------------------------------+
      Example: <header class="navbar navbar-fixed-top bg-primary">
      Results: Fixed top navbar with blue background 
    ------------------------------------------------------------------->

    <!-- Start: Header -->
    <header class="navbar navbar-fixed-top">
      <div class="navbar-branding">
        <a class="navbar-brand" href="dashboard.php">
          <?php 
          if(file_exists($logo['url']))
          {
              print '<img src = "'.$logo['url'].'" width = "150" class="img-rounded" style = "margin-left:0px;margin-bottom:5px;margin-top:5px;">';
          }
          else
          {
              print '<h2>'.$setup['company_name'].'<h2>';
          } 
          ?>
        </a>
        <span id="toggle_sidemenu_l" class="ad ad-lines"></span>
      </div>
      <ul class="nav navbar-nav navbar-left">
      </ul>
      <form class="navbar-form navbar-left navbar-search" role="search" method = "post" action = "search.php">
        <div class="form-group">
          <input type="text" name = "search" class="form-control" placeholder="Search..." value="">
        </div>
        <input type = "hidden" name = "action" value = "5">
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle fw600 p15" data-toggle="dropdown"><?php 
          	  if( (isset($_SESSION['admin_id'])) && ($_SESSION['admin_id']!=$_SESSION['user_id']) )
			  {
			  	print '(';
          	  	print $emp['fullname']; 
          	  	print ')';
          	  }
          	  else
          	  {
          	  	print $emp['fullname']; 
          	  }
          	  ?>
            
            <span class="caret caret-tp hidden-xs"></span>
          </a>
          <ul class="dropdown-menu list-group dropdown-persist w250" role="menu">
          	<?php 
          	  if(isset($_SESSION['admin_id']))
			  {
			  		?>
			  		<li class="list-group-item">
		              <a href="admin.php" class="animated animated-short fadeInUp">
		                <span class="fa fa-gear"></span> Admin Settings </a>
		            </li>
			  		<?php
          	  }
          	  ?>

            <li class="list-group-item">
              <a href="my_account.php" class="animated animated-short fadeInUp">
                <span class="fa fa-gear"></span> Account Settings </a>
            </li>

            <li class="dropdown-footer">
              <a href="logout.php" class="">
              <span class="fa fa-power-off pr5"></span> Logout </a>
            </li>
          </ul>
        </li>
      </ul>

    </header>
    <!-- End: Header -->

    <!-----------------------------------------------------------------+ 
       "#sidebar_left" Helper Classes: 
    -------------------------------------------------------------------+ 
       * Positioning Classes: 
        '.affix' - Sets Sidebar Left to the fixed position 

       * Available Skin Classes:
         .sidebar-dark (default no class needed)
         .sidebar-light  
         .sidebar-light.light   
    -------------------------------------------------------------------+
       Example: <aside id="sidebar_left" class="affix sidebar-light">
       Results: Fixed Left Sidebar with light/white background
    ------------------------------------------------------------------->

    <!-- Start: Sidebar Left -->
    <aside id="sidebar_left" class="nano nano-primary affix">

      <!-- Start: Sidebar Left Content -->
      <div class="sidebar-left-content nano-content">

        <!-- Start: Sidebar Header -->
        <header class="sidebar-header">

        </header>
        <!-- End: Sidebar Header -->

        <!-- Start: Sidebar Left Menu -->
        <ul class="nav sidebar-menu">
          <li class="sidebar-label pt20">Menu</li>
          <li class="<?php if($section==1) print 'active'; ?>">
            <a href="dashboard.php">
              <span class="glyphicon glyphicon-home"></span>
              <span class="sidebar-title">Dashboard</span>
            </a>
          </li>

          <li class="<?php if($section==2) print 'active'; ?>">
            <a href="new_project.php">
              <span class="glyphicon glyphicon-file"></span>
              <span class="sidebar-title">New Project</span>
            </a>
          </li>

          <li class="<?php if($section==3) print 'active'; ?>">
            <a href="search.php?action=3">
              <span class="glyphicons glyphicons-folder_open"></span>
              <span class="sidebar-title">Existing Projects</span>
            </a>
          </li>

          <li class="<?php if($section==4) print 'active'; ?>">
            <a href="queues.php">
              <span class="imoon imoon-barcode"></span>
              <span class="sidebar-title">Queues</span>
            </a>
          </li>

          <li class="<?php if($section==5) print 'active'; ?>">
            <a href="schedule.php">
              <span class="glyphicon glyphicon-calendar"></span>
              <span class="sidebar-title">Schedule / Calendar</span>
            </a>
          </li>

          <li class="<?php if($section==6) print 'active'; ?>">
            <a href="reports.php">
              <span class="glyphicons glyphicons-stats"></span>
              <span class="sidebar-title">Reports</span>
            </a>
          </li>

          <li class="<?php if($section==7) print 'active'; ?>">
            <a href="records.php">
              <span class="glyphicons glyphicons-table"></span>
              <span class="sidebar-title">Records</span>
            </a>
          </li>

          <li class="<?php if($section==8) print 'active'; ?>">
            <a href="forms.php">
              <span class="glyphicons glyphicons-edit"></span>
              <span class="sidebar-title">Forms</span>
            </a>
          </li>

          <li class="<?php if($section==9) print 'active'; ?>">
            <a href="accounting.php">
              <span class="glyphicon glyphicon-usd"></span>
              <span class="sidebar-title">Accounting</span>
            </a>
          </li>

          <?php 
          if($section==1)
          {
            ?>
            <li class="sidebar-proj">
              <a href="#" id = "tasks">
                <span class="glyphicons glyphicons-inbox"></span>
                <span class="sidebar-title">My Tasks</span>
                <span class="sidebar-title-tray">
                  <span class="label label-xs bg-danger" id = "badge-task-count"></span>
                </span>
              </a>
            </li>

            <li class="sidebar-proj">
              <a href="#" id = "designs">
                <span class="imoon imoon-pencil"></span>
                <span class="sidebar-title">Design Requests</span>
                <span class="sidebar-title-tray">
                  <span class="label label-xs bg-danger" id = "badge-design-count"></span>
                </span>
              </a>
            </li>

            <li class="sidebar-proj">
              <a href="#" id = "estimates">
                <span class="glyphicons glyphicons-coins"></span>
                <span class="sidebar-title">Estimate Requests</span>
                <span class="sidebar-title-tray">
                  <span class="label label-xs bg-danger" id = "badge-estimate-count"></span>
                </span>
              </a>
            </li>
          <?php 
          }
          else
          {
              ?>
              <li class="sidebar-proj">
                <a href="dashboard.php?show=1" id = "tasks">
                  <span class="glyphicons glyphicons-inbox"></span>
                  <span class="sidebar-title">My Tasks</span>
                  <span class="sidebar-title-tray">
                    <span class="label label-xs bg-danger" id = "badge-task-count"><?php print $_SESSION['task_count']; ?></span>
                  </span>
                </a>
              </li>

              <li class="sidebar-proj">
                <a href="dashboard.php?show=2" id = "designs">
                  <span class="imoon imoon-pencil"></span>
                  <span class="sidebar-title">Design Requests</span>
                  <span class="sidebar-title-tray">
                    <span class="label label-xs bg-danger" id = "badge-design-count"><?php print $_SESSION['design_count']; ?></span>
                  </span>
                </a>
              </li>

              <li class="sidebar-proj">
                <a href="dashboard.php?show=3" id = "estimates">
                  <span class="glyphicons glyphicons-coins"></span>
                  <span class="sidebar-title">Estimate Requests</span>
                  <span class="sidebar-title-tray">
                    <span class="label label-xs bg-danger" id = "badge-estimate-count"><?php print $_SESSION['estimate_count']; ?></span>
                  </span>
                </a>
              </li>
              <?php
          }
          ?>

	          <li class="sidebar-proj">
	              &nbsp; 
	          </li>
			  <div class="sidebar-toggle-mini">
					<a href="#">
					<span class="fa fa-sign-out"></span>
					</a>
			  </div>
	          <li class="sidebar-proj">
	              
	          	<style>
	          	.alert.radius-bordered 
	          	{
				    -webkit-border-radius: 3px;
				    -webkit-background-clip: padding-box;
				    -moz-border-radius: 3px;
				    -moz-background-clip: padding;
				    border-radius: 3px;
				    background-clip: padding-box;
				}
				.product_of_the_week:hover
				{
					cursor:pointer;
				}
	          	</style>
	          		<!--
		          	<div class="product_of_the_week alert alert-micro alert-border-left alert-primary radius-bordered" style = "margin-top:200px;margin-left:10px;margin-right:10px;" onclick=product_of_the_week()>
						<a href="http://vsignsoftware.com/product_of_the_week.php" class="alert-link" target = "_blank">Product of the Week</a>
					</div>
					-->
	          </li>

        </ul>
        <!-- End: Sidebar Menu -->

          <!-- Start: Sidebar Collapse Button -->
          
          <!-- End: Sidebar Collapse Button -->

      </div>
      <!-- End: Sidebar Left Content -->

    </aside>
    <!-- End: Sidebar Left -->