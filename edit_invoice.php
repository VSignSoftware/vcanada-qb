<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

# permissions
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$setup = $vujade->get_setup();
$state_sales_tax=$setup['state_sales_tax'];
$invoice_setup = $vujade->get_invoice_setup();

$s = array();

// shop order
$shop_order = $vujade->get_shop_order($id,$idcol="project_id");

$invoice_db_id=$_REQUEST['invoice_db_id'];
$invoiceid=$invoice_db_id;

// page has been posted for processing
if(isset($_REQUEST['action']))
{
	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	$invoice=$vujade->get_invoice($invoice_db_id);
	if($invoice['error']=="0")
	{
		$lump_sum=str_replace(',', '', $_POST['lump_sum']);
		$parts = str_replace(',', '', $_POST['parts']);
		$labor = str_replace(',', '', $_POST['labor']);
		$tax_rate=$_POST['tax_rate'];
		$tax_amount=str_replace(',', '', $_POST['tax_amount']);
		$change_orders=str_replace(',', '', $_POST['change_orders']);
		$engineering=str_replace(',', '', $_POST['engineering']);
		$permits=str_replace(',', '', $_POST['permits']);
		$permit_acquisition=str_replace(',', '', $_POST['permit_acquisition']);
		$shipping=str_replace(',', '', $_POST['shipping']);
		$retention=str_replace(',', '', $_POST['retention']);
		$retention=str_replace('-', '', $retention);
		
		// discount
		$discount=str_replace(',', '', $_POST['discount']);
		$discount=str_replace('-', '', $discount);
		$discount='-'.$discount;
		
		// deposit
		$deposit=$_POST['deposit'];
		$deposit=str_replace(',', '', $deposit);
		$deposit=str_replace('-', '', $deposit);
		$deposit='-'.$deposit;

		$cc_processing_fees = str_replace(',', '', $_POST['cc_processing_fees']);
		$po_id = $_POST['po_id'];
		$memo = $_POST['description'];

		$invoice=$vujade->get_invoice($invoice_db_id);
		$dbid = $invoice_db_id;

		// update the parent record
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'TxnDate',$qbdate);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ts',strtotime($date));
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_description',$memo);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'tax',$tax_rate);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_web_sales_tax',$tax_amount);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'po_number',$po_id);

		//$memo=strip_tags($memo);
		//$memo=str_replace('&nbsp','',$memo);
		//$memo=str_replace(';','',$memo);
		
		# get correct customer info for this project
		$listiderror=0;
		$localiderror=0;
		// try to get by list id
	    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
	    if($customer1['error']!="0")
	    {
	    	$listiderror++;
	    	unset($customer1);
	    }
	    else
	    {
	    	$customer=$customer1;
	    }
	    // try to get by local id
	    $customer2 = $vujade->get_customer($project['client_id'],'ID');
	    if($customer2['error']!="0")
	    {
	    	$localiderror++;
	    	unset($customer2);
	    }
	    else
	    {
	    	$customer=$customer2;
	    }

	    $iderror=$listiderror+$localiderror;

	    if($iderror<2)
	    {
	    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
	    }
	    else
	    {
	    	// can't find customer or no customer on file
	    }
	    
	    //$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'CustomerRef_ListID',$customer['database_id']);

	    $s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_Addr1',$project['site']);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_Addr2',$project['address_1']);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_City',$project['city']);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_State',$project['state']);
		$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_PostalCode',$project['zip']);

		// update line items or create new ones

		// variations

	    $type = $invoice['type_1'];
	    $type2 = $invoice['type_2'];

		// 1: illuminated and lump sum
		if( ($type==1) && ($type2==1) )
		{
			$labor_only = $lump_sum - ($lump_sum*$invoice_setup['sale_price_1']);
			$parts_cost = $lump_sum * $invoice_setup['sale_price_1'];

			$test1_arr = $vujade->get_invoice_line_items($invoice_db_id,'Labor Only');
			if($test1_arr['error']=="0")
			{
				$tempid=$test1_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$labor_only,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}

			$test2_arr = $vujade->get_invoice_line_items($invoice_db_id,'Parts');
			if($test2_arr['error']=="0")
			{
				$tempid=$test2_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$parts_cost,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}

			// non-taxable co
			$ntco = $change_orders;
			$change_orders=$change_orders*$state_sales_tax;
			$ntco = $ntco-$change_orders;

			$test3_arr = $vujade->get_invoice_line_items($invoice_db_id,'Non-Taxable CO');
			if($test3_arr['error']=="0")
			{
				$tempid=$test3_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$ntco,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Non-Taxable CO','$ntco',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug);
			}
		}

		// 2: illuminated sign parts and labor
		if( ($type==1) && ($type2==2) )
		{
			$labor_only = $labor;
			$parts_cost = $parts;

			$test1_arr = $vujade->get_invoice_line_items($invoice_db_id,'Labor Only');
			if($test1_arr['error']=="0")
			{
				$tempid=$test1_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$labor_only,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}

			$test2_arr = $vujade->get_invoice_line_items($invoice_db_id,'Parts');
			if($test2_arr['error']=="0")
			{
				$tempid=$test2_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$parts_cost,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}

			// split change orders
			// non-taxable co
			$ntco = $change_orders;
			$change_orders=$change_orders*$state_sales_tax;
			$ntco = $ntco-$change_orders;

			$test3_arr = $vujade->get_invoice_line_items($invoice_db_id,'Non-Taxable CO');
			if($test3_arr['error']=="0")
			{
				$tempid=$test3_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$ntco,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Non-Taxable CO','$ntco',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug);
			}
		}

		// 3: non-illuminated lump sum
		if( ($type==2) && ($type2==1) )
		{
			$selling_price = $lump_sum*$invoice_setup['sale_price_2'];

			$test1_arr = $vujade->get_invoice_line_items($invoice_db_id,'Manufacture & Install');
			if($test1_arr['error']=="0")
			{
				$tempid=$test1_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$selling_price,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Manufacture & Install','$selling_price',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}
		}

		// 4: non-illuminated sign parts and labor
		if( ($type==2) && ($type2==2) )
		{
			$labor_only = $labor;
			$parts_cost = $parts;

			$test1_arr = $vujade->get_invoice_line_items($invoice_db_id,'Labor Only');
			if($test1_arr['error']=="0")
			{
				$tempid=$test1_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$labor_only,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}

			$test2_arr = $vujade->get_invoice_line_items($invoice_db_id,'Parts');
			if($test2_arr['error']=="0")
			{
				$tempid=$test2_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$parts_cost,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			} 
		}

		// 5: service with parts and labor
		if( ($type==3) && ($type2==2) )
		{
			$labor_only = $labor;
			$parts_cost = $parts;

			$test1_arr = $vujade->get_invoice_line_items($invoice_db_id,'Labor Only');
			if($test1_arr['error']=="0")
			{
				$tempid=$test1_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$labor_only,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}

			$test2_arr = $vujade->get_invoice_line_items($invoice_db_id,'Parts');
			if($test2_arr['error']=="0")
			{
				$tempid=$test2_arr[0]['database_id'];
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$parts_cost,'TxnLineID');
			}
			else
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
				$vujade->generic_query($sql,$debug); 
			}
		}
		
		// change orders
		$co_arr = $vujade->get_invoice_line_items($invoice_db_id,'Change Order');
		if($co_arr['error']=="0")
		{
			$tempid=$co_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$change_orders,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Change Order','$change_orders',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// engineering
		$engineering_arr = $vujade->get_invoice_line_items($invoice_db_id,'Engineering');
		if($engineering_arr['error']=="0")
		{
			$tempid=$engineering_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$engineering,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Engineering','$engineering',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// permits
		$permits_arr = $vujade->get_invoice_line_items($invoice_db_id,'Permits');
		if($permits_arr['error']=="0")
		{
			$tempid=$permits_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$permits,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Permits','$permits',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// permit acq
		$permit_acquisition_arr = $vujade->get_invoice_line_items($invoice_db_id,'Permit Acquisition');
		if($permit_acquisition_arr['error']=="0")
		{
			$tempid=$permit_acquisition_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$permit_acquisition,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Permit Acquisition','$permit_acquisition',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// shipping
		$shipping_arr = $vujade->get_invoice_line_items($invoice_db_id,'Shipping');
		if($shipping_arr['error']=="0")
		{
			$tempid=$shipping_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$shipping,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Shipping','$shipping',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// discount
		$discount_arr = $vujade->get_invoice_line_items($invoice_db_id,'Discount');
		if($discount_arr['error']=="0")
		{
			$tempid=$discount_arr[0]['database_id'];
			$res=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$discount,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Discount','$discount',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// retention
		$retention_arr = $vujade->get_invoice_line_items($invoice_db_id,'Retention');
		if($retention_arr['error']=="0")
		{
			$tempid=$retention_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$retention,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Retention','$retention',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// deposit
		$deposit_arr = $vujade->get_invoice_line_items($invoice_db_id,'Deposit');
		if($deposit_arr['error']=="0")
		{
			$tempid=$deposit_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$deposit,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Deposit','$deposit',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}

		// tax amount
		/*
		$sales_tax_arr = $vujade->get_invoice_line_items($invoice_db_id,'Sales Tax');
		if($sales_tax_arr['error']=="0")
		{
			$tempid=$sales_tax_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$tax_amount,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','Sales Tax','$tax_amount',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}
		*/	

		// credit card processing fees
		$cc_arr = $vujade->get_invoice_line_items($invoice_db_id,'CC Processing Fee');
		if($cc_arr['error']=="0")
		{
			$tempid=$cc_arr[0]['database_id'];
			$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$tempid,'Amount',$cc_processing_fees,'TxnLineID');
		}
		else
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`IsNew`) VALUES ('$invoice_db_id','CC Processing Fee','$cc_processing_fees',1,'$fakeid','true')";
			$vujade->generic_query($sql,$debug); 
		}	

		// send the invoice data to quickbooks
		// qb config
		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;

		// create item on qb server
		if(function_exists('date_default_timezone_set'))
		{
			date_default_timezone_set('America/Los_Angeles');
		}
		
		if (!QuickBooks_Utilities::initialized($dsn))
		{	
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}

		// insert into the quickbooks vendor table
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_MOD_INVOICE, $invoice_db_id);

		$vujade->page_redirect('project_invoices.php?id='.$id.'&invoiceid='.$invoice_db_id);
	}
}
else
{
	// page has not yet been posted for processing
	$invoice=$vujade->get_invoice($invoice_db_id);
	if($invoice['error']=="0")
	{
		// these come from the parent invoice record
		$date=$invoice['date'];
		$memo=$invoice['memo'];
		$po_id = $invoice['po_number'];
		if(empty($po_id))
		{
			$po_id = $shop_order['po_number'];
		}

		// subtotal 1 (selling price)
		$sp=0;

		// invoice types
		// 1: illuminated and lump sum
		if( ($invoice['type_1']==1) && ($invoice['type_2']==1) )
		{
			// labor
			$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			// total
			$sp = $labor+$parts; 

			$parts=0;

			// selling price text
			$show_lump_sum=true;
		}

		// 2: illuminated sign parts and labor
		if( ($invoice['type_1']==1) && ($invoice['type_2']==2) )
		{
			// labor
			$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			// selling price text
			$show_labor=true;
		}

		// 3: non-illuminated lump sum
		if( ($invoice['type_1']==2) && ($invoice['type_2']==1) )
		{
			$sp = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

			$parts=0;

			$show_lump_sum=true;
		}

		// 4: non-illuminated sign parts and labor
		if( ($invoice['type_1']==2) && ($invoice['type_2']==2) )
		{
			// labor
			$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			$show_labor=true;
		}

		// 5: service with parts and labor
		if( ($invoice['type_1']==3) && ($invoice['type_2']==2) )
		{
			// labor
			$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			$show_labor=true;
		}

		// change orders
		$change_orders = $vujade->get_invoice_line_items($invoice_db_id,'Change Order',true);

		// non-taxable co
		$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);

		$change_orders+=$ntco;

		// engineering
		$engineering = $vujade->get_invoice_line_items($invoice_db_id,'Engineering',true);

		// permits
		$permits = $vujade->get_invoice_line_items($invoice_db_id,'Permits',true);

		// permit acq
		$permit_acquisition = $vujade->get_invoice_line_items($invoice_db_id,'Permit Acquisition',true);

		// shipping
		$shipping = $vujade->get_invoice_line_items($invoice_db_id,'Shipping',true);

		// discount
		$discount = $vujade->get_invoice_line_items($invoice_db_id,'Discount',true);

		// retention
		$retention = $vujade->get_invoice_line_items($invoice_db_id,'Retention',true);
		$retention=str_replace("-","",$retention);

		// deposit
		$deposit = $vujade->get_invoice_line_items($invoice_db_id,'Deposit',true);
		$deposit = str_replace("-","",$deposit);

		// tax amount
		// $tax_amount = $vujade->get_invoice_line_items($invoice_db_id,'Sales Tax',true);

		$tax_amount=$invoice['sales_tax'];

		// payments
		$payments = $vujade->get_invoice_payments($invoice_db_id);
		$pt = 0;
		if($payments['error']=="0")
		{
			unset($payments['error']);
			foreach($payments as $payment)
			{
				$pt+=$payment['amount'];
				$pt_row = $vujade->get_invoice_payment($payment['rp_id'],2);
				if(!empty($pt_row['payment_method']))
				{
					$pmdata = $vujade->get_invoice_payment_method($pt_row['payment_method']);
				}
			}
		}

		// cc processing fees
		$cc_processing_fees = $vujade->get_invoice_line_items($invoice_db_id,'CC Processing Fee',true);

		$subtotal1=$sp+$change_orders+$engineering+$permits+$permit_acquisition+$shipping+$parts+$cc_processing_fees;
		$subtotal1=$subtotal1+$discount;

		// sum subtotal - tax - deposit - retention
		$subtotal2 = $subtotal1+$tax_amount;

		// if advanced deposit, this must be multiplied by advanced 

		$total_due = $subtotal2-$deposit;
		$total_due=$total_due-$retention-$pt;

		$balance_due=0; // sum total due - payments
		$balance_due = $total_due;

		// tax rate
		$tax_rate = $invoice['tax_rate'];
	}
}

$section=3;
$menu=17;
$cmenu=1;
$title = 'Edit Invoice - '.$project['project_id'] . ' - ';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		<form method = "post" action = "edit_invoice.php" id = "np">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				<strong>Number: <?php print $invoice_id; ?> (Proposal: 
				<?php print $shop_order['proposal']; ?>)
				</strong>
			</div>
			<div class = "col-md-8 pull-right">
				<strong>Date: <input type = "text" name = "date" value = "<?php print $date; ?>" class = "dp" style = "width:150px;margin-rigth:10px;"></strong> 
				<strong>PO #: <input type = "text" name = "po_id" id = "po_id" value = "<?php print $po_id; ?>" class = "" style = "width:150px;"></strong> 
			</div>
		</div>

		<div class = "row">
			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Billing Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
					# get correct customer info for this project
					$listiderror=0;
					$localiderror=0;
					// try to get by list id
				    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
				    if($customer1['error']!="0")
				    {
				    	$listiderror++;
				    	unset($customer1);
				    }
				    else
				    {
				    	$customer=$customer1;
				    }
				    // try to get by local id
				    $customer2 = $vujade->get_customer($project['client_id'],'ID');
				    if($customer2['error']!="0")
				    {
				    	$localiderror++;
				    	unset($customer2);
				    }
				    else
				    {
				    	$customer=$customer2;
				    }

				    $iderror=$listiderror+$localiderror;

				    if($iderror<2)
				    {
				    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
				    }
				    else
				    {
				    	// can't find customer or no customer on file

				    }
		            if($customer['error']=="0")
		            {
		            	print $customer['name'].'<br>';
		            	print $customer['address_1'].'<br>';
		            	if(!empty($customer['address_2']))
		            	{
		            		print $customer['address_2'].'<br>';
		            	}
		            	print $customer['city'].', '.$customer['state'].' '.$customer['zip'].'<br>';
		            }
					?>
				  </div>
				</div>
			</div>

			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Job Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
		            # get site location for this project
					print $project['site'].'<br>';
					print $project['address_1'].'<br>';
					print $project['city'].', ';
					print $project['state'].' ';
					print $project['zip'].'<br>';;
					// county
					$county=$vujade->get_county($project['city'],$project['state']);
					if($county['error']=="0")
					{
						print $county['county'];
					}
					?>
				  </div>
				</div>
			</div>
		</div>
	
		<input type = "hidden" id = "action" value = "1" name = "action">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

		<div class = "row">
			<div class = "col-md-12">
			<strong>Description:</strong>
			<br>
			
			<center>
			<textarea id = "description" name = "description">
			<?php
			print $memo;
			?>
			</textarea>
			
			<!-- ckeditor -->
			<script src="vendor/plugins/ckeditor/ckeditor.js"></script>
			<script>
	        CKEDITOR.replace('description');
			CKEDITOR.config.toolbar = [
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
				'/',
				{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
				{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
			];
			CKEDITOR.config.fontSize_sizes = '7/7pt;8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;';
	        CKEDITOR.config.removePlugins = 'elementspath';
	        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
	        CKEDITOR.config.tabSpaces = 5;
	        CKEDITOR.config.height = 550;
	        CKEDITOR.config.disableNativeSpellChecker = false;
	        </script>
			</div>
		</div>

		<p style = "height:15px;">&nbsp;</p>

		<!-- bottom block -->
		<div style = "clear:both;width:100%;margin-top:15px;margin-left:10px;margin-right:10px;">

			<style>
			.short
			{
				width:75px;
			}
			.bordered-row
			{
				border-bottom:1px solid #cecece;
				padding-bottom: 5px;
				height:30px;
			}
			</style>

			<!-- left block: input boxes -->
			<div class = "row">
				<div style = "width:650px;">
					<div id = "input_boxes" style = "">
						<!-- left -->
						<div style = "float:left;margin-right:15px;width:300px;padding:3px;height:310px;">
							<table width = "100%">
							
							<tr id = "lump_sum_row" style = "<?php if($show_labor){ print 'display:none;'; } ?>">
								<td>Lump Sum: </td>
								<td><input type = "text" name = "lump_sum" id = "lump_sum" value = "<?php print $sp; ?>" class = "short"></td>
							</tr>

							<tr id = "labor_row" style = "<?php if($show_lump_sum){ print 'display:none;'; } ?>">
								<td>Labor: </td>
								<td><input type = "text" name = "labor" id = "labor" value = "<?php print $sp; ?>" class = "short"></td>
							</tr>

							<tr id = "parts_row" style = "<?php if($show_lump_sum){ print 'display:none;'; } ?>">
								<td>Parts: </td>
								<td><input type = "text" name = "parts" id = "parts" value = "<?php print $parts; ?>" class = "short"></td>
							</tr>

							<tr>
								<td>Change Order: </td>
								<td><input type = "text" name = "change_orders" id = "change_orders" value = "<?php print @number_format($change_orders,2,'.',','); ?>" class = "short"></td>
							</tr>

							<tr>
								<td>Engineering: </td>
								<td><input type = "text" name = "engineering" id = "engineering" value = "<?php print @number_format($engineering,2,'.',','); ?>" class = "short"></td>
							</tr>
							<tr>
								<td>Permits: </td>
								<td><input type = "text" name = "permits" id = "permits" value = "<?php print @number_format($permits,2,'.',','); ?>" class = "short"></td>
							</tr>

							<tr>
								<td>Permit Acquisition: </td>
								<td><input type = "text" class = "short" name = "permit_acquisition" id = "permit_acquisition" value = "<?php print @number_format($permit_acquisition,2,'.',','); ?>"></td>
							</tr>

							<tr>
								<td>Shipping: </td>
								<td><input type = "text" class = "short" name = "shipping" id = "shipping" value = "<?php print @number_format($shipping,2,'.',','); ?>"></td>
							</tr>

							<tr>
								<td>Credit Card Processing: </td>
								<td><input type = "text" class = "short" name = "cc_processing_fees" id = "cc_processing_fees" value = "<?php print @number_format($cc_processing_fees,2,'.',','); ?>"></td>
							</tr>
			
							<tr>
								<td>
									Discount: <br>
									Subtotal: 
								</td>
								<td>
									<input type = "text" class = "short" name = "discount" id = "discount" value = "<?php print @number_format($discount,2,'.',','); ?>">
									<br>
									<input type = "text" class = "short" name = "subtotal1" id = "subtotal1" value = "<?php print @number_format($subtotal1,2,'.',','); ?>" disabled>
								</td>
							</tr>

							</table>
						</div>

						<!-- right -->
						<div style = "float:right;width:300px;padding:3px;height:310px;">
							<table width = "100%">
								<tr>
									<td>Tax Rate: </td>
									<td><input type = "text" name = "tax_rate" class = "short" id = "tax_rate" value = "<?php print round($tax_rate,4); ?>"></td>
								</tr>
								
								<tr>
									<td>Tax: </td>
									<td><input type = "text" name = "tax_amount" id = "tax_amount" value = "<?php print @number_format($tax_amount,2,'.',','); ?>" class = "short" disabled></td>
								</tr>

								<tr>
									<td>Subtotal: </td>
									<td><input type = "text" name = "subtotal2" id = "subtotal2" class = "short" value = "<?php print @number_format($subtotal2,2,'.',','); ?>" disabled></td>
								</tr>

								<tr>
									<td>Deposit: </td>
									<td>
										<?php 
										if($advanced_deposit==1)
										{
										?>
										<input type = "text" class = "short" name = "deposit" id = "deposit" disabled value = "">
										<?php
										}
										else
										{
											?>
											<input type = "text" class = "short" name = "deposit" id = "deposit" value = "<?php print @number_format($deposit,2,'.',','); ?>">
											<?php
										}
										?>
									</td>
								</tr>

								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td>Retention: </td>
									<td><input type = "text" class = "short" name = "retention" id = "retention" value = "<?php print @number_format($retention,2,'.',','); ?>"></td>
								</tr>

								<tr>
									<td>Total Due: </td>
									<td><input type = "text" class = "short" name = "total_due" id = "total_due" value = "<?php print @number_format($total_due,2,'.',','); ?>" disabled></td>
								</tr>

								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>

								<tr>
									<td colspan = "2">
										<center>
											<a href = "#" class = "btn btn-primary" style = "" id = "update_totals">UPDATE TOTALS</a>
										</center>
									</td>
								</tr>

							</table>
						</div>
					</div>

					<hr style = "clear:both;width:97%;">

					<input type = "hidden" name = "invoice_id" id = "invoice_id" value = "<?php print $invoice_id; ?>">

					<input type = "hidden" name = "action" value = "1">

					<input type = "hidden" name = "invoice_db_id" id = "invoice_db_id" value = "<?php print $invoice_db_id; ?>">

					<input type = "submit" value = "SAVE AND CLOSE" id = "done" class = "btn btn-primary" style = ""><br>

					</form>
					
				</div>
			</div>

		</div>

    </div>
</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	$('.dp').datepicker();

    var d = $('#description').html();
    $("#description").html($.trim(d));

    $('body').on('click','#update_totals',function(e)
	{
		e.preventDefault();

    	// type
    	var type1 = "<?php print $invoice['type_1']; ?>";
    	var type2 = "<?php print $invoice['type_2']; ?>";
    	//alert(type);

    	if(type2==1)
    	{
    		var lump_sum=parseFloat($('#lump_sum').val().replace(/,/g, ""));
    		var parts=0;
    		var labor=0;
    	}
    	if(type2==2)
    	{
    		var parts=parseFloat($('#parts').val().replace(/,/g, ""));;
    		var labor=parseFloat($('#labor').val().replace(/,/g, ""));;
    		var lump_sum=0;
    	}

    	// column 1 values
    	var change_orders=parseFloat($('#change_orders').val().replace(/,/g, ""));
    	var engineering=parseFloat($('#engineering').val().replace(/,/g, ""));
    	var permits=parseFloat($('#permits').val().replace(/,/g, ""));
    	var permit_acquisition=parseFloat($('#permit_acquisition').val().replace(/,/g, ""));
    	var shipping=parseFloat($('#shipping').val().replace(/,/g, ""));
    	var discount=parseFloat($('#discount').val().replace(/,/g, ""));
    	var cc=parseFloat($('#cc_processing_fees').val().replace(/,/g, ""));

    	// column 2 values
    	var tax_rate=parseFloat($('#tax_rate').val().replace(/,/g, ""));
    	var deposit=parseFloat($('#deposit').val().replace(/,/g, ""));
    	var retention=parseFloat($('#retention').val().replace(/,/g, ""));

    	// advanced deposit
		if($('#advanced_deposit').is(':checked'))
		{
			var adv=1;
		}
		else
		{
			var adv=0;
		}

		// asynch post to php script for correct calculations
    	$.post("jq.format_price.php", { action: 1, lump_sum: lump_sum, parts: parts, labor: labor, co: change_orders, e: engineering, p: permits, pa: permit_acquisition, shipping: shipping, discount: discount, tax_rate: tax_rate, deposit: deposit, retention: retention, adv: adv, cc: cc, type1: type1, type2: type2 })
		.done(function(result) 
		{
			$('#input_boxes').html('');
			$('#input_boxes').html(result);
		});
	});

	$('#done').click(function()
	{
		$('#tax_amount').prop('disabled',false);
		$('#form').submit();
	});
    
});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
