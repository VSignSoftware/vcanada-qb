<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# get hours and tasks worked for date and employee
if($action==1)
{
	$date = $_REQUEST['date'];
	$datets = strtotime($date);
	$employee_id = $_REQUEST['employee_id'];
	$timedata = $vujade->get_timecard($datets,$employee_id);
	$posted = 0;
}

# save new
if($action==2)
{
	$please_confirm=0;
	$confirmed = 0;
	$show_confirm = 0;
	$posted = 1;

	if(isset($_REQUEST['confirmed']))
	{
		$confirmed=$_REQUEST['confirmed'];
	}

	$date = $_REQUEST['date'];
	$datets = strtotime($date);
	$employee_id = $_REQUEST['employee_id'];

	$start = $_REQUEST['start'];
	$end = $_REQUEST['end'];

	$standard_time = $_REQUEST['standard_time'];
	$over_time = $_REQUEST['over_time'];
	$double_time = $_REQUEST['double_time'];

	$project_id = $_REQUEST['project_id'];
	$type = $_REQUEST['type'];

	if(!in_array($type, array('Sick','Holiday','Other','Vacation','Office Time')))
	{
		# must be valid
		$project = $vujade->get_project($project_id,2);
		if($project['error']!="0")
		{
			$vujade->errors[] = "Project number is not valid.";
			$project_is_invalid=1;
		}	
	}

	if($project['status']=="Pending")
	{
		$please_confirm=1;
	}
	if($project['status']=="Ready to be turned in")
	{
		$please_confirm=1;
	}
	if($project['status']=="Turned in for billing")
	{
		$please_confirm=1;
	}
	if($project['status']=="Closed")
	{
		$vujade->errors[]="Time cannot be added to closed projects.";
		$project_is_invalid=2;
	}

	if( ($please_confirm==1) && ($confirmed!=1) )
	{
		$vujade->errors[]="Please confirm your selection.";
		$show_confirm = 1;
	}

	if($confirmed==1)
	{
		$please_confirm=0;
	}

	# must be active; if not active, allow them to continue

	$ec = $vujade->get_error_count();
	if($ec<=0)
	{
		$vujade->create_row('timecards');
		$row_id=$vujade->row_id;
		$s[]=$vujade->update_row('timecards',$row_id,'employee_id',$employee_id);
		$s[]=$vujade->update_row('timecards',$row_id,'date',$date);
		$s[]=$vujade->update_row('timecards',$row_id,'date_ts',$datets);
		$s[]=$vujade->update_row('timecards',$row_id,'type',$_REQUEST['type']);

		# can't add certain types to project costing; ignore the project number or throw an error?
		if(isset($type))
		{
			if(ctype_digit($type))
			{
				$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
			}
		}

		if(!in_array($type, array('Sick','Holiday','Other','Vacation','Office Time')))
		{
			# must be valid
			$project = $vujade->get_project($project_id,2);
			if($project['error']!="0")
			{
				$vujade->errors[] = "Project number is not valid.";
				$project_is_invalid=1;
			}	
		}

		# special for office, holiday, vacation
		if($project_id=='OFFICE')
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
		}
		if($project_id=='VACATION')
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
		}
		if($project_id=='HOLIDAY')
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
		}
		if($project_id=='SICK')
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
		}
		if($type=='Other')
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
		}

		$s[]=$vujade->update_row('timecards',$row_id,'start',$_REQUEST['start']);
		$s[]=$vujade->update_row('timecards',$row_id,'end',$_REQUEST['end']);

		$s[]=$vujade->update_row('timecards',$row_id,'standard_time',$_REQUEST['standard_time']);
		$s[]=$vujade->update_row('timecards',$row_id,'over_time',$_REQUEST['over_time']);
		$s[]=$vujade->update_row('timecards',$row_id,'double_time',$_REQUEST['double_time']);
		$s[]=$vujade->update_row('timecards',$row_id,'entered_ts',strtotime('now'));
		unset($start);
		unset($end);
		unset($type);
		unset($standard_time);
		unset($over_time);
		unset($double_time);
		unset($project_id);
		unset($project);
		$posted = 0;
		//print_r($s);
	}
	$timedata = $vujade->get_timecard($datets,$employee_id);
	$action = 1;
}

# update
if($action==3)
{
	$row_id=$_REQUEST['row_id'];
	$date = $_REQUEST['date'];
	$datets = strtotime($date);
	$employee_id = $_REQUEST['employee_id'];
	$project_id = $_REQUEST['project_id'];
	$posted = 1;

	if(!in_array($_REQUEST['type'], array('Sick','Holiday','Other','Vacation','Office Time')))
	{
		# must be valid
		$project = $vujade->get_project($project_id,2);
		if($project['error']!="0")
		{
			$vujade->errors[] = "Project number is not valid.";
			$project_is_invalid=1;
			$posted = 0;
		}
		if($project['status']=="Closed")
		{
			$vujade->errors[]="Time cannot be added to closed projects.";
			$project_is_invalid=2;
			$posted = 0;
		}	
	}

	$ec = $vujade->get_error_count();
	if($ec<=0)
	{
		$s[]=$vujade->update_row('timecards',$row_id,'employee_id',$employee_id);
		$s[]=$vujade->update_row('timecards',$row_id,'date',$date);
		$s[]=$vujade->update_row('timecards',$row_id,'date_ts',$datets);
		$s[]=$vujade->update_row('timecards',$row_id,'type',$_REQUEST['type']);

		# can't add certain types to project costing; ignore the project number or throw an error?

		if(!in_array($_REQUEST['type'], array('Sick','Holiday','Other','Vacation','Office Time')))
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id',$_REQUEST['project_id']);
		}
		else
		{
			$s[]=$vujade->update_row('timecards',$row_id,'project_id','');
		}

		$s[]=$vujade->update_row('timecards',$row_id,'start',$_REQUEST['start']);
		$s[]=$vujade->update_row('timecards',$row_id,'end',$_REQUEST['end']);
		$s[]=$vujade->update_row('timecards',$row_id,'standard_time',$_REQUEST['standard_time']);
		$s[]=$vujade->update_row('timecards',$row_id,'over_time',$_REQUEST['over_time']);
		$s[]=$vujade->update_row('timecards',$row_id,'double_time',$_REQUEST['double_time']);
		$posted = 0;
	}
	$timedata = $vujade->get_timecard($datets,$employee_id);
	$action = 1;
}

# delete
if($action==4)
{
	$date = $_REQUEST['date'];
	$datets = strtotime($date);
	$employee_id = $_REQUEST['employee_id'];
	$row_id=$_REQUEST['row_id'];
	$s[]=$vujade->delete_row('timecards',$row_id);
	$timedata = $vujade->get_timecard($datets,$employee_id);
	$action = 1;
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Enter Time - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

  <!-- Begin: Content -->
  <section id="content" class="table-layout animated fadeIn">

    <!-- begin: .tray-left -->
    <aside class="tray tray250 p30" id = "left_tray" style = "width:250px;">

	    <div id = "menu_2" style = "">

	    	<a class = "glyphicons glyphicons-left_arrow" href = "accounting.php" id = "back" style = "margin-bottom:10px;"></a>
			<br>

			<a href = "print_time_cards.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Print Time Cards</a>
	      	<br>

	      	<a href = "enter_time.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Enter Time</a>
	      	<br>

	      	<a href = "payroll_summary.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Payroll Summary</a>
	      	<br>

	      	<a href = "payroll_hourly_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Hourly Report</a>
	      	<br>

	      	<a href = "payroll_labor_sort_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Labor Report</a>
	      	<br>
		</div>

    </aside>
    <!-- end: .tray-left -->

    <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="pl20 pr50">

        	<div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				
	        	<div class="panel-body bg-light">
	        		
	        		<div class = "row">

	        			<div style = "float:left;margin-right:5px;">
	        				<strong>Time Cards for Date: </strong>
	        			</div>

	        			<div style = "float:left;margin-left:5px;margin-right:5px;">
	        				<input type = "text" class = "dp" style = "width:200px;" name = "date" id = "date" value = "<?php print $date; ?>"> 
	        			</div>
						<br style="clear:both;"/>
						<br style="clear:both;"/>
	        			<div style = "float:left;margin-right:5px;">
	        				<strong>Employee</strong>
	        			</div>

	        			<div style = "float:left;margin-right:5px;">
	        				<select name = "employee" id = "employee">
								<?php
								if(!empty($employee_id))
								{
									$emp = $vujade->get_employee($employee_id);
									if($emp['error']=="0")
									{
										print '<option value ="'.$emp['database_id'].'">'.$emp['fullname'].'</option>';
									}
								}
								print '<option value = "">-Select-</option>';
								$employees = $vujade->get_employees(1);
								if($employees['error']=="0")
								{
									unset($employees['error']);
									foreach($employees as $employee)
									{
										if($employee['is_admin']!=1)
										{
											print '<option value ="'.$employee['database_id'].'">'.$employee['fullname'].'</option>';
										}
									}
								}
								?>
								</select>
	        			</div>

	        			<div style = "float:left;margin-right:5px;">
	        				<input type = "submit" id = "go" value = "Go" class = "btn btn-xs btn-primary">
	        			</div>
	        		</div>
	        	</div>
		<hr>

		<div style = "width:100%;overflow:auto;">

		<?php
		$labor_types = $vujade->get_labor_types();
		unset($labor_types['error']);

		# display data for this employee for this date
		if($action==1)
		{
			print '<table class = "table">';
			print '<tr class = "bordered header">';
			print ' <td>Department</td>
					<td>Start</td>
					<td>End</td>
					<td>ST</td>
					<td>OT</td>
					<td>DT</td>
					<td>Total</td>
					<td>&nbsp;</td><td>&nbsp;</td>';
			print '</tr>';

			# existing timecard data
			if($timedata['error']=="0")
			{
				if($project_is_invalid==1)
				{
					print '<tr class = "bordered">';
					print '<td colspan = "9"><font color = "red"><b>Project Number is invalid.</b></font></td>';
					print '</tr>';
				}
				if($project_is_invalid==2)
				{
					print '<tr class = "bordered">';
					print '<td colspan = "9"><font color = "red"><b>Project is closed. Time cannot be added to closed projects.</b></font></td>';
					print '</tr>';
				}
				unset($timedata['error']);
				$total_time=0;
				$st_total = 0;
				$ot_total = 0;
				$dt_total = 0;

				$daytotal_st = 0;
				$daytotal_ot = 0;
				$daytotal_dt = 0;

				$daytotal = 0;

				foreach($timedata as $td)
				{
					$st = 0;
					$ot = 0;
					$dt = 0;

					# hours were selected from the time pickers
					if(!empty($td['start']))
					{
						//$startnotempty="Time picked from list";
						$ts = $td['date'];
						$begints = strtotime($ts.' '.$td['start']);
						$endts = strtotime($ts.' '.$td['end']);
						$diff = $endts-$begints;
						$diff = $diff / 60;
						$diff = $diff / 60;
						$line_total=$diff;
						$daytotal+=$line_total;

						# daytotal is a counter for all hours worked on this date
						if($daytotal<=8)
						{
							# hours worked for this entry (not the same as the date because dates can have multiple entries)
							$st = $diff;
							$st_total+=$diff;
							$ot = 0;
							$dt = 0;
						}
						if( ($daytotal > 8) && ($daytotal <= 12) )
						{
							$st = 0;
							$ot = $daytotal-8;
							$st = $diff-$ot;
							$st_total+=$st;
							$ot_total+=$ot;
							$dt = 0;
						}

						if($daytotal > 12)
						{
							$ot = 0;
							$st = 0;
							$dt = $daytotal-12;
							$dt_total+=$dt;
							$ot = $diff-$dt;
							$ot_total+=$ot;
						}
					}
					else
					{
						//$startnotempty="Time entered manually";

						# hours were manually typed in
						$line_total = $td['standard_time'] + $td['over_time'] + $td['double_time'];
						$daytotal+=$line_total;
						$st_total+=$td['standard_time'];
						$ot_total+=$td['over_time'];
						$dt_total+=$td['double_time'];
						$st = $td['standard_time'];
						$ot = $td['over_time'];
						$dt = $td['double_time'];
					}
					$total_time+=$line_total;

					print '<tr class = "bordered">';
					print '<form method = "post" action = "enter_time.php">';
					print '<input type = "hidden" name = "action" value = "3">';
					print '<input type = "hidden" name = "employee_id" value = "'.$employee_id.'">';
					print '<input type = "hidden" name = "row_id" value = "'.$td['database_id'].'">';
					print '<input type = "hidden" name = "date" value = "'.$date.'">';

					// 1
					// labor type
					print '<td>';
					print '<select name = "type">';
					if(!empty($td['type']))
					{
						if(!ctype_digit($td['type']))
						{
							print '<option value = "'.$td['type'].'" selected = "selected">'.$td['type'].'</option>';
						}
						else
						{
							$selected_type = $vujade->get_labor_type_for_timecard($td['type']);
							if($selected_type['error']=="0")
							{
								print '<option value = "'.$td['type'].'">'.$selected_type['type'].'</option>';
							}
						}
					}
					print '<option value = "">-Select-</option>';
					// static types
					print '<option value = "Holiday">Holiday</option>';
					print '<option value = "Other">Other</option>';
					print '<option value = "Vacation">Vacation</option>';
					print '<option value = "">-------------</option>';
					print '<option value = "Office Time">Office Time</option>';
					print '<option value = "">-------------</option>';
					foreach($labor_types as $lt)
					{
						print '<option value ="'.$lt['database_id'].'">'.$lt['type'].'</option>';
					}
					print '</select><br>';

					# project id
					$project = $vujade->get_project($td['project_id'],2);
					if($project['error']!="0")
					{
						$td['project_id']="";
					}
					print 'Job Number: <input type = "text" name = "project_id" style = "width:100px;" value = "'.$td['project_id'].'"><br>';
					if($project['error']=="0")
					{
						print $project['site'];
					}
					print '</td>';

					// 2
					# start
					print '<td>';
					print '<input type = "text" style = "width:50px;" name = "start" class = "tp" value = "'.$td['start'].'">';
					print '</td>';

					# end 3
					print '<td>';
					print '<input type = "text" name = "end" style = "width:50px;" class = "tp" value = "'.$td['end'].'">';
					print '</td>';

					# st 4
					print '<td>';
					print '<input type = "text" style = "width:50px;" name = "standard_time" value = "'.$st.'">';
					print '</td>';

					# ot 5
					print '<td>';
					print '<input type = "text" style = "width:50px;" name = "over_time" value = "'.$ot.'">';
					print '</td>';

					# dt 6
					print '<td>';
					print '<input type = "text" style = "width:50px;" name = "double_time" value = "'.$dt.'">';
					print '</td>';

					// 7 total
					print '<td>';
					print '<input type = "text" style = "width:50px;" readonly = "readonly" value = "'.$line_total.'">';
					print '</td>';

					// 8
					# buttons 
					print '<td>';

					# save
					print '<input type = "submit" value = "Save" class = "btn btn-xs btn-primary" style = "float:left;"></form><br>';

					# delete 9
					print '<form method = "post" action = "enter_time.php">';
					print '<input type = "hidden" name = "action" value = "4">';
					print '<input type = "hidden" name = "employee_id" value = "'.$employee_id.'">';
					print '<input type = "hidden" name = "date" value = "'.$date.'">';
					print '<input type = "hidden" name = "row_id" value = "'.$td['database_id'].'">';
					print '<input type = "submit" value = "Delete" style = "float:left;margin-left:0px;margin-top:5px;" class = "btn btn-xs btn-danger"></form>';
					print '</td>';

					print '<td></td>';

					print '</tr>';
				}
			}

			# 

			# new blank row
			if($posted==0)
			{
				print '<tr class = "bordered">';
				print '<form method = "post" action = "enter_time.php">';
				print '<input type = "hidden" name = "action" value = "2">';
				print '<input type = "hidden" name = "employee_id" value = "'.$employee_id.'">';
				print '<input type = "hidden" name = "date" value = "'.$date.'">';
				
				// labor type 1
				print '<td>';
				print '<select name = "type" id = "type">';
				print '<option value = "">-Select-</option>';
				// static types
				print '<option value = "Sick">Sick</option>';
				print '<option value = "Holiday">Holiday</option>';
				print '<option value = "Other">Other</option>';
				print '<option value = "Vacation">Vacation</option>';
				print '<option value = "">-------------</option>';
				print '<option value = "Office Time">Office Time</option>';
				print '<option value = "">-------------</option>';
				foreach($labor_types as $lt)
				{
					print '<option value ="'.$lt['database_id'].'">'.$lt['type'].'</option>';
				}
				print '</select><br>';
				print 'Job Number: <input type = "text" name = "project_id" id = "project_id" value = "" style = "width:100px;">';
				print '</td>';

				# start 2
				print '<td>';
				print '<input type = "text" name = "start" class = "tp" value = "" style = "width:50px;">';
				print '</td>';

				# end 3
				print '<td>';
				print '<input type = "text" name = "end" class = "tp" value = "" style = "width:50px;">';
				print '</td>';

				# st 4
				print '<td>';
				print '<input type = "text" name = "standard_time" value = "" style = "width:50px;">';
				print '</td>';

				# ot 5
				print '<td>';
				print '<input type = "text" name = "over_time" value = "" style = "width:50px;">';
				print '</td>';

				# dt 6
				print '<td>';
				print '<input type = "text" name = "double_time" value = "" style = "width:50px;">';
				print '</td>';

				# total 7
				print '<td>';
				print '<input type = "text" readonly = "readonly" value = "" style = "width:50px;">';
				print '</td>';

				# save button 8
				print '<td>';
				print '<input type = "submit" value = "Save" class = "btn btn-xs btn-primary">';
				print '</form>';
				print '</td>';

				// blank 9
				print '<td>&nbsp;';
				print '</td>';

				print '</tr>';
			}

			# new entry was posted, but error detected
			if($posted==1)
			{

				if($please_confirm==1)
				{
					print '<tr class = "bordered">';
					print '<td colspan = "9"><font color = "red"><b>Project is not active. Please confirm your entry:</b></font></td>';
					print '</tr>';
				}
				if($project_is_invalid==1)
				{
					print '<tr class = "bordered">';
					print '<td colspan = "9"><font color = "red"><b>Project Number is invalid.</b></font></td>';
					print '</tr>';
				}
				if($project_is_invalid==2)
				{
					print '<tr class = "bordered">';
					print '<td colspan = "9"><font color = "red"><b>Project is closed. Time cannot be added to it.</b></font></td>';
					print '</tr>';
				}
				print '<tr class = "bordered">';
				print '<form method = "post" action = "enter_time.php">';
				print '<input type = "hidden" name = "action" value = "2">';
				print '<input type = "hidden" name = "employee_id" value = "'.$employee_id.'">';
				print '<input type = "hidden" name = "date" value = "'.$date.'">';
				
				if($please_confirm==1)
				{
					print '<input type = "hidden" name = "confirmed" value = "1">';
				}

				// labor type 1
				print '<td>';
				print '<select name = "type">';
				if( (isset($type)) && (!ctype_digit($type)) )
				{
					print '<option value = "'.$type.'">'.$type.'</option>';
				}
				print '<option value = "">-Select-</option>';
				// static types
				print '<option value = "Sick">Sick</option>';
				print '<option value = "Holiday">Holiday</option>';
				print '<option value = "Other">Other</option>';
				print '<option value = "Vacation">Vacation</option>';
				print '<option value = "">-------------</option>';
				print '<option value = "Office Time">Office Time</option>';
				print '<option value = "">-------------</option>';
				foreach($labor_types as $lt)
				{
					if($type==$lt['database_id'])
					{
						print '<option value ="'.$lt['database_id'].'" selected = "selected">'.$lt['type'].'</option>';
					}
					else
					{
						print '<option value ="'.$lt['database_id'].'">'.$lt['type'].'</option>';
					}
				}
				print '</select><br>';
				print 'Job Number: <input type = "text" name = "project_id" value = "'.$project_id.'" style = "width:100px;"><br>';
				print $project['site'];
				print '</td>';

				# start 2
				print '<td>';
				print '<input type = "text" name = "start" class = "tp" value = "'.$start.'" style = "width:50px;">';
				print '</td>';

				# end 3
				print '<td>';
				print '<input type = "text" name = "end" class = "tp" value = "'.$end.'" style = "width:50px;">';
				print '</td>';

				# st 4
				print '<td>';
				print '<input type = "text" name = "standard_time" value = "'.$standard_time.'" style = "width:50px;">';
				print '</td>';

				# ot 5
				print '<td>';
				print '<input type = "text" name = "over_time" value = "'.$over_time.'" style = "width:50px;">';
				print '</td>';

				# dt 6
				print '<td>';
				print '<input type = "text" name = "double_time" value = "'.$double_time.'" style = "width:50px;">';
				print '</td>';

				# total 7
				print '<td>';
				print '<input type = "text" readonly = "readonly" value = "" style = "width:50px;">';
				print '</td>';

				# save button 8
				print '<td>';
				if($please_confirm==1)
				{
					print '<input type = "submit" value = "Confirm" class = "btn btn-xs btn-primary">';
				}
				else
				{
					print '<input type = "submit" value = "Save" class = "btn btn-xs btn-primary">';
				}
				
				print '</form>';
				print '</td>';

				// blank 9
				print '<td>&nbsp;';
				print '</td>';

				print '</tr>';
			}
			
			// summary
			print '<tr class = "bordered">';
			print '<td colspan = "9">&nbsp;</td>';
			print '</tr>';

			print '<tr class = "">';
			print '<td colspan = "4">&nbsp;</td>';

			//
			print '<td>';
			print '<input type = "text" style = "width:50px;" readonly = "readonly" value = "'.$st_total.'">';
			print '</td>';

			print '<td>';
			print '<input type = "text" style = "width:50px;" readonly = "readonly" value = "'.$ot_total.'">';
			print '</td>';

			print '<td>';
			print '<input type = "text" style = "width:50px;" readonly = "readonly" value = "'.$dt_total.'">';
			print '</td>';
			
			print '<td>';
			print '<input type = "text" style = "width:50px;" readonly = "readonly" value = "'.$total_time.'">';
			print '</td>';

			print '<td>&nbsp;</td>';
			print '</tr>';
			print '</table>';
		}
		?>
				
				</div>
			</div>
		</div>
	</div><!-- end div with overflow:auto -->
        </div>

  </section>
  <!-- End: Content -->

</section>

</div>

<!-- --><link rel="stylesheet" type="text/css" href="css/jquery.timepicker.css" />

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/jquery.timepicker.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// datepickers
    $(".dp").datepicker();

    // time picker
    $('.tp').timepicker({ 'step': 15, 'minTime': '6:00am'});

    $('#go').click(function()
    {
    	var employee_id = $('#employee').val();
    	var date = $('#date').val();
    	if(date=="")
    	{
    		alert('Date must be selected.');
    		return false;
    	}
    	if(employee_id=="")
    	{
    		alert('Employee must be selected.');
    		return false;
    	}
    	// refresh the page
    	var href = "enter_time.php?action=1&date="+date+"&employee_id="+employee_id;
    	window.location.href = href;
    });

    $('#type').change(function()
    {
    	var val = $(this).val();
    	$('project_id').val('');
    	if(val=='Office Time')
    	{
    		$('#project_id').val('OFFICE');
    	}
    	if(val=='Holiday')
    	{
    		$('#project_id').val('HOLIDAY');
    	}
    	if(val=='Vacation')
    	{
    		$('#project_id').val('VACATION');
    	}
    	if(val=='Sick')
    	{
    		$('#project_id').val('SICK');
    	}
    });

    $('td').each(function()
    {
    	$(this).css('vertical-align','top');
    });

    $('select').each(function()
    {
    	$(this).css('margin-bottom','5px');
    });

});
</script>

</body>
</html>