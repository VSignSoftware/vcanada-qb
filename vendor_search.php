<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions

# setup
$setup=$vujade->get_setup();

if(isset($_REQUEST['zoom']))
{
	$zoom=$_REQUEST['zoom'];
}
else
{
	$zoom=11;
}

// maps access key
$k='pk.eyJ1IjoidG9kZGhhbW0xIiwiYSI6ImNpdDFoMnJ4ODBxNzcyemtoMDF1enkzbDIifQ.bM2v1sdtB628GDiimJEH_Q';

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$title = "Vendor Map Search - ";
require_once('h.php');
?>

<section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Vendor Map Search</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">
        	<div class = "row">
        		<div class = "col-md-3">
        			<!-- containing div for the list of found vendors, etc -->
        			<div class = "" style = "height:450px;overflow:auto;">
		        	<?php
		        	if(isset($_REQUEST['zip']))
		        	{
			        	$zip=trim($_REQUEST['zip']);
			        	if(empty($zip))
			        	{
			        		$e='<div class = "alert alert-danger">Invalid zip code.</div>';
			        		die($e);
			        	}

			        	if(isset($_REQUEST['radius']))
			        	{
			        		$radius=$_REQUEST['radius'];
			        	}
			        	else
			        	{
			        		$radius=25;
			        	}
			        	$valid_radius = array(10,25,50,100);
			        	if(!in_array($radius, $valid_radius))
			        	{
			        		$radius=25;
			        	}

			        	if($radius==10)
			        	{
			        		$zoom=11;
			        	}
			        	if($radius==25)
			        	{
			        		$zoom=10;
			        	}
			        	if($radius==50)
			        	{
			        		$zoom=9;
			        	}
			        	if($radius==100)
			        	{
			        		$zoom=8;
			        	}

			        	if(empty($zip))
			        	{
			        		$e='<div class = "alert alert-danger">Invalid zip code.</div>';
			        		die($e);
			        	}

			        	$zip_data=$vujade->get_lat_long($zip);
			        	//$vujade->print_r_array($zip_data);
			        	$coords = $vujade->get_zips_by_radius($radius, $zip, $zip_data['latitude'], $zip_data['longitude']);

			        	//$vujade->print_r_array($coords);

			        	// project data
			        	if(isset($_REQUEST['project_id']))
			        	{
			        		$project_id=$_REQUEST['project_id'];
			        	}
			        	if( (!empty($project_id)) && (ctype_digit($project_id)) )
			        	{
			        		$project=$vujade->get_project($project_id,2);
			        		if($project['error']!=0)
							{
								$e='<div class = "alert alert-danger">Invalid zip Project.</div>';
			        			die($e);
							}
			        	}

			        	print '<div class = "alert alert-warning">You searched for vendors within '.$radius.' miles of zip code '.$zip.'.<br>';

			        	// change radius
			        	print '<select name = "new_radius" id = "new_radius" class = "form-control">';
			        	print '<option value = "">Change Search Radius</option>';
			        	print '<option value = "10">10 Miles</option>';
			        	print '<option value = "25">25 Miles</option>';
			        	print '<option value = "50">50 Miles</option>';
			        	print '<option value = "100">100 Miles</option>';
			        	print '</select><br>';

			        	// found vendors array
			        	$vendors = array();

			        	// zip codes near radius
			        	if($coords['count']>0)
			        	{
			        		unset($coords['count']);
			        		unset($coords['error']);
			        		unset($coords['sql']);
				        	foreach($coords as $coord)
				        	{

				        		// get vendors with zip code in result set
				        		$vends = $vujade->get_vendors_by_zip($coord['zip'],$setup['is_qb']); 

				        		//$vujade->print_r_array($vends);


				        		if($vends['count']>0)
				        		{
				        			unset($vends['error']);
				        			unset($vends['count']);
				        			unset($vends['sql']);
				        			foreach($vends as $vend)
				        			{
				        				$vendors[]=$vend;
				        			}
				        		}
				        	}
				        }

				        // lat-long coords for markers
				        $markers = array();

				        // get lat long for project if requested
				        if($project)
				        {
				        	// address
	        				$test_address = $project['address_1'].'+'.$project['city'].'+'.$project['state'].'+'.$project['zip'];

	        				$test_address=str_replace(" ","+",$test_address);

	        				// curl post get lat long
				        	$url = "https://api.mapbox.com/geocoding/v5/mapbox.places/".$test_address.".json?country=us&access_token=".$k;

				        	$ch = curl_init(); 

					        // set url 
					        curl_setopt($ch, CURLOPT_URL, $url); 

					        //return the transfer as a string 
					        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

					        // $output contains the output string 
					        $output = curl_exec($ch); 

					        // close curl resource to free up system resources 
					        curl_close($ch);   

					        $json = json_decode($output);
					        $new_lat_long=$json->features[0]->center[1].','.$json->features[0]->center[0];
					        $project_marker=$new_lat_long;
				        }

				        $c = count($vendors);
				        if($c>0)
				        {
				        	print 'Found <span id = "found"></span> vendors: </div>';
				        	$x=1;
				        	foreach($vendors as $vendor_id)
				        	{
				        		// loop through each vendor;
				        		// if has lat and long, skip 
				        		// else get lat and long for address
				        		$vendor = $vujade->get_vendor($vendor_id);
				        		if(($vendor['error']=='0') && ($vendor['address_1']!=''))
				        		{
				        			if($vendor['latlong']=='')
				        			{
				        				// address
				        				$test_address = $vendor['address_1'].'+'.$vendor['city'].'+'.$vendor['state'].'+'.$vendor['zip'];

				        				$test_address=str_replace(" ","+",$test_address);

				        				// curl post get lat long
				        				$k='pk.eyJ1IjoidG9kZGhhbW0xIiwiYSI6ImNpdDFoMnJ4ODBxNzcyemtoMDF1enkzbDIifQ.bM2v1sdtB628GDiimJEH_Q';
							        	$url = "https://api.mapbox.com/geocoding/v5/mapbox.places/".$test_address.".json?country=us&access_token=".$k;

							        	$ch = curl_init(); 

								        // set url 
								        curl_setopt($ch, CURLOPT_URL, $url); 

								        //return the transfer as a string 
								        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

								        // $output contains the output string 
								        $output = curl_exec($ch); 

								        // close curl resource to free up system resources 
								        curl_close($ch);   
								        
								        // debug result
								        $vujade->print_r_array($output);
					        			print '<hr>';

								        $json = json_decode($output);
								        $new_lat_long=$json->features[0]->center[1].','.$json->features[0]->center[0];

								        if($new_lat_long!=',')
								        {
								        	$markers[]=$new_lat_long.'^'.$vendor['name'].'^'.$vendor['rating'].'^'.$vendor['database_id'];
								        
								        	// update lat long
					        				if($setup['is_qb']==1)
					        				{
					        					$s[]=$vujade->update_row('quickbooks_vendor',$vendor['database_id'],'latlong',$new_lat_long,'ID');
					        				}
					        				else
					        				{
					        					$s[]=$vujade->update_row('vendors',$vendor['database_id'],'latlong',$new_lat_long);
					        				}

					        				print '<address>';
						        			print '<strong>'.$x.') '.$vendor['name'].'</strong><br>';
											$x++;
						        			print $vendor['address_1'].'<br>';
						        			print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];
						        			print '<br>'.$new_lat_long;
						        			print '</address>';
								        }

				        			}
				        			else
				        			{
				        				if($vendor['latlong']!=',')
				        				{
					        				$markers[]=$vendor['latlong'].'^'.$vendor['name'].'^'.$vendor['rating'].'^'.$vendor['database_id'];

					        				print '<address>';
						        			print '<strong>'.$x.') '.$vendor['name'].'</strong><br>';
											$x++;
						        			print $vendor['address_1'].'<br>';
						        			print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];
						        			print '</address>';
						        		}
				        			}
				        		}
				        		else
				        		{
				        			// error
				        			//print '<div class = "alert alert-danger">Invalid vendor.</div>';
				        		}
				        	}
				        }
				        else
				        {
				        	// none found
				        	print '</div><div class = "alert alert-danger">No vendors found.</div>';
				        }
				        print '</div>';
				        print '<table><tr><td>';
				        print '<a href = "vendor_search.php" class = "btn btn-sm btn-primary">New Search</a> </td>';

				        print '<td>&nbsp;</td>';

				        if($project_id)
				        {
				        	print '<td> <a href = "project.php?id='.$project_id.'" class = "btn btn-sm btn-primary">Back to Project</a></td>';
				        }
				        else
				        {
				        	print '<td> <a href = "vendors.php" class = "btn btn-sm btn-primary">Vendor List</a></td>';
				        }
				        print '</tr></table>';
			        }
			        else
			        {
		        	?>
		        		<div class = "row">
		        			<div class = "col-md-6">
		        				<input type = "text" name = "zip" id = "zip" value = "" class = "form-control" placeholder = "Zip Code"> 

		        			</div>
		        			<div class = "col-md-6">
		        				<a href = "#" id = "go" class = "btn btn-success">Find Vendors</a>
		        			</div>
		        		</div>
		        	<?php
		        }
	        	?>
	        	</div>
	        	<div class = "col-md-9">
	        		<?php
	        		if($project)
				    {
				    	$addr = $project['project_id'].' - '.$project['site'].' | '.$project['address_1'].' | '.$project['city'].', '.$project['state'].' '.$project['zip'];
        				print '<div class = "alert alert-primary" style = "width:95%;margin-bottom:5px;">
        				<span class="glyphicon glyphicon-map-marker"></span> ';
        				print $addr;
        				print '</div>';
	        		}
	        		$mc = count($markers);
	        		if($mc>0)
	        		{
	        			?>

	        			<!-- load the map and populate with markers -->
	        			<script src='https://api.mapbox.com/mapbox.js/v3.0.1/mapbox.js'></script>
						<link href='https://api.mapbox.com/mapbox.js/v3.0.1/mapbox.css' rel='stylesheet' />

						<style>
						#map 
						{ 
							position:absolute; 
							left: 100; 
							top:0; 
							bottom:0; 
							width:764px; 
							height:450px;
							<?php
			        		if($project)
						    {
								print 'margin-top:60px;';

							}
							?>
						}
						.marker 
						{
						    display: block;
						    border: none;
						    border-radius: 50%;
						    cursor: pointer;
						    padding: 0;
						}
						</style>

						<div id='map' style = ""></div>

	        			<script>
						// access token
						L.mapbox.accessToken = 'pk.eyJ1IjoidG9kZGhhbW0xIiwiYSI6ImNpdDFoMnJ4ODBxNzcyemtoMDF1enkzbDIifQ.bM2v1sdtB628GDiimJEH_Q';

						// create the map with default view 
						// set view is lat, long, and zoom level
						var map = L.mapbox.map('map', 'mapbox.streets')
						    .setView([<?php print $zip_data['latitude']; ?>, <?php print $zip_data['longitude']; ?>], <?php print $zoom; ?>);

						var vicon = L.icon({
								iconUrl: 'images/v-map-logo.png',
								iconSize: [25, 41],
							});

						// add marker(s) to the map
						// markers can be styled
	        			<?php
	        			// job address
	        			if($project_marker)
	        			{
	        				$mark=explode(',',$project_marker);
	        				$lat=$mark[0];
	        				$long = $mark[1];
	        				$project_title = $project['project_id'].' - '.$project['site'].' '.$project['address_1'].' '.$project['city'].', '.$project['state'].' '.$project['zip'];
	        				$project_name = $project_id.' - '.$project['site'];
	        				?>
	        				L.marker([<?php print $lat; ?>, <?php print $long; ?>], {
	        					title: "<?php print $project_name; ?>", 
	        					}).bindPopup("<?php print $project_title; ?>").addTo(map);
	        				<?php
	        				unset($lat);
	        				unset($long);
	        				unset($mark);
	        			}

	        			$n=1;
	        			foreach($markers as $marker)
	        			{
	        				//print $marker.'<br>';
	        				$mark=explode('^',$marker);
	        				$latlong=$mark[0];
	        				$ll = explode(',',$latlong);
	        				$lat = $ll[0];
	        				$long = $ll[1];
	        				$name = $mark[1];
	        				$rating = $mark[2];
	        				$vid = $mark[3];
	        				if(!empty($lat))
	        				{
	        				?>

	        				L.marker([<?php print $lat; ?>, <?php print $long; ?>], {
	        					title: "<?php print $n.') '.$name; ?>", 
	        					icon: vicon
	        					}).bindPopup("<?php print $name.' (Rating: '.$rating.')'; ?> <a class = 'btn btn-xs btn-success' href = 'vendor.php?id=<?php print $vid; ?>'>View Vendor</a>").addTo(map);
	        				<?php
		        				unset($marker);
		        				unset($latlong);
		        				unset($ll);
		        				unset($lat);
		        				unset($long);
		        				unset($mark);
		        				unset($name);
		        				unset($vid);
		        				unset($rating);

		        				$n++;
	        				}
	        			}
	        			?>

	        			function mclick(t)
	        			{
	        				alert(t.latlng);
	        			}

	        			</script>

	        		<?php
	        		}
	        		?>

				</div>
			</div>
		</div>

	</section>
</section>

<!-- Start: Content-Wrapper -->

<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->
<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	// search
	$('#go').click(function(e)
	{
		e.preventDefault();
		var zip = $('#zip').val();
		if(zip=='')
		{
			$('#zip').css('border','1px solid red');
			return false;
		}
		else
		{
			window.location.href='vendor_search.php?zip='+zip;
		}
	});

	// change radius
	$('#new_radius').change(function(e)
	{
		e.preventDefault();
		var zip = '<?php print $zip; ?>';
		var project_id = '<?php print $project_id; ?>';
		var radius = $(this).val();
		var url = 'vendor_search.php?radius='+radius+'&zip='+zip;
		if(project_id!='')
		{
			url=url+'&project_id='+project_id;
		}
		window.location.href=url;
	});

	$('#found').html('<?php print $n-1; ?>');

});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
