<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
if($project['error']!="0")
{
    $vujade->page_redirect('error.php?m=3');
}
$pos = $vujade->get_costing_purchase_orders($project_id );


$html = '<html>
<head>
    <title>Print Costing Ledger</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family:Helvetica;
            
            color:#000;
        }
        body
        {
            width:120%;
            font-family:Helvetica;
            margin:0;
            padding:0;
        }
         
        p
        {
            margin:0;
            padding:0;
        }
         
        #wrapper
        {
            width:210mm;
            margin:0 10mm;
            padding-right: 35px;
        }
         
        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }
        
        th{
            font-size: 7mm;
        }
        td{
            font-size: 6mm;
        }
        
    </style>
</head>
<body>
<div id="wrapper">
    <p style="text-align:center; font-size: 7mm; padding-top:15mm;">Job Costs: Ledger for Project # '.$project['project_id'].'</p>
    <br />
    <table border="1" style="border-spacing:0;width:120%;border-collapse: collapse;">
        <tr style="background-color: rgb(153,153,153);border-spacing: 0; border-collapse: collapse; ">
            <th style="width: 20%;text-align:left;border-spacing: 0;">Date</th>
            <th style="width: 20%;text-align:left;border-spacing: 0;">Cost</th>
            <th style="width: 20%;text-align:left;border-spacing: 0;">Account</th>
            <th style="width: 35%;text-align:left;border-spacing: 0;">Vendor/Description</th>
            <th style="width: 16%;text-align:left;border-spacing: 0;">Invoice #</th>
            <th style="width: 15%;text-align:left;border-spacing: 0;">Check #</th>
            <th style="width: 15%;text-align:left;border-spacing: 0;">PO #</th>
        </tr>';  
$total_materials = 0;      
$total_outsource = 0;
$total_subcontract = 0;  
$subtotal = 0;      
$purchase_orders = $pos;

foreach($purchase_orders as $po) 
{
    if(empty($po['type']))
    {
        continue;
    }
    $subtotal += $po['cost'];
    if($po['type']=="Materials")
    {
        $total_materials+=$po['cost'];
    }
    if($po['type']=="Outsource")
    {
        $total_outsource+=$po['cost'];
    }
    if($po['type']=="Subcontract")
    {
        $total_subcontract+=$po['cost'];
    }
    if(empty($po['vendor_name']))
    {            
        $vendor = $vujade->get_vendor($po['vendor']);
        if(!empty($vendor['name']))
        {
        	$po['vendor_name'] = $vendor['name'];
        }
        else
        {
        	// try to get by list id
        	$vendor2 = $vujade->get_vendor($po['vendor'],'ListID');
        	if(!empty($vendor2['name']))
	        {
	        	$po['vendor_name'] = $vendor2['name'];
	        }
        }
    }

    // print_r($po);
    // print '<hr>';

    $html .= '
    <tr style="padding-left:5px;border-spacing: 0;border-collapse: collapse;">
        <td style="border:1px solid black;">' . $po['date'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . number_format($po['cost'],2,'.',',') . ' &nbsp;</td>
        <td style="border:1px solid black;">' . $po['type'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . $po['vendor_name'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . $po['invoice_number'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . $po['check_number'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . $po['purchase_order_id'] . '&nbsp;</td>
    </tr>';
}

//die;

$html .= '</table>
    <br>
        <div>
            <p>________________________________________________________________________________________&nbsp;</p>
        </div>
        <div>
            <p style="font-size: 3.5mm;">&nbsp;Direct Purchase: $'.number_format($total_materials,2,'.',',').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Outsource: $'.number_format($total_outsource,2,'.',',').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Subcontract: $'.number_format($total_subcontract,2,'.',',').' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subtotal: $'.number_format($subtotal,2,'.',',').'
            </p>
        </div>
    </div> 
</body>
</html>';

require_once("mpdf60/mpdf.php");
$mpdf=new mPDF('c','A4','','Helvetica' , 0 , 0 , 0 , 0 , 0 , 0); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
$mpdf->WriteHTML($html);

// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
    $pdfts = strtotime('now');
    $pdfname = 'mobile_pdf/'.$pdfts.'-costing-ledger.pdf';

    // set to mysql table (chron job deletes these files nightly after they are 1 day old)
    $vujade->create_row('mobile_pdf');
    $pdf_row_id = $vujade->row_id;
    $s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
    $s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
    $mpdf->Output($pdfname,'F');
    print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
    $mpdf->Output('Costing Ledger.pdf','I'); 
}
?>