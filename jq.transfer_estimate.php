<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$setup = $vujade->get_setup();

// validate the estimate that will be transfered from
$transfer_from = $_REQUEST['transfer_from'];
$etf = $vujade->get_estimate($transfer_from,'estimate_id');

// the estimate that will be deleted and the old one copied to
$ttid = $_REQUEST['transfer_to']; // numeric database id
if($ttid=="NEW")
{
	$project_id = $_REQUEST['project_id'];
	$estimates = $vujade->get_estimates_for_project($project_id);
	if($estimates['error']!='0')
	{
		$nextid=$project_id."-01";
	}
	else
	{
		$nextid=$project_id."-".$estimates['next_id'];
	}

	$vujade->create_row('estimates');
	$row_id = $vujade->row_id;
	$s = array();
	$s[] = $vujade->update_row('estimates',$row_id,'estimate_id',$nextid);
	$s[] = $vujade->update_row('estimates',$row_id,'project_id',$project_id);

	$s[] = $vujade->update_row('estimates',$row_id,'design_id',$_REQUEST['design']);
	$s[] = $vujade->update_row('estimates',$row_id,'description',$_REQUEST['old_description']);
	$s[] = $vujade->update_row('estimates',$row_id,'date',$_REQUEST['open_date']);
	$s[] = $vujade->update_row('estimates',$row_id,'approved_by',$_REQUEST['approved_by']);
	$s[] = $vujade->update_row('estimates',$row_id,'created_by',$_REQUEST['estimated_by']);
	$s[] = $vujade->update_row('estimates',$row_id,'required_date',$_REQUEST['date_required']);
	$s[] = $vujade->update_row('estimates',$row_id,'status',$_REQUEST['status']);
	//$s[] = $vujade->update_row('estimates',$row_id,'',$_REQUEST['']);

	if($_REQUEST['template']==1)
	{
		$s[] = $vujade->update_row('estimates',$row_id,'is_template',1);
		$s[] = $vujade->update_row('estimates',$row_id,'template_name','Transferred Template from '.$transfer_from);
		$s[] = $vujade->update_row('estimates',$row_id,'project_id','Template');
	}
	$ttid=$row_id;

	$s[] = $vujade->update_row('estimates',$ttid,'overhead_buyout_rate',$setup['buyout_overhead']);
	$s[] = $vujade->update_row('estimates',$ttid,'overhead_general_rate',$setup['general_overhead']);
	$s[] = $vujade->update_row('estimates',$ttid,'indeterminant',$setup['indeterminant']);
	$s[] = $vujade->update_row('estimates',$ttid,'overhead_machines_rate',$setup['machine_overhead']);
	$s[] = $vujade->update_row('estimates',$ttid,'overhead_materials_rate',$setup['materials_overhead']);

}
$transfer_to = $vujade->get_estimate($ttid);

// update some variables in the transfer to estimate
/*
$s[] = $vujade->update_row('estimates',$ttid,'overhead_buyout_rate',$etf['overhead_buyout_rate']);
$s[] = $vujade->update_row('estimates',$ttid,'overhead_general_rate',$etf['overhead_general_rate']);
$s[] = $vujade->update_row('estimates',$ttid,'indeterminant',$etf['indeterminant']);
$s[] = $vujade->update_row('estimates',$ttid,'overhead_machines_rate',$etf['overhead_machines_rate']);
$s[] = $vujade->update_row('estimates',$ttid,'overhead_materials_rate',$etf['overhead_materials_rate']);
*/
$s[] = $vujade->update_row('estimates',$ttid,'description',$_REQUEST['old_description']);

// user determines this at time of transfer
if($_REQUEST['copy_description']==1)
{
	$s[] = $vujade->update_row('estimates',$ttid,'description',$etf['description']);
}

// can't be invalid
if(empty($etf['error']))
{
	$s=array();

	# step 2: delete all existing materials and copy from the old estimate
	$s[]=$vujade->delete_row('estimates_material',$ttid,1,'estimate_id');
	$existing_materials = $vujade->get_materials_for_estimate($etf['database_id']);
	if(empty($existing_materials['error']))
	{
		//print 'Transferring materials...<br>';
		unset($existing_materials['error']);
		foreach($existing_materials as $m)
		{
			$vujade->create_row('estimates_material');
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'inventory_id',$m['inventory_id']);
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'estimate_id',$transfer_to['database_id']);

			// get the current price of this item
			// the price from the old estimate might be outdated
			if($setup['is_qb']==1)
			{
				$item = $vujade->get_item($m['inventory_id'],'Name');
			}
			else
			{
				$item = $vujade->get_item($m['inventory_id'],'inventory_id');
			}
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'cost',$item['cost']);
			
			// transfer the rest of the data from the old estimate
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'cost_ext',$m['cost_ext']);
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'description',$m['description']);
			//print $m['description'].'<br>';
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'qty',$m['qty']);
			$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'type',$m['type']);
		}
	}

	# step 3: delete any existing labor and copy from the other estimate
	$s[]=$vujade->delete_row('estimates_time',$transfer_to['estimate_id'],1,'estimate_id');

	$existing_labor = $vujade->get_labor_for_estimate($etf['estimate_id']);
	if(empty($existing_labor['error']))
	{
		unset($existing_labor['error']);

		// determine if rate data can be copied from existing
		$error_check = 0;
		foreach($existing_labor as $l)
		{
			//print_r($existing_labor);
			//print $l['hours'];
			//print '<hr>';
			if(empty($l['labor_id']))
			{
				$error_check++;
			}
		}

		// labors in the estimates labor table have the labor id so they can 
		// be matched to the labor rates table

		//print 'error check: '.$error_check.'<hr>';

		if($error_check==0)
		{
			foreach($existing_labor as $l)
			{
				$vujade->create_row('estimates_time');
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'estimate_id',$transfer_to['estimate_id']);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_hours',$l['hours']);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'labor_id',$l['labor_id']);

				// attempt to get the current rate and type for this
				$rate_data = $vujade->get_labor_rate($l['labor_id'],'id');
				if($rate_data['error']=="0")
				{
					unset($rate_data['error']);
					$rate = $rate_data['rate'];
					$type = $rate_data['type'];
				}
				else
				{
					// match wasn't found or id is null / 0 
					$rate=0;
					$type="";
				}
				
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_type',$type);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'rate',$rate);
			}
		}
		else
		{
			// they can't be matched, so the fail safe needs to be run
			// the fail safe gets all labor types and creates a new row for each one in estimates labor, sets the rate and lable, but leaves the hours as 0; the user must fill in the hours
			$new_labor = $vujade->get_labor_types();
			if($new_labor['error']=="0")
			{
				unset($new_labor['error']);
				foreach($new_labor as $nl)
				{
					$vujade->create_row('estimates_time');
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'estimate_id',$transfer_to['estimate_id']);
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_hours',0);
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_type',$nl['type']);					
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'rate',$nl['rate']);
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'labor_id',$nl['database_id']);
				}	
			}
		}
	}

	// step 4: delete existing machine data and transfer from old estimate
	$s[]=$vujade->delete_row('estimates_machines',$transfer_to['estimate_id'],1,'estimate_id');

	$existing_machines = $vujade->get_machines_for_estimate($etf['estimate_id']);
	if(empty($existing_machines['error']))
	{
		unset($existing_machines['error']);

		// determine if rate data can be copied from existing
		$error_check = 0;
		foreach($existing_machines as $l)
		{
			//print_r($existing_labor);
			//print $l['hours'];
			//print '<hr>';
			if(empty($l['labor_id']))
			{
				$error_check++;
			}
		}

		// labors in the estimates labor table have the labor id so they can 
		// be matched to the labor rates table

		//print 'error check: '.$error_check.'<hr>';

		if($error_check==0)
		{
			foreach($existing_machines as $l)
			{
				$vujade->create_row('estimates_machines');
				$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'estimate_id',$transfer_to['estimate_id']);
				$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'time_hours',$l['hours']);
				$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'labor_id',$l['labor_id']);

				// attempt to get the current rate and type for this
				$rate_data = $vujade->get_machine_rate($l['labor_id']);
				if($rate_data['error']=="0")
				{
					unset($rate_data['error']);
					$rate = $rate_data['rate'];
					$type = $rate_data['type'];
				}
				else
				{
					// match wasn't found or id is null / 0 
					$rate=0;
					$type="";
				}
				
				$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'time_type',$type);
				$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'rate',$rate);
			}
		}
		else
		{
			// they can't be matched, so the fail safe needs to be run
			// the fail safe gets all labor types and creates a new row for each one in estimates labor, sets the rate and lable, but leaves the hours as 0; the user must fill in the hours
			$new_labor = $vujade->get_labor_types();
			if($new_labor['error']=="0")
			{
				unset($new_labor['error']);
				foreach($new_labor as $nl)
				{
					$vujade->create_row('estimates_time');
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'estimate_id',$transfer_to['estimate_id']);
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_hours',0);
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_type',$nl['type']);					
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'rate',$nl['rate']);
					$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'labor_id',$nl['database_id']);
				}	
			}
		}
	}

	# step 5: delete existing buyouts and copy old ones over
	$s[]=$vujade->delete_row('estimates_buyouts',$ttid,1,'estimate_id');
	$existing_buyouts = $vujade->get_buyouts($etf['database_id']);
	if(empty($existing_buyouts['error']))
	{
		//print 'Transferring buyouts...<br>';
		unset($existing_buyouts['error']);
		foreach($existing_buyouts as $buyout)
		{
			$vujade->create_row('estimates_buyouts');
			$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'estimate_id',$transfer_to['database_id']);
			$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'cost',$buyout['cost']);
			$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'description',$buyout['description']);
			$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'subcontractor',$buyout['subcontractor']);
			//print 'Description: '.$buyout['description'].'<br>';
		}
	}

	if($_REQUEST['alt_redirect']==1)
	{
		print 'edit_estimate.php?project_id='.$_REQUEST['project_id'].'&estimateid='.$ttid;
	}
	if($_REQUEST['alt_redirect']==2)
	{
		print 'edit_estimate_template.php?&estimateid='.$ttid;
	}
	if(!isset($_REQUEST['alt_redirect']))
	{	
		print 'Success';
	}	
}
else
{
	print 'Invalid estimate.';
}
?>