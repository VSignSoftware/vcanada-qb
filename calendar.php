<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$admin_permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');

$calendar_permissions = $vujade->get_permission($_SESSION['user_id'],'Calendar');
if($calendar_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

# enables event drag, drop, resize
$editable = 1;

unset($_SESSION['view']);
unset($_SESSION['start']);
unset($_SESSION['end']);

if(isset($_REQUEST['view']))
{
	$view=$_REQUEST['view'];
}
else
{
	$view = "month";
}
if(isset($_REQUEST['start']))
{
	$start=$_REQUEST['start'];
}
else
{
	$start = date('Y-m-d');
}
if(isset($_REQUEST['end']))
{
	$start=$_REQUEST['end'];
}

// change color
if($_REQUEST['action']==1)
{
	$id = $_REQUEST['id'];
	$hex = '#'.$_REQUEST['c'];
	$s[]=$vujade->update_row('calendar',$id,'bottom_border','');
	$s[]=$vujade->update_row('calendar',$id,'bottom_border',$hex);
}

$show_ms=0;
if(isset($_REQUEST['show_ms']))
{
    $show_ms=$_REQUEST['show_ms']; 
    $_SESSION['show_ms']=$show_ms;
    unset($_SESSION['view']);
    unset($_SESSION['start']);
    unset($_SESSION['end']);
    if(isset($_REQUEST['view']))
    {
        $view=$_REQUEST['view'];
    }
    else
    {
        $view="month";
    }
    if(isset($_REQUEST['start']))
    {
        $start = urldecode($_REQUEST['start']);
        $start = $start/1000;
        $end = urldecode($_REQUEST['end']);
        $end = $end/1000;
        $avg = $start + $end;
        $avg = $avg/2;
        $start = date('Y-m-d',$avg);
    }
    else
    {
        $start = date('Y-m-d');
    }
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=5;
$title = "Calendar - ";
$calendar=true;
//$charset="iso-8859-1";
require_once('h.php');
?>

<style>
	.jqui-modal
	{
		position: relative;
		z-index: 10;
	}
	table th:first-child 
    {
        width: 150px;
    }

	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    padding-top: 100px; /* Location of the box */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}

	/* Modal Content */
	.modal-content {
	    background-color: #fefefe;
	    margin: auto;
	    padding: 20px;
	    border: 1px solid #888;
	    width: 80%;
	}

	/* The Close Button */
	.close {
	    color: #aaaaaa;
	    float: right;
	    font-size: 28px;
	    font-weight: bold;
	}

	.close:hover,
	.close:focus {
	    color: #000;
	    text-decoration: none;
	    cursor: pointer;
	}
</style>

<!-- popup modal when you click on an event -->
<div id="modal" class="modal" style = "z-index:2;">

  <!-- Modal content -->
  <div class="modal-content">
    <span class="close" id = "close">&times;</span>
    <p id = "modal-data">
    	
    </p>
  </div>

</div>

<input type = "hidden" name = "start_placeholder" id = "start_placeholder">
<input type = "hidden" name = "end_placeholder" id = "end_placeholder">

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- calendar -->
<script src='vendor/plugins/fullcalendar/lib/moment.min.js'></script>
<script src='vendor/plugins/fullcalendar/fullcalendar.min.js'></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/jspdf_old.min.js"></script>
<script src="assets/js/html2canvas.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	$('#calendar').fullCalendar({
        //ignoreTimezone:false,
        //timezone: "America/Chicago",
        defaultView: "<?php print $view; ?>",
        defaultDate: "<?php print $start; ?>",
		allDaySlot: true,
		slotEventOverlap: true,
		minTime: "04:00:00",
		scrollTime: "08:00:00",
        //hiddenDays: [ 0 ],
		header: {
			left: 'prev,next today',
			center: 'title',
			right: 'month,agendaWeek,agendaDay'
		},
        <?php 
        if( ($projects_permissions['edit']==1) && ($calendar_permissions['edit']==1) )
        {
        ?>
		    editable: true,
        <?php
        }
        else
        {
        ?>
            editable: false,
        <?php
        }
        ?>
		events: 'calendar_events.php?show_ms='+'<?php print $show_ms; ?>',
		eventDrop: function(event,dayDelta,minuteDelta,allDay) 
		{
			var id = event.id;
        	var is_all_day = event.allDay;
            if(is_all_day==false)
            {
                var all_day = 0;
            }
            else
            {
                var all_day = 1;
            }
            $('#start_placeholder').val('');
            $('#start_placeholder').val(event.start);
            $('#end_placeholder').val('');
            $('#end_placeholder').val(event.end);
            var s = $('#start_placeholder').val();
            var e = $('#end_placeholder').val();
        	$.post("calendar_update.php", { id: id, start: s, end: e, is_all_day: all_day })
			.done(function( data ) 
			{
			    // get the current view and refresh the calendar
                var view = $('#calendar').fullCalendar('getView');
                window.location.href = "calendar.php?view="+view.name+"&show_ms="+"<?php print $show_ms; ?>"+"&start="+view.start+"&end="+view.end;
			});
    	},
    	eventResize: function(event,dayDelta,minuteDelta,allDay) 
		{
            var id = event.id;
            //alert(id);
            var is_all_day = event.allDay;
            if(is_all_day==false)
            {
                var all_day = 0;
            }
            else
            {
                var all_day = 1;
            }
            $('#start_placeholder').val('');
            $('#start_placeholder').val(event.start);
            $('#end_placeholder').val('');
            $('#end_placeholder').val(event.end);
            var s = $('#start_placeholder').val();
            var e = $('#end_placeholder').val();
            $.post("calendar_update.php", { id: id, start: s, end: e, is_all_day: all_day })
            .done(function( data ) 
            {
                // get the current view and refresh the calendar
                var view = $('#calendar').fullCalendar('getView');
                window.location.href = "calendar.php?view="+view.name+"&show_ms="+"<?php print $show_ms; ?>"+"&start="+view.start+"&end="+view.end;
            });
    	},
    	viewRender: function(view, element) 
    	{ 
    		var view = $('#calendar').fullCalendar('getView');
           	$('#current_view').val('');
            $('#current_view').val(view.name); 
            $('#current_view_start').val('');
            $('#current_view_start').val(view.start); 
            $('#current_view_end').val('');
            $('#current_view_end').val(view.end); 
    	},
        eventRender: function (event, element) 
        {
            // this allows the event title to contain custom html such as a colored bottom border
            //element.find('.fc-event-title').html(event.title);
            element.find('span.fc-title').html(element.find('span.fc-title').text());
            element.click(function(e) 
            {
            	e.preventDefault();

            	// shows a jquery ui modal styled with alert class
            	$("#event-modal").dialog({ modal: true, title: 'test', width:500});

            	// hides the title bar
            	$(".ui-dialog-titlebar").hide();

            	// clear event modal content
            	$('#modal-data').html('');

   				// event data (this will be parsed in php file)
   				var id = event.id;

   				var view = $('#calendar').fullCalendar('getView');

   				var v = view.name;
   				var s = view.intervalStart;

   				$('#view').val(v);
   				$('#start').val(s);

   				//alert(v+" "+s);
   				var hv = $('#view').val();
   				var hs = $('#start').val();

   				// get content back from php with correctly parsed data and form
   				$.post("calendar_event.php", { id: id, v: hv, s: hs })
		        .done(function( data ) 
		        {
		            $('#modal-data').html(data);
		            $('#modal').css('display','inline');
		        });

            });
        }
	});

    $('#print').click(function(e)
    {
        e.preventDefault();
        var win = window.open("wait.html", "_blank");
        //win.documnt.write("<h3>Loading, please wait....</h3>");
        var cache_height = $(".fc-day-grid-container").height();
        if(cache_height < 350){
            $(".fc-day-grid-container").height(350);
        }
        $('body').scrollTop(0);
        
    	createPDF().then(function(doc){
        	var blob = doc.output("blob");
        	win.location.href = URL.createObjectURL(blob);
        	//window.open(URL.createObjectURL(blob));
        	$(".fc-day-grid-container").height(cache_height);
            $(form).width(cache_width);
            $(form).height(cache_height);            
            $(form).css('background', '#fafafa');
            $("#print").show();
            $(form).css("font-size", "1em");    
            $(".fc-other-month").css("opacity", "0.3");
            $(".fc-other-month").css("color", "#777");  
            $(".fc-day-number").css("font-size", "1em"); 
        });
        /*
        var view = $('#calendar').fullCalendar('getView');
        var url="calendar_print.php?show_ms="+"<?php print $show_ms; ?>";
        window.open(url, 'Calendar');
        */
        
    });
    
    //vinit edit
    //create pdf
    var form = $('.bg-light')[0];
	var cache_width = $(form).width();	
	var cache_height = $(form).height();
	var a4  =[ 595.28,  841.89];  // for a4 size paper width and height

    function createPDF()
    {
	    return getCanvas().then(function(canvas){
		    var
		    img = canvas.toDataURL("image/png"),
		    doc = new jsPDF({
              unit:'px',
              format:'a4'
            });
            //$(document).append("<img src='" + img + "'>");
            doc.addImage(img, 'JPEG', 10, 10, 400, 400);
            //doc.save('Calendar.pdf');
            
            //var blob = doc.output("blob");
                       
            return doc;           
	    });
    }

    // create canvas object
    function getCanvas()
    {
        $("#print").hide();
        var btnArray = $($(".fc-button-group")[1]).children("button");
        var m_view_wd1 = 981, m_view_wd2 = 1010, m_view_ht1 = 888, m_view_ht2 = 920;
        var w_view_wd1 = 981, w_view_wd2 = 1010, w_view_ht1 = 1200, w_view_ht2 = 1230;
        $(form).css('background', '#fff');        
        if($(btnArray[0]).hasClass("fc-state-active")){
            //its month view;
            console.log("Month View");
            $(".fc-other-month").css("opacity", "1");
            $(".fc-other-month").css("color", "#ddd");            
            $(form).width(m_view_wd1).css('max-width','none');
    	    $(form).height(m_view_ht1).css('max-height', 'none');
    	    return html2canvas(form,{
	            width: m_view_wd2,
	            height: m_view_ht2,
            	imageTimeout:2000,
            	removeContainer:true
            });
        }else if($(btnArray[1]).hasClass("fc-state-active") || $(btnArray[2]).hasClass("fc-state-active")){
            //its week view;
            console.log("Week View");   
            $(form).width(w_view_wd1).css('max-width','none');
    	    $(form).height(w_view_ht1).css('max-height', 'none');
    	    return html2canvas(form,{
	            width: w_view_wd2,
	            height: w_view_ht2,
            	imageTimeout:2000,
            	removeContainer:true
            });
        }
        var wd = a4[0]*1.5;
        var ht = a4[1]*2;
    }

    $('.close-modal').click(function()
    {
        var view = $('#calendar').fullCalendar('getView');
        var s = view.start;
        window.location.href = "calendar.php?view="+view.name+"&show_ms="+"<?php print $show_ms; ?>"+"&start="+s+"&end="+view.end;
    });

    $('.fc-button').click(function()
    {
        var view = $('#current_view').val();
        var start = $('#current_view_start').val();
        var end = $('#current_view_end').val();
        var c_ms = $('#show_ms').val();
        if(c_ms==1)
        {
            var show_ms=0;
        }
        else
        {
            var show_ms=1;
        }
        $('#show_ms').val(show_ms);
        $.post("calendar_set_view.php", { view: view, start: start, end: end, show_ms: show_ms })
        .done(function( data ) 
        {
            
        });
    });

    <?php 
    // money schedule button 
    if($admin_permissions['read']==1)
    {
        ?>

        $('#msbtn').click(function(e)
        {
            e.preventDefault();
            var view = $('#calendar').fullCalendar('getView');
            <?php
            if($show_ms==1)
            {
            ?>
                window.location.href = "calendar.php?show_ms=0&view="+view.name+"&start="+view.start+"&end="+view.end;
            <?php
            }
            else
            {
            ?>
                window.location.href = "calendar.php?show_ms=1&view="+view.name+"&start="+view.start+"&end="+view.end;
            <?php
            }
            ?>
        });

    <?php } ?>

    /* does not work */
    $('body').on('change','#hidden-event-id',function()
    {
    	alert('changed.');
    });

    $('body').on('click','#event-modal-close',function()
    {
    	// $("#event-modal").dialog('close');
        $("#event-modal").hide();
    });

    // change the color
	$('body').on('change','.color',function()
	{
		var id = $('#event_id').val();
		var c = $('input[name=color]:checked', '#form').val()
		var href = "calendar.php?action=1&c="+c+"&id="+id+"&view=<?php print $view; ?>&start=<?php print $start; ?>";
		window.location.href=href;

		/*
		var id = "<?php print $event_id; ?>";
		var c = $('input[name=color]:checked', '#form').val()
		$.post("calendar_color_update.php", { id: id, hex: c })
		.done(function( data ) 
		{
			//$('#result2').css('border','2px solid #3275cd');
		    $('#result2').html(data);
		    $('#result2').show();
		    $('#done-p').show();
		});
		*/
	});

	// close the modal
	$('body').on('click','#close',function()
	{
		$('#modal').hide();
		//var view = $('#calendar').fullCalendar('getView');
        //var s = view.start;
        //window.location.href = "calendar.php?view="+view.name+"&show_ms="+"<?php print $show_ms; ?>"+"&start="+s+"&end="+view.end;
        var href = "calendar.php?view=<?php print $view; ?>&start=<?php print $start; ?>";
        //window.location.href = "calendar.php?view="+view.name+"&show_ms="+"<?php print $show_ms; ?>"+"&start="+s+"&end="+view.end;
        window.location.href=href
	});
	$('body').on('click','#close-modal',function()
	{
		$('#modal').hide();
		//var view = $('#calendar').fullCalendar('getView');
        //var s = view.start;
        var href = "calendar.php?view=<?php print $view; ?>&start=<?php print $start; ?>";
        //window.location.href = "calendar.php?view="+view.name+"&show_ms="+"<?php print $show_ms; ?>"+"&start="+s+"&end="+view.end;
        window.location.href=href;
	});
});
</script>

<input type = "hidden" name = "current_view" id = "current_view">
<input type = "hidden" name = "current_view_start" id = "current_view_start">
<input type = "hidden" name = "current_view_end" id = "current_view_end">
<input type = "hidden" name = "show_ms" id = "show_ms" value = "<?php print $_SESSION['show_ms']; ?>">

<style>

    .fc-grid, .fc-day, .fc-day-header
    {
        border:1px solid #3C3C3D;
    }

    .fc-event
    {
        border-width: 2px;
    }

    .underline_controller
    {
        font-size:14px;
    }

    .underline_controller a:link, a:visited
    {
        text-decoration: none;
    }

    .underline_controller a:hover
    {
        text-decoration: none;
    }

    .underline_controller a:visited
    {
        text-decoration: none;
    }

    #color_picker
    {
        width: 600px;
        padding: 30px; 
        display:none;
        background: #FFF;
        border-radius: 5px; 
        -moz-border-radius: 5px; 
        -webkit-border-radius: 5px;
        box-shadow: 0px 0px 4px rgba(0,0,0,0.7); 
        -webkit-box-shadow: 0 0 4px rgba(0,0,0,0.7); 
        -moz-box-shadow: 0 0px 4px rgba(0,0,0,0.7);   
    }

    #color_picker p 
    { 
        color: #666; text-shadow: none; 
    }

    a.cp:link {text-decoration: none;}
    a.cp:visited {text-decoration: none;}
    a.cp:hover {text-decoration: none;}
    .cp, .fc-event-title
    {
        margin-bottom:0px;
        padding-left: 0px;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom:1px;
        text-overflow:ellipsis;
    }

    .event-title
    {
    	text-overflow:ellipsis;
    }

    #ms_toggle_div
    {
        /*
        position:absolute;
        left: 375px;
        top: 225px;
        width:250px;
        height:100px;
        */
    }

    .ms
    {
        width:25px;
        border-radius: 5px; 
        -moz-border-radius: 5px; 
        -webkit-border-radius: 5px;
        box-shadow: 0px 0px 4px rgba(0,0,0,0.7); 
        -webkit-box-shadow: 0 0 4px rgba(0,0,0,0.7); 
        -moz-box-shadow: 0 0px 4px rgba(0,0,0,0.7);  
        text-align: center; 
    }

    <?php
    // money schedule is active
    if($show_ms==1)
    {
    ?>
        a.ms:link
        {
            color:#27AAE0;
            width:25px;
            text-decoration: none;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
        a.ms:visited
        {
            color:#27AAE0;
            width:25px;
            text-decoration: none;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
        a.ms:hover
        {
            color:#ececec;
            width:25px;
            text-decoration: none;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
    <?php
    // money schedule is inactive
    }
    else
    {
    ?>
        a.ms:link
        {
            color:#ececec;
            width:25px;
            text-decoration: none;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
        a.ms:visited
        {
            color:#ececec;
            width:25px;
            text-decoration: none;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
        a.ms:hover
        {
            color:#27AAE0;
            width:25px;
            text-decoration: none;
            padding-left:10px;
            padding-right: 10px;
            padding-top: 2px;
            padding-bottom: 2px;
        }
    <?php
    }
    ?>

    #ms_toggle
    {
        font-size:18px;
        margin-top: 0px;
        padding-top: 0px;
        color:#cecece;
    }
	
	@media (max-width: 480px){
		.fc-toolbar .fc-right{
				float: left;
				margin-top: 10px !important;			
		}		
		.fc-center{
			margin-top: 10px !important;
		}
	}

</style>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#" id = "page-title">Calendar</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading" style="overflow: visible;">

					<a class = "btn btn-primary" href="schedule.php">Schedule</a> 
					<a class = "btn btn-primary" href="calendar.php" style = "margin-left:15px;">Calendar</a> 
					
					<input type = "hidden" id = "view"><input type = "hidden" id = "start">
				</div>
	        	<div class="panel-body bg-light">

	        		<!-- legend and money schedule button -->
				    <div div class = "row">
				        <div class = "col-md-12">
					<div class="row" style="padding: 10px;">
				            <?php
				            $data = $vujade->get_legend_data();
				            if($data['error']=="0")
				            {
				                unset($data['error']);
				                //print '<ul>';
				                foreach($data as $legend)
				                {
				                    print '<div class="col-xl-2 col-lg-2 col-md-4 col-sm-6 col-xs-12"  style="padding: 0px;"><div style = "border:1px solid #eeeeee; width:25px;height:15px;background-color:'.$legend['hex'].';display:block;float:left">&nbsp;</div><div style = "float:left;margin-left: 2px; margin-right: 0px;font-size: 0.9em;">= '.$legend['explanation'].'</div></div>';
				                }
				                //print '</ul>';
				            }
				            ?>
					</div>
				            </div>
				    </div>
					<div id='calendar' class="admin-theme"></div><br/><br/>
				            <div style = "float:right;margin-right:15px;">
				                <?php if($admin_permissions['read']==1)
				                {
				                    ?>
				                    <div id="ms_toggle_div">
				                        <h1 id = "ms_toggle" style = "width:20px;text-align:center;cursor:pointer;"><a href = "#" id = "msbtn">$</a>
				                     	</h1>
				                    </div>
				                <?php } ?>
				            </div>
					<center>
						<a href = "#" id = "print" class = "btn btn-primary">Print</a>
					</center>

				</div>
			</div>
		</div>
		</section>
	</section>

</body>
</html>