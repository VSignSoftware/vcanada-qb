<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$customer_permissions = $vujade->get_permission($_SESSION['user_id'],'Clients');
if($customer_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$id = $_REQUEST['id'];
$note = $vujade->get_note($id);
$employee = $vujade->get_employee($_SESSION['user_id']);
$post_to=$_REQUEST['post_to'];
if($post_to==1)
{
	$post_to_url='customer.php';
	$action=10;
	$ahref = 'customer.php?id='.$note['customer_vendor_id'].'&action=9&note_id='.$note['id'];
}
if($post_to==2)
{
	$post_to_url='vendor.php';
	$action=7;
	$ahref = 'vendor.php?id='.$note['customer_vendor_id'].'&action=8&note_id='.$note['id'];
}
?>

<p>
	<b><?php print $note['date'].' - '.$note['name_display']; ?></b>
</p>
<form method = "post" action = "<?php print $post_to_url; ?>" id = "update-note-form">
	<input type = "hidden" name = "action" value = "<?php print $action; ?>">
	<input type = "hidden" name = "customer_vendor_id" value = "<?php print $note['customer_vendor_id']; ?>">
	<input type = "hidden" name = "note_id" value = "<?php print $note['id']; ?>">
	<textarea name = "notes" id = "notes" class = "form-control ckeditor" style = "width:100%;" rows = "10"><?php print $note['body']; ?></textarea>
</form>
<br>
<div class = "pull-right">
	<a href = "#" id = "update-note" class = "btn btn-success">Save</a> 
	<a href = "#delete-note-modal" id = "delete-note" class = "btn btn-danger" data-href = "<?php print $ahref; ?>">Delete</a>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() 
{

	// update note
	$('#update-note').click(function(e)
    {
    	e.preventDefault();
    	$('#update-note-form').submit();
    });

	// delete note modal
	$('#delete-note').click(function() 
    {
		$('#delete-note-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-note-modal',
		modal: true
	});

    // dismiss any modal
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});
</script>