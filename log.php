<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');

$pcount = 0;
$pcount+=$accounting_permissions['read'];
$pcount+=$accounting_permissions['edit'];
$pcount+=$accounting_permissions['delete'];

$pcount+=$er_permissions['read'];
$pcount+=$er_permissions['edit'];
$pcount+=$er_permissions['delete'];

if($pcount!=6)
{
	$vujade->page_redirect('error.php?m=1');
}

// setup for paging
$start = 0;
# start value
$perpage = 25;
if(isset($_GET["page"]))
{
	$page = $_GET["page"];
	if(!ctype_digit($page))
	{
		$page = 1;
	}
}
else
{
	$page = 1;
}
$calc = $perpage * $page;
$start = $calc - $perpage;

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "User Access Log - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">User Activity Log</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu pull-right">
						<select name = "eid" id = "eid" class = "" style = "width:200px;float:left;margin-right:5px;margin-top:0px;">
							<option value = "">Select Employee</option>
							<?php
							$employees = $vujade->get_employees(3);
							if($employees['error']=="0")
							{
								unset($employees['error']);
								foreach($employees as $opt)
								{
									print '<option value = "'.$opt['database_id'].'">'.$opt['first_name'].' '.$opt['last_name'].'</option>';
								}
							}
							?>
						</select> 
						<input class = "btn btn-xs btn-primary" type = "submit" id = "filter" value = "SUBMIT" style = "float:left;margin-right:5px;"> <input class = "btn btn-xs btn-primary" type = "submit" id = "reset" value = "RESET" style = "float:left;margin-right:5px;">
					</div>
				</div>
	        	<div class="panel-body bg-light">
					<?php 
					// log was filtered
					if(isset($_REQUEST['eid']))
					{
						$eid=$_REQUEST['eid'];
						if( (!empty($eid)) && (ctype_digit($eid)) )
						{
							$activity = $vujade->get_activity_data(1,$eid,$start);
						}
						else
						{
							$activity = $vujade->get_activity_data('','',$start);
						}	
					}
					else
					{
						// default 
						$activity = $vujade->get_activity_data('','',$start);
						$eid="";
					}
					
					//print_r($activity);

					if($activity['error']=="0")
					{
						$total = $activity['count'];
						unset($activity['sql1']);
						unset($activity['sql2']);
						unset($activity['error']);
						unset($activity['count']);
						$totalPages = ceil($total / $perpage);
						?>
						<table id="datatable" class="employees-table table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>Time</strong></td>
									<td valign = "top"><strong>User</strong></td>
									<td valign = "top"><strong>Page</strong></td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">
						    <form method = "post" action = "taxes.php">	
							<?php
							foreach($activity as $act)
							{
						        print '<tr class = "">';

						        print '<td valign = "top" style = "">';
						        $ts = strtotime($act['ts']);
								$date=date('l, F j, Y g:i a',$ts);
								print $date;
						        print '</td>';

						        print '<td valign = "top" style = "">';
						        $remp = $vujade->get_employee($act['user_id']);
								if($remp['error']=="0")
								{
									print $remp['first_name'].' '.$remp['last_name'];
								}
								else
								{
									print 'The user id was either not set or the user was not logged in. The database returned this error: '.$remp['error'];
								}
								print '</td>';

						        print '<td valign = "top" style = "">';
						        print $act['url'];
						        print '</td>';

								print '</tr>';
							}

							?>
							</tbody>
						</table>


					<?php
					}
					else
					{
						$vujade->set_error('Could not fetch user activity from database. ');
						$vujade->set_error($activity['error']);
						$vujade->show_errors();
						$totalPages = 1;
					}
					?>

					<div id = "paging" style = "margin-left:auto;margin-right:auto;text-align:center;">
					</div>

				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
 <script src="assets/js/paging.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	$('#paging').twbsPagination({
    totalPages: <?php print $totalPages; ?>,
    visiblePages: 25,
    href: 'log.php?page={{number}}<?php if(!empty($eid)){ print "&eid=".$eid; } ?>'
	});

    $('#filter').click(function(e)
	{
		e.preventDefault();
		var eid = $('#eid').val();
		if(eid=='')
		{
			alert('No employee selected.');
		}
		else
		{
			window.location.href="log.php?eid="+eid;
		}
	});

	$('#reset').click(function(e)
	{
		e.preventDefault();
		window.location.href="log.php";
	});

});
</script>

</body>
</html>