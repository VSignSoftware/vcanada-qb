<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id=$_REQUEST['project_id'];
$id = $project_id;
$nid = $_REQUEST['nid'];
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$sec=$_REQUEST['section'];
$valid=array(1,2,3,4,5,6,7,8);
if(!in_array($sec, $valid))
{
	$sec=1;
}

if($sec==1)
{
	$tab=0;
}
if($sec==2)
{
	$tab=1;
}
if($sec==3)
{
	$tab=2;
}
if($sec==4)
{
	$tab=3;
}
if($sec==5)
{
	$tab=4;
}
if($sec==6)
{
	$tab=5;
}
if($sec==7)
{
	$tab=6;
}
if($sec==8)
{
	$tab=7;
}

$section_data = $vujade->get_job_status_section($sec);

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# add a note
if($action==1)
{
	$date = $_REQUEST['date'];
	$msg = $_REQUEST['msg'];
	$s = array();
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$s[]=$vujade->update_row('job_status_notes',$nid,'date',$date);
	$s[]=$vujade->update_row('job_status_notes',$nid,'body',$msg);
	$s[]=$vujade->update_row('job_status_notes',$nid,'user_id',$_SESSION['user_id']);
	$s[]=$vujade->update_row('job_status_notes',$nid,'ts',strtotime($date));
	$s[]=$vujade->update_row('job_status_notes',$nid,'name_display',$employee['fullname']);
	$vujade->page_redirect('project_job_status.php?id='.$id.'&note_id='.$nid."&tab=".$tab);
}
$note = $vujade->get_job_status_note($nid);
$shop_order = $vujade->get_shop_order($project_id, 'project_id');
$section=3;
$menu = 7;
$title = "Edit Note - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->

<section id="content_wrapper">

	<!-- Start: Topbar -->

	<header id="topbar">

		<div class="topbar-left">

			<ol class="breadcrumb">

				<li class="crumb-active">

					<a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>

				</li>

			</ol>

		</div>

	</header>

	<!-- Begin: Content -->
	<section id="content" class="animated fadeIn">

		<div class="admin-form theme-primary">

			<div class="panel heading-border panel-primary">

				<div class="panel-body bg-light">

					<div id="" style = "margin-top:15px;">
						<style>
							#form label{
								margin: 0 0 10px;
							}
						</style>

						<!-- form -->
						<div class = "row">
							<div class = "col-md-12">
								<!-- form -->
								<form id = "form" method = "post" action = "edit_js_note.php">
									<strong>Date:</strong><br>
									<input type = "text" name = "date" id = "date" style = "width:200px;" class = "form-control" value = "<?php print $note['date']; ?>">
									<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
									<br>
									<br>
									<strong>Message:</strong><br>
									<textarea name = "msg" id = "msg" style = "width:400px;height:100px;" class = "form-control ckeditor"><?php print $note['body']; ?>	
									</textarea>
									<div id = "error_3" style = "display:none;">This field cannot be empty.</div>
									<br>
									<br>
									<input type = "hidden" name = "action" value = "1">
									<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
									<input type = "hidden" name = "section" value = "<?php print $sec; ?>">
									<input type = "hidden" name = "nid" id = "nid" value = "<?php print $nid; ?>">
									<input type = "submit" id = "save" value = "SAVE" class = "btn btn-primary"> 
									<button id = "cancel" class = "btn btn-warning">CANCEL</button>
								</form>
							</div>
						</div>

						<!-- ckeditor new version 4.5x -->
						<?php require_once('ckeditor.php'); ?>
						
					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		"use strict";

		// Init Theme Core
		Core.init();

		// date picker
		$('#date').datepicker();

		// fix for text areas
		var n = $('#msg').html();
	    $("#msg").html($.trim(n));

	    $('#save').click(function(e)
	    {
	    	e.preventDefault();
		
			var date = $('#date').val();
			var message = $('#msg').text();
			var error = 0;
			if(date=="")
			{	
				$('#error_1').css('background-color','#C60F13');
				$('#error_1').css('color','white');
				$('#error_1').css('font-weight','bold');
				$('#error_1').css('font-size','16px');
				$('#error_1').css('padding','3px');
				$('#error_1').show();
				error++;
			}
			else
			{
				$('#error_1').hide();
			}
			if(error==0)
			{
				$('#form').submit();
			}

		});

	    $('#cancel').click(function(e)
	    {
	    	e.preventDefault();

	    	window.location="project_job_status.php?tab=<?php print $tab; ?>&id=<?php print $project_id; ?>";
	    });
	});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>