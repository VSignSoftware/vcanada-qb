<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');
if($_SESSION['is_admin']==1)
{
	$permissions['read']=1;
}
if($permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();

if(!isset($_REQUEST['date']))
{
	// start and end of the current month
	$test_date = date('m/d/Y');
	$date = date('m/01/Y');
	$report_t=date('M Y');
}
else
{
	// user selected month and year or fiscal year
	$test_date = $_REQUEST['date'];
	$date = $_REQUEST['date'];
	
	// fiscal year
	if($test_date[0]=="F")
	{
		$year_value = $test_date[3].$test_date[4].$test_date[5].$test_date[6];
		$report_t="Fiscal Year ".$year_value;
		$fiscal_year_search=true;
	}
	
	// year and month
	if(!isset($report_t))
	{
		//$report_t=$date;
		$td = strtotime($test_date);
		$td = date('M Y',$td);
		$report_t=$td;
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$title = 'Volume Report - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Volume Report - <?php print $report_t; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "reports.php" class = "btn btn-primary btn-sm" style = "width:100px;">Back</a>
						<select name = "date" id = "date" class = "pull-right form-control" style = "width:200px;">
							<option value = "">-Change Month-</option>
							<?php
							for ($i=0; $i<=12; $i++) 
							{ 
								print '<option value="'.date('Y-m-01', strtotime("-$i month")).'">'.date('m-Y', strtotime("-$i month")).'</option>';
							}

							// fiscal years
							if( (!empty($setup['fiscal_year_start'])) && (!empty($setup['fiscal_year_end'])) ) 
							{
								print '<option value = "">------------</option>';
								$fystartmonth = $vujade->get_month_name($setup['fiscal_year_start']);
								$fyendmonth = $vujade->get_month_name($setup['fiscal_year_end']);

								// past fiscal years
								// start at 2014
								$option_start = 2014;
								$option_current_year = date('Y');
								while($option_start<$option_current_year)
								{
									print '<option value = "FY-'.$option_start.'">Fiscal Year '.$option_start.' ('.$fystartmonth.' - '.$fyendmonth.')</option>';

									$option_start=$option_start+1;
								}

								// current fiscal year
								print '<option value = "FY-'.(date('Y')).'">Fiscal Year '.(date('Y')).' ('.$fystartmonth.' - '.$fyendmonth.')</option>';

								// next fiscal year
								print '<option value = "FY-'.(date('Y')+1).'">Fiscal Year '.(date('Y')+1).' ('.$fystartmonth.' - '.$fyendmonth.')</option>';
							}
							?>
						</select>
					</div>
				</div>
	        	<div class="panel-body bg-light">
	        		
	        		<!-- pie chart goes here -->
	        		<div id = "pie-chart" style = "width:100%;height:300px;margin-bottom:15px;"></div>

	        		<p>&nbsp;</p>

	        		<?php 
	        		if(!$fiscal_year_search)
	        		{
	        			/*
	        			// first day of the month
						$first = date('Y-m-01', strtotime($test_date));

						// last day of the month
						$last = date('t',strtotime($test_date));
		        		$start = strtotime($first);
		        		$end = strtotime(date('Y-m-'.$last));
		        		*/

		        		// first day of the month
						$tds = strtotime($test_date);
						$first = date('Y-m-01',$tds);

						// last day of the month

						$last = date('Y-m-t',strtotime($first));
						$start = strtotime($first);
						$end = strtotime($last);

	        		}
	        		else
	        		{
	        			// get by fiscal year

						// fiscal year does not start on Jan. 1
						if($setup['fiscal_year_start']!=1)
						{	
							// any year in the past 
							if($year_value<$option_current_year)
							{
								// start
								$y=$year_value;

								// end
								$y2=$year_value+1;
							}

							// current year
							if($year_value==$option_current_year)
							{
								// start
								$y=$option_current_year-1;

								// end
								$y2=$year_value;
							}

							// next year
							if($year_value>$option_current_year)
							{
								// start
								$y=$option_current_year;

								// end
								$y2=$year_value;
							}
	
							$m=$setup['fiscal_year_start'];
							$m2=$setup['fiscal_year_end'];
							$start = strtotime($y.'-'.$m.'-01');
							$date1 = $y.'-'.$m2.'-01'; 
							$last_day = date('t',strtotime($date1));
							$end = strtotime($y2.'-'.$m2.'-'.$last_day);
						}
						else
						{
							$y=$year_value;
							$y2=$year_value;
							$m=$setup['fiscal_year_start'];
							$m2=$setup['fiscal_year_end'];
							$start = strtotime($y.'-'.$m.'-01');
							$date1 = $y.'-'.$m2.'-01'; 
							$last_day = date('t',strtotime($date1));
							$end = strtotime($y2.'-'.$m2.'-'.$last_day);
						}
	        		}

	        		/*
	        		print 'start and end ts: ';
					print $start.' '.$end.'<br>';
					die;
					*/
					
	        		$total_opened = 0;
	        		$total_processed = 0;
	        		$total_did_not_sell = 0;
	        		$total_closed = 0;

	        		// total projects opened
	        		$tpo=0;

	        		// total projects closed
	        		$tpc=0;

	        		// total projects processed
					$tpp=0;

	        		// closed percentage (avg)
	        		$avgc=array();

	        		// proposal volume total
	        		$tpv=0;

	        		// sales volume total
	        		$tsv=0;

	        		$salespeople=$vujade->get_salespeople(1);
	        		if($salespeople['error']=="0")
	        		{
	        			unset($salespeople['error']);
	        			print '<table class = "table" width = "100%">';

	        			foreach($salespeople as $sp)
	        			{
	        				$projects = $vujade->get_dashboard_projects($start,$end,$sp['fullname']);

	        				$processed_jobs = $vujade->get_processed_projects($start,$end,$sp['fullname']);

	        				//print $processed_jobs['sql'];
							//print '<br>';

	        				$proposal_data = $vujade->get_proposals_for_volume_report($start,$end,$sp['fullname']);

	        				// row 1
	        				print '<tr>';

	        				print '<td width = "">';
	        				print $sp['fullname'];
	        				print '</td>';

	        				print '<td width = "">';
	        				print 'Projects Opened: ';
	        				print '</td>';

	        				print '<td>';
	        				print $projects['count'];
	        				$tpo+=$projects['count'];
	        				print '</td>';

	        				print '<td>Proposal Volume: ';
	        				print '</td>';

	        				print '<td>';
	        				print @number_format($proposal_data['proposal_volume'],2);
	        				$tpv+=$proposal_data['proposal_volume'];
	        				print '</td>';

	        				print '</tr>';

	        				// row 2
	        				print '<tr>';

	        				print '<td>&nbsp;';
	        				print '</td>';

	        				print '<td>';
	        				print 'Projects Processed: ';
	        				print '</td>';

	        				print '<td>';
	        				print $processed_jobs['count'];
	        				$tpp+=$processed_jobs['count'];
	        				print '</td>';

	        				print '<td>';
	        				print 'Sales Volume: ';
	        				print '</td>';

	        				print '<td>';
	        				$rd = $vujade->get_individual_yearly_sales_report($start,$end,$sp['fullname']);
	        				$dv = 0;
	        				unset($rd['error']);
							unset($rd['sql']);
	        				foreach($rd as $prd)
							{
								$do=$prd['open_date'];
								$do=strtotime($do);
								if(($do>=$start) && ($do<=$end))
								{
									$lt = str_replace(',','',$prd['line_total']);
									$dv+=$lt;
								}
								unset($lt);
								unset($do);
							}
							unset($rd);
							print @number_format($dv,2);
							$tsv+=$dv;
							$dv=0;
	        				print '</td>';

	        				print '</tr>';

	        				// row 3
	        				print '<tr>';

	        				print '<td>&nbsp;';
	        				print '</td>';

	        				print '<td>';
	        				print 'Closed %: ';
	        				print '</td>';

	        				print '<td>';
	        				// closed rate = processed jobs / total opened jobs
	        				if( ($projects['opened_jobs']>0) && ($processed_jobs['count']>0) )
	        				{
	        					$number_closed = $processed_jobs['count']/$projects['opened_jobs'];
	        					$number_closed=round($number_closed,2);
	        					$number_closed=$number_closed*100;
	        					print $number_closed;
	        					//$avgc[]=$number_closed;
	        				}
	        				else
	        				{
	        					print '0';
	        				}
	        				print '</td>';

	        				print '<td>&nbsp;';
	        				print '</td>';

	        				print '<td>&nbsp;';
	        				print '</td>';

	        				print '</tr>';

	        				// debugging
	        				/*   
	        				print '<tr>';
	        				print '<td colspan = "5">sql 1: '.$projects['sql'];
	        				print '</td>';
	        				print '</tr>';
							
							print '<tr>';
	        				print '<td colspan = "5">sql 2: '.$processed_jobs['sql'];
	        				print '<br>sql 3: ';
	        				print $processed_jobs['sql2'];
	        				print '</td>';
	        				print '</tr>';
							*/
							
	        				// blank row
	        				print '<tr>';
	        				print '<td colspan = "5">&nbsp;';
	        				print '</td>';
	        				print '</tr>';

	        				// counters
	        				$total_opened+=$projects['opened_jobs'];
	        				$total_processed+=$processed_jobs['count'];
	        				$total_did_not_sell+=$projects['did_not_sell_jobs'];
	        				$total_opened+=$projects['did_not_sell_jobs'];
	        				$total_closed+=$projects['has_closed_date'];
	        			}
	        			if($total_opened>0)
	        			{
	        				$total_closed_rate = @$total_processed/$total_opened;
	        				$total_closed_rate = round($total_closed_rate,2);
	        				$total_closed_rate=$total_closed_rate*100;
	        			}
	        			else
	        			{
	        				$total_closed_rate=0;
	        			}

	        			// summary
	        			print '<tr>';

        				print '<td>Total Opened: '.$tpo;
        				print '</td>';

        				print '<td>';
        				print 'Total Processed: '.$tpp;
        				print '</td>';

        				print '<td>';
        				print 'Total Closed: '.round(($tpp/$tpo)*100).'%';
        				print '</td>';

        				print '<td>Proposal Volume: $'.number_format($tpv,2);
        				print '</td>';

        				print '<td>Sales Volume: $'.number_format($tsv,2);
        				print '</td>';

        				print '</tr>';

	        			// print button
	        			print '<tr>';

        				print '<td colspan = "5"><center><a href = "#" id = "print" class = "btn btn-primary">Print</a>';
        				print '</td>';

        				print '</tr>';

	        			print '</table>';
	        			?>

	        			<!-- pie chart js -->
	        			<script type="text/javascript" src="vendor/plugins/canvas/canvasjs.min.js"></script>
						<script type="text/javascript">
						window.onload = function () 
						{
							// pie chart colors
							CanvasJS.addColorSet("pie_chart_colors",
						    [
						    	"#0DAEE8",
								"#12FF2A",
								"#FFA60A"
						    ]);

							// pie chart

							var chart = new CanvasJS.Chart("pie-chart",
							{
								title:{
									text: "Volume Report"
								},
								subtitles:[
								{
									text: "Total Opened: <?php print $total_opened; ?>",
									verticalAlign: "bottom",
									fontSize:15
								},
								{
									text: "Total Processed: <?php print $total_processed; ?>",
									verticalAlign: "bottom",
									fontSize:15
								},
								{
									text: "Total Close Rate: <?php print $total_closed_rate; ?>%",
									verticalAlign: "bottom",
									fontSize:15
								}
								],
						        animationEnabled: true,
								legend: {
									verticalAlign: "bottom",
									horizontalAlign: "center"
								},
								colorSet:  "pie_chart_colors",
								data: [
								{        
									type: "pie",
									indexLabelFontFamily: "Garamond",       
									indexLabelFontSize: 20,
									indexLabelFontWeight: "bold",
									startAngle:0,
									indexLabelFontColor: "MistyRose",       
									indexLabelLineColor: "darkgrey", 
									indexLabelPlacement: "inside", 
									toolTipContent: "{name}: {y}",
									showInLegend: true,
									indexLabel: "{y}", 
									dataPoints: [
										{  y: <?php print $total_opened; ?>, name: "Open", legendMarkerType: "square"},
										{  y: <?php print $total_processed; ?>, name: "Processed", legendMarkerType: "square"}
										,
										{  y: <?php print $total_did_not_sell; ?>, name: "Did Not Sell", legendMarkerType: "square"}
									]
								}
								]
							});
							chart.render();
						}
						</script>
					<?php
	        		}
	        		else
	        		{
	        			$vujade->set_error('No salespeople found. ');
						$vujade->show_errors();
	        		}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
function GetParameterValues(param) 
{  		
	var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');  		
	for (var i = 0; i < url.length; i++) {  		
	    var urlparam = url[i].split('=');  		
	    if (urlparam[0] == param) {  		
	        return urlparam[1];  		
	    }  		
	}  		
} 

jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // date was changed
    $('#date').change(function()
    {
    	var date = $(this).val();
    	if(date!='')
    	{
    		var href = "volume_report.php?date="+date;
    		window.location.href=href;
    	}
    });


    $("#print").click(function(e){		
    	e.preventDefault();		
    	$("#loader").removeClass('f-hidden');		
    	var img = document.getElementsByTagName("canvas")[0].toDataURL();		
    	$.ajax({		
    		type: 'POST',		
    		url: 'print_volume_report.php',		
    		data: {		
    			img: img		
    		}		
    	}).done(function(o){    				
    		$("#loader").addClass('f-hidden');		
    		var loc = "";		
    		if(GetParameterValues('date') == undefined) {		
    			var d = new Date();		
    			var dstr = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();		
    			loc = 'print_volume_report.php?file=' + o + "&date=" + dstr;    					
    		}else{		
    			loc = 'print_volume_report.php?file=' + o + "&date=" + GetParameterValues('date');	    					
    		}    				
    		window.open(loc, "_blank");		
    				
    		    				
    	});		
    	return false;		
    });


});
</script>

<div class="full-wide f-hidden" id="loader" >		
	<div id="circularG" style="margin-top: 50px;">		
		<div id="circularG_1" class="circularG"></div>		
		<div id="circularG_2" class="circularG"></div>		
		<div id="circularG_3" class="circularG"></div>		
		<div id="circularG_4" class="circularG"></div>		
		<div id="circularG_5" class="circularG"></div>		
		<div id="circularG_6" class="circularG"></div>		
		<div id="circularG_7" class="circularG"></div>		
		<div id="circularG_8" class="circularG"></div>		
	</div>		
</div>		
<style>		
.f-hidden{		
	display: none;		
}		
.full-wide{		
	width: 100vw;		
	height: 100vh;		
	z-index: 10000;		
	background: rgba(220, 220, 220, 0.5);		
	position: fixed;		
	top: 0px;		
	left: 0px;			
}		
#circularG{		
	position:relative;		
	width:58px;		
	height:58px;		
	margin: auto;			
}		
.circularG{		
	position:absolute;		
	background-color:rgb(0,0,0);		
	width:14px;		
	height:14px;		
	border-radius:9px;		
		-o-border-radius:9px;		
		-ms-border-radius:9px;		
		-webkit-border-radius:9px;		
		-moz-border-radius:9px;		
	animation-name:bounce_circularG;		
		-o-animation-name:bounce_circularG;		
		-ms-animation-name:bounce_circularG;		
		-webkit-animation-name:bounce_circularG;		
		-moz-animation-name:bounce_circularG;		
	animation-duration:1.1s;		
		-o-animation-duration:1.1s;		
		-ms-animation-duration:1.1s;		
		-webkit-animation-duration:1.1s;		
		-moz-animation-duration:1.1s;		
	animation-iteration-count:infinite;		
		-o-animation-iteration-count:infinite;		
		-ms-animation-iteration-count:infinite;		
		-webkit-animation-iteration-count:infinite;		
		-moz-animation-iteration-count:infinite;		
	animation-direction:normal;		
		-o-animation-direction:normal;		
		-ms-animation-direction:normal;		
		-webkit-animation-direction:normal;		
		-moz-animation-direction:normal;		
}		
#circularG_1{		
	left:0;		
	top:23px;		
	animation-delay:0.41s;		
		-o-animation-delay:0.41s;		
		-ms-animation-delay:0.41s;		
		-webkit-animation-delay:0.41s;		
		-moz-animation-delay:0.41s;		
}		
#circularG_2{		
	left:6px;		
	top:6px;		
	animation-delay:0.55s;		
		-o-animation-delay:0.55s;		
		-ms-animation-delay:0.55s;		
		-webkit-animation-delay:0.55s;		
		-moz-animation-delay:0.55s;		
}		
#circularG_3{		
	top:0;		
	left:23px;		
	animation-delay:0.69s;		
		-o-animation-delay:0.69s;		
		-ms-animation-delay:0.69s;		
		-webkit-animation-delay:0.69s;		
		-moz-animation-delay:0.69s;		
}		
#circularG_4{		
	right:6px;		
	top:6px;		
	animation-delay:0.83s;		
		-o-animation-delay:0.83s;		
		-ms-animation-delay:0.83s;		
		-webkit-animation-delay:0.83s;		
		-moz-animation-delay:0.83s;		
}		
#circularG_5{		
	right:0;		
	top:23px;		
	animation-delay:0.97s;		
		-o-animation-delay:0.97s;		
		-ms-animation-delay:0.97s;		
		-webkit-animation-delay:0.97s;		
		-moz-animation-delay:0.97s;		
}		
#circularG_6{		
	right:6px;		
	bottom:6px;		
	animation-delay:1.1s;		
		-o-animation-delay:1.1s;		
		-ms-animation-delay:1.1s;		
		-webkit-animation-delay:1.1s;		
		-moz-animation-delay:1.1s;		
}		
#circularG_7{		
	left:23px;		
	bottom:0;		
	animation-delay:1.24s;		
		-o-animation-delay:1.24s;		
		-ms-animation-delay:1.24s;		
		-webkit-animation-delay:1.24s;		
		-moz-animation-delay:1.24s;		
}		
#circularG_8{		
	left:6px;		
	bottom:6px;		
	animation-delay:1.38s;		
		-o-animation-delay:1.38s;		
		-ms-animation-delay:1.38s;		
		-webkit-animation-delay:1.38s;		
		-moz-animation-delay:1.38s;		
}		
@keyframes bounce_circularG{		
	0%{		
		transform:scale(1);		
	}		
	100%{		
		transform:scale(.3);		
	}		
}		
@-o-keyframes bounce_circularG{		
	0%{		
		-o-transform:scale(1);		
	}		
	100%{		
		-o-transform:scale(.3);		
	}		
}		
@-ms-keyframes bounce_circularG{		
	0%{		
		-ms-transform:scale(1);		
	}		
	100%{		
		-ms-transform:scale(.3);		
	}		
}		
@-webkit-keyframes bounce_circularG{		
	0%{		
		-webkit-transform:scale(1);		
	}		
	100%{		
		-webkit-transform:scale(.3);		
	}		
}		
@-moz-keyframes bounce_circularG{		
	0%{		
		-moz-transform:scale(1);		
	}		
	100%{		
		-moz-transform:scale(.3);		
	}		
}		
</style>

</body>
</html>