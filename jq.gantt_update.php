<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();

$id = $_REQUEST['project_id'];
$days = $_REQUEST['days'];
$new_start_date = $_REQUEST['new_start_date'];

// new start date looks like:
// Fri Feb 17 2017

// reformat to work with strtotime
$nsd = explode(" ",$new_start_date);
// $m = date('m',$nsd[1]);
if($nsd[1]=='Jan')
{
	$m="01";
}
if($nsd[1]=='Feb')
{
	$m="02";
}
if($nsd[1]=='Mar')
{
	$m="03";
}
if($nsd[1]=='Apr')
{
	$m="04";
}
if($nsd[1]=='May')
{
	$m="05";
}
if($nsd[1]=='Jun')
{
	$m="06";
}
if($nsd[1]=='Jul')
{
	$m="07";
}
if($nsd[1]=='Aug')
{
	$m="08";
}
if($nsd[1]=='Sep')
{
	$m="09";
}
if($nsd[1]=='Oct')
{
	$m="10";
}
if($nsd[1]=='Nov')
{
	$m="11";
}
if($nsd[1]=='Dec')
{
	$m="12";
}

$d = $nsd[2];
$y = $nsd[3];
//print 'date: '.$m.'/'.$d.'/'.$y;

$df = $m.'/'.$d.'/'.$y;

// determine the new end date (new_start_date + days)
$ts = strtotime($df.'+'.$days.' days');
// $ts = strtotime($new_start_date);

//print '<br>ts: '.$ts.'<br>'.$df.' '.$days;

$new_install_date=date('m/d/Y',$ts);

$js = $vujade->get_job_status($id);

// update the job status
$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date',$new_install_date);

// update the calendar event
$event = $vujade->event_exists($id,'installation');
if($event['exists']=="1")
{

	// reset the start date and end date
	$s[]=$vujade->update_row('calendar',$event['id'],'start',$new_install_date);
    $s[]=$vujade->update_row('calendar',$event['id'],'end',$new_install_date);
}
?>