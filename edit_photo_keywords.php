<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$photo_id = $_REQUEST['photo_id'];
$photo = $vujade->get_site_photo($photo_id);
if($photo['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$s = array();
$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
# update keywords for this photo
if($action==1)
{
	# delete any keywords for it
	$s[]=$vujade->delete_row('site_photos_keywords',$photo_id,1,'photo_id');

	# create new set of keywords
	$keywords = $_POST['keywords'];
	if($keywords)
	{
		foreach($keywords as $k)
		{
			$vujade->create_row('site_photos_keywords');
			$row_id = $vujade->row_id;
			$s[]=$vujade->update_row('site_photos_keywords',$row_id,'photo_id',$photo_id);
			$s[]=$vujade->update_row('site_photos_keywords',$row_id,'keyword_id',$k);
		}
	}

	# redirect to the site photos overview page
	$vujade->page_redirect('project_site_photos.php?id='.$project_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$nav = 0;
$title = "Site Photo Keywords - ";
?>
<?php
$vujade->show_errors();
$vujade->show_messages();
?><?php require_once('tray_header.php'); ?><!-- Start: Content-Wrapper -->
<section id="content_wrapper"><!-- Begin: Content -->
	<section id="content" class="table-layout animated fadeIn"><!-- begin: .tray-left -->

		<div class="tray tray-center" style = "width:100%;">
			<div class="pl15 pr15" style = "width:100%;">


<div class="panel panel-primary panel-border top">
	<!--heading-->
	<div class="panel-heading">
		<span class="panel-title">Keywords</span>
	</div><!--close heading-->

	<div class="panel-body">
		<table width = "100%">
			<tr>
				<!-- display the photo -->
				<td valign = "top" width = "100%">
					<img style="width: 100%; height: auto" src="thumbnail.php?file=uploads/images/<?php print $project_id."/".$photo['filename']; ?>&width='400'&height='400'" border="0">
				</td>
			</tr>
			<tr>
				<td>
				<form method = "post" action = "edit_photo_keywords.php">
					<input type = "hidden" name = "action" value = "1">
					<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
					<input type = "hidden" name = "photo_id" value = "<?php print $photo_id; ?>">
					<?php
$keywords = $vujade->get_keywords();
					if($keywords['error']=="0")
					{
					unset($keywords['error']);
					$pids = array('a');
					$photo_keywords = $vujade->get_keywords_by_id($photo_id);
					if($photo_keywords)
					{
					//unset($photo_keywords['error']);
					foreach($photo_keywords as $pkw)
					{
					$pids[]=$pkw['id'];
					}
					}
					foreach($keywords as $keyword)
					{
					print '<input type = "checkbox" name = "keywords[]" value = "'.$keyword['database_id'].'"';
					if(in_array($keyword['database_id'], $pids))
					{
					print 'checked = "checked"';
					}
					print '> ';
					print $keyword['keyword'].'<br>';
					}
					}
					?>
					<br>
						<a href="project_site_photos.php?id=<?php echo $project_id; ?>" class = "sbt200 btn btn-primary btn-lg">CANCEL</a>
						<input type = "submit" value = "SAVE" class = "sbt200 btn btn-primary btn-lg pull-right">
				</form>
				</td>
			</tr>
		</table>
	</div>

</div>
			</div>
		</div>
		</section>
</section>
</body>
</html>