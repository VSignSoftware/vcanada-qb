<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['create']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

/*
if($accounting_permissions['delete']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
*/

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$date1 = $_REQUEST['date1'];
	$date1ts = strtotime($date1);
	$date2 = $_REQUEST['date2'];
	$date2ts = strtotime($date2);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Print Time Cards - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

  <!-- Begin: Content -->
  <section id="content" class="table-layout animated fadeIn">

    <!-- begin: .tray-left -->
    <aside class="tray /*tray-left*/ tray250 p20" id = "left_tray">

	    <div id = "menu_2" style = "">
	    	<a class = "glyphicons glyphicons-left_arrow" href = "accounting.php" id = "back" style = "margin-bottom:10px;"></a>
			<br>
			
			<a href = "print_time_cards.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Print Time Cards</a>
	      	<br>

	      	<a href = "enter_time.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Enter Time</a>
	      	<br>

	      	<a href = "payroll_summary.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Payroll Summary</a>
	      	<br>

	      	<a href = "payroll_hourly_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Hourly Report</a>
	      	<br>

	      	<a href = "payroll_labor_sort_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Labor Report</a>
	      	<br>
		</div>

    </aside>
    <!-- end: .tray-left -->

    <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="pl20 pr50">

        	<div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				
	        	<div class="panel-body bg-light">
	        		<table>
	        			<tr>
	        				<td>
	        					<strong>Print Time Cards for Date: </strong>
	        				</td>
	        				<td>&nbsp;</td>
	        				<td>
	        					<input type = "text" class = "dp form-control" style = "width:200px;float:left;" name = "date1" id = "date1" value = "<?php print $date1; ?>"> 
	        				</td>
	        				<td>&nbsp;</td>
	        				<td>
	        					<input type = "submit" id = "go" value = "Print" class = "btn btn-primary">
	        				</td>
	        			</tr>
	        		</table>
				</div>
			</div>
		</div>

        </div>

  </section>
  <!-- End: Content -->

</section>

</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// datepickers
    $(".dp").datepicker();

    $('#go').click(function()
    {
    	var date1 = $('#date1').val();
    	if(date1=="")
    	{
    		alert('Date 1 must be selected.');
    		return false;
    	}
    	// refresh the page
    	var href = "pdf.php?page=time-card&date1="+date1;
    	//window.location.href = href;
    	window.open(href, 'Print Time Cards', '');
  		return false;

    });

});
</script>

</body>
</html>