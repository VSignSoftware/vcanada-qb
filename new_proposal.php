<?php
session_start();
error_reporting(E_ALL & ~E_NOTICE);
define('SITE', 1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'], 'Projects');
if ($projects_permissions['read'] != 1) {
    $vujade->page_redirect('error.php?m=1');
}

$proposals_permissions = $vujade->get_permission($_SESSION['user_id'], 'Proposals');
if ($proposals_permissions['read'] != 1) {
    $vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;

$project = $vujade->get_project($project_id, 2);
if ($project['error'] != 0) 
{
    $vujade->page_redirect('error.php?m=3');
}

$setup = $vujade->get_setup();
$proposals = $vujade->get_proposals_for_project($project_id);
$proposal_id = $proposals['next_id'];

$vujade->create_row('proposals');
$proposal_database_id = $vujade->row_id;
$s[] = $vujade->update_row('proposals', $vujade->row_id, 'project_id', $project_id);
$proposal = $vujade->get_proposal($proposal_database_id);

# tax setup (note this is user editable)
if($setup['is_qb']==1)
{
	// check if has group sales tax
	$td = $vujade->get_tax_groups($project['city'].', '.$project['state']);

	//$vujade->debug_array($td);
	//die;

	if($td['count']==1)
	{
		$tax['rate'] = $td['rate'];
	}
	else
	{
		// does not have group sales tax
		$tax = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
	}
}
else
{
	$tax = $vujade->get_tax_for_city($project['city'], $project['state']);
}

if ($tax['error'] != 0) 
{
    $tax['rate'] = "0";
}
$tax_rate = $tax['rate'];

# first proposal or form has been posted
if($proposal_id == $id . '-01')
{
    $proposal_items = $vujade->get_items_for_proposal($proposal_id);
    $sterms = $vujade->get_terms(1);
    $terms = $sterms['terms'];
    $s[] = $vujade->update_row('proposals', $proposal_database_id, 'terms', $terms);
    $s[] = $vujade->update_row('proposals', $proposal_database_id, 'proposal_id', $proposal_id);
    $s[] = $vujade->update_row('proposals', $proposal_database_id, 'tax_rate', $tax_rate);
    $vujade->page_redirect('edit_proposal.php?project_id='.$project_id.'&proposalid='.$proposal_database_id);
} 
else 
{
    // not first proposal
    $pips = explode('-', $proposal_id);
	$pid = $pips[1];

	//print $pid;

    $firstdigit = $pid[0]; // 0
    $seconddigit = $pid[1]; // 2
    if($firstdigit == 0) 
    {
        $subt = $seconddigit - 1;
        if ($subt <= 0) 
        {
            $subt = 1;
        }
    } 
    else 
    {
        $subt = $pid - 1;
        if ($subt <= 0) {
            $subt = 1;
        }
    }

    if ($subt < 10) 
    {
        $subt = "0" . $subt;
    }

    $first_proposal_id = $pips[0] . '-' . $subt;    

    $s[] = $vujade->update_row('proposals', $proposal_database_id, 'proposal_id', $proposal_id);
	$s[] = $vujade->update_row('proposals', $proposal_database_id, 'tax_rate', $tax_rate);

    // determine which proposal to get the terms from
    // case 1: -02 proposal
    if($subt=='01')
    {
    	$n_proposal_id=$first_proposal_id;
    }
    else
    {
    	// case 2: -03 or higher proposal
    	if($firstdigit==0)
    	{
    		$last=$seconddigit-1;
    		if($last>1)
    		{
    			$n_proposal_id=$pips[0] . '-0' . $last;
    		}
    		else
    		{
    			$n_proposal_id=$pips[0] . '-01';
    		}
    	}
    	else
    	{
    		// last two digits are 10 or above
    		$last=$firstdigit+$seconddigit-1;
    		if($last>9)
    		{
    			$n_proposal_id=$pips[0] . '-' . $last;
    		}
    		else
    		{
    			$n_proposal_id=$pips[0] . '-0'.$last;
    		}
    	}
    }

    // get terms from proposal n
    $n_proposal = $vujade->get_proposal($n_proposal_id,'proposal_id');
    $terms = $n_proposal['terms']; 
    $s[] = $vujade->update_row('proposals', $proposal_database_id, 'terms', $terms);

    //print $n_proposal_id;
    //die;

    // this is actually getting the items from the previous proposal, if any
    $proposal_items = $vujade->get_items_for_proposal($first_proposal_id);
    if($proposal_items['error'] == "0") 
    {
        # write new records for each item and relate it to this proposal
        unset($proposal_items['error']);
        $s = array();
        foreach ($proposal_items as $proposal_item) 
        {
            $vujade->create_row('proposal_items');
            $row_id = $vujade->row_id;
            $s[] = $vujade->update_row('proposal_items', $row_id, 'proposal_id', $proposal_id);
            $s[] = $vujade->update_row('proposal_items', $row_id, 'item', $proposal_item['item']);
            $s[] = $vujade->update_row('proposal_items', $row_id, 'amount', $proposal_item['amount']);
            $s[] = $vujade->update_row('proposal_items', $row_id, 'sort', $proposal_item['sort']);
            $s[] = $vujade->update_row('proposal_items', $row_id, 'tax_label_name', $proposal_item['tax_label_name']);
            $s[] = $vujade->update_row('proposal_items', $row_id, 'tax_label_rate', $proposal_item['tax_label_rate']);
            //$st += $proposal_item['amount'];
        }
    }
    $vujade->page_redirect('edit_proposal.php?project_id='.$project_id.'&proposalid='.$proposal_database_id.'&is_new=1');
}
?>