<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$setup=$vujade->get_setup();
$shop_order = $vujade->get_shop_order($id, 'project_id');
$show_estimates=0; 
$estimates = $vujade->get_estimates_for_project($id);
$estimate_ids = $vujade->get_estimate_ids_for_project($id);

# get selected invoice or get first invoice found
$show_invoices=0;
if(isset($_REQUEST['invoiceid']))
{
	$firstinvoice = $vujade->get_invoice($_REQUEST['invoiceid']);
	$invoiceid = $_REQUEST['invoiceid'];
	$invoices = $vujade->get_invoices_for_project($id);
	unset($invoices['error']);
	unset($invoices['next_id']);
	$show_invoices=1;
}
else
{
	$invoices = $vujade->get_invoices_for_project($id);
	if($invoices['error']=='0')
	{
		unset($invoices['error']);
		//unset($invoices['next_id']);
		$show_invoices=1;
		$invoiceids = array();
		foreach($invoices as $invoice)
		{
			$invoiceids[]=$invoice['database_id'];
		}
		$firstinvoice = $vujade->get_invoice($invoiceids[0]);
		$invoiceid = $invoiceids[0];
	}
	else
	{
		$show_invoices=0;
		$firstinvoice['error']=1;
	}	
}

$show_plus_button=0;
if($shop_order['error']=="0")
{
	$show_plus_button++;
}
if($project['client_id']>0)
{
	$show_plus_button++;
}

$setup2 = $vujade->get_setup(1);
if($setup2['invoice_email_message']=="")
{
	$default_msg = "Please find attached invoice for your review and processing.";
}
else
{
	$default_msg = $setup2['invoice_email_message'];
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 17;
$cmenu = 6;
$section = 3;
$title = "Costing Invoices - ".$project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">

<!-- begin: .tray-left 
<aside class="tray tray-left tray320 p20">
-->
<?php require_once('project_left_tray.php'); ?>

<!-- end: .tray-left </aside>-->

<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;border:0px solid red;">

    <div class="pl15 pr15" style = "width:100%;">

    	<?php require_once('project_right_tray.php'); ?>

    	<!-- page content -->
    	<div class="row">
			<div class="col-md-2" style="width:200px;">
				<div class="panel panel-primary panel-border top">
					<div class="panel-heading">
						<span class="panel-title">Invoices</span>
						<div class="widget-menu pull-right">
							
						</div>
					</div>
					<div class="panel-body">
						<?php
						// current
						if($show_invoices==1)
						{
							print '<table>';
							unset($invoices['error']);

							// current
							foreach($invoices as $invoice)
							{
								if($invoice['is_legacy']==0)
								{
									if($invoiceid==$invoice['database_id'])
									{
										$bgcolor = "cecece";
									}
									else
									{
										$bgcolor = "ffffff";
									}
									print '<tr bgcolor = "'.$bgcolor.'">';
									$color = "#f7584c";

									print '<td valign = "top">';
									print '<a class = "linknostyle" href = "costing_invoices.php?id='.$id.'&invoiceid='.$invoice['database_id'].'">';
									print '<font color = "'.$color.'">'.$invoice['date'].'</font>';
									print '</a>';
									print '</td>';
									print '<td valign = "top">&nbsp;</td>';
									
									print '<td valign = "top">';
									print '<a class = "linknostyle" href = "costing_invoices.php?id='.$id.'&invoiceid='.$invoice['database_id'].'">';
									print $invoice['invoice_id'];
									print '</a>';
									print '</td>';
									print '</tr>';
								}
							}

							// legacy
							print '<tr><td colspan="3">&nbsp;</td></tr>';
							print '<tr><td colspan="3"><strong>Legacy Invoices</strong></td></tr>';
							foreach($invoices as $invoice)
							{
								if($invoice['is_legacy']==1)
								{
									if($invoiceid==$invoice['database_id'])
									{
										$bgcolor = "cecece";
									}
									else
									{
										$bgcolor = "ffffff";
									}
									print '<tr bgcolor = "'.$bgcolor.'">';
									$color = "#f7584c";

									print '<td valign = "top">';
									print '<a class = "linknostyle" href = "costing_invoices.php?id='.$id.'&invoiceid='.$invoice['database_id'].'">';
									print '<font color = "'.$color.'">'.$invoice['date'].'</font>';
									print '</a>';
									print '</td>';
									print '<td valign = "top">&nbsp;</td>';
									
									print '<td valign = "top">';
									print '<a class = "linknostyle" href = "costing_invoices.php?id='.$id.'&invoiceid='.$invoice['database_id'].'">';
									print $invoice['invoice_id'];
									print '</a>';
									print '</td>';
									print '</tr>';
								}
							}

							print '</table>';
						}

						if($show_plus_button!=2)
						{
							print '<div class = "alert alert-danger"><em>Shop Order page is incomplete or no customer has been selected for this project. Please complete the shop order page and select a customer before creating a new invoice.</em></div>';
						}
						?>
					</div>
				</div>
			</div>

<div id = "" class="col-md-9" style="margin-left:5px;border:0px solid red;padding:0px;">
<?php
if( ($show_invoices==1) && ($firstinvoice['error']=='0') )
{
?>
	<div class="panel panel-primary panel-border top">
		<div class="panel-heading">
			<span class="panel-title">Invoice #<?php print $firstinvoice['invoice_id']; ?></span>
			<div class="widget-menu pull-right">

				<?php
				// not legacy
				if($firstinvoice['is_legacy']==0)
				{
					if($firstinvoice['costing']==1)
					{
						?>
						<input type = "checkbox" id = "costing" disabled checked = "checked">
						<?php
					}
					else
					{
						?>
						<input type = "checkbox" id = "costing" disabled>
						<?php
					}
					?>
					<strong><font color = "#4A89DC">Included in costing</font></strong> 

					<a href = "#email-invoice-form" class = "btn btn-primary btn-sm" id = "email-invoice" style = "">Email</a>

					<!-- email modal content -->
					<div id = "email-invoice-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:450px;">
						<!-- form -->
						<form id = "email-form">
							<input type = "hidden" name = "invoice_id" id = "invoice_id" value = "<?php print $firstinvoice['database_id']; ?>">
							<table class = "table">
								<tr class = "project_search_results">
									<td class = "project_search_results">Send To</td>
									<td class = "project_search_results">
										<input type = "text" name = "send_to" id = "send_to" class = "form-control" value = "<?php print $project['project_contact_email']; ?>">
									<div id = "error_1" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">Subject</td>
									<td class = "project_search_results">
										<input type = "text" name = "subject" id = "subject" value = "<?php print $firstinvoice['project_id']; ?> Invoice" class = "form-control">
										<div id = "error_2" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td valign = "top">Message</td>
									<td class = "project_search_results">
										<textarea name = "msg" id = "msg" class = "form-control" style = "height:200px;">
											<?php print $default_msg; ?>
										</textarea><div id = "error_3" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">&nbsp;</td>
									<td class = "project_search_results">
										<div id = "working"></div>
										<a href = "#" id = "send-email" class = "btn btn-success btn-lg">Send</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
									</td>
								</tr>
							</table>
						</form>
					</div>

					<a class = "btn btn-sm btn-primary" href = "print_invoice.php?id=<?php print $id; ?>&invoiceid=<?php print $invoiceid; ?>" target = "_blank" title = "Print Invoice">Print</a> 
				<?php 
				} 
				else
				{
					// legacy
					// has email, print and delete buttons
					?>
					<a href = "#email-invoice-form" class = "btn btn-primary btn-sm" id = "email-invoice" style = "">Email</a>

					<!-- email modal content -->
					<div id = "email-invoice-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:450px;">
						<!-- form -->
						<form id = "email-form">
							<input type = "hidden" name = "invoice_id" id = "invoice_id" value = "<?php print $firstinvoice['database_id']; ?>">
							<table class = "table">
								<tr class = "project_search_results">
									<td class = "project_search_results">Send To</td>
									<td class = "project_search_results">
										<input type = "text" name = "send_to" id = "send_to" class = "form-control" value = "<?php print $project['project_contact_email']; ?>">
									<div id = "error_1" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">Subject</td>
									<td class = "project_search_results">
										<input type = "text" name = "subject" id = "subject" value = "<?php print $firstinvoice['project_id']; ?> Invoice" class = "form-control">
										<div id = "error_2" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td valign = "top">Message</td>
									<td class = "project_search_results">
										<textarea name = "msg" id = "msg" class = "form-control" style = "height:200px;">
											<?php print $default_msg; ?>
										</textarea><div id = "error_3" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">&nbsp;</td>
									<td class = "project_search_results">
										<div id = "working"></div>
										<a href = "#" id = "send-email" class = "btn btn-success btn-lg">Send</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
									</td>
								</tr>
							</table>
						</form>
					</div>

					<a class = "btn btn-sm btn-primary" href = "print_invoice.php?id=<?php print $id; ?>&invoiceid=<?php print $invoiceid; ?>" target = "_blank" title = "Print Invoice">Print</a> 

				<?php  
				}
				?>	
			</div>
		</div>

<!-- invoice body content -->
<div class="panel-body">
	<div class="row">
		<div class="col-md-12 well">
			<strong>Description</strong><br>
			<?php
			// not legacy
			if($firstinvoice['is_legacy']==0)
			{
				// line items
				$lines = $vujade->get_invoice_line_items($firstinvoice['database_id'],'',false,2);
				if($lines['count']>0)
				{
					unset($lines['count']);
					unset($lines['error']);
					print '<table width = "100%">';
					foreach($lines as $line)
					{
						print '<tr>';
						print '<td width = "100%" style = "word-break: break-all;">';
						//print $line['line_description'];
						$line['line_description'] = str_replace("&nbsp;", '&nbsp; ', $line['line_description']);
						print $line['line_description'];
						print '</td>';
						//print '<td>';
						//print '$'.@number_format($line['line_amount'],2);
						//print '</td>';
						print '</tr>';
					}
					print '</table>';
				}
			}
			else
			{
				print $firstinvoice['memo'];
			}
			?>
		</div>
	</div>

	<div class="row">
		<div class="col-md-12 well">
			<?php
			$invoice=$vujade->get_invoice($firstinvoice['database_id']);
			if($invoice['error']=="0")
			{
				// not legacy
				if($invoice['is_legacy']==0)
				{
					$advanced_deposit=$invoice['advanced_deposit'];
					$date=$invoice['date'];
					$selling_price=$invoice['v_sales_price'];
					$tax_rate=$invoice['v_tax_rate'];
					$tax_amount=$invoice['v_tax_amount'];
					$engineering=$invoice['v_engineering'];
					$permits=$invoice['v_permits'];
					$deposit=$invoice['v_deposit'];
					$permit_acquisition=$invoice['v_permit_acquisition'];
					$retention=$invoice['v_retention'];
					$shipping=$invoice['v_shipping'];
					$total_due=$invoice['balance_due'];
					$discount=$invoice['v_discount'];
					$cc=$invoice['v_cc_processing'];
					$i4_label=$invoice['i4_label'];
					$i4_amount=$invoice['i4_amount'];

					// sum left hand columns
					$subtotal1=$selling_price+$i4_amount+$engineering+$permits+$permit_acquisition+$shipping+$cc-$discount;

					// sum subtotal - tax - deposit - retention
					$subtotal2 = $subtotal1+$tax_amount;
					$deposit=$invoice['v_deposit'];
					$total_due=$subtotal2-$deposit-$retention;

					if($advanced_deposit==1)
					{
						$total_due=$deposit+$cc;
					}

					$column_2_st_2 = $total_due;

					$total_payments=0; // sum all payments
					$payments = $vujade->get_invoice_payments($invoice['database_id']);

					//print_r($payments);

					if($payments['error']=="0")
					{
						unset($payments['error']);
						foreach($payments as $payment)
						{
							$total_payments+=$payment['amount'];
						}
					}

					// credit memos
					$credit_memos=$vujade->get_credit_memos($invoice['invoice_id']);
					if($credit_memos['count']>0)
					{
						unset($credit_memos['sql']);
						unset($credit_memos['error']);
						unset($credit_memos['count']);
						foreach($credit_memos as $credit_memo)
						{
							$total_payments+=$credit_memo['amount'];
						}
					}

					$balance_due=0; // sum total due - payments
					$balance_due = $total_due-$total_payments;
				}
				else
				{
					// legacy
					$date = $invoice['date'];
					$advanced_deposit = $invoice['advanced_deposit'];
					$description = $invoice['memo'];
					$legacy_items=$vujade->get_invoice_line_items($firstinvoice['database_id'],'ALL');
					$tax_amount=$invoice['sales_tax'];
					$tax_rate=$invoice['tax_rate'];
					if($legacy_items['error']=="0")
					{
						unset($legacy_items['error']);
						foreach($legacy_items as $legacy_item)
						{
							if($legacy_item['item_description']=='Manufacture & Install')
							{
								$mfi=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Permit Acquisition')
							{
								$permit_acquisition=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Retention')
							{
								$retention=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Deposit')
							{
								$deposit=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Discount')
							{
								$discount=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Permits')
							{
								$permits=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Labor only')
							{
								$labor=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='CC Processing Fee')
							{
								$cc=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Shipping')
							{
								$shipping=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Engineering')
							{
								$engineering=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Change Order')
							{
								$co=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Non-Taxable CO')
							{
								$nto=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Parts')
							{
								$parts=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Prepaids')
							{
								$prepaids=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Labor')
							{
								$labor=$legacy_item['line_total'];
							}
							if($legacy_item['item_description']=='Non-Taxable M&I')
							{
								$ntmi=$legacy_item['line_total'];
							}
						}
						$co=$nto+$co;
					}
					else
					{
						// none exist; set to 0
						$tax_amount=0;
						$mfi=0;
						$permit_acquisition=0;
						$retention=0;
						$deposit=0;
						$discount=0;
						$permits=0;
						$labor=0;
						$cc=0;
						$shipping=0;
						$engineering=0;
						$co=0;
						$nto=0;
						$parts=0;
						$prepaids=0;
						$labor=0;
						$ntmi=0;
					}

					// sum left hand columns
					$subtotal1=$mfi+$labor+$parts+$co+$engineering+$permits+$permit_acquisition+$shipping+$cc-$discount;

					// second subtotal
					$subtotal2 = $subtotal1+$tax_amount;

					// advanced deposit
					if($advanced_deposit==1)
					{
						$advd = $subtotal2*$invoice['advanced_deposit_rate'];
						$total_due=$advd;
						$total_due=$advd-$retention;
					}
					else
					{
						$total_due=$subtotal2+$deposit-$retention;
						$column_2_st_2 = $total_due;
					}

					$total_payments=0; // sum all payments
					$payments = $vujade->get_invoice_payments($invoice['database_id']);
					if($payments['error']=="0")
					{
						unset($payments['error']);
						foreach($payments as $payment)
						{
							$total_payments+=$payment['amount'];
						}
					}

					$balance_due=0; // sum total due - payments
					$balance_due = $total_due-$total_payments;
				}
			}
			?>

			<?php
			// not legacy
			if($invoice['is_legacy']==0)
			{
				?>
				<!-- left -->	
				<div class = "row">
					<div class = "col-md-5">
						<table width = "100%" style = "">
							<tr>
								<td>Selling Price: </td>
								<td>$<?php print @number_format($selling_price,2,'.',','); ?></td>
							</tr>

							<!--
							<tr>
								<td><?php //print $i4_label; ?>: </td>
								<td>$<?php //print @number_format($i4_amount,2,'.',','); ?></td>
							</tr>
							-->
							<tr>
							<td>Engineering: </td>
							<td>$<?php print @number_format($engineering,2,'.',','); ?></td>
							</tr>
							<tr>
							<td>Permits: </td>
							<td>$<?php print @number_format($permits,2,'.',','); ?></td>
							</tr>
							<tr>
							<td>Permit Acquisition: </td>
							<td>
							$<?php print @number_format($permit_acquisition,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Shipping: </td>
							<td>
							$<?php print @number_format($shipping,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Credit Card Fees: </td>
							<td>
							$<?php print @number_format($cc,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Discount: </td>
							<td>
							- $<?php print @number_format($discount,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Subtotal: </td>
							<td>
							$<?php print @number_format($subtotal1,2,'.',','); ?>
							</td>
							</tr>
						</table>
					</div>

					<!-- middle -->
					<div class = "col-md-2">
						<table width = "100%" style = "">
						<tr>
						<td>Tax %:</td>
						<td><?php print $tax_rate; ?></td>
						</tr>
						</table>
					</div>

					<!-- right -->
					<div class = "col-md-5">
						<table width = "100%" style = "">
						<tr>
						<td>Tax: </td>
						<td>
							$<?php print @number_format($tax_amount,2,'.',','); ?>
						</td>
						</tr>

						<tr>
							<td>
								Subtotal: 
							</td>
							<td>
								$<?php print @number_format($subtotal2,2,'.',','); ?>
							</td>
							</tr>

						<tr>
							<td>Deposit: </td>
							<td>
								<?php
								if($firstinvoice['advanced_deposit']==1)
								{
									print '$'.@number_format($deposit,2,'.',',');
								}
								else
								{
									print '- $'.@number_format($deposit,2,'.',',');
								}
								?>
							</td>
							</tr>

						<tr>
							<td>Retention: </td>
							<td>
								- $<?php print @number_format($retention,2,'.',','); ?>
							</td>
							</tr>

						<tr>
							<td>Total: </td>
							<td>
								$<?php print @number_format($column_2_st_2,2,'.',','); ?>
							</td>
						</tr>

						<tr>
							<td>Payments: </td>
							<td>
								<?php
								print '- $'.@number_format($total_payments,2,'.',',');
								?>
							</td>
						</tr>

						<tr>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td>Balance Due: </td>
							<td>
								$<?php print @number_format($balance_due,2,'.',','); ?>
							</td>
						</tr>
						</table>
					</div>
				</div>

			<?php 
			} 
			else
			{
				// legacy
				?>
				<!-- left -->	
				<div class = "row">
					<div class = "col-md-5">
						<table width = "100%" style = "">
							<tr>
								<td>Manufacture &amp; Install: </td>
								<td>$<?php print @number_format($mfi,2,'.',','); ?></td>
							</tr>
							<tr>
								<td>Parts: </td>
								<td>$<?php print @number_format($parts,2,'.',','); ?></td>
							</tr>
							<tr>
								<td>Labor: </td>
								<td>$<?php print @number_format($labor,2,'.',','); ?></td>
							</tr>

							<tr>
							<td>Engineering: </td>
							<td>$<?php print @number_format($engineering,2,'.',','); ?></td>
							</tr>
							<tr>
							<td>Permits: </td>
							<td>$<?php print @number_format($permits,2,'.',','); ?></td>
							</tr>
							<tr>
							<td>Permit Acquisition: </td>
							<td>
							$<?php print @number_format($permit_acquisition,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Change Order: </td>
							<td>$<?php print @number_format($co,2,'.',','); ?></td>
							</tr>

							<tr>
							<td>Shipping: </td>
							<td>
							$<?php print @number_format($shipping,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Credit Card Fees: </td>
							<td>
							$<?php print @number_format($cc,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Discount: </td>
							<td>
							- $<?php print @number_format($discount,2,'.',','); ?>
							</td>
							</tr>

							<tr>
							<td>Subtotal: </td>
							<td>
							$<?php print @number_format($subtotal1,2,'.',','); ?>
							</td>
							</tr>
						</table>
					</div>

					<!-- middle -->
					<div class = "col-md-2">
						<table width = "100%" style = "">
						<tr>
						<td>Tax %:</td>
						<td><?php print $tax_rate; ?></td>
						</tr>
						</table>
					</div>

					<!-- right -->
					<div class = "col-md-5">
						<table width = "100%" style = "">
						<tr>
						<td>Tax: </td>
						<td>
							$<?php print @number_format($tax_amount,2,'.',','); ?>
						</td>
						</tr>

						<tr>
							<td>
								Subtotal: 
							</td>
							<td>
								$<?php print @number_format($subtotal2,2,'.',','); ?>
							</td>
							</tr>

						<tr>
							<td>Deposit: </td>
							<td>
								<?php
								if($firstinvoice['advanced_deposit']==1)
								{
									print '$'.@number_format($deposit,2,'.',',');
								}
								else
								{
									$deposit = str_replace('-','',$deposit);
									print '- $'.@number_format($deposit,2,'.',',');
								}
								?>
							</td>
							</tr>

						<tr>
							<td>Retention: </td>
							<td>
								- $<?php print @number_format($retention,2,'.',','); ?>
							</td>
							</tr>

						<tr>
							<td>Total: </td>
							<td>
								$<?php print @number_format($column_2_st_2,2,'.',','); ?>
							</td>
						</tr>

						<tr>
							<td>Payments: </td>
							<td>
								<?php
								print '- $'.@number_format($total_payments,2,'.',',');
								?>
							</td>
						</tr>

						<tr>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td>Balance Due: </td>
							<td>
								$<?php print @number_format($balance_due,2,'.',','); ?>
							</td>
						</tr>
						</table>
					</div>
				</div>
				<?php
			}
			?>

			<p style = "clear:both;text-align:right;margin-right:30px;">
			<?php
			if($firstinvoice['advanced_deposit']==1)
			{
			?>
				<em>NOTE: This is an advanced deposit invoice.</em>
			<?php
			}
			?>
			</p>
										</div>
									</div>

								</div>

							</div>

						<?php } ?>
					</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // send email modal
    $('#email-invoice').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#email-invoice-form',
		modal: true
	});

    // send email processing
    var msg = $('#msg').html();
    $("#msg").html($.trim(msg));

    $('#send-email').click(function(e)
    {
    	e.preventDefault();
	
    	var id = $('#invoice_id').val();

		// send to, subject and message must not be blank
		var send_to = $('#send_to').val();
		var subject = $('#subject').val();
		var message = $('#msg').val();
		var error = 0;
		if(send_to=="")
		{	
			$('#error_1').show();
			error++;
		}
		else
		{
			$('#error_1').hide();
		}
		if(subject=="")
		{
			$('#error_2').show();
			error++;
		}
		else
		{
			$('#error_2').hide();
		}
		if(message=="")
		{
			$('#error_3').show();
			error++;
		}
		else
		{
			$('#error_3').hide();
		}

		if(error==0)
		{
			$('#working').css('padding','3px');
			$('#working').css('background-color','#E6A728');
			$('#working').css('color','white');
			$('#working').css('font-weight','bold');
			$('#working').css('font-size','16px');
			$('#working').html('Working...');
			$.post( "jq.send.php", { id: id, type: "invoice", send_to: send_to, subject: subject, message: message })
			.done(function( response ) 
			{
			    $('#working').css('background-color','#36D86C');
			    $('#working').html('');
			    $('#working').html(response);

			    // close the modal automatically after x seconds
			    setTimeout(function()
			    {
				  $.magnificPopup.close()
				}, 2000);

			});
		}
	});

	// dismiss modals
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
