<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$shop_order = $vujade->get_shop_order($project_id,"project_id");
$proposal = $vujade->get_proposal($shop_order['proposal'],'proposal_id');
$setup = $vujade->get_setup();
$invoice_setup = $vujade->get_invoice_setup();

// resale list id
$resale_list_info = $vujade->get_resale_info();
$resale_list_id = $resale_list_info['list_id'];

// array to hold mysql crud response
$s=array();

# create a new invoice
$new=$_REQUEST['new'];
if($new==1)
{
	# determine what the invoice number is
	$invoices = $vujade->get_invoices_for_project($project_id);
	if($invoices['error']!='0')
	{
		$next_id=$project_id."-1";
	}
	else
	{
		$next_id=$project_id."-".$invoices['next_id'];
	}
	$vujade->create_row('quickbooks_invoice');
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'project_id',$project_id);
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'RefNumber',$next_id);
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'TxnDate',date('Y-m-d'),'ID');
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_time_created',strtotime('now'),'ID');
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'costing',1,'ID');

	# get correct customer info for this project
	$listiderror=0;
	$localiderror=0;
	// try to get by list id
    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
    if($customer1['error']!="0")
    {
    	$listiderror++;
    	unset($customer1);
    }
    else
    {
    	$customer=$customer1;
    }
    // try to get by local id
    $customer2 = $vujade->get_customer($project['client_id'],'ID');
    if($customer2['error']!="0")
    {
    	$localiderror++;
    	unset($customer2);
    }
    else
    {
    	$customer=$customer2;
    }

    $iderror=$listiderror+$localiderror;

    if($iderror<2)
    {
    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
    }
    else
    {
    	// can't find customer or no customer on file
    }

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'CustomerRef_ListID',$customer['database_id']);

	// get and set the sales tax code 
	// qb tax state is the same as shipping address
	if($project['state']==$setup['qb_tax_state'])
	{
		//$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);

		// check if has group sales tax
		$td = $vujade->get_tax_groups($project['city'].', '.$project['state']);

		//$vujade->debug_array($td);
		//die;

		if($td['count']==1)
		{
			//$tax['rate'] = $td['rate'];
			// $tax_data
			$tax_data['rate']=$td['rate'];
			$tax_data['error']=0;
			$tax_data['ListID']=$td['list_id'];
		}
		else
		{
			// does not have group sales tax
			$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
		}
		
	}
	else
	{
		// not the same
		$tax_data = $vujade->QB_get_sales_tax('Out of State');
	}
	if($tax_data['error']=="0")
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data['ListID']);
		$tax_rate=$tax_data['rate']; 
	}
	else
	{
		$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data2['ListID']);
		$tax_rate=$tax_data2['rate'];
	}

	if($setup['country']=="Canada")
	{
		$tax_rate=1;
	}

	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'TxnID',$fakeid);

	// shipping address
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_Addr1',$project['site']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_Addr2',$project['address_1']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_City',$project['city']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_State',$project['state']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_PostalCode',$project['zip']);

	// other updates
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_tax_rate',$tax_rate);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'i4_label',$invoice_setup['label_4']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'i4_rate',$invoice_setup['sale_price_4']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'po_number',$shop_order['po_number']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'is_taxable_5',$_POST['is_taxable_5']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'is_taxable_6',$_POST['is_taxable_6']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'is_taxable_7',$_POST['is_taxable_7']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'is_taxable_8',$_POST['is_taxable_8']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'is_taxable_9',$_POST['is_taxable_9']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'is_taxable_10',$_POST['is_taxable_10']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_rate_5',$invoice_setup['sale_price_5']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_rate_6',$invoice_setup['sale_price_6']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_rate_7',$invoice_setup['sale_price_7']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_rate_8',$invoice_setup['sale_price_8']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_rate_9',$invoice_setup['sale_price_9']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_rate_10',$invoice_setup['sale_price_10']);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_amount_5',0);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_amount_6',0);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_amount_7',0);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_amount_8',0);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_amount_9',0);
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'taxable_amount_10',0);

	// payments for any previous ad invoices
	// if this is the first regular invoice, this code applies, 
	// otherwise it is skipped
	// all payment amounts - all credit card fees
	// calculate deposit
	$deposit=0;
	$cc_fees=0;
	$existing_invoices = $vujade->get_invoices_for_project($project_id);

	//$vujade->print_r_array($existing_invoices);
	//die;
	//print '<hr>';
	$skip=0;

	if($existing_invoices['error']=="0")
	{
		unset($existing_invoices['error']);
		unset($existing_invoices['next_id']);
		foreach($existing_invoices as $existing_i)
		{
			if( (!empty($existing_i['database_id'])) && ($existing_i['database_id']!=$vujade->row_id) )
			{
				//print $existing_i['invoice_id'].'<br>';
				if($existing_i['advanced_deposit']!=1)
				{
					//print 'is not advanced deposit...<hr>';
					// means there are more than one existing regular invoice
					$skip++;
				}
			}
		}

		if($skip==0)
		{
			foreach($existing_invoices as $existing_i)
			{
				$cc_fees+=$existing_i['v_cc_processing'];

				if(!empty($existing_i['database_id']))
				{
					$payments = $vujade->get_invoice_payments($existing_i['database_id']);
					if($payments['error']=="0")
					{
						unset($payments['error']);
						foreach($payments as $payment)
						{
							$deposit+=$payment['amount'];
						}
					}
					unset($payments);
				}
			}
		}
	}
	
	$deposit=$deposit-$cc_fees;

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_deposit',$deposit);

	// brad block: queue up new invoice
	$ID = $vujade->row_id;
	$vujade->create_new_QB_invoice_lineitem($ID, "Manufacture & Install", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Parts", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Labor only", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Engineering", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Permits", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Permit Acquisition", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Shipping", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "CC Processing Fee", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Discount", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Deposit", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($ID, "Retention", 0.00, 1,"");
	
	$vujade->queue_add($ID);
	
	//end brad block: this section is complete.
	
	$invoice_db_id=$vujade->row_id;

	//die;
}
else
{
	// get invoice details (invoice is not new; user pressed edit or any button on the invoice page except the save button)
	$invoice_db_id = $_REQUEST['invoice_db_id'];
	$invoice=$vujade->get_invoice($invoice_db_id);
	if($invoice['error']!="0")
	{
		$vujade->page_redirect('error.php?m=3');
	}
}

if($setup['country']=="Canada")
{
	$invoice['tax_rate']=1;
	$invoice['v_tax_rate']=1;
}

// action routing
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}
else
{
	$action=0;
}

// add new / update existing line items
if($action==1)
{

	$invoice=$vujade->get_invoice($invoice_db_id);

	// add the line items
    $skipped = array('action','project_id','proposal_id','proposalid','proposal_database_id','expiration_date','date','design','send_to_name','send_to_phone','description','terms','update_amount','update_description','item_id','amount','tax_rate','tax_total','total','deposit_rate','tax_type','is_taxable_5','is_taxable_6','is_taxable_7','is_taxable_8','is_taxable_9','is_taxable_10','invoice_db_id');
    foreach($_POST as $key => $value)
    {
        if(!in_array($key, $skipped))
        {
            $test_key = explode('-',$key);
            $row_id=$test_key[1];
            if($test_key[0]=="desc")
            {
                $description=$value;
                $description=str_replace("\r"," \r",$description);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$description);
            }
            if($test_key[0]=="amount")
            {
                $amount=str_replace(",", "", $value);
                $amount=str_replace("$", "", $amount);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$amount);
            }
            if($test_key[0]=="sort")
            {
                $sort=str_replace(",", "", $value);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$sort);
            }
            if($test_key[0]=="tax")
            {
                $value = explode('^',$value);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$value[0]);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$value[1]);
            }
        }
    }

    // new line item one
    $description=$_POST['description'];
    $amount=str_replace(',', '',$_POST['amount']);
    $amount=str_replace("$", "", $amount);
    $sort=str_replace(',', '',$_POST['sort']);
    if(!empty($description))
    {
        $vujade->create_row('invoice_line_items');
        $row_id = $vujade->row_id;
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'invoice_id',$invoice_db_id);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$description);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$amount);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$sort);

        $tax_type = explode('^',$_POST['tax_type']);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$tax_type[0]);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$tax_type[1]);
    }

    // line items
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'','',2);

	$li_total=0;
    $li_subtotal= 0;
    $li_tax_total=0;
    $li_tax_line=0;
    if($lines['error'] == "0") 
    {
        unset($lines['error']);
        unset($lines['count']);
        $has_lines=1;
        foreach($lines as $item) 
        {
            $li_subtotal += $item['line_amount'];
            if(!empty($item['tax_rate']))
         	{
         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
         	}
         	$li_tax_total+=$li_tax_line;
         	$li_tax_line=0;         
        }
        $li_total=$li_subtotal+$li_tax_total;
    }

    $sales_price=$li_subtotal;
    $tax_rate=$invoice['v_tax_rate'];

    // get tax amount for engineering, permits, permit acq, shipping, credit card fees, discount
    $engineering=str_replace(',', '', $_POST['engineering']);
	$permits=str_replace(',', '', $_POST['permits']);
	$permit_acquisition=str_replace(',', '', $_POST['permit_acquisition']);
	$shipping=str_replace(',', '', $_POST['shipping']);
	$retention=str_replace(',', '', $_POST['retention']);
	$retention=str_replace('-', '', $retention);
	$discount=str_replace(',', '', $_POST['discount']);
	$discount=str_replace('-','',$discount);
	$cc = str_replace(',', '', $_POST['cc_processing_fees']);

	$engineering=str_replace('$', '', $engineering);
	$permits=str_replace('$', '', $permits);
	$permit_acquisition=str_replace('$', '', $permit_acquisition);
	$shipping=str_replace('$', '', $shipping);
	$retention=str_replace('$', '', $retention);
	$discount=str_replace('$','',$discount);
	$cc = str_replace('$', '', $cc);

    // is taxable
    $is_taxable_5=$_POST['is_taxable_5'];
    $is_taxable_6=$_POST['is_taxable_6'];
    $is_taxable_7=$_POST['is_taxable_7'];
    $is_taxable_8=$_POST['is_taxable_8'];
    $is_taxable_9=$_POST['is_taxable_9'];
    $is_taxable_10=$_POST['is_taxable_10'];

    $taxable_amount_5=0;
    $taxable_amount_6=0;
    $taxable_amount_7=0;
    $taxable_amount_8=0;
    $taxable_amount_9=0;
    $taxable_amount_10=0;
    $taxable_amount_total=0;

    if($is_taxable_5==1)
    {
    	$taxable_amount_5=$engineering*$invoice['taxable_rate_5']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_5;
    }
    if($is_taxable_6==1)
    {
    	$taxable_amount_6=$permits*$invoice['taxable_rate_6']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_6;
    }
    if($is_taxable_7==1)
    {
    	$taxable_amount_7=$permit_acquisition*$invoice['taxable_rate_7']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_7;
    }
    if($is_taxable_8==1)
    {
    	$taxable_amount_8=$shipping*$invoice['taxable_rate_8']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_8;
    }
    if($is_taxable_9==1)
    {
    	$taxable_amount_9=$cc*$invoice['taxable_rate_9']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_9;
    }
    if($is_taxable_10==1)
    {
    	$taxable_amount_10=$discount*$invoice['taxable_rate_10']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_10;
    }

    $tax_amount=$taxable_amount_total+$li_tax_total;

	$po_id = $_POST['po_id'];
	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// resale
	$is_resale=$_POST['is_resale'];
	if($is_resale==1)
	{
		$tax_amount=0;
	}

	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'TxnDate',$qbdate);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'po_number',$po_id);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_sales_price',$sales_price);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_amount',$tax_amount);

	if($is_resale==1)
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'ItemSalesTaxRef_ListID',$resale_list_id);
	}

	// brad block: update 
	//if this code causes problems, I have additional tools to help resolve this issue.
	//it's possible this block doesn't need to exist.
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 1" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);

	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Manufacture & Install", $arr['Manufacture'], 1, $arr['ManuDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Parts", $arr['Parts'], 1, $arr['PartsDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Labor only", $arr['Labor'], 1, $arr['LaborDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Engineering", $engineering, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Permits", $permits, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Permit Acquisition", $permit_acquisition, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Shipping", $shipping, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Discount", (-1) * $discount, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", (-1) * $deposit, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Retention", (-1) * $retention, 1,"");
	
	$vujade->update_taxes($invoice_db_id);
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete.
	
    $action=0;
}

// delete line item
if($action==2)
{
    $s = array();
    $row_id = $_POST['item_id'];
    $s[]=$vujade->delete_row('invoice_line_items',$row_id);

    // brad block: update based on new deletion.
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 2" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);

	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Manufacture & Install", $arr['Manufacture'], 1, $arr['ManuDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Parts", $arr['Parts'], 1, $arr['PartsDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Labor only", $arr['Labor'], 1, $arr['LaborDesc']);
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete

    $action=0;
}

// add new line item between existing
if($action==3)
{
	$position=$_REQUEST['position'];
	$after_id = $_REQUEST['after_id'];
	$vujade->update_invoice_items_sort($after_id,$invoice_db_id,$position);
	
	// brad block: update based on new sort.
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 3" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);

	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Manufacture & Install", $arr['Manufacture'], 1, $arr['ManuDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Parts", $arr['Parts'], 1, $arr['PartsDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Labor only", $arr['Labor'], 1, $arr['LaborDesc']);
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete
	
	$action=0;
}

// update totals button or save button was pressed
if( ($action==4) || ($action==5) )
{

    $date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	$sales_price=str_replace(',', '', $_POST['sales_price']);
	$i4_amount=str_replace(',', '', $_POST['i4_amount']);
	$engineering=str_replace(',', '', $_POST['engineering']);
	$permits=str_replace(',', '', $_POST['permits']);
	$permit_acquisition=str_replace(',', '', $_POST['permit_acquisition']);
	$shipping=str_replace(',', '', $_POST['shipping']);
	$retention=str_replace(',', '', $_POST['retention']);
	$retention=str_replace('-', '', $retention);
	$discount=str_replace(',', '', $_POST['discount']);
	$discount=str_replace('-','',$discount);
	$deposit=$_POST['deposit'];
	$deposit=str_replace(',', '', $deposit);
	$deposit = str_replace('-','',$deposit);
	$cc = str_replace(',', '', $_POST['cc_processing_fees']);
	$po_id = $_POST['po_id'];
	$tax_rate=$_POST['tax_rate'];
    $tax_amount=$_POST['tax_amount'];
    $custom_tax_amount=$_POST['custom_tax_amount'];
    
    $sales_price=str_replace('$', '', $sales_price);
	$i4_amount=str_replace('$', '', $i4_amount);
	$engineering=str_replace('$', '', $engineering);
	$permits=str_replace('$', '', $permits);
	$permit_acquisition=str_replace('$', '', $permit_acquisition);
	$shipping=str_replace('$', '', $shipping);
	$retention=str_replace('$', '', $retention);
	$discount=str_replace('$','',$discount);
	$deposit = str_replace('$','',$deposit);
	$cc = str_replace('$', '', $cc);
	$tax_rate=str_replace('$', '', $tax_rate);
	$tax_rate=str_replace(',', '', $tax_rate);
    $tax_amount=str_replace('$', '', $tax_amount);
    $tax_amount=str_replace(',', '', $tax_amount);
    $custom_tax_amount=str_replace('$', '', $custom_tax_amount);
    $custom_tax_amount=str_replace(',', '', $custom_tax_amount);

    // user entered valid decimal
    if( ($tax_rate>0) && ($tax_rate<1) )
    {
    	//print 'tax rate > 0 and tax rate < 1<br>';

    	# update the invoice
    	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_rate',$tax_rate);
    }

    // user entered 0
    if( ($tax_rate=="0") || (empty($tax_rate)) )
    {
    	//print 'tax rate empty <br>';
    	# update the invoice
    	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_rate','0');
    }

    $invoice=$vujade->get_invoice($invoice_db_id);

    // is taxable
    $is_taxable_5=$_POST['is_taxable_5'];
    $is_taxable_6=$_POST['is_taxable_6'];
    $is_taxable_7=$_POST['is_taxable_7'];
    $is_taxable_8=$_POST['is_taxable_8'];
    $is_taxable_9=$_POST['is_taxable_9'];
    $is_taxable_10=$_POST['is_taxable_10'];

    $taxable_amount_5=0;
    $taxable_amount_6=0;
    $taxable_amount_7=0;
    $taxable_amount_8=0;
    $taxable_amount_9=0;
    $taxable_amount_10=0;
    $taxable_amount_total=0;

    if($is_taxable_5==1)
    {
    	$taxable_amount_5=$engineering*$invoice['taxable_rate_5']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_5;
    }
    if($is_taxable_6==1)
    {
    	$taxable_amount_6=$permits*$invoice['taxable_rate_6']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_6;
    }
    if($is_taxable_7==1)
    {
    	$taxable_amount_7=$permit_acquisition*$invoice['taxable_rate_7']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_7;
    }
    if($is_taxable_8==1)
    {
    	$taxable_amount_8=$shipping*$invoice['taxable_rate_8']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_8;
    }
    if($is_taxable_9==1)
    {
    	$taxable_amount_9=$cc*$invoice['taxable_rate_9']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_9;
    }
    if($is_taxable_10==1)
    {
    	$taxable_amount_10=$discount*$invoice['taxable_rate_10']*$tax_rate;
    	$taxable_amount_total+=$taxable_amount_10;
    }

    $subtotal1=$sales_price+$i4_amount+$engineering+$permits+$permit_acquisition+$shipping+$cc-$discount;

    // tax on line items at top of page
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'','',2);

	$li_total=0;
    $li_subtotal= 0;
    $li_tax_total=0;
    $li_tax_line=0;
    if($lines['error'] == "0") 
    {
        unset($lines['error']);
        unset($lines['count']);
        $has_lines=1;
        foreach($lines as $item) 
        {
            $li_subtotal += $item['line_amount'];
            if(!empty($item['tax_rate']))
         	{
         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
         	}
         	$li_tax_total+=$li_tax_line;
         	$li_tax_line=0;         
        }
        $li_total=$li_subtotal+$li_tax_total;
    }

    if($custom_tax_amount==1)
	{
		$tax_amount=str_replace(',', '', $_POST['tax_amount']);
	}
	else
	{
		// automatically calculate it as the sum of the tax on the line items
		$tax_amount=$taxable_amount_total+$li_tax_total;
	}

	// resale
	$is_resale=$_POST['is_resale'];
	if($is_resale==1)
	{
		$tax_amount=0;
	}

	# update the invoice
    $s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_amount',$tax_amount);
    $s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_resale',$is_resale);

    if($is_resale==1)
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'ItemSalesTaxRef_ListID',$resale_list_id);
	}

    /*
    // user entered a tax amount
    if($tax_amount>0)
    {
    	//print 'tax amount > 0 <br>';
    	# update the invoice
    	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_amount',$tax_amount);
    }
    else
    {
    	# update the invoice

    	// update the tax amount
    	$tax_amount=$taxable_amount_total+$li_tax_total;

    	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_amount',$tax_amount);
    }

    //print 'tax rate: '.$tax_rate.'<br>';
    //print 'tax amount: '.$tax_amount.'<br>';
    //die;
	*/

    $subtotal2=$subtotal1+$tax_amount;

    $total_due=$subtotal2-$deposit-$retention;

	// payments
	$total_payments=0; // sum all payments
	$payments = $vujade->get_invoice_payments($invoice['invoice_id']);
	if($payments['error']=="0")
	{
		unset($payments['error']);
		foreach($payments as $payment)
		{
			$total_payments+=$payment['amount'];
		}
	}

	$balance_due=0; // sum total due - payments
	$balance_due = $total_due-$total_payments;

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'TxnDate',$qbdate);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_deposit',$deposit);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_discount',$discount);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_balance_due',$balance_due);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_engineering',$engineering);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_permit_acquisition',$permit_acquisition);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_permits',$permits);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_sales_price',$sales_price);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_shipping',$shipping);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_retention',$retention);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_cc_processing',$cc);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'po_number',$po_id);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'i4_amount',$i4_amount);

	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_taxable_5',$is_taxable_5);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_taxable_6',$is_taxable_6);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_taxable_7',$is_taxable_7);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_taxable_8',$is_taxable_8);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_taxable_9',$is_taxable_9);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'is_taxable_10',$is_taxable_10);

	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'taxable_amount_5',$taxable_amount_5);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'taxable_amount_6',$taxable_amount_6);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'taxable_amount_7',$taxable_amount_7);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'taxable_amount_8',$taxable_amount_8);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'taxable_amount_9',$taxable_amount_9);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'taxable_amount_10',$taxable_amount_10);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_custom_tax_amount',$custom_tax_amount);

	// brad block: update 
	//if this code causes problems, I have additional tools to help resolve this issue.
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 4" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);

	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Manufacture & Install", $arr['Manufacture'], 1, $arr['ManuDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Parts", $arr['Parts'], 1, $arr['PartsDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Labor only", $arr['Labor'], 1, $arr['LaborDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Engineering", $engineering, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Permits", $permits, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Permit Acquisition", $permit_acquisition, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Shipping", $shipping, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Discount", (-1) * $discount, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", (-1) * $deposit, 1,"");
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Retention", (-1) * $retention, 1,"");
	
	$vujade->update_taxes($invoice_db_id);
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete.
	// save button was pressed
	if($action==5)
	{
		# redirect
		$vujade->page_redirect('project_invoices.php?id='.$project_id.'&invoiceid='.$invoice_db_id);
	}
	
    $action=0;
}

// transfer from proposal
if($action==6)
{
	//print_r($_REQUEST);
	//print '<br>';
	//die;
	$po_id = $_POST['po_id'];
	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'TxnDate',$qbdate);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'po_number',$po_id);

	//print_r($s);
	//die;

	// proposal id
	$proposal=$_REQUEST['proposal'];

	// clear any existing lines
	$s[]=$vujade->delete_row('invoice_line_items',$invoice_db_id,1,'invoice_id'); 

	// get all the items from the proposal and copy them to the invoice line items table
	$items = $vujade->get_items_for_proposal($proposal);
	if($items['error']=="0")
	{
		unset($items['error']);
		foreach($items as $item)
		{
			$vujade->create_row('invoice_line_items');
		    $row_id = $vujade->row_id;
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'invoice_id',$invoice_db_id);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$item['item']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$item['amount']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$item['sort']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$item['tax_label_name']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$item['tax_label_rate']);
		}
	}
	unset($items);

	// reset the sales price, tax, deposit, etc
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'','',2);

	$li_total=0;
    $li_subtotal= 0;
    $li_tax_total=0;
    $li_tax_line=0;
    if($lines['error'] == "0") 
    {
        unset($lines['error']);
        unset($lines['count']);
        $has_lines=1;
        foreach($lines as $item) 
        {
            $li_subtotal += $item['line_amount'];
            if(!empty($item['tax_rate']))
         	{
         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
         	}
         	$li_tax_total+=$li_tax_line;
         	$li_tax_line=0;         
        }
        $li_total=$li_subtotal+$li_tax_total;
    }

    $sales_price=$li_subtotal;
    $tax_amount=$li_tax_total;

	$invoice=$vujade->get_invoice($invoice_db_id);

	$t5=$invoice['taxable_amount_5'];
	$t6=$invoice['taxable_amount_6'];
	$t7=$invoice['taxable_amount_7'];
	$t8=$invoice['taxable_amount_8'];
	$t9=$invoice['taxable_amount_9'];
	$t10=$invoice['taxable_amount_10'];
	$tt=$t5+$t6+$t7+$t6+$t9+$t10;
	$tax_amount=$tax_amount+$tt;

	// resale
	$is_resale=$_POST['is_resale'];
	if($is_resale==1)
	{
		$tax_amount=0;
	}

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_sales_price',$sales_price);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_amount',$tax_amount);
	if($is_resale==1)
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'ItemSalesTaxRef_ListID',$resale_list_id);
	}

	// brad block: create new line items 
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 5" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);

	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Manufacture & Install", $arr['Manufacture'], 1, $arr['ManuDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Parts", $arr['Parts'], 1, $arr['PartsDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Labor only", $arr['Labor'], 1, $arr['LaborDesc']);
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete
	//if this code throws errors, it is most likely because I am not checking that the above line items exist before updating them.

    $action=0;
}

// transfer from shop order
if($action==7)
{

	//print_r($_REQUEST);
	//die;

	$po_id = $_POST['po_id'];
	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'TxnDate',$qbdate);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'po_number',$po_id);

	// clear any existing lines
	$s[]=$vujade->delete_row('invoice_line_items',$invoice_db_id,1,'invoice_id'); 

	// get all the items from the shop order and copy them to the invoice line items table
	$items = $vujade->get_items_for_proposal($project_id);

	//print_r($items);
	//die;

	if($items['error']=="0")
	{
		unset($items['error']);
		foreach($items as $item)
		{
			$vujade->create_row('invoice_line_items');
		    $row_id = $vujade->row_id;
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'invoice_id',$invoice_db_id);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$item['item']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$item['amount']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$item['sort']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$item['tax_label_name']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$item['tax_label_rate']);
		}
	}
	unset($items);

	// reset the sales price, tax, deposit, etc
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'','',2);

	$li_total=0;
    $li_subtotal= 0;
    $li_tax_total=0;
    $li_tax_line=0;
    if($lines['error'] == "0") 
    {
        unset($lines['error']);
        unset($lines['count']);
        $has_lines=1;
        foreach($lines as $item) 
        {
            $li_subtotal += $item['line_amount'];
            if(!empty($item['tax_rate']))
         	{
         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
         	}
         	$li_tax_total+=$li_tax_line;
         	$li_tax_line=0;         
        }
        $li_total=$li_subtotal+$li_tax_total;
    }

    $sales_price=$li_subtotal;
    $tax_amount=$li_tax_total;

	$invoice=$vujade->get_invoice($invoice_db_id);

	$t5=$invoice['taxable_amount_5'];
	$t6=$invoice['taxable_amount_6'];
	$t7=$invoice['taxable_amount_7'];
	$t8=$invoice['taxable_amount_8'];
	$t9=$invoice['taxable_amount_9'];
	$t10=$invoice['taxable_amount_10'];
	$tt=$t5+$t6+$t7+$t6+$t9+$t10;
	$tax_amount=$tax_amount+$tt;

	// resale
	$is_resale=$_POST['is_resale'];
	if($is_resale==1)
	{
		$tax_amount=0;
	}

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_sales_price',$sales_price);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'v_tax_amount',$tax_amount);
	if($is_resale==1)
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'ItemSalesTaxRef_ListID',$resale_list_id);
	}

	// brad block: create new line items 
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 6" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);

	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Manufacture & Install", $arr['Manufacture'], 1, $arr['ManuDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Parts", $arr['Parts'], 1, $arr['PartsDesc']);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Labor only", $arr['Labor'], 1, $arr['LaborDesc']);
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}
	
	//end brad block: this section is complete
	//if this code throws errors, it is most likely because I am not checking that the above line items exist before updating them.

    $action=0;
}

// default
if($action==0)
{

	# get invoice details
	$invoice=$vujade->get_invoice($invoice_db_id);

	if($invoice['error']=="0")
	{

		// line items
		$lines = $vujade->get_invoice_line_items($invoice_db_id,'','',2);

		$date=$invoice['date'];
		$po_id = $invoice['po_number'];	
		//$resale = $invoice['resale'];
		$sales_price=$invoice['v_sales_price'];
		
	    if($lines['error'] == "0") 
	    {
	        unset($lines['error']);
	        unset($lines['count']);
	        $has_lines=1;
	    }

		$i4_label=$invoice['i4_label'];
		$i4_amount=$invoice['i4_amount'];
		$i4_rate=$invoice['i4_rate'];

		// tax rate is the city tax for the work location or user editable
		$tax_rate=$invoice['v_tax_rate'];

		// tax amount is the sum of the tax on the line items or the user editable tax
		$tax_amount = $invoice['v_tax_amount'];

		$engineering=$invoice['v_engineering'];
		$i4_amount=$invoice['i4_amount'];
		$permits=$invoice['v_permits'];
		$permit_acquisition=$invoice['v_permit_acquisition'];
		$retention=$invoice['v_retention'];
		$shipping=$invoice['v_shipping'];
		$discount=$invoice['v_discount'];
		$cc=$invoice['v_cc_processing'];
		$deposit = $invoice['v_deposit'];
		$custom_tax_amount=$invoice['v_custom_tax_amount'];
		$is_resale=$invoice['is_resale'];

		// sum left hand columns
		$subtotal1=$sales_price+$engineering+$i4_amount+$permits+$permit_acquisition+$shipping+$cc-$discount;

		// sum subtotal - tax - deposit - retention
		$subtotal2 = $subtotal1+$tax_amount;

		$total_due = $subtotal2-$deposit;
		$total_due=$total_due-$retention;

		$total_payments=0; // sum all payments
		$payments = $vujade->get_invoice_payments($invoice['invoice_id']);
		if($payments['error']=="0")
		{
			unset($payments['error']);
			foreach($payments as $payment)
			{
				$total_payments+=$payment['amount'];
			}
		}

		$balance_due=0; // sum total due - payments
		$balance_due = $total_due-$total_payments;

	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$menu=17;
$cmenu=1;
$title = 'Invoice - '.$project['project_id'] . ' - ';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		<form method = "post" action = "invoice.php" id = "np">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				<strong>Number: <?php print $invoice['invoice_id']; ?> (Proposal: 
				<?php print $shop_order['proposal']; ?>)
				</strong>
			</div>
			<div class = "col-md-8 pull-right">
				<strong>Date: <input type = "text" name = "date" value = "<?php print $invoice['date']; ?>" id = "date" class = "dp" style = "width:150px;margin-right:10px;"></strong> 
				<strong>PO #: <input type = "text" name = "po_id" id = "po_id" value = "<?php print $invoice['po_number']; ?>" class = "" style = "width:150px;"></strong> 
			</div>
		</div>

		<div class = "row">
			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Billing Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
					# get correct customer info for this project
					$listiderror=0;
					$localiderror=0;
					// try to get by list id
				    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
				    if($customer1['error']!="0")
				    {
				    	$listiderror++;
				    	unset($customer1);
				    }
				    else
				    {
				    	$customer=$customer1;
				    }
				    // try to get by local id
				    $customer2 = $vujade->get_customer($project['client_id'],'ID');
				    if($customer2['error']!="0")
				    {
				    	$localiderror++;
				    	unset($customer2);
				    }
				    else
				    {
				    	$customer=$customer2;
				    }

				    $iderror=$listiderror+$localiderror;

				    if($iderror<2)
				    {
				    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
				    }
				    else
				    {
				    	// can't find customer or no customer on file

				    }
		            if($customer['error']=="0")
		            {
		            	print $customer['name'].'<br>';
		            	print $customer['address_1'].'<br>';
		            	if(!empty($customer['address_2']))
		            	{
		            		print $customer['address_2'].'<br>';
		            	}
		            	print $customer['city'].', '.$customer['state'].' '.$customer['zip'].'<br>';
		            }
					?>
				  </div>
				</div>
			</div>

			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Job Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
		            # get site location for this project
					print $project['site'].'<br>';
					print $project['address_1'].'<br>';
					print $project['city'].', ';
					print $project['state'].' ';
					print $project['zip'].'<br>';;
					// county
					$county=$vujade->get_county($project['city'],$project['state']);
					if($county['error']=="0")
					{
						print $county['county'];
					}
					?>
				  </div>
				</div>
			</div>
		</div>
	
<input type = "hidden" id = "action" value = "1" name = "action">
<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

<div class = "row">
	<div class = "pull-right" style = "margin-right:15px;">
		
		<a class = "btn btn-xs btn-primary pull-right" style = "margin-left:5px;" id = "transfer" href = "#transfer-form" title = "Transfer from other">Transfer</a> 
		<a class = "btn btn-xs btn-primary pull-right" id = "preview" href = "#" title = "Print Preview">Print Preview</a>

		<!-- transfer modal -->
		<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
			<table width = "100%">
				<tr>
					<td>
						<center><label>Proposal Number</label></center>
					</td>
					<td>
						<center><select name = "transfer_proposal" id = "transfer_proposal" class = "form-control">
							<?php
							if(!empty($shop_order['proposal']))
							{
								print '<option value = "'.$shop_order['proposal'].'" selected = "selected">'.$shop_order['proposal'].'</option>';
							}
							print '<option value = "">-Select-</option>';

							$proposals = $vujade->get_proposals_for_project($project_id);
							if($proposals['error']=="0")
							{
								unset($proposals['error']);
								unset($proposals['next_id']);
								foreach($proposals as $proposal)
								{
									print '<option value = "'.$proposal['proposal_id'].'">'.$proposal['proposal_id'].'</option>';
								}
							}
							?>
						</select></center>	
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2"><center><label>OR</label></center></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2">
						<center>
							<a href = "#" id = "use-scope-of-work" class = "btn btn-success">Shop Order</a>
						</center>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>

			</table>

			<input type = "hidden" id = "scope_of_work" name = "scope_of_work" value = "0">

			<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
			
			<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please choose a proposal number.</div>
			<div id = "error_2" style = "display:none;" class = "alert alert-danger">This proposal number is invalid.</div>

			<div class = "" style = "width:100%;margin-top:15px;text-align:center;">
				<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a> 
				<a id = "transfer_btn" href = "#" class = "btn btn-lg btn-success" style = "margin-right:15px;">SAVE AND COMPLETE</a>
			</div>
		</div>

	</div>
</div>

<div style="margin-bottom: 20px; overflow: hidden">
<div style="width: 100%; overflow: hidden;">
<div class="panel panel-primary">
<div class="panel-heading" style="padding: 0 11px">
    <div class="row">
        <div class="col-lg-9">Description</div>
        <div class="col-lg-3">Amount</div>
    </div>
</div>
<div class="panel-body">
<div class="row">
<div class="col-lg-12">
    <!-- header row -->
    <!-- any existing items will be displayed here -->
    <?php
    $li_total=0;
    $li_subtotal= 0;
    $next_sort=1;
    $current_sorts = array();
    $li_tax_total=0;
    $li_tax_line=0;
    $li_count=0;
    if($has_lines) 
    {
        //unset($lines['error']);
        //unset($lines['count']);
        print '<table width = "100%">';
        foreach($lines as $item) 
        {
        	$next_sort++;
        	$li_count++;
            $li_subtotal += $item['line_amount'];
            $current_sorts[]=$item['sort'];
            ?>
            <div class="row">
                <div>
                    <div class="ta_container col-lg-9 col-xs-8">
                        <?php
                        $rand = mt_rand();
                        ?>
                        
                        <textarea style="margin-bottom: 15px; width: 100%" class="ckeditor ars existing-line-item form-control" id="desc-<?php print $item['id']; ?>" name="desc-<?php print $item['id']; ?>"><?php print $item['line_description'];?></textarea>              
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <input type="hidden" class="rand" value="<?php print $rand; ?>">
                        <input type="text" class="amount form-control" value="<?php print $item['line_amount']; ?>"
                               name="amount-<?php print $item['id']; ?>"
                               id="amount-<?php print $item['id']; ?>">

                        Tax Type: 
	                    <select class="tt-select tax form-control" name="tax-<?php print $item['id']; ?>" id="tax-<?php print $item['id']; ?>">
	                     	<?php
	                     	if(!empty($item['tax_type']))
	                     	{
	                     		print '<option value = "'.$item['tax_type'].'^'.$item['tax_rate'].'">'.$item['tax_type'].'</option>';
	                     	}
	                     	print '<option value = "">-Select-</option>';
	                     	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
	                     	?>
	                    </select> 
	                    Tax Amount: $

	                    <?php
	                    if(!empty($item['tax_rate']))
                     	{
                     		$li_tax_line=$item['line_amount']*$item['tax_rate']*$tax_rate;
                     		print @number_format($li_tax_line,2);
                     	}
                     	else
                     	{
                     		print '0.00';
                     	}
                     	$li_tax_total+=$li_tax_line;
                     	$li_tax_line=0;
	                    ?>

                    </div>
					<div class="col-lg-1 col-xs-12">
					<a href = "invoice.php?project_id=<?php print $project_id; ?>&invoice_db_id=<?php print $invoice_db_id; ?>&action=3&after_id=<?php print $item['id']; ?>&position=<?php print $item['sort']; ?>" id = "" class = "add-new-row btn btn-xs btn-success" title = "Add New Item After This Item">+</a> 
					<span style="line-height: 39px"><a title = "Delete Item" class="plus-delete btn btn-xs btn-danger" href="#"
					id="<?php print $item['id']; ?>">X</a></span>
					<br>
					<input type="hidden" class="sort" value="<?php print $item['sort']; ?>"
                               name="sort-<?php print $item['id']; ?>"
                               id="sort-<?php print $item['id']; ?>" style = "">
					</div>
				</div>
			</div>
			<br>
        <?php
        }

        $li_total=$li_subtotal+$li_tax_total;

        // highest current sort
        $highest = max($current_sorts);
        if($next_sort<=$highest)
        {
        	$next_sort=$highest+1;
        }
    }
    ?>
    <!-- blank input row -->
    <div class="row">
        <div class="col-lg-9 col-xs-8" style="">
            <textarea name="description" name="description" id="description" style="margin-bottom: 15px; width: 100%" class="ckeditor ars form-control"></textarea>
        </div>
        <div class="col-lg-2 col-xs-4" style="">
            <input class="form-control" type="text" name="amount" id="amount" style="">
            <span style="margin-right:55px;width:148px;height:35px;">&nbsp;</span>
            <br>
            <input class="" type="hidden" name="sort" id="sort" style="" value = "<?php print $next_sort; ?>">
            Tax Type: 
            <select class="tt-select tax form-control" name="tax_type" id="tax_type">
             	<?php
             	print '<option value = "">-Select-</option>';
             	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
             	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
             	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
             	?>
            </select> 
        </div>
    </div>
</div>
    <div style="margin-bottom: 20px;">
        <div class="row">
            <div class="col-md-12">
                <a class="plus-update btn btn-success btn-xs pull-right" id="update-items" href="#" style="">Update Items</a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>

<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>
<hr>

<!-- subtotal row -->
<div class = "row">
	<div class = "pull-right">
		<h5>Line Items Subtotal: $
		<?php
		print @number_format($li_subtotal,2);
		?>
		</h5>
		<h5>Line Items Tax: $
		<?php
		print @number_format($li_tax_total,2);
		?>
		</h5>
		<h4>Line Items Total: $
		<?php
		print @number_format($li_total,2);
		?>
		</h4>
	</div>
</div>

<hr>

<!-- other amounts boxes and update button -->
<div class = "row">
	<p style = "margin-left:15px;">
		Accepted Proposal Amount: $
		<?php 
		$am=$proposal['amounts']-$proposal['tax_total'];
		print @number_format($am,2); 
		?>
	</p>
</div>

<div class = "row">
	<div class = "col-md-6">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Sales Price
			</div>
			<div class = "col-md-8">
				<input type = "text" name = "sales_price" id = "sales_price" value = "<?php print $sales_price; ?>" class = "form-control col-md-1 mask-number" style = "margin-bottom:15px;">
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Engineering
			</div>
			<div class = "col-md-8">
				<input type = "text" name = "engineering" id = "engineering" value = "<?php print $engineering; ?>" class = "form-control col-md-1 mask-number" style = "margin-bottom:10px;"> 
				<select name = "is_taxable_5" id = "is_taxable_5" class = "form-control" style = "margin-bottom:15px;">
					<?php
					// user has selected something
					if(!empty($invoice['is_taxable_5']))
					{
						if($invoice['is_taxable_5']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice['is_taxable_5']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
					}
					else
					{
						// otherwise display the default
						if($invoice_setup['is_taxable_5']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice_setup['is_taxable_5']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
						// otherwise display the default
						if($invoice_setup['is_taxable_5']==2)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
					}
					print '<option value = "">-Select-</option>';
					print '<option value = "1">Taxable</option>';
					print '<option value = "2">Non-Taxable</option>';
					?>
				</select>
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Permits
			</div>
			<div class = "col-md-8">
				<input type = "text" name = "permits" id = "permits" value = "<?php print $permits; ?>" class = "form-control mask-number" style = "margin-bottom:10px;"> 
				<select name = "is_taxable_6" id = "is_taxable_6" class = "form-control" style = "margin-bottom:15px;">
					<?php
					// user has selected something
					if(!empty($invoice['is_taxable_6']))
					{
						if($invoice['is_taxable_6']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice['is_taxable_6']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
					}
					else
					{
						// otherwise display the default
						if($invoice_setup['is_taxable_6']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice_setup['is_taxable_6']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
						// otherwise display the default
						if($invoice_setup['is_taxable_6']==2)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
					}
					print '<option value = "">-Select-</option>';
					print '<option value = "1">Taxable</option>';
					print '<option value = "2">Non-Taxable</option>';
					?>
				</select>
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Permit Acquisition
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control mask-number" name = "permit_acquisition" id = "permit_acquisition" value = "<?php print $permit_acquisition; ?>" style = "margin-bottom:10px;"> 
				<select name = "is_taxable_7" id = "is_taxable_7" class = "form-control" style = "margin-bottom:15px;">
					<?php
					// user has selected something
					if(!empty($invoice['is_taxable_5']))
					{
						if($invoice['is_taxable_7']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice['is_taxable_7']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
					}
					else
					{
						// otherwise display the default
						if($invoice_setup['is_taxable_7']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice_setup['is_taxable_7']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
						// otherwise display the default
						if($invoice_setup['is_taxable_7']==2)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
					}
					print '<option value = "">-Select-</option>';
					print '<option value = "1">Taxable</option>';
					print '<option value = "2">Non-Taxable</option>';
					?>
				</select>
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Shipping
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control mask-number" name = "shipping" id = "shipping" value = "<?php print $shipping; ?>" style = "margin-bottom:10px;"> 
				<select name = "is_taxable_8" id = "is_taxable_8" class = "form-control" style = "margin-bottom:15px;">
					<?php
					// user has selected something
					if(!empty($invoice['is_taxable_8']))
					{
						if($invoice['is_taxable_8']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice['is_taxable_8']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
					}
					else
					{
						// otherwise display the default
						if($invoice_setup['is_taxable_8']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice_setup['is_taxable_8']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
						// otherwise display the default
						if($invoice_setup['is_taxable_8']==2)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
					}
					print '<option value = "">-Select-</option>';
					print '<option value = "1">Taxable</option>';
					print '<option value = "2">Non-Taxable</option>';
					?>
				</select>
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Credit Card Fees
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control mask-number" name = "cc_processing_fees" id = "cc_processing_fees" value = "<?php print $cc; ?>" style = "margin-bottom:10px;"> 
				<select name = "is_taxable_9" id = "is_taxable_9" class = "form-control" style = "margin-bottom:15px;">
					<?php
					// user has selected something
					if(!empty($invoice['is_taxable_9']))
					{
						if($invoice['is_taxable_9']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice['is_taxable_9']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
					}
					else
					{
						// otherwise display the default
						if($invoice_setup['is_taxable_9']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice_setup['is_taxable_9']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
						// otherwise display the default
						if($invoice_setup['is_taxable_9']==2)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
					}
					print '<option value = "">-Select-</option>';
					print '<option value = "1">Taxable</option>';
					print '<option value = "2">Non-Taxable</option>';
					?>
				</select>
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Discount
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control mask-number" name = "discount" id = "discount" value = "-<?php print $discount; ?>" style = "margin-bottom:10px;"> 
				<select name = "is_taxable_10" id = "is_taxable_10" class = "form-control" style = "margin-bottom:15px;">
					<?php
					// user has selected something
					if(!empty($invoice['is_taxable_10']))
					{
						if($invoice['is_taxable_10']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice['is_taxable_10']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
					}
					else
					{
						// otherwise display the default
						if($invoice_setup['is_taxable_10']==1)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
						if($invoice_setup['is_taxable_10']==2)
						{
							print '<option value = "2" selected">Non-Taxable</option>';
						}
						// otherwise display the default
						if($invoice_setup['is_taxable_10']==2)
						{
							print '<option value = "1" selected">Taxable</option>';
						}
					}
					print '<option value = "">-Select-</option>';
					print '<option value = "1">Taxable</option>';
					print '<option value = "2">Non-Taxable</option>';
					?>
				</select>
			</div>
		</div>

		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Subtotal
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control" name = "subtotal1" id = "subtotal1" value = "<?php print @number_format($subtotal1,2,'.',','); ?>" disabled style = "float:left;width:150px;">
			</div>
		</div>
	</div>

	<div class = "col-md-6">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Tax Rate
			</div>
			<div class = "col-md-8">
				<input type = "text" name = "tax_rate" class = "form-control col-md-3 mask-number" id = "tax_rate" value = "<?php print $tax_rate; ?>" style = "margin-bottom:15px;">
			</div>
		</div>
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Tax Amount
			</div>
			<div class = "col-md-8">
				<input type = "text" name = "tax_amount" id = "tax_amount" value = "<?php print $tax_amount; ?>" class = "form-control col-md-3 mask-number" style = "margin-bottom:15px;">
				<br>
				<input type = "checkbox" name = "custom_tax_amount" value = "1" id = "custom_tax_amount" <?php if($custom_tax_amount==1){ print 'checked'; } ?> > Use custom tax amount <a href="#" data-toggle="tooltip" title="Enter your custom tax amount in the box to the left and then check this box to have the system override and save the tax amount to your custom value."><span class = "glyphicon glyphicon-info-sign">&nbsp;</span></a>
			</div>
		</div>
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Subtotal
			</div>
			<div class = "col-md-8">
				<input type = "text" name = "subtotal2" id = "subtotal2" class = "form-control col-md-3" value = "<?php print @number_format($subtotal2,2,'.',','); ?>" disabled style = "margin-bottom:15px;">
			</div>
		</div>
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Deposit
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control col-md-3 mask-number" name = "deposit" id = "deposit" value = "-<?php print $deposit; ?>" style = "margin-bottom:15px;">
			</div>
		</div>
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Retention
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control col-md-3 mask-number" name = "retention" id = "retention" value = "-<?php print $retention; ?>" style = "margin-bottom:15px;">
			</div>
		</div>
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				Total Due
			</div>
			<div class = "col-md-8">
				<input type = "text" class = "form-control col-md-3" name = "total_due" id = "total_due" value = "<?php print @number_format($balance_due,2,'.',','); ?>" disabled style = "margin-bottom:15px;">
			</div>
		</div>

	</div>
</div>

<p style = "margin-left:0px;">
	<strong>Is this invoice resale?</strong><br>
	<input type = "radio" class = "resale" name = "is_resale" id = "is_resale_1" class = "" value = "1" <?php if($is_resale==1){print "checked"; } ?>>Yes
	<br>
	<input type = "radio" class = "resale" name = "is_resale" id = "is_resale_2" class = "" value = "0" <?php if($is_resale==0){ print "checked"; } ?>>No
</p>

<hr>

<div class = "row">
	<div class = "col-md-12">
		<a href = "#" class = "btn btn-success" style = "" id = "update_totals">UPDATE TOTALS</a>
	</div>
</div>

<hr style = "clear:both;width:97%;">

<input type = "hidden" name = "invoice_db_id" id = "invoice_db_id" value = "<?php print $invoice_db_id; ?>">

<p style = "margin-left:0px;">
	<input type = "submit" value = "SAVE" id = "done" class = "btn btn-success" style = "">
</p>

</form>

</div>
</div>

</div>	
</div>

</div>

</div>
</div>

</section>
</section>

<div id = "project-desc" style = "display:none;">
	<?php print $project['description']; ?>
</div>

<!-- modal for tax type selection errors -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id = "tax_type_error_modal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style = "padding:15px;">
    <h1>Warning</h1>

    <p id = "js-values"></p>

	<p>
		<a id = "review" class="popup-modal-dismiss btn btn-lg btn-success" href="#">Review</a>
		<a id = "proceed" class="btn btn-lg btn-primary" href="#">Proceed as is</a>
	</p>
    </div>
  </div>
</div>

<!-- modal for print preview -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id = "preview-modal">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" id = "preview-container" style = "padding:15px;width:900px;height:600px;overflow:auto;">
    
    </div>
  </div>
</div>

<style>
/* The side navigation menu */
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 0; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111; /* Black*/
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>
<div id="print_preview" class="sidenav">
  	<a href="#" class="closebtn" id="close_preview" style = "margin-top:50px;clear:both;">X</a><br>
  	<div id = "preview_container"></div>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	// invoice preview
	$('#preview').click(function(e)
	{
		e.preventDefault();
		$('#topbar').hide();
		$('#print_preview').css('width','100%');
		$('#preview_container').html('');

		// load as iframe
		var id = "<?php print $project_id; ?>";
		var invoiceid = "<?php print $invoice_db_id; ?>";
		var iframesrc = "print_invoice.php?id="+id+"&invoiceid="+invoiceid;
		var iframe = '<iframe width = "1000" height="1200" src="'+iframesrc+'"></iframe>';
		$('#preview_container').html(iframe);
        //$('#preview-modal').modal('show');
	});
	
	// close invoice preview
	$('#close_preview').click(function(e)
	{
		e.preventDefault();
		$('#preview-container').html('');
        //$('#preview-modal').modal('hide');
        $('#print_preview').css('width','0');
        $('#topbar').show();
	});

	// date pickers
	$('.dp').datepicker();

	// 
    var d = $('#description').html();
    $("#description").html($.trim(d));

    // if user types in the tax rate box, delete the tax amount
    $('#tax_rate').focus(function()
    {
        $('#tax_amount').val(0);
    });

    // change in the tax amount box deletes the tax rate
    $('#tax_amount').focus(function()
    {
        $('#tax_rate').val(0);
    });

    // delete an item
    $('.plus-delete').click(function(e){
        e.preventDefault();
        $('#tax_amount_box').val(0);
        var item_id = this.id;
        $('<input>').attr({
            type: 'hidden',
            id: 'item_id',
            name: 'item_id',
            value: item_id
        }).appendTo('#np');
        $('#action').val('2');
        $('#np').submit();
    });

    // update items button
    $('#update-items').click(function(e)
    {
        e.preventDefault();
        // clear tax amount
        $('#tax_amount_box').val('');

        var js_values='';

        // check if all line items have tax rate...
        var count=0;

        // count of line items
        var li_count=<?php print $li_count; ?>; 

        // determine whether to check the blank row

        // blank row amount
        var namount = $('#amount').val();

        // blank row description
        var ndesc = CKEDITOR.instances['description'].getData();
        
        // one or the other was typed into, so tax type should be checked
        if( (namount!='') || (ndesc!='') )
        {
        	// alert('not blank...');
        	// var tax_amount_selected = $('#tax_type').val();
        	li_count++;
        }

        // loop through each tax type and check if selected
        $('.tt-select').each(function()
        {
        	// they are selected
        	if($(this).val()!='')
        	{
        		count++;
        		//alert('not selected...'+this.id);
        	}
		});

        // tax selected for blank item
        if( (namount!='') || (ndesc!='') )
        {
        	// alert('not blank...');
        	var tax_amount_selected = $('#tax_type').val();
        	if(tax_amount_selected!='')
        	{
        		count++;
        	}
        }

		// at least one tax type missing
		if(count<li_count)
		{
			//js_values='One or more line items was not assigned a tax rate. Do you wish to proceed anyway? error 2. line items count: '+li_count+' count: '+count;

			js_values='One or more line items was not assigned a tax rate. Do you wish to proceed anyway?';

			//tax_type_error_modal
			$('#js-values').html(js_values);
			$('#tax_type_error_modal').modal('show');
			return false;
		}

        // submit form
        $('#action').val('1');
        $('#np').submit();
    });

	// proceed button was pressed
    $('#proceed').click(function(e)
    {
        e.preventDefault();

        // submit form
        $('#action').val('1');
        $('#np').submit();
    });

    // review button 
    $('#review').click(function(e)
    {
        e.preventDefault();
        $('#tax_type_error_modal').modal('hide');
    });

    // auto resize text areas to fit the height of line item text
    $('.existing-line-item').each(function()
    {
    	$(this).height( $(this)[0].scrollHeight );
    });

    $('.existing-line-item').change(function()
    {
    	$(this).height( $(this)[0].scrollHeight );
    });

	// update totals button
    $('#update_totals').click(function(e)
    {
    	e.preventDefault();
    	$('#tax_amount').prop('disabled',false);
    	$('#action').val('4');
    	//alert($('#action').val());
    	$('#np').submit();
    });

    // done button
    $('#done').click(function(e)
    {
    	e.preventDefault();
    	$('#tax_amount').prop('disabled',false);
    	$('#action').val('5');
    	//alert($('#action').val());
    	$('#np').submit();
    });

    // transfer button : show modal
    $('#transfer').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,
		callbacks: 
		{
		    open: function() 
		    {
		    	// modal is opened; 
		    	// set the option list to select no value
		    	// set the button light gray
		    	$('#scope_of_work').val(0);
				$('#transfer_proposal').val('');
				$('#use-scope-of-work').css('opacity','.33');
		    },
		    close: function() 
		    {
		      // Will fire when popup is closed
		    }
		}		
	});

    // user changed the proposal number in the modal to 
	// something other than no value
	// set the opacity fo the use scope of work button to 33%
	$('#transfer_proposal').change(function()
	{
		var pid = $('#transfer_proposal').val();
		if(pid!='')
		{
			$('#scope_of_work').val(0);
			$('#use-scope-of-work').css('opacity','.33');
		}
	});

    // user pressed scope of work button
	$('#use-scope-of-work').click(function(e)
	{
		e.preventDefault();
		$('#scope_of_work').val(1);
		$('#transfer_proposal').val('');
		$('#use-scope-of-work').css('opacity','1');
	});
    
    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();

    	$('#error_1').hide();

    	var sow = $('#scope_of_work').val();

    	// use scope of work
    	if(sow==1)
    	{
			$('#proposal').remove();
    		$('#action').val('7');
			$('#np').submit();
    	}
    	else
    	{
    		// user selected a proposal
    		var pid = $('#transfer_proposal').val();
    		if(pid=='')
    		{
    			$('#error_1').show();
    			return false;
    		}
    		else
    		{
    			//$('#proposal').val(pid).change();
    			$('#proposal').remove();
    			$('<input>').attr({
		            type: 'hidden',
		            id: 'proposal',
		            name: 'proposal',
		            value: pid
		        }).appendTo('#np');
    			$('#action').val('6');
				$('#np').submit();
    		}
    	}

    	// close the modal
    	$.magnificPopup.close();

    	$('#scope_of_work').val(0);

    });

    // modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
