<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$setup = $vujade->get_setup();

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($projects_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
if($project['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}
$pos = $vujade->get_materials_for_costing($project_id);

$html = '<html>
<head>
    <title>Print Costing Inventory</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family:Helvetica;
            
            color:#000;
        }
        body
        {
            width:120%;
            font-family:Helvetica;
            margin:0;
            padding:0;
        }
         
        p
        {
            margin:0;
            padding:0;
        }
         
        #wrapper
        {
            width:210mm;
            margin:0 10mm;
            padding-right: 35px;
        }
         
        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }
        
        th{
            font-size: 6mm;
        }
        td{
            font-size: 4mm;
        }
        
    </style>
</head>
<body>
<div id="wrapper">
    <p style="text-align:center; font-size: 7mm; padding-top:15mm;">Job Costs: Materials for Project # '.$project['project_id'].'</p>
    <br />
    <table border="1" style="border-spacing:0;width:105%;border-collapse: collapse;">
        <tr style="background-color: rgb(153,153,153);border-spacing: 0; border-collapse: collapse; ">
            <th style="width: 20%;text-align:left;border-spacing: 0;">Date</th>
            <th style="width: 20%;text-align:left;border-spacing: 0;">Item #</th>
            <th style="width: 15%;text-align:left;border-spacing: 0;">Qty</th>
            <th style="width: 35%;text-align:left;border-spacing: 0;">Description</th>
            <th style="width: 15%;text-align:left;border-spacing: 0;">Total</th>
            
        </tr>';  
$subtotal = 0;
$indt = 0;
$total = 0;
$purchase_orders = $pos;

foreach($purchase_orders as $po) 
{

    if(empty($po['qty']))
    {
        continue;
    }
   
    $quant = $po['qty'];
    $quant_subtotal = $quant * $po['cost']; 
    $subtotal += $quant_subtotal;
   
    $html .= '
    <tr style="padding-left:5px;border-spacing: 0;border-collapse: collapse;">
        <td style="border:1px solid black;">' . $po['date_entered'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . $po['inventory_id'] . ' &nbsp;</td>
        <td style="border:1px solid black;">' . $po['qty'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . $po['description'] . '&nbsp;</td>
        <td style="border:1px solid black;">' . number_format($quant_subtotal,2,'.',',') . '&nbsp;</td>
    </tr>';
}

$indt = $subtotal*$project['indeterminant'];
$total = $subtotal + $indt;
$indet_text = $setup['indeterminant']*100;

$html .= '</table>
    <br>
        <div>
            <p>________________________________________________________________________________________&nbsp;</p>
        </div>
        <div style="padding-left:50%; ">
         
        <table style="width:100%; ">
            <tr>
                <td style="width: 70%;text-align:right;"><b>Sum of Inventory Material:</b></td>
                <td style="width: 30%;text-align:right;"><b>$'.number_format($subtotal,2,'.',',').'</b></td>
            </tr>
            <tr>
                <td style="width: 35%;text-align:right;"><b>Indeterminate '.$indet_text.'%:</b></td>
                <td style="width: 15%;text-align:right;"><b>$'.number_format($indt,2,'.',',').'</b></td>
            </tr>
            <tr>
                <td style="width: 35%;text-align:right;"><b>Total:</b></td>
                <td style="width: 15%;text-align:right;"><b>$'.number_format($total,2,'.',',').'</b></td>
            </tr>
        </table>
      </div>
    </div> 
</body>
</html>';

require_once("mpdf60/mpdf.php");
$mpdf=new mPDF('c','A4','','Helvetica' , 0 , 0 , 0 , 0 , 0 , 0); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
$mpdf->WriteHTML($html);

require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
    $pdfts = strtotime('now');
    $pdfname = 'mobile_pdf/'.$pdfts.'-costing-inventory.pdf';

    // set to mysql table (chron job deletes these files nightly after they are 1 day old)
    $vujade->create_row('mobile_pdf');
    $pdf_row_id = $vujade->row_id;
    $s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
    $s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
    $mpdf->Output($pdfname,'F');
    print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
    $mpdf->Output('Costing Inventory.pdf','I'); 
}
?>