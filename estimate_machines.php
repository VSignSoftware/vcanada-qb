<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($estimates_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$estimateid=$_REQUEST['estimateid'];

$estimate = $vujade->get_estimate($estimateid);

$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# recalculate the time total (update button was pressed)
if($action>0)
{
	$estimate_id = $estimate['estimate_id'];
	
	// determine if labor already exists (ie estimate not new)
	// or does not exist (ie estimate is new)
	$el_test = $vujade->get_machines_for_estimate($estimate['estimate_id']);

	if($el_test['error']=="0")
	{
		// estimate is not new

		// combine row ids and hours (update value)
		$dbids = $_POST['dbids'];
		$hours = $_POST['hours'];
		$new_arr = @array_combine($dbids, $hours);

		foreach($new_arr as $row_id => $hours)
		{
			// update existing
			$s[]=$vujade->update_row('estimates_machines',$row_id,'time_hours',$hours);
		}
	}
	else
	{
		// estimate is new
		$types = $_POST['types'];
		$hours = $_POST['hours'];
		$rates = $_POST['rates'];
		$labor_ids = $_POST['labor_ids'];

		foreach($types as $key => $type)
		{
			// create new
			$vujade->create_row('estimates_machines');
			$id=$vujade->row_id;

			$s[]=$vujade->update_row('estimates_machines',$id,'estimate_id',$estimate_id);
			$s[]=$vujade->update_row('estimates_machines',$id,'time_type',$type);

			$rate = $rates[$key];
			$hour = $hours[$key];
			$labor_id = $labor_ids[$key];

			$s[]=$vujade->update_row('estimates_machines',$id,'rate',$rate);
			$s[]=$vujade->update_row('estimates_machines',$id,'time_hours',$hour);
			$s[]=$vujade->update_row('estimates_machines',$id,'labor_id',$labor_id);
		}
	}

	if($action==2)
	{
		$vujade->page_redirect('estimate_labor.php?project_id='.$project_id.'&estimateid='.$estimateid);
	}
	if($action==3)
	{
		$vujade->page_redirect('estimate_buyout.php?project_id='.$project_id.'&estimateid='.$estimateid);
	}
	if($action==4)
	{
		$vujade->page_redirect('project_estimates.php?id='.$project_id.'&estimateid='.$estimateid);
	}
}

$estimate = $vujade->get_estimate($estimateid);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$section=3;
$menu=4;
$title = "Estimate Machines - ";
$charset = "ISO-8859-1";
require_once('h.php');
?>
<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Estimate <?php print $estimate['estimate_id']; ?> Machines</a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

	<div class="admin-form theme-primary">

	    <?php 
	    $vujade->show_errors();
	    $vujade->show_messages();
	    ?>

	    <form method = "post" action = "estimate_machines.php" id = "nd" class = "form-inline">
		<input type = "hidden" name = "action" id = "action" value = "">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
		<input type = "hidden" value = "<?php print $estimateid; ?>" name = "estimateid">
		<div class="panel heading-border panel-primary">
            <div class="panel-body bg-light">
		
				<div style = "width:100%;height:50px;overflow-x:auto;margin-bottom:15px;">
					<?php print $estimate['description']; ?>
				</div>

				<div class = "row">
					<div class = "col-md-12">

						<table width = "100%" class = "table table-hover table-striped  table-bordered">
							<thead>
								<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
								<th width = "70%">
									Machine Description
								</th>
								<th width = "10%">
									Quantity
								</th>
								<th width = "10%">
									Rate
								</th>
								<th width = "10%">
									Amount
								</th>
								</tr>
								</thead>

								<tbody>
									<?php
									$labor_total=0;

									// estimate is not new
									// (ie, has labor added to it)
									$el_test = $vujade->get_machines_for_estimate($estimate['estimate_id']);
									if($el_test['error']=="0")
									{
										unset($el_test['error']);
										foreach($el_test as $lt)
										{
											$line_total = $lt['rate']*$lt['hours'];
											$labor_total+=$line_total;
											print '<tr>';
											print '<td>';
											print $lt['type'];
											print '</td>';
											print '<td>';
											print '<input type = "hidden" name = "labor_types[]" value = "'.$lt['type'].'">';

											print '<input type = "text" name = "hours[]" class = "form-control" value = "'.$lt['hours'].'" style = "width:100px;">';

											print '<input type = "hidden" name = "dbids[]" value = "'.$lt['database_id'].'" style = "width:50px;">';

											print '</td>';
											print '<td>';
											print '$'.@number_format($lt['rate'],2,'.',',');
											print '</td>';
											print '<td>';
											print '$'.@number_format($line_total,2,'.',',');
											print '</td>';
											print '</tr>';
										}	
									}
									else
									{
										// is new
										$machine_types = $vujade->get_machine_types();
										if($machine_types['error']=="0")
										{
											unset($machine_types['error']);
											foreach($machine_types as $lt)
											{
												$line_hours = 0;
												$line_total = 0;
												$labor_total+=$line_total;
												print '<input type = "hidden" name = "types[]" value = "'.$lt['type'].'" style = "width:30px;">';
												print '<tr>';
												print '<td>';
												print $lt['type'];
												print '</td>';
												print '<td>';
												print '<input type = "text" name = "hours[]" class = "form-control" value = "'.$lt['hours'].'" style = "width:100px;">';
												print '</td>';
												print '<td>';
												print '$'.@number_format($lt['rate'],2,'.',',');
												print '<input type = "hidden" name = "rates[]" value = "'.$lt['rate'].'" style = "width:30px;">';
												print '<input type = "hidden" name = "labor_ids[]" value = "'.$lt['database_id'].'" style = "width:30px;">';
												print '</td>';
												print '<td>';
												print '$'.@number_format($line_total,2,'.',',');
												print '</td>';
												print '</tr>';
											}
										}
									}
									?>
									<tr class = "dark">
										<td>&nbsp;</td>
										<td><a class = "plus btn btn-xs btn-primary" href = "#">Update</a></td>
										<td>Subtotal: </td>
										<td>$<?php print @number_format($labor_total,2,'.',','); ?></td>
									</tr>
								</tbody>
						</table>
					</div>
				</div>

				<div style = "margin-top:15px;">
					<a class = "btn btn-primary" href = "#" id = "prev" style = "">PREVIOUS</a> 
					<a class = "btn btn-success" href = "#" id = "next" style = "margin-left:15px;">SAVE AND CONTINUE</a> 
					<a class = "btn btn-success" href = "#" id = "done" style = "margin-left:15px;">SAVE AND COMPLETE</a> 
			        </form> 
		        </div>

			</div>
		</div>
	</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  //var estimateid = <?php print $estimateid; ?>;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // prevent user from pressing return on qty fields
	$("form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) 
        {
            $('button[type=submit] .default').click();   
            return false;
        }
    });

	// update button
	$('.plus').click(function(e)
	{
		e.preventDefault();
		$('#action').val('1');
		$('#nd').submit();
	});

	// previous button
	$('#prev').click(function()
	{
		$('#action').val('2');
		$('#nd').submit();
	});

	// next button
	$('#next').click(function()
	{
		$('#action').val('3');
		$('#nd').submit();
	});

	// done button
	$('#done').click(function()
	{
		$('#action').val('4');
		$('#nd').submit();
	});
	
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
