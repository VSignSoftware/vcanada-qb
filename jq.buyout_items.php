<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$estimate_id = $_REQUEST['estimate_id'];
$estimate = $vujade->get_estimate($estimate_id);
if($estimate['error']!="0")
{
	$estimate['estimate_id']=$estimate_id;
}
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# new buyout
if($action==1)
{
	$cost = str_replace(',', '', $_POST['cost']);
	$cost = str_replace('$', '', $_POST['cost']);
	$description=str_replace('"','&quot;',$_POST['description']);
	$description=str_replace("'",'&#39;',$_POST['description']);
	$subcontractor=str_replace('"','&quot;',$_POST['vendor']);
	$subcontractor=str_replace("'",'&#39;',$_POST['vendor']);
	$s=array();
	$vujade->create_row('estimates_buyouts');
	$row_id = $vujade->row_id;
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'estimate_id',$estimate['estimate_id']);
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
	$action=0;
}

# update buyout
if($action==2)
{
	$row_id = $_POST['id'];
	$cost = str_replace(',', '', $_POST['cost']);
	$cost = str_replace('$', '', $_POST['cost']);
	$description=str_replace('"','&quot;',$_POST['description']);
	$description=str_replace("'",'&#39;',$_POST['description']);
	$subcontractor=str_replace('"','&quot;',$_POST['vendor']);
	$subcontractor=str_replace("'",'&#39;',$_POST['vendor']);
	$s=array();
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
	$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
	$action=0;
}

# delete buyout
if($action==3)
{
	$row_id = $_POST['id'];
	$s=$vujade->delete_row('estimates_buyouts',$row_id);
	$action=0;
}

if($action==0)
{
	?>

<!DOCTYPE html>
<html>

<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title><?php print $title; ?></title>
<meta name="keywords" content="<?php print $title; ?>" />
<meta name="description" content="<?php print $title; ?>">
<meta name="author" content="V Sign Software">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600'>
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
<link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome/font-awesome.css">

	<table width = "100%" class = "table table-hover table-striped  table-bordered">
		<thead>
			<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
			<th>
				Vendor
			</th>
			<th>
				Description
			</th>
			<th>
				Amount
			</th>
			<th>
				&nbsp;
			</th>
			</tr>
			</thead>

			<tbody>

			<?php
			$buyout_total=0;
			$buyouts = $vujade->get_buyouts($estimate_id);
			if($buyouts['error']=="0")
			{
				unset($buyouts['error']);
				foreach($buyouts as $buyout)
				{
					print '<tr>';
					print '<td>';
					print '<input type = "hidden" name = "buyout_id[]" value = "'.$buyout['database_id'].'">';
					print '<input type = "text" class = "existing-buyout-vendor form-control" value = "'.str_replace('"','&quot;',$buyout['subcontractor']).'" style = "width:100%;" name = "vendor[]">';
					print '</td>';

					print '<td>';
					print '<input type = "text" class = "existing-buyout-description form-control" value = "'.str_replace('"','&quot;',$buyout['description']).'" style = "width:100%;" name = "description[]">';
					print '</td>';

					print '<td>';
					print '<input type = "text" class = "existing-buyout-cost form-control" value = "'.$buyout['cost'].'" style = "" name = "cost[]">';
					print '</td>';

					print '<td>';
					print '&nbsp;';

					print '<a href = "#" id = "'.$buyout['database_id'].'" class = "plus-buyout-delete btn btn-xs btn-danger">X</a>';

					print '</td>';
					print '</tr>';
					$buyout_total=$buyout_total+$buyout['cost'];
				}
			}

			# blank row
			print '<tr>';

			print '<td>';

			print '<input type = "hidden" name = "buyout_id[]" value ="0">';

			print '<input type = "text" class = "new-buyout-vendor form-control" value = "" style = "width:100%;" name = "vendor[]">';
			print '</td>';

			print '<td>';
			print '<input type = "text" class = "new-buyout-description form-control" value = "" style = "width:100%;" name = "description[]">';
			print '</td>';

			print '<td>';
			print '<input type = "text" class = "new-buyout-cost form-control" value = "" style = "" name = "cost[]">';
			print '</td>';

			print '<td>';

			//print ' <a href = "#" id = "new-buyout" class = "plus-buyout-new btn btn-primary btn-xs">Save</a> ';
			print '&nbsp;';
			print '</td>';
			print '</tr>';
			?>
			<tr class = "dark">
				<td>&nbsp;</td>
				<td>&nbsp;</td>
				<td>Total:</td>
				<td>$<?php print number_format($buyout_total,2,'.',','); ?></td>
			</tr>
		</tbody>
	</table>

	<?php
}
?>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$('.plus-buyout-new').click(function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#buyout_items').html('');
		$('#buyout_items').html(loading);
		$('#tfc').html('');
		var estimate_id = "<?php print $estimate_id; ?>";
		var vendor = $(this).closest('tr').find('.new-buyout-vendor').val();
		var description = $(this).closest('tr').find('.new-buyout-description').val();
		var cost = $(this).closest('tr').find('.new-buyout-cost').val();
		$.post( "jq.buyout_items.php", { action: 1, estimate_id: estimate_id, vendor: vendor, description: description, cost: cost })
		  .done(function( data ) 
		  {
		  		$('#buyout_items').html(data);
		  });
	});

	$('.plus-buyout-save').click(function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#buyout_items').html('');
		$('#buyout_items').html(loading);
		$('#tfc').html('');
		var estimate_id = "<?php print $estimate_id; ?>";
		var id = this.id;
		var vendor = $(this).closest('tr').find('.existing-buyout-vendor').val();
		var description = $(this).closest('tr').find('.existing-buyout-description').val();
		var cost = $(this).closest('tr').find('.existing-buyout-cost').val();
		$.post( "jq.buyout_items.php", { action: 2, estimate_id: estimate_id, vendor: vendor, description: description, cost: cost, id: id })
		  .done(function( data ) 
		  {
		  		$('#buyout_items').html(data);
		  });
	});

	$('.plus-buyout-delete').click(function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#buyout_items').html('');
		$('#buyout_items').html(loading);
		$('#tfc').html('');
		var estimate_id = "<?php print $estimate_id; ?>";
		var id = this.id;
		$.post( "jq.buyout_items.php", { action: 3, id: id, estimate_id: estimate_id })
		  .done(function( data ) 
		  {
		  		$('#buyout_items').html(data);
		  });
	});

});	
</script>