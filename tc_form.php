<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

if(!isset($_REQUEST['dt']))
{
	$download_type='I';
}
else
{
	$download_type=$_REQUEST['dt'];
}

$title = "Time Card Report";
$html = '';

$userData = array(
array('', ''),
array('', ''),
array('', '')
);

$html .= '

<!DOCTYPE html>
	<head>
	<meta charset="'.$charset.'">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>'.$title.'</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<style>
		.bordered
		{
		    padding-bottom:3px;
		    border-bottom: 1px solid #cecece;
		}
		.header
		{
		    font-weight: bold;
		    font-size: 14px;
		}
		.header_graybg
		{
		    background-color: #cecece;
		    font-weight: bold;
		    font-size: 14px;
		    padding-bottom:3px;
		    border-bottom: 1px solid #cecece;
		}
		
		
		table {font-family: DejaVuSansCondensed; font-size: 11pt; line-height: 1.2;
			margin-top: 2pt; margin-bottom: 5pt;
			border-collapse: collapse;  }

		thead {	font-weight: bold; vertical-align: bottom; }
		tfoot {	font-weight: bold; vertical-align: top; }
		thead td { font-weight: bold; }
		tfoot td { font-weight: bold; }

		thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }

		.headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
		.footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }

		th {	font-weight: bold; 
			vertical-align: top; 
			text-align:left; 
			padding-left: 2mm; 
			padding-right: 2mm; 
			padding-top: 0.5mm; 
			padding-bottom: 0.5mm; 
		 }

		td {	padding-left: 2mm; 
			vertical-align: top; 
			text-align:left; 
			padding-right: 2mm; 
			padding-top: 0.5mm; 
			padding-bottom: 0.5mm;
		 }

		th p { text-align: left; margin:0pt;  }
		td p { text-align: left; margin:0pt;  }
		
		.each-card-box{
			margin-bottom: 10px;
		}
		
		</style>
		
</head>
<body>
	<div id="mainContent">
		<div id="content">
			'.each_time_card_table($userData).'
		</div>
	</div>
</body>
</html>';

function each_time_card_table($userData)
{
	
	$separatorHTML = '<div style="margin-bottom: 10px;">
						<table width="100%">
							<tr>
								<td width="10%" style="border-bottom: #000000 1px dotted"></td>
								<td width="80%"></td>
								<td width="10%" style="border-bottom: #000000 1px dotted"></td>
							</tr>
						</table>
					</div>';
	
	$HTMLReslt = '';
	$counter = 1;
	foreach($userData as $data)
	{
		$HTML = '<div class="each-card-box">
					<div class="header">
					<table width="100%" >
					<tr>
					<td width="40%"><img style = "height:40px;" src="images/print_logo_small.jpg" /></td>
					<td width="60%">
					<h3>DAILY TIME CARD</h3>
					</td>
					</tr>
					</table>
					</div>
					<div style="margin-bottom: 10px;" class="card-person-info">
					<table width="100%" >
					<tr>
					<td width="8%">Name:</td>
					<td width="40%" style="border-bottom: #000000 1px solid">'.$data[0].'</td>
					<td align="right" width="10%">Date:</td>
					<td width="40%" style="border-bottom: #000000 1px solid">'.$data[1].'</td>
					</tr>
					</table>
					</div>

					<div class="card-table-container">
					<table width="100%" cellspacing="0" cellpading="0" border="1" >
					<tr class = "header_graybg">
					<td width="8%">Desc.</td>
					<td width="10%">Job No</td>
					<td width="17%">Job Name</td>
					<td width="7%">ST</td>
					<td width="7%">OT</td>
					<td width="8%">Desc.</td>
					<td width="10%">Job No.</td>
					<td width="17%">Job Name</td>
					<td width="7%">ST</td>
					<td width="7%">OT</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
					<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					</tr>
						
					<tr>
					<td colspan="2">Approved</td>
					<td colspan="8">
					 
					<table width="100%" >
					<tr>
					<td width="60%">FOR OFFICE USE ONLY</td>
					<td width="40%">
					<table width="100%" >
					<tr>
					<td width="6%">ST</td>
					<td width="23%" style="border: #000000 1px solid"></td>
					<td width="3%"></td>
					
					<td width="6%">OT</td>
					<td width="23%" style="border: #000000 1px solid"></td>
					<td width="3%"></td>
					
					<td width="6%">DT</td>
					<td width="23%" style="border: #000000 1px solid"></td>
					<td width="3%"></td>
					</tr>
					</table>
					</td>
					</tr>
					</table>
					</td>
					</tr>
						
					</table>
					</div>
					</div>
					';
					
		
		/*
		if($counter > 0 && $counter%3 == 0)
			//$HTMLReslt .= $HTML.'<pagebreak />';
		else
			$HTMLReslt .= $HTML.$separatorHTML;
		*/
		$HTMLReslt .= $HTML.$separatorHTML;
		$counter++;
	}
	return $HTMLReslt;
}
 
//echo $html;

require_once("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Futura-Book', 10, 10, 10, 10);
$mpdf->WriteHTML($html,'I');

// show the pdf in the iframe
if($download_type=="I")
{
	$mpdf->Output('time-cards.pdf','I');
}
else
{
	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-time-cards.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
	 	$mpdf->Output($pdfname,'F');
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		$mpdf->Output('time-cards.pdf','I'); 
	}
}
?>