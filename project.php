<?php 
session_start();
$tarray=array();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);
# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$_SESSION['clicked_detail_page']=1;
$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# update the notes
if($action==1)
{
	if($projects_permissions['edit']==1)
	{
		$notes = trim($_POST['notes']);
		$success = $vujade->update_row('projects',$id,'notes',$notes,'project_id');
		if($success==1)
		{
			$vujade->messages[]="Notes updated.";
		}
		$project = $vujade->get_project($id,2);
		if($project['error']!=0)
		{
			$vujade->page_redirect('error.php?m=3');
		}
	}
	else
	{
		$vujade->page_redirect('error.php?m=1');
	}
}

# update the project scope
if($action==2)
{
	if($projects_permissions['edit']==1)
	{
		$description = $_POST['description'];
		$success = $vujade->update_row('projects',$id,'description',$description,'project_id');
		if($success==1)
		{
			$vujade->messages[]="Scope of work updated.";
		}
		$project = $vujade->get_project($id,2);
		if($project['error']!=0)
		{
			$vujade->page_redirect('error.php?m=3');
		}
	}
	else
	{
		$vujade->page_redirect('error.php?m=1');
	}
}

// duplicate the job
if($action==3)
{
	if($projects_permissions['edit']==1)
	{
		// user set up selections
		$scope=$_REQUEST['scope'];
		$designs=$_REQUEST['designs'];
		$estimates=$_REQUEST['estimates'];
		$proposals=$_REQUEST['proposals'];
		$selections = array('scope' => $scope, 'designs' => $designs, 'estimates' => $estimates, 'proposals' => $proposals);
		$s=$vujade->duplicate_project($project,$selections);
		if( ($s!=0) && ($scope=='') )
		{
			$vujade->page_redirect('edit_project_scope.php?id='.$s);
		}
		if( ($s!=0) && ($scope==1) )
		{
			$vujade->page_redirect('project.php?id='.$s);
		}
	}
	else
	{
		$vujade->page_redirect('error.php?m=1');
	}
}

// shop order
$shop_order = $vujade->get_shop_order($id, 'project_id');

// show link to vendor map
$vendor_map_search=true;

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$menu=1;
$title = $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left 
        <aside class="tray /*tray-left*/ tray320 p20">
		-->
        <?php 
        require_once('project_left_tray.php'); 
		//$step2 = microtime(true);
		//$tarray['step2']=$vujade->script_start_time-$step2;
        ?>

        <!-- end: .tray-left </aside>-->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">

            <div class="pl15 pr15" style = "width:100%;">

            	<?php 
            	require_once('project_right_tray.php'); 
				//$step3 = microtime(true);
				//$tarray['step3']=$vujade->script_start_time-$step3;
            	?>

            	<!-- main content for this page -->
				<div class = "well">
					<strong>Scope of Work:</strong><br>
					<form method = "post" action = "project.php">
					<textarea name = "description" id = "description" class = "ckeditor">
                      	<?php print ltrim($project['description']); ?>
					</textarea>
					<br>
					<input type = "hidden" name = "id" value = "<?php print $id; ?>">
					<input type = "hidden" name = "action" value = "2">
					<input type = "submit" value = "Update Scope Of Work" class = "btn btn-primary btn-sm" style = "">
					</form>
					<?php 
					require_once('ckeditor.php'); 
					// timed for step 1
					//$step4 = microtime(true);
					//$tarray['step4']=$vujade->script_start_time-$step4;
					?>
				</div>

            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- Load Google Map API -->
  <script src="https://maps.google.com/maps/api/js?sensor=true"></script>
  
  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Bootstrap -->
  <script src="vendor/plugins/map/gmaps.min.js"></script>
  <script src="vendor/plugins/gmap/jquery.ui.map.min.js"></script>
  <script src="vendor/plugins/gmap/ui/jquery.ui.map.extensions.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    var n = $('#notes').html();
    $("#notes").html($.trim(n));
    var d = $('#description').html();
    $("#description").html($.trim(d));

    /* */
	var street = "<?php print $project['address_1']; ?>";
	var city = "<?php print $project['city']; ?>";
	var state = "<?php print $project['state']; ?>";
	var address = street+","+city+","+state;
	var url = "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&sensor=true";

    var j = $.getJSON( url, function( data ) 
    {
    	var location = data.results[0].geometry.location;
	  	var lat = location.lat;
	  	var lng = location.lng
		//$('#test').html(lat+', '+lng);
		map = new GMaps({
        div: '#map',
        lat: lat,
        lng: lng,
      	});

		map.addMarker({
		  lat: lat,
		  lng:  lng,
		  title: address,
		  click: function(e) {
		    //alert('You clicked in this marker');
		  }
		});

	});

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

<?php $vujade->script_stop_time(); ?>

</body>

</html>
