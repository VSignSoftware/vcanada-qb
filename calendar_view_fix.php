<?php
session_start();
//error_reporting(E_ALL);
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['eventid'];
$view = $_SESSION['view'];
$start = $_SESSION['start'];
$event = $vujade->get_event_data($id);

// calendar apparently has some kind of bug where the start of 
// the month is off 
if(empty($view))
{
	$view="month";
}
if($view=="month")
{
	unset($_SESSION['start']);
	unset($_SESSION['view']);
	// Mon Jun 30 2014 00:00:00 GMT+0000
	// 2014-12-18
	$_SESSION['start']="";
	$new_start = explode('-',$event['start']);
	$d = $new_start[2];
	$m = $new_start[1];
	$y = $new_start[0];
	//print date('M',$m);
	//$_SESSION['start']=" ".$m." ".$d." ".$y." 00:00:00 GMT+0000";
	header('location:calendar.php?view=month&start=Mon '.$m.' '.$d.' '.$y);
	die;
}
else
{
	header('location:calendar.php');
	die;
}

//print $start .'<br>';
//print 'view: '.$view;
//print '<br>event: <br>';
//print_r($event);

?>
