<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$poid = $_REQUEST['poid'];
$po = $vujade->get_purchase_order($poid);
if($po['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}

// get vendor
// attempt to get by list id
$vendor1 = $vujade->get_vendor($po['vendor_id'],'ListID');
if($vendor1['error']=='0')
{	
	$vendor = $vendor1;
}
else
{
	// get by id column
	$vendor2 = $vujade->get_vendor($po['vendor_id']);
	if($vendor2['error']=='0')
	{	
		$vendor = $vendor2;
	}
}

$vendor_contact = $vujade->get_vendor_contact($po['vendor_contact_id']);

if($po['type']=="Outsource")
{
    $terms = $vujade->get_terms(5);
}
if($po['type']=="Subcontract")
{
    $terms = $vujade->get_terms(6);
}

$project = $vujade->get_project($po['project_id'],2);
if($project['error']!="0")
{
    //$vujade->page_redirect('error.php?m=3');
}

$logo['url']="images/invoice_logo.jpg";
$total = 0;
$setup = $vujade->get_invoice_company_setup(3);

# html
$charset="ISO-8859-1";
$html = '<!DOCTYPE html>
    <head>
    <meta charset="'.$charset.'">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Purchase Order</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/print_po_style.css">
    </head>
    <body>';

# line items 
$html.='<div class = "row">';

if($po['type']=="Materials")
{

    //$poids = $vujade->get_po_project_ids($po['purchase_order_id']);

    $html.='<div class = "column_75_border_all" style = "width:92px">Item #:</div>';        
    $html.='<div class = "column_100_border_all" style = "width:266px">Description:</div>';
    $html.='<div class = "column_100_border_all_centered" style = "width:92px">Account:</div>';
    //$html.='<div class = "column_75_border_all_centered" style = "width:92px">Job No.:</div>';
    $html.='<div class = "column_75_border_all_centered" style = "width:92px">Qty:</div>';
    $html.='<div class = "column_75_border_all_centered" style = "width:92px">Unit Price:</div>';
    $html.='<div class = "column_100_border_all_centered" style = "width:92px;">Line Total:</div>';
    $materials = $vujade->get_materials_for_purchase_order($poid);
    unset($materials['error']);
    foreach($materials as $item)
    {
        $html.='<div class = "column_75_border_all" style = "width:92px">'.$item['inventory_id'].'</div>';
        $html.='<div class = "column_100_border_all" style = "width:266px">'.nl2br($item['description']).'</div>';
        $html.='<div class = "column_100_border_all_centered" style = "width:92px">';
        if(!empty($item['account']))
        {
            $html.=$item['account'].'<br>';
        }
        if(!empty($item['project_id']))
        {
            $html.=$item['project_id'];
        }
        $html.='</div>';
        $html.='<div class = "column_75_border_all_centered" style = "width:92px">'.$item['qty'].'</div>';
        $html.='<div class = "column_75_border_all_centered" style = "width:92px">'.@number_format($item['unit_price'],2,'.',',').'</div>';
        $line_total = $item['unit_price']*$item['qty'];
        $total+=$line_total;
        $html.='<div class = "column_100_border_all_centered" style = "width:92px;">'.@number_format($line_total,2,'.',',').'</div>';
        $html.='<div class = "row_border_bottom"></div>';
    }
}
else
{
    //$outsourced = $vujade->get_outsourced_items($poid);
    //print $poid.'<br>';
    //print_r($outsourced);
    //die;
    $outsourced = $vujade->get_materials_for_purchase_order($poid);
    unset($outsourced['error']);
    $html.='<table width = "100%">';
    $html.='<tr>';
    $html.='<td valign = "top" width = "70%" style = "border:1px solid black;">Description:';
    $html.='</td>';
    $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">Date Due:';
    $html.='</td>';
    $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">Amount:';
    $html.='</td>';
    $html.='</tr>';
    foreach($outsourced as $o)
    {
        if(empty($o['date_required']))
        {
            $o['date_required']='&nbsp;';
        }
        if(empty($o['amount']))
        {
            $o['amount']='0';
        }
        $html.='<tr>';
        $html.='<td valign = "top" width = "70%" style = "border:1px solid black;">'.$o['description'];
        $html.='</td>';
        $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">'.$o['date_required'];
        $html.='</td>';
        $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">$'.number_format($o['amount'],2,'.',',');
        $html.='</td>';
        $html.='</tr>';
        $total+=$o['amount'];
    }
    $html.='</table>';
}

$html.='<div class = "total_row">';
$total+=$po['tax_amount'];
$html.='Tax: $'.@number_format($po['tax_amount'],2,'.',',');
$html.='<br>Total: $'.@number_format($total,2,'.',',');
$html.='</div>';

$html.='</div>';

# spacer 
$html.='<div class = "spacer_full_width"></div>';

# terms
if($po['type']!="Materials")
{
    $html.='<div class = "terms_header">Terms:</div>';
    $html.= str_replace('<p>', '', $terms['terms']);
}

# end html
$html.='</body></html>';

# repeating header
$header_1 = '<div class = "header" style = "border-bottom:4px solid black;border-top:0px;border-right:0px;border-left:0px;">';
$header_1 .= '<div class = "header_padded_inner">';

# logo
$header_1 .= '<div class = "header_left" style = "margin-right:30px;margin-bottom:15px;">';
$header_1 .= '<img src = "'.$logo['url'].'" width = "250" height = "67"><br>';
$header_1 .= '</div>';

# h1 and type
$header_1 .= '<div style = "margin-left:15px;float:left;">';

$header_1 .= '<div style = "width:100%;">';

$header_1 .= '<div style = "float:left;width:320px;">';
$header_1 .= '<h1>Purchase Order';
$header_1 .= '</h1>';
$header_1 .= '</div>';

$header_1 .= '<div style = "float:right;width:100px;">';
$header_1 .= '<h1>'.$po['purchase_order_id'].'</h1>';
$header_1 .= '</div>';

$header_1 .= '</div>';

$header_1 .= '<div style = "width:435px;text-align:right;">';
$header_1 .= $po['date'];
$header_1 .= '</div>';

$header_1 .= '</div>';

$header_1 .= '<div style = "width:100%;">';
$header_1 .= '<div style = "width:269px;float:left;">';
$compinfo = $setup['address_1'];
if(!empty($setup['address_2']))
{
    $compinfo .= ", ".$setup['address_2'];
}
$compinfo .= '<br>'.$setup['city'].', '.$setup['state'].' '.$setup['zip'];
$header_1 .=$compinfo;
$header_1 .= '</div>';

$header_1 .= '<div style = "width:210px;float:left;">';
$header_1 .=$setup['phone'];
$header_1 .= '</div>';
$header_1 .= '</div>';

$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "header_left">';
$header_1 .= '<div style = "float:left;width:125px;">Project #:</div><div style = "float:left;">'.$project['project_id'];
$header_1 .= '</div>';
$header_1 .= '<div style = "float:left;width:125px;">Project Name:</div><div style = "float:left;">'.$project['site'];
$header_1 .= '</div>';
$header_1 .= '<div style = "float:left;width:125px;">Design #:</div><div style = "float:left;">'.$shop_order['design'];
$header_1 .= '</div>';
$header_1 .= '</div>';

$header_1 .= '<div class = "header_middle" style = "width:210px;">';
$header_1 .= '<strong>To:</strong><br><div style = "margin-left:30px;">';
$header_1 .= $vendor['name'].'<br>';
$header_1 .= $vendor['address_1'];
if(!empty($vendor['address_2']))
{
    $header_1 .= ', '.$vendor['address_2'];
}
$header_1 .= '<br>'.$vendor['city'].', '.$vendor['state'].' '.$vendor['zip'].'';
$header_1 .= '<br>';
$header_1 .= 'Contact: '.$vendor_contact['fullname'];
$header_1 .= '<br>';
// limit size of phone number 
$phone = $vujade->limit_chars($vendor_contact['phone1'],12);
$header_1 .= 'Phone: '.$phone;

$header_1 .= '</div></div>';

//$header_1 .= '<div style = "width:210px;margin-left:10px;float:left;">';
//$header_1 .= '<strong>Job Address:</strong><br><div style = "margin-left:30px;">';
//$header_1 .= '</div></div>';

$header_1 .= '<div style = "width:210px;margin-left:10px;float:left;">';
$header_1 .= '<strong>Job Address:</strong><br><div style = "margin-left:30px;">';
$header_1 .= $po['company'].'<br>';
$header_1 .= $po['address_1'];
if(!empty($po['address_2']))
{
    $header_1 .= ', '.$po['address_2'];
}
$header_1 .= '<br>'.$po['city'].', '.$po['state'].' '.$po['zip'].'';
$header_1 .= '</div></div>';

$header_1 .= '</div>';

$header_1 .= '</div>'; // end of header padded inner
$header_1 .= '</div>'; // end of header div

# repeating footer
$footer_1 = '<div class = "footer">';

$footer_1.= '<div class = "footer_col" style = "border:none;">
'.$po['ordered_by'].'</div></div>';

$footer_1.= '<div class = "footer_row">';
$footer_1.= '<div class = "footer_col"> Ordered By:</div><div class = "spacer_30px"></div>';
$footer_1.= '<div class = "footer_col"> Authorized Signature:</div><div class = "spacer_30px"></div>';
$footer_1.= '<div class = "footer_col"> Acceptance Signature:</div>';
$footer_1.='</div>';
$footer_1.='Page {PAGENO}';
$footer_1.='</div>';

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 85, 30, 10);

//new mPDF($mode, $format, $font_size, $font, $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);

//$mpdf->setAutoTopMargin = 'stretch';
//$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($html);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-blank-purchase-order.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output(); 
}
?>