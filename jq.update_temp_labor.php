<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$debug=0;

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$s=array();

$pid = $_REQUEST['pid'];
$labor_id = $_REQUEST['labor_id'];
//$estimate_id = $_REQUEST['estimate_id'];
$checked = $_REQUEST['checked'];

$shop_order=$vujade->get_shop_order($pid,'project_id');
if($shop_order['estimates']!='')
{
	$estimates=$shop_order['estimates'];
	$estimates = explode('^',$estimates);
	foreach($estimates as $k => $v)
	{
		if(empty($v))
		{
			unset($estimates[$k]);
		}
	}
	$est_cnt = count($estimates);
}

// checked = 0
// user unchecked something
if($checked==0)
{
	// delete any rows in the temp_estimate_time table for this department and this estimate
	if($est_cnt>0)
	{	
		foreach($estimates as $estimate)
		{
			$q="delete from temp_estimates_time where estimate_id = '$estimate' and labor_id = '$labor_id'";
			$qr = $vujade->generic_query($q,0,2);
		}
	}

	// set the skip / override to 1
	if($est_cnt>0)
	{	
		foreach($estimates as $estimate)
		{
			$est_data = $vujade->get_estimate($estimate,'estimate_id');
			$eid = $est_data['database_id'];
			$q2="update estimates_time set work_dept_hours_report_skipped = 1 where estimate_id = '$estimate' and labor_id = '$labor_id'";
			$qr2 = $vujade->generic_query($q2,0,2);
		}
	}
	if($debug==0)
	{
		// output 1 (no errors)
		print '1';
	}
	else
	{
		// print request array
		print_r($_REQUEST);
		print '<br>'.$q;
		print '<br>'.$q2;
	}
}

// checked = 1
// user checked something
// add temp reference for this labor type and estimate (hours will be zero)
if($checked==1)
{

	// delete any references in temp_estimates_time for this estimate and this labor type
	if($est_cnt>0)
	{	
		foreach($estimates as $estimate)
		{
			$q="delete from temp_estimates_time where estimate_id = '$estimate_id' and labor_id = '$labor_id'";
			$qr = $vujade->generic_query($q,0,2);
		}
	}
	
	// attempt to set the estimates time skip to 0 so this reappears on the report
	if($est_cnt>0)
	{	
		foreach($estimates as $estimate)
		{
			$est_data = $vujade->get_estimate($estimate,'estimate_id');
			$eid = $est_data['database_id'];
			//$hours = $est_data['hours'];

			$q2="update estimates_time set work_dept_hours_report_skipped = '0' where estimate_id = '$estimate' and labor_id = '$labor_id'";
			$qr2 = $vujade->generic_query($q2,0,2);
			
			$q3="insert into temp_estimates_time (estimate_id,project_id,labor_id) values('$estimate','$pid','$labor_id')";
			$qr3 = $vujade->generic_query($q3,0,2);
		}
	}

	if($debug==0)
	{
		// output 1 (no errors)
		print '1';
	}
	else
	{
		// print request array
		print_r($_REQUEST);
		print '<br>'.$q;
		print '<br>'.$q2;
		print '<br>'.$q3;
		print '<br>'.$qr3;
	}
}
?>