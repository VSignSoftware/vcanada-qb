<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

$title = "Payroll Hourly Report";

$pateHTML = '';
$pateHTML .= '
<!DOCTYPE html>
	<head>
	<meta charset="'.$charset.'">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>'.$title.'</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<style>
		.bordered
		{
		    padding-bottom:3px;
		    border-bottom: 1px solid #cecece;
		}
		.header
		{
		    font-weight: bold;
		    font-size: 14px;
		}
		.header_graybg
		{
		    background-color: #cecece;
		    font-weight: bold;
		    font-size: 14px;
		    padding-bottom:3px;
		    border-bottom: 1px solid #cecece;
		}
		
		
		table {font-family: DejaVuSansCondensed; font-size: 11pt; line-height: 1.2;
			margin-top: 2pt; margin-bottom: 5pt;
			border-collapse: collapse;  }

		thead {	font-weight: bold; vertical-align: bottom; }
		tfoot {	font-weight: bold; vertical-align: top; }
		thead td { font-weight: bold; }
		tfoot td { font-weight: bold; }

		thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }

		.headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
		.footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }

		th {	font-weight: bold; 
			vertical-align: top; 
			text-align:left; 
			padding-left: 2mm; 
			padding-right: 2mm; 
			padding-top: 0.5mm; 
			padding-bottom: 0.5mm; 
		 }

		td {	padding-left: 2mm; 
			vertical-align: top; 
			text-align:left; 
			padding-right: 2mm; 
			padding-top: 0.5mm; 
			padding-bottom: 0.5mm;
		 }

		th p { text-align: left; margin:0pt;  }
		td p { text-align: left; margin:0pt;  }
		
		.button-container-style{
			margin-top: 20px; 
			padding-top:10px; 
			border-top: 1px #ccc solid;
		}
		
		</style>
		
</head>
<body>
	<div id="mainContent">
		<div id="content">
			<br style="clear: both;"/>
			<h2 class="signInText">'.$title.'</h2>
			<div style = "background-color:#989898;width:100%">
				<div style = "padding:5px;">
					Hourly Payroll Report for Dates '.$date1.' to '.$date2.'  
				</div>
			</div>
';
 
# display data for this date range
if($action==1)
{
	$pateHTML .= '
    <table width = "100%" cellspacing="0" cellpading="0" border="0" style="border-style: none">
    <tr class = "header_graybg">
        <td width="15%">Employee</td>
        <td width="10%">Job No</td>
        <td width="6%">S.T.</td>
        <td width="10%">Rate</td>
        <td width="6%">O.T.</td>
        <td width="10%">Rate</td>
        <td width="6%">D.T.</td>
        <td width="10%">Rate</td>
        <td width="6%">Vac</td>
        <td width="6%">Hol</td>
        <td width="12%">Total Paid</td>
    </tr>';

	$pateHTML .= '<tr>';
    $pateHTML .= '<td colspan = "11">&nbsp;</td>';
    $pateHTML .= '</tr>';
	
	# get report for the time range
    $date1 = $_REQUEST['date1'];
    $date1ts = strtotime($date1);
    $date2 = $_REQUEST['date2'];
    $date2ts = strtotime($date2);

    $ids = $vujade->get_employees(1);
    
    if($ids['error']=="0")
    {
        unset($ids['error']);
        foreach($ids as $id)
        {
            $eid = $id['database_id'];
            $fullname = $id['fullname'];
            $rate_data = $vujade->get_employee_current_rate($eid);
            $rate=$rate_data['rate'];
            $pateHTML .= $vujade->get_timecards_str($date1ts,$date2ts,$eid,$fullname,$rate);
        }
        # grand total line
        
        $pateHTML .= '
        <tr class = "header_graybg">
            <td colspan = "2">&nbsp;</td>
            <td>'.number_format($vujade->grand_total_st,2,'.',',').'</td>
            <td>&nbsp;</td>
            <td>'.number_format($vujade->grand_total_ot,2,'.',',').'</td>
            <td>&nbsp;</td>
            <td>'.number_format($vujade->grand_total_dt,2,'.',',').'</td>
            <td>&nbsp;</td>
            <td>'.number_format($vujade->grand_total_vacation,2,'.',',').'</td>
            <td>'.number_format($vujade->grand_total_holiday,2,'.',',').'</td>
            <td>'.number_format($vujade->grand_total_paid,2,'.',',').'</td>
        </tr>
        </table>
       ';
        
    }
}

$pateHTML .= '
			<div class="button-container-style">&nbsp;</div>	
			</div>
		</div>
	</body>
</html>';

 