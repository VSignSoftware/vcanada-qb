<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();

$logo = $vujade->get_logo();
if(!isset($charset))
{
	 $charset = "utf-8";
}

$setup = $vujade->get_setup();

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
if($action==1)
{
  	$username = $_POST['username'];
  	$password = $_POST['password'];
  	$vujade->check_empty($username,'Username');
  	$vujade->check_empty($password,'Password');
  	$e = $vujade->get_error_count();
  	if($e<=0)
  	{
  		$vujade->login($username,$password);
  	}
}
?>

<!DOCTYPE html>
<html>

<head>
  <!-- Meta, title, CSS, favicons, etc. -->
  <meta charset="utf-8">
  <title><?php print $setup['company_name']; ?> - Login</title>
  <meta name="keywords" content="V Sign Software" />
  <meta name="description" content="V Sign Software">
  <meta name="author" content="V Sign Software">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <!-- Font CSS (Via CDN) -->
  <link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>

  <!-- Theme CSS -->
  <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">

  <!-- Admin Forms CSS -->
  <link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">

  <!-- Favicon -->
  <link rel="shortcut icon" href="assets/img/favicon.ico">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
   <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
   <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
   <![endif]-->
</head>

<body class="external-page sb-l-c sb-r-c">

  <!-- Start: Main -->
  <div id="main" class="animated fadeIn">

    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- begin canvas animation bg -->
      <div id="canvas-wrapper">
        <canvas id="demo-canvas"></canvas>
      </div>

      <!-- Begin: Content -->
      <section id="content">
        <div class="admin-form theme-info" id="login1" style = "max-width: 400px;">

          <div class="panel panel-info mt10 br-n">

            <div class="panel-heading heading-border bg-white">
              <div class="section row mn">
                <div class="col-sm-12">
                	  <?php 
                	  /*
			          if(file_exists($logo['url']))
			          {
			              print '<img src = "'.$logo['url'].'" width = "150" class="img-rounded" style = "">';
			          }
			          else
			          {
			              print '<h4>'.$setup['company_name'].'<h4>';
			          } 
			          */
			          ?>
			          <img src = "images/v_sign_logo.png" width = "150" class="img-rounded" style = "">
                </div>
              </div>
            </div>

            <!-- end .form-header section -->
            <form method="post" action="login.php" id="form">
              <input type = "hidden" name = "action" value = "1">
              <div class="panel-body bg-light p30">
                <div class="row">
                  <div class="col-sm-12 pr30">

					<div id = "errors" style = "display:none;" class="alert alert-danger">
					 
					</div>

					<?php
					if(isset($_GET['m']))
					{
						$m=$_GET['m'];
					}
					if($m==1)
					{
						print '<div class = "alert alert-success">You are logged out.</div>';
					}
					if($m==2)
					{
						print '<div class = "alert alert-danger">You must be logged in to access that page.</div>';
					}
					$vujade->show_errors();
					?>

                    <div class="section">
                      <label for="username" class="field-label text-muted fs18 mb10">Username</label>
                      <label for="username" class="field prepend-icon">
                        <input type="text" name="username" id="username" class="gui-input" placeholder="Enter username">
                        <label for="username" class="field-icon">
                          <i class="fa fa-user"></i>
                        </label>
                      </label>
                    </div>
                    <!-- end section -->

                    <div class="section">
                      <label for="username" class="field-label text-muted fs18 mb10">Password</label>
                      <label for="password" class="field prepend-icon">
                        <input type="password" name="password" id="password" class="gui-input" placeholder="Enter password">
                        <label for="password" class="field-icon">
                          <i class="fa fa-lock"></i>
                        </label>
                      </label>
                    </div>
                    <!-- end section -->
                  </div>
                </div>
              </div>
              <!-- end .form-body section -->

              <div class="panel-footer clearfix p10 ph15">
                <button id = "sbt" type="submit" class="button btn-primary ml15 mr10 center-block">Sign In</button>

                <!--
                <a href = "http://vsignsoftware.com/" target = "_blank"><img src = "images/footer_logo.jpg" width = "100" class="img-rounded center-block pull-right mr15"></a>
                -->

              </div>
              <!-- end .form-footer section -->
            </form>

          </div>
        </div>

      </section>
      <!-- End: Content -->

    </section>
    <!-- End: Content-Wrapper -->

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- CanvasBG Plugin(creates mousehover effect) -->
  <script src="vendor/plugins/canvasbg/canvasbg.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>

  <!-- Page Javascript -->
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core      
    Core.init();

    // Init Demo JS
    // Demo.init();

    // Init CanvasBG and pass target starting location
    CanvasBG.init({
      Loc: {
        x: window.innerWidth / 2,
        y: window.innerHeight / 3.3
      },
    });

    $('#sbt').click(function(e)
    {
    	e.preventDefault();
    	$('#username').css('border','');
    	$('#password').css('border','');

    	$('#errors').hide();
    	var errors = 0;

    	var u = $('#username').val();
    	var p = $('#password').val();

    	if(u=="")
    	{
    		errors++;
    		$('#errors').append('<i class="fa fa-warning pr10"></i> Username is missing.<br>');
    		$('#errors').show();
    	}
    	if(p=="")
    	{
    		errors++;
    		$('#errors').append('<i class="fa fa-warning pr10"></i> Password is missing.<br>');
    		$('#errors').show();
    	}
    	if(errors==0)
    	{
    		$('#form').submit();
    	}
    });

  });
  </script>

  <!-- END: PAGE SCRIPTS -->

</body>

</html>