<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

$title = "Closed Projects Report - ";
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $title; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "reports.php" class = "btn btn-primary btn-sm">&laquo; Back</a>
					</div>
				</div>

				<style>
				.dp:hover
				{
					cursor: pointer;
				}
				</style>

	        	<div class="panel-body bg-light">
					<div class = "row">
						<div class = "col-md-12">
							<form>
								<div class="row">
									<div class="col-md-6">
								From: <input class = "dp form-control" type = "text" id = "date1" name = "date1" readonly>
								</div><div class="col-md-6">
								 To:  <input class = "dp form-control" type = "text" id = "date2" name = "date2" readonly></div>
								</div><br/>
								<input type = "hidden" name = "action" value = "1">
								<input type = "submit" id = "run" value = "Run Report" class = "btn btn-primary btn-xs">
							</form>

							</div>

							<div class = "push">
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    $('.dp').datepicker();

    $('#run').click(function(e)
    {
    	e.preventDefault();
    	var date1 = $('#date1').val();
    	var date2 = $('#date2').val();
    	window.open('print_report_closed_jobs.php?date1='+date1+'&date2='+date2, '_blank');
    });

    // hides keyboard on ios; not needed since only two inputs are calendar widgets
    var hideKeyboard = function() 
    {
	    document.activeElement.blur();
	    $("input").blur();
	};

});
</script>

</body>
</html>