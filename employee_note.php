<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// employee id
$employee_id=$_REQUEST['employee_id'];
$employee = $vujade->get_employee($employee_id);

// note id 
if(isset($_REQUEST['note_id']))
{
	$note_id=$_REQUEST['note_id'];
	$note = $vujade->get_employee_note($note_id);
	if($note['error']!="0")
	{
		$vujade->page_redirect('error.php?m=3');
	}
}

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
# add note
if($action==1)
{
	$date = $_REQUEST['date'];
	$msg = $_REQUEST['msg'];
	$s = array();
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$vujade->create_row('employee_notes');
	$s[]=$vujade->update_row('employee_notes',$vujade->row_id,'employee_id',$employee_id);
	$s[]=$vujade->update_row('employee_notes',$vujade->row_id,'date',$date);
	$s[]=$vujade->update_row('employee_notes',$vujade->row_id,'note',$msg);
	$s[]=$vujade->update_row('employee_notes',$vujade->row_id,'entered_by',$_SESSION['user_id']);
	$s[]=$vujade->update_row('employee_notes',$vujade->row_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('employee_notes',$vujade->row_id,'created_by_name',$employee['fullname']);
	$vujade->page_redirect('employee.php?tab=3&id='.$employee_id.'&note_id='.$vujade->row_id);
}

// save note
if($action==2)
{
	$date = $_REQUEST['date'];
	$msg = $_REQUEST['msg'];
	$s = array();
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$s[]=$vujade->update_row('employee_notes',$note_id,'employee_id',$employee_id);
	$s[]=$vujade->update_row('employee_notes',$note_id,'date',$date);
	$s[]=$vujade->update_row('employee_notes',$note_id,'note',$msg);
	$s[]=$vujade->update_row('employee_notes',$note_id,'entered_by',$_SESSION['user_id']);
	$s[]=$vujade->update_row('employee_notes',$note_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('employee_notes',$note_id,'created_by_name',$employee['fullname']);
	$vujade->page_redirect('employee.php?tab=3&id='.$employee_id.'&note_id='.$note_id);
}

$user = $vujade->get_employee($_SESSION['user_id']);
$emp=$user;
$section=9;
$title = "Employee Note - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Employee Note</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                	<!-- form -->
					<form id = "form" method = "post" action = "employee_note.php">
						Date:<br>
						<input type = "text" name = "date" id = "date" class = "form-control" style = "width:150px;" value = "<?php print $note['date']; ?>">
						<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
						<br>
						<br>
						Message:<br>
						<textarea name = "msg" id = "msg" style = "width:100%;height:200px;" class = "form-control ckeditor">
						<?php print $note['note']; ?>	
						</textarea>
						<div id = "error_3" style = "display:none;">This field cannot be empty.</div>
						<br>
						<br>
						<?php
						if(!isset($note_id))
						{
							print '<input type = "hidden" name = "action" value = "1">';
						}
						else
						{
							print '<input type = "hidden" name = "action" value = "2">';
							print '<input type = "hidden" name = "note_id" value = "'.$note_id.'">';
						}
						?>
						
						<input type = "hidden" name = "employee_id" value = "<?php print $employee_id; ?>">
						<input type = "submit" id = "save" value = "SAVE" class = "btn btn-primary"> 
						<a href = "#" id = "cancel" class = "btn btn-primary">CANCEL</a>
					</form>

<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    // date picker
	$('#date').datepicker();

	// fix for text areas
	var n = $('#msg').html();
    $("#msg").html($.trim(n));

    $('#save').click(function(e)
    {
    	e.preventDefault();
	
		var date = $('#date').val();
		var message = $('#msg').text();
		var error = 0;
		if(date=="")
		{	
			$('#error_1').css('background-color','#C60F13');
			$('#error_1').css('color','white');
			$('#error_1').css('font-weight','bold');
			$('#error_1').css('font-size','16px');
			$('#error_1').css('padding','3px');
			$('#error_1').show();
			error++;
		}
		else
		{
			$('#error_1').hide();
		}
		if(error==0)
		{
			$('#form').submit();
		}

	});

    $('#cancel').click(function(e)
    {
    	e.preventDefault();

    	window.location="employee.php?tab=3&id=<?php print $employee_id; ?>";
    });
   
});
</script>

</body>
</html>