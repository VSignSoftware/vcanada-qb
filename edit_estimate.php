<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// full access permissions
$estimates_fa_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Full Access');

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$estimate_database_id = $_REQUEST['estimateid'];

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$estimate = $vujade->get_estimate($estimate_database_id);
if($estimate['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# save/update the estimate
if($action==1)
{

	// full access permissions
	if($estimates_fa_permissions['read']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}

	$project_id=$_POST['project_id'];
	$design=$_POST['design'];
	$open_date=$_POST['open_date'];
	$status=$_POST['status'];
	$required_date=$_POST['required_date'];
	$description=$_POST['description'];
	$notes=$_POST['notes'];
	$estimated_by=$_POST['estimated_by'];
	$approved_by=$_POST['approved_by'];
	$tier = $_POST['commission_tier'];

	$row_id = $estimate_database_id;
	$s = array();

	$s[] = $vujade->update_row('estimates',$row_id,'design_id',$design);
	$s[] = $vujade->update_row('estimates',$row_id,'date',$open_date);

	$s[] = $vujade->update_row('estimates',$row_id,'created_by',$estimated_by);
	$s[] = $vujade->update_row('estimates',$row_id,'approved_by',$approved_by);
	$s[] = $vujade->update_row('estimates',$row_id,'description',$description);
	$s[] = $vujade->update_row('estimates',$row_id,'notes',$notes);

	$s[] = $vujade->update_row('estimates',$row_id,'required_date',$required_date);
	$s[] = $vujade->update_row('estimates',$row_id,'commission_level',$tier);
	$s[] = $vujade->update_row('estimates',$row_id,'status',$status);
	$s[] = $vujade->update_row('estimates',$row_id,'ts',strtotime($open_date));

	$vujade->page_redirect('estimate_material.php?project_id='.$project_id.'&estimateid='.$row_id);
}

# done button was pressed 
if($action==2)
{
	$project_id=$_POST['project_id'];
	$design=$_POST['design'];
	$open_date=$_POST['open_date'];
	$status=$_POST['status'];
	$required_date=$_POST['required_date'];
	$description=$_POST['description'];
	$notes=$_POST['notes'];
	$estimated_by=$_POST['estimated_by'];
	$approved_by=$_POST['approved_by'];
	$tier = $_POST['commission_tier'];

	$row_id = $estimate_database_id;
	$s = array();

	$s[] = $vujade->update_row('estimates',$row_id,'design_id',$design);
	$s[] = $vujade->update_row('estimates',$row_id,'date',$open_date);

	$s[] = $vujade->update_row('estimates',$row_id,'created_by',$estimated_by);
	$s[] = $vujade->update_row('estimates',$row_id,'approved_by',$approved_by);
	$s[] = $vujade->update_row('estimates',$row_id,'description',$description);
	$s[] = $vujade->update_row('estimates',$row_id,'notes',$notes);

	$s[] = $vujade->update_row('estimates',$row_id,'required_date',$required_date);
	$s[] = $vujade->update_row('estimates',$row_id,'commission_level',$tier);
	$s[] = $vujade->update_row('estimates',$row_id,'status',$status);
	$s[] = $vujade->update_row('estimates',$row_id,'ts',strtotime($open_date));

	$vujade->page_redirect('project_estimates.php?id='.$project_id.'&estimateid='.$row_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

// estimate templates
$templates = $vujade->get_estimates(3);

$section=3;
$menu=4;
$title = "Edit Estimate - ";
require_once('h.php');
?>
<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Estimate <?php print $estimate['estimate_id']; ?> Overview</a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

	<div class="admin-form theme-primary">

	    <?php 
	    $vujade->show_errors();
	    $vujade->show_messages();
	    ?>

	    <form method = "post" action = "edit_estimate.php" id = "nd">
		<input type = "hidden" name = "action" id = "action" value = "">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
		<input type = "hidden" value = "<?php print $estimate_database_id; ?>" name = "estimateid">
		<div class="panel heading-border panel-primary">
            <div class="panel-body bg-light">
		<table width = "100%">

		<tr>
		<td>Project</td>
		<td><?php print $project_id; ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		</tr>

		<tr>
		<td colspan = "4">&nbsp;</td>
		</tr>

		<tr>
		<td>Design</td>
		<td>
			<select name = "design" id = "design" class = "form-control" style = "width:200px;">
				<?php
				if(!empty($estimate['status']))
				{
					print '<option value = "'.$estimate['design_id'].'">'.$estimate['design_id'].'</option>';
				}
				print '<option value = "">-Select-</option>';
				$designs = $vujade->get_designs_for_project($project_id);
				if($designs['error']=='0')
				{
					unset($designs['error']);
					unset($designs['next_id']);
					foreach($designs as $design)
					{
						print '<option value = "'.$design['design_id'].'">'.$design['design_id'].'</option>';
					}
				}
				else
				{
					print '<option value = "">No designs for project.</option>';
				}
				?>
			</select>
		</td>
		<td>Date Opened
		</td>
		<td><?php print date('m/d/Y'); ?>
			<input type = "hidden" name = "open_date" id = "open_date" value = "<?php print $estimate['date']; ?>"></td>
		</td>
		</tr>

		<tr>
		<td colspan = "4">&nbsp;</td>
		</tr>

		<tr>
		<td>
		Status 
		</td>
		<td>
		<select name = "status" id = "status" class = "form-control" style = "width:200px;">
		<?php
		if(!empty($estimate['status']))
		{
			print '<option value = "'.$estimate['status'].'">'.$estimate['status'].'</option>';
		}
		?>
		<option value = "">-Select-</option>
		<option value = "Submitted">Submitted</option>
		<option value = "Complete">Complete</option>
		<option value = "In Progress">In Progress</option>
		<option value = "On Hold">On Hold</option>
		</select>
		</td>
		<td>Required</td>
		<td><input type = "text" name = "required_date" id = "dp2" class = "form-control dp" style = "width:200px;" value = "<?php print $estimate['required_date']; ?>"></td>
		</tr>

		<tr>
		<td colspan = "4">&nbsp;
		</td>
		</tr>

		<tr>
		<td colspan = "2">Description
		</td>
		<td colspan = "2">

			<!-- transfer button -->
			<a class = "btn btn-primary" id = "transfer" href = "#transfer-form" style = "margin-left:50%;" title = "Transfer from other">Transfer</a>

			<!-- transfer modal -->
			<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
				<input type = "hidden" name ="transfer_to" id = "transfer_to" value = "<?php print $estimate_database_id; ?>">
				<form id = "transfer_form" class="form-inline" style = "margin-bottom:15px;">
					<table>
						<tr>
							<td><label>Estimate Number</label></td>
							<td><input type = "text" name = "transfer_from" id = "transfer_from" class = "form-control input-sm" style = "width:230px;margin-left:5px;"></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><label>OR</label></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><label>Template</label></td>
							<td><select name = "transfer_from_template" id = "transfer_from_template" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
							<option value = "">-Select-</option>
							<?php
							if($templates['count']>0)
							{
								unset($templates['count']);
								unset($templates['error']);
								unset($templates['sql']);
								foreach($templates as $template)
								{
									print '<option value = "'.$template['estimate_id'].'">'.$template['template_name'].'</option>';
								}
							}
							?>
						</select></td>
						</tr>

						<tr>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td><label>Include Description?</label></td>
							<td>
								<select name = "copy_description" id = "copy_description" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
									<option value = "">-Select-</option>
									<option value = "1">Yes</option>
									<option value = "">No</option>
								</select>
						</td>
						</tr>

					</table>

					<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
					
					<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please enter an estimate number.</div>
					<div id = "error_2" style = "display:none;" class = "alert alert-danger">This estimate number is invalid.</div>

					<div class = "" style = "width:100%;margin-top:15px;">
						<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a> <a id = "transfer_btn" href = "#" class = "btn btn-lg btn-success" style = "margin-right:15px;">SAVE</a> 
					</div>
				</form>
			</div>

			<a href "#" id = "pastescope" class = "btn btn-primary" style = "float:right;text-align:center;">Insert Project Description</a>
		</td>
		</tr>

		<tr>
		<td colspan = "4">
		<textarea name = "description" id = "description" class ="ckeditor"><?php print $estimate['description']; ?></textarea>
			<!-- ckeditor new version 4.5x -->
			<?php require_once('ckeditor.php'); ?>

		</td>
		</tr>

		<tr>
		<td colspan = "4">
		&nbsp;
		</td>
		</tr>

		<tr>
		<td colspan = "2" style = "padding-right:15px;">Estimated By
		<input type = "text" name = "estimated_by" id = "estimated_by" class = "form-control" value = "<?php print $estimate['created_by']; ?>">
		</td>
		<td colspan = "2" style = "padding-right:15px;">Approved By 
		<input type = "text" name = "approved_by" id = "approved_by" class = "form-control" value = "<?php print $estimate['approved_by']; ?>">
		</td>
		</tr>

		</table>

		<div style = "margin-top:15px;">
          <div style = "float:left;">

			<?php
			if($estimates_fa_permissions['read']==1)
			{
				?>
				<input type = "submit" class = "btn btn-success" value = "SAVE AND CONTINUE" id = "next"> 
			<?php
			}
			?>
			<a class = "btn btn-success" href = "#" id = "done" style = "margin-left:15px;">SAVE AND COMPLETE</a> 
			</form>
			</div>  
        </div>

		</div>
		</div>
	</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  //var estimateid = <?php print $estimateid; ?>;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

	$("#dp1").datepicker();
	$("#dp2").datepicker();

	$('#pastescope').click(function(e)
	{
		e.preventDefault();
		var desc = $.trim($('#hidden_desc').html());
		CKEDITOR.instances.description.setData(desc);
	});

	$('#next').click(function()
	{
		$('#action').val('1');
		$('#nd').submit();
	});

	$('#done').click(function()
	{
		$('#action').val('2');
		$('#nd').submit();
	});
	
	// modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	// modal: transfer estimate
    $('#transfer').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,		
	});
	
    // transfer button in the modal 
    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();
	
    	$('#working').hide();
    	$('#error_1').hide();
    	$('#error_2').hide();

    	var transfer_to = $('#transfer_to').val();
		var transfer_from = $('#transfer_from').val();
		var transfer_from_template = $('#transfer_from_template').val();
		var copy_description = $('#copy_description').val();
		var id = "<?php print $id; ?>";
		//var old_description = $('#description').val();
		
		var old_description = CKEDITOR.instances['description'].getData();
		//alert(old_description);

		//alert(transfer_from);

		var error = 0;
		if(transfer_from=="")
		{	
			//$('#error_1').show();
			error++;
			//return false;
		}
		if(transfer_from_template=="")
		{	
			//$('#error_1').show();
			error++;
			//return false;
		}
		if(error>1)
		{
			$('#error_1').show();
			return false;
		}
		else
		{
			if(transfer_from=='')
			{
				transfer_from=transfer_from_template;
			}
			$('#error_1').hide();
			$('#working').show();
			$('#working').html('Working...');
			$.post( "jq.transfer_estimate.php", { transfer_from: transfer_from, transfer_to: transfer_to, project_id: id, alt_redirect: 1, copy_description: copy_description, old_description: old_description })
			.done(function( response ) 
			{
				$('#working').show();
			    $('#working').html('');
			    $('#working').html(response);
			    if(response!="Invalid estimate.")
			    {
			    	$('#working').html('Success! This page will reload in 3 seconds...');

			    	var href = response;

			    	setTimeout(function(){ window.location.href=href; }, 3000);

			    	//$('#working').html('');
			    	//window.location="project_estimates.php?id=<?php print $project_id; ?>&estimateid=<?php print $estimateid; ?>";
			    }
			    else
			    {
			    	$('#error_2').html(response);
			    	$('#error_2').show();
					$('#working').hide();
			    }
			});
		}
	});

	// press return on estimate transfer form
    $('#transfer_form').on('keypress', function(e) 
    {
		if (e.which == 13) 
		{
			//$('#form').submit();
			$('#transfer_btn').click();
			return false;
		}
	});

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
