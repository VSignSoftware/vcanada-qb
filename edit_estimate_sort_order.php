<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$es_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Sort');
if($es_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=0;
$title = "Edit Estimate Sort Order - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Edit Estimate Sort Order</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">
                  
                <p>Click on an estimate and drag it to resort it to a different position.
			    </p>

			    <p id = "success"></p>

			    <div class="panel panel-widget task-widget">
                  <div class="panel-body pn" style = "height:800px;overflow:auto;">
                    <ul class="task-list task-current">
                      <?php
				      $estimates = $vujade->get_estimates($sort=2,$start=0,$status='Submitted');
				      if($estimates['error']==0)
				      {
				            unset($estimates['error']);
				            $x = 1;
				            foreach($estimates as $d)
				            {
				                if(!empty($d['estimate_id']))
				                {
				                    $project_id = explode('-',$d['estimate_id']);
				                    $pid = $project_id[0];
				                    $project = $vujade->get_project($project_id[0],2);

				                    ?>
				                    <li class="task-item primary" id = "<?php print $x.'_'.$d['database_id']; ?>">
				                        <div class="task-handle">
				                          <div class="checkbox-custom">
				                            &nbsp;
				                          </div>
				                        </div>
				                        <div class="task-desc">
				                        <?php
				                        print $x.') ';
					                    print $d['estimate_id'].' -- '.$d['date'].' -- '.$project['site'];
					                    ?>
				                        </div>
				                        <div class="task-menu"></div>
				                    </li>

				                    <?php
				                    $x++;
				                }
				            }
				        }
				        else
				        {
				            print $estimates['error'];
				        }
				        ?>
                    </ul>
                  </div>
                </div>

                <p><a href = "dashboard.php?show=3" class = "btn btn-lg btn-success">Done Editing</a></p>

                </div>
              </div>
            </div>
	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    // sortable estimates
    var taskWidget = $('.task-widget');
    var taskItems = taskWidget.find('li.task-item');
    taskWidget.sortable({
      zIndex: 1,
      items: taskItems,
      placeholder: "alert alert-success",
      update: function( event, ui ) 
      {
            $('#success').html('');
            var sorted = ($.map($(this).find('li'), function(el) {
                return $(el).index()+'^'+el.id;
            }));
            $('#success').html('');

            sorted = sorted.toString();
            $.post( "jq.update_estimate_sort.php", { sorted: sorted })
              .done(function(result) 
              {
                    $('#success').html(result);
              });
      }
    });
});
</script>

</body>

</html>