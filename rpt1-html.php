<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
 
$begin = $_REQUEST['begin'];
$end = $_REQUEST['end'];
$employee_id = $_REQUEST['employee_id'];
$title = "Labor Sort Report";

# check empty
$vujade->check_empty($begin,'Begin Date');
$vujade->check_empty($end,'Begin End');
$vujade->check_empty($employee_id,'Employee');

# validate person id
$person = $vujade->get_employee($employee_id);
if($person['error']!="0")
{
	$vujade->errors[]="Employee was not found.";
}

$rate = $vujade->get_employee_current_rate($employee_id);

$e = $vujade->get_error_count();
if($e>0)
{
	$action=0;
}

$pateHTML .= '
 
 <!DOCTYPE html>
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>PDF Report</title>
 <meta name="description" content="">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <style>
 .bordered
 {
 padding-bottom:3px;
 border-bottom: 1px solid #cecece;
 }
 .header
 {
 font-weight: bold;
 font-size: 14px;
 }
 .header_graybg
 {
 background-color: #cecece;
 font-weight: bold;
 font-size: 14px;
 padding-bottom:3px;
 border-bottom: 1px solid #cecece;
 }
 
 table {font-family: DejaVuSansCondensed; font-size: 11pt; line-height: 1.2;
 margin-top: 2pt; margin-bottom: 5pt;
 border-collapse: collapse;  }
 
 thead {	font-weight: bold; vertical-align: bottom; }
 tfoot {	font-weight: bold; vertical-align: top; }
 thead td { font-weight: bold; }
 tfoot td { font-weight: bold; }
 
 thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }
 
 .headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
 .footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
 
 th {	font-weight: bold;
 vertical-align: top;
 text-align:left;
 padding-left: 2mm;
 padding-right: 2mm;
 padding-top: 0.5mm;
 padding-bottom: 0.5mm;
 }
 
 td {
 	padding-left: 2mm;
	vertical-align: top;
	text-align:left;
	padding-right: 2mm;
	padding-top: 0.5mm;
	padding-bottom: 0.5mm;
 }
 
 th p { text-align: left; margin:0pt;  }
 td p { text-align: left; margin:0pt;  }
  .center-text{ text-align: center; }
 
 </style>
 
 </head>
 <body>
 <div id="mainContent">
 	<div id="content">
	 	<br style="clear: both;"/>
		 <h2 class="signInText center-text">'.$title.'</h2>
';

# person info
$person = $vujade->get_employee($employee_id,1);
$rate_data = $vujade->get_employee_current_rate($employee_id);

# date/day info
$startts = strtotime($begin);
$endts = strtotime($end);
$days = $vujade->get_dates_in_range($startts,$endts);

# project ids
$project_ids = array();
# 1: this gets all projects where the employee had time worked in the selected date range; ordered by date
$sql = "SELECT * FROM `timecards` WHERE `date_ts` >= '$startts' AND `date_ts` <= '$endts' AND `employee_id` = '$employee_id' ORDER BY `date_ts`"; 

//print '<p>Query: '.$sql.'</p>';

$mysqli = $vujade->mysqli;
$result = $mysqli->query($sql);
if( (!$result) || ($result->num_rows<1) )
{
    $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
    //print '<p>Error: '.$error.'</p>';
}
else
{
    while($rows = $result->fetch_assoc())
    {
        $project_ids[] = $rows['project_id'];
    }
}
$project_ids=array_unique($project_ids);
$c1 = count($project_ids);
if($c1>0)
{

    $total_st=0;
    $total_ot=0;
    $total_dt=0;

    $total_vacation=0;
    $total_holiday=0;

    $total_paid;

    # heading row
    $pateHTML.='
    <center><span style = "font-weight:bold;font-size:16px;">Labor Sort '.$begin.' to '.$end.'</span></center>
    <table width = "100%">
        <tr class = "header_graybg">
            <td>Job No</td>
            <td>Employee</td>
            <td>S.T.</td>
            <td>Rate</td>
            <td>O.T.</td>
            <td>Rate</td>
            <td>D.T.</td>
            <td>Rate</td>
            <td>Vac</td>
            <td>Hol</td>
            <td>Total Paid</td>
        </tr>';

    # get all time worked for this project; order by day
    foreach($project_ids as $pid)
    {

        # project id row heading;
        # all work types should now have a project number, even office time

            $pateHTML.='<tr>
                <td colspan = "11"><strong>'.$pid.'</strong></td>
            </tr>';

        # loop through each day in the range and get hours worked for the project
        foreach($days as $day)
        {
            $daytotal=0;
            $day_total = 0; // deprecated
            $day_total_paid; 

            $day_vacation=0;
            $day_holiday=0;

            $ts = strtotime($day);
            $sql = "SELECT * FROM `timecards` WHERE `date_ts` = '$ts' AND `employee_id` = '$employee_id' AND `project_id` = '$pid' ORDER BY `date_ts`"; 
    
            $line_total = 0;

            $result = $mysqli->query($sql);
            if( (!$result) || ($result->num_rows<1) )
            {
                $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
            }
            else
            {
                while($rows = $result->fetch_assoc())
                {
                    $date = $rows['date'];
                    $date_ts = $rows['date_ts'];
                    $type = $rows['type'];
                    $project_id = $rows['project_id'];
                    $start = $rows['start'];
                    $end = $rows['end'];
                    $standard_time = $rows['standard_time'];
                    $over_time = $rows['over_time'];
                    $double_time = $rows['double_time'];

                    $line_st=0;
                    $line_ot=0;
                    $line_dt=0;
                    $line_vacation=0;
                    $line_holiday=0;

                    # hours were picked from the drop down
                    if(!empty($start))
                    {
                        # hours worked for this entry
                        $begints = strtotime($date.' '.$start);
                        $endts = strtotime($date.' '.$end);
                        $diff = $endts-$begints;
                        $diff = $diff / 60;
                        $diff = $diff / 60; // total time worked for this row

                        $line_total=$diff;
                        $daytotal+=$line_total;

                        if($daytotal<=8)
                        {
                            # hours worked for this entry (not the same as the date because dates can have multiple entries)
                            $st = $diff;
                            $st_total+=$diff;
                            $ot = 0;
                            $dt = 0;
                        }
                        if( ($daytotal > 8) && ($daytotal <= 12) )
                        {
                            $st = 0;
                            $ot = $daytotal-8;
                            $st = $diff-$ot;
                            $st_total+=$st;
                            $ot_total+=$ot;
                            $dt = 0;
                        }

                        if($daytotal > 12)
                        {
                            $ot = 0;
                            $st = 0;
                            $dt = $daytotal-12;
                            $dt_total+=$dt;
                            $ot = $diff-$dt;
                            $ot_total+=$ot;
                        }
                    }
                    else
                    {
                        # hours were typed in manually
                        $daytotal+=$standard_time;
                        $daytotal+=$over_time;
                        $daytotal+=$double_time;
                        $st=$standard_time;
                        $dt = $double_time;
                        $ot = $over_time;
                        if($type=="Holiday")
                        {
                            $day_holiday+=$standard_time;
                        }
                        if($type=="Vacation")
                        {
                            $day_vacation+=$standard_time;
                        } 
                    }
                    # row html output
                    $pateHTML.= '<tr>';

                    # blank cells
                    $pateHTML.= '<td>';
                    $pateHTML.= '</td>';

                    # name and date
                    $pateHTML.= '<td>';
                    $pateHTML.= $person['fullname'];
                    $pateHTML.= '</td>';

                    # standard hour running total
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($st,2,'.',',');
                    $pateHTML.= '</td>';

                    # standard rate
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($rate_data['rate'],2,'.',',');
                    $pateHTML.= '</td>';

                    # overtime
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($ot,2,'.',',');
                    $pateHTML.= '</td>';

                    # overtime rate
                    $pateHTML.= '<td>';
                    $otrate = $rate_data['rate']*1.5;
                    $pateHTML.= @number_format($otrate,2,'.',',');
                    $pateHTML.= '</td>';

                    # double time
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($dt,2,'.',',');
                    $pateHTML.= '</td>';

                    # double time rate
                    $pateHTML.= '<td>';
                    $dtrate = $rate_data['rate']*2;
                    $pateHTML.= @number_format($dtrate,2,'.',',');
                    $pateHTML.= '</td>';

                    # vacation
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_vacation,2,'.',',');
                    $pateHTML.= '</td>';

                    # holiday
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_holiday,2,'.',',');
                    $pateHTML.= '</td>';

                    # total paid
                    $st_pay = $rate_data['rate'] * $st;
                    $ot_pay = $otrate * $ot;
                    $dt_pay = $dtrate * $dt;
                    $h_pay = $rate_data['rate'] * $day_holiday;
                    $v_pay = $rate_data['rate'] * $day_vacation;
                    $day_total_paid=($st_pay+$ot_pay+$dt_pay+$h_pay+$v_pay);
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_total_paid,2,'.',',');
                    $pateHTML.= '</td>';
                    $pateHTML.= '</tr>';

                    $total_st+=$st;
                    $total_ot+=$ot;
                    $total_dt+=$dt;

                    $total_vacation+=$day_vacation;
                    $total_holiday+=$day_holiday;

                    $total_paid+=$day_total_paid;
                }
            }
        }
    }
    # summary rows
    $pateHTML.= '<tr>';
    $pateHTML.= '<td colspan = "11"><hr>';
    $pateHTML.= '</td>';
    $pateHTML.= '</tr>';
    $pateHTML.= '<tr>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= @number_format($total_st,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= @number_format($total_ot,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= @number_format($total_dt,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= @number_format($total_vacation,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= @number_format($total_holiday,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= @number_format($total_paid,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '</tr>';
    $pateHTML.= '</table>';
}
$pateHTML .= '
   			</div>
    	</div>
    </body>
</html>';
?>