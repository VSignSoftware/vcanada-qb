<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];
$project_id = $id;

# permissions
$designs_permissions = $vujade->get_permission($_SESSION['user_id'],'Designs');
if($designs_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($designs_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$designid = $_REQUEST['designid'];
$design = $vujade->get_design($designid);
if($design['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# edit design
if($action==1)
{
	$project_id=$_POST['project_id'];
	//$date=$_POST['date'];
	$designer=$_POST['designer'];
	$required_date=$_POST['required_date'];
	$status=$_POST['status'];
	$priority=$_POST['priority'];
	$company=$_POST['company'];
	$contact=$_POST['contact'];
	$phone=$_POST['phone'];
	$email=$_POST['email'];
	$comments=$_POST['comments'];
	$description=$_POST['description'];
	$designer=$_POST['designer'];
	$t=$_POST['type'];
	$type_1=$t[0];
	$type_2=$t[1];
	$type_3=$t[2];
	$type_4=$t[3];
	$f=$_POST['format'];
	$format_1=$f[0];
	$format_2=$f[1];
	$format_3=$f[3]; 
	$format_4=$f[4];
	$format_5=$f[2];
	$row_id = $designid;
	$hours = $_POST['hours'];

	$s=array();
	//$s[]=$vujade->update_row('designs',$row_id,'request_date',$date);
	//$s[]=$vujade->update_row('designs',$row_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('designs',$row_id,'request_required_date',$required_date);
	$s[]=$vujade->update_row('designs',$row_id,'request_description',$description);
	$s[]=$vujade->update_row('designs',$row_id,'priority',$priority);
	$s[]=$vujade->update_row('designs',$row_id,'type',$type_1.' '.$type_2.' '.$type_3.' '.$type_4);
	$s[]=$vujade->update_row('designs',$row_id,'type_sub',$format_1.' '.$format_2.' '.$format_3.' '.$format_4.' '.$format_5);
	$s[]=$vujade->update_row('designs',$row_id,'status',$status);
	$s[]=$vujade->update_row('designs',$row_id,'employee_id',$designer);	
	$s[]=$vujade->update_row('designs',$row_id,'designer_company',$company);
	$s[]=$vujade->update_row('designs',$row_id,'designer_contact',$contact);
	$s[]=$vujade->update_row('designs',$row_id,'designer_phone',$phone);	
	$s[]=$vujade->update_row('designs',$row_id,'designer_email',$email);	
	$s[]=$vujade->update_row('designs',$row_id,'designer_comments',$comments);
	$s[]=$vujade->update_row('designs',$row_id,'hours',$hours);
	$vujade->page_redirect('project_designs.php?id='.$project_id.'&designid='.$designid);
}

# cancel button was pressed
if($action==2)
{
	$project_id=$_POST['project_id'];
	$vujade->page_redirect('project_designs.php?id='.$project_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$title = "Edit Design - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            $vujade->show_messages();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">
                  <strong><?php print $design['design_id']; ?></strong>
                  <form method="post" action="edit_design.php" id="form">

                    <input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
                    <input type = "hidden" name = "designid" value = "<?php print $designid; ?>">

                    <div style = "padding:10px;width:100%;">
						<div class="row">
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-3 col-sm-3 col-xs-3 vcenter">Date:</div>
									<div class="col-md-9 col-sm-3 col-xs-3"><?php print $design['request_date']; ?></div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="row">
									<div class="col-md-3 text-right vcenter">
										Assign to: 
									</div>
									<div class="col-md-9">
										<select name = "designer" class = "form-control" style = "width:200px;margin-bottom:5px;">
											<?php
											if(!empty($design['employee_id']))
											{
												$employee = $vujade->get_employee($design['employee_id'],2);
												if($employee['error']=="0")
												{
													print '<option value = "'.$employee['employee_id'].'">'.$employee['fullname'].'</option>';
												}
											}
											print '<option value = "">-Select-</option>';
											$designers = $vujade->get_designers();
											if($designers['error']=="0")
											{
												unset($designers['error']);
												foreach($designers as $designer)
												{
													print '<option value = "'.$designer['employee_id'].'">'.$designer['fullname'].'</option>';
												}
											}
											?>
										</select>
									</div>
								</div>
							</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3 vcenter">Required:</div>
							<div class="col-md-9">								
								<input type = "text" class = "form-control dp"  name = "required_date" id = "required_date" value = "<?php print $design['request_required_date']; ?>"></td>
							</div>								
						</div>
					</div>
					<div class="col-md-6">
						<div class = "row">
							<div class="col-md-3 text-right vcenter">
								Hours
							</div>
							<div class="col-md-9">
								<input type = "text" name = "hours" id = "hours" class = "form-control" style = "width:100px;" value = "<?php print $design['hours']; ?>">
							</div>
						</div>
					</div>
				</div>
				<div class="row">						
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3 vcenter">Status: </div>	
							<div class="col-md-9">
								<select name = "status" class = "form-control">
							<?php
							if(!empty($design['status']))
							{
								print '<option value = "'.$design['status'].'">'.$design['status'].'</option>';
							}
							?>
							<option value = "">-Select-</option>
							<option value = "Submitted">Submitted</option>
							<option value = "Complete">Complete</option>
							<option value = "In Progress">In Progress</option>
							<option value = "On Hold">On Hold</option>
							</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<div class="row">
							<div class="col-md-3">Priority: </div>							
							<div class="col-md-9">
						<select name = "priority" class = "form-control">
							<?php
							if(!empty($design['priority']))
							{
								print '<option value = "'.$design['priority'].'">'.$design['priority'].'</option>';
							}
							?>
							<option value = "">-Select-</option>
							<option value = "Urgent">Urgent</option>
							<option value = "Standard" selected = "selected">Standard</option>
							<option value = "Low">Low</option>
						</select>
						</div>
						</div>
					</div>
				</div>
				<br style="clear: both; margin-top: 5px;"/>
				<div class="row">	
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-1">Type: </div>							
									<div class="col-md-11">
										<div class="row">	

								<?php
								# parse types
								if(preg_match("/Presentation/", $design['type']))
								{
									$type1=1;
								}
								else
								{
									$type1=0;
								}
								if(preg_match("/Const. Dwgs./", $design['type']))
								{
									$type2=1;
								}
								else
								{
									$type2=0;
								}
								if(preg_match("/Production/", $design['type']))
								{
									$type3=1;
								}
								else
								{
									$type3=0;
								}
								if(preg_match("/Permits Required/", $design['type']))
								{
									$type4=1;
								}
								else
								{
									$type4=0;
								}
								?>
								<div class="col-md-3"><input type = "checkbox" name = "type[]" value = "Presentation" <?php if($type1==1){print 'checked="checked"';}?> > Presentation </div>
								<div class="col-md-3"><input type = "checkbox" name = "type[]" value = "Const. Dwgs." <?php if($type2==1){print 'checked="checked"';}?> > Const. Dwgs. </div>
								<div class="col-md-3"><input type = "checkbox" name = "type[]" value = "Production" <?php if($type3==1){print 'checked="checked"';}?> > Production</div>
								<div class="col-md-3"><input type = "checkbox" name = "type[]" value = "Permits Required" <?php if($type4==1){print 'checked="checked"';}?> > Permits Required</div>
								</div>
									</div>
								</div>
							</div>
						</div>
						

						<div class="row">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-1">
										Format: 
									</div>							
									<div class="col-md-11">
										<div class="row">
								<?php
								# parse format
								if(preg_match("/11x17/", $design['type_sub']))
								{
									$f1=1;
								}
								else
								{
									$f1=0;
								}
								if(preg_match("/8.5x11/", $design['type_sub']))
								{
									$f2=1;
								}
								else
								{
									$f2=0;
								}
								if(preg_match("/photocomp/", $design['type_sub']))
								{
									$f3=1;
								}
								else
								{
									$f3=0;
								}
								if(preg_match("/thumbnail/", $design['type_sub']))
								{
									$f4=1;
								}
								else
								{
									$f4=0;
								}
								if(preg_match("/8.5x14/", $design['type_sub']))
								{
									$f5=1;
								}
								else
								{
									$f5=0;
								}
								?>
								<div class="col-md-2"><input type = "checkbox" name = "format[]" value = "11x17" <?php if($f1==1){print 'checked="checked"';}?> > 11x17 </div>
								<div class="col-md-2"><input type = "checkbox" name = "format[]" value = "8.5x11" <?php if($f2==1){print 'checked="checked"';}?> > 8.5x11 </div>
								<div class="col-md-2"><input type = "checkbox" name = "format[]" value = "8.5x14" <?php if($f5==1){print 'checked="checked"';}?> > 8.5x14 </div>
								<div class="col-md-2"><input type = "checkbox" name = "format[]" value = "photocomp" <?php if($f3==1){print 'checked="checked"';}?> > photocomp </div>
								<div class="col-md-2"><input type = "checkbox" name = "format[]" value = "thumbnail" <?php if($f4==1){print 'checked="checked"';}?> > thumbnail</div>
							</div>
									</div>
								</div>
							</div>	
						</div>		
						<div class="row">
							<div class="col-md-12">
								<h3>Architect/Designer</h3>													
							</div>
						</div>					

					<div class="row">
						<div class="col-md-1">							
								Company:
						</div>
						<div class="col-md-5"><input type = "text" class = "form-control" value = "<?php print $design['designer_company']; ?>" name = "company"></div>
						<div class="col-md-1">								
							Phone:
						</div>
						<div class="col-md-5">
								
							<input type = "text" class = "form-control" name = "phone" value = "<?php print $design['designer_phone']; ?>">
						</div>
					</div>
					<div class="row">
						<div class="col-md-1">Contact: </div>								
						<div class="col-md-5">
							<input type = "text" class = "form-control" name = "contact" value = "<?php print $design['designer_contact']; ?>">
						</div>
						<div class="col-md-1">								
								Email:
						</div>
						<div class="col-md-5">
							<input type = "text" class = "form-control" name = "email" value = "<?php print $design['designer_email']; ?>">
						</div>
					</div>
													
					</div>

					<br style="clear:both; margin-top: 5px"/>
					
					<div class="row" style="margin-top: 5px;">
						<div class="col-md-1">
							Description:
						</div>
						<div class="col-md-11 text-right">								
							<a href "#" id = "pastescope" class = "btn btn-xs btn-primary">Insert Project Description</a>															
						</div>
					</div>


                    <div class="row" style = "margin-top:15px;">

                      <div class="col-md-12">
                      
                      		<textarea name = "description" id = "description" class = "ckeditor">
                      			<?php print $design['request_description']; ?>
							</textarea>
							<!-- ckeditor new version 4.5x -->
							<?php require_once('ckeditor.php'); ?>

                      </div>
                    </div>

                    <input type = "hidden" name = "id" value = "<?php print $id; ?>">
					<input type = "hidden" name = "action" id = "action" value = "1">

                    <div style = "margin-top:15px;">
                      <div style = "float:left">
                        <a href = "#" id = "cancel" class = "btn btn-lg btn-danger" style = "width:100px;">CANCEL</a> <a href = "#" id = "done" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND COMPLETE</a>
                      </div>
                    </div>

                  </form>
                </div>
              </div>

            </div>

	    </section>
	</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    $('#cancel').click(function(e)
	{
		e.preventDefault();
		$('#action').val(2);
		$('#form').submit();
	});

	$('#done').click(function(e)
	{
		e.preventDefault();
		$('#action').val(1);
		$('#form').submit();
	});

	$('.dp').datepicker();

	// fix the indent on the text area
    var d = $('#description').html();
    $("#description").html($.trim(d));
 	
 	// fix text area indents
	var c = $('#comments').html();
    $("#comments").html($.trim(c));

	$('#pastescope').click(function(e)
	{
		e.preventDefault();
		var desc = $.trim($('#hidden_desc').html());
		CKEDITOR.instances.description.setData(desc);
	});

});
</script>

</body>

</html>