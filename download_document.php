<?php
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$file_id = $_REQUEST['file_id'];
$employee = $_REQUEST['employee'];
$employee_id = $_REQUEST['employee_id'];
$vendor = $_REQUEST['vendor'];
$vendor_id = $_REQUEST['vendor_id'];
$project_id = $_REQUEST['project_id'];
$customer = $_REQUEST['customer'];
$customer_id = $_REQUEST['customer_id'];
$vujade->connect();

// zip all documents
$zip = 0;
if(isset($_REQUEST['zip']))
{
	$zip = $_REQUEST['zip'];
}
if($zip==1)
{
	// zip all 
	//$file_url = 'uploads/documents/'.$project_id.'/'.$file['file_name'];
	$folder = "uploads/documents/".$project_id;
	$oldzip=$folder."/".$project_id."-all-files.zip";
	if(file_exists($oldzip))
	{
		unlink($oldzip);
	}
	$new_zip=$folder."/".$project_id."-all-files.zip";
	$zipcmd = $vujade->create_zip($folder, $new_zip);
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"" . basename($new_zip) . "\""); 
	readfile($new_zip);
	if(file_exists($new_zip))
	{
		unlink($new_zip);
	}
}
else
{
	// get one files
	$file = $vujade->get_document($file_id);
	if($file['error']=='0')
	{
		if($vendor==1)
		{
			$file_url = 'uploads/vendor_documents/'.$vendor_id.'/'.$file['file_name'];
		}

		if($employee==1)
		{
			$file_url = 'uploads/employee_documents/'.$employee_id.'/'.$file['file_name'];
		}

		if($customer==1)
		{
			$file_url = 'uploads/customer_documents/'.$customer_id.'/'.$file['file_name'];
		}
		
		if(!isset($file_url))
		{

			$file_url = 'uploads/documents/'.$project_id.'/'.$file['file_name'];
		}
		header('Content-Type: application/octet-stream');
		header("Content-Transfer-Encoding: Binary"); 
		header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\""); 
		readfile($file_url);
	}
	else
	{
		print $file['error'];
		die;
	}
}
?>