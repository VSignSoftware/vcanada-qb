<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	die;
}

$id = $_REQUEST['id'];
$s = array();

// jquery post values
$req = explode('-',$_REQUEST['id']);
$id = $req[0];
$column = $req[1];
$start = $_REQUEST['start'];
$end = $_REQUEST['end'];
if(empty($end))
{
	$end=$start;
}
if($end=="null")
{
	$end=$start;
}
if($end=="undefined")
{
	$end=$start;
}
$is_all_day = $_REQUEST['is_all_day'];
//$s[]=$vujade->update_row('calendar',$id,'obj',$_REQUEST['start'].' '.$_REQUEST['end']);
//$s[]=$vujade->update_row('calendar',$id,'obj',$_REQUEST['id'].' '.$_REQUEST['start'].' '.$_REQUEST['end']);

$req = "";
foreach($_REQUEST as $k => $v)
{
	$req.=$k.' -> '.$v.' && ';
}

$s[]=$vujade->update_row('calendar',$id,'obj',$req);


// reformat the dates 
// currently in this format from the request
// Sat Jul 05 2014 00:00:00 GMT+0000 
// Sat Jul 05 2014 00:00:00 GMT+0000
$sa = explode(" ",$start);
$dayname = $sa[0];
$monthname = $sa[1];

/*
Jan, Feb, Mar, Apr,
May, Jun, Jul, Aug,
Sep, Oct, Nov, Dec
*/

if($monthname=="Jan")
{
	$month = "01";
}
if($monthname=="Feb")
{
	$month = "02";
}
if($monthname=="Mar")
{
	$month = "03";
}
if($monthname=="Apr")
{
	$month = "04";
}
if($monthname=="May")
{
	$month = "05";
}
if($monthname=="Jun")
{
	$month = "06";
}
if($monthname=="Jul")
{
	$month = "07";
}
if($monthname=="Aug")
{
	$month = "08";
}
if($monthname=="Sep")
{
	$month = "09";
}
if($monthname=="Oct")
{
	$month = "10";
}
if($monthname=="Nov")
{
	$month = "11";
}
if($monthname=="Dec")
{
	$month = "12";
}

$day = $sa[2];
$year = $sa[3];
$hms = $sa[4];
$offset = $sa[5];

$ea = explode(" ",$end);
$edayname = $ea[0];
$emonthname = $ea[1];
if($emonthname=="Jan")
{
	$emonth = "01";
}
if($emonthname=="Feb")
{
	$emonth = "02";
}
if($emonthname=="Mar")
{
	$emonth = "03";
}
if($emonthname=="Apr")
{
	$emonth = "04";
}
if($emonthname=="May")
{
	$emonth = "05";
}
if($emonthname=="Jun")
{
	$emonth = "06";
}
if($emonthname=="Jul")
{
	$emonth = "07";
}
if($emonthname=="Aug")
{
	$emonth = "08";
}
if($emonthname=="Sep")
{
	$emonth = "09";
}
if($emonthname=="Oct")
{
	$emonth = "10";
}
if($emonthname=="Nov")
{
	$emonth = "11";
}
if($emonthname=="Dec")
{
	$emonth = "12";
}
$eday = $ea[2];
$eyear = $ea[3];
$ehms = $ea[4];
$eoffset = $ea[5];

/*
// they are in gmt format
// $today_9 = date('Y-m-d H:i:s', strtotime( $today." GMT-9"));
$start = strtotime($start);
$end = strtotime($end);
$start = date('m/d/Y',$start_c);
$end = date('m/d/Y',$end_c-1);
*/

# update the calendar table
$s[]=$vujade->update_row('calendar',$id,'start',$month."/".$day."/".$year);
$s[]=$vujade->update_row('calendar',$id,'end',$emonth."/".$eday."/".$eyear);
$s[]=$vujade->update_row('calendar',$id,'start_hour',$hms);
$s[]=$vujade->update_row('calendar',$id,'end_hour',$ehms);
$s[]=$vujade->update_row('calendar',$id,'is_all_day',$is_all_day);

# get the job status row data for this project
$event = $vujade->get_event_data($id); 
$js = $vujade->get_job_status($event['project_id']); 

// update the job status row
$dbcol="";
if($column=="shipping")
{
	$dbcol="shipping_date";
}
if($column=="service")
{
	$dbcol="service_date";
}

if($column=="installation")
{
	$s[]=$vujade->update_row('job_status',$js['database_id'],"installation_date",$month."/".$day."/".$year);

	$start_c = strtotime($day." ".$monthname." ".$year." ".$hms);
	$end_c = strtotime($eday." ".$emonthname." ".$eyear." ".$ehms);

	// single day event fix
	// the calendar gives the end day as 12 a.m. of the next day for 
	// single day events;
	// in order for single day events to be correctly shown on the 
	// job status page, if the end date is exactly 86400 seconds greater 
	// than the start date, it is a single day event; otherwise it is a 
	// multi day event
	$sum = $end_c-$start_c;
	if($sum<=86400)
	{
		// means it is a single day event
		$s[]=$vujade->update_row('job_status',$js['database_id'],"installation_end_date",$month."/".$day."/".$year);
	}
	if($sum>86400)
	{
		// means it is a single day event
		$end_date = $end_c-86400;
		$end_date = date('m/d/Y',$end_date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_end_date',$end_date);
	}
}
else
{
	$s[]=$vujade->update_row('job_status',$js['database_id'],$dbcol,$month."/".$day."/".$year);
}
$s[]=$vujade->update_row('job_status',$js['database_id'],'calendar_update','request var: '.$_REQUEST['id'].' '.'dbcol: '.$dbcol.' column:'.$column.' end:'.$end);
// fix for multiple; doesn't know which one to update when there are multiple
?>