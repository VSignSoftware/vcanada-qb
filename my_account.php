<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$id = $_SESSION['user_id'];
$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}

# only logged in user can access this page and admins
if( ($_SESSION['user_id']==$id) || ($_SESSION['user_id']==1) || ($_SESSION['user_id']==2) )
{
	# update username
	if($action==1)
	{
		$u = $_POST['u'];
		$vujade->check_empty($u,'Username');
		$unique = $vujade->check_username_is_unique($u);
		$e = $vujade->get_error_count();
		if($e<=0)
		{
			# they are reviewing their own record
			if($_SESSION['user_id']==$id)
			{
				unset($_SESSION['username']);
				$_SESSION['username']=$u;
			}
			
			#update
			$success = $vujade->update_row('employees',$id,'username',$u);
			if($success==1)
			{
				$vujade->set_message('Username updated');
			}
		}
	}

	# update password
	if($action==2)
	{
		$p = $_POST['p'];
		$vujade->check_empty($p,'Password');
		$e = $vujade->get_error_count();
		if($e<=0)
		{
			# encrypt
			$p = crypt($p);

			#update
			$s1 = $vujade->update_row('employees',$id,'password',$p);
			$s2 = $vujade->update_row('employees',$id,'pw_changed',1);
			$s3 = $vujade->update_row('employees',$id,'pw_encrypted',1);

			# clear password change first time flag
			unset($_SESSION['pw_changed']);
			$_SESSION['pw_changed']=1;
			$success = $s1+$s2+$s3;
			if($success==3)
			{
				$vujade->set_message('Password updated');
			}
		}
	}

	if($action==3)
	{
		$reply_to = $_POST['reply_to'];
		$sig = $_POST['sig'];
		$bcc=$_POST['bcc'];
		$vujade->check_empty($reply_to,'Reply To');
		$vujade->check_empty($sig,'Signature');
		$send_tasks_to_email=$_POST['send_tasks_to_email'];

		// optional / only for sales people
		$status_change_task=$_POST['status_change_task'];
		$status_change_email=$send_tasks_to_email;

		$e = $vujade->get_error_count();
		if($e<=0)
		{
			$s = array();

			#update
			$s[] = $vujade->update_row('employees',$id,'email_reply_to',$reply_to);
			$s[] = $vujade->update_row('employees',$id,'email_signature',$sig);
			$s[] = $vujade->update_row('employees',$id,'send_tasks_to_email',$send_tasks_to_email);
			$s[] = $vujade->update_row('employees',$id,'status_change_task',$status_change_task);
			$s[] = $vujade->update_row('employees',$id,'status_change_email',$status_change_email);
			$s[] = $vujade->update_row('employees',$id,'bcc',$bcc);
			$vujade->set_message('Settings changed.');
		}
	}

}

$employee = $vujade->get_employee($_SESSION['user_id']);

$emp=$employee;
$section=0;
$title = "My Account - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">My Account</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

			<?php
			if($_SESSION['pw_changed']==0)
			{
				$vujade->set_error[]='You must change your password.';
			}
			$vujade->show_messages();
			$vujade->show_errors();

			if($employee['error']==0)
			{
				unset($employee['error']);
				?>

				<div class="panel heading-border panel-primary">
                	<div class="panel-body bg-light">

                		<div class = "row">
							<form class="" method = "post" action = "my_account.php">
								<div class="form-group">
									<div class="col-lg-4">
									    <input type = "hidden" name = "action" value = "1">
										<input type = "hidden" name = "id" value = "<?php print $id; ?>">
										<input type = "text" name = "u" id = "username" value = "<?php print $employee['username']; ?>" class="form-control input-lg">
									</div>
									<div class="col-lg-4">
										<button id = "change_un" href = "#" class = "btn btn-lg btn-primary">Update Username</button>
									</div>
								</div>			
							</form>
						</div>

						<div class = "row">
							<form class="" method = "post" action = "my_account.php">
								<div class="form-group">
									<div class="col-lg-4">
									    <input type = "hidden" name = "action" value = "2">
										<input type = "hidden" name = "id" value = "<?php print $id; ?>">
										<input type = "text" name = "p" id = "password" value = "*****" class="form-control input-lg">
									</div>
									<div class="col-lg-4">
										<button id = "change_pw" class = "btn btn-lg btn-primary">Update Password</button>
									</div>
								</div>			
							</form>
						</div>

						<div class="section-divider mb40" id="spy1" style = "margin-top:30px;margin-bottom:30px;">
	                      <span>Email Settings</span>
	                    </div>

	                    <form method = "post" action = "my_account.php">
								<input type = "hidden" name = "action" value = "3">
								<input type = "hidden" name = "id" value = "<?php print $id; ?>">

							<div class = "row pb30">
								  <div class="form-group">
				                    <label class="col-lg-3" for=""><h4>Email Reply To</h4></label>
				                    <div class="col-lg-8">
										<input type = "text" name = "reply_to" id = "reply_to" value = "<?php print $employee['email_reply_to']; ?>" style = "" class="form-control input-lg"> 
				                    </div>
				                    <div class="col-lg-1">	
				                    </div>
				                  </div>			
							</div>

							<div class = "row pb30">
				                  <div class="form-group">
				                    <label class="col-lg-3" for=""><h4>Signature</h4></label>
				                    <div class="col-lg-8">
										<textarea rows = "5" class="form-control input-lg" name = "sig" id = "sig" style = "">
											<?php 
											// replace new lines?
											print $employee['email_signature'];
											?>
										</textarea>
				                    </div>
				                    <div class="col-lg-1">
				                    </div>
				                  </div>
							</div>

							<div class = "row pb30">
				                  <div class="form-group">
				                    <label class="col-lg-3" for=""><h4>Send tasks to my email?</h4></label>
				                    <div class="col-lg-8">
				                    	<div class="radio-custom mb5">
											<input type = "radio" id = "send_tasks_to_email1" name = "send_tasks_to_email" value = "1" <?php if($employee['send_tasks_to_email']==1){ print 'checked="checked"'; }?>><label for="send_tasks_to_email1">Yes</label>
										</div>
										<div class="radio-custom mb5">
											<input style = "" type = "radio" id = "send_tasks_to_email2" name = "send_tasks_to_email" value = "0" <?php if($employee['send_tasks_to_email']==0){ print 'checked="checked"'; }?>><label for="send_tasks_to_email2">No</label>
										</div>
				                    </div>
				                    <div class="col-lg-1">
				                    </div>
								</div>
							</div>

							<div class = "row pb30">
				                  <div class="form-group">
				                    <label class="col-lg-3" for=""><h4>Send me a BCC email of all emails I send? (Designs, Invoices, Photos, Proposals, Purchase Orders)</h4></label>
				                    <div class="col-lg-8">
				                    	<div class="radio-custom mb5">
											<input type = "radio" id = "bcc1" name = "bcc" value = "1" <?php if($employee['bcc']==1){ print 'checked="checked"'; }?>><label for="bcc1">Yes</label>
										</div>
										<div class="radio-custom mb5">
											<input style = "" type = "radio" id = "bcc2" name = "bcc" value = "0" <?php if($employee['bcc']==0){ print 'checked="checked"'; }?>><label for="bcc2">No</label>
										</div>
				                    </div>
				                    <div class="col-lg-1">
				                    </div>
								</div>
							</div>

							<?php
							// if user is sales person, show this:
							if( ($employee['department']=="All Departments") || ($employee['department']=="Sales Dept. 400") )
							{
								?>

								<div class="section-divider mb40" id="spy1">
		                      		<span>Sales Task Settings</span>
		                    	</div>

		                    	<div class = "row pb30">
									<div class="form-group">
				                    <label class="col-lg-3" for=""><h4>Send me status change tasks?</h4></label>
				                    <div class="col-lg-8">
				                    	<div class="radio-custom mb5">
											<input type = "radio" id = "status_change_task1" name = "status_change_task" value = "1" <?php if($employee['status_change_task']==1){ print 'checked="checked"'; }?>> <label for="status_change_task1">Yes</label>
										</div>
										<div class="radio-custom mb5">
											<input style = "" type = "radio" id = "status_change_task2" name = "status_change_task" value = "0" <?php if($employee['status_change_task']==0){ print 'checked="checked"'; }?>> <label for="status_change_task2">No</label>
										</div>
				                    </div>

				                    <div class="col-lg-1">
				                    </div>
				                	</div>
				                </div>
				        <?php } ?>

				        <div class = "row">
		                	<button type = "submit" id = "" class = "btn btn-lg btn-primary">SAVE</button>
		                </div>

					</form>
				</div>	
			<?php 
			} 
			else
			{
				$vujade->set_error($employee['error']);
				$vujade->show_errors();
			}
			?>
		  </div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    var sig = $('#sig').html();
    $("#sig").html($.trim(sig));

    // password box focus
    $('#password').focus(function()
    {
    	$(this).val('');
    });

    // focus out
    $('#password').focusout(function()
    {
    	var val = $(this).val();
    	if(val=="")
    	{
    		$(this).val('*****');
    	}
    });
});
</script>

</body>

</html>