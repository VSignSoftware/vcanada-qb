<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$shop_order = $vujade->get_shop_order($id,$idcol="project_id");
$setup = $vujade->get_setup();
$invoice_setup = $vujade->get_invoice_setup();
$s=array();
$posted = 0;

# create a new invoice
$new=$_REQUEST['new'];
if($new==1)
{
	# determine what the invoice number is
	$invoices = $vujade->get_invoices_for_project($project_id);
	if($invoices['error']!='0')
	{
		$next_id=$project_id."-1";
	}
	else
	{
		$next_id=$project_id."-".$invoices['next_id'];
	}
	$vujade->create_row('quickbooks_invoice');

	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'RefNumber',$next_id,'ID');

	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'project_id',$id,'ID');

	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'advanced_deposit',1,'ID');

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'advanced_deposit_rate',$setup['advanced_deposit'],'ID');

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ts',strtotime('now'),'ID');

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'po_number',$shop_order['po_number'],'ID');

	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_time_created',strtotime('now'),'ID');

	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'costing',1,'ID');

	// copied from invoice.php
	// may not be needed...

	# get correct customer info for this project
	$listiderror=0;
	$localiderror=0;
	// try to get by list id
    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
    if($customer1['error']!="0")
    {
    	$listiderror++;
    	unset($customer1);
    }
    else
    {
    	$customer=$customer1;
    }
    // try to get by local id
    $customer2 = $vujade->get_customer($project['client_id'],'ID');
    if($customer2['error']!="0")
    {
    	$localiderror++;
    	unset($customer2);
    }
    else
    {
    	$customer=$customer2;
    }

    $iderror=$listiderror+$localiderror;

    if($iderror<2)
    {
    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
    }
    else
    {
    	// can't find customer or no customer on file
    }

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'CustomerRef_ListID',$customer['database_id'],'ID');

	// get and set the sales tax code 
	// qb tax state is the same as shipping address
	if($project['state']==$setup['qb_tax_state'])
	{
		// $tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);

		// check if has group sales tax
		$td = $vujade->get_tax_groups($project['city'].', '.$project['state']);

		//$vujade->debug_array($td);
		//die;

		if($td['count']==1)
		{
			//$tax['rate'] = $td['rate'];
			// $tax_data
			$tax_data['rate']=$td['rate'];
			$tax_data['error']=0;
			$tax_data['ListID']=$td['list_id'];
		}
		else
		{
			// does not have group sales tax
			$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
		}
	}
	else
	{
		// not the same
		$tax_data = $vujade->QB_get_sales_tax('Out of State');
	}
	if($tax_data['error']=="0")
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data['ListID'],'ID');
		$tax_rate=$tax_data['rate']; 
	}
	else
	{
		$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data2['ListID'],'ID');
		$tax_rate=$tax_data2['rate'];
	}

	if($setup['country']=="Canada")
	{
		$tax_rate=1;
	}

	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'TxnID',$fakeid,'ID');

	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'TxnDate',date('Y-m-d'),'ID');

	$sales_price=$shop_order['selling_price'];
	$tax_amount=$sales_price*$tax_rate;
	$deposit = ($sales_price+$tax_amount)*$setup['advanced_deposit'];
	$deposit_rate=$setup['advanced_deposit'];

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_deposit',$deposit,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_tax_amount',$tax_amount,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_tax_rate',$tax_rate,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'v_sales_price',$sales_price,'ID');

	// shipping address
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_Addr1',$project['site'],'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_Addr2',$project['address_1'],'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_City',$project['city'],'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_State',$project['state'],'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ShipAddress_PostalCode',$project['zip'],'ID');
	
	// brad block: queue up new invoice
	//$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ARAccountRef_FullName',"Advance Deposits",'ID');
	
	/* debug the above  
	$test_q=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ARAccountRef_FullName','Advance Deposits','ID');
	print $test_q.'<br>';
	print $vujade->row_id;
	die;
	*/
	
	//$qbquery = "UPDATE `quickbooks_invoice_lineitem` SET `SalesTaxCodeRef_FullName` = 'Non' WHERE `Parent_ID` = '" . $vujade->row_id . "' && `ItemRef_FullName` = 'Deposit'"; 
	
	$vujade->create_new_QB_invoice_lineitem($vujade->row_id, "Deposit", 0.00, 1,"");
	$vujade->create_new_QB_invoice_lineitem($vujade->row_id, "CC Processing Fee", 0.00, 1,"");
	
	$vujade->generic_query($qbquery);
	
	$vujade->queue_add($vujade->row_id);
	
	//end brad block: this section is complete.

	$invoice_id = $next_id;
	$invoice_db_id=$vujade->row_id;

}
else
{
	if(isset($_REQUEST['invoice_id']))
	{
		$invoice_id = $_REQUEST['invoice_id'];
		$invoice=$vujade->get_invoice($invoice_id,'RefNumber');
		$invoice_db_id=$invoice['database_id'];
	}
	if(isset($_REQUEST['invoice_db_id']))
	{
		$invoice_id = $_REQUEST['invoice_id'];
		$invoice=$vujade->get_invoice($invoice_db_id);
		$invoice_id = $invoice['invoice_id'];
	}
}

if($setup['country']=="Canada")
{
	$invoice['tax_rate']=1;
	$invoice['v_tax_rate']=1;
}

// form post
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

// add new line items / update items button
if($action==1)
{

	// add the line items
    $skipped = array('action','project_id','proposal_id','invoice_id','date','po_id','sales_price','tax_rate','tax_amount','deposit_rate','deposit');
    foreach($_POST as $key => $value)
    {
        if(!in_array($key, $skipped))
        {
            $test_key = explode('-',$key);
            $row_id=$test_key[1];
            if($test_key[0]=="desc")
            {
                $description=$value;
                $description=str_replace("\r"," \r",$description);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$description);
            }
            if($test_key[0]=="amount")
            {
                $amount=str_replace(",", "", $value);
                $amount=str_replace("$", "", $amount);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$amount);
            }
            if($test_key[0]=="sort")
            {
                $sort=str_replace(",", "", $value);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$sort);
            }
            if($test_key[0]=="tax")
            {
                $value = explode('^',$value);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$value[0]);
                $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$value[1]);
            }
        }
    }

    // new one
    $description=$_POST['description'];
    $amount=str_replace(',', '',$_POST['amount']);
    $amount=str_replace("$", "", $amount);
    $sort=str_replace(',', '',$_POST['sort']);
    if(!empty($description))
    {
        $vujade->create_row('invoice_line_items');
        $row_id = $vujade->row_id;
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'invoice_id',$invoice_db_id);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$description);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$amount);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$sort);

        $tax_type = explode('^',$_POST['tax_type']);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$tax_type[0]);
        $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$tax_type[1]);
    }

    // line items
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'',false,2);
	$li_total=0;
    $li_subtotal= 0;
    $li_tax_total=0;
    $li_tax_line=0;
    if($lines['error'] == "0") 
    {
        unset($lines['error']);
        unset($lines['count']);
        $has_lines=1;
        foreach($lines as $item) 
        {
            $li_subtotal += $item['line_amount'];
            //print $item['tax_rate'].'<br>';
            if(!empty($item['tax_rate']))
         	{
         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
         		//print $item['line_amount'].'<br>';
         		//print $item['tax_rate']
         		//print $li_tax_line.'<br>';
         	}
         	$li_tax_total+=$li_tax_line;
         	$li_tax_line=0;         
        }
        $li_total=$li_subtotal+$li_tax_total;
    }

    $sales_price=$li_subtotal;
    $tax_amount=$li_tax_total;

    //print $tax_amount;
    //die;

    $date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	$invoice=$vujade->get_invoice($invoice_db_id);
	$dbid = $invoice['database_id'];
	$po_id = $_POST['po_id'];

	// deposit rate and amount
	$deposit = ($sales_price+$tax_amount)*$setup['advanced_deposit'];
	$deposit_rate=$setup['advanced_deposit'];

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'TxnDate',$qbdate,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'po_number',$po_id,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_sales_price',$sales_price,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_tax_amount',$tax_amount,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_deposit',$deposit,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'advanced_deposit_rate',$deposit_rate,'ID');
	
	// brad block: update 
	//if this code causes problems, I have additional tools to help resolve this issue.
	//it's possible this block doesn't need to exist.
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	/*
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 1" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);
	*/
	$finalDesc = $arr['ManuDesc'] . " " . $arr['PartsDesc'] . " " . $arr['LaborDesc'];
	
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", $deposit, 1, $finalDesc);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1, "");
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete.

    $action=0;
}

// delete line item
if($action==2)
{
    $s = array();
    $row_id = $_POST['item_id'];
    $s[]=$vujade->delete_row('invoice_line_items',$row_id); 
    
    // brad block: update based on new deletion.
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	/*
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 2" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);
	*/

	$finalDesc = $arr['ManuDesc'] . " " . $arr['PartsDesc'] . " " . $arr['LaborDesc'];
	
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", $deposit, 1, $finalDesc);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1, "");
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete
    
    $action=0;
}

// add new line item between existing
if($action==3)
{
	$position=$_REQUEST['position'];
	$after_id = $_REQUEST['after_id'];
	$vujade->update_invoice_items_sort($after_id,$invoice_db_id,$position);
	
	// brad block: update based on new sort.
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	/*
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 3" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);
	*/

	$finalDesc = $arr['ManuDesc'] . " " . $arr['PartsDesc'] . " " . $arr['LaborDesc'];
	
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", $deposit, 1, $finalDesc);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1, "");
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete
	
	$action=0;
}

// save button pressed or update amounts pressed
if( ($action==5) || ($action==7) )
{
	// get the invoice
	if(isset($_POST['invoice_id']))
	{
		$invoice_id=$_POST['invoice_id'];
		$invoice=$vujade->get_invoice($invoice_id,'RefNumber');
	}
	if(isset($_POST['invoice_db_id']))
	{
		$invoice_db_id=$_POST['invoice_db_id'];
		$invoice=$vujade->get_invoice($invoice_db_id);
		$invoice_id=$invoice['invoice_id'];
		$dbid=$invoice_db_id;
	}
	
	$sales_price=str_replace(',', '', $_POST['sales_price']);
	$sales_price=str_replace('$', '', $sales_price);
	$cc=str_replace(',', '', $_POST['cc']);
	$cc=str_replace('$', '', $cc);
	$dbid = $invoice['database_id'];
	$po_id = $_POST['po_id'];
	$custom_tax_amount=$_POST['custom_tax_amount'];
	$custom_tax_amount=str_replace(',', '', $custom_tax_amount);
	$custom_tax_amount=str_replace('$', '', $custom_tax_amount);
	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'TxnDate',$qbdate,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'po_number',$po_id,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_cc_processing',$cc,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_custom_tax_amount',$custom_tax_amount,'ID');

	// deposit, deposit rate, tax rate, tax amount
	$deposit=str_replace(',', '', $_POST['deposit']);
	$deposit=str_replace('$', '', $deposit);
	$deposit_rate=str_replace(',', '', $_POST['deposit_rate']);
	$deposit_rate=str_replace('$', '', $deposit_rate);
	$tax_rate=str_replace(',', '', $_POST['tax_rate']);
	$tax_rate=str_replace('$', '', $tax_rate);
	$tax_amount=str_replace(',', '', $_POST['tax_amount']);
	$tax_amount=str_replace('$', '', $tax_amount);

	// these can't be larger than 1
	if($tax_rate>1)
	{
		$tax_rate=0;
	}
	if($deposit_rate>1)
	{
		$deposit_rate=0;
	}

	if($setup['country']=="Canada")
	{
		$tax_rate=1;
		$invoice['tax_rate']=1;
		$invoice['v_tax_rate']=1;
	}

	// user can change the deposit amount and tax amounts
	// or they can let the system calculate for them

	// determine what the user did

	// tax section
	
	// tax rate is not empty
	// automatically calculate the tax amount
	if($tax_rate>0)
	{

		if($custom_tax_amount==1)
		{
			$tax_amount=str_replace(',', '', $_POST['tax_amount']);
		}
		else
		{
			// automatically calculate 
			// $tax_amount=$sales_price*$tax_rate;

			// line items
			$lines = $vujade->get_invoice_line_items($invoice_db_id,'','',2);

			$li_total=0;
		    $li_subtotal= 0;
		    $li_tax_total=0;
		    $li_tax_line=0;
		    if($lines['error'] == "0") 
		    {
		        unset($lines['error']);
		        unset($lines['count']);
		        $has_lines=1;
		        foreach($lines as $item) 
		        {
		            $li_subtotal += $item['line_amount'];
		            if(!empty($item['tax_rate']))
		         	{
		         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
		         	}
		         	$li_tax_total+=$li_tax_line;
		         	$li_tax_line=0;         
		        }
		        $li_total=$li_subtotal+$li_tax_total;
		    }

		    $tax_amount=$li_tax_total;

		}

		// $tax_amount=$sales_price*$tax_rate;
		//print 'tax rate greater than 0...<br>';
		//print 'tax amount: '.$tax_rate.'<br>';
		//print 'tax amount: '.$tax_amount.'<br>';
	}
	else
	{
		// it is empty
		// no action needed
		//print 'tax rate <= 0...<br>';
		//print 'tax amount: '.$tax_amount.'<br>';
	}

	// deposit section
	// deposit rate is not empty
	// automatically calculate the deposit amount
	if($deposit_rate>0)
	{
		$deposit=($sales_price+$tax_amount) * $deposit_rate;
		//print 'deposit rate greater than 0...<br>';
		//print 'deposit rate: '.$deposit_rate.'<br>';
		//print 'deposit amount: '.$deposit.'<br>';
	}
	else
	{
		// it is empty
		// no action needed
		//print 'deposit rate < than 0...<br>';
		//print 'deposit amount: '.$deposit.'<br>';
	}

	//$deposit+=$cc;

	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_deposit',$deposit,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'advanced_deposit_rate',$deposit_rate,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_tax_amount',$tax_amount,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_tax_rate',$tax_rate,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_sales_price',$sales_price,'ID');
	
	// brad block: update 
	//if this code causes problems, I have additional tools to help resolve this issue.
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	/*
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 4" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);
	*/

	$finalDesc = $arr['ManuDesc'] . " " . $arr['PartsDesc'] . " " . $arr['LaborDesc'];
	
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", $deposit, 1, $finalDesc);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1, "");
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}

	//end brad block: this section is complete.

	if($action==5)
	{
		$vujade->page_redirect('project_invoices.php?id='.$id.'&invoiceid='.$dbid);
	}
	else
	{
		$action=0;
	}
}

// transfer from proposal or shop order
if( ($action==6) || ($action==8) )
{

	if($setup['country']=="Canada")
	{
		$invoice['v_tax_rate']=1;
		$invoice['tax_rate']=1;
	}

	$po_id = $_POST['po_id'];
	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'TxnDate',$qbdate);
	$s[]=$vujade->update_row('quickbooks_invoice',$invoice_db_id,'po_number',$po_id);

	// clear any existing lines
	$s[]=$vujade->delete_row('invoice_line_items',$invoice_db_id,1,'invoice_id'); 

	// copy from proposal
	if($action==6)
	{
		$items = $vujade->get_items_for_proposal($_REQUEST['proposal']);
	}

	// copy from shop order
	if($action==8)
	{
		$items = $vujade->get_items_for_proposal($shop_order['proposal']);
	}	

	if($items['error']=="0")
	{
		unset($items['error']);
		foreach($items as $item)
		{
			$vujade->create_row('invoice_line_items');
		    $row_id = $vujade->row_id;
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'invoice_id',$invoice_db_id);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_description',$item['item']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'line_amount',$item['amount']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'sort',$item['sort']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_type',$item['tax_label_name']);
		    $s[] = $vujade->update_row('invoice_line_items',$row_id,'tax_rate',$item['tax_label_rate']);
		}
	}
	unset($items);

	// reset the sales price, tax, deposit, etc
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'',false,2);

	$li_total=0;
    $li_subtotal= 0;
    $li_tax_total=0;
    $li_tax_line=0;
    if($lines['error'] == "0") 
    {
        unset($lines['error']);
        unset($lines['count']);
        $has_lines=1;
        foreach($lines as $item) 
        {
            $li_subtotal += $item['line_amount'];
            if(!empty($item['tax_rate']))
         	{
         		$li_tax_line=$item['line_amount']*$item['tax_rate']*$invoice['v_tax_rate'];
         	}
         	$li_tax_total+=$li_tax_line;
         	$li_tax_line=0;         
        }
        $li_total=$li_subtotal+$li_tax_total;
    }

    $sales_price=$li_subtotal;
    $tax_amount=$li_tax_total;

    //print $tax_amount;
    // die;

	$invoice=$vujade->get_invoice($invoice_db_id);
	$dbid = $invoice['database_id'];

	// deposit rate and amount
	$deposit = ($sales_price+$tax_amount)*$setup['advanced_deposit'];
	$deposit_rate=$setup['advanced_deposit'];

	# update
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_sales_price',$sales_price,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_tax_amount',$tax_amount,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_deposit',$deposit,'ID');
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'advanced_deposit_rate',$deposit_rate,'ID');
	
	// brad block: create new line items 
	
	$arr = $vujade->calculate_lineitems($invoice_db_id);
	
	/*
	$handle = fopen(dirname(__FILE__) . '/qblogfile.txt', 'a+');
	fwrite($handle, date(DATE_COOKIE) . "\r\n");
	fwrite($handle, "Calculate_Lineitems called from block 5" . "\r\n");
	fwrite($handle, $arr['Manufacture'] . " " . $arr['ManuDesc'] . "\r\n");
	fwrite($handle, $arr['Parts'] . " " . $arr['PartsDesc'] . "\r\n");
	fwrite($handle, $arr['Labor'] . " " . $arr['LaborDesc'] . "\r\n");
	$return = fclose($handle);
	*/

	$finalDesc = $arr['ManuDesc'] . " " . $arr['PartsDesc'] . " " . $arr['LaborDesc'];
	
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "Deposit", $deposit, 1, $finalDesc);
	$vujade->update_QB_invoice_lineitem($invoice_db_id, "CC Processing Fee", $cc, 1, "");
	
	// only do this if included in costing
	if($invoice['costing']==1)
	{
		$vujade->queue_mod($invoice_db_id);
	}
	
	//end brad block: this section is complete
	//if this code throws errors, it is most likely because I am not checking that the above line items exist before updating them.

    $action=0;
}

// default
if($action==0)
{
	# get invoice details
	//print $invoice_db_id.'<br>';
	$invoice=$vujade->get_invoice($invoice_db_id);
	//print_r($invoice);
	//die;
	if($invoice['error']=="0")
	{
		$deposit = $invoice['v_deposit'];
		$sales_price=$invoice['v_sales_price'];
		$tax_rate=$invoice['v_tax_rate'];
		$tax_amount=$invoice['v_tax_amount'];
		$deposit_rate=$invoice['advanced_deposit_rate'];
		$date=$invoice['date'];
		$po_id = $invoice['po_number'];
		$cc = $invoice['v_cc_processing'];
		$custom_tax_amount=$invoice['v_custom_tax_amount'];

		// line items
		$lines = $vujade->get_invoice_line_items($invoice['database_id'],'',false,2);
		$i4_label=$invoice['i4_label'];
		$i4_amount=$invoice['i4_amount'];
		$i4_rate=$invoice['i4_rate'];

		if($lines['error'] == "0") 
	    {
	        unset($lines['error']);
	        unset($lines['count']);
	        $has_lines=1;
	    }
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$menu=17;
$cmenu=1;
$title = 'Advanced Deposit Invoice - '.$project['project_id'] . ' - ';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		<form method = "post" action = "ad_invoice.php" id = "np">
			<input type = "hidden" name = "invoice_db_id" value = "<?php print $invoice_db_id; ?>">
			<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				<strong>Number: <?php print $invoice_id; ?> (Proposal: 
				<?php print $shop_order['proposal']; ?>)
				</strong>
			</div>
			<div class = "col-md-8 pull-right">
				<strong>Date: <input type = "text" name = "date" value = "<?php print $date; ?>" class = "dp" style = "width:150px;margin-rigth:10px;"></strong> 
				<strong>PO #: <input type = "text" name = "po_id" id = "po_id" value = "<?php print $po_id; ?>" class = "" style = "width:150px;"></strong> 
			</div>
		</div>

		<div class = "row">
			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Billing Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
					# get correct customer info for this project
					$listiderror=0;
					$localiderror=0;
					// try to get by list id
				    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
				    if($customer1['error']!="0")
				    {
				    	$listiderror++;
				    	unset($customer1);
				    }
				    else
				    {
				    	$customer=$customer1;
				    }
				    // try to get by local id
				    $customer2 = $vujade->get_customer($project['client_id'],'ID');
				    if($customer2['error']!="0")
				    {
				    	$localiderror++;
				    	unset($customer2);
				    }
				    else
				    {
				    	$customer=$customer2;
				    }

				    $iderror=$listiderror+$localiderror;

				    if($iderror<2)
				    {
				    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
				    }
				    else
				    {
				    	// can't find customer or no customer on file

				    }
		            if($customer['error']=="0")
		            {
		            	print $customer['name'].'<br>';
		            	print $customer['address_1'].'<br>';
		            	if(!empty($customer['address_2']))
		            	{
		            		print $customer['address_2'].'<br>';
		            	}
		            	print $customer['city'].', '.$customer['state'].' '.$customer['zip'].'<br>';
		            }
					?>
				  </div>
				</div>
			</div>

			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Job Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
		            # get site location for this project
					print $project['site'].'<br>';
					print $project['address_1'].'<br>';
					print $project['city'].', ';
					print $project['state'].' ';
					print $project['zip'].'<br>';;
					// county
					$county=$vujade->get_county($project['city'],$project['state']);
					if($county['error']=="0")
					{
						print $county['county'];
					}
					?>
				  </div>
				</div>
			</div>
		</div>

<div class = "row">
	<div class = "pull-right" style = "margin-right:15px;">
		
		<a class = "btn btn-xs btn-primary pull-right" id = "transfer" href = "#transfer-form" title = "Transfer from other" style = "margin-left:5px;">Transfer</a> 
		<a class = "btn btn-xs btn-primary pull-right" id = "preview" href = "#" title = "Print Preview">Print Preview</a>

		<!-- transfer modal -->
		<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
			<table width = "100%">
				<tr>
					<td>
						<center><label>Proposal Number</label></center>
					</td>
					<td>
						<center><select name = "transfer_proposal" id = "transfer_proposal" class = "form-control">
							<?php
							if(!empty($shop_order['proposal']))
							{
								print '<option value = "'.$shop_order['proposal'].'" selected = "selected">'.$shop_order['proposal'].'</option>';
							}
							print '<option value = "">-Select-</option>';

							$proposals = $vujade->get_proposals_for_project($project_id);
							if($proposals['error']=="0")
							{
								unset($proposals['error']);
								unset($proposals['next_id']);
								foreach($proposals as $proposal)
								{
									print '<option value = "'.$proposal['proposal_id'].'">'.$proposal['proposal_id'].'</option>';
								}
							}
							?>
						</select></center>	
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2"><center><label>OR</label></center></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan = "2">
						<center>
							<a href = "#" id = "use-scope-of-work" class = "btn btn-success">Shop Order</a>
						</center>
					</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
				</tr>

			</table>

			<input type = "hidden" id = "scope_of_work" name = "scope_of_work" value = "0">

			<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
			
			<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please choose a proposal number.</div>
			<div id = "error_2" style = "display:none;" class = "alert alert-danger">This proposal number is invalid.</div>

			<div class = "" style = "width:100%;margin-top:15px;text-align:center;">
				<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a> 
				<a id = "transfer_btn" href = "#" class = "btn btn-lg btn-success" style = "margin-right:15px;">SAVE</a>
			</div>
		</div>

	</div>
</div>

<div style="margin-bottom: 20px; overflow: hidden">
<div style="width: 100%; overflow: hidden;">
<div class="panel panel-primary">
<div class="panel-heading" style="padding: 0 11px">
    <div class="row">
        <div class="col-lg-9">Description</div>
        <div class="col-lg-3">Amount</div>
    </div>
</div>
<div class="panel-body">
<div class="row">
<div class="col-lg-12">
    <!-- header row -->
    <!-- any existing items will be displayed here -->
    <?php
    $li_total=0;
    $li_subtotal= 0;
    $next_sort=1;
    $current_sorts = array();
    $li_tax_total=0;
    $li_tax_line=0;
    $li_count=0;
    if($has_lines) 
    {
        print '<table width = "100%">';
        foreach($lines as $item) 
        {
        	$li_count++;
        	$next_sort++;
            $li_subtotal += $item['line_amount'];
            $current_sorts[]=$item['sort'];
            ?>
            <div class="row">
                <div>
                    <div class="ta_container col-lg-9 col-xs-8">
                        <?php
                        $rand = mt_rand();
                        ?>
                        
                        <textarea style="margin-bottom: 15px; width: 100%" class="ckeditor ars existing-line-item form-control" id="desc-<?php print $item['id']; ?>" name="desc-<?php print $item['id']; ?>"><?php print $item['line_description'];?></textarea>              
                    </div>
                    <div class="col-lg-2 col-xs-4">
                        <input type="hidden" class="rand" value="<?php print $rand; ?>">
                        <input type="text" class="amount form-control" value="<?php print $item['line_amount']; ?>"
                               name="amount-<?php print $item['id']; ?>"
                               id="amount-<?php print $item['id']; ?>" data-dbid="<?php print $item['id']; ?>">

                        Tax Type: 
	                    <select class="tt-select tax form-control" name="tax-<?php print $item['id']; ?>" id="tax-<?php print $item['id']; ?>">
	                     	<?php
	                     	if(!empty($item['tax_type']))
	                     	{
	                     		print '<option value = "'.$item['tax_type'].'^'.$item['tax_rate'].'">'.$item['tax_type'].'</option>';
	                     	}
	                     	print '<option value = "">-Select-</option>';
	                     	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
	                     	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
	                     	?>
	                    </select> 
	                    Tax Amount: $

	                    <?php
	                    if(!empty($item['tax_rate']))
                     	{
                     		$li_tax_line=$item['line_amount']*$item['tax_rate']*$tax_rate;
                     		print @number_format($li_tax_line,2);
                     	}
                     	else
                     	{
                     		print '0.00';
                     	}
                     	$li_tax_total+=$li_tax_line;
                     	$li_tax_line=0;
	                    ?>

                    </div>
					<div class="col-lg-1 col-xs-12">
					<a href = "ad_invoice.php?project_id=<?php print $project_id; ?>&invoice_id=<?php print $invoice['invoice_id']; ?>&invoice_db_id=<?php print $invoice['database_id']; ?>&action=3&after_id=<?php print $item['id']; ?>&position=<?php print $item['sort']; ?>" id = "" class = "add-new-row btn btn-xs btn-success" title = "Add New Item After This Item">+</a> 
					<span style="line-height: 39px"><a title = "Delete Item" class="plus-delete btn btn-xs btn-danger" href="#"
					id="<?php print $item['id']; ?>">X</a></span>
					<br>
					<input type="hidden" class="sort" value="<?php print $item['sort']; ?>"
                               name="sort-<?php print $item['id']; ?>"
                               id="sort-<?php print $item['id']; ?>" style = "">
					</div>
				</div>
			</div>
			<br>
        <?php
        }

        $li_total=$li_subtotal+$li_tax_total;

        // highest current sort
        $highest = max($current_sorts);
        if($next_sort<=$highest)
        {
        	$next_sort=$highest+1;
        }
    }
    ?>
    <!-- blank input row -->
    <div class="row">
        <div class="col-lg-9 col-xs-8" style="">
            <textarea name="description" name="description" id="description" style="margin-bottom: 15px; width: 100%" class="ckeditor ars form-control"></textarea>
        </div>
        <div class="col-lg-2 col-xs-4" style="">
            <input class="form-control" type="text" name="amount" id="amount" style="">
            <span style="margin-right:55px;width:148px;height:35px;">&nbsp;</span>
            <br>
            <input class="" type="hidden" name="sort" id="sort" style="" value = "<?php print $next_sort; ?>">
            Tax Type: 
            <select class="tt-select tax form-control" name="tax_type" id="tax_type">
             	<?php
             	print '<option value = "">-Select-</option>';
             	print '<option value = "'.$invoice_setup['label_1'].'^'.$invoice_setup['sale_price_1'].'">'.$invoice_setup['label_1'].'</option>';
             	print '<option value = "'.$invoice_setup['label_2'].'^'.$invoice_setup['sale_price_2'].'">'.$invoice_setup['label_2'].'</option>';
             	print '<option value = "'.$invoice_setup['label_3'].'^'.$invoice_setup['sale_price_3'].'">'.$invoice_setup['label_3'].'</option>';
             	?>
            </select> 
        </div>
    </div>
</div>
    <div style="margin-bottom: 20px;">
        <div class="row">
            <div class="col-md-12">
                <a class="plus-update btn btn-success btn-xs pull-right" id="update-items" href="#" style="">Update Items</a>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
		
<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>
		
<p style = "height:15px;">&nbsp;</p>

<!-- bottom block -->
<div style = "clear:both;width:100%;margin-top:15px;margin-left:10px;margin-right:10px;">

	<!-- sales price, tax rate, amount and total -->
	<div style="margin-bottom: 20px; overflow: hidden">

		<table class = "table">

			<tr>
				<td>
					Sales Price: 
				</td>

				<td>
					<input class="form-control" type="text" id="sales_price" name="sales_price" value="<?php print $sales_price; ?>" style = "width:150px;">
				</td>

				<td colspan="2">
					&nbsp;
				</td>
			</tr>

			<tr>
				<td>
					Sales Tax (decimal): 
				</td>

				<td>
					<input class="form-control" type="text" id="tax_rate" name="tax_rate" value="<?php print $tax_rate; ?>" style = "width:150px;">
				</td>

				<td>
					Tax Amount: 
				</td>

				<td>
					<input type = "text" class = "form-control" id = "tax_amount" name = "tax_amount" value = "<?php print $tax_amount; ?>" style = "width:150px;">
				</td>

			</tr>

			<tr>
				<td>
					&nbsp;
				</td>

				<td>
					&nbsp;
				</td>

				<td>
					&nbsp;
				</td>

				<td>
					<input type = "checkbox" name = "custom_tax_amount" value = "1" id = "custom_tax_amount" <?php if($custom_tax_amount==1){ print 'checked'; } ?> > Use custom tax amount <a href="#" data-toggle="tooltip" title="Enter your custom tax amount in the box to the left and then check this box to have the system override and save the tax amount to your custom value."><span class = "glyphicon glyphicon-info-sign">&nbsp;</span></a>
				</td>

			</tr>

			<tr>
				<td>
					Deposit Rate (decimal): 
				</td>

				<td>
					<input class="form-control" type="text" id="deposit_rate" name="deposit_rate" value="<?php print $deposit_rate; ?>" style = "width:150px;">
				</td>

				<td>
					Deposit Amount:
				</td>

				<td>
					<input type = "text" name = "deposit" id = "deposit" value = "<?php print $deposit; ?>"> 
				</td>
			</tr>

			<tr>
				<td>
					Credit Card Processing: 
				</td>

				<td>
					<input class="form-control" type="text" id="cc" name="cc" value="<?php print $cc; ?>" style = "width:150px;">
				</td>

				<td colspan="2">
					&nbsp;
				</td>
			</tr>

			<tr>

				<td>
					<a class = "btn btn-success" id = "update" href = "#">Update Amounts</a>
				</td>

				<td colspan="3">
					&nbsp;
				</td>

			</tr>

			<tr>
				<input type = "hidden" name = "invoice_id" id = "invoice_id" value = "<?php print $invoice_id; ?>">

				<input type = "hidden" name = "invoice_db_id" id = "invoice_db_id" value = "<?php print $invoice_db_id; ?>">

				<input type = "hidden" id = "action" value = "" name = "action">

				<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

				<td>
					<a class = "btn btn-success" id = "save" href = "#">SAVE AND COMPLETE</a>
				</td>

				<td colspan="3">
					&nbsp;
				</td>

			</tr>

		</table>
		</form>
	</div>

</div>

</div>
</div>

</section>
</section>

<!-- modal for tax type selection errors -->
<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" id = "tax_type_error_modal">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content" style = "padding:15px;">
    <h1>Warning</h1>

    <p id = "js-values"></p>

	<p>
		<a id = "review" class="popup-modal-dismiss btn btn-lg btn-success" href="#">Review</a>
		<a id = "proceed" class="btn btn-lg btn-primary" href="#">Proceed as is</a>
	</p>
    </div>
  </div>
</div>

<style>
/* The side navigation menu */
.sidenav {
    height: 100%; /* 100% Full-height */
    width: 0; /* 0 width - change this with JavaScript */
    position: fixed; /* Stay in place */
    z-index: 1; /* Stay on top */
    top: 0;
    left: 0;
    background-color: #111; /* Black*/
    overflow-x: hidden; /* Disable horizontal scroll */
    padding-top: 60px; /* Place content 60px from the top */
    transition: 0.5s; /* 0.5 second transition effect to slide in the sidenav */
}

/* The navigation menu links */
.sidenav a {
    padding: 8px 8px 8px 32px;
    text-decoration: none;
    font-size: 25px;
    color: #818181;
    display: block;
    transition: 0.3s
}

/* When you mouse over the navigation links, change their color */
.sidenav a:hover, .offcanvas a:focus{
    color: #f1f1f1;
}

/* Position and style the close button (top right corner) */
.sidenav .closebtn {
    position: absolute;
    top: 0;
    right: 25px;
    font-size: 36px;
    margin-left: 50px;
}

/* Style page content - use this if you want to push the page content to the right when you open the side navigation */
#main {
    transition: margin-left .5s;
    padding: 20px;
}

/* On smaller screens, where height is less than 450px, change the style of the sidenav (less padding and a smaller font size) */
@media screen and (max-height: 450px) {
    .sidenav {padding-top: 15px;}
    .sidenav a {font-size: 18px;}
}
</style>
<div id="print_preview" class="sidenav">
  	<a href="#" class="closebtn" id="close_preview" style = "margin-top:50px;clear:both;">X</a><br>
  	<div id = "preview_container"></div>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	// invoice preview
	$('#preview').click(function(e)
	{
		e.preventDefault();
		$('#topbar').hide();
		$('#print_preview').css('width','100%');
		$('#preview_container').html('');

		// load as iframe
		var id = "<?php print $project_id; ?>";
		var invoiceid = "<?php print $invoice_db_id; ?>";
		var iframesrc = "print_invoice.php?id="+id+"&invoiceid="+invoiceid;
		var iframe = '<iframe width = "1000" height="1200" src="'+iframesrc+'"></iframe>';
		$('#preview_container').html(iframe);
        //$('#preview-modal').modal('show');
	});
	
	// close invoice preview
	$('#close_preview').click(function(e)
	{
		e.preventDefault();
		$('#preview-container').html('');
        //$('#preview-modal').modal('hide');
        $('#print_preview').css('width','0');
        $('#topbar').show();
	});

	// date picker	
	$('.dp').datepicker();

	// text area fix (legacy)
    var d = $('#description').html();
    $("#description").html($.trim(d));

    // user made a change to any of these, set the opposite to 0
    $('#deposit_rate').on('input', function()
    {
    	$('#deposit').val(0);
    });
    $('#deposit').on('input', function()
    {
    	$('#deposit_rate').val(0);
    });
    $('#tax_rate').on('input', function()
    {
    	$('#tax_amount').val(0);
    });
    $('#tax_amount').on('input', function()
    {
    	$('#tax_rate').val(0);
    });

    // update button
    $('#update').click(function(e)
    {
    	e.preventDefault();
    	$('#action').val('7');
    	$('#np').submit();
    });

    // save button
    $('#save').click(function(e)
    {
    	e.preventDefault();
    	$('#action').val('5');
    	$('#np').submit();
    });

    // transfer button : show modal
    $('#transfer').magnificPopup(
    {
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,
		callbacks: 
		{
		    open: function() 
		    {
		    	// modal is opened; 
		    	// set the option list to select no value
		    	// set the button light gray
		    	$('#scope_of_work').val(0);
				$('#transfer_proposal').val('');
				$('#use-scope-of-work').css('opacity','.33');
		    },
		    close: function() 
		    {
		      // Will fire when popup is closed
		    }
		}		
	});

    // user changed the proposal number in the modal to 
	// something other than no value
	// set the opacity fo the use scope of work button to 33%
	$('#transfer_proposal').change(function()
	{
		var pid = $('#transfer_proposal').val();
		if(pid!='')
		{
			$('#scope_of_work').val(0);
			$('#use-scope-of-work').css('opacity','.33');
		}
	});

    // user pressed scope of work button
	$('#use-scope-of-work').click(function(e)
	{
		e.preventDefault();
		$('#scope_of_work').val(1);
		$('#transfer_proposal').val('');
		$('#use-scope-of-work').css('opacity','1');
	});
    
    // user pressed transfer button
    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();
    	
    	$('#error_1').hide();

    	var sow = $('#scope_of_work').val();

    	// use scope of work
    	if(sow==1)
    	{
    		$('#proposal').remove();
    		$('#action').val('8');
			$('#np').submit();
    	}
    	else
    	{
    		// user selected a proposal
    		var pid = $('#transfer_proposal').val();
    		//alert(pid);
    		if(pid=='')
    		{
    			$('#error_1').show();
    			return false;
    		}
    		else
    		{
    			//$('#proposal').val(pid).change();
    			$('#proposal').remove();
    			$('<input>').attr({
		            type: 'hidden',
		            id: 'proposal',
		            name: 'proposal',
		            value: pid
		        }).appendTo('#np');
    			$('#action').val('6');
				$('#np').submit();
    		}
    	}

    	// close the modal
    	$.magnificPopup.close();

    	$('#scope_of_work').val(0);

    });

    // modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    // delete an item
    $('.plus-delete').click(function(e)
    {
        e.preventDefault();
        $('#tax_amount_box').val(0);
        var item_id = this.id;
        $('<input>').attr({
            type: 'hidden',
            id: 'item_id',
            name: 'item_id',
            value: item_id
        }).appendTo('#np');
        $('#action').val('2');
        $('#np').submit();
    });

    // update items button
    $('#update-items').click(function(e)
    {
        e.preventDefault();

        // clear tax amount
        $('#tax_amount_box').val('');

        var error_count=0;

        // loop through every tax amount box
        // if something is in this box, check the tax type
        // if tax type is empty increase error count
        $('.amount').each(function()
        {
        	var v=$(this).val();
        	if( (v!=0) && (v!='') )
        	{
        		var dbid = $(this).data('dbid');
        		var sel = $('#tax-'+dbid).val();
        		if(sel=='')
        		{
        			error_count++;
        			$('#tax-'+dbid).css('border','1px solid red');
        		}
        	}
        });

        // default blank row
        var da = $('#amount').val();
        if( (da!=0) && (da!='') )
        {
        	var ta = $('#tax_type').val();
        	if(ta=='')
        	{
        		error_count++;
        		$('#tax_type').css('border','1px solid red');
        	}
        }

        if(error_count>0)
        {
        	var error_msg='One or more line items was not assigned a tax rate. Do you wish to proceed anyway?';

			//tax_type_error_modal
			$('#js-values').html(error_msg);
			$('#tax_type_error_modal').modal('show');
			return false;
        }

        // submit form
        $('#action').val('1');
        $('#np').submit();
    });

	// proceed button was pressed
    $('#proceed').click(function(e)
    {
        e.preventDefault();

        // submit form
        $('#action').val('1');
        $('#np').submit();
    });

    // review button 
    $('#review').click(function(e)
    {
        e.preventDefault();
        $('#tax_type_error_modal').modal('hide');
    });

    // auto resize text areas to fit the height of line item text
    $('.existing-line-item').each(function()
    {
    	$(this).height( $(this)[0].scrollHeight );
    });

    $('.existing-line-item').change(function()
    {
    	$(this).height( $(this)[0].scrollHeight );
    });

});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
