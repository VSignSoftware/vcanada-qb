<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$config = $vujade->get_setup();
$id = 2;
$invoice_setup = $vujade->get_invoice_company_setup($id);
$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
if($action==1)
{
	$s = array();

	$s[]=$vujade->update_row('invoice_company_setup',$id,'name',$_POST['name']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'address_1',$_POST['address_1']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'address_2',$_POST['address_2']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'city',$_POST['city']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'state',$_POST['state']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'zip',$_POST['zip']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'license_number',$_POST['license_number']);
	$s[]=$vujade->update_row('invoice_company_setup',$id,'phone',$_POST['phone']);
	$vujade->messages[]="Changes Saved.";
	$invoice_setup = $vujade->get_invoice_company_setup($id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Invoice Company Setup - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">
        	
			<?php
        	$ss_menu=6;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="width: 100%">

		<div class="pl20 pr50">

			<?php
			$vujade->show_errors();
			$vujade->show_messages();
			$setup=$invoice_setup;
			?>

			<div class="panel panel-primary panel-border top">
				<div class="panel-body bg-light">

               	<!-- content goes here -->
					<form method = "post" action = "invoice_company_setup.php">
						<strong>Company Name</strong><br>
						<input type = "text" name = "name" class="form-control" value = "<?php print $setup['name']; ?>">
						<br>
						<br>
						<strong>Address Line 1</strong><br>
						<input type = "text" name = "address_1" class="form-control" value = "<?php print $setup['address_1']; ?>">
						<br>
						<br>
						<strong>Address Line 2</strong><br>
						<input type = "text" name = "address_2" class="form-control" value = "<?php print $setup['address_2']; ?>">
						<br>
						<br>
						<strong>City</strong><br>
						<input type = "text" name = "city" class="form-control" value = "<?php print $setup['city']; ?>">
						<br>
						<br>
						<strong>State / Province</strong><br>
						<select id="state" name="state" style = "width:200px;" class = "form-control">
			              <?php
			              if(isset($setup['state']))
			              {
			                  print '<option value = "'.$setup['state'].'" selected = "selected">'.$setup['state'].'</option>';
			                  //print '<option value = "">-State-</option>';
			              }
			              else
			              {
			                  //print '<option value = "">-State-</option>';
			              }
			              if($config['country']=="USA")
			              {
			              ?>
				              <option value="">-US States-</option>
				              <option value="AL">Alabama</option>
				              <option value="AK">Alaska</option>
				              <option value="AZ">Arizona</option>
				              <option value="AR">Arkansas</option>
				              <option value="CA">California</option>
				              <option value="CO">Colorado</option>
				              <option value="CT">Connecticut</option>
				              <option value="DE">Delaware</option>
				              <option value="DC">District Of Columbia</option>
				              <option value="FL">Florida</option>
				              <option value="GA">Georgia</option>
				              <option value="HI">Hawaii</option>
				              <option value="ID">Idaho</option>
				              <option value="IL">Illinois</option>
				              <option value="IN">Indiana</option>
				              <option value="IA">Iowa</option>
				              <option value="KS">Kansas</option>
				              <option value="KY">Kentucky</option>
				              <option value="LA">Louisiana</option>
				              <option value="ME">Maine</option>
				              <option value="MD">Maryland</option>
				              <option value="MA">Massachusetts</option>
				              <option value="MI">Michigan</option>
				              <option value="MN">Minnesota</option>
				              <option value="MS">Mississippi</option>
				              <option value="MO">Missouri</option>
				              <option value="MT">Montana</option>
				              <option value="NE">Nebraska</option>
				              <option value="NV">Nevada</option>
				              <option value="NH">New Hampshire</option>
				              <option value="NJ">New Jersey</option>
				              <option value="NM">New Mexico</option>
				              <option value="NY">New York</option>
				              <option value="NC">North Carolina</option>
				              <option value="ND">North Dakota</option>
				              <option value="OH">Ohio</option>
				              <option value="OK">Oklahoma</option>
				              <option value="OR">Oregon</option>
				              <option value="PA">Pennsylvania</option>
				              <option value="RI">Rhode Island</option>
				              <option value="SC">South Carolina</option>
				              <option value="SD">South Dakota</option>
				              <option value="TN">Tennessee</option>
				              <option value="TX">Texas</option>
				              <option value="UT">Utah</option>
				              <option value="VT">Vermont</option>
				              <option value="VA">Virginia</option>
				              <option value="WA">Washington</option>
				              <option value="WV">West Virginia</option>
				              <option value="WI">Wisconsin</option>
				              <option value="WY">Wyoming</option>
				              <option value="">---------------</option>
				              <option value="">-Canadian Provinces-</option>
				              <option value="Alberta">Alberta</option>
				              <option value="British Columbia">British Columbia</option>
				              <option value="Manitoba">Manitoba</option>
				              <option value="New Brunswick">New Brunswick</option>
				              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
				              <option value="Northwest Territories">Northwest Territories</option>
				              <option value="Nova Scotia">Nova Scotia</option>
				              <option value="Nunavut">Nunavut</option>
				              <option value="Ontario">Ontario</option>
				              <option value="Prince Edward Island">Prince Edward Island</option>
				              <option value="Quebec">Quebec</option>
				              <option value="Saskatchewan">Saskatchewan</option>
				              <option value="Yukon">Yukon</option>
				        <?php 
				    	} 
				    	// canadian servers
				    	if($config['country']=='Canada')
				    	{
				    		?>
				    		  <option value="">-Canadian Provinces-</option>
				              <option value="Alberta">Alberta</option>
				              <option value="British Columbia">British Columbia</option>
				              <option value="Manitoba">Manitoba</option>
				              <option value="New Brunswick">New Brunswick</option>
				              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
				              <option value="Northwest Territories">Northwest Territories</option>
				              <option value="Nova Scotia">Nova Scotia</option>
				              <option value="Nunavut">Nunavut</option>
				              <option value="Ontario">Ontario</option>
				              <option value="Prince Edward Island">Prince Edward Island</option>
				              <option value="Quebec">Quebec</option>
				              <option value="Saskatchewan">Saskatchewan</option>
				              <option value="Yukon">Yukon</option>
				              <option value="">---------------</option>
				              <option value="">-US States-</option>
				              <option value="AL">Alabama</option>
				              <option value="AK">Alaska</option>
				              <option value="AZ">Arizona</option>
				              <option value="AR">Arkansas</option>
				              <option value="CA">California</option>
				              <option value="CO">Colorado</option>
				              <option value="CT">Connecticut</option>
				              <option value="DE">Delaware</option>
				              <option value="DC">District Of Columbia</option>
				              <option value="FL">Florida</option>
				              <option value="GA">Georgia</option>
				              <option value="HI">Hawaii</option>
				              <option value="ID">Idaho</option>
				              <option value="IL">Illinois</option>
				              <option value="IN">Indiana</option>
				              <option value="IA">Iowa</option>
				              <option value="KS">Kansas</option>
				              <option value="KY">Kentucky</option>
				              <option value="LA">Louisiana</option>
				              <option value="ME">Maine</option>
				              <option value="MD">Maryland</option>
				              <option value="MA">Massachusetts</option>
				              <option value="MI">Michigan</option>
				              <option value="MN">Minnesota</option>
				              <option value="MS">Mississippi</option>
				              <option value="MO">Missouri</option>
				              <option value="MT">Montana</option>
				              <option value="NE">Nebraska</option>
				              <option value="NV">Nevada</option>
				              <option value="NH">New Hampshire</option>
				              <option value="NJ">New Jersey</option>
				              <option value="NM">New Mexico</option>
				              <option value="NY">New York</option>
				              <option value="NC">North Carolina</option>
				              <option value="ND">North Dakota</option>
				              <option value="OH">Ohio</option>
				              <option value="OK">Oklahoma</option>
				              <option value="OR">Oregon</option>
				              <option value="PA">Pennsylvania</option>
				              <option value="RI">Rhode Island</option>
				              <option value="SC">South Carolina</option>
				              <option value="SD">South Dakota</option>
				              <option value="TN">Tennessee</option>
				              <option value="TX">Texas</option>
				              <option value="UT">Utah</option>
				              <option value="VT">Vermont</option>
				              <option value="VA">Virginia</option>
				              <option value="WA">Washington</option>
				              <option value="WV">West Virginia</option>
				              <option value="WI">Wisconsin</option>
				              <option value="WY">Wyoming</option>
				    		<?php
				    	}
				    	?>
			            </select>

						<br>
						<br>
						<strong>Zip / Postal Code</strong><br>
						<input type = "text" name = "zip" class="form-control" value = "<?php print $setup['zip']; ?>">
						<br>
						<br>
						<strong>Phone</strong><br>
						<input type = "text" name = "phone" class="form-control" value = "<?php print $setup['phone']; ?>">
						<br>
						<br>
						<?php
						if($config['country']=="Canada")
						{
							//$label = 'HST #';
							print $config['company_license_label'].' #';
						}
						else
						{
							$label = 'License Number';
						}
						?>
						<strong><?php print $label; ?></strong><br>
						<input type = "text" name = "license_number" class="form-control" value = "<?php print $setup['license_number']; ?>">
						<br>
						<br>
						<input type = "hidden" name = "id" value = "<?php print $id; ?>">
						<input type = "hidden" name = "action" value = "1">
						<p><input type = "submit" value = "Save" class = "btn btn-primary"></p>
					</form>
				</div>
			</div>
		</div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

 <!-- BEGIN: PAGE SCRIPTS -->

 <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
   	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
