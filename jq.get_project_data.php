<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);
$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);

// send basic data back in json
if(!isset($_REQUEST['column']))
{
	$json = array();
	$json['address_1']=$project['address_1'];
	$json['address_2']=$project['address_2'];
	$json['city']=$project['city'];
	$json['state']=$project['state'];
	$json['zip']=$project['zip'];
	$json['project_contact']=$project['project_contact'];
	$json['project_contact_phone']=$project['project_contact_phone'];
	$json['project_contact_email']=$project['project_contact_email'];
	$json['company']=$project['site'];

	// split contact into two names
	$pc = @explode(" ",$json['project_contact']);
	$fn = $pc[0];
	$ln = $pc[1];

	$json['project_contact_fn']=$fn;
	$json['project_contact_ln']=$ln;

	$json['site_contact']=$project['site_contact'];
	$json['site_contact_phone']=$project['site_contact_phone'];
	$json['site_contact_email']=$project['site_contact_email'];
	print json_encode($json);
}
else
{
	// send specific data back as string
	$column=$_REQUEST['column'];
	print $project[$column];
}
?>