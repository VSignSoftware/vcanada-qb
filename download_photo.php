<?php
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$id = $_REQUEST['id'];
$project_id = $_REQUEST['project_id'];
$vujade->connect();

$zip = 0;
if(isset($_REQUEST['zip']))
{
	$zip = $_REQUEST['zip'];
}
if($zip==1)
{
	// zip all 
	$folder = "uploads/images/".$project_id;
	$oldzip=$folder."/".$project_id."-all-files.zip";
	if(file_exists($oldzip))
	{
		unlink($oldzip);
	}
	$new_zip=$folder."/".$project_id."-all-files.zip";
	$zipcmd = $vujade->create_zip($folder, $new_zip);
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"" . basename($new_zip) . "\""); 
	readfile($new_zip);
	if(file_exists($new_zip))
	{
		unlink($new_zip);
	}
}
else
{
	$pic = $vujade->get_site_photo($id);
	$file_url = 'uploads/images/'.$project_id.'/'.$pic['filename'];
	header('Content-Type: application/octet-stream');
	header("Content-Transfer-Encoding: Binary"); 
	header("Content-disposition: attachment; filename=\"" . basename($file_url) . "\"");
	readfile($file_url);
}
?>