<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$pateHTML = '<html><head>';
$pateHTML.='<style>';
$pateHTML.='
body
{
	font-size:12px;
	font-family:arial;
	color:black;
}
/* calendar */
table.calendar		{ border-left:1px solid #999; }
tr.calendar-row	{  }
td.calendar-day	{ min-height:150px; font-size:11px; position:relative; height:150px; } * html div.calendar-day { height:150px; }
td.calendar-day:hover	{ background:#eceff5; }
td.calendar-day-np	{ background:#eee; min-height:150px; height:150px;} * html div.calendar-day-np { height:150px; }
td.calendar-day-head { background:#ccc; font-weight:bold; text-align:center; width:120px; padding:5px; border-bottom:1px solid #999; border-top:1px solid #999; border-right:1px solid #999; }
div.day-number		{ background:#999; padding:5px; color:#fff; font-weight:bold; float:right; margin:-5px -5px 0 0; width:20px; text-align:center; }
/* shared */
td.calendar-day, td.calendar-day-np { width:120px; padding:5px; border-bottom:1px solid #999; border-right:1px solid #999; }
';
$pateHTML.='</style>';
$pateHTML.='</head><body>';
$pateHTML .= '
<div id="content">
<br style="clear: both;"/>
<div>
	<div style = "width:400px;float:left">
	<h2 class="signInText"><h2>'.date('F').' '.date('Y').'</h2></h2>
	</div>
</div>';
$schedule = $vujade->get_schedule(date('m'),date('Y'));
$pateHTML.=$schedule;
$pateHTML.='</div>
</body>
</html>';