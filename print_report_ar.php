<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

ini_set("memory_limit","300M");

## permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();

// get data for the last 30 days

$now = strtotime('now');
$thirty = strtotime('-30 days');
$thirtyone = strtotime('-31 days');
$fortyfive = strtotime('-45 days');
$fortysix = strtotime('-46 days');
$sixty = strtotime('-60 days');
$sixtyone = strtotime('-61 days');

$projects = $vujade->get_ar_report_data();

if($projects['error']=="0")
{

	$title = 'Accounts Receivable Report';
	$html = '<html><head><title>Accounts Receivable Report</title>';
	$html.='<style>';
	$html.='
	body
	{
		font-size:12px;
		font-family:arial;
		color:black;
	}
	table 
	{
	    border-collapse: collapse;
	}
	table, td, th 
	{
	    border: 1px solid #cecece;
	}
	td
	{

	}
	';
	$html.='</style>';
	$html.='</head><body>';
	$header_1='<h2>Accounts Receivable Report</h2>';

	// table
	$html.='
	<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
			<thead>
				<tr style = "border-bottom:1px solid black;">
					<td valign = "top" width = "25%"><strong>Job Name</strong></td>
					<td valign = "top"><strong>Invoice #</strong></td>
					<td valign = "top"><strong>Salesperson</strong></td>
					<td valign = "top"><strong>Date</strong></td>
					<td valign = "top"><strong>Amount</strong></td>
					<td valign = "top"><strong>Current</strong></td>
					<td valign = "top"><strong>31 to 45 Days</strong></td>
					<td valign = "top"><strong>46 to 60 Days</strong></td>
					<td valign = "top"><strong>Over 60 Days</strong></td>
					<td valign = "top"><strong>Days</strong></td>';
					
	$html.='</tr></thead>';
	$html.='<tbody style = "font-size:14px;">';

	if($projects['count']>0)
	{
		$invoice_count=$projects['count'];

		// clear these no longer needed
		unset($projects['error']);	
		unset($projects['count']);
		unset($projects['sql']);	

		// clear zero balances
		foreach($projects as $k => $i)
		{
			if($i['balance_due']=="0")
			{
				unset($projects[$k]);
			}
			//print $i['invoice']. ' ';
			//print $i['balance_due'].'<hr>';
		}
		//die;

		// table body
		$html.='<tbody style = "font-size:14px;">';

		// grand totals
		$gt1=0;
		$gt2=0;
		$gt3=0;
		$gt4=0;

		// counts for non-ad invoice days overdue + totals
		$step1 = 0;
		$step2 = 0;
		$step3 = 0;
		$step4 = 0;
		$step1t = 0;
		$step2t = 0;
		$step3t = 0;
		$step4t = 0;
		$total=0;

		// must be advanced deposit or included in costing

		/*
		not advanced deposit but included in costing
		advanced deposit and not included in costing
		advanced deposit and included in costing
		*/

		// non-advanced deposits and included in costing
		$html.='<tr><td colspan="10"><strong><u>Invoices</u></strong></td></tr>';
		foreach($projects as $i)
		{
			if( ($i['ad']==0) && ($i['costing']==1) )
			{

				$overdue=0;
				$overdue = $now-$i['invoice_date_ts'];
				$overdue = $overdue/86400;
				$overdue = round($overdue);

		        $html.='<tr>';

		        // customer name (was job number)
		        //$customer = $vujade->get_customer($i['customer_id']);
		        $html.='<td valign = "top">'.$i['customer_name'].'</td>';
		        //$html.='<td valign = "top">'.$i['project_name'].'<br>costing: '.$i['costing'].'</td>';

		        // invoice number
		        $html.='<td valign = "top">'.$i['invoice'].'</td>';

		        // salesperson
		        $html.='<td valign = "top">'.$i['salesperson'].'</td>';

		        // date
		        $html.='<td valign = "top">'.$i['invoice_date'].'</td>';

		        // amount
		        $bd = number_format($i['balance_due'],2);
		        $html.='<td valign = "top" style = "text-align:right;"><div style = "text-align:right;">'.$bd.'</div></td>';

		        // current (invoice is less than 30 days old)
				if($overdue<=30)
				{
					$html.='<td valign = "top" style = "text-align:right;"><div style = "text-align:right;">'.$bd.'</div></td>';
					$step1+=1;
					$step1t+=$i['balance_due'];
					
				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}
		        
		        // 31 to 45 Days
		        if( ($overdue>=31) && ($overdue<=45) )
				{
					$html.='<td valign = "top" style = "text-align:right;"><div style = "text-align:right;">'.$bd.'</div></td>';
					$step2+=1;
					$step2t+=$i['balance_due'];

				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}

		        // 46 to 60 Days
		        if( ($overdue>=46) && ($overdue<=60) )
				{
					$html.='<td valign = "top" style = "text-align:right;"><div style = "text-align:right;">'.$bd.'</div></td>';
					$step3+=1;
					$step3t+=$i['balance_due'];

				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}

		        // over 60 days
		        if($overdue>60)
				{
					$html.='<td valign = "top" style = "text-align:right;"><div style = "text-align:right;">'.$bd.'</div></td>';
					$step4+=1;
					$step4t+=$i['balance_due'];

				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}

		        // days overdue
		        $html.='<td valign = "top" style = "text-align:right;">'.$overdue.'</td>';

		        $html.='</tr>';

		        $total+=$i['balance_due'];
		    }
		}

		// blank row
		//$html.='<tr><td colspan="10">&nbsp;</td></tr>';

		// totals
		$html.='<tr><td colspan="4">Invoice Totals</td>';
		$html.='<td style = "text-align:right;">'.@number_format($total,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step1t,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step2t,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step3t,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step4t,2);
		$html.='</td>';
		$html.='<td>&nbsp;';
		$html.='</td>';
		$html.='</tr>';

		// blank row
		//$html.='<tr><td colspan="10">&nbsp;</td></tr>';

		// percentage of totals
		$html.='<tr><td colspan="5">Percentage of Total</td>';
		$step1 = round($step1/$invoice_count,2);
		$step1=$step1*100;
		$html.='<td style = "text-align:right;">'.$step1.'%';
		$html.='</td>';

		$step2 = round($step2/$invoice_count,2);
		$step2=$step2*100;
		$html.='<td style = "text-align:right;">'.$step2.'%';
		$html.='</td>';
		
		$step3 = round($step3/$invoice_count,2);
		$step3=$step3*100;
		$html.='<td style = "text-align:right;">'.$step3.'%';
		$html.='</td>';

		$step4 = round($step4/$invoice_count,2);
		$step4=$step4*100;
		$html.='<td style = "text-align:right;">'.$step4.'%';
		$html.='</td>';

		$html.='<td>&nbsp;';
		$html.='</td>';
		$html.='</tr>';

		// advanced deposits
		// counts for non-ad invoice days overdue + totals
		$gt1+=$step1t;
		$gt2+=$step2t;
		$gt3+=$step3t;
		$gt4+=$step4t;
		$step1 = 0;
		$step2 = 0;
		$step3 = 0;
		$step4 = 0;
		$step1t = 0;
		$step2t = 0;
		$step3t = 0;
		$step4t = 0;
		$total2 = 0;

		// blank row
		$html.='<tr><td colspan="10">&nbsp;</td></tr>';
		$html.='<tr><td colspan="10"><strong><u>Advance Deposits</u></strong></td></tr>';

		// advanced deposits
		foreach($projects as $i)
		{
			if($i['ad']==1)
			{
				$overdue=0;
				$overdue = $now-$i['invoice_date_ts'];
				$overdue = $overdue/86400;
				$overdue = round($overdue);

		        $html.='<tr>';

		        // customer name
		        //$html.='<td valign = "top">'.$i['project_name'].'<br>costing: '.$i['costing'].'</td>';
		        $html.='<td valign = "top">'.$i['customer_name'].'</td>';
		        
		        // invoice number
		        $html.='<td valign = "top">'.$i['invoice'].'</td>';

		        // salesperson
		        $html.='<td valign = "top">'.$i['salesperson'].'</td>';

		        // date
		        $html.='<td valign = "top">'.$i['invoice_date'].'</td>';

		        // amount
		        $bd = number_format($i['balance_due'],2);
		        $html.='<td valign = "top" style = "text-align:right;">'.$bd.'</td>';

		        // current (invoice is less than 30 days old)
				if($overdue<=30)
				{
					$html.='<td valign = "top" style = "text-align:right;">'.$bd.'</td>';
					$step1+=1;
					$step1t+=$i['balance_due'];

				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}
		        
		        // 31 to 45 Days
		        if( ($overdue>=31) && ($overdue<=45) )
				{
					$html.='<td valign = "top" style = "text-align:right;">'.$bd.'</td>';
					$step2+=1;
					$step2t+=$i['balance_due'];
				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}

		        // 46 to 60 Days
		        if( ($overdue>=46) && ($overdue<=60) )
				{
					$html.='<td valign = "top" style = "text-align:right;">'.$bd.'</td>';
					$step3+=1;
					$step3t+=$i['balance_due'];
				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}

		        // over 60 days
		        if($overdue>60)
				{
					$html.='<td valign = "top" style = "text-align:right;">'.$bd.'</td>';
					$step4+=1;
					$step4t+=$i['balance_due'];
				}
				else
				{
					$html.='<td valign = "top">&nbsp;</td>';
				}

		        // days overdue
		        $html.='<td valign = "top" style = "text-align:right;">'.$overdue.'</td>';

		        $html.='</tr>';

		        $total2+=$i['balance_due'];
		    }
		}

		// blank row
		//$html.='<tr><td colspan="10">&nbsp;</td></tr>';

		// totals
		$html.='<tr><td colspan="4">Advanced Deposit Totals</td>';
		$html.='<td style = "text-align:right;">'.@number_format($total2,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step1t,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step2t,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step3t,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($step4t,2);
		$html.='</td>';
		$html.='<td>&nbsp;';
		$html.='</td>';
		$html.='</tr>';

		// blank row
		//$html.='<tr><td colspan="10">&nbsp;</td></tr>';

		// percentage of totals
		$html.='<tr><td colspan="5">Percentage of Total</td>';
		$step1 = round($step1/$invoice_count,2);
		$step1=$step1*100;
		$html.='<td style = "text-align:right;">'.$step1.'%';
		$html.='</td>';

		$step2 = round($step2/$invoice_count,2);
		$step2=$step2*100;
		$html.='<td style = "text-align:right;">'.$step2.'%';
		$html.='</td>';
		
		$step3 = round($step3/$invoice_count,2);
		$step3=$step3*100;
		$html.='<td style = "text-align:right;">'.$step3.'%';
		$html.='</td>';

		$step4 = round($step4/$invoice_count,2);
		$step4=$step4*100;
		$html.='<td style = "text-align:right;">'.$step4.'%';
		$html.='</td>';

		$html.='<td>&nbsp;';
		$html.='</td>';
		$html.='</tr>';

		// blank row
		$html.='<tr><td colspan="10">&nbsp;</td></tr>';

		// grand total
		$html.='<tr><td colspan="4">Accounts Receivable Grand Totals</td>';

		// totals
		$gt1+=$step1t;
		$gt2+=$step2t;
		$gt3+=$step3t;
		$gt4+=$step4t;
		$total3 = $total+$total2;
		$html.='<td style = "text-align:right;">'.@number_format($total3,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($gt1,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($gt2,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($gt3,2);
		$html.='</td>';
		$html.='<td style = "text-align:right;">'.@number_format($gt4,2);
		$html.='</td>';
		$html.='<td>&nbsp;';
		$html.='</td>';
		$html.='</tr>';

		$html.='</tbody></table>';
	}
	else
	{
		$html.='No projects found.';
	}
	$html.="</tbody></table></body></html>";

	$footer_1 = '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';

	# mpdf class (pdf output)
	include("mpdf60/mpdf.php");
	$mpdf = new mPDF('', 'LETTER-L', 0, 'Helvetica', 10, 10, 20, 10, 10);
	$mpdf->DefHTMLHeaderByName('header_1',$header_1);
	$mpdf->SetHTMLHeaderByName('header_1');
	$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
	$mpdf->SetHTMLFooterByName('footer_1');
	$mpdf->WriteHTML($html);
	
	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-ar-report.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
	 	$mpdf->Output($pdfname,'F');
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		$mpdf->Output('AR Report.pdf','I'); 
	}
}
else
{
	$vujade->set_error('No projects found. ');
	$vujade->set_error($projects['error']);
	$vujade->show_errors();
}
?>