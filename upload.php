<?php
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$designid = $_REQUEST['design_id'];
$type = $_REQUEST['type'];
$ds = DIRECTORY_SEPARATOR;
$vujade->connect();

$m='';
foreach($_REQUEST as $k => $v)
{
	$m.=$k.' -> '.$v."\n";
}

//mail('toddhamm30@gmail.com','test',$m);

# design files
if($type==1)
{
    $project_id = $_REQUEST['project_id'];
    $storeFolder = 'uploads/design_files/'.$project_id;
    if(!is_dir($storeFolder))
    {
        mkdir($storeFolder);
    }
}

# production files
if($type==2)
{
    $project_id = $_REQUEST['project_id'];
    $storeFolder = 'uploads/production_files/'.$project_id;
    if(!is_dir($storeFolder))
    {
        mkdir($storeFolder);
    }
}

# documents
if($type==3)
{
    $project_id = $_REQUEST['project_id'];
    $storeFolder = 'uploads/documents/'.$project_id;
    if(!is_dir($storeFolder))
    {
        mkdir($storeFolder);
    }
}

# employee docs
if($type==4)
{
    $employee_id=$_REQUEST['employee_id'];
    if(!is_dir('uploads/employee_documents/'.$employee_id))
    {
        mkdir('uploads/employee_documents/'.$employee_id);
    }
    $storeFolder = 'uploads/employee_documents/'.$employee_id;
}

# vendor docs
if($type==5)
{
    $vendor_id=$_REQUEST['vendor_id'];
    if(!is_dir('uploads/vendor_documents/'.$vendor_id))
    {
        mkdir('uploads/vendor_documents/'.$vendor_id);
    }
    $storeFolder = 'uploads/vendor_documents/'.$vendor_id;
}

# customer docs
if($type==6)
{
    $customer_id=$_REQUEST['customer_id'];
    if(!is_dir('uploads/customer_documents/'.$customer_id))
    {
        mkdir('uploads/customer_documents/'.$customer_id);
    }
    $storeFolder = 'uploads/customer_documents/'.$customer_id;
}

if (!empty($_FILES)) 
{
	//$_FILES['file']['tmp_name'];
 	//$info = pathinfo($_FILES['file']['name']);

	/*
 	foreach($_FILES as $key => $value)
 	{
 		$msg .= $key.' -> '.$value."\n";
 		foreach($value as $k => $v)
 		{
 			$msg .= $k.' -> '.$v."\n";
 		}
 	}
	*/

 	// unlink any existing files with the same name
 	// and delete the database reference

 	// design files
 	if($type==1)
 	{
 		
 		$msg = '';
 		$e = '';
 		$count = '';
		$mysqli=$vujade->mysqli;
		$sql = "SELECT * FROM `files` WHERE `design_id` = '$designid'";
		$result = $mysqli->query($sql);
		$count = $result->num_rows;
		if(!$result)
		{
			$e="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		else
		{
			while($rows = $result->fetch_assoc())
			{
				if($rows['file_name']==$_FILES['file']['name'])
    			{
    				@unlink('uploads/design_files/'.$project_id.'/'.$rows['file_name']);
					$msg.=$rows['file_name']."\n";
					$s = $vujade->delete_row('files',$rows['id']);
					$msg.=$rows['id']."\n\n";
    			}
			}
		}

 		//mail('toddhamm30@gmail.com','test',$msg.' SQL: '.$sql.' Error: '.$e.' Count: '.$count);
 	}

 	// production files
 	if($type==2)
 	{
 		$msg = '';
 		$e = '';
 		$count = '';
		$mysqli=$vujade->mysqli;
		$sql = "SELECT * FROM `production_files` WHERE `project_id` = '$project_id'";
		$result = $mysqli->query($sql);
		$count = $result->num_rows;
		if(!$result)
		{
			$e="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		else
		{
			while($rows = $result->fetch_assoc())
			{
				if($rows['file']==$_FILES['file']['name'])
    			{
    				@unlink('uploads/production_files/'.$project_id.'/'.$rows['file']);
					$msg.=$rows['file']."\n";
					$s = $vujade->delete_row('production_files',$rows['id']);
					$msg.=$rows['id']."\n\n";
    			}
			}
		}
 	}

 	// documents
 	if($type==3)
 	{
 		$msg = '';
 		$e = '';
 		$count = '';
		$mysqli=$vujade->mysqli;
		$sql = "SELECT * FROM `documents` WHERE `project_id` = '$project_id'";
		$result = $mysqli->query($sql);
		$count = $result->num_rows;
		if(!$result)
		{
			$e="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
		}
		else
		{
			while($rows = $result->fetch_assoc())
			{
				if($rows['file']==$_FILES['file']['name'])
    			{
    				@unlink('uploads/documents/'.$project_id.'/'.$rows['file']);
					$msg.=$rows['file']."\n";
					$s = $vujade->delete_row('documents',$rows['id']);
					$msg.=$rows['id']."\n\n";
    			}
			}
		}
 	}

 	$filename = $_FILES['file']['name'];
    $tempFile = $_FILES['file']['tmp_name'];             
    $targetPath = dirname( __FILE__ ) . $ds. $storeFolder . $ds;  
    $targetFile =  $targetPath.$filename;  

    //mail('toddhamm30@gmail.com','test',$msg);

    move_uploaded_file($tempFile,$targetFile); 

    $vujade->connect();

    # file is a design file
    if($type==1)
    {
    	$vujade->create_row('files');
    	$rowid=$vujade->row_id;
    	$s=array();
    	$s[]=$vujade->update_row('files',$rowid,'design_id',$designid);
    	$s[]=$vujade->update_row('files',$rowid,'file_name',$filename);
    }

    # file is a production file
    if($type==2)
    {
        $project_id=$_REQUEST['project_id'];
        $folder_id=$_REQUEST['folder_id'];
        $vujade->create_row('production_files');
        $rowid=$vujade->row_id;
        $s=array();
        $s[]=$vujade->update_row('production_files',$rowid,'project_id',$project_id);
        $s[]=$vujade->update_row('production_files',$rowid,'folder_id',$folder_id);
        $s[]=$vujade->update_row('production_files',$rowid,'file',$filename);
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s[]=$vujade->update_row('production_files',$rowid,'file_type',$ext); 

        // file upload errors
        $s[]=$vujade->update_row('production_files',$rowid,'upload_error',$_FILES['file']['error']); 

    }

    # file is a document
    if($type==3)
    {
        $project_id=$_REQUEST['project_id'];
        $folder_id=$_REQUEST['folder_id'];
        $type_id=$_REQUEST['type_id'];
        $vujade->create_row('documents');
        $rowid=$vujade->row_id;
        $s=array();
        $s[]=$vujade->update_row('documents',$rowid,'project_id',$project_id);
        $s[]=$vujade->update_row('documents',$rowid,'folder_id',$folder_id);
        $s[]=$vujade->update_row('documents',$rowid,'file',$filename);
        $s[]=$vujade->update_row('documents',$rowid,'file_type',$type_id);
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s[]=$vujade->update_row('documents',$rowid,'ext',$ext);  
    }

    # file is an employee document
    if($type==4)
    {
        $employee_id=$_REQUEST['employee_id'];
        $vujade->create_row('documents');
        $rowid=$vujade->row_id;
        $s=array();
        $s[]=$vujade->update_row('documents',$rowid,'project_id',$employee_id);
        $s[]=$vujade->update_row('documents',$rowid,'file',$filename);
        //$s[]=$vujade->update_row('documents',$rowid,'file_type',$type_id);
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s[]=$vujade->update_row('documents',$rowid,'ext',$ext);  
    }  

    # file is an vendor document
    if($type==5)
    {
        $vendor_id=$_REQUEST['vendor_id'];
        $vujade->create_row('documents');
        $rowid=$vujade->row_id;
        $s=array();
        $s[]=$vujade->update_row('documents',$rowid,'project_id',$vendor_id);
        $s[]=$vujade->update_row('documents',$rowid,'file',$filename);
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s[]=$vujade->update_row('documents',$rowid,'ext',$ext);  
    } 
    
    # file is a customer document
    if($type==6)
    {
        $customer_id=$_REQUEST['customer_id'];
        $vujade->create_row('documents');
        $rowid=$vujade->row_id;
        $s=array();
        $s[]=$vujade->update_row('documents',$rowid,'project_id',$customer_id);
        $s[]=$vujade->update_row('documents',$rowid,'file',$filename);
        $path = $_FILES['file']['name'];
        $ext = pathinfo($path, PATHINFO_EXTENSION);
        $s[]=$vujade->update_row('documents',$rowid,'ext',$ext);  
    } 

}
?>     