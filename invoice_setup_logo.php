<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
if($action==1)
{
	# file must be 360 wide x 98 tall
	$image_info=getimagesize($_FILES["logo"]["tmp_name"]);
	$width = $image_info[0];
	$height = $image_info[1];
	
	# other variables available from get image size
	# type (numeric value)
	# attributes (width and height)
	# bits
	# channels
	# mime

	# get the extension
	//$ext = pathinfo($_FILES["logo"]["tmp_name"], PATHINFO_EXTENSION);

	/* */

	if($width>"260")
	{
		//$vujade->errors[]="Image width is larger tha 260 pixels.";
	}
	if($height>"60")
	{
		//$vujade->errors[]="Image height is larger than 60 pixels.";
	}

	/* does not work...
	# file must be jpg
	if($ext!=".jpg")
	{
		$vujade->errors[]="Image must be .jpg format.";
		print $ext;
		die;
	}
	*/

	# if no errors, attempt to upload the new logo
	$e1 = $vujade->get_error_count();
	if($e1<=0)
	{
		$uploadfile = 'images/invoice_logo.jpg';
		if(move_uploaded_file($_FILES['logo']['tmp_name'], $uploadfile)) 
		{
			$s2=$vujade->update_row('cms_logo',3,'url',$uploadfile);  
			$success=1;
		} 
		else 
		{
			$ec = $_FILES['logo']['error']; 
			if($ec==1)
			{
				$vujade->errors[]="The uploaded file is too large.";
			}
			if($ec==3)
			{
				$vujade->errors[]="The uploaded file was only partially uploaded.";
			}
			if($ec==4)
			{
				$vujade->errors[]="No file was uploaded.";
			}
			# there is no error code 5....
			if($ec==6)
			{
				$vujade->errors[]="Could not upload file to the server. Error Code 6.";
			}
			if($ec==7)
			{
				$vujade->errors[]="Could not upload file to the server. Error Code 7.";
			}
			if($ec==8)
			{
				$vujade->errors[]="Could not upload file to the server. Error Code 8.";
			}
		}
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Invoice / Shop Order Logo Setup - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">
        	
        	<?php
        	$ss_menu=6;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

               	<!-- content goes here -->
		<?php

if($success==1)

{

	print '<div class = "success">Logo was uploaded.</div>';

}

else

{

	$vujade->show_errors();

}

?>



<div style = "width:100%;padding:10px;border:1px solid #3C3C3D;height:220px;margin-bottom:15px;">

	<div style = "width:500px;float:left;height:200px;">

	<strong>Requirements</strong><br>

	Width: 260 pixels<br>

	Height: 60 pixels<br>

	Dots Per Inch (dpi): 300<br>

	File Type: .jpg<br>

	<em>For best results, please upload a file that meets the above requirements. Files with less than 300 dpi may appear blurry or pixelated on the invoice print out.</em>

	</div>

	<div style = "float:right;text-align:center;font-weight:bold;">

	<?php

	$logo = $vujade->get_logo(3);

	if($logo['error']=="0")

	{
		print '<H4>Current Logo</H4>';
		print '<img src = "'.$logo['url'].'" width = "360" height = "98">';


	}

	?>
	<hr>
	</div>

</div>



<form method = "post" enctype="multipart/form-data" action = "invoice_setup_logo.php">

<input type = "hidden" name = "action" value = "1">

Logo: <input name="logo" class="form-control" type="file" />
<br>
<input type="submit" class="btn btn-primary" value="Upload" />

</form>


				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

 <!-- BEGIN: PAGE SCRIPTS -->

 <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
   	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
