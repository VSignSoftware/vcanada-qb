<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($permissions['edit']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');

$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
$taxdata = $vujade->get_tax_for_city($project['city']);

$shop_order = $vujade->get_shop_order($project_id, 'project_id');

$amounts = array();
$setup = $vujade->get_setup();
if($setup['error']!=0)
{
    $vujade->page_redirect('error.php?m=3');
}
$buyout_overhead_percentage=$setup['buyout_overhead'];
$general_overhead_percentage=$setup['general_overhead'];
$machines_overhead_percentage=$setup['machine_overhead'];
$materials_overhead_percentage=$setup['materials_overhead'];

$pos = $vujade->get_costing_purchase_orders($project_id);
if($pos['error']=="0")
{
    $subtotal = 0;
    $total_dp = 0;
    $total_outsource = 0;
    $total_subcontract = 0;
    unset($pos['error']);
    foreach($pos as $po)
    {
        $subtotal+=$po['cost'];
        if($po['type']=="Materials")
        {
            $total_dp+=$po['cost'];
        }
        if($po['type']=="Outsource")
        {
            $total_outsource+=$po['cost'];
        }
        if($po['type']=="Subcontract")
        {
            $total_subcontract+=$po['cost'];
        }
    }
}

$inventory_cost = 0;
$items = $vujade->get_materials_for_costing($project_id);
if($items['error']=="0")
{
    unset($items['error']);
    foreach($items as $i)
    {
        $line = $i['cost']*$i['qty'];
        $inventory_cost += $line;
    }
    $indt = $inventory_cost * $project['indeterminant'];
    //$inventory_cost += $indt;
}
$tm = $total_dp+$inventory_cost+$indt;
$materials_overhead=$materials_overhead_percentage*$tm;

// machines
$machines_total=0;
$el_test = $vujade->get_machines_for_estimate($project_id);
if($el_test['error']=="0")
{
	unset($el_test['error']);
	foreach($el_test as $elt)
	{
		$mtl = $elt['rate']*$elt['hours'];
		$machines_total+=$mtl;
	}	
}
$machines_overhead=$machines_overhead_percentage*$machines_total;

# labor
$raw_labor = 0;
$labor_rider = 0;
$total_labor = 0;
$tcdata = $vujade->get_timecards_for_project($project_id);
if($tcdata['error']=="0")
{
    unset($tcdata['error']);
    foreach($tcdata as $tc)
    {
        # employee 
        $employee_id = $tc['employee_id'];

        # employee's rate
        $current_rate = $vujade->get_employee_current_rate($employee_id);

        # type of work
        $work_type = $tc['type'];

        # rate for type of work
        $rate_data = $vujade->get_labor_rate($work_type,'id');

        # hours worked
        $total_time = 0;
        $st_total = 0;
        $ot_total = 0;
        $dt_total = 0;
        if(!empty($tc['start']))
        {
            $ts = $tc['date'];
            $begints = strtotime($ts.' '.$tc['start']);
            $endts = strtotime($ts.' '.$tc['end']);
            $diff = $endts-$begints;
            $diff = $diff / 60;
            $diff = $diff / 60;
            $line_total=$diff;
            //$st_total+=$diff;
            if($diff<=8)
            {
                $st_total=$diff;
            }
            if( ($diff > 8) && ($diff <= 12) )
            {
                $ot_total = $diff - 8;
                $st_total = 8;
            }

            if($diff > 12)
            {
                $ot_total = 4;
                $st_total = 8;
                $dt_total = $diff - 12;
            }
        }
        else
        {
            $line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
            $st_total+=$tc['standard_time'];
            $ot_total+=$tc['over_time'];
            $dt_total+=$tc['double_time'];

        }
        $total_time+=$line_total;

        # raw labor 
        $st_rate_total = $st_total * $current_rate['rate'];
        $ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
        $dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
        $raw_labor += $st_rate_total + $ot_rate_total + $dt_rate_total;

        # labor rider
        $labor_rider_rate = $rate_data['rate']-$current_rate['rate'];
        $labor_rider += $labor_rider_rate * $total_time;
    }
    $total_labor += $labor_rider + $raw_labor;
}

$col1_subtotal = $tm + $total_labor + $total_outsource + $total_subcontract;

# general overhead
$general_overhead = 0;
$general_overhead = $total_labor;
$general_overhead = $general_overhead * $general_overhead_percentage;

// 48.5% of Inventory + Indeterminent + Total Labor Cost
// $go = $subtotal + $indet + $labor_subtotal;
// $go = $go * $setup['general_overhead'];

# buyout overhead
$buyout_overhead = ($total_outsource + $total_subcontract) * $buyout_overhead_percentage;

# total cost
$total_cost = 0;
$total_cost = $col1_subtotal + $general_overhead + $buyout_overhead + $machines_overhead + $materials_overhead;

# base sale, additional billing, permit, permit acquisition, totla sale price
$pt=0;
$st=0;
$base_sale = 0;
$additional_billing = 0;
$permit = 0;
$permit_acquisition = 0;
$total_sale_price = 0;
$bsa=array();
// Additional billing = Engineering + Change Orders + Shipping - discount. 
$project_invoices = $vujade->get_invoices_for_project($project_id);
if($project_invoices['error']=="0")
{
    unset($project_invoices['error']);
    foreach($project_invoices as $pi)
    {
        if($pi['costing']==1)
        {
            $invoiceid=$pi['database_id'];
            $firstinvoice=$pi;

			// legacy
			if($pi['is_legacy']==1)
			{
				$st=0;

				// invoice types
				// 1: illuminated and lump sum
				if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==1) )
				{
					// labor
					$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// total
					$st = $labor+$parts; 

					$parts=0;

					// selling price text
					$selling_price_text = "Selling Price";
				}

				// 2: illuminated sign parts and labor
				if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==2) )
				{
					$show_parts=true;

					// labor
					$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// selling price text
					$selling_price_text = "Labor Only Price";
				}

				// 3: non-illuminated lump sum
				if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==1) )
				{
					$show_parts=false;

					$st = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

					$parts=0;

					// selling price text
					$selling_price_text = "Selling Price";
				}

				// 4: non-illuminated sign parts and labor
				if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==2) )
				{
					$show_parts=true;

					// labor
					$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// selling price text
					$selling_price_text = "Labor Only Price";
				}

				// 5: service with parts and labor
				if( ($firstinvoice['type_1']==3) && ($firstinvoice['type_2']==2) )
				{
					$show_parts=true;

					// labor
					$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// selling price text
					$selling_price_text = "Labor Only Price";
				}
			
				// payments
				$payments = $vujade->get_invoice_payments($invoiceid);
				if($payments['error']=="0")
				{
					unset($payments['error']);
					$pt = 0;
					foreach($payments as $payment)
					{
						$pt+=$payment['amount'];
					}
				}
				else
				{
					$pt=0;
				}

				// tax amount
				// this will be changing in the future
				//$tax_amount += $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
				//$st+=$tax_amount;

				$tax_amount=$pi['sales_tax'];

				$tax_rate=$pi['tax_rate'];

				// change orders
				$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);
				
				// non-taxable co
				$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);

				$co+=$ntco;

				// engineering
				$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

				// permits
				$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

				// permit acq
				$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

				// shipping
				$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

				// discount
				//$discount+=$pi['discount'];
				//$discount = str_replace("-","",$discount);

				$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

				// retention
				$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
				$retention=str_replace("-","",$retention);

				// deposit
				$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
				//$deposit = str_replace("-","",$deposit);

				$base_sale += $st;
				$base_sale += $parts;
				$bsa[]=$st;
				$bsa[]=$parts;
				$ab = $engineering+$co+$shipping;
				//$ab=$ab+$discount;
				$additional_billing += $ab;
				$permit += $permits;
				$permit_acquisition += $pa;
			}
			else
			{
				// not legacy
				// payments
				$payments = $vujade->get_invoice_payments($invoiceid);
				if($payments['error']=="0")
				{
					unset($payments['error']);
					$pt = 0;
					foreach($payments as $payment)
					{
						$pt+=$payment['amount'];
					}
				}
				else
				{
					$pt=0;
				}

				// tax amount
				$tax_amount=$pi['v_sales_tax'];

				$tax_rate=$pi['v_tax_rate'];

				// i4 amount
				$i4_amount+=$pi['i4_amount'];

				// engineering
				$engineering += $pi['v_engineering'];

				// permits
				$permits += $pi['v_permits'];

				// permit acq
				$pa += $pi['v_permit_acquisition'];

				// shipping
				$shipping += $pi['v_shipping'];

				// discount
				$discount += $pi['v_discount'];

				// retention
				$retention += $pi['v_retention'];
				$retention=str_replace("-","",$retention);

				// deposit
				$deposit += $pi['v_deposit'];
				$deposit = str_replace("-","",$deposit);

				$base_sale += $pi['v_sales_price'];
				$bsa[]=$pi['v_sales_price'];
				$ab = $engineering+$co+$shipping;
				$additional_billing += $ab;
				$permit += $permits;
				$permit_acquisition += $pa;
			}
        }
    }
    foreach($bsa as $bamnt)
    {
        $total_sale_price+=$bamnt;
    }

    if($discount<0)
    {
        $total_sale_price += $ab+$permits+$pa+$discount;
    }
    else
    {
        $total_sale_price += $ab+$permits+$pa-$discount;
        $discount = 0-$discount;
    }

    //$total_sale_price += $additional_billing+$permit+$permit_acquisition;
    $net_profit = $total_sale_price - $total_cost - $project['commission'];
}

//$markup_decimal = $total_sale_price / $total_cost;
$markup_decimal = $net_profit / $total_sale_price;
$markup_decimal = floor($markup_decimal*100)/100;
$markup_decimal+=1;

$charset="ISO-8859-1";
$html = '<!DOCTYPE html>
    <head>
    <meta charset="'.$charset.'">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Job Costs</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/print_costing_style.css">
    </head>
    <body>';

// scope
// absolute positioned
// element is placed at the top of the html so that mpdf renders the overflow hidden part correctly; if placed anywhere else (ie nested, overflow doesn't work)
$scope = $project['description'];
$html.='<p style = "position:absolute;left:50;top:270;height:150px;width:720px;overflow:hidden;">'.$scope.'</p>';

$html.='<div class="container">
    <div class = "header"><h1>Job Costs</h1></div>';

# design number, project name, address, other details
$html.='<table width = "100%">';
$html.='<tr>';
$html.='<td class = "first" valign = "top">';
$html.=$project['project_id'];
$html.='<br>';
$title = (strlen($project['site']) > 40) ? substr($project['site'],0,30).'...' : $project['site'];
$html.=$title;
$html.='</td>';
$html.='<td class = "second" valign = "top">';
$html.='<table width = "100%">
<tr>
<td valign = "top" width = "33%">
Site:<br>
<table><tr><td class = "mr">';
$html.=$project['address_1'];
if(!empty($project['address_2']))
{
    $html.=', '.$project['address_2'];  
}
$html.='<br>';
$html.=$project['city'].', '.$project['state'].' '.$project['zip'];
$html.='</td></tr></table>
</td>

<td valign = "top" width = "33%">
Project Contact:<br>
<table><tr><td class = "mr">';
$html.=$project['project_contact'].'&nbsp;';
$html.='</td></tr></table>
Salesperson:<br>
<table><tr><td class = "mr">';
$html.=$project['salesperson'];
$html.='</td></tr></table>
</td>

<td valign = "top" width = "33%">
Date Opened: <table><tr><td class = "mr">'.$project['open_date'].'</td></tr></table>
Date Billed: <table><tr><td class = "mr">'.$project['date_billed'].'</td></tr></table>
Date Closed: <table><tr><td class = "mr">'.$project['date_closed'].'</td></tr></table>
</td>
</tr>
</table>';
$html.='</td>';
$html.='</tr>';
$html.='</table>';
$html.='<div class = "blue_border_thin"></div>';
$html.='<div class = "desc-container" style = "width:100%;height:240px;"><div class = "desc-container-inner"></div></div>';

# amounts
$html.='<table width = "100%">
<tr>
<td valign = "top" width = "33%">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Inventory
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($inventory_cost,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Base Sale
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($base_sale,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Direct Purchases
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($total_dp,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Additional Billing
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($ab,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Indeterminate
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($indt,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Permit
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($permits,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
    
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Total Materials
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($tm,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Permit Acquisition
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($pa,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Raw Labor
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($raw_labor,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Discount
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($discount,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Labor Rider
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($labor_rider,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>

    <td width = "60%" valign = "top">
    Total Sale Price
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($total_sale_price,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>

    <td width = "60%" valign = "top">
	    Total Labor
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($total_labor,2,'.',',').'
    </td>

    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Commission
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($project['commission'],2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Machines
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($machines_total,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
        <tr>
        <td width = "60%" valign = "top">
        Net Markup
        </td>
        <td width = "40%" valign = "top" class = "sum_box">'.$markup_decimal.'
        </td>
        </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Outsource
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($total_outsource,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td valign = "top" width = "33%">
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Subcontract
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($total_subcontract,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Net Profit
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($net_profit,2,'.',',').'
    </td>
    </tr>
    </table>
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Subtotal
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($col1_subtotal,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">

	<table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Materials Overhead
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($materials_overhead,2,'.',',').'
    </td>
    </tr>
    </table>

    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Labor Overhead
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($general_overhead,2,'.',',').'
    </td>
    </tr>
    </table>

    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Machines Overhead
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($machines_overhead,2,'.',',').'
    </td>
    </tr>
    </table>
    
</td>

<td width = "33%" valign = "top">
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Outsource Overhead
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($buyout_overhead,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>
</tr>

<tr>
<td valign = "top" width = "33%">
</td>

<td width = "33%" valign = "top">
    <table class = "col1">
    <tr>
    <td width = "60%" valign = "top">
    Total Cost
    </td>
    <td width = "40%" valign = "top" class = "sum_box">$'.@number_format($total_cost,2,'.',',').'
    </td>
    </tr>
    </table>
</td>

<td width = "33%" valign = "top">
</td>
</tr>

</table>';

$html.='</div></body></html>';

//print $html;
//die;

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 10, 10, 10);
$mpdf->setAutoTopMargin = 'stretch';
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($html);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-costing-overview.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output('Costing Overview.pdf','I'); 
}
?>