<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
$proposals_permissions = $vujade->get_permission($_SESSION['user_id'],'Proposals');
if($proposals_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
$setup = $vujade->get_setup();
$proposal_database_id=$_REQUEST['proposalid'];
$proposal = $vujade->get_proposal($proposal_database_id);

//$vujade->debug_array($setup);
//die;

# get billing info for this project
if($setup['is_qb']==1)
{
	$listiderror=0;
	$localiderror=0;

	// try to get by list id
	$customer1 = $vujade->get_customer($project['client_id'],'ListID');
	if($customer1['error']!="0")
	{
	    $listiderror++;
	    unset($customer1);
	}
	else
	{
	    $customer=$customer1;
	}
	// try to get by local id
	$customer2 = $vujade->get_customer($project['client_id'],'ID');
	if($customer2['error']!="0")
	{
	    $localiderror++;
	    unset($customer2);
	}
	else
	{
	    $customer=$customer2;
	}

	$iderror=$listiderror+$localiderror;

	if($iderror<2)
	{
	    $customer_contact = $vujade->get_contact($project['client_contact_id']);
	}
	else
	{
	// can't find customer or no customer on file

	}
}
else
{
	$customer = $vujade->get_customer($project['client_id']);
}

$total = 0;
$tax_total = 0;
$tax_rate = 0;

$items = $vujade->get_items_for_proposal($proposal['proposal_id']);
$terms = $proposal['terms'];

// $logo_data = $vujade->get_logo(2);
$logo = "images/proposal_logo.jpg";

$charset="ISO-8859-1";
$html = '<!DOCTYPE html>
    <head>
    <meta charset="'.$charset.'">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Proposal</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/print_proposal_style.css">
    <style>
    .marker
    {
    	background-color: yellow;
    }
    .gray
    {
    	
    }
    
    </style>
    </head>
    <body>
    <div class="container">';

# header
// $header_1 = '<div><div style = "float:left;width:405px;"><img src = "'.$logo.'" style = "width:400px;height:150px;border:0px solid red;"></div><div style = "float:left;width:300px;"><h1 style = "text-align:right;margin-bottom:0px;">PROPOSAL</h1>';

// best fit options based on logo size
$logo_width=300;

if($setup['site']=='Sparkle Signs')
{
	$logo_width=600;
}
if($setup['site']=='Scott Paragon')
{
	$logo_width=200;
}

$header_1 = '<div><div style = "float:left;width:405px;"><img width = "'.$logo_width.'" src = "'.$logo.'"></div><div style = "float:left;width:300px;"><h1 style = "text-align:right;margin-bottom:0px;">PROPOSAL</h1>';

$header_1.='<div style = "text-align:right;margin-bottom:0px;">'.$proposal['proposal_id'].'</div>';
$header_1.='<table width = "100%">';
$header_1.='<tr>';
$header_1.='<td valign = "top" width = "60%" class = "alignright">';
$header_1.='Date:<br>';
$header_1.='Expires:<br>';
$header_1.='Drawing Numbers:<br>';
$header_1.='</td>';
$header_1.='<td valign = "top" width = "40%" class = "alignright">';
$header_1.=$proposal['proposal_date'].'<br>';
$header_1.=$proposal['expiration_date'].'<br>';
$header_1.=$proposal['drawing_numbers'].'<br>';
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table>';

$header_1.='</div></div>';

$header_1.='<table width = "100%" style = "margin-top:15px;">';
$header_1.='<tr>';

# left col
$header_1.='<td valign = "top" width = "50%">';
$header_1.='<table width = "100%">';
$header_1.='<tr>';
$header_1.='<td valign = "top" class = "col1">';
$header_1.='<strong>Project:</strong>';
$header_1.='</td>';
$header_1.='<td valign = "top" class = "col2">';
$header_1.=$project['site'].'<br>';
$header_1.=$project['address_1'].'<br>';
$header_1.=$project['city'].', '.$project['state'].' '.$project['zip'].'<br>';
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table><br>';
$header_1.='</td>';

# right col
$header_1.='<td valign = "top" width = "50%">';
$header_1.='<table width = "100%">';
$header_1.='<tr>';
$header_1.='<td valign = "top" width = "" class = "col1">';
$header_1.='<strong>Client:</strong>';
$header_1.='</td>';
$header_1.='<td valign = "top" class = "col2">';
$header_1.=$customer['name'].'<br>';
$header_1.=$customer['address_1'].'<br>';
if(!empty($customer['address_2']))
{
    $header_1.=$customer['address_2'].'<br>';
}
$header_1.=$customer['city'].', '.$customer['state'].' '.$customer['zip'];
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table>';
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table>';

$header_1.='<table>';
$header_1.='<tr>';
$header_1.='<td>';
$header_1.='<table width = "100%">';
$header_1.='<tr>';
$header_1.='<td valign = "top" class = "col1">';
$header_1.='<strong>Contact:</strong>';
$header_1.='</td>';
$header_1.='<td valign = "top" class = "col2" style = "margin-right:20px;">';
$header_1.=$proposal['send_to_name'];
$header_1.='</td>';
$header_1.='<td valign = "top" class = "col2" style = "padding-left:20px;">';
$header_1.=$proposal['send_to_phone'];
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table>';
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table>';

if($setup['proposal_message']=='')
{
    $html.='<p style = "margin-top:15px;">We are pleased to offer this proposal for the following services at the above location.</p>';
}
else
{
    $html.='<p style = "margin-top:15px;">'.$setup['proposal_message'].'</p>';
}

# items and cost header
$html.='<div style = "width:100%; padding:0px; margin:0px;height:10px;border-bottom:1px solid black;">';
$html.='<div style = "width:84%; padding:0px; margin:0px; float:left; font-weight:bold; height:5px;">Project Description:';
$html.='</div>';
$html.='<div style = "width:15%; padding:0px; margin:0px; float:right;  font-weight:bold; height:5px; text-align:right;">Item Total:';
$html.='</div>';
$html.='</div>';

// tax rate for work location
$pcity = $project['city'];
$pstate = $project['state'];
if((!empty($pcity)) && (!empty($pstate)) )
{
	$tax_data = $vujade->get_tax_for_city($pcity,$pstate);
}

// not applicable for canada
if($setup['country']=="Canada")
{
	$proposal['tax_rate']=1;
}

//$items['error']=1;

# items and cost
if($items['error']=="0")
{
	$html.='<div style = "width:100%;border-bottom:1px solid black; padding-bottom:0px;padding-top: 0px;margin-top:5px;">';	        
    $subtotal = 0;
    $tax_total=0;
	$tax_line=0;
    unset($items['error']);
    foreach($items as $item)
    {

    	if(!empty($item['tax_label_rate']))
     	{
     		$tax_line=$item['amount']*$item['tax_label_rate']*$proposal['tax_rate'];
     	}

     	$tax_total+=$tax_line;
     	$tax_line=0;

        $subtotal+=$item['amount'];

        // debug: show the html code from ckeditor
        // $item['item']=htmlentities($item['item']);

        // 
        //$item['item'] = str_replace("&nbsp;", '&nbsp; ', $item['item']);

        $item['item'] = str_replace("<p>", '', $item['item']);
        $item['item'] = str_replace("</p>", '<br>', $item['item']);

        // table row
    	$html .= '<div class = "" style="width: 100%;border:0px solid red;">';

    	// item 
    	$html .= '<div class = "" valign="top" style="width: 85%;margin: 0px; padding: 0px;float: left;border:0px solid blue;">';
    	$html.=$item['item'];
    	$html.='</div>';

    	// price
    	$html.='<div class = "" style = "width:14%;text-align:right;margin: 0px;padding: 0px;float: right;border:0px solid yellow;">';
    	if(!empty($item['amount']))
        {
            $html.='$'.@number_format($item['amount'],2,'.',',');
        }
    	$html.='</div>';
		$html .= "</div>";

		//$html.='<br style = "clear:both;">';

    }
    $html.='</div>';
}

$html.='<table>';
$html.='<tr class = "bb">';
$html.='<td valign = "top" width = "70%" class = "">';
$html.='';
$html.='</td>';
$html.='<td valign = "top" width = "20%" class = "alignright">';
$html.='';
$html.='</td>';
$html.='</tr>';
$html.='</table>';

if($proposal['tax_total']>0)
{
	$tax_total=$proposal['tax_total'];
}

$total = $subtotal + $tax_total;

$html.='<table width = "100%">';
$html.='<tr class = "">';
$html.='<td valign = "top" width = "70%" style = "padding-left:100px;">';

// deposit
if(empty($proposal['deposit_rate']))
{
    $deposit_rate=$setup['advanced_deposit'];
    $deposit_amount = $total*$deposit_rate;
}
else
{
    $deposit_rate=$proposal['deposit_rate'];
    $deposit_amount = $total*$deposit_rate;
}
$deposit_rate=$deposit_rate*100;
$html.='<center><strong>'.$deposit_rate.'% Deposit: <br>$'.@number_format($deposit_amount,2,'.',',').'</strong></center>';
$html.='</td>';
$html.='<td valign = "top" width = "15%" class = "alignright">';
$html.='<strong>Subtotal: </strong><br>';
$html.='<strong>Tax: </strong><br>';
$html.='</td>';
$html.='<td valign = "top" width = "15%" class = "alignright">';
$html.='<strong>$'.@number_format($subtotal,2,'.',',').'</strong><br>';
$html.='<strong>$'.@number_format($tax_total,2,'.',',').'</strong><br>';
$html.='</td>';
$html.='</tr>';

$html.='<tr class = "">';
$html.='<td valign = "top" width = "70%" class = "alignright">';
$html.='&nbsp;';
$html.='</td>';
$html.='<td valign = "top" width = "15%" class = "alignright">';
$html.='<strong>Total: </strong>';
$html.='</td>';
$html.='<td valign = "top" width = "15%" class = "alignright">';
$html.='<strong>$'.@number_format($total,2,'.',',').'</strong>';
$html.='</td>';
$html.='</tr>';

$html.='</table>';

$pnt = 7;
$terms = $proposal['terms'];
//$terms = htmlentities($terms);
//$terms = str_replace('&rsquo;', '', $terms);
//$terms = str_replace('&ldquo;', '', $terms);
//$terms = str_replace('&trade;', 'TM', $terms);
//$terms = str_replace('&rdquo;', '', $terms);
//$terms = str_replace('&nbsp;', ' ', $terms);

//$terms='test';
$html.='<div>'.$terms.'</div>';

// special case for this customer
if($setup['site']=="Sparkle Signs")
{
	$html.='<table><tr>';
	$html.='<td><img src = "images/bbb_logo.jpg" width = "50"></td>';
	$html.='<td><img src = "images/tsa_logo.jpg" width = "200"></td>';
	$html.='<td><img src = "images/isa_logo.jpg" width = "200"></td>';
	$html.='</tr></table>';
}

//print $html;
//die;

# footer
$footer_1='<p><strong>Salesperson: '.$project['salesperson'].'</strong></p><br>';

$footer_1.='<table width = "100%">';
$footer_1.='<tr class = "">';

$footer_1.='<td valign = "top" class = "t1">';
$footer_1.="Buyer's Acceptance";
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "bb2">';
$footer_1.='';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "t2">';
$footer_1.='Title';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "bb2">';
$footer_1.='';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "t2">';
$footer_1.='Date';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "bb3">';
$footer_1.='';
$footer_1.='</td>';

$footer_1.='</tr>';

$footer_1.='<tr class = "">';
$footer_1.='<td colspan = "6">&nbsp;';
$footer_1.='</td>';
$footer_1.='</tr>';

$footer_1.='<tr class = "">';

$footer_1.='<td valign = "top" class = "t1">';
$footer_1.="Seller's Acceptance";
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "bb2">';
$footer_1.='';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "t2">';
$footer_1.='Title';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "bb2">';
$footer_1.='';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "t2">';
$footer_1.='Date';
$footer_1.='</td>';

$footer_1.='<td valign = "top" class = "bb3">';
$footer_1.='';
$footer_1.='</td>';

$footer_1.='</tr>';
$footer_1.='</table>';

$internal_footer = '<div style="width: 100%;">';
$internal_footer .='<p style="width: 60%;float:left;"><strong>Salesperson: '.$project['salesperson'].'</strong></p>';
$internal_footer .= '<p style="width: 35%;float:right; margin-top: -4px;"><span>Buyer</span>____________' ;
$internal_footer .= '<span >Seller</span>____________</p>';
$internal_footer .= '</div>';
//print $html;
//die;

# mpdf class (pdf output)
include("mpdf60/mpdf.php");

//$html.=$footer_1;
//unset($footer_1);

$footer_1.= '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';
$internal_footer .= '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';


$html .= '</div>';
// $html .= ('<htmlpagefooter name="lastPageFooter">' . $footer_1 . '</htmlpagefooter>');
$html .= '</body></html>';

$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 80, 45, 10);
$mpdf->debug=true;
$mpdf->allow_output_buffering = true;
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
// $mpdf->DefHTMLHeaderByName('lastPageFooter', $footer_1);
$mpdf->DefHTMLFooterByName('footer_1', $internal_footer);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($html);
$mpdf->SetHTMLFooter($footer_1);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-proposal.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output($project_id.'.pdf','I'); 
}
?>
