<?php
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$project_id = $_REQUEST['project_id'];
$category = $_REQUEST['category'];
$category = $category[0];

// determine if safari
// safari has a bug where it sends every uploaded file as image.jpg or image.png (ie with the same name)
require_once('mobile_detect.php');
$detect = new Mobile_Detect;

if (!empty($_FILES)) 
{

	if($detect->isiOS())
	{
		$is_ios=true;
		$ios_debug=1;
	}

	$e = $_FILES['file']['error'];
	$f = fopen("upload_errors.txt", "w") or die("Unable to open file!");
	$txt = $e."\n";
	$txt .= $msg."\n";
	$txt .= $m."\n";
	$txt.=$ios_debug;
	fwrite($f, $txt);
	fclose($f);

    $storeFolder = 'uploads/images/'.$project_id;
    if(!is_dir($storeFolder))
    {
        mkdir($storeFolder);
    }

    $msg = '';
	$e = '';
	$count = '';
	$mysqli=$vujade->mysqli;
	$sql = "SELECT * FROM `site_photos` WHERE `project_id` = '$project_id'";
	$result = $mysqli->query($sql);
	$count = $result->num_rows;
	if(!$result)
	{
		$e2="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
		$e = $_FILES['file']['error'];
		$f = fopen("upload_errors.txt", "w") or die("Unable to open file!");
		$txt = $e."\n";
		$txt = $e2."\n";
		$txt .= $msg."\n";
		$txt .= $m."\n";
		fwrite($f, $txt);
		fclose($f);
	}
	else
	{
		while($rows = $result->fetch_assoc())
		{
			if($rows['file_name']==$_FILES['file']['name'])
			{
				@unlink('uploads/images/'.$project_id.'/'.$rows['file_name']);
				$msg.=$rows['file_name']."\n";
				$s = $vujade->delete_row('site_photos',$rows['id']);
				$msg.=$rows['id']."\n\n";
			}
		}
	}

    $post = "";
    $ret = $vujade->site_photo_upload($project_id, $post, $_FILES, $category, $is_ios);
   	$e = $_FILES['file']['error'];
	$f = fopen("upload_errors.txt", "w") or die("Unable to open file!");
	$txt = $e."\n";
	$txt = $ret."\n";
	$txt .= $msg."\n";
	$txt .= $m."\n";
	$txt.=$ios_debug;
	fwrite($f, $txt);
	fclose($f);
}
?>     