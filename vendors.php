<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$vendors_permissions = $vujade->get_permission($_SESSION['user_id'],'Vendors');
if($vendors_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup=$vujade->get_setup();

$vendors = $vujade->get_vendors(1,0,1);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$title = "Vendors - ";
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Vendors</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
        	if($_REQUEST['m']==1)
        	{
        		$vujade->messages[]="Vendor deleted.";
        	}
        	$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<a href = "vendor_search.php" class = "btn btn-primary btn-sm">Map Search</a> 
					<?php
					if($vendors_permissions['create']==1)
					{
						?>
							<div class="widget-menu pull-right">
								<a href = "vendor.php?action=1" class = "btn btn-primary pull-right btn-sm">New</a>
							</div>
					<?php } ?>
				</div>
	        	<div class="panel-body bg-light">
					<?php 
					if($vendors['error']=="0")
					{
						unset($vendors['error']);
						?>
						<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>Vendor #</strong></td>
									<td valign = "top"><strong>Name</strong></td>
									<td valign = "top"><strong>City</strong></td>
									<td valign = "top"><strong>State / Province</strong></td>
									<td valign = "top"><strong>Rating</strong></td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">
							<?php
							foreach($vendors as $i)
							{
								//$vid = $i['database_id'];
						        $link = 'vendor.php?id='.$i['database_id'];
						        print '<tr class = "clickableRow-row">';

						        print '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><a href = "'.$link.'">'.$i['database_id'].'</a></td>';

						        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = "">'.$i['name'].'</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['city'].'</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['state'].'</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
						        print $i['rating'];
						        print '</td>';

								print '</tr>';
							}

							?>
							</tbody>
						</table>
					<?php
					}
					else
					{
						$vujade->set_error('Could not fetch inventory from database. ');
						$vujade->set_error($inventory['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    $("tbody").on("click", ".clickableRow", function(){
    	window.document.location = $(this).attr("href");	
    });

});
</script>

</body>
</html>