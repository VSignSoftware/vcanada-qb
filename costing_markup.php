<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$js = $vujade->get_job_status($id);

$setup = $vujade->get_setup();
if($setup['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$buyout_overhead_percentage=$setup['buyout_overhead'];
$general_overhead_percentage=$setup['general_overhead'];

$shop_order = $vujade->get_shop_order($id, 'project_id');
foreach($shop_order as $k => $v)
{
	if($k=="is_illuminated")
	{
		$lt = $v;
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 17;
$cmenu = 7;
$section = 3;
$title = "Costing Markup - ".$project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <?php require_once('project_left_tray.php'); ?>
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">
            <div class="pl15 pr15" style = "width:100%;">
              <?php require_once('project_right_tray.php'); ?>
              
              <div class="panel panel-primary panel-border top">
                <div class="panel-heading">
                  <span class="panel-title"></span>
                  <div class="widget-menu pull-right">
                    
                  </div>
                </div>
                <div class="panel-body bg-light">
    			
    			<?php
				$pos = $vujade->get_costing_purchase_orders($id);
				if($pos['error']=="0")
				{
					$subtotal = 0;
					$total_dp = 0;
					$total_outsource = 0;
					$total_subcontract = 0;
					unset($pos['error']);
					foreach($pos as $po)
					{
						$subtotal+=$po['cost'];
						if($po['type']=="Materials")
						{
							$total_dp+=$po['cost'];
						}
						if($po['type']=="Outsource")
						{
							$total_outsource+=$po['cost'];
						}
						if($po['type']=="Subcontract")
						{
							$total_subcontract+=$po['cost'];
						}
					}
				}

				$inventory_cost = 0;
				$items = $vujade->get_materials_for_costing($id);
				if($items['error']=="0")
				{
					unset($items['error']);
					foreach($items as $i)
					{
						$line = $i['cost']*$i['qty'];
						$inventory_cost += $line;
					}
					$indt = $inventory_cost * .1;
					//$inventory_cost += $indt;
				}
				$tm = $total_dp+$inventory_cost+$indt;

				# labor
				$raw_labor = 0;
				$labor_rider = 0;
				$total_labor = 0;
				$tcdata = $vujade->get_timecards_for_project($id);
				if($tcdata['error']=="0")
				{
					unset($tcdata['error']);
					foreach($tcdata as $tc)
					{
						# employee 
						$employee_id = $tc['employee_id'];

						# employee's rate
						$current_rate = $vujade->get_employee_current_rate($employee_id);

						# type of work
						$work_type = $tc['type'];

						# rate for type of work
						$rate_data = $vujade->get_labor_rate($work_type,'id');

						# hours worked
						$total_time = 0;
						$st_total = 0;
						$ot_total = 0;
						$dt_total = 0;
						if(!empty($tc['start']))
						{
							$ts = $tc['date'];
							$begints = strtotime($ts.' '.$tc['start']);
							$endts = strtotime($ts.' '.$tc['end']);
							$diff = $endts-$begints;
							$diff = $diff / 60;
							$diff = $diff / 60;
							$line_total=$diff;
							//$st_total+=$diff;
							if($diff<=8)
							{
								$st_total=$diff;
							}
							if( ($diff > 8) && ($diff <= 12) )
							{
								$ot_total = $diff - 8;
								$st_total = 8;
							}

							if($diff > 12)
							{
								$ot_total = 4;
								$st_total = 8;
								$dt_total = $diff - 12;
							}
						}
						else
						{
							$line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
							$st_total+=$tc['standard_time'];
							$ot_total+=$tc['over_time'];
							$dt_total+=$tc['double_time'];

						}
						$total_time+=$line_total;

						# raw labor 
						$st_rate_total = $st_total * $current_rate['rate'];
						$ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
						$dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
						$raw_labor += $st_rate_total + $ot_rate_total + $dt_rate_total;

						# labor rider
						$labor_rider_rate = $rate_data['rate']-$current_rate['rate'];
						$labor_rider += $labor_rider_rate * $total_time;
					}
					$total_labor += $labor_rider + $raw_labor;
				}

				$col1_subtotal = $tm + $total_labor + $total_outsource + $total_subcontract;

				# general overhead
				$general_overhead = 0;
				$general_overhead = $tm + $total_labor;
				$general_overhead = $general_overhead * $general_overhead_percentage;

				// 48.5% of Inventory + Indeterminent + Total Labor Cost
				// $go = $subtotal + $indet + $labor_subtotal;
				// $go = $go * $setup['general_overhead'];

				# buyout overhead
				$buyout_overhead = ($total_outsource + $total_subcontract) * $buyout_overhead_percentage;
				
				# total cost
				$total_cost = 0;
				$total_cost = $col1_subtotal + $general_overhead + $buyout_overhead;

				# base sale, additional billing, permit, permit acquisition, totla sale price
				$base_sale = 0;
				$additional_billing = 0;
				$permit = 0;
				$permit_acquisition = 0;
				$total_sale_price = 0;
				$bsa=array();
				// Additional billing = Engineering + Change Orders + Shipping - discount. 
				$project_invoices = $vujade->get_invoices_for_project($id);
				if($project_invoices['error']=="0")
				{
					unset($project_invoices['error']);
					foreach($project_invoices as $pi)
					{
						if($pi['costing']==1)
						{
							$base_sale += $pi['price_selling'];
							$bsa[]=$pi['price_selling'];
							//print 'Base Sale: '.$base_sale.'<br>';
							// Additional billing = Engineering + Change Orders + Shipping - discount. 
							$ab = $pi['price_engineering']+$pi['price_change_orders']+$pi['price_shipping'];
							$ab=$ab-$pi['discount'];
							$additional_billing += $ab;
							//print 'Additional Billing: '.$additional_billing.'<br>';
							$permit += $pi['price_permits'];
							//print 'Permits: '.$permit.'<br>';
							$permit_acquisition += $pi['price_permit_acquisition'];
							//print 'Permits 2: '.$permit_acquisition.'<br>';
							//$total_sale_price += $additional_billing+$permit+$permit_acquisition;
							//print 'tsp: '.$total_sale_price.'<br>';
							//$total_sale_price += $tsp;
						}
					}
					foreach($bsa as $bamnt)
					{
						$total_sale_price+=$bamnt;
					}
					$total_sale_price += $additional_billing+$permit+$permit_acquisition;
					$net_profit = $total_sale_price - $total_cost - $project['commission'];
				}
				?>

				<p>
				<?php
				$tiers = $vujade->get_commission_tiers();
				if($tiers['error']=="0")
				{
					unset($tiers['error']);
					print '<strong>Commission Tier: <select name = "commission_tier" id = "commission_tier" class = "" style = "width:200px;"></strong>';
					foreach($tiers as $tier)
					{
						print '<option value = "'.$tier['tier_id'].'">'.$tier['tier_label'].'</option>';
					}
					print '</select>';
				}
				?>
				</p>

				<div id = "markup" style="overflow: auto !important;">
				<?php
				# markups tab
				$tier = 1;
				$markups = $vujade->get_markups($tier,$sort=1);
				if($markups['error']=='0')
				{
					unset($markups['error']);
					print '<table class = "table table-hover table-striped  table-bordered">';
					print '<tr class = "primary bold" style = "font-weight:bold;border-top:1px solid #4A89DC;">';
					print '<td>';
					print '<strong>M/U</strong>';
					print '</td>';
					print '<td>';
					print '<strong>Base Sale</strong>';
					print '</td>';
					print '<td>';
					print '<strong>Comm %</strong>';
					print '</td>';
					print '<td>';
					print '<strong>Commission</strong>';
					print '</td>';
					print '<td>';
					print '<strong>Total Sale</strong>';
					print '</td>';
					print '<td>';
					print '<strong>Profit</strong>';
					print '</td>';
					print '</tr>';
					foreach($markups as $markup)
					{
						print '<tr>';
						print '<td>';
						print @number_format($markup['markup'],2,'.',',');
						print '</td>';
						print '<td>';
						$bs = $markup['markup']*$total_cost;
						//$bs = round($bs,2);
						print '$'.@number_format($bs,2,'.',',');
						print '</td>';
						print '<td>';
						print $markup['commission_percent'].'%';
						print '</td>';
						print '<td>';
						$commission = $markup['commission_percent']*$bs;
						//$commission=round($commission,2);
						print '$'.@number_format($commission,2,'.',',');
						print '</td>';
						print '<td>';
						$ts = $bs+$commission;
						print '$'.@number_format($ts,2,'.',',');
						print '</td>';
						print '<td>';
						$profit = $bs-$factory_cost;
						print '$'.@number_format($profit,2,'.',',');
						print '</td>';
						print '</tr>';
					}
					print '</table>';
				}
				?>
				</div>
                  
                </div>
                </div>
            </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->
  <!-- BEGIN: PAGE SCRIPTS -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {
    	"use strict";
    
    	// Init Theme Core    
    	Core.init();
    	
    	//var n = $('#notes').html();
    	//$("#notes").html($.trim(n));

    	$('#commission_tier').change(function()
		{
			var fc = "<?php print $total_cost; ?>";
			var tier = $('#commission_tier').val();
			$('#markup').html('');
			$.post( "jq.get_estimate_cost.php", { total: 3, tier: tier, fc: fc })
			  .done(function( data ) 
			  {
			  		$('#markup').html(data);
			  });
		});

		//$('.bold').css('font-weight','bold');
		//$('.bold').css('font-size','14px');
    
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>