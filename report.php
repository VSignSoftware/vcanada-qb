<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$type = $_REQUEST['type'];

// type 16 may have special parameters
$action = 0;
$sort=$_REQUEST['sort1'];
$salesperson=$_REQUEST['sort2'];
$status=$_REQUEST['sort3'];
if($type==16)
{
	$options=array('sort'=>$sort, 'salesperson'=>$salesperson, 'status'=>$status);
	$data = $vujade->get_projects_by_status($type,$options);
}
if($type==17)
{
	$options=array('sort'=>$sort, 'salesperson'=>$salesperson, 'status'=>$status);
	$data = $vujade->get_projects_by_status($type,$options);
}
if(!in_array($type,array(16,17)))
{
	$data = $vujade->get_projects_by_status($type);
}

$tdata = $data;

if($tdata['error']=="0")
{
	unset($tdata['error']);
	unset($tdata['type']);
	$pids = array();
	foreach($tdata as $td)
	{
		$pids[]=$td['database_id'];
	}
	$pids_str=implode('^',$pids);
}

// set up for the project search function
$vujade->create_row('project_search');
$row_id = $vujade->row_id;
unset($_SESSION['search_id']);
$_SESSION['search_id']=$row_id;
$s=array();
$s[]=$vujade->update_row('project_search',$row_id,'uid',$_SESSION['user_id']);
$s[]=$vujade->update_row('project_search',$row_id,'status',$type);
$s[]=$vujade->update_row('project_search',$row_id,'pids',$pids_str);
$_SESSION['list_url']='report.php?type='.$type;

$title = $data['type'].' - ';
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=4;
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $data['type']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "queues.php" class = "btn btn-primary btn-sm">&laquo; Back</a>
					</div>
				</div>

	        	<div class="panel-body bg-light">
					<div class = "row">
						<div class = "col-md-12">

<?php
if($data['error']=="0")
{

	/*
	<p style = "clear:both;margin-bottom:5px;">
	<a class="btn btn-sm btn-primary pull-right" target="_blank" href="pdf.php?type=<?php echo $type;?>&sort1=<?php echo $sort;?>&sort2=<?php echo $salesperson;?>&sort3=<?php echo $status;?>&page=print-report">Print</a>
	</p>
	*/
	
	// this type has a special layout
	if($type==16)
	{
		?>

		<div style = "">
			<div style = "">

				<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
				<thead>
					<tr style = "border-bottom:1px solid black;">
						<td><strong>Project #</strong></td>
						<td><strong>Name</strong></td>
						<td><strong>Location</strong></td>
						<td><strong>Salesperson</strong></td>
						</td>
						<td><strong>PM</strong></td>
						<td><strong>Status</strong></td>
						</td>
						<td><strong>Ship Date</strong></td>
						</td>
						<td><strong>Due Date</strong></td>
						</td>
					</tr>
				</thead>

			    <tbody style = "">

				<?php
				unset($data['error']);
				unset($data['type']);
				foreach($data as $project)
				{
					$js = $vujade->get_job_status($project['project_id']);
					$link = 'project.php?r='.$type.'&id='.$project['project_id'];
			        print '<tr class="clickableRow" href="'.$link.'">';
			        print '<td valign = "top" style = "">'.$project['project_id'].'</td>';
			        print '<td valign = "top" style = "">'.$project['site'].'</td>';
			        print '<td valign = "top" style = "">'.$project['location'].'</td>';
			        print '<td valign = "top" style = "">'.$project['salesperson'].'</td>';
			        print '<td valign = "top" style = "">'.$project['project_manager'].'</td>';
			        print '<td valign = "top" style = "">'.$project['project_status'].'</td>';
			        print '<td valign = "top" style = "">'.$js['shipping_date'].'</td>';

			        //if there is a service date display service date, otherwise check for install date and display it if there is one. 
			        print '<td valign = "top" style = "">';
			        if(!empty($js['service_date']))
			        {
			        	print $js['service_date'];
			        }
			        else
			        {
			        	print $js['installation_date'];
			        }
			        print '</td>';
					print '</tr>';
				}
				?>

				</tbody>
		    	</table>
			</div>
		</div>

		<?php
	}
	if($type==17)
	{
		?>

		<div style = "">
			<div style = "">
				<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
				<thead>
					<tr style = "border-bottom:1px solid black;">
						<td width = "10%"><strong>Project #</strong></td>
						<td width = "20%"><strong>Name</strong></td>
						<td width = "30%"><strong>Location</strong></td>
						<td width = "20%"><strong>Salesperson</strong></td>
						</td>
						<td><strong>PM</strong></td>
						<td width = "20%"><strong>Status</strong></td>
						</td>
					</tr>
				</thead>

			    <tbody style = "">

				<?php
				unset($data['error']);
				unset($data['type']);
				foreach($data as $project)
				{
					$link = 'project.php?r='.$type.'&id='.$project['project_id'];
			        print '<tr class="clickableRow" href="'.$link.'">';
			        print '<td valign = "top" style = "">'.$project['project_id'].'</td>';
			        print '<td valign = "top" style = "">'.$project['site'].'</td>';
			        print '<td valign = "top" style = "">'.$project['location'].'</td>';
			        print '<td valign = "top" style = "">'.$project['salesperson'].'</td>';
			        print '<td valign = "top" style = "">'.$project['project_manager'].'</td>';
			        print '<td valign = "top" style = "">'.$project['project_status'].'</td>';
					print '</tr>';
				}
				?>

				</tbody>
		    	</table>
			</div>
		</div>

		<?php
	}
	if(!in_array($type,array(16,17)))
    {
	?>
		<div>
			<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
			<thead>
				<tr style="border-bottom:1px solid black;">
					<td><strong>Project #</strong></td>
					<td><strong>Name</strong></td>
					<td><strong>Location</strong></td>
					<td><strong>Salesperson</strong></td>
					<td><strong>PM</strong></td>
					<?php
					if(in_array($type,array(3,4,5,6,9,15,16)))
					{
						print '<td><strong>Ship Date</strong></td>';
						print '<td><strong>Due Date</strong></td>';
					}
					?>

				</tr>
			</thead>
			<tbody>
				<?php
				unset($data['error']);
				unset($data['type']);
				foreach($data as $project)
				{
					$link = 'project.php?r='.$type.'&id='.$project['project_id'];
			        print '<tr class="clickableRow" href="'.$link.'">';
			        print '<td valign = "top" style = "">'.$project['project_id'].'</td>';
			        print '<td valign = "top" style = "">'.$project['site'].'</td>';
			        print '<td valign = "top" style = "">'.$project['location'].'</td>';
			        print '<td valign = "top" style = "">'.$project['salesperson'].'</td>';
			        print '<td valign = "top" style = "">'.$project['project_manager'].'</td>';
			        if(in_array($type,array(3,4,5,6,9,15,16)))
					{
						$js = $vujade->get_job_status($project['project_id']);
						print '<td width = "20%">'.$js['shipping_date'].'</td>';
						print '<td width = "20%">'.$js['installation_date'].'</td>';
					}

					print '</tr>';
					//print '<tr><td colspan = "4">&nbsp;</td></tr>';
				}
				?>
				
			</tbody>
			</table>
		</div>
	
	<?php
	}
}
else
{
	$vujade->errors[]="No projects found.";
	$vujade->show_errors();
}
?>
								     
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script src="vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // Init DataTables
    $('#datatable').dataTable({
      
      "aaSorting" : [],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    $("#go").click(function() 
    {
    	var sort1 = $('#sort1').val();
    	var sort2 = $('#sort2').val();
    	var sort3 = $('#sort3').val();
    	var href = "report.php?sort1="+sort1+"&sort2="+sort2+"&sort3="+sort3+"&type=16";
		window.document.location = href;
    });

    $("#reset").click(function() 
    {
    	href = "report.php?type=16";
        window.document.location = href;
    });

    $("#go2").click(function() 
    {
    	var sort1 = $('#sort1').val();
    	var sort2 = $('#sort2').val();
    	var sort3 = $('#sort3').val();
    	var href = "report.php?sort1="+sort1+"&sort2="+sort2+"&sort3="+sort3+"&type=17";
		window.document.location = href;
    });

    $("#reset2").click(function() 
    {
    	href = "report.php?type=17";
        window.document.location = href;
    });

});

$(document).on('click','.clickableRow',function(e)
{
	e.preventDefault();
    window.document.location = $(this).attr("href");
});

</script>
<style>
.pagination {
	margin: 0px auto !important;
	padding-left: 20px !important;
}
</style>
</body>
</html>