<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
if($id==4)
{
	$vujade->page_redirect('error.php?m=2');
}
$valid = array(1,2,3,4,5,6,7);
if(!in_array($id, $valid))
{
	$vujade->page_redirect('error.php?m=2');
}
else
{
	// option 2 for proposals (id = 7) might not exist; create it
	if($id==7)
	{
		$test = $vujade->get_terms($id);
		if($test['error']!='0')
		{
			// create the row
			$vujade->create_row('terms');
			// id should be 7 of next terms row
			// if not, problem...
			$terms=$vujade->get_terms($id);
		}
		else
		{
			$terms=$test;
		}
	}
	else
	{
		$terms = $vujade->get_terms($id);
	}
}
$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
if($action==1)
{
	$terms = $_POST['terms'];
	$success=$vujade->update_row('terms',$id,'terms',$terms);
	if($success==1)
	{
		$vujade->messages[]="Terms Updated.";
	}
	else
	{
		$vujade->errors[]="Could not save your changes. Please contact the website administrator.";
	}
	$terms = $vujade->get_terms($id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Terms - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	if($id==1)
        	{
        		$ss_menu=2;
        	}
        	if($id==7)
        	{
        		$ss_menu=2;
        	}
        	if( ($id==2) || ($id==5) || ($id==6) )
        	{
        		$ss_menu=3;
        	}
        	if($id==3)
        	{
        		$ss_menu=6;
        	}
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="width: 100%;">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<form method = "post" action = "terms.php">
<p><strong>
<?php
if($terms['id']==1)
{
	print 'Terms on Proposals (Option 1) <select name = "change-terms" id = "change-terms" class = "">
		<option value = "">-select-</option>
		<option value = "1">Terms 1</option>
		<option value = "7">Terms 2</option>
	</select>';
}
if($terms['id']==2)
{
	print 'Material Terms';
}
if($terms['id']==3)
{
	print 'Invoicing Terms';
}
if($terms['id']==4)
{
	print 'Invoicing Notes';
}
if($terms['id']==5)
{
	print 'Outsource Terms';
}
if($terms['id']==6)
{
	print 'Subcontractor Terms';
}
if($terms['id']==7)
{
	print 'Terms on Proposals (Option 2) <select name = "change-terms" id = "change-terms" class = "">
		<option value = "">-select-</option>
		<option value = "1">Terms 1</option>
		<option value = "7">Terms 2</option>
	</select>';
}
?>
</strong></p>
<p>
	
	<textarea name = "terms" class="form-control ckeditor" id = "terms" rows = "20" cols = "50">
	<?php print $terms['terms']; ?>
	</textarea>

	<!-- ckeditor new version 4.5x -->
	<?php require_once('ckeditor.php'); ?>
</p>
<input type = "hidden" name = "id" value = "<?php print $id; ?>">
<input type = "hidden" name = "action" value = "1">
<p><input type = "submit" class = "btn btn-primary" value = "Save" class = "sbt200"></p>
</form>


					</div>
				</div>
		</div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // change the terms
    $('#change-terms').change(function(e)
    {
        e.preventDefault();
        var id = $(this).val();
        var href = "terms.php?id="+id;
        window.location.href = href;
    });

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
   		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
