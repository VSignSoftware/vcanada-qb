<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();
if($setup['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$state_sales_tax=$setup['state_sales_tax'];

$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
if($action==1)
{
	$state_sales_tax=$_POST['state_sales_tax'];

	$valid=false;

	// must be 0, 1 or decimal
	$test = floatval($state_sales_tax);

	if($test==0)
	{
		$valid=true;
	}
	if($test==1)
	{
		$valid=true;
	}
	if( ($test>0) && ($test<1) )
	{
		$valid=true;
	}

	if($valid)
	{
		$s = $vujade->update_row('misc_config',1,'state_sales_tax',$test);
	}
	else
	{
		$vujade->errors[]="The number you entered is not a valid value. Valid values must be a decimal. Please enter a decimal value with no other characters.";
	}
	$setup = $vujade->get_setup();
	$state_sales_tax=$setup['state_sales_tax'];
	if($s==1)
	{
		$vujade->messages[]="Changes saved.";
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "State Sales Tax Setup - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=8;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<h3>State Sales Tax Percentage:</h3>
                	<form method = "post" action = "state_sales_tax.php">
                	<input type = "hidden" name = "action" value = "1">
                		<table>
                			<tr>
                				<td>
                					<input type = "text" name = "state_sales_tax" value = "<?php print $state_sales_tax; ?>" class = "form-control" style = "width:150px;">
                				</td>

                				<td>&nbsp;
                				</td>
                				<td>
                					<input type = "submit" value = "SAVE" class = "btn btn-primary">
                				</td>
                			</tr>
                		</table>
					</form>

				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
