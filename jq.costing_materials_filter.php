<?php
session_start();
print '<meta charset="ISO-8859-1">';
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$id = $_POST['id'];
$reset = $_POST['reset'];
if($reset==1)
{
	$materials = $vujade->get_inventory($sort=1,$start=1,$no_limit=1);
	if($materials['error']=='0')
	{
		unset($materials['error']);
		//print '<div style = "overflow:auto;height:300px;">';
		//print '<input type = "text" class = "dp"><br>';
		print '<table id = "itable" class = "table">';
		print '<tr>';
		print '<td><strong>Date</strong>';
		print '</td>';
		print '<td><strong>ID</strong>';
		print '</td>';
		print '<td><strong>Description</strong>';
		print '</td>';
		print '<td><strong>Size</strong>';
		print '</td>';
		print '<td><strong>Cost</strong>';
		print '</td>';
		print '<td>';
		print '&nbsp;';
		print '</td>';
		print '</tr>';
		foreach($materials as $m)
		{
			print '<tr>';
			print '<td>';
			print '<input type = "text" class = "" name = "date-'.$m['database_id'].'" id = "date-'.$m['database_id'].'">';
			print '</td>';
			print '<td>';
			print $m['inventory_id'];
			print '</td>';
			print '<td>';
			print $m['description'];
			print '</td>';
			print '<td>';
			print $m['size'];
			print '</td>';
			print '<td>';
			//print $m['cost'];
			@$c = number_format($m['cost'],2,'.',',');
			print '$'.$c;
			print '</td>';
			print '<td>';
			# must have create permission 
			if($projects_permissions['create']==1)
			{
				print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-xs btn-primary pull-right">+</a>';
			}
			print '</td>';
			print '</tr>';
		}
		print '</table>';
		//print '</div>';
	}
}
else
{
	$filter = $_POST['filter'];
	$filter_field = $_POST['filter_field'];
	if($filter_field==1)
	{
		$col="inventory_id";
	}
	if($filter_field==2)
	{
		$col="description";
	}
	if($filter_field==3)
	{
		$col="size";
	}
	if($filter_field==4)
	{
		$col="cost";
	}
	if($filter_field==5)
	{
		$col="all";
	}
	$materials = $vujade->filter_inventory($filter,$col);
	if($materials['error']=='0')
	{
		unset($materials['error']);
		//print '<div style = "overflow:auto;height:300px;">';
		print '<table id = "itable" class = "table">';
		print '<tr>';
		print '<td><strong>Date</strong>';
		print '</td>';
		print '<td><strong>ID</strong>';
		print '</td>';
		print '<td><strong>Description</strong>';
		print '</td>';
		print '<td><strong>Size</strong>';
		print '</td>';
		print '<td><strong>Cost</strong>';
		print '</td>';
		print '<td>';
		print '&nbsp;';
		print '</td>';
		print '</tr>';
		foreach($materials as $m)
		{
			print '<tr>';
			print '<td>';
			print '<input type = "text" class = "" name = "date-'.$m['database_id'].'" id = "date-'.$m['database_id'].'">';
			print '</td>';
			print '<td>';
			print $m['inventory_id'];
			print '</td>';
			print '<td>';
			print $m['description'];
			print '</td>';
			print '<td>';
			print $m['size'];
			print '</td>';
			print '<td>';
			@$c = number_format($m['cost'],2,'.',',');
			print '$'.$c;
			print '</td>';
			print '<td>';
			# must have create permission 
			if($projects_permissions['create']==1)
			{
				print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-primary btn-xs pull-right">+</a>';
			}
			print '</td>';
			print '</tr>';
		}
		print '</table>';
		//print '</div>';
	}
}
?>

<!-- BEGIN: PAGE SCRIPTS -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!--
<script src="vendor/plugins/moment/moment.min.js"></script>
<script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
-->
<script type="text/javascript">
jQuery(document).ready(function() 
{
	// datepickers
    // $(".dp").datepicker();

	$('.plus').click(function(e)
	{
		e.preventDefault();
		var id = "<?php print $id; ?>";
		var item_id = this.id;
		var date = $('#date-'+item_id).val();
		var href = "costing_inventory.php?id="+id+"&action=1&item_id="+item_id+"&date_entered="+date;
		window.location.href = href;
	});
});	
</script>