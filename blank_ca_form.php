<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
if(!isset($_REQUEST['dt']))
{
	$download_type='D';
}
else
{
	$download_type=$_REQUEST['dt'];
}
if(isset($_REQUEST['id']))
{
    $id = $_REQUEST['id'];
    $project = $vujade->get_project($id,2);
    $project['open_date']=date('m/d/Y');
    $download_type='I';
}

$html='<!DOCTYPE html>
<html>
<head>
    <title>Customer Acceptance Form</title>
    <style>
        *
        {
            margin:0;
            padding:0;
            font-family:Arial;
            font-size:10pt;
            color:#000;
        }
        body
        {
            width:100%;
            font-family:Arial;
            font-size:10pt;
            margin:0;
            padding:0;
        }
         
        p
        {
            margin:0;
            padding:0;
        }
         
        #wrapper
        {
            width:180mm;
            margin:0 15mm;
        }
         
        .page
        {
            height:297mm;
            width:210mm;
            page-break-after:always;
        }
 
        
    </style>
</head>
<body>
<div id="wrapper">
     <img style="padding-top:8mm; width: 60%; display: block;" src="images/proposal_logo.jpg">

    <p style="text-align:center; font-size: 7mm; padding-top:2mm;">ACCEPTANCE / COMPLETION REPORT</p>
    <br />
    <p style="float:left;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;width: 18%">Job Number :</p><p style="margin-top:-1px;margin-left:5%;float:left;width:75%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">'.$project['project_id'].'&nbsp;</p>
    <br>
    <p style="float:left;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;width: 18%">Job Name :</p><p style="margin-left:5%;float:left;width:75%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">'.$project['site'].'&nbsp;</p>
    <br/>
    <p style="float:left;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;width: 18%">Job Address :</p><p style="margin-left:5%;float:left;width:75%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">'.$project['address_1'].'&nbsp;</p>
    <br/>
    <p style="float:left;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;width: 18%">City, State, Zip :</p><p style="margin-left:5%;float:left;width:75%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">'.$project['city'].' '.$project['state'].' '.$project['zip'].'&nbsp;</p>
    <br/>
    <p style="float:left;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;width: 18%">Date :</p><p style="margin-left:5%;float:left;width:75%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">'.$project['open_date'].'&nbsp;</p>
    <br/>
    <p style="float:left;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;width: 18%">Installer(s) :</p><p style="margin-left:5%;float:left;width:75%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">&nbsp;</p>
    <br>
    <p style="text-align:center; font-weight: bold; font-size: 4mm; padding-top:2mm;">This Form Must Be Signed By The Customer Representative Upon Completion Of The Job.</p>
    <br>
    <input type="checkbox" value="none" style="border:1mm solid black;"> &nbsp;&nbsp;&nbsp;      All signed are installed in the correct location as per the drawings:</input><br>
    <input type="checkbox" value="none" style="border:1mm solid black;">   &nbsp;&nbsp;&nbsp;   All signs or letters are level and straight.</input><br>
    <input type="checkbox" value="none" style="border:1mm solid black;">  &nbsp;&nbsp;&nbsp;     Final electrical hookups have been made, if applicable.</input><br>
    <input type="checkbox" value="none" style="border:1mm solid black;">   &nbsp;&nbsp;&nbsp;    All signs are lighted properly and/or have been tested.</input><br>
    <input type="checkbox" value="none" style="border:1mm solid black;">   &nbsp;&nbsp;&nbsp;    All signs have been cleaned after installation.</input>
    <br>
    <input type="checkbox" value="none" style="border:1mm solid black;">   &nbsp;&nbsp;&nbsp;    All debris have been removed and the area is clean.</input>
    <br>
    <input type="checkbox" value="none" style="border:1mm solid black;">    &nbsp;&nbsp;&nbsp;   Customer accepts signage and installation as completed, except as noted.</input>
    <br>

    <br>
     <p style="font-size: 4mm; padding-top:2mm;">Customer Comments:</p>   
    <br>
    <p style="text-decoration: none;border-bottom: 1px solid black;"></p><br><br>
    <p style="text-decoration: none;border-bottom: 1px solid black;"></p><br><br>
    <p style="text-decoration: none;border-bottom: 1px solid black;"></p><br><br>
    <p style="text-decoration: none;border-bottom: 1px solid black;"></p><br><br>
    <br>
    <p style="text-align:center;font-size:4mm;text-decoration: none;width: 40%;">Customer Representative Signature :</p> <p style="margin-top:-20px;float:left;margin-left:40%;width:53%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;"> &nbsp;</p>
    <br>
    <p style="text-align:center;font-size:4mm;text-decoration: none;width: 40%;margin-left: 27mm;padding-top:2mm;">Print :</p> <p style="margin-top:-20px;float:left;margin-left:40%;width:53%;font-size:4.5mm;text-decoration: none;border-bottom: 1px solid black;">&nbsp;</p>
    <br>
    <p style="text-align:center;font-size:4mm;text-decoration: none;width: 40%;margin-left: 27mm;padding-top:2mm;">Date :</p> <p style="margin-top:-20px;float:left;margin-left:40%;width:53%;font-size:4mm;text-decoration: none;border-bottom: 1px solid black;">&nbsp;</p>
    
    <br>
    </div> 
</body>
</html>';

require_once("mpdf60/mpdf.php");
$mpdf=new mPDF('c','A4','','Helvetica' , 0 , 0 , 0 , 0 , 0 , 0); 
$mpdf->SetDisplayMode('fullpage');
$mpdf->list_indent_first_level = 0;  // 1 or 0 - whether to indent the first level of a list
$mpdf->WriteHTML($html);

// show the pdf in the iframe
if($download_type=="I")
{
	$mpdf->Output("Completion Report.pdf", "I");
}
else
{
	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-completion_report.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
		$mpdf->Output($pdfname, "F");
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		// output it to the browser (not mobile)
		$mpdf->Output($name, $download_type);
		// downloads as pdf
		//$pdf->Output($name,'I'); // outputs to browser as pdf
	}
}
?>