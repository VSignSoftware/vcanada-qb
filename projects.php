<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

unset($_SESSION['list_url']);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// paging
$start = 0;
# start value
$perpage = 100;
if(isset($_GET["page"]))
{
	$page = $_GET["page"];
	if(!ctype_digit($page))
	{
		$page = 1;
	}
}
else
{
	$page = 1;
}
$calc = $perpage * $page;
$start = $calc - $perpage;
$totalPages=1;

// action
if( (!isset($_REQUEST['action'])) || ($_REQUEST['action']=='') || ($_REQUEST['action']==0) || (empty($_REQUEST['action'])) )
{
	$action==1;
}
else
{
	$action=$_REQUEST['action'];
}

// default
if($action==1)
{
	$number=date('y');
	$search = array('number'=>$number);
	$found_projects=$vujade->find_projects($search,$start,0,false,'project_id');
	$total = $found_projects['count'];
	$found_projects2=$vujade->find_projects_helper($found_projects['sql']);
}

// user searched from the top bar or the left bar (advanced search)
if($action==2)
{
	// new method: get search from stored results in database
	$params = $vujade->get_search_params($_SESSION['search_id']);
	if($params['error']=="0")
	{
		$search = array('number'=>$params['number'],'name'=>$params['name'],'billing_name'=>$params['billing_name'],'address'=>$params['address'],'status'=>$params['status'],'city'=>$params['city'],'state'=>$params['state'],'salesperson'=>$params['sp'],'pm'=>$params['pm'],'scope'=>$params['scope']);
	}

	$found_projects=$vujade->find_projects($search,$start,0,false,'project_id');
	$total = $found_projects['count'];
	$found_projects2=$vujade->find_projects_helper($found_projects['sql'],0,'project_id');
	//print_r($found_projects);
	//die;

	// prevent job number search field from having a value in it
	if(!empty($params['scope']))
	{
		$params['number']='';
	}

}

$setup = $vujade->get_setup();

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$title = 'Project Search - ';
require_once('tray_header.php');
?>

<style>
    .clickableRow:hover
    {
        background-color: #83ACE2;
    }
</style>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <!-- search fields -->
        <aside class="tray /*tray-left*/ tray320 p20">

        	<div class="admin-form theme-primary" style = "margin-top:50px;">
        		<div class="panel heading-border panel-primary">
                	<div class="panel-body bg-light">

                		<h3>Project Search</h3>

        				<form method = "post" action = "search.php" id = "form">

        					<div class="section">
	                          <label class="field">
	                            <input class="gui-input" type = "text" name = "number" id = "number" value = "<?php print $params['number']; ?>" placeholder="Project Number" tabindex = "1">
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field">
	                            <input class="gui-input" type = "text" name = "name" id = "name" value = "<?php print $params['name']; ?>" placeholder="Project Name" tabindex = "2">
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field select">
	                            <select name="status" id = "status" tabindex="3">
	                            	<?php
	                            	//print '<option value="'.$status.'" selected>'.$status.'</option>';
	                            	if(!empty($params['status']))
	                            	{
	                            		$status_name=$vujade->get_status_by_number($params['status']);
	                            		if(!empty($status_name))
	                            		{
	                            			print '<option value="'.$params['status'].'">'.$status_name.'</option>';
	                            		}
	                            	}
	                            	?>
	                            	<option value="">-Status-</option>
									<option value="17">All Pending</option>
									<option value="1">Pending</option>
									<option value="11">Pending Drawings</option>
									<option value="12">Pending Estimates</option>
									<option value="14">Pending Proposal Sent</option>
									<option value="16">All Active</option>
									<option value="3">Active LL Approval</option>
									<option value="4">Active Permits</option>
									<option value="9">Active Survey</option>
									<option value="5">Active Manufacturing</option>
									<option value="6">Active Install</option>
									<option value="15">Service</option>
									<option value="10">Ready to be turned in</option>
									<option value="7">Turned in for Billing</option>
									<option value="2">Did Not Sell</option>
									<option value="8">Closed</option>
								</select>
	                            <i class="arrow double"></i>
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field">
	                            <input class="gui-input" type = "text" name = "address" id = "address" value = "<?php print $params['address']; ?>" placeholder="Site Address" tabindex = "4">
	                          </label>
	                        </div>
							
							<div class="section">
	                          <label class="field">
	                            <input class="gui-input" type = "text" name = "city" id = "city" value = "<?php print $params['city']; ?>" placeholder="City" tabindex = "5">
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field select">
	                            <select name = "state" id = "state" tabindex = "6">
	                            	<?php
	                            	if(!empty($params['state']))
	                            	{
	                            		print '<option value="'.$params['state'].'" selected>'.$params['state'].'</option>';
	                            	}
	                            	if($setup['country']=="USA")
		          					{
	                            	?>
	                            	<option value = "">-State-</option>
									<option value="AL">Alabama</option>
									<option value="AK">Alaska</option>
									<option value="AZ">Arizona</option>
									<option value="AR">Arkansas</option>
									<option value="CA">California</option>
									<option value="CO">Colorado</option>
									<option value="CT">Connecticut</option>
									<option value="DE">Delaware</option>
									<option value="DC">District Of Columbia</option>
									<option value="FL">Florida</option>
									<option value="GA">Georgia</option>
									<option value="HI">Hawaii</option>
									<option value="ID">Idaho</option>
									<option value="IL">Illinois</option>
									<option value="IN">Indiana</option>
									<option value="IA">Iowa</option>
									<option value="KS">Kansas</option>
									<option value="KY">Kentucky</option>
									<option value="LA">Louisiana</option>
									<option value="ME">Maine</option>
									<option value="MD">Maryland</option>
									<option value="MA">Massachusetts</option>
									<option value="MI">Michigan</option>
									<option value="MN">Minnesota</option>
									<option value="MS">Mississippi</option>
									<option value="MO">Missouri</option>
									<option value="MT">Montana</option>
									<option value="NE">Nebraska</option>
									<option value="NV">Nevada</option>
									<option value="NH">New Hampshire</option>
									<option value="NJ">New Jersey</option>
									<option value="NM">New Mexico</option>
									<option value="NY">New York</option>
									<option value="NC">North Carolina</option>
									<option value="ND">North Dakota</option>
									<option value="OH">Ohio</option>
									<option value="OK">Oklahoma</option>
									<option value="OR">Oregon</option>
									<option value="PA">Pennsylvania</option>
									<option value="RI">Rhode Island</option>
									<option value="SC">South Carolina</option>
									<option value="SD">South Dakota</option>
									<option value="TN">Tennessee</option>
									<option value="TX">Texas</option>
									<option value="UT">Utah</option>
									<option value="VT">Vermont</option>
									<option value="VA">Virginia</option>
									<option value="WA">Washington</option>
									<option value="WV">West Virginia</option>
									<option value="WI">Wisconsin</option>
									<option value="WY">Wyoming</option>
									<option value="">---------------</option>
						              <option value="">-Canadian Provinces-</option>
						              <option value="Alberta">Alberta</option>
						              <option value="British Columbia">British Columbia</option>
						              <option value="Manitoba">Manitoba</option>
						              <option value="New Brunswick">New Brunswick</option>
						              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
						              <option value="Northwest Territories">Northwest Territories</option>
						              <option value="Nova Scotia">Nova Scotia</option>
						              <option value="Nunavut">Nunavut</option>
						              <option value="Ontario">Ontario</option>
						              <option value="Prince Edward Island">Prince Edward Island</option>
						              <option value="Quebec">Quebec</option>
						              <option value="Saskatchewan">Saskatchewan</option>
						              <option value="Yukon">Yukon</option>
						        <?php 
						    	} 
						    	// canadian servers
						    	if($setup['country']=='Canada')
						    	{
						    		?>
						    		  <option value="">-Canadian Provinces-</option>
						              <option value="Alberta">Alberta</option>
						              <option value="British Columbia">British Columbia</option>
						              <option value="Manitoba">Manitoba</option>
						              <option value="New Brunswick">New Brunswick</option>
						              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
						              <option value="Northwest Territories">Northwest Territories</option>
						              <option value="Nova Scotia">Nova Scotia</option>
						              <option value="Nunavut">Nunavut</option>
						              <option value="Ontario">Ontario</option>
						              <option value="Prince Edward Island">Prince Edward Island</option>
						              <option value="Quebec">Quebec</option>
						              <option value="Saskatchewan">Saskatchewan</option>
						              <option value="Yukon">Yukon</option>
						              <option value="">---------------</option>
						              <option value="">-US States-</option>
						              <option value="AL">Alabama</option>
						              <option value="AK">Alaska</option>
						              <option value="AZ">Arizona</option>
						              <option value="AR">Arkansas</option>
						              <option value="CA">California</option>
						              <option value="CO">Colorado</option>
						              <option value="CT">Connecticut</option>
						              <option value="DE">Delaware</option>
						              <option value="DC">District Of Columbia</option>
						              <option value="FL">Florida</option>
						              <option value="GA">Georgia</option>
						              <option value="HI">Hawaii</option>
						              <option value="ID">Idaho</option>
						              <option value="IL">Illinois</option>
						              <option value="IN">Indiana</option>
						              <option value="IA">Iowa</option>
						              <option value="KS">Kansas</option>
						              <option value="KY">Kentucky</option>
						              <option value="LA">Louisiana</option>
						              <option value="ME">Maine</option>
						              <option value="MD">Maryland</option>
						              <option value="MA">Massachusetts</option>
						              <option value="MI">Michigan</option>
						              <option value="MN">Minnesota</option>
						              <option value="MS">Mississippi</option>
						              <option value="MO">Missouri</option>
						              <option value="MT">Montana</option>
						              <option value="NE">Nebraska</option>
						              <option value="NV">Nevada</option>
						              <option value="NH">New Hampshire</option>
						              <option value="NJ">New Jersey</option>
						              <option value="NM">New Mexico</option>
						              <option value="NY">New York</option>
						              <option value="NC">North Carolina</option>
						              <option value="ND">North Dakota</option>
						              <option value="OH">Ohio</option>
						              <option value="OK">Oklahoma</option>
						              <option value="OR">Oregon</option>
						              <option value="PA">Pennsylvania</option>
						              <option value="RI">Rhode Island</option>
						              <option value="SC">South Carolina</option>
						              <option value="SD">South Dakota</option>
						              <option value="TN">Tennessee</option>
						              <option value="TX">Texas</option>
						              <option value="UT">Utah</option>
						              <option value="VT">Vermont</option>
						              <option value="VA">Virginia</option>
						              <option value="WA">Washington</option>
						              <option value="WV">West Virginia</option>
						              <option value="WI">Wisconsin</option>
						              <option value="WY">Wyoming</option>
						    		<?php
						    	}	
						    	?>	
								</select>		
								<i class="arrow double"></i>
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field">
	                            <input class="gui-input" type = "text" name = "billing_name" id = "billing_name" value = "<?php print $params['billing_name']; ?>" placeholder="Billing Name" tabindex = "7">
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field select">
	                            <select name="sp" id = "sp" tabindex="8">
									<?php
	                            	if(!empty($params['sp']))
	                            	{
	                            		print '<option value="'.$params['sp'].'" selected>'.$params['sp'].'</option>';
	                            	}
	                            	print '<option value="">-Salesperson-</option>';
									$sps = $vujade->get_salespeople();
									if($sps['error']=="0")
									{
										unset($sps['error']);
										foreach($sps as $sp)
										{
											print '<option value = "'.$sp['fullname'].'">'.$sp['fullname'].'</option>';
										}
									}
									?>
								</select>
	                            <i class="arrow double"></i>
	                          </label>
	                        </div>

	                        <div class="section">
	                          <label class="field select">
	                            <select name="pm" id = "pm" tabindex="9">
									<?php
									if(!empty($params['pm']))
	                            	{
	                            		print '<option value="'.$params['pm'].'" selected>'.$params['pm'].'</option>';
	                            	}
	                            	print '<option value="">-Project Manager-</option>';
									$pms = $vujade->get_project_managers();
									if($pms['error']=="0")
									{
										unset($pms['error']);
										foreach($pms as $pm)
										{
											print '<option value = "'.$pm['fullname'].'">'.$pm['fullname'].'</option>';
										}
									}
									?>
								</select>
	                            <i class="arrow double"></i>
	                          </label>
	                        </div>
							
							<input type = "hidden" name = "action" value = "1">
							<a id = "reset" href = "#" class = "btn btn-lg btn-danger" tabindex = "11">RESET</a> 
							<a id = "search" href = "#" class = "btn btn-lg btn-success" tabindex = "10">SEARCH</a> 
							
							</form>
					</div>
                </div>
            </div>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <!-- search results (blank by default) -->
        
        <div class="tray tray-center" id="push-down">

            <div class="pl20 pr50" style="min-width: 340px; padding: 0px;">

            	<?php
            	//print_r($found_projects);
				if($found_projects['error']==0)
				{
					unset($found_projects['error']);
					print '<div class = "alert alert-success" style = "margin-top:50px;"><strong>Matching Projects: '.$found_projects['count'].'</strong></div>';
					?>

					<div class="panel heading-border panel-primary">
	                	<div class="panel-body bg-light" style="overflow: auto !important;">
							<div class = "row">
					            <div class="col-md-12">

	            	<?php
	            	$q1=$found_projects['sql'];
	            	$q2=$found_projects['sql2'];
	            	//print $found_projects['sql'].'<br>';
	            	//print $found_projects['sql2'].'<br>';
					$t=$found_projects['count'];
					unset($found_projects['count']);
					unset($found_projects['sql']);
					unset($found_projects['sql2']);
					if($total>0)
					{
						//$_SESSION['project_search']=1;
						$pids = array();
						foreach($found_projects2 as $fp)
						{
							$pids[]=$fp;
						}
						$pids_str=implode('^',$pids);
						if(isset($_SESSION['search_id']))
						{
							$s=array();
							$s[]=$vujade->update_row('project_search',$_SESSION['search_id'],'pids',$pids_str);
						}
						//$_SESSION['project_ids']=$pids;
					}
					if($t>0)
					{
						?>
						<table class="table table-striped table-hover" id="datatable" cellspacing="0" width="100%">
						<thead>
	                      <tr>
	                        <th>Number</th>
	                        <th>Name</th>
	                        <th>Address</th>
	                        <th>Salesperson</th>
	                        <th>&nbsp;</th>
	                        <th>Status</th>
	                      </tr>
	                    </thead>
	                    <tbody>
	                    <?php
						foreach($found_projects as $project_id)
						{
							$p=$vujade->get_project($project_id,2);
							if($p['error']==0)
							{
								unset($p['error']);
								$link = 'project.php?id='.$p['project_id'];

				           		print '<tr class="clickableRow" href="'.$link.'" style = "">';

								print '<td valign = "top" style = "">';
								print '<a href = "'.$link.'">';
								print $p['project_id'];
								print '</a></td>';

								print '<td valign = "top">';
								print $p['site'].'<br>';
								//print 'Date Opened: '.$p['open_date'];
								
								print '</td>';

								print '<td valign = "top">';
								print $p['address_1'].' &bull; ';
								print $p['city'].', ';
								print $p['state'].' ';
								print $p['zip'];
								print '</td>';

								print '<td valign = "top">';
								print $p['salesperson'].'<br>';
								print '</td>';

								print '<td valign = "top">';
								print '&nbsp;';
								print '</td>';

								print '<td valign = "top">';
								print $p['status'].'<br>';
								print '</td>';

								print '</tr>';
							}
							else
							{
								print '<tr class = "">';
								print '<td cospan="5" class = "">';
								print '<div class = "alert alert-danger">';
								print 'Unable to display projects. Please contact the site administrator.<br>';
								if($vujade->debug==1)
								{
									print 'Database or class error: '.$p['error'];
								}
								print '</div>';
								print '</td>';
								print '</tr>';
							}
						}
						print '</tbody></table>';
						$totalPages = ceil($total / $perpage);
						?>
						
						<div id = "paging" style = "margin-left:auto;margin-right:auto;text-align:center;">
						</div>

						<?php
						print '</div></div></div></div>';
					}
					else
					{
						print '<div class = "alert alert-danger">';
						if($vujade->debug==1)
						{
							//print 'Database or class error: '.$found_projects['error'].'<br>';
							//print $found_projects['sql'].'<br>';
						}
						print 'No projects found.';
						//print $q1.'<br>';
	            		//print $q2.'<br>';
						print '</div>';

						$totalPages = 1;
					}
				}
				else
				{
					print '<div class = "alert alert-danger">';
					print 'No projects found.';
					print $found_projects['sql'].'<br>';
	            	print $found_projects['sql2'].'<br>';
					print '</div>';
					$totalPages = 1;
				}
				if(isset($pids))
				{
					unset($pids);
				}
				$pids=array();
				if(isset($_SESSION['project_ids']))
				{
					foreach($_SESSION['project_ids'] as $dbid) 
					{
						$project = $vujade->get_project($dbid,2);
						$pids[]=$project['project_id'];
					}
					sort($pids);
					unset($_SESSION['project_ids']);
					$_SESSION['project_ids']=$pids;
				}
				?>

            </div>
        </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Datatables -->
  <script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script src="assets/js/paging.js"></script>
  <script type="text/javascript">

  /*
  $(document).on('click','.clickableRow',function()
  {
  		window.document.location = $(this).attr("href");
  });
  */
  
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // paginator plugin
    $('#paging').twbsPagination({
    totalPages: <?php print $totalPages; ?>,
    visiblePages: 25,
    href: 'projects.php?action=<?php print $action;?>&page={{number}}'
	});

    // submit the search form on press enter
    $('#form').on('keypress', function(e) 
    {
		if (e.which == 13) 
		{
			$('#form').submit();
			return false;
		}
	});

    $("#number").focus();

    $('#search').click(function(e) 
    {
    	e.preventDefault();
        $('#form').submit();
    });

    $('.clickableRow').click(function(e) 
    {
    	e.preventDefault();
        window.document.location = $(this).attr('href');
    });

    $('#reset').click(function(e) 
    {
    	e.preventDefault();
        window.document.location = "projects.php?action=1";
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

<?php $vujade->script_stop_time(); ?>

</body>

</html>
