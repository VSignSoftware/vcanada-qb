<?php
session_start();
print '<meta charset="ISO-8859-1">';
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$estimate_id = $_POST['estimate_id'];
$reset = $_POST['reset'];
?>

<!DOCTYPE html>
<html>

<head>
<!-- Meta, title, CSS, favicons, etc. -->
<meta charset="utf-8">
<title><?php print $title; ?></title>
<meta name="keywords" content="<?php print $title; ?>" />
<meta name="description" content="<?php print $title; ?>">
<meta name="author" content="V Sign Software">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='stylesheet' type='text/css' href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600'>
<link rel="stylesheet" type="text/css" href="assets/skin/default_skin/css/theme.css">
<link rel="stylesheet" type="text/css" href="assets/admin-tools/admin-forms/css/admin-forms.css">
<link rel="stylesheet" type="text/css" href="assets/fonts/font-awesome/font-awesome.css">

<?php
if($reset==1)
{
	$materials = $vujade->get_inventory($sort=1,$start=1,$no_limit=1);
	if($materials['error']=='0')
	{
		unset($materials['error']);
		?>

		<div class = "container">
			<div class = "row" style = "width:500px;height:40px;background-color:#CADDF5;border:1px solid #5D9CEC;color:#2d6ca2;padding-top:10px;font-weight:bold;">
				<div style = "float:left;padding-left:10px;width:100px;">
					ID
				</div>
				<div style = "float:left;width:200px;">
					Description
				</div>
				<div style = "float:left;width:75px;">
					Size
				</div>
				<div style = "float:left;width:75px;">
					Cost
				</div>
				<div style = "float:left;width:50px;">
					&nbsp;
				</div>
			</div>
		</div>

		<div style = "height:800px;overflow:auto;">
			<?php
			foreach($materials as $m)
			{
				if(empty($m['inventory_id']))
				{
					$m['inventory_id']=$m['database_id'];
				}
				print '<div style = "width:485px;border-bottom:1px solid #cecece;clear:both;">';
				print '<div style = "padding-left:10px;padding-top:5px;padding-bottom:5px;width:100px;float:left;">';
				print $m['inventory_id'];
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:200px;float:left;">';
				print $m['description'];
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:75px;float:left;">';
				print $m['size'];
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:75px;float:left;">';
				$c = trim($m['cost']);
				@$c = number_format($c,2,'.',',');
				print '$'.$c;
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:30px;float:left;">';
				print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-primary btn-xs">+</a>';
				print '</div>';
				print '</div>';
			}
			?>
		</div>

		<?php
	}
	else
	{
		print '<div class = "alert alert-danger">No items found.</div>';
	}
}
else
{
	$filter = $_POST['filter'];
	$filter_field = $_POST['filter_field'];
	if($filter_field==1)
	{
		$col="inventory_id";
	}
	if($filter_field==2)
	{
		$col="description";
	}
	if($filter_field==3)
	{
		$col="size";
	}
	if($filter_field==4)
	{
		$col="cost";
	}
	if($filter_field==5)
	{
		$col="all";
	}
	$materials = $vujade->filter_inventory($filter,$col);
	if($materials['error']=='0')
	{
		unset($materials['error']);
		?>

		<div class = "container">
			<div class = "row" style = "width:500px;height:40px;background-color:#CADDF5;border:1px solid #5D9CEC;color:#2d6ca2;padding-top:10px;font-weight:bold;">
				<div style = "float:left;padding-left:10px;width:100px;">
					ID
				</div>
				<div style = "float:left;width:200px;">
					Description
				</div>
				<div style = "float:left;width:75px;">
					Size
				</div>
				<div style = "float:left;width:75px;">
					Cost
				</div>
				<div style = "float:left;width:50px;">
					&nbsp;
				</div>
			</div>
		</div>

		<div style = "height:800px;overflow:auto;">
			<?php
			foreach($materials as $m)
			{
				if(empty($m['inventory_id']))
				{
					$m['inventory_id']=$m['database_id'];
				}
				print '<div style = "width:485px;border-bottom:1px solid #cecece;clear:both;">';
				print '<div style = "padding-left:10px;padding-top:5px;padding-bottom:5px;width:100px;float:left;">';
				print $m['inventory_id'];
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:200px;float:left;">';
				print $m['description'];
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:75px;float:left;">';
				print $m['size'];
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:75px;float:left;">';
				$c = trim($m['cost']);
				@$c = number_format($c,2,'.',',');
				print '$'.$c;
				print '</div>';
				print '<div style = "padding-top:5px;padding-bottom:5px;width:30px;float:left;">';
				print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-primary btn-xs">+</a>';
				print '</div>';
				print '</div>';
			}
			?>
		</div>
		<?php	
	}
	else
	{
		print '<div class = "alert alert-danger">No items found.</div>';
	}
}
?>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
$(document).ready(function()
{

	"use strict";

    // Init Theme Core    
    Core.init();

	
});	
</script>