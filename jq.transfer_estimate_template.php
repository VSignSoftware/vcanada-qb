<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$setup = $vujade->get_setup();

// validate the estimate that will be transfered from
$estimate = trim($_REQUEST['estimate']);
$template = trim($_REQUEST['template']);
$name = $_REQUEST['name'];
if(!empty($estimate))
{
	$transfer_from = $vujade->get_estimate($estimate,'estimate_id');
}
if(!empty($template))
{
	$transfer_from = $vujade->get_estimate($template,'estimate_id');
}

if(empty($transfer_from['database_id']))
{
	print 'Invalid estimate.';
	die;
}

// get the next template id
$project_id='Template';
$estimates= $vujade->get_estimates_for_project($project_id);
if($estimates['error']!='0')
{
	$nextid=$project_id."-01";
}
else
{
	$nextid=$project_id."-".$estimates['next_id'];
}

# step 1: create the new template
$vujade->create_row('estimates');
$row_id = $vujade->row_id;
$new_id = $row_id;
$s = array();
$s[] = $vujade->update_row('estimates',$row_id,'estimate_id',$nextid);
$s[] = $vujade->update_row('estimates',$row_id,'project_id','Template');
$s[] = $vujade->update_row('estimates',$row_id,'is_template',1);
$s[] = $vujade->update_row('estimates',$row_id,'template_name',$name);
if($_REQUEST['copy_description']==1)
{
	$s[] = $vujade->update_row('estimates',$row_id,'description',$transfer_from['description']);
}
$s[] = $vujade->update_row('estimates',$row_id,'date',$transfer_from['date']);
$s[] = $vujade->update_row('estimates',$row_id,'approved_by',$transfer_from['approved_by']);
$s[] = $vujade->update_row('estimates',$row_id,'created_by',$transfer_from['created_by']);
$s[] = $vujade->update_row('estimates',$row_id,'overhead_buyout_rate',$transfer_from['overhead_buyout_rate']);
$s[] = $vujade->update_row('estimates',$row_id,'overhead_general_rate',$transfer_from['overhead_general_rate']);
$s[] = $vujade->update_row('estimates',$row_id,'indeterminant',$transfer_from['indeterminant']);
$s[] = $vujade->update_row('estimates',$row_id,'overhead_machines_rate',$transfer_from['overhead_machines_rate']);

# step 2: copy existing materials from the old estimate
$existing_materials = $vujade->get_materials_for_estimate($transfer_from['database_id']);
if(empty($existing_materials['error']))
{
	unset($existing_materials['error']);
	foreach($existing_materials as $m)
	{
		$vujade->create_row('estimates_material');
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'inventory_id',$m['inventory_id']);
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'estimate_id',$new_id);

		// get the current price of this item
		// the price from the old estimate might be outdated
		if($setup['is_qb']==1)
		{
			$item = $vujade->get_item($m['inventory_id'],'Name');
		}
		else
		{
			$item = $vujade->get_item($m['inventory_id'],'inventory_id');
		}
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'cost',$item['cost']);
		
		// transfer the rest of the data from the old estimate
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'cost_ext',$m['cost_ext']);
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'description',$m['description']);
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'qty',$m['qty']);
		$s[]=$vujade->update_row('estimates_material',$vujade->row_id,'type',$m['type']);
	}
}

# step 3: copy existing labor from the other estimate
$existing_labor = $vujade->get_labor_for_estimate($transfer_from['estimate_id']);
if(empty($existing_labor['error']))
{
	unset($existing_labor['error']);

	// determine if rate data can be copied from existing
	$error_check = 0;
	foreach($existing_labor as $l)
	{
		if(empty($l['labor_id']))
		{
			$error_check++;
		}
	}

	// labors in the estimates labor table have the labor id so they can 
	// be matched to the labor rates table
	if($error_check==0)
	{
		foreach($existing_labor as $l)
		{
			$vujade->create_row('estimates_time');
			$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'estimate_id',$nextid);
			$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_hours',$l['hours']);
			$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'labor_id',$l['labor_id']);

			// attempt to get the current rate and type for this
			$rate_data = $vujade->get_labor_rate($l['labor_id'],'id');
			if($rate_data['error']=="0")
			{
				unset($rate_data['error']);
				$rate = $rate_data['rate'];
				$type = $rate_data['type'];
			}
			else
			{
				// match wasn't found or id is null / 0 
				$rate=0;
				$type="";
			}
			
			$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_type',$type);
			$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'rate',$rate);
		}
	}
	else
	{
		// they can't be matched, so the fail safe needs to be run
		// the fail safe gets all labor types and creates a new row for each one in estimates labor, sets the rate and lable, but leaves the hours as 0; the user must fill in the hours
		$new_labor = $vujade->get_labor_types();
		if($new_labor['error']=="0")
		{
			unset($new_labor['error']);
			foreach($new_labor as $nl)
			{
				$vujade->create_row('estimates_time');
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'estimate_id',$nextid);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_hours',0);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_type',$nl['type']);					
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'rate',$nl['rate']);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'labor_id',$nl['database_id']);
			}	
		}
	}
}

// step 4: copy machine data from old estimate
$existing_machines = $vujade->get_machines_for_estimate($transfer_from['estimate_id']);
if(empty($existing_machines['error']))
{
	unset($existing_machines['error']);

	// determine if rate data can be copied from existing
	$error_check = 0;
	foreach($existing_machines as $l)
	{
		if(empty($l['labor_id']))
		{
			$error_check++;
		}
	}

	// labors in the estimates labor table have the labor id so they can 
	// be matched to the labor rates table

	//print 'error check: '.$error_check.'<hr>';

	if($error_check==0)
	{
		foreach($existing_machines as $l)
		{
			$vujade->create_row('estimates_machines');
			$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'estimate_id',$nextid);
			$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'time_hours',$l['hours']);
			$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'labor_id',$l['labor_id']);

			// attempt to get the current rate and type for this
			$rate_data = $vujade->get_machine_rate($l['labor_id']);
			if($rate_data['error']=="0")
			{
				unset($rate_data['error']);
				$rate = $rate_data['rate'];
				$type = $rate_data['type'];
			}
			else
			{
				// match wasn't found or id is null / 0 
				$rate=0;
				$type="";
			}
			
			$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'time_type',$type);
			$s[]=$vujade->update_row('estimates_machines',$vujade->row_id,'rate',$rate);
		}
	}
	else
	{
		// they can't be matched, so the fail safe needs to be run
		// the fail safe gets all labor types and creates a new row for each one in estimates labor, sets the rate and lable, but leaves the hours as 0; the user must fill in the hours
		$new_labor = $vujade->get_labor_types();
		if($new_labor['error']=="0")
		{
			unset($new_labor['error']);
			foreach($new_labor as $nl)
			{
				$vujade->create_row('estimates_time');
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'estimate_id',$nextid);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_hours',0);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'time_type',$nl['type']);					
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'rate',$nl['rate']);
				$s[]=$vujade->update_row('estimates_time',$vujade->row_id,'labor_id',$nl['database_id']);
			}	
		}
	}
}

# step 5: copy buyouts from old estimate
$existing_buyouts = $vujade->get_buyouts($transfer_from['database_id']);
if(empty($existing_buyouts['error']))
{
	unset($existing_buyouts['error']);
	foreach($existing_buyouts as $buyout)
	{
		$vujade->create_row('estimates_buyouts');
		$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'estimate_id',$new_id);
		$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'cost',$buyout['cost']);
		$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'description',$buyout['description']);
		$s[]=$vujade->update_row('estimates_buyouts',$vujade->row_id,'subcontractor',$buyout['subcontractor']);
	}
}
print 'Success';
?>