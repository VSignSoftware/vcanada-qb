<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$po_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($po_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$shop_order = $vujade->get_shop_order($id, 'project_id');
$poid = $_REQUEST['poid'];
$po = $vujade->get_purchase_order($poid);
if($po['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

// list id for subcontract
$odetails = $vujade->get_qb_list_id('Subcontract');
$list_id = $odetails['list_id'];

$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# save new item
if($action==1)
{
	$desc = $_POST['new_description'];
	$date = $_POST['new_date_required'];
	$amount = $_POST['new_amount'];

	$desc=$vujade->clean($desc);

	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$fakeid2 = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_purchaseorder_lineitem` (`Parent_ID`,`TxnLineID`,`ItemRef_ListID`,`Rate`,`Desc`,`Quantity`,`ReceivedQuantity`,`Amount`,`project_id`,`Other1`,`vujade_date_required`,`IsNew`) VALUES ('$poid','$fakeid','$list_id','$amount','$desc','1','0','$amount','$id','$fakeid2','$date','true')";
	$vujade->generic_query($sql,$debug); 
}

# update item
if($action==2)
{
	$db_id=$_POST['dbid'];
	$desc = $_POST['description'];
	$date = $_POST['date_required'];
	$amount = $_POST['amount'];

	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'Desc',$desc,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'vujade_date_required',$date,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'Rate',$amount,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'Amount',$amount,'TxnLineID');
}

# delete item
if($action==3)
{
	$db_id=$_POST['dbid'];
	$s[] = $vujade->delete_row('quickbooks_purchaseorder_lineitem',$db_id,0,'TxnLineID');
}

# done button was pressed
if($action==4)
{
	$notes = $_REQUEST['notes'];
	$price_quote = $_REQUEST['price_quote'];
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'price_quote',$price_quote);

	# database id of the duplicate copy in the costing purchase orders table
	$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);

	# update the amount
	$s[] = $vujade->update_row('costing_purchase_orders',$cpo['database_id'],'cost',$price_quote);

	// can't queue qb unless this purchase order has line items
	// if edit sequence ! null, queue mod, else queue new add
	$has_items = $vujade->get_materials_for_purchase_order($poid);
	if($has_items['error']=="0")
	{

		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;

		// qb add
		if(empty($po['edit_sequence']))
		{

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_ADD_PURCHASEORDER, $poid);
		}
		else
		{
			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_MOD_PURCHASEORDER, $poid);
		}
	}

	$vujade->page_redirect('project_purchase_orders.php?id='.$project_id.'&poid='.$poid);
}

// previous button was pressed
if($action==5)
{
	$notes = $_REQUEST['notes'];
	$price_quote = $_REQUEST['price_quote'];
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'price_quote',$price_quote);

	# database id of the duplicate copy in the costing purchase orders table
	$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);

	# update the amount
	$s[] = $vujade->update_row('costing_purchase_orders',$cpo['database_id'],'cost',$price_quote);

	$vujade->page_redirect('edit_purchase_order.php?project_id='.$project_id.'&poid='.$poid);
}
$po = $vujade->get_purchase_order($poid);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 8;
$section = 3;
$title = "Edit Purchase Order Subcontract - ".$project['project_id'].' - ';
//$charset='<meta charset="ISO-8859-1">';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Subcontract <?php print $po['purchase_order_id']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		
		<!-- vendor and ship to -->
		<div class = "row">
			<div class = "col-md-6">
				<div class="panel panel-primary">
				  <div class="panel-heading">
				    <span class="panel-title">To: </span>
				    <div class="widget-menu pull-right">
				    </div>
				  </div>
				  <div class="panel-body" style = "height:150px;">
				    <?php
					$vendor = $vujade->get_vendor($po['vendor_id'],'ListID');
					if($vendor['error']=="0")
					{
						print '<strong>';
						print $vendor['name'];
						print '</strong>';
						print '<br>';
						print $vendor['address_1'];
						if(!empty($vendor['address_2']))
						{
							print ', ' . $vendor['address_1'];
						}
						print '<br>';
						print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];
						print '<br>';
						print $vendor['phone'];
						?>
						<br>
						<br>
						<strong>Vendor Contact: </strong><br>
						<?php
						$vendor_contact = $vujade->get_vendor_contact($po['vendor_contact_id']);
						if($vendor_contact['error']=="0")
						{
							print $vendor_contact['fullname'].' - phone: ';
							print $vendor_contact['phone1'];
						}
					}
					else
					{
						//print $vendor['error'];
					}
					?>
				  </div>
				</div>
			</div>

			<div class = "col-md-6">
				<div class="panel panel-primary">
				  <div class="panel-heading">
				    <span class="panel-title">Ship To: </span>
				    <div class="widget-menu pull-right">
				    </div>
				  </div>
				  <div class="panel-body" style = "height:150px;">
				    <?php
				    print $po['company'].'<br>';
					print $po['address_1'];
					if(!empty($po['address_2']))
					{
						print ', '.$po['address_2'];
					}
					print '<br>';
					print $po['city'].', '.$po['state'].' '.$po['zip'];
					?>
				  </div>
				</div>
			</div>
		</div>

		<!-- po data -->
		<div class = "well">
			<table width = "100%">
				<tr>
					<td><strong>PO No.:</strong>
					</td>
					<td><?php print $po['purchase_order_id']; ?>
					</td>
					<td><strong>Date:</strong>
					</td>
					<td><?php print $po['date']; ?>
					</td>
					<td><strong>Revised: </strong>
					</td>
					<td><?php print $po['date_revised']; ?>
					</td>
				</tr>
				<tr>
					<td colspan = "6">&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong>Written By: </strong>
					</td>
					<td><?php print $po['ordered_by']; ?>
					</td>
					<td><strong>Type: </strong> 
					</td>
					<td><?php print $po['type']; ?>
					</td>
					<td>
					</td>
					<td>
					</td>
				</tr>
			</table>
		</div>

		<div>
		<table class = "table">
			<tr>
				<td class = "bordered size200" valign = "top" width = "70%">Description</td>
				<td valign = "top" class = "bordered size100">Date Required</td>
				<td valign = "top" class = "bordered size100">Amount</td>
				<td valign = "top" class = "bordered size100">&nbsp;</td>
			</tr>

			<?php
			# existing outsource
			$outsourced = $vujade->get_materials_for_purchase_order($poid);
			if($outsourced['error']=="0")
			{
				$price_quote=0;
				unset($outsourced['error']);
				$x = 1;
				$textareaids = array();
				foreach($outsourced as $outsource)
				{
					$price_quote+=$outsource['amount'];
				?>
					<tr>
						<form method = "post" action = "edit_purchase_order_subcontract.php">
						<td class = "bordered size200" valign = "top"  >
							<textarea name = "description" class = "form-control ckeditor" id = "<?php print $x; ?>">
								<?php print $outsource['description']; ?>
							</textarea>
						</td>
						<td valign = "top" class = "bordered size100">
							<input type = "text" class = "dp form-control" name = "date_required" value = "<?php print $outsource['date_required']; ?>">
						</td>
						<td valign = "top" class = "bordered size100">
							<input type = "text" class = "form-control" name = "amount" value = "<?php print $outsource['amount']; ?>">
						</td>
						<input type = "hidden" name = "action" value = "2">
						<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
						<input type = "hidden" name = "poid" value = "<?php print $poid; ?>">
						<input type = "hidden" name = "dbid" value = "<?php print $outsource['database_id']; ?>">
						<td valign = "top" class = "bordered size100">
							<input type = "submit" id = "update" class = "btn btn-primary btn-xs" value = "UPDATE" style = "float:left;margin-right:5px;"> 
							</form>

							<form method = "post" action = "edit_purchase_order_subcontract.php">
								<input type = "hidden" name = "action" value = "3">
								<input type = "hidden" name = "dbid" value = "<?php print $outsource['database_id']; ?>">
								<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
								<input type = "hidden" name = "poid" value = "<?php print $poid; ?>"> 
								<input class = "btn btn-xs btn-danger" type = "submit" id = "delete" value = "X">
							</form>
						</td>
					</tr>
				<?php
					$textareaids[]=$x;
					$x++;
				}
			}
			?>

			<tr>
				<form method = "post" action = "edit_purchase_order_subcontract.php">
				<td class = "bordered size200" valign = "top">
					<textarea name = "new_description" class = "form-control indented ckeditor"></textarea>
				</td>
				<td valign = "top" class = "bordered size100">
					<input type = "text" class = "dp form-control" name = "new_date_required">
				</td>
				<td valign = "top" class = "bordered size100">
					<input type = "text" class = "form-control" name = "new_amount">
				</td>
				<input type = "hidden" name = "action" value = "1">
				<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
				<input type = "hidden" name = "poid" value = "<?php print $poid; ?>">
				<td valign = "top" class = "bordered size100">
					<input type = "submit" id = "new" value = "SAVE" class = "btn btn-xs btn-primary">
				</td>
			</form>
			</tr>

		</table>

		</div>

		<div class = "well">
			<form method = "post" action = "edit_purchase_order_subcontract.php" id = "done">

			<div class = "row">
				<div class = "col-md-10">
					<strong>Billing Notes</strong> (These will only appear in the front office)<br>
						<textarea name = "notes" id = "notes" class = "form-control ckeditor">
							<?php print $po['notes']; ?>
					</textarea>
				</div>
				<div class = "col-md-2">
					<div class = "label label-dark pull-right">
						Quoted Price: $<?php print @number_format($price_quote,2,'.',','); ?>
					</div>
				</div>
			</div>
		</div>

		<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
		<input type = "hidden" name = "price_quote" value = "<?php print $price_quote; ?>">
		<input type = "hidden" name = "poid" value = "<?php print $poid; ?>">
		<input type = "hidden" name = "action" id = "action" value = "4">
		
		<!-- previous and done buttons -->
		<div style = "margin-top:15px;">
			<input type = "submit" value = "PREVIOUS" id = "previous" class = "btn btn-primary"> 

			<input type = "submit" value = "SAVE AND COMPLETE" id = "sbt" class = "btn btn-success">
		</div>

		</form>

    </div>
</div>

</section>
</section>

<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>

<!-- End: Main -->
<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{
	"use strict";

	// Init Theme Core    
	Core.init();

	$('.dp').datepicker();

	var n = $('#notes').html();
    $("#notes").html($.trim(n));

    <?php
    $count = count($textareaids);
    if($count>0)
    {
	    foreach($textareaids as $taid)
	    {
	    	?>
	    	var n<?php print $taid; ?> = $('#<?php print $taid; ?>').html();
	    	$("#<?php print $taid; ?>").html($.trim(n<?php print $taid; ?>));
	    	<?php
	    }
	}
    ?>

    // previous button was clicked
	$('#previous').click(function(e)
	{
		e.preventDefault();
		$('#action').val(5);
		$('#done').submit();
	});

});
</script>
<!-- END: PAGE SCRIPTS -->
</body>
</html>