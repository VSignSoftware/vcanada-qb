<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$setup=$vujade->get_setup();

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

if(isset($_POST['action']))
{
	$id = $_POST['id'];
	//$project = $vujade->get_project($id,2);
	$po = $vujade->get_purchase_order($id,'RefNumber');
	if($po['error']=="0")
	{
		if(!empty($po['project_id']))
		{
			$vujade->page_redirect('project_purchase_orders.php?id='.$po['project_id']);
		}
		else
		{
			// get vendor
			// attempt to get by list id
			$vendor1 = $vujade->get_vendor($po['vendor_id'],'ListID');
			if($vendor1['error']=='0')
			{	
				$vujade->page_redirect('vendor.php?id='.$vendor1['database_id'].'&tab=5');
			}
			
			// get by id column
			$vendor2 = $vujade->get_vendor($po['vendor_id']);
			if($vendor2['error']=='0')
			{	
				$vujade->page_redirect('vendor.php?id='.$vendor1['database_id'].'&tab=5');
			}

			// both failed, show error
			if( ($vendor1['error']!='0') || ($vendor2['error']!='0') )
			{
				$vujade->errors[]="Invalid Vendor ID.";
			}
		}
	}	
	else
	{
		$vujade->errors[]="Invalid purchase order number.";
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = 'Purchase Orders - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Purchase Orders</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu pull-right">
						<?php 
						if($setup['is_qb']==1)
						{
							print '<a href = "new_purchase_order.php?is_blank=1" class = "btn btn-primary pull-right btn-sm" style = "width:100px;">New</a>';
						}
						else
						{
							print '<a href = "edit_purchase_order.php?new=1&blank=1" class = "btn btn-primary pull-right btn-sm" style = "width:100px;">New</a>';
						}
						?>
					</div>
				</div>
	        	<div class="panel-body bg-light">
	        		<?php 
	        		// get every purchase order 
	        		// qb servers
	        		if($setup['is_qb']==1)
	        		{
	        			$n=4;
	        		}
	        		else
	        		{
	        			$n=0;
	        		}
	        		$pos = $vujade->get_all_blank_purchase_orders($n);
					if($pos['error']=="0")
					{
						unset($pos['error']);
						?>
			        		<!-- data table -->
			        		<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
									<thead>
										<tr style = "border-bottom:1px solid black;">
											<td valign = "top"><strong>Date</strong></td>
											<td valign = "top"><strong>PO#</strong></td>
											<td valign = "top"><strong>Vendor</strong></td>
											<td valign = "top"><strong>Items</strong></td>
											<td valign = "top"><strong>Jobs</strong></td>
											<td valign = "top"><strong>Cost</strong></td>
										</tr>
									</thead>

								    <tbody style = "font-size:14px;">
									<?php
									foreach($pos as $i)
									{
										// has a job number
										if(!empty($i['project_id']))
										{
											$link = 'project_purchase_orders.php?id='.$i['project_id'];
										}
										else
										{
											// is a blank po
											$link = 'edit_purchase_order.php?blank=1&poid='.$i['database_id'];
										}
								        
								        print '<tr class = "clickableRow-row">';

								        // date
								        $ts = strtotime($i['date']);
								        print '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><span style = "display:none;">'.$ts.'</span><a href = "'.$link.'" class = "ts-change">'.$i['date'].'</a></td>';
								        unset($ts);

								        // po id
								        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = ""><a href = "'.$link.'">'.$i['purchase_order_id'].'</a></td>';

								        // vendor
								        if($setup['is_qb']!=1)
						        		{
						        			$vendor = $vujade->get_vendor($i['vendor_id']);
						        			$i['vendor_name']=$vendor['name'];
						        		}

								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['vendor_name'].'</td>';

								        // items
								        // qb version
								        if($setup['is_qb']==1)
						        		{
									        $items = $vujade->get_materials_for_purchase_order($i['database_id']);
									        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';

									        if($items['error']=="0")
									        {
									        	unset($items['error']);
									        	$project_ids = array();
									        	foreach($items as $item)
									        	{
									        		$project_ids[]=$item['project_id'];
									        		$d = $item['description'];
									        		$d = $vujade->trim_text($d,70,60);
									        		print $d.'<br>';
									        	}
									        }
									        else
									        {
									        	// just show 1 job number; po is not materials; it is subcontract or outsource
									        	if(!empty($i['project_id']))
												{
													$project_ids[]=$i['project_id'];
													//print $i['project_id'];
												}
									        }
									        print '</td>';
									    }
									    else
									    {
									        // non qb
									        // $outsourced = $vujade->get_outsourced_items($poid);

									    	// materials type
									    	if($i['type']=="Materials")
									    	{
									    		$items = $vujade->get_materials_for_purchase_order($i['database_id']);
									    	}
									    	else
									    	{
									    		// outsource / subcontract
									    		$items = $vujade->get_outsourced_items($i['database_id']);
									    	}
										    print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';

									        if($items['error']=="0")
									        {
									        	unset($items['error']);
									        	$project_ids = array();
									        	foreach($items as $item)
									        	{
									        		$project_ids[]=$item['project_id'];
									        		$d = $item['description'];
									        		$d = $vujade->trim_text($d);
									        		print $d.'<br>';
									        	}
									        }
								        }

								        // jobs 
								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
								        if(count($project_ids)>0)
								        {
								        	foreach($project_ids as $pid)
								        	{
								        		print $pid.'<br>';
								        	}
								        }
								        unset($project_ids);
								        print '</td>';

								        // cost
								        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
								        print $i['total'];
								        print '</td>';

										print '</tr>';
									}

									?>
									</tbody>
							</table>
						<?php
					}
					else
					{
						$vujade->set_error('No purchase orders found. ');
						$vujade->set_error($pos['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      },
      "order": [[ 0, "desc" ]]
    });

    $('.clickableRow').click(function(e) 
	{
		e.preventDefault();
		var href = $(this).attr('href');
		window.location.href=href;
	});

});
</script>

</body>
</html>