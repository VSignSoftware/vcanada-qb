<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// qb config
require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
$dsn = $vujade->qbdsn;

$inventory_permissions = $vujade->get_permission($_SESSION['user_id'],'Inventory');
if($inventory_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($inventory_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($inventory_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

// 
$is_new=0;

// new
if($action==1)
{
	$title = "New Item - ";
	$is_new=1;
	$item=array();
}

// save new / update
if($action==2)
{	
	$id = $_REQUEST['id'];
	// set up for extra data array for quickbooks mod
	// if cogs account or income account different than original selection, send 
	// true values to extra data, else send false values
	$ed_value_1='false'; // default/not used
	$ed_value_2='false'; // default/not used
	$ed_value_3='false'; // default/not used
	$ed_value_4='false'; // default value of income account
	$ed_value_5='false'; // default value of cogs account
	$income_acct=$_POST['income_acct'];
	$cogs=$_POST['cogs'];
	if($income_acct!=$item['income_acct'])
	{
		//$ed_value_4='true';
	}
	if($cogs!=$item['cogs'])
	{
		//$ed_value_5='true';
	}
	$extradata = array(
				'AssignEvenIfUsed' => $ed_value_1,
				'AllowOverride' => $ed_value_2,
				'ForceUOMChange' => $ed_value_3,
				'ApplyIncomeAccountRefToExistingTxns' => $ed_value_4,
				'ApplyCOGSAccountRefToExistingTxns' => $ed_value_5
		);

	$is_new=$_REQUEST['is_new'];
	if($is_new!=1)
	{
		$item=$vujade->get_item($id);
	}

	$inventory_id=trim($_POST['inventory_id']);

	// can't be empty 
	if(empty($inventory_id))
	{
		$vujade->errors[]='ID cannot be blank or empty.';
	}
	else
	{
		// must be unique if user changed it
		if($inventory_id!=$item['inventory_id'])
		{
			$testitem = $vujade->get_item($inventory_id,"Name");
			if($testitem['error']=="0")
			{
				$vujade->errors[]='ID already exists. ID must be unique.';
				unset($inventory_id);
			}
			unset($testitem);
		}
	}

	// new quantity
	$new_qoh=trim($_POST['new_qoh']);

	// must be a number; zero allowed, decimals allowed
	$is_number = is_numeric($new_qoh);

	// active/inactive
	$active=$_REQUEST['active'];

	$e = $vujade->get_error_count();
	if($e==0)
	{
		$description=$_POST['description'];
		$description_secondary=$_POST['description_secondary'];
		$category=$_POST['category'];
		$size=$_POST['size'];
		$type=$_POST['type'];
		$uom=$_POST['uom'];
		$qoh=$_POST['qoh'];
		$tm = date('Y-m-d h:m:s');
		$asset_acct=$_POST['asset_acct'];
		$tax_code=$_POST['tax_code'];
		$cost = $_POST['cost'];

		$is_new=$_REQUEST['is_new'];
		if($is_new==1)
		{
			$vujade->create_row('quickbooks_inventoryitem');
			$id = $vujade->row_id;
			$tc = date('Y-m-d h:m:s');
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'TimeCreated',$tc);
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'IsActive','true');
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'QuantityOnHand',$qoh);
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'COGSAccountRef_ListID',$cogs);
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'IncomeAccountRef_ListID',$income_acct);
			$item=$vujade->get_item($id);
		}
		else
		{
			$id = $_REQUEST['id'];

			if($active==1)
			{
				$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'IsActive','true');
			}
			else
			{
				$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'IsActive','false');
			}
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'COGSAccountRef_ListID',$cogs);
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'IncomeAccountRef_ListID',$income_acct);

			$item=$vujade->get_item($id);
		}

		// update mysql
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'Name',$inventory_id);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'PurchaseDesc',$description);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'SalesDesc',$description);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'Notes',$description_secondary);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'Category',$category);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'Size',$size);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'Type',$type);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'TimeModified',$tm);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'UOM',$uom);

		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'PurchaseCost',$cost);

		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'AssetAccountRef_ListID',$asset_acct);
		$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'SalesTaxCodeRef_ListID',$tax_code);

		// update qoh
		if( ($is_number==1) && ($new_qoh!=$item['qoh']) )
		{
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'QuantityOnHand',$new_qoh);

			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$vujade->create_row('quickbooks_inventoryadjustment');
			$adj_id = $vujade->row_id;
			$s[] = $vujade->update_row('quickbooks_inventoryadjustment',$adj_id,'TxnID',$fakeid,'ID');
			$s[] = $vujade->update_row('quickbooks_inventoryadjustment',$adj_id,'AccountRef_ListID','8000005D-1322855217','ID');

			$item_list_id=$item['list_id'];
			$diff = $new_qoh-$item['qoh'];

			// Parent_ID	Parent_TxnID	TxnLineID	ItemRef_ListID	ItemRef_FullName	QuantityDifference	ValueDifference
			$fakeid2 = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			
			$sql = "INSERT INTO `quickbooks_inventoryadjustment_line` (`Parent_ID`,`Parent_TxnID`,`ItemRef_ListID`,`TxnLineID`,`QuantityDifference`,`ValueDifference`) VALUES ('$adj_id','$fakeid','$item_list_id','$fakeid2','$diff','0.00')";
			$s[]=$vujade->generic_query($sql,0,2);

			/*
			Next, create an entry in quickbooks_inventoryadjustment_line:
			-set Parent_ID and Parent_TxnID to the right values
			-set TxnLineID to be another randomly generated ID.
			-Set ItemRef_ListID to be the ListID of the item that is being adjusted.
			-If the item quantity was adjusted set 'QuantityDifference' to the positive/negative value based on the original. (EG, if you are decreasing the quantity by five, QuantityDifference is now -5. This means you'll have to calculate the difference between Pre-Adjustment and Post-Adjustment. Aaron suggested two boxes on the Inventory Item display: a non-editable Current Quantity, and then an Editable New Quantity box.)
			-If the item value was adjusted set 'ValueDifference' to the new Value. The behavior is the same as with Quantity above.
			*/

			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// add to qb
			if(function_exists('date_default_timezone_set'))
			{
				//IMPORTANT: Make sure this matches your correct time zone.
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_ADD_INVENTORYADJUSTMENT, $adj_id);
		}	

		// edit inventory
		if( (!empty($item['edit_sequence'])) && ($is_new==0) )
		{
			// update qb server
			if(function_exists('date_default_timezone_set'))
			{
				//IMPORTANT: Make sure this matches your correct time zone.
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_MOD_INVENTORYITEM, $id, 0, $extradata);
		}

		if($is_new==1)
		{
			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'ListID',$fakeid);

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_ADD_INVENTORYITEM, $id);
		}

		// set success message and refresh item array
		$vujade->messages[]="Item Updated";
	}

	$item=$vujade->get_item($id);
	$is_new=0;
	$title = $item['inventory_id']." - ";
}

// delete
if($action==3)
{
	$id = $_REQUEST['id'];
	$item=$vujade->get_item($id);
	
	$s[] = $vujade->update_row('quickbooks_inventoryitem',$id,'IsActive','false');

	// set up for extra data array for quickbooks mod
	// if cogs account or income account different than original selection, send 
	// true values to extra data, else send false values
	$ed_value_1='false'; // default/not used
	$ed_value_2='false'; // default/not used
	$ed_value_3='false'; // default/not used
	$ed_value_4='false'; // default value of income account
	$ed_value_5='false'; // default value of cogs account
	$extradata = array(
				'AssignEvenIfUsed' => $ed_value_1,
				'AllowOverride' => $ed_value_2,
				'ForceUOMChange' => $ed_value_3,
				'ApplyIncomeAccountRefToExistingTxns' => $ed_value_4,
				'ApplyCOGSAccountRefToExistingTxns' => $ed_value_5
		);

	/*
	// delete from mysql "queue"
	$q1 = "DELETE FROM `quickbooks_queue` WHERE `qb_action` = 'ItemInventoryAdd' AND `ident` = '$id' AND `dequeue_datetime` IS NULL";
	$s[]=$vujade->generic_query($q1,0,2);

	$q2 = "DELETE FROM `quickbooks_queue` WHERE `qb_action` = 'ItemInventoryMod' AND `ident` = '$id' AND `dequeue_datetime` IS NULL";
	$s[]=$vujade->generic_query($q2,0,2);
	*/
	
	if(!empty($item['edit_sequence']))
	{

		// update qb server
		if(function_exists('date_default_timezone_set'))
		{
			//IMPORTANT: Make sure this matches your correct time zone.
			date_default_timezone_set('America/Los_Angeles');
		}
		
		if (!QuickBooks_Utilities::initialized($dsn))
		{	
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}

		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_MOD_INVENTORYITEM, $id, 0, $extradata);
	}
	else
	{
		$s[]=$vujade->delete_row('quickbooks_inventoryitem',$id,0,'ID');
	}

	$vujade->page_redirect('inventory.php?m=1');
}

if($action==0)
{
	$id = $_REQUEST['id'];
	$item=$vujade->get_item($id);
	if($item['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$title = $item['inventory_id']." - ";
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $title; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "inventory.php" class = "btn btn-primary btn-sm">&laquo; Back</a>
					</div>
				</div>

	        	<div class="panel-body bg-light">
				<div class = "row">
					<div class = "col-md-6">

						<form method = "post" action = "<?php print $_SERVER['PHP_SELF']; ?>" id = "form">
							<table class = "table">
								<tr class = "project_search_results">
								<td class = "project_search_results">
									ID
								</td>
								<td class = "project_search_results">
									<input type = "text" name = "inventory_id" id = "inventory_id" value = "<?php print $item['inventory_id']; ?>" class = "form-control"> <i>(This number can be changed to your preference.)</i>
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Description
								</td>
								<td class = "project_search_results">
									<textarea name = "description" rows = "5" cols = "45" class = "form-control"><?php print $item['description']; ?></textarea>
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Category
								</td>
								<td class = "project_search_results">
									<input type = "text" name = "category" value = "<?php print $item['category']; ?>" class = "form-control">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Size
								</td>
								<td class = "project_search_results">
									<input type = "text" name = "size" value = "<?php print $item['size']; ?>" class = "form-control">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Type
								</td>
								<td class = "project_search_results">
									<input type = "text" name = "type" value = "<?php print $item['type']; ?>" class = "form-control">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Cost
								</td>
								<td class = "project_search_results">
									<input type = "text" name = "cost" value = "<?php print $item['cost']; ?>" class = "form-control">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Date Updated
								</td>
								<td class = "project_search_results">
									<?php print $item['date_updated']; ?>
								</td>
								</tr>

								<?php
								if($is_new==0)
								{
									?>
									<tr class = "project_search_results">
										<td class = "project_search_results">
											Quantity on hand
										</td>
										<td class = "project_search_results">
											<input type = "text" name = "qoh" value = "<?php print $item['qoh']; ?>" class = "form-control" disabled>
										</td>
									</tr>

									<tr class = "project_search_results">
										<td class = "project_search_results">
											New Quantity on hand
										</td>
										<td class = "project_search_results">
											<input type = "text" name = "new_qoh" value = "" class = "form-control">
										</td>
									</tr>
									<?php
								}
								else
								{
									?>
									<tr class = "project_search_results">
										<td class = "project_search_results">
											Quantity on hand
										</td>
										<td class = "project_search_results">
											<input type = "text" name = "qoh" value = "<?php print $item['qoh']; ?>" class = "form-control">
										</td>
									</tr>
									<?php
								}
								?>

								<tr class = "project_search_results">
									<td class = "project_search_results">
										Unit of measure
									</td>
									<td class = "project_search_results">
										<select name = "uom" class = "form-control">
											<?php
											if(!empty($item['uom']))
											{
												print '<option value = "'.$item['uom'].'" selected = "selected">'.$item['uom'].'</option>';
											}
											?>
											<option value = "">-Select-</option>
											<option value = "EACH">EACH</option>
											<option value = "FOOT">FOOT</option>
											<option value = "GALLON">GALLON</option>
											<option value = "PINT">PINT</option>
											<option value = "QUART">QUART</option>
											<option value = "ROLL">ROLL</option>
											<option value = "SQUARE FOOT">SQUARE FOOT</option>
											<option value = "SHEET">SHEET</option>
											<option value = "SET">SET</option>
											<option value = "TUBE">TUBE</option>
											<option value = "YARD">YARD</option>
										</select>
									</td>
									</tr>

									<tr class = "project_search_results">
									<td class = "project_search_results">
										COGS Account
									</td>
									<td class = "project_search_results">
										<select name = "cogs" class = "form-control">
										<?php
										if(!empty($item['cogs']))
										{
											$cog_data = $vujade->get_inventory_account($item['cogs']);
											print '<option value = "'.$item['cogs'].'" selected = "selected">'.$cog_data['account_name'].'</option>';
										}
										$cogs_accounts = $vujade->get_qb_accounts('Inventory COGS');
										if($cogs_accounts['error']=="0")
										{
											print '<option value = "">-Select-</option>';
											unset($cogs_accounts['error']);
											foreach($cogs_accounts as $cog)
											{
												print '<option value = "'.$cog['list_id'].'">'.$cog['label'].'</option>';
											}
										}
										?>
									</select>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">
										Income Account
									</td>
									<td class = "project_search_results">
										<select name = "income_acct" class = "form-control">
										<?php
										if(!empty($item['income_acct']))
										{
											$income_data = $vujade->get_inventory_account($item['income_acct']);
											print '<option value = "'.$item['income_acct'].'" selected = "selected">'.$income_data['account_name'].'</option>';
										}
										$income_accts = $vujade->get_qb_accounts('Inventory Income');
										if($income_accts['error']=="0")
										{
											print '<option value = "">-Select-</option>';
											unset($income_accts['error']);
											foreach($income_accts as $ia)
											{
												print '<option value = "'.$ia['list_id'].'">'.$ia['label'].'</option>';
											}
										}
										?>
										</select>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">
										Asset Account
									</td>
									<td class = "project_search_results">
										<select name = "asset_acct" class = "form-control">
										<?php
										if(!empty($item['asset_acct']))
										{
											$asset_data = $vujade->get_inventory_account($item['asset_acct']);
											print '<option value = "'.$item['asset_acct'].'" selected = "selected">'.$asset_data['account_name'].'</option>';
										}
										$asset_accts = $vujade->get_inventory_accounts('OtherCurrentAsset',1);
										if($asset_accts['error']=="0")
										{
											print '<option value = "">-Select-</option>';
											unset($asset_accts['error']);
											foreach($asset_accts as $aa)
											{
												print '<option value = "'.$aa['ListID'].'">'.$aa['account_name'].'</option>';
											}
										}
										else
										{
											print '<option value = "">'.$asset_accts['error'].'</option>';
											print '<option value = "">'.$asset_accts['sql'].'</option>';
										}
										?>
									</select>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">
										Tax Code
									</td>
									<td class = "project_search_results">
										<select name = "tax_code" class = "form-control">
										<?php
										if(!empty($item['tax_code']))
										{
											$tax_data = $vujade->get_sales_tax_code($item['tax_code']);
											print '<option value = "'.$item['tax_code'].'" selected = "selected">'.$tax_data['description'].'</option>';
										}
										$taxes = $vujade->get_sales_tax_codes();
										if($taxes['error']=="0")
										{
											print '<option value = "">-Select-</option>';
											unset($taxes['error']);
											foreach($taxes as $tax)
											{
												print '<option value = "'.$tax['id'].'">'.$tax['description'].'</option>';
											}
										}
										else
										{
											//print '<option value = "">'.$asset_accts['error'].'</option>';
											//print '<option value = "">'.$asset_accts['sql'].'</option>';
										}
										?>
										</select>
									</td>
								</tr>

								<tr class = "project_search_results">
									<td class = "project_search_results">
										Notes
									</td>
									<td class = "project_search_results">
										<textarea name = "description_secondary" rows = "5" cols = "45" class = "form-control"><?php print $item['description_secondary']; ?></textarea>
									</td>
								</tr>

								<tr>
									<td>
										Active
									</td>
									<td>
										<input type = "checkbox" name = "active" id = "active" value = "1" <?php if($item['is_active']=='true'){ print 'checked'; } ?>>
									</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
								&nbsp;
								</td>
								<td class = "project_search_results">
								<input type = "hidden" name = "id" value = "<?php print $id; ?>">
								<input type = "hidden" id = "action" name = "action" value = "2">
								<input type = "hidden" id = "is_new" name = "is_new" value = "<?php print $is_new; ?>">
								<input type = "submit" value = "SAVE" class = "btn btn-primary" id = "save"> 

								<?php
								if($is_new==0)
								{
									?>
									<a href = "#delete-item-modal" id = "delete-item" class = "btn btn-danger pull-right">Delete</a>
								<?php 
								} 
								else
								{
								?>
									<a href = "inventory.php" class = "btn btn-danger pull-right" id = "cancel">Cancel</a>
								<?php 
								} 
								?>
								</td>
								</tr>
							</table>
							</form>
					</div>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- delete item modal -->
<div id="delete-item-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Item</h1>
	<p>Are you sure you want to delete this item?</p>
	<p><a id = "delete-item-yes" class="btn btn-lg btn-danger" href="item.php?action=3&id=<?php print $id; ?>">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // delete customer modal
    $('#delete-item').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-item-modal',
		modal: true
	});

    // dismiss any modal
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});
</script>

</body>
</html>