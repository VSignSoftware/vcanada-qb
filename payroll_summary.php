<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$date1 = $_REQUEST['date1'];
	$date1ts = strtotime($date1);
	$date2 = $_REQUEST['date2'];
	$date2ts = strtotime($date2);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Payroll Summary Report - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

  <!-- Begin: Content -->
  <section id="content" class="table-layout animated fadeIn">

    <!-- begin: .tray-left -->
    <aside class="tray /*tray-left*/ tray250 p30" id = "left_tray">

	    <div id = "menu_2" style = "">
	    	<a class = "glyphicons glyphicons-left_arrow" href = "accounting.php" id = "back" style = "margin-bottom:10px;"></a>
			<br>
			
			<a href = "print_time_cards.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Print Time Cards</a>
	      	<br>

	      	<a href = "enter_time.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Enter Time</a>
	      	<br>

	      	<a href = "payroll_summary.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Payroll Summary</a>
	      	<br>

	      	<a href = "payroll_hourly_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Hourly Report</a>
	      	<br>

	      	<a href = "payroll_labor_sort_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Labor Report</a>
	      	<br>
		</div>

    </aside>
    <!-- end: .tray-left -->

    <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="pl20 pr50">

        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				
	        	<div class="panel-body bg-light">

	        		<table>
	        			<tr>
	        				<td>
	        					<strong>Payroll Summary Report for Dates: </strong>
	        				</td>
	        				<td>&nbsp;</td>
	        				<td>
	        					<input type = "text" class = "dp form-control" style = "width:200px;float:left;" name = "date1" id = "date1" value = "<?php print $date1; ?>"> 
	        				</td>

	        				<td>&nbsp;</td>

	        				<td>
	        					<input type = "text" class = "dp form-control" style = "width:200px;float:left;" name = "date2" id = "date2" value = "<?php print $date2; ?>"> 
	        				</td>
	        				
	        				<td>&nbsp;</td>
	        				<td>
	        					<input type = "submit" id = "go" value = "Run Report" class = "btn btn-sm btn-primary">
	        				</td>
	        			</tr>
	        		</table>

<hr>
<?php
# 
if($action==1)
{
	// depts 100-500
	$d1st = 0;
	$d1ot = 0;
	$d1dt = 0;
	$d1vacation = 0;
	$d1holiday = 0;
	$d1sick = 0;

	// dept 600
	$d2st = 0;
	$d2ot = 0;
	$d2dt = 0;
	$d2vacation = 0;
	$d2holiday = 0;
	$d2sick = 0;

	// total payroll
	$tst = 0;
	$tot = 0;
	$tdt = 0;
	$tvacation = 0;
	$tholiday = 0;
	$tsick = 0;
	?>
	<table class = "table">
		<tr class = "bordered header">
			<td colspan = "2"></td>
			<td><strong>Regular</strong></td>
			<td><strong>Overtime</strong></td>
			<td><strong>DBL Time</strong></td>
			<td><strong>Vac</strong></td>
			<td><strong>Holiday</strong></td>
			<td><strong>Sick</strong></td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">All Departments</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata1 = $vujade->get_payroll_summary_report_data('All Departments',$date1ts,$date2ts);
		//print_r($reportdata1);
		if($reportdata1['error']=="0")
		{
			unset($reportdata1['error']);
			foreach($reportdata1 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d1st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick+=$data['sick'];
			}
		}
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">OFFICERS - DEPT. 100</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata1 = $vujade->get_payroll_summary_report_data('Officers Dept. 100',$date1ts,$date2ts);
		if($reportdata1['error']=="0")
		{
			unset($reportdata1['error']);
			foreach($reportdata1 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d1st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick+=$data['sick'];
			}
		}
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Clerical Dept. 200</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata2= $vujade->get_payroll_summary_report_data('Clerical Dept. 200',$date1ts,$date2ts);
		if($reportdata2['error']=="0")
		{
			unset($reportdata2['error']);
			foreach($reportdata2 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d1st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick+=$data['sick'];
			}
		}
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Design Dept. 300</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata3= $vujade->get_payroll_summary_report_data('Design Dept. 300',$date1ts,$date2ts);
		if($reportdata3['error']=="0")
		{
			unset($reportdata3['error']);
			foreach($reportdata3 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d1st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick+=$data['sick'];
			}
		}
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Sales Dept. 400</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata4= $vujade->get_payroll_summary_report_data('Sales Dept. 400',$date1ts,$date2ts);
		if($reportdata4['error']=="0")
		{
			unset($reportdata4['error']);
			foreach($reportdata4 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d1st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick+=$data['sick'];
			}
		}
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Project Cord. Dept. 500</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata5= $vujade->get_payroll_summary_report_data('Project Cord. Dept. 500',$date1ts,$date2ts);
		if($reportdata5['error']=="0")
		{
			unset($reportdata5['error']);
			foreach($reportdata5 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d1st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick+=$data['sick'];
			}
		}
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Sign Mfg. Dept. 600</td>
			<td colspan = "6">&nbsp;</td>
		</tr>

		<?php
		$reportdata6= $vujade->get_payroll_summary_report_data('Sign Mfg. Dept. 600',$date1ts,$date2ts);
		if($reportdata6['error']=="0")
		{
			unset($reportdata6['error']);
			foreach($reportdata6 as $data)
			{
				?>
				<tr class = "bordered">
					<td><?php print $data['employee_id']; ?></td>
					<td><?php print $data['fullname']; ?></td>
					<td>
						<?php 
						if($data['rate']!="Salary")
						{
							print $data['standard_time'];
							$d2st+=$data['standard_time'];
						}
						else
						{
							print 'Salary';
						}
						?>
					</td>
					<td>
						<?php print $data['over_time']; ?>
					</td>
					<td>
						<?php print $data['double_time']; ?>
					</td>
					<td>
						<?php print $data['vacation']; ?>
					</td>
					<td>
						<?php print $data['holiday']; ?>
					</td>	
					<td>
						<?php print $data['sick']; ?><br>
					</td>
				</tr>
				<?php
				$d2ot += $data['over_time'];
				$d2dt += $data['double_time'];
				$d2vacation += $data['vacation'];
				$d2holiday += $data['holiday'];
				$d2sick+=$data['sick'];
			}
		}

		$tst += $d1st+$d2st;
		$tot += $d1ot+$d2ot;
		$tdt += $d1dt+$d2dt;
		$tvacation += $d1vacation+$d2vacation;
		$tholiday += $d1holiday+$d2holiday;
		$tsick += $d1sick+$d2sick;
		?>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Total Dept 100, 200, 300, 400, 500 &amp; All</td>
			<td>
				<?php print $d1st; ?>
			</td>
			<td>
				<?php print $d1ot; ?>
			</td>
			<td>
				<?php print $d1dt; ?>
			</td>
			<td>
				<?php print $d1vacation; ?>
			</td>
			<td>
				<?php print $d1holiday; ?>
			</td>	
			<td>
				<?php print $d1sick; ?><br>
			</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Total Dept 600</td>
			<td>
				<?php print $d2st; ?>
			</td>
			<td>
				<?php print $d2ot; ?>
			</td>
			<td>
				<?php print $d2dt; ?>
			</td>
			<td>
				<?php print $d2vacation; ?>
			</td>
			<td>
				<?php print $d2holiday; ?>
			</td>	
			<td>
				<?php print $d2sick; ?><br>
			</td>
		</tr>

		<!-- blank row -->
		<tr>
			<td colspan = "8">&nbsp;</td>
		</tr>

		<tr class = "header_graybg">
			<td colspan = "2">Total Payroll Hours</td>
			<td>
				<?php print $tst; ?>
			</td>
			<td>
				<?php print $tot; ?>
			</td>
			<td>
				<?php print $tdt; ?>
			</td>
			<td>
				<?php print $tvacation; ?>
			</td>
			<td>
				<?php print $tholiday; ?>
			</td>	
			<td>
				<?php print $tsick; ?><br>
			</td>
		</tr>

	</table>

<?php
}
?>

<?php if($action==1){ ?>
<div class="">
	<a class="btn btn-primary" target="_blank" href="pdf.php?page=payroll-summery&action=1&date1=<?php echo $date1; ?>&date2=<?php echo $date2; ?>">Print</a>
</div>
<?php } ?>

</div>


			</div>
		</div>

        </div>

  </section>
  <!-- End: Content -->

</section>

</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/jquery.timepicker.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// datepickers
    $(".dp").datepicker();

    $('#go').click(function()
    {
    	var date1 = $('#date1').val();
    	var date2 = $('#date2').val();
    	if(date1=="")
    	{
    		alert('Date 1 must be selected.');
    		return false;
    	}
    	if(date2=="")
    	{
    		alert('Date 2 must be selected.');
    		return false;
    	}
    	// refresh the page
    	var href = "payroll_summary.php?action=1&date1="+date1+"&date2="+date2;
    	window.location.href = href;
    });

});
</script>

</body>
</html>