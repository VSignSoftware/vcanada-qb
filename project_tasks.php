<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);
# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$manager_permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');

$user = $vujade->get_user($_SESSION['user_id']);

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$show_tasks=0; // default (project does not have tasks)
$tasks = $vujade->get_tasks($id);
if($tasks['error']=='0')
{
	unset($tasks['error']);
	$show_tasks=1; // has at least one task
	$taskids = array();
	foreach($tasks as $task)
	{
		$taskids[]=$task['database_id'];
	}
	$firsttask = $vujade->get_task($taskids[0]);
	$taskid = $taskids[0];
}
else
{
	$show_tasks=0;
	$firsttask['error']=1;
}

if(isset($_REQUEST['taskid']))
{
	$firsttask = $vujade->get_task($_REQUEST['taskid']);
	$taskid = $_REQUEST['taskid'];
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# mark complete
if($action==1)
{
	$taskid = $_REQUEST['taskid'];
	$cd = date('m/d/Y h:i:s a');  // 10/27/2013 3:26:08 p.m.
	$s[] = $vujade->update_row('tasks',$taskid,'task_completed',$cd);
	
	$uid = $user['employee_id'];

	// clear any rows in user_tasks where user_id = logged in user and task id = this task
	$sql = "DELETE FROM `user_tasks` WHERE `task_id` = '$taskid' AND `user_id` = '$uid' LIMIT 1";
	$s[]=$vujade->generic_query($sql);

	// create new row
	$vujade->create_row('user_tasks');
	$nid = $vujade->row_id;
	$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
	$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$uid);
	$cts = strtotime($cd);
	$s[]=$vujade->update_row('user_tasks',$nid,'is_complete',1);
	$s[]=$vujade->update_row('user_tasks',$nid,'complete_ts',$cts);
	$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$id);

	$firsttask = $vujade->get_task($taskid);
	$show_tasks=0; // default (project does not have tasks)
	$tasks = $vujade->get_tasks($id);
	if($tasks['error']=='0')
	{
		unset($tasks['error']);
		$show_tasks=1; // has at least one task
		$taskids = array();
		foreach($tasks as $task)
		{
			$taskids[]=$task['database_id'];
		}
	}
}

# delete task
if($action==2)
{
	$taskid = $_REQUEST['taskid'];
	$s[] = $vujade->delete_row('tasks',$taskid);

	// clear any rows in user_tasks where task id = this task
	$sql = "DELETE FROM `user_tasks` WHERE `task_id` = '$taskid'";
	$s[]=$vujade->generic_query($sql);

	$show_tasks=0; // default (project does not have tasks)
	$tasks = $vujade->get_tasks($id);
	if($tasks['error']=='0')
	{
		unset($tasks['error']);
		$show_tasks=1; // has at least one task
		$taskids = array();
		foreach($tasks as $task)
		{
			$taskids[]=$task['database_id'];
		}
		$firsttask = $vujade->get_task($taskids[0]);
		$taskid = $taskids[0];
	}
	else
	{
		$show_tasks=0;
		$firsttask['error']=1;
	}
}

// $taskid must have a value
if(empty($taskid))
{
	// means there are no tasks
	// set to zero so that it doesn't iterfere with javascript
	$taskid = "0";
}

$shop_order = $vujade->get_shop_order($id, 'project_id');
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu=2;
$title = 'Tasks - ' . $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <?php require_once('project_left_tray.php'); ?>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style = "width:100%;">

            <div class="pl15 pr15" style = "width:100%;">

            	<?php require_once('project_right_tray.php'); ?>

            	<!-- main content for this page -->
					<div class = "row">
						<div class = "col-md-4">
							<div class="panel panel-primary panel-border top">
							  <div class="panel-heading">
							    <span class="panel-title">Tasks</span>
							    <div class="widget-menu pull-right">
							    	<?php
									if($projects_permissions['create']==1)
									{
									?>
							      		<a href = "new_task.php?project_id=<?php print $id; ?>" class = "btn btn-primary btn-sm">New</a>
							      	<?php } ?>
							    </div>
							  </div>
							  <div class="panel-body">
								<?php
								if($show_tasks==1)
								{
									print '<table>';
									unset($tasks['error']);
									foreach($tasks as $task)
									{
										if($taskid==$task['database_id'])
										{
											$bgcolor = "ECECEC";
										}
										else
										{
											$bgcolor = "ffffff";
										}
										print '<tr bgcolor = "'.$bgcolor.'">';
										print '<td valign = "top">';
										$n=strtotime('now');
										$tdd = strtotime($task['task_due_date']);
										if($n>$tdd)
										{
											$color = "#f7584c";
										}
										else
										{
											$color="#288BC3";
										}

										// user viewing the page was assigned the task
										// check if user has completed this task
										$c_task = $vujade->get_user_task($task['database_id'],$user['employee_id']);
										//print_r($c_task);
										if($c_task['error']=="0")
										{
											if($c_task['is_complete']==1)
											{
												print '<strike>';
											}

											print '<a class = "" href = "project_tasks.php?id='.$id.'&taskid='.$task['database_id'].'">';
											print '<font color = "'.$color.'">'.$task['task_due_date'].'</font>';
											print '</a>';

											if($c_task['is_complete']==1)
											{
												print '</strike>';
											}

											print '</td>';
											print '<td valign = "top">&nbsp;</td>';
											print '<td valign = "top">';
											if($c_task['is_complete']==1)
											{
												print '<strike>';
											}
											print '<a class = "" href = "project_tasks.php?id='.$id.'&taskid='.$task['database_id'].'">';
											print $task['task_subject'];
											print '</a>';
											if($c_task['is_complete']==1)
											{
												print '</strike>';
											}
											print '</td>';
											print '</tr>';

										}
										else
										{
											// person viewing the page has not been assigned the task
											// determine if all people have completed the task
											$task_completed = $vujade->get_completed_tasks($task['database_id']);

											if($task_completed['is_complete']==1)
											{
												$strike1 = "<strike>";
												$strike2 = "</strike>";
											}

											print $strike1;
											print '<a class = "" href = "project_tasks.php?id='.$id.'&taskid='.$task['database_id'].'">';
											print '<font color = "'.$color.'">'.$task['task_due_date'].'</font>';
											print '</a>';
											print $strike2;
											
											print '</td>';

											print '<td valign = "top">&nbsp;</td>';
											print '<td valign = "top">';
											print $strike1;
											print '<a class = "" href = "project_tasks.php?id='.$id.'&taskid='.$task['database_id'].'">';
											print $task['task_subject'];
											print '</a>';
											print $strike2;
											print '</td>';
											print '</tr>';
										}
									}
									print '</table>';
								}
								?>
							  </div>
							</div>
						</div>

						<div class = "col-md-8" style = "border:0px solid red;">

							<?php
							if($show_tasks==1)
							{
								?>
								<div class="panel panel-primary panel-border top">
							  <div class="panel-heading">
							    <span class="panel-title">Selected Task</span>
							    <div class="widget-menu pull-right">
							    	  
								    <?php
								    $c_task = $vujade->get_user_task($taskid,$user['employee_id']);
									if( ($c_task['error']=="0") && ($c_task['is_complete']=="0") )
									{
									?>
										<a class = "btn btn-success btn-xs" href = "project_tasks.php?id=<?php print $id; ?>&taskid=<?php print $taskid; ?>&action=1" title = "Complete Task">Complete</a>  
									<?php 
									} 
									else
									{
										//print $c_task['error'].' '.$c_task['is_complete'];
									}
									?>

									<a target = "_blank" class = "btn btn-info btn-xs" href = "print_tasks.php?task_id=<?php print $taskid; ?>" title = "Print Task">Print</a>

									<?php

									if($projects_permissions['edit']==1)
									{
										?>
							     		<a href = "edit_task.php?id=<?php print $id; ?>&taskid=<?php print $taskid; ?>" class = "btn btn-primary btn-xs">Edit</a> 
							     	<?php } ?>

									<?php
									if($projects_permissions['delete']==1)
									{
										?>
										<a href="#delete-modal" class = "btn btn-danger btn-xs popup-modal">Delete</a>

									<?php } ?>
							    </div>
							  </div>
							  <div class="panel-body">
								<?php
								if($firsttask['error']=='0')
								{
									// determine if the task is completed and what the completed date is
									$completed_date="";

									// user viewing the page was assigned the task
									// check if user has completed this task
									$c_task = $vujade->get_user_task($firsttask['database_id'],$user['employee_id']);
									//print_r($c_task);
									if($c_task['error']=="0")
									{
										if($c_task['is_complete']==1)
										{
											$completed_date=date('m/d/Y', $c_task['complete_ts']);
										}
									}
									else
									{
										// person viewing the page has not been assigned the task
										// determine if all people have completed the task
										$task_completed = $vujade->get_completed_tasks($firsttask['database_id'],1);

										if($task_completed['is_complete']==1)
										{
											$completed_date=$task_completed['completed_date'];
										}
									}

									//print_r($c_task);
									print '<div style = "padding-left:10px;">';
									print '<p>Subject: <strong>'.$firsttask['task_subject'].'
									</strong></p>';
									print '<p>Due Date: <strong>'.$firsttask['task_due_date'].'
									</strong></p><p>Completed: <strong>'.$completed_date.'</strong></p>';
									print '<p>Created By: <strong>';
									if($firsttask['is_sales_task']==1)
									{
										$cb = $vujade->get_employee($firsttask['created_by'],1);
									}
									else
									{
										$cb = $vujade->get_employee($firsttask['created_by'],2);
									}
									
									print $cb['fullname'];
									print '</strong></p>';
									print '<p>Assigned To: <strong><br>';
									//print $firsttask['employee_ids'];
									@$employee_ids = explode(',',$firsttask['employee_ids']);
									$ceids = count($employee_ids);
									//print $ceids;
									if($ceids>0)
									{
										foreach($employee_ids as $eid)
										{

											// group task
											if($eid[0]=="G")
											{
												$gid = str_replace("G-", "", $eid);
												$g = $vujade->get_groups(false,$gid);

												$geids = @explode(',',$g['employee_ids']);
												$gc = count($geids);
												if($gc>0)
												{
													foreach($geids as $eid)
													{
														$at = $vujade->get_employee($eid,2);
														if($at['error']=='0')
														{
															print $at['fullname'];

															// check if user has completed it and get completed date
															// only for manager permissions users
															if($manager_permissions['read']==1)
															{
																$ct2 = $vujade->get_user_task($firsttask['database_id'],$eid);
																if($ct2['error']=="0")
																{
																	if($ct2['is_complete']==1)
																	{
																		print ' - <font color = "red">Completed on '.date('m/d/Y', $ct2['complete_ts']).'</font>';
																	}
																}
															}
															print '<br>';
														}
													}
												}
											}
											else
											{
												// single employee task
												$at = $vujade->get_employee($eid,2);
												if($at['error']=='0')
												{
													print $at['fullname'];

													// check if user has completed it and get completed date
													// only for manager permissions users
													if($manager_permissions['read']==1)
													{
														$ct2 = $vujade->get_user_task($firsttask['database_id'],$eid);
														if($ct2['error']=="0")
														{
															if($ct2['is_complete']==1)
															{
																print ' - <font color = "red">Completed on '.date('m/d/Y', $ct2['complete_ts']).'</font>';
															}
														}
													}
													print '<br>';
												}
											}
										}
									}
									print '</strong></p>';
									print 'Message: <strong>'.$firsttask['task_message'].'</strong></p>';
									print '</div>';
								}
								?>
							<?php } ?>
						</div>
					</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  	<!-- modal -->	
  	<div id="delete-modal" class="popup-basic p25 mfp-zoomIn mfp-hide">
		<h1>Delete Task</h1>
		<p>Are you sure you want to delete this task?</p>
		<p><a id = "delete-task" class="btn btn-lg btn-danger" href="project_tasks.php?id=<?php print $id; ?>&taskid=<?php print $taskid; ?>&action=2">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
	</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  var taskid = <?php print $taskid; ?>;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    $('.popup-modal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#username',
		modal: true
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    var n = $('#notes').html();
    $("#notes").html($.trim(n));
		
	$('#delete_task').click(function(e)
    {
    	e.preventDefault();
    	$('#dialog').appendTo('body')
		  .html('<div><h6>Are you sure you want to delete this task?</h6></div>')
		  .dialog({
		      modal: true, title: 'Confirm Delete', zIndex: 10000, autoOpen: true,
		      width: 'auto', resizable: false,
		      buttons: {
		          Yes: function () 
		          {
		          	  $(this).dialog("close");
		          	  var id = "<?php print $id; ?>";
		              var taskid = "<?php print $taskid; ?>";
		              window.location.href = "project_tasks.php?action=2&id="+id+"&taskid="+taskid;
		          },
		          No: function () {
		              $(this).dialog("close");
		          }
		      },
		      close: function (event, ui) {
		          //$(this).remove();
		      }
    	});

	});

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
