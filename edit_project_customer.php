<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$id = $_REQUEST['id'];
$onetime = $_REQUEST['onetime'];
	
# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['create']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
  	$vujade->page_redirect('error.php?m=3');
}

// site setup
$setup=$vujade->get_setup();

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}

# update the existing client info and client contact info
if( ($action==1) || ($action==2) )
{

	$client_id=$_POST['customer_id']; // id of the client or the client name
	$company=trim($_POST['company']); // company name

	if(empty($company))
	{
		$vujade->errors[]='Customer name cannot be empty';
	}
	else
	{
		// customer name must be unique if they entered a different name
		$customer=$vujade->get_customer($client_id,'ID');
		if($customer['error']!='0')
		{
			print $client_id.'<br>';
			print __LINE__.'<br>';
			die("Invalid Customer ID.");
		}
		else
		{
			if($customer['name']!=$company)
			{
				$test = $vujade->get_customer_by_name($company,1,1);
				$test['error']=$test['count'];
			}
			else
			{
				$test['error']=0;
			}
		}

		//$vujade->debug_array($test);
		//die;

		//print $client_id;
		//die;

		if($test['error']=="0")
		{
			$address_1=$_POST['b_address_1'];
			$address_2=$_POST['b_address_2'];
			$city=$_POST['b_city'];
			$state=$_POST['b_state'];
			$zip=$_POST['b_zip'];
			$one_time_only = $_POST['one_time_only'];
			
			# update the customer record
			$s = array();
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'Name',$company,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'FullName',$company,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_Addr1',$company,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_Addr2',$address_1,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_Addr3',$address_2,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_City',$city,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_State',$state,'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_PostalCode',$zip,'ID');
			
			// tracking of who entered new customer
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'entered_by',$_SESSION['user_id'],'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'time_entered',strtotime('now'),'ID');
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'page_entered_on',$_SERVER['PHP_SELF'],'ID');

			// customer - project relational key
			$st=$vujade->update_row('projects',$project['database_id'],'client_id',$customer['local_id']);

			//print $st.'<br>';
			//print $client_id.'<br>';
			//die;
			$customer1=$vujade->get_customer($client_id,'ID');
			if($customer1['error']=="0")
			{
				$customer=$customer1;
			}
			$customer2=$vujade->get_customer($client_id,'ListID');
			if($customer2['error']=="0")
			{
				$customer=$customer2;
			}

			// add to quickbooks
			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks customer table
			// only execute if there is an editsequence and
			// there is not already an unsynced add in the queue 
			$is_queued=$vujade->check_for_qb_queue_object($customer['local_id']);
			if( ($customer['edit_sequence']!='') && (!$is_queued) )
			{
				$Queue = new QuickBooks_WebConnector_Queue($dsn);
				$Queue->enqueue(QUICKBOOKS_MOD_CUSTOMER, $customer['local_id']);
			}
			
			$vujade->messages[]="Customer Updated";

			# contact for the existing client
			$customer_contact_id=$_POST['customer_contact_id'];
			
			# add a new one
			if( ($customer_contact_id=='') || ($customer_contact_id==0) )
			{
				$nc_fname=$_POST['nc_fname'];
				$nc_lname=$_POST['nc_lname'];
				$nc_phone=$_POST['nc_phone'];
				$nc_email=$_POST['nc_email'];

				# create a new contact record
				$vujade->create_row('customer_contact');
				$rowid = $vujade->row_id;
				$s[]=$vujade->update_row('customer_contact',$rowid,'customer_id',$customer['local_id']);
				$s[]=$vujade->update_row('customer_contact',$rowid,'first_name',$nc_fname);
				$s[]=$vujade->update_row('customer_contact',$rowid,'last_name',$nc_lname);

				$s[]=$vujade->update_row('customer_contact',$rowid,'phone1',$nc_phone);
				$s[]=$vujade->update_row('customer_contact',$rowid,'email1',$nc_email);

				# set the client contact id 
				$s[]=$vujade->update_row('projects',$id,'ccid',$rowid,'project_id');
			}
			else
			{
				# set the client contact id (not new)
				$s[]=$vujade->update_row('projects',$id,'ccid',$customer_contact_id,'project_id');
			}

			$c1 = array_sum($s);
			if($action==1)
			{
				$vujade->page_redirect('edit_project_scope.php?id='.$id);
			}
			if($action==2)
			{
				$vujade->page_redirect('edit_project.php?id='.$id);
			}
		}
		else
		{
			$vujade->errors[]="Customer name already exists for a different customer.";
		}
	}
}

if($action==3)
{
	$vujade->page_redirect('edit_project.php?id='.$id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=2;
$title = "Select Customer - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                  <p style = "margin-bottom:15px;">
                    <span class="label label-default">Step 1: Project Setup</span> <span class="label label-primary">Step 2: Customer Selection</span> <span class="label label-default">Step 3: Scope of Work</span>
                  </p>

                  <form method="post" action="edit_project_customer.php" id="form">
                  
                    <input type = "hidden" name = "action" id = "action" value = "1">

                    <div class="row">

                      <!-- left side -->
                      <div class="col-md-6">

                        <div class="section">
                            
                            <style>
							  .ui-autocomplete {
							    max-height: 100px;
							    overflow-y: auto;
							    /* prevent horizontal scrollbar */
							    overflow-x: hidden;
							    width:300px;
							    background-color: #777777;
							    color:white;
							  }
							  /* IE 6 doesn't support max-height
							   * we use height instead, but this forces the menu to always be this tall
							   */
							  * html .ui-autocomplete {
							    height: 100px;
							  }
							  .black-border
								{
									border:1px solid black;
								}
								.black-border-short
								{
									border:1px solid black;
									width:150px;
								}
							</style>

							<div class="ui-widget"></div>

							<div class="section">
	                          <label class="field">
	                            <input class="gui-input" type = "text" name = "name_filter" id = "name_filter" value = "" placeholder="Search for a Customer by Name">
	                          </label>
	                        </div>

							<?php
							if($onetime==0)
							{
								?>
								
								<div class="section">
		                          <div class="checkbox-custom mb5">
		                            <input type = "checkbox" name = "one_time_only" id = "one_time_only" value = "1">
		                            <label for="one_time_only">Include one time only customers</label> <a href = "new_customer.php?project_id=<?php print $id; ?>" class = "btn btn-sm btn-primary pull-right">Create New Customer</a>
		                          </div>

		                        </div>

								<?php
							}
							else
							{
								?>

								<div class="section">
		                          <div class="checkbox-custom mb5">
		                            <input type = "checkbox" name = "reset" id = "reset" value = "0" checked = "checked">
		                            <label for="reset">Clear One Time Customers</label> <a href = "new_customer.php?project_id=<?php print $id; ?>" class = "btn btn-sm btn-primary pull-right">Create New Customer</a>
		                          </div> 							
		                          
		                        </div>

								<?php
							}
							?>

<div class="panel">

<div class="panel-body" style = "height:450px;overflow:auto;">
	<div id = "customer_list">
    <table class="table mbn tc-list-1 tc-text-muted-2 tc-fw600-2" style = "">
      <thead>
        <tr class="hidden">
          <th class="w30">&nbsp;</th>
          <th>&nbsp;</th>
        </tr>
      </thead>
      <tbody>

      	<?php
      	# preselect the customer associated with this project
		$client_id = $project['client_id'];

		// test 1: get by list id	
		$current_client = $vujade->get_customer($client_id,'ListID');
		if($current_client['error']=="0")
		{
			$customer=$current_client;
			$id_type=1;
		}

		// test 2: get by ID
		// don't run if listid is temporary id (ie not synced to qb)
		if(ctype_digit($client_id))
		{
			$current_client2 = $vujade->get_customer($client_id,'ID');
			if($current_client2['error']=="0")
			{
				//print 'ID search successful: <br>';
				$customer=$current_client2;
				$id_type=2;
				//print_r($customer);
				//print '<hr>';
			}
		}

		// selected customer is one time only
		//if($customer['is_one_time_only']==1)
		//{
			print '<tr>';
			print '<td>';
			print '<input type = "radio" name = "customer_id" value = "'.$customer['local_id'].'" id = "'.$customer['local_id'].'" class = "cb" checked = "checked">';
			print '</td>';
			print '<td>';
			print '<font color = "red">';	
			print $customer['name'].'<br>';
			print '</font>';	
			print '</td>';
			print '</tr>';
		//}

		$customers = $vujade->get_customers(1,0,10000,$onetime);
		if($customers['error']=='0')
		{
			unset($customers['error']);
			foreach($customers as $c)
			{
				if($c['name']!='')
				{

					if($c['database_id']==$customer['database_id'])
					{
						/*
						print '<tr>';
						print '<td>';
						print '<input type = "radio" name = "customer_id" value = "'.$c['local_id'].'" id = "'.$c['local_id'].'" class = "cb" checked = "checked">';
						print '</td>';
						print '<td>';
						if($c['is_one_time_only']==1)
						{
							print '<font color = "red">';	
						}
						print $c['name'].'<br>';
						if($c['is_one_time_only']==1)
						{
							print '</font>';	
						}
						print '</td>';
						print '</tr>';
						*/
					}
					else
					{
						print '<tr>';
						print '<td>';
						print '<input type = "radio" name = "customer_id" value = "'.$c['local_id'].'" id = "'.$c['local_id'].'" class = "cb">';
						print '</td>';
						print '<td>';
						if($c['is_one_time_only']==1)
						{
							print '<font color = "red">';
						}
						print '<label for="'.$c['local_id'].'">'.$c['name'].'</label>';
						if($c['is_one_time_only']==1)
						{
							print '</font>';	
						}
						print '</td>';
						print '</tr>';
					}
				}
			}
		}
		else
		{

		}
		?>
        
      </tbody>
    </table>
</div>
</div>
</div>
    </div>

  </div>

  <!-- right side -->
  <div class="col-md-6">
    
      	<!-- company contact info -->
		<div class="panel">
              
             <div class="panel-body" style = "height:553px;overflow:auto;">
			
				<div id = "pcd">
					<div class="section">
				      <label class="field">
				        <input class="gui-input" type = "text" name = "company" id = "company" value = "<?php print $customer['name']; ?>" placeholder="Customer Name">
				      </label>
				    </div>

				    <div class="section">
				      <label class="field">
				        <input class="gui-input" type = "text" name = "b_address_1" id = "b_address_1" value = "<?php print $customer['address_1']; ?>" placeholder="Billing Address Line 1">
				      </label>
				    </div>

				    <div class="section">
				      <label class="field">
				        <input class="gui-input" type = "text" name = "b_address_2" id = "b_address_2" value = "<?php print $customer['address_2']; ?>" placeholder="Billing Address Line 2">
				      </label>
				    </div>

				    <div class="section">
				      <label class="field">
				        <input class="gui-input" type = "text" name = "b_city" id = "b_city" value = "<?php print $customer['city']; ?>" placeholder="Billing Address Line 2">
				      </label>
				    </div>

					<div class="section">
				      <label class="field select">
				        <select id="b_state" name="b_state">
				          <?php
				          if(isset($customer['state']))
				          {
				              print '<option value = "'.$customer['state'].'" selected = "selected">'.$customer['state'].'</option>';
				              print '<option value = "">-State-</option>';
				          }
				          else
				          {
				              //print '<option value = "">-State-</option>';
				          }
				          if($setup['country']=="USA")
			              {
                              ?>
                              <option value="AL">Alabama</option>
                              <option value="AK">Alaska</option>
                              <option value="AZ">Arizona</option>
                              <option value="AR">Arkansas</option>
                              <option value="CA">California</option>
                              <option value="CO">Colorado</option>
                              <option value="CT">Connecticut</option>
                              <option value="DE">Delaware</option>
                              <option value="DC">District Of Columbia</option>
                              <option value="FL">Florida</option>
                              <option value="GA">Georgia</option>
                              <option value="HI">Hawaii</option>
                              <option value="ID">Idaho</option>
                              <option value="IL">Illinois</option>
                              <option value="IN">Indiana</option>
                              <option value="IA">Iowa</option>
                              <option value="KS">Kansas</option>
                              <option value="KY">Kentucky</option>
                              <option value="LA">Louisiana</option>
                              <option value="ME">Maine</option>
                              <option value="MD">Maryland</option>
                              <option value="MA">Massachusetts</option>
                              <option value="MI">Michigan</option>
                              <option value="MN">Minnesota</option>
                              <option value="MS">Mississippi</option>
                              <option value="MO">Missouri</option>
                              <option value="MT">Montana</option>
                              <option value="NE">Nebraska</option>
                              <option value="NV">Nevada</option>
                              <option value="NH">New Hampshire</option>
                              <option value="NJ">New Jersey</option>
                              <option value="NM">New Mexico</option>
                              <option value="NY">New York</option>
                              <option value="NC">North Carolina</option>
                              <option value="ND">North Dakota</option>
                              <option value="OH">Ohio</option>
                              <option value="OK">Oklahoma</option>
                              <option value="OR">Oregon</option>
                              <option value="PA">Pennsylvania</option>
                              <option value="RI">Rhode Island</option>
                              <option value="SC">South Carolina</option>
                              <option value="SD">South Dakota</option>
                              <option value="TN">Tennessee</option>
                              <option value="TX">Texas</option>
                              <option value="UT">Utah</option>
                              <option value="VT">Vermont</option>
                              <option value="VA">Virginia</option>
                              <option value="WA">Washington</option>
                              <option value="WV">West Virginia</option>
                              <option value="WI">Wisconsin</option>
                              <option value="WY">Wyoming</option>
                              <option value="">---------------</option>
				              <option value="">-Canadian Provinces-</option>
				              <option value="Alberta">Alberta</option>
				              <option value="British Columbia">British Columbia</option>
				              <option value="Manitoba">Manitoba</option>
				              <option value="New Brunswick">New Brunswick</option>
				              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
				              <option value="Northwest Territories">Northwest Territories</option>
				              <option value="Nova Scotia">Nova Scotia</option>
				              <option value="Nunavut">Nunavut</option>
				              <option value="Ontario">Ontario</option>
				              <option value="Prince Edward Island">Prince Edward Island</option>
				              <option value="Quebec">Quebec</option>
				              <option value="Saskatchewan">Saskatchewan</option>
				              <option value="Yukon">Yukon</option>
				            <?php 
					    	} 
					    	// canadian servers
					    	if($setup['country']=='Canada')
					    	{
					    		?>
					    		  <option value="">-Canadian Provinces-</option>
					              <option value="Alberta">Alberta</option>
					              <option value="British Columbia">British Columbia</option>
					              <option value="Manitoba">Manitoba</option>
					              <option value="New Brunswick">New Brunswick</option>
					              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
					              <option value="Northwest Territories">Northwest Territories</option>
					              <option value="Nova Scotia">Nova Scotia</option>
					              <option value="Nunavut">Nunavut</option>
					              <option value="Ontario">Ontario</option>
					              <option value="Prince Edward Island">Prince Edward Island</option>
					              <option value="Quebec">Quebec</option>
					              <option value="Saskatchewan">Saskatchewan</option>
					              <option value="Yukon">Yukon</option>
					              <option value="">---------------</option>
					              <option value="">-US States-</option>
					              <option value="AL">Alabama</option>
					              <option value="AK">Alaska</option>
					              <option value="AZ">Arizona</option>
					              <option value="AR">Arkansas</option>
					              <option value="CA">California</option>
					              <option value="CO">Colorado</option>
					              <option value="CT">Connecticut</option>
					              <option value="DE">Delaware</option>
					              <option value="DC">District Of Columbia</option>
					              <option value="FL">Florida</option>
					              <option value="GA">Georgia</option>
					              <option value="HI">Hawaii</option>
					              <option value="ID">Idaho</option>
					              <option value="IL">Illinois</option>
					              <option value="IN">Indiana</option>
					              <option value="IA">Iowa</option>
					              <option value="KS">Kansas</option>
					              <option value="KY">Kentucky</option>
					              <option value="LA">Louisiana</option>
					              <option value="ME">Maine</option>
					              <option value="MD">Maryland</option>
					              <option value="MA">Massachusetts</option>
					              <option value="MI">Michigan</option>
					              <option value="MN">Minnesota</option>
					              <option value="MS">Mississippi</option>
					              <option value="MO">Missouri</option>
					              <option value="MT">Montana</option>
					              <option value="NE">Nebraska</option>
					              <option value="NV">Nevada</option>
					              <option value="NH">New Hampshire</option>
					              <option value="NJ">New Jersey</option>
					              <option value="NM">New Mexico</option>
					              <option value="NY">New York</option>
					              <option value="NC">North Carolina</option>
					              <option value="ND">North Dakota</option>
					              <option value="OH">Ohio</option>
					              <option value="OK">Oklahoma</option>
					              <option value="OR">Oregon</option>
					              <option value="PA">Pennsylvania</option>
					              <option value="RI">Rhode Island</option>
					              <option value="SC">South Carolina</option>
					              <option value="SD">South Dakota</option>
					              <option value="TN">Tennessee</option>
					              <option value="TX">Texas</option>
					              <option value="UT">Utah</option>
					              <option value="VT">Vermont</option>
					              <option value="VA">Virginia</option>
					              <option value="WA">Washington</option>
					              <option value="WV">West Virginia</option>
					              <option value="WI">Wisconsin</option>
					              <option value="WY">Wyoming</option>
					    		<?php
					    	}
					    	?>
				        </select>
				        <i class="arrow double"></i>
				        </label>
				    </div>

					<div class="section">
				      <label class="field">
				        <input class="gui-input" type = "text" name = "b_zip" id = "b_zip" value = "<?php print $customer['zip']; ?>" placeholder="Billing Zip / Province" maxlength="10">
				      </label>
				    </div>

					<hr>

					<h3>Contact Person</h3>
					<?php
					//print $customer['customer_id'].'<br>';

					// get by list id
					// don't get if no edit_sequence
					if( (!empty($customer['edit_sequence'])) && ($id_type==1) )
					{
						$contacts = $vujade->get_customer_contacts($customer['customer_id']);
						if($contacts['error']==0)
						{
							unset($contacts['error']);
							foreach($contacts as $c)
							{
								if( ($c['first_name']!='') && ($c['customer_id']!=0) )
								{

									if($c['database_id']==$project['client_contact_id'])
									{
										$selected='checked = "checked"';
									}
									else
									{
										$selected='';
									}
									/*
									if($c['local_id']==$project['client_contact_id'])
									{
										$selected='checked = "checked"';
									}
									*/
									print '<div class="">';
									print '<input type = "radio" name = "customer_contact_id" id = "customer_contact_id" value = "'.$c['database_id'].'" class = "existing_contact"'.' '.$selected.'>';
									print ' <label for="customer_contact_id">'.$c['first_name'].' '.$c['last_name'].'</label>
									</div>';
								}
							}
						}
					}
					
					// get by ID
					// don't get if no edit_sequence
					if( (!empty($customer['edit_sequence'])) && ($id_type==2) )
					{
						$contacts2 = $vujade->get_customer_contacts($customer['local_id']);
						if($contacts2['error']==0)
						{
							unset($contacts2['error']);
							foreach($contacts2 as $c)
							{
								if( ($c['first_name']!='') && ($c['customer_id']!=0) )
								{

									if($c['database_id']==$project['client_contact_id'])
									{
										$selected='checked = "checked"';
									}
									else
									{
										$selected='';
									}

									print '<div class="">';
									print '<input type = "radio" name = "customer_contact_id" id = "customer_contact_id" value = "'.$c['database_id'].'" class = "existing_contact"'.' '.$selected.'>';
									print ' <label for="customer_contact_id">'.$c['first_name'].' '.$c['last_name'].'</label>
									</div>';
								}
							}
						}
					}
					?>

					<div class="">
						<input type = "radio" name = "customer_contact_id" id = "new_contact" value = "0" class = ""> 
						<label for="new_contact">New</label>
					</div>

					<div id = "new_contact_container" style = "display:none;margin-top:5px;">
						
						<div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_fname" id = "nc_fname" value = "" placeholder="First Name">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_lname" id = "nc_lname" value = "" placeholder="Last Name">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_phone" id = "nc_phone" value = "" placeholder="Phone">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_email" id = "nc_email" value = "" placeholder="Email">
					      </label>
					    </div>
					</div>

					<input type = "hidden" name = "client_contact" id = "client_contact" value = "">
				</div>
			</div>
		</div>

  </div>

</div>

<input type = "hidden" name = "client_contact" id = "client_contact" value = "">
<input type = "hidden" name = "client" id = "client" value = "">
<input type = "hidden" name = "id" value = "<?php print $id; ?>">

<div style = "margin-top:15px;clear:both;">
  <div style = "float:left">
    <a href = "#" id = "back" class = "btn btn-lg btn-danger" style = "width:100px;">BACK</a> <a href = "#" id = "next" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND CONTINUE</a>
  </div>
  <div style = "float:right;">
  </div>  
</div>


<div>

<?php
/*
$customers = $vujade->get_customers(1,0,10000,$onetime);
if($customers['error']=='0')
{
	unset($customers['error']);
	foreach($customers as $c)
	{
		if( ($c['name']!='') && (!empty($c['name'])) )
		{
			$cname=str_replace("'", "", $c['name']);
			$cname=str_replace("$", "", $cname);
			print $cname.'<br>';
		}
	}
}
*/
?>

</div>

</form>
</div>
</div>

</div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    $('.cb').click(function()
	{
		var cid = this.id;
		var pid = "<?php print $id; ?>";
		$('#edit_selected_client').show();
		$('#client').val(cid);
		$.post( "project_customer_details.php", { pid: pid, cid: cid })
		  .done(function( data ) 
		  {
		  		$('#pcd').html(data);
		  });
	});

	var customers = [
	<?php
	$customers = $vujade->get_customers(1,0,10000,$onetime);
	if($customers['error']=='0')
	{
		unset($customers['error']);
		foreach($customers as $c)
		{
			if( ($c['name']!='') && (!empty($c['name'])) )
			{
				$cname = addslashes($c['name']);
				print '"'.$cname.'",';
			}
		}
		print '""';
	}
	?>
					    ];

    $("#name_filter").autocomplete({
      source: customers,
      select: function(event, ui) 
      {
      	//var cid = this.id;
      	$('#edit_selected_client').show();
      	var cname = ui.item.value;
      	var pid = "<?php print $id; ?>";
      	$('#client').val(cname);
		$.post( "project_customer_list_filter.php", { pid: pid, cname: cname, v:'2' })
		  .done(function( data ) 
		  {
		  		$('#customer_list').html(data);
		  });
		  $.post( "project_customer_details.php", { pid: pid, cname: cname })
		  .done(function( data ) 
		  {
		  		$('#pcd').html(data);
		  });
        return false;
      }
    });

    $('#back').click(function(e)
	{
		e.preventDefault();
		$('#action').val(2);
		$('#form').submit();
	});

	$('#next').click(function(e)
	{
		e.preventDefault();
		$('#action').val(1);
		$('#form').submit();
	});

	$('#one_time_only').click(function(e)
	{
		e.preventDefault();
		var id = '<?php print $id; ?>';
		var href = 'edit_project_customer.php?id='+id+'&onetime=1';
		window.location.href=href;
	});

	$('#reset').click(function(e)
	{
		e.preventDefault();
		var id = '<?php print $id; ?>';
		var href = 'edit_project_customer.php?id='+id+'&onetime=0';
		window.location.href=href;
	});

	$('#new_contact').change(function(e)
    {
    	e.preventDefault();
    	//alert($(this).is(':checked'));
    	if($(this).is(':checked'))
    	{
    		$('#new_contact_container').show();
    	}
    	else
    	{
    		$('#new_contact_container').hide();
    	}
    	//$('#new_contact').hide();
    	$('#client_contact').val('');
	});

	$('.existing_contact').change(function(e)
    {
    	e.preventDefault();
    	$('#new_contact_container').hide();
	});
 
});
</script>

</body>

</html>