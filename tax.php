<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$charset="ISO-8859-1";
$deleted = 0;
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

if($_REQUEST['new']==1)
{
	$new=1;
}
else
{
	$new=0;
}

# new
if($action==1)
{
	if($accounting_permissions['edit']==1)
	{

		//die('Add new tax is under construction');

		$vujade->create_row('quickbooks_salestaxitem');
		$id = $vujade->row_id;

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$s[] = $vujade->update_row('quickbooks_salestaxitem',$id,'ListID',$fakeid);

		$city=trim($_POST['city']);
		$state=trim($_POST['state']);
		$rate=$_POST['rate'];

		$s[] = $vujade->update_row('quickbooks_salestaxitem',$id,'Name',$city.', '.$state,'ID');
		$s[] = $vujade->update_row('quickbooks_salestaxitem',$id,'TaxRate',$rate*100,'ID');
		//$vujade->messages[]="Tax Updated";

		//$vujade->messages[]='Tax Added. Please complete the form below.';
		$item=$vujade->get_tax_by_id($id);

		// queue up quickbooks add
		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;
		if(function_exists('date_default_timezone_set'))
		{
			date_default_timezone_set('America/Los_Angeles');
		}
		if (!QuickBooks_Utilities::initialized($dsn))
		{	
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_ADD_SALESTAXITEM, $id);
		$vujade->page_redirect('taxes.php?m=1');
	}
	else
	{
		$vujade->messages[]='You do not have permission to add taxes.';
	}
}

# save/edit
if($action==2)
{
	if($accounting_permissions['edit']==1)
	{
		$id = $_REQUEST['id'];
		$city=trim($_POST['city']);
		$state=trim($_POST['state']);
		$rate=$_POST['rate'];

		//die('Update tax is under construction');

		$s[] = $vujade->update_row('quickbooks_salestaxitem',$id,'Name',$city.', '.$state,'ID');
		$s[] = $vujade->update_row('quickbooks_salestaxitem',$id,'TaxRate',$rate*100,'ID');
		$vujade->messages[]="Tax Updated";

		// queue up quickbooks mod
		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;
		if(function_exists('date_default_timezone_set'))
		{
			date_default_timezone_set('America/Los_Angeles');
		}
		if (!QuickBooks_Utilities::initialized($dsn))
		{	
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_MOD_SALESTAXITEM, $id);
	}
	else
	{
		$vujade->messages[]='You do not have permission to update taxes.';
	}
	$vujade->page_redirect('taxes.php?m=1');
}

# delete 
if($action==3)
{
	if($accounting_permissions['delete']==1)
	{
		$id = $_REQUEST['id'];

		// die('Delete tax is under construction');
		$query = "SELECT `ListID` FROM `quickbooks_salestaxitem` WHERE `ID` = " . $id;
		$result = $vujade->generic_query($query);
		
		$arr = mysqli_fetch_assoc($result);
		
		// queue up quickbooks delete
		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;

		// create item on qb server
		if(function_exists('date_default_timezone_set'))
		{
			date_default_timezone_set('America/Los_Angeles');
		}
		
		if (!QuickBooks_Utilities::initialized($dsn))
		{	
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_DEL_LIST, $arr['ListID'], 0, 'ItemSalesTax');
		
		$success = $vujade->delete_row('quickbooks_salestaxitem',$id);
		
		if($success==1)
		{
			$vujade->page_redirect('taxes.php?m=2');
		}
		else
		{
			print 'Could not delete quickbooks_salestaxitem ID col = '.$id.'<br>';
			$vujade->show_errors();
			die;
			//$vujade->print_r_array($success);
		}
	}
}

# default
if( (!isset($id)) && ($new==0) )
{
	$id = $_REQUEST['id'];
	$tax=$vujade->get_tax_by_id($id);
	if($tax['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
}

$setup=$vujade->get_setup();

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = 'City Tax Rate - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">City Tax Rate</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
	        	<div class="panel-body bg-light">

	        		<div style = "display:none;" id = "errors" class = "alert alert-danger">
	        		</div>

	        		<form method = "post" action = "<?php print $_SERVER['PHP_SELF']; ?>" id = "form">
					<table class = "table">
					<tr class = "project_search_results">
					<td class = "project_search_results">
						City
					</td>
					<td class = "project_search_results">
						<input type = "text" name = "city" id = "city" value = "<?php print $tax['city']; ?>">
					</td>
					</tr>

					<tr class = "project_search_results">
					<td class = "project_search_results">
						State
					</td>
					<td class = "project_search_results">
						<select name = "state" id = "state">

							<?php
                              if(isset($tax['state']))
                              {
                                  print '<option value = "'.$tax['state'].'" selected = "selected">'.$tax['state'].'</option>';
                                  print '<option value = "">-State-</option>';
                              }
                              else
                              {
                                  print '<option value = "">-State-</option>';
                              }
                              ?>
                              <option value="AL">Alabama</option>
                              <option value="AK">Alaska</option>
                              <option value="AZ">Arizona</option>
                              <option value="AR">Arkansas</option>
                              <option value="CA">California</option>
                              <option value="CO">Colorado</option>
                              <option value="CT">Connecticut</option>
                              <option value="DE">Delaware</option>
                              <option value="DC">District Of Columbia</option>
                              <option value="FL">Florida</option>
                              <option value="GA">Georgia</option>
                              <option value="HI">Hawaii</option>
                              <option value="ID">Idaho</option>
                              <option value="IL">Illinois</option>
                              <option value="IN">Indiana</option>
                              <option value="IA">Iowa</option>
                              <option value="KS">Kansas</option>
                              <option value="KY">Kentucky</option>
                              <option value="LA">Louisiana</option>
                              <option value="ME">Maine</option>
                              <option value="MD">Maryland</option>
                              <option value="MA">Massachusetts</option>
                              <option value="MI">Michigan</option>
                              <option value="MN">Minnesota</option>
                              <option value="MS">Mississippi</option>
                              <option value="MO">Missouri</option>
                              <option value="MT">Montana</option>
                              <option value="NE">Nebraska</option>
                              <option value="NV">Nevada</option>
                              <option value="NH">New Hampshire</option>
                              <option value="NJ">New Jersey</option>
                              <option value="NM">New Mexico</option>
                              <option value="NY">New York</option>
                              <option value="NC">North Carolina</option>
                              <option value="ND">North Dakota</option>
                              <option value="OH">Ohio</option>
                              <option value="OK">Oklahoma</option>
                              <option value="OR">Oregon</option>
                              <option value="PA">Pennsylvania</option>
                              <option value="RI">Rhode Island</option>
                              <option value="SC">South Carolina</option>
                              <option value="SD">South Dakota</option>
                              <option value="TN">Tennessee</option>
                              <option value="TX">Texas</option>
                              <option value="UT">Utah</option>
                              <option value="VT">Vermont</option>
                              <option value="VA">Virginia</option>
                              <option value="WA">Washington</option>
                              <option value="WV">West Virginia</option>
                              <option value="WI">Wisconsin</option>
                              <option value="WY">Wyoming</option>
                        </select>
						
					</td>
					</tr>

					<tr class = "project_search_results">
					<td class = "project_search_results">
						Rate
					</td>
					<td class = "project_search_results">
						<input type = "text" name = "rate" id = "rate" value = "<?php print $tax['rate']; ?>">
					</td>
					</tr>

					<tr class = "project_search_results">
					<td class = "project_search_results">
					&nbsp;
					</td>
					<td class = "project_search_results">
						<?php
						if($new==1)
						{
							print '<input type = "hidden" name = "action" value = "1">';
							//print '<input type = "hidden" name = "action" value = "2">';
						}
						else
						{
						?>
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "action" value = "2">
						<?php } ?>
					</form>
					</td>
					</tr>
					</table>

        			<div>
	        			<a href = "taxes.php" class = "btn btn-danger">Cancel</a>
	        			
	        			<?php
						if($accounting_permissions['edit']==1)
						{
							?>
							<a href = "#" id = "save" class = "btn btn-primary pull-right" style = "margin-left:5px;margin-right:5px;">Save</a> 
							<?php
						}
						if($accounting_permissions['delete']==1)
						{
						?>

							<a href = "#delete-modal" id = "delete" class = "btn btn-danger pull-right" style = "margin-left:5px;margin-right:5px;">Delete</a>

							<!-- delete modal -->
							<div id = "delete-modal" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "">
								<h1>Delete Tax Rate</h1>
								<strong>Are you sure you want to delete this tax rate?</strong>
								<br>
								<a href = "tax.php?id=<?php print $id; ?>&action=3" id = "" class = "btn btn-success btn-lg">Yes</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
							</div>

							<?php
						}
						?>

	        		</div>

				</div>
			</div>
		</div>
		</section>
	</section>

<!-- js -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // save
    $('#save').click(function(e)
    {
    	e.preventDefault();

    	var city=$('#city').val();
    	var state=$('#state').val();
    	var rate=$('#rate').val();
    	var errors=0;
    	var error_text = '';
    	if(city=='')
    	{
    		errors++;
    		error_text='City is required.<br>';
    	}
    	if(state=='')
    	{
    		errors++;
    		error_text=error_text+'State is required.<br>';
    	}
    	if(rate=='')
    	{
    		errors++;
    		error_text=error_text+'Rate is required.<br>';
    	}
    	if(errors!=0)
    	{
    		$('#errors').html(error_text);
    		$('#errors').show();
    		return false;
    	}
    	else
    	{
    		$('#form').submit();
    	}
    });

	// delete invoice
	$('#delete').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-modal',
		modal: true
	});

	// dismiss modals
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});
</script>

</body>
</html>