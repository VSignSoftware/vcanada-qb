<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['vendor_id'];
$vendor = $vujade->get_vendor($id);
$title = "Vendor Document - ";
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $title; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "vendor.php?id=<?php print $vendor_id; ?>" class = "btn btn-primary btn-sm">&laquo; Back</a>
					</div>
				</div>

	        	<div class="panel-body bg-light">
					<div class = "row">
						<div class = "col-md-12">

							<link rel="stylesheet" href="vendor/plugins/dropzone/css/dropzone.css">	
							<script src="vendor/plugins/dropzone/dropzone.min.js"></script>	

							<div style = "width:100%;margin-bottom:15px;height:375px;">
								<form action="upload.php?type=5&vendor_id=<?php print $id; ?>" class="dropzone dropzone-sm">
								</form>
							</div>

							<a href = "vendor.php?id=<?php print $id; ?>&tab=4" class = "btn btn-primary">DONE</a>
							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>


<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    
});
</script>

</body>
</html>