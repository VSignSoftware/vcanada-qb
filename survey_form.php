<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// 
if(isset($_REQUEST['id']))
{
	$id = $_REQUEST['id'];
	$project = $vujade->get_project($id,2);
	if($project['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}

	// auto populate
	$job_name = $project['site'];
	$job_address1 = $project['address_1'];
	$job_address2 =  $project['address_2'];
	$job_address3 = $project['city'].', '.$project['state'].' '.$project['zip']; 
	$job_number = $project['project_id'];
	$contact_name = $project['site_contact'];
	$contact_number = $project['site_contact_phone'];
}
else
{
	$job_name = '';
	$job_address1 = '';
	$job_address2 = '';
	$job_address3 = '';
	$job_number = '';
	$contact_name = '';
	$contact_number = '';
}

$stylesheet = file_get_contents('css/survey_form_css.css');
$page1 = file_get_contents('survey_form_page_1.html');
$page2 = '<img src="images/survey_form_page_2.png"/>';
$page3 = '<img src="images/survey_form_page_3.png"/>';
// mpdf class (pdf output)
include("mpdf60/mpdf.php");

$mpdf = new mPDF('', 'Letter', 0, 'Verdana', 10, 10, 10, 10, 100);
$mpdf->debug = true;
$mpdf->WriteHTML($stylesheet, 1);    

// create a header now
//$header = '<h1 style="float: left; width: 35%;">SIGN SURVEY FORM</h1>';
$header = '<table class="header-table">';
$header .= '<tr><td><img src = "images/proposal_logo.jpg" style = "width:250px;height:75px;">';
$header.='';
$header .= '</td><td>';
$header .= '<table class="prefill-table">';
$header .= '<tr><td >';
$header .= 'JOB NAME: ';
$header .= $job_name;
$header .= '</td>';
$header .= '<td>';
$header .= 'Job No.: ';
$header .= $job_number;
$header .= '</td></tr>';
$header .= '<tr><td>';
$header .= 'ADDRESS: ';
$header .= $job_address1;
$header .= '</td>';
$header .= '<td >';
$header .= 'Date: ';//check if needs to be populated
$header .= '</td></tr>';
$header .= '<tr><td >';	
$header .= $job_address2;
$header .= '</td>';
$header .= '<td >';
$header .= 'Surveyor: ';//check if needs to be populated
$header .= '</td></tr>';
$header .= '<tr><td >';	
$header .= $job_address3;
$header .= '</td>';
$header .= '<td >';
$header .= 'page:  ____ of ____';//check if needs to be populated	
$header .= '</td></tr>';
/* */
$header .= '<tr><td>';	
if(empty($contact_name))
{
	$header.='Contact: ';
}
else
{
	$header .= $contact_name;
}
$header .= '</td>';
$header .= '<td>';
$header .= $contact_number;
$header .= '</td></tr>';

$header .= '</table>';
$header .= '</td>';
$header .= '</tr></table>';

$mpdf->WriteHTML($header, 2);
//Now put up the remaining page
$mpdf->WriteHTML($page1, 3);
//put header
$mpdf->WriteHTML($header);
$mpdf->WriteHTML($page2);
$mpdf->writeHTML($header);
$mpdf->WriteHTML($page3);   
if($_REQUEST['dt']=="I")
{
	$mpdf->Output('site_survey.pdf', 'I');	
} 
else
{
	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-site-survey.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
	 	$mpdf->Output($pdfname,'F');
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		$mpdf->Output('Site Survey.pdf','I'); 
	}
}
?>
