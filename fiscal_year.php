<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$start = $_REQUEST['start'];
	$end = $_REQUEST['end'];
	$s[] = $vujade->update_row('misc_config',1,'fiscal_year_start',$start);
	$s[] = $vujade->update_row('misc_config',1,'fiscal_year_end',$end);
	$vujade->messages[]="Changes Saved.";
	$action=0;
}

if($action==0)
{
	$setup = $vujade->get_setup();
	$start = $setup['fiscal_year_start'];
	$end = $setup['fiscal_year_end'];
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Fiscal Year Setup - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=11;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<h3>Edit Fiscal Year</h3>
                	<form method = "post" action = "fiscal_year.php">
                	<input type = "hidden" name = "action" value = "1">
                		<table width = "500">
                			<tr>
                				<td>Start
                				</td>
                				<td>&nbsp;</td>
                				<td>End
                				</td>
                				<td>
                					&nbsp;
                				</td>
                				<td>
                					&nbsp;
                				</td>
                			</tr>
                			<tr>
                				<td>
                					<select name="start" id="start" class = "form-control">
                						<?php
                						if(!empty($start))
                						{
                							//$start_month = jdmonthname($start,3);
                							$start_month=$vujade->get_month_name($start);
                							print '<option value="'.$start.'" selected>'.$start_month.'</option>';
                						}
                						?>
                						<option value="">-Select-</option>
									    <option value="1">January</option>
									    <option value="2">February</option>
									    <option value="3">March</option>
									    <option value="4">April</option>
									    <option value="5">May</option>
									    <option value="6">June</option>
									    <option value="7">July</option>
									    <option value="8">August</option>
									    <option value="9">September</option>
									    <option value="10">October</option>
									    <option value="11">November</option>
									    <option value="12">December</option>
									</select>
                				</td>
                				<td>
                					&nbsp;
                				</td>
                				<td>
                					<select name="end" id="end" class = "form-control">
                						<?php
                						if(!empty($end))
                						{
                							$end_month = $vujade->get_month_name($end);
                							print '<option selected value="'.$end.'">'.$end_month.'</option>';
                						}
                						?>
                						<option value="">-Select-</option>
									    <option value="1">January</option>
									    <option value="2">February</option>
									    <option value="3">March</option>
									    <option value="4">April</option>
									    <option value="5">May</option>
									    <option value="6">June</option>
									    <option value="7">July</option>
									    <option value="8">August</option>
									    <option value="9">September</option>
									    <option value="10">October</option>
									    <option value="11">November</option>
									    <option value="12">December</option>
									</select>
                				</td>
                				<td>
                					&nbsp;
                				</td>
                				<td><input type = "submit" value = "SAVE" class = "btn btn-sm btn-primary">
                				</td>
                			</tr>

                		</table>
					</form>

				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
