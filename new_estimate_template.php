<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// full access permissions
$estimates_fa_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Full Access');

$project_id='Template';
$estimates= $vujade->get_estimates_for_project($project_id);
if($estimates['error']!='0')
{
	$nextid=$project_id."-01";
}
else
{
	$nextid=$project_id."-".$estimates['next_id'];
}

$setup = $vujade->get_setup();

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# save the estimate and go to next page
if($action==1)
{

	// full access permissions
	if($estimates_fa_permissions['read']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}

	$name=$_POST['name'];
	$open_date=$_POST['open_date'];
	$status=$_POST['status'];
	$required_date=$_POST['required_date'];
	$description=$_POST['description'];
	$notes=$_POST['notes'];
	$estimated_by=$_POST['estimated_by'];
	$approved_by=$_POST['approved_by'];

	$vujade->create_row('estimates');
	$row_id = $vujade->row_id;
	$s = array();
	$s[] = $vujade->update_row('estimates',$row_id,'estimate_id',$nextid);
	$s[] = $vujade->update_row('estimates',$row_id,'project_id','Template');
	$s[] = $vujade->update_row('estimates',$row_id,'is_template',1);
	$s[] = $vujade->update_row('estimates',$row_id,'template_name',$name);
	$s[] = $vujade->update_row('estimates',$row_id,'date',$open_date);
	$s[] = $vujade->update_row('estimates',$row_id,'created_by',$estimated_by);
	$s[] = $vujade->update_row('estimates',$row_id,'approved_by',$approved_by);
	$s[] = $vujade->update_row('estimates',$row_id,'description',$description);
	$s[] = $vujade->update_row('estimates',$row_id,'notes',$notes);
	$s[] = $vujade->update_row('estimates',$row_id,'required_date',$required_date);
	$s[] = $vujade->update_row('estimates',$row_id,'status',$status);
	$s[] = $vujade->update_row('estimates',$row_id,'ts',strtotime($open_date));
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_buyout_rate',$setup['buyout_overhead']);
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_general_rate',$setup['general_overhead']);
	$s[] = $vujade->update_row('estimates',$row_id,'indeterminant',$setup['indeterminant']);
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_machines_rate',$setup['machine_overhead']);
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_materials_rate',$setup['materials_overhead']);

	// create labor
	$labor_types = $vujade->get_labor_types();
	if($labor_types['error']=="0")
	{
		unset($labor_types['error']);
		foreach($labor_types as $lt)
		{
			$vujade->create_row('estimates_time');
			$id=$vujade->row_id;

			$s[]=$vujade->update_row('estimates_time',$id,'estimate_id',$nextid);
			$s[]=$vujade->update_row('estimates_time',$id,'time_type',$lt['type']);
			$s[]=$vujade->update_row('estimates_time',$id,'labor_id',$lt['database_id']);
			$s[]=$vujade->update_row('estimates_time',$id,'rate',$lt['rate']);
			$s[]=$vujade->update_row('estimates_time',$id,'time_hours',0);
		}
	}
	else
	{
		// this should never be an error
		die('Error: Labor must be defined in site setup.');
	}

	// create machines
	$m_types = $vujade->get_machine_types();
	if($m_types['error']=="0")
	{
		unset($m_types['error']);
		foreach($m_types as $mt)
		{
			$vujade->create_row('estimates_machines');
			$id=$vujade->row_id;

			$s[]=$vujade->update_row('estimates_machines',$id,'estimate_id',$nextid);
			$s[]=$vujade->update_row('estimates_machines',$id,'time_type',$mt['type']);
			$s[]=$vujade->update_row('estimates_machines',$id,'labor_id',$mt['database_id']);
			$s[]=$vujade->update_row('estimates_machines',$id,'rate',$mt['rate']);
			$s[]=$vujade->update_row('estimates_machines',$id,'time_hours',0);
		}
	}
	else
	{
		// do nothing if error; some accounts do not have machines and won't ever define them
		//die('Error: Machine types must be defined in site setup.');
	}

	$vujade->page_redirect('estimate_template_material.php?project_id='.$project_id.'&estimateid='.$row_id);
}

// done button was clicked
if($action==2)
{
	$name=$_POST['name'];
	$open_date=$_POST['open_date'];
	$status=$_POST['status'];
	$required_date=$_POST['required_date'];
	$description=$_POST['description'];
	$notes=$_POST['notes'];
	$estimated_by=$_POST['estimated_by'];
	$approved_by=$_POST['approved_by'];

	$vujade->create_row('estimates');
	$row_id = $vujade->row_id;
	$s = array();
	$s[] = $vujade->update_row('estimates',$row_id,'estimate_id',$nextid);
	$s[] = $vujade->update_row('estimates',$row_id,'project_id',$project_id);
	$s[] = $vujade->update_row('estimates',$row_id,'is_template',1);
	$s[] = $vujade->update_row('estimates',$row_id,'template_name',$name);
	$s[] = $vujade->update_row('estimates',$row_id,'date',$open_date);
	$s[] = $vujade->update_row('estimates',$row_id,'created_by',$estimated_by);
	$s[] = $vujade->update_row('estimates',$row_id,'approved_by',$approved_by);
	$s[] = $vujade->update_row('estimates',$row_id,'description',$description);
	$s[] = $vujade->update_row('estimates',$row_id,'notes',$notes);
	$s[] = $vujade->update_row('estimates',$row_id,'required_date',$required_date);
	$s[] = $vujade->update_row('estimates',$row_id,'status',$status);
	$s[] = $vujade->update_row('estimates',$row_id,'ts',strtotime($open_date));
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_buyout_rate',$setup['buyout_overhead']);
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_general_rate',$setup['general_overhead']);
	$s[] = $vujade->update_row('estimates',$row_id,'indeterminant',$setup['indeterminant']);
	$s[] = $vujade->update_row('estimates',$row_id,'overhead_machines_rate',$setup['machine_overhead']);
	
	// create labor
	$labor_types = $vujade->get_labor_types();
	if($labor_types['error']=="0")
	{
		unset($labor_types['error']);
		foreach($labor_types as $lt)
		{
			$vujade->create_row('estimates_time');
			$id=$vujade->row_id;

			$s[]=$vujade->update_row('estimates_time',$id,'estimate_id',$nextid);
			$s[]=$vujade->update_row('estimates_time',$id,'time_type',$lt['type']);
			$s[]=$vujade->update_row('estimates_time',$id,'labor_id',$lt['database_id']);
			$s[]=$vujade->update_row('estimates_time',$id,'rate',$lt['rate']);
			$s[]=$vujade->update_row('estimates_time',$id,'time_hours',0);
		}
	}
	else
	{
		// this should never be an error
		die('Error: Labor must be defined in site setup.');
	}

	// create machines
	$m_types = $vujade->get_machine_types();
	if($m_types['error']=="0")
	{
		unset($m_types['error']);
		foreach($m_types as $mt)
		{
			$vujade->create_row('estimates_machines');
			$id=$vujade->row_id;

			$s[]=$vujade->update_row('estimates_machines',$id,'estimate_id',$nextid);
			$s[]=$vujade->update_row('estimates_machines',$id,'time_type',$mt['type']);
			$s[]=$vujade->update_row('estimates_machines',$id,'labor_id',$mt['database_id']);
			$s[]=$vujade->update_row('estimates_machines',$id,'rate',$mt['rate']);
			$s[]=$vujade->update_row('estimates_machines',$id,'time_hours',0);
		}
	}
	else
	{
		// see above; allow user to proceed
		// die('Error: Machine types must be defined in site setup.');
	}

	$vujade->page_redirect('estimate_templates.php?m=1');
}

// estimate templates
$templates = $vujade->get_estimates(3);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$section=7;
$menu=4;
$title = "New Estimate Template - ";
require_once('h.php');
?>
<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Estimate <?php print $nextid; ?> Overview</a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

	<div class="admin-form theme-primary">

	    <?php 
	    $vujade->show_errors();
	    $vujade->show_messages();
	    ?>

	    <form method = "post" action = "new_estimate_template.php" id = "nd">
		<input type = "hidden" name = "action" id = "action" value = "">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
		<div class="panel heading-border panel-primary">
            <div class="panel-body bg-light">
		<table width = "100%">

		<tr>
		<td>Project</td>
		<td><?php print $project_id; ?></td>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		</tr>

		<tr>
		<td colspan = "4">&nbsp;</td>
		</tr>

		<tr>
		<td>Name</td>
		<td>
			<input type = "text" name = "name" id = "name" class = "form-control" style = "width:90%;" value = "">
		</td>

		<td>Date Opened
		</td>
		<td><?php print date('m/d/Y'); ?>
			<input type = "hidden" name = "open_date" value = "<?php print date('m/d/Y'); ?>"></td>
		</td>
		</tr>

		<tr>
		<td colspan = "4">&nbsp;
		</td>
		</tr>

		<tr>
		<td colspan = "2">Description
		</td>
		<td colspan = "2">
			<!-- transfer button -->
			<a class = "btn btn-primary" id = "transfer" href = "#transfer-form" style = "float:right;" title = "Transfer from other">Transfer</a>

			<!-- transfer modal -->
			<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
				<input type = "hidden" name ="transfer_to" id = "transfer_to" value = "NEW">
				<form id = "transfer_form" class="form-inline" style = "margin-bottom:15px;">
					<table>
						<tr>
							<td><label>Estimate Number</label></td>
							<td><input type = "text" name = "transfer_from" id = "transfer_from" class = "form-control input-sm" style = "width:230px;margin-left:5px;"></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><label>OR</label></td>
							<td></td>
						</tr>
						<tr>
							<td></td>
							<td></td>
						</tr>
						<tr>
							<td><label>Template</label></td>
							<td><select name = "transfer_from_template" id = "transfer_from_template" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
							<option value = "">-Select-</option>
							<?php
							if($templates['count']>0)
							{
								unset($templates['count']);
								unset($templates['error']);
								unset($templates['sql']);
								foreach($templates as $template)
								{
									print '<option value = "'.$template['estimate_id'].'">'.$template['template_name'].'</option>';
								}
							}
							?>
						</select></td>
						</tr>

						<tr>
							<td></td>
							<td></td>
						</tr>

						<tr>
							<td><label>Include Description?</label></td>
							<td>
								<select name = "copy_description" id = "copy_description" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
									<option value = "">-Select-</option>
									<option value = "1">Yes</option>
									<option value = "">No</option>
								</select>
						</td>
						</tr>

					</table>

					<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
					
					<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please enter an estimate number.</div>
					<div id = "error_2" style = "display:none;" class = "alert alert-danger">This estimate number is invalid.</div>

					<div class = "" style = "width:100%;margin-top:15px;">
						<a id = "transfer_btn" href = "#" class = "btn btn-lg btn-primary" style = "margin-right:15px;">Go</a><a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">Cancel</a>
					</div>
				</form>
			</div>
		</td>
		</tr>

		<tr>
		<td colspan = "4">
		<textarea name = "description" id = "description" class ="ckeditor"></textarea>
			<!-- ckeditor new version 4.5x -->
			<?php require_once('ckeditor.php'); ?>
		</td>
		</tr>

		<tr>
		<td colspan = "4">&nbsp;
		</td>
		</tr>

		<tr>
		<td colspan = "2" style = "padding-right:15px;">Estimated By
		<input type = "text" name = "estimated_by" class = "form-control">
		</td>
		<td colspan = "2" style = "padding-right:15px;">Approved By 
		<input type = "text" name = "approved_by" class = "form-control">
		</td>
		</tr>

		</table>

		<div style = "margin-top:15px;">
          <div style = "">
          	<a class = "btn btn-danger" href = "estimate_templates.php" id = "cancel" style = "">CANCEL</a>
			<div class = "pull-right">
				<div style = "float:left;margin-right:15px;">
				<?php
				if($estimates_fa_permissions['read']==1)
				{
					?>
					<input type = "submit" class = "btn btn-primary" value = "NEXT" style = "" id = "next"> 
				<?php
				}
				?>
				</div>
				<div style = "float:right;margin-left:5px;">
					<a class = "btn btn-primary" style = "" href = "#" id = "done">DONE</a> 
				</div>
			</div>
			</form>
			</div>  
        </div>

		</div>
		</div>
	</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  //var estimateid = <?php print $estimateid; ?>;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

	$("#dp1").datepicker();
	$("#dp2").datepicker();

	$('#next').click(function()
	{
		$('#action').val('1');
		$('#nd').submit();
	});

	$('#done').click(function()
	{
		$('#action').val('2');
		$('#nd').submit();
	});

	// modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	// modal: transfer estimate
    $('#transfer').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,		
	});
	
    // transfer button in the modal 
    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();
	
    	$('#working').hide();
    	$('#error_1').hide();
    	$('#error_2').hide();

    	var transfer_to = $('#transfer_to').val();
		var transfer_from = $('#transfer_from').val();
		var transfer_from_template = $('#transfer_from_template').val();
		var copy_description = $('#copy_description').val();
		var id = "<?php print $id; ?>";
		//var old_description = $('#description').val();

		var old_description = CKEDITOR.instances['description'].getData();

		var design = $('#design').val();
		var open_date = $('#open_date').val();
		var status = $('#status').val();
		var dp2 = $('#dp2').val();
		var estimated_by = $('#estimated_by').val();
		var approved_by = $('#approved_by').val();

		var error = 0;
		if(transfer_from=="")
		{	
			error++;
		}
		if(transfer_from_template=="")
		{	
			error++;
		}
		if(error>1)
		{
			$('#error_1').show();
			return false;
		}
		else
		{
			if(transfer_from=='')
			{
				transfer_from=transfer_from_template;
				var template = 1;
			}
			$('#error_1').hide();
			$('#working').show();
			$('#working').html('Working...');
			$.post( "jq.transfer_estimate.php", { transfer_from: transfer_from, transfer_to: transfer_to, project_id: "Template", alt_redirect: 2, copy_description: copy_description, old_description: old_description, design: design, open_date: open_date, status: status, date_required: dp2, estimated_by: estimated_by, approved_by: approved_by, template: 1 })

			.done(function( response ) 
			{
				$('#working').show();
			    $('#working').html('');
			    $('#working').html(response);
			    if(response!="Invalid estimate.")
			    {
			    	$('#working').html('Success! This page will reload in 3 seconds...');
			    	var href = response;
			    	setTimeout(function(){ window.location.href=href; }, 3000);
			    }
			    else
			    {
			    	$('#error_2').html(response);
			    	$('#error_2').show();
					$('#working').hide();
			    }
			});
		}
	});

	// press return on estimate transfer form
    $('#transfer_form').on('keypress', function(e) 
    {
		if (e.which == 13) 
		{
			//$('#form').submit();
			$('#transfer_btn').click();
			return false;
		}
	});
	
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
