<?php

// fix for fiscal year that stretches over two years
$current_year = date('Y');
$current_month=date('n');
/*
bootz example:
m = 9
m2 = 8
dashboard will only show between 9-2015 and 8-2016;
problematic because if month is 10-2016, db won't show correct fiscal year
*/
$m=$setup['fiscal_year_start'];
$m2=$setup['fiscal_year_end'];

// fiscal month end is before the current month
if($m2<$current_month)
{
	$y=$current_year;
	$y2 = $current_year+1;
}
else
{
	$y=$current_year-1;
	$y2 = $current_year;
}

$start = strtotime($y.'-'.$m.'-01');
$date1 = $y2.'-'.$m2; 
$d = date_create_from_format('Y-m',$date1); 
$last_day = date_format($d, 't');
$end = strtotime($y2.'-'.$m2.'-'.$last_day);

//print $current_month.'<br>'; // 10
//print $y.' '.$y2.'<br>'; // 2015, 2016
//print $start.' '.$end;
//die;
/*
12
2016 2017
1480579200 1512028800
*/
/*
old; has a bug where current fiscal year is not being shown
$y=$year-1;
$y2 = $year;
$m=$setup['fiscal_year_start'];
$m2=$setup['fiscal_year_end'];
$start = strtotime($y.'-'.$m.'-01');
$date1 = $y2.'-'.$m2; 
$d = date_create_from_format('Y-m',$date1); 
$last_day = @date_format($d, 't');
$end = strtotime($y2.'-'.$m2.'-'.$last_day);
*/

$sales_start = microtime(true);

$projects = $vujade->get_individual_yearly_sales_report($start,$end,$sp);
unset($projects['error']);

$sales_end = microtime(true);

$js_data_points = '';
while($start < $end)
{
	$lmonth = date('m',$start);
	$lyear = date('Y',$start);
	$lstart = mktime(0, 0, 1, $lmonth, 1, $lyear);
	$lend   = mktime(23, 59, 00, $lmonth, date('t', $lmonth), $lyear);
    $line_start = $start;
    $line_end = $lend;

    //print $line_start.' - '.$line_end.'<br>';

	foreach($projects as $p)
	{
		$date_opened = strtotime($p['open_date']);
		if(($date_opened>=$line_start) && ($date_opened<=$line_end))
		{
			//print_r($p);
			//print '<hr>';
			$line = str_replace(',','',$p['line_total']);
			$line_total+=$line;
			$total+=$line;
		}
	}

	// javascript fix; line total can't be = ""
	if(empty($line_total))
	{
		$line_total=0;
	}
	$js_data_points.='{
         x: new Date('.$lyear.','.($lmonth-1).', 1),
         y: '.$line_total.', click: function(e){ window.location.href="report_individual_sales.php?sp='.$emp['fullname'].'"}
       },';

    $start = strtotime("+1 month", $start);
    $line_total=0;
}

//die;

// strip last comma from js data points
rtrim($js_data_points, ",");
?>

<!-- chart -->
<script type="text/javascript">
window.onload = function () 
{
	// pie chart colors
	CanvasJS.addColorSet("pie_chart_colors",
    [
    	"#0DAEE8",
		"#12FF2A",
		"#FFA60A"
    ]);

	// pie chart

	var chart = new CanvasJS.Chart("donut",
	{
		title:{
			text: "My Projects"
		},
		subtitles:[
		{
			text: "Your closed rate is <?php print @number_format($closed_rate,1); ?>%",
			verticalAlign: "bottom",
			fontSize:25
		}
		],
        animationEnabled: true,
		legend: {
			verticalAlign: "bottom",
			horizontalAlign: "center"
		},
		colorSet:  "pie_chart_colors",
		data: [
		{        
			type: "pie",
			indexLabelFontFamily: "Garamond",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			toolTipContent: "{name}: {y}",
			showInLegend: true,
			indexLabel: "{y}", 
			dataPoints: [
				{  y: <?php print $pending_jobs; ?>, name: "Pending Projects", legendMarkerType: "square"},
				{  y: <?php print $processed_jobs; ?>, name: "Processed Jobs", legendMarkerType: "square"}
				,
				{  y: <?php print $did_not_sell_jobs; ?>, name: "Did Not Sell", legendMarkerType: "square"}
			]
		}
		]
	});
	chart.render();
	
	// monthly sales
    var chart2 = new CanvasJS.Chart("chart",
    {
      title:{
        text: "Individual Yearly Sales for <?php print $sp.' - '.$year; ?>"
      },
      axisX:{
        interval: 1,
        intervalType: "month"
      },
      data: [
      {
        type: "line",
        dataPoints: [//array
        <?php print $js_data_points; ?>
       ]
     }
     ]
    });

    chart2.render();
	
    // bar chart colors

	CanvasJS.addColorSet("bar_chart_colors",
    [
    	"#18FF13",
		"#E8E106",
		"#FF5F04",
		"#FF090A"
    ]);

    // bar chart
    var chart3 = new CanvasJS.Chart("bar_chart",
	{
		animationEnabled: true,
		colorSet:  "bar_chart_colors",
		title:{
			text: "A/R Report"
		},
		data: [
		{
			type: "column", 
			dataPoints: [
				{ y: <?php print $gt1; ?>, label: "Current" },
				{ y: <?php print $gt2; ?>, label: "31 to 45 Days" },
				{ y: <?php print $gt3; ?>, label: "46 to 60 Days" },
				{ y: <?php print $gt4; ?>, label: "Over 60 Days" }
			]
		}
		]
	});

	chart3.render();

} 
</script>
<script type="text/javascript" src="vendor/plugins/canvas/canvasjs.min.js"></script>

<div id = "chart" name = "chart" style = "height:200px;margin-top:10px;">
</div>