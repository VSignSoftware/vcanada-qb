<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();

if( (isset($_POST['action'])) && ($_POST['action']==1) )
{
	$s=array();
	$label1=$_POST['label1'];
	$sp1_decimal=$_POST['sp1_decimal'];
	$l1_decimal=$_POST['l1_decimal'];
	$p1_decimal=$_POST['p1_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_1',$label1);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_1',$sp1_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_1',$l1_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_1',$p1_decimal);

	$label2=$_POST['label2'];
	$sp2_decimal=$_POST['sp2_decimal'];
	$l2_decimal=$_POST['l2_decimal'];
	$p2_decimal=$_POST['p2_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_2',$label2);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_2',$sp2_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_2',$l2_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_2',$p2_decimal);
	
	$label3=$_POST['label3'];
	$sp3_decimal=$_POST['sp3_decimal'];
	$l3_decimal=$_POST['l3_decimal'];
	$p3_decimal=$_POST['p3_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_3',$label3);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_3',$sp3_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_3',$l3_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_3',$p3_decimal);

	$label4=$_POST['label4'];
	$sp4_decimal=$_POST['sp4_decimal'];
	$l4_decimal=$_POST['l4_decimal'];
	$p4_decimal=$_POST['p4_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_4',$label4);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_4',$sp4_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_4',$l4_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_4',$p4_decimal);
	
	$label5=$_POST['label5'];
	$sp5_decimal=$_POST['sp5_decimal'];
	$l5_decimal=$_POST['l5_decimal'];
	$p5_decimal=$_POST['p5_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_5',$label5);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_5',$sp5_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_5',$l5_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_5',$p5_decimal);
	
	$label6=$_POST['label6'];
	$sp6_decimal=$_POST['sp6_decimal'];
	$l6_decimal=$_POST['l6_decimal'];
	$p6_decimal=$_POST['p6_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_6',$label6);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_6',$sp6_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_6',$l6_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_6',$p6_decimal);
	
	$label7=$_POST['label7'];
	$sp7_decimal=$_POST['sp7_decimal'];
	$l7_decimal=$_POST['l7_decimal'];
	$p7_decimal=$_POST['p7_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_7',$label7);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_7',$sp7_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_7',$l7_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_7',$p7_decimal);
	
	$label8=$_POST['label8'];
	$sp8_decimal=$_POST['sp8_decimal'];
	$l8_decimal=$_POST['l8_decimal'];
	$p8_decimal=$_POST['p8_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_8',$label8);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_8',$sp8_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_8',$l8_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_8',$p8_decimal);
	
	$label9=$_POST['label9'];
	$sp9_decimal=$_POST['sp9_decimal'];
	$l9_decimal=$_POST['l9_decimal'];
	$p9_decimal=$_POST['p9_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_9',$label9);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_9',$sp9_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_9',$l9_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_9',$p9_decimal);
	
	$label10=$_POST['label10'];
	$sp10_decimal=$_POST['sp10_decimal'];
	$l10_decimal=$_POST['l10_decimal'];
	$p10_decimal=$_POST['p10_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_10',$label10);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_10',$sp10_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_10',$l10_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_10',$p10_decimal);

	$label11=$_POST['label11'];
	$sp11_decimal=$_POST['sp11_decimal'];
	$l11_decimal=$_POST['l11_decimal'];
	$p11_decimal=$_POST['p11_decimal'];
	$s[]=$vujade->update_row('invoice_setup',1,'label_11',$label11);
	$s[]=$vujade->update_row('invoice_setup',1,'sale_price_decimal_11',$sp11_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'labor_decimal_11',$l11_decimal);
	$s[]=$vujade->update_row('invoice_setup',1,'parts_decimal_11',$p11_decimal);

	$s[]=$vujade->update_row('invoice_setup',1,'is_taxable_5',$_POST['is_taxable_5']);
	$s[]=$vujade->update_row('invoice_setup',1,'is_taxable_6',$_POST['is_taxable_6']);
	$s[]=$vujade->update_row('invoice_setup',1,'is_taxable_7',$_POST['is_taxable_7']);
	$s[]=$vujade->update_row('invoice_setup',1,'is_taxable_8',$_POST['is_taxable_8']);
	$s[]=$vujade->update_row('invoice_setup',1,'is_taxable_9',$_POST['is_taxable_9']);
	$s[]=$vujade->update_row('invoice_setup',1,'is_taxable_10',$_POST['is_taxable_10']);

}

$invoice_setup=$vujade->get_invoice_setup(1);
if($invoice_setup['error']!="0")
{
	// row doesn't exist; create new
	//$vujade->create_row('invoice_setup');
	$q = "insert into invoice_setup (id) values ('1')";
	$s = $vujade->generic_query($q);
	$invoice_setup_id = 1;
	$invoice_setup=$vujade->get_invoice_setup($invoice_setup_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Invoice Setup - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">
        	
        	<?php
        	$ss_menu=6;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

		<div class="pl20 pr50">

			<?php
				$vujade->show_errors();
				$vujade->show_messages();
			?>

			<div class="panel panel-primary panel-border top">
				<div class="panel-body bg-light">

               	<!-- content goes here -->
				<form method = "post" action = "invoice_setup.php">
					<input type = "hidden" name = "action" value = "1">
					<table width = "100%" class = "table">
				
					<tr>
						<td valign = "top">
							Drop Down
						</td>
						<td valign = "top">
							Tax Percentage
						</td>
						<td valign = "top">
							Default Tax
						</td>
					</tr>

					<tr>
						<td valign = "top">
							<input type = "text" name = "label1" value = "<?php print $invoice_setup['label_1']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp1_decimal" value = "<?php print $invoice_setup['sale_price_1']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							N/A
						</td>
					</tr>

					<tr>
						<td valign = "top">
							<input type = "text" name = "label2" value = "<?php print $invoice_setup['label_2']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp2_decimal" value = "<?php print $invoice_setup['sale_price_2']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							N/A
						</td>
					</tr>

					<tr>
						<td valign = "top">
							<input type = "text" name = "label3" value = "<?php print $invoice_setup['label_3']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp3_decimal" value = "<?php print $invoice_setup['sale_price_3']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							N/A
						</td>
					</tr>

					<?php
					if( ($setup['country']=="Canada") && ($setup['is_qb']==1) )
					{
					?>

					<!-- -->
					<tr>
						<td valign = "top">
							<input type = "text" name = "label4" value = "<?php print $invoice_setup['label_4']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp4_decimal" value = "<?php print $invoice_setup['sale_price_4']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_4" id = "is_taxable_4" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_4']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_4']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>
						
					<?php } ?>	

					<tr>
						<td valign = "top">
							Engineering <input type = "hidden" name = "label5" value = "Engineering" class = "">

						</td>
						<td valign = "top">
							<input type = "text" name = "sp5_decimal" value = "<?php print $invoice_setup['sale_price_5']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_5" id = "is_taxable_5" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_5']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_5']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td valign = "top">
							Permits <input type = "hidden" name = "label6" value = "Permits" class = "">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp6_decimal" value = "<?php print $invoice_setup['sale_price_6']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_6" id = "is_taxable_6" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_6']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_6']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td valign = "top">
							Permit Acquisition <input type = "hidden" name = "label7" value = "Permit Acquisition" class = "">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp7_decimal" value = "<?php print $invoice_setup['sale_price_7']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_7" id = "is_taxable_7" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_7']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_7']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td valign = "top">
							Shipping <input type = "hidden" name = "label8" value = "Shipping" class = "">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp8_decimal" value = "<?php print $invoice_setup['sale_price_8']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_8" id = "is_taxable_8" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_8']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_8']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td valign = "top">
							Credit Card Processing <input type = "hidden" name = "label9" value = "Credit Card Processing" class = "">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp9_decimal" value = "<?php print $invoice_setup['sale_price_9']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_9" id = "is_taxable_9" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_9']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_9']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>

					<tr>
						<td valign = "top">
							Discount <input type = "hidden" name = "label10" value = "Discount" class = "">
						</td>
						<td valign = "top">
							<input type = "text" name = "sp10_decimal" value = "<?php print $invoice_setup['sale_price_10']; ?>" class = "form-control">
						</td>
						<td valign = "top">
							<select name = "is_taxable_10" id = "is_taxable_10" class = "form-control">
								<?php
								if($invoice_setup['is_taxable_10']==1)
								{
									print '<option value = "1" selected">Taxable</option>';
								}
								if($invoice_setup['is_taxable_10']==2)
								{
									print '<option value = "2" selected">Non-Taxable</option>';
								}
								print '<option value = "">-Select-</option>';
								print '<option value = "1">Taxable</option>';
								print '<option value = "2">Non-Taxable</option>';
								?>
							</select>
						</td>
					</tr>

					</table>

					<br>
					<input type = "submit" value = "Save" class = "btn btn-primary">
				</form>
				</div>
			</div>
		</div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

 <!-- BEGIN: PAGE SCRIPTS -->

 <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
   	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
