<?php 
session_start();
//error_reporting(E_ALL);
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$show_ms=0;
if(isset($_REQUEST['show_ms']))
{
    $show_ms=$_REQUEST['show_ms']; 
}
# disables event drag, drop, resize
$editable = 2;
$charset = "utf-8";
$title = "Calendar";
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="<?php print $charset; ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php print $title; ?></title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/normalize.css">

<link href='vendor/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<!--<link href='css/fullcalendar.print.css' rel='stylesheet' media='print' />-->
<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- calendar -->
<script src='vendor/plugins/fullcalendar/lib/moment.min.js'></script>
<script src='vendor/plugins/fullcalendar/fullcalendar.min.js'></script>
</head>
<body>
<script>
$(document).ready(function() 
{	
	$('#calendar').fullCalendar({
        //ignoreTimezone:false,
        //timezone: "America/Chicago",
		allDaySlot: true,
		slotEventOverlap: true,
		minTime: "06:00:00",
		maxTime: "20:00:00",
		scrollTime: "08:00:00",
		header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
		editable: false,
		events: "calendar_events_print.php?show_ms="+"<?php print $show_ms; ?>",
		eventDrop: function(event,dayDelta,minuteDelta,allDay) 
		{
			return false;
    	},
    	eventResize: function(event,dayDelta,minuteDelta,allDay) 
		{
            return false;
    	},
    	viewRender: function(view, element) 
    	{ 
    		//var view = $('#calendar').fullCalendar('getView');
           	// $('#current_view_data').val('');
            //$('#current_view_data').val(view.name+" "+view.start+" "+view.end);   
    	},
        eventRender: function (event, element) 
        {
            // this allows the event title to contain custom html such as a colored bottom border
            element.find('span.fc-title').html(element.find('span.fc-title').text());
        }
	});

});
</script>
<style>
	body 
	{
		margin: 0;
		padding: 0;
		font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
		font-size: 14px;
        -webkit-print-color-adjust:exact;
	}

	#calendar 
	{
		width: 900px;
		margin: 40px auto;
	}

	.fc-grid, .fc-day, .fc-day-header
    {
        border:1px solid #3C3C3D;
    }

    .fc-event
    {
        border-width: 2px;
    }

    a.cp:link {text-decoration: none;}
    a.cp:visited {text-decoration: none;}
    a.cp:hover {text-decoration: none;}
    .cp, .fc-event-title
    {
        margin-bottom:0px;
        padding-left: 0px;
        padding-top: 0px;
        padding-right: 0px;
        padding-bottom:1px;
    }

</style>

<div id="content">
	<br style="clear: both;"/>

    <!-- legend and money schedule button -->
    <div style = "width:950px;text-align:center;margin-left:auto;margin-right:auto;">
        <div style = "margin-left:30px;float:left;width:950px;margin-top:15px;display:block;">
            <?php
            $data = $vujade->get_legend_data();
            if($data['error']=="0")
            {
                unset($data['error']);
                //print '<ul>';
                foreach($data as $legend)
                {
                    print '<div style = "border:1px solid #eeeeee; width:30px;height:15px;background-color:'.$legend['hex'].';display:block;float:left">&nbsp;</div><div style = "float:left;margin-left:5px;margin-right:10px;">= '.$legend['explanation'].'</div>';
                }
                //print '</ul>';
            }
            ?>
        </div>
    </div>

	<!-- the calendar -->
	<div id="calendar"></div>

</div>

<div class = "push">
</div>

</body>
</html>