<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// user must be admin to access this page
if(!isset($_SESSION['admin_id']))
{
	$vujade->page_redirect('logout.php');
}

require_once('../library/sshinfo.php');

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{

	$location=$_REQUEST['location'];
	if($location==1)
	{
		$dir='/var/www/uploads/';
	}
	if($location==2)
	{
		$dir='/var/library/';
	}

	$dir='/var/www/uploads/';

	$fn = basename($_FILES['file']['name']);
	$f = $dir . basename($_FILES['file']['name']);

	if(move_uploaded_file($_FILES['file']['tmp_name'], $f)) 
	{
	    $success=1;
	} 
	else 
	{
	    //echo "Possible file upload attack!\n";
	    print_r($_FILES);
	    die;
	}

	// file was uploaded
	if($success==1)
	{
		set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclibrary');
		include('Net/SSH2.php');
		$ssh = new Net_SSH2('localhost'); 
		if(!$ssh->login($u, $p)) 
		{ 
		    exit('Login Failed');
		}

		// move the file to the correct location
		if($location==1)
		{
			// move the new file to the old location
			$ssh->exec('mv /var/www/uploads/'.$fn.' /var/www/'.$fn);

			// delete the old file
			$ssh->exec('rm '.$f);
		}
		if($location==2)
		{
			// move the new file to the old location
			$ssh->exec('mv /var/www/uploads/'.$fn.' /var/library/'.$fn);

			// delete the old file
			$ssh->exec('rm '.$f);
		}
	}

	//print_r($_FILES);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Administrator Settings - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<h3>File Manager</h3>

					<form enctype="multipart/form-data" action="file_manager.php" method="POST">
					    Upload File: <input name="file" type="file" id = "file" />
					    <br>
					    Location: 
					    <select name = "location" id = "location">
					    	<option value = "1">web folder</option>
					    	<option value = "2">library folder</option>
						</select>
						<br>
						<input type = "hidden" name = "action" value = "1">
					    <input type="submit" value="Upload" />
					</form>

					<p style = "width:800px;height:100px;"></p>

				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
