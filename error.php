<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=0;
$title = "Error - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $title; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
			if(!isset($_GET['m']))
			{
				$vujade->errors[]="This page could not be loaded properly. Please contact the system administrator.";
			}
			else
			{
				# no access
				if($_GET['m']==1)
				{
					$vujade->errors[]="You do not have access to that page.";
				}

				# user manipulated the url in an incorrect manner
				if($_GET['m']==2)
				{
					$vujade->errors[]="The page could not be loaded properly. Please do not alter the address bar when accessing pages on this site.";
				}

				# database id is missing
				if($_GET['m']==3)
				{
					$vujade->errors[]="The page could not be loaded properly. It appears that there was no match to the data you requested.";
				}

				# could not add a record to the database
				if($_GET['m']==4)
				{
					$vujade->errors[]="The information could not be added to the system. Please contact the website administrator.";
				}

				# trying to alter an admin account
				if($_GET['m']==5)
				{
					$vujade->errors[]="Administrator account cannot be modified. Please contact the website administrator for further assistance.";
				}

				# test project; do not delete
				if($_GET['m']==6)
				{
					$vujade->errors[]="This project cannot be deleted. It is used for testing and development.";
				}

				# blank customer name
				if($_GET['m']==7)
				{
					$vujade->errors[]="There is an invalid customer in the database. Please contact technical support.";
				}

				# blank customer name
				if($_GET['m']==8)
				{
					$vujade->errors[]="QuickBooks Account database error. Please contact technical support.";
				}

				# bad credit memo id
				if($_GET['m']==9)
				{
					$vujade->errors[]="Invalid credit memo.";
				}

			}
			$vujade->show_errors(); 
			?>
	
		</div>
	</section>
</section>


<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();
    
});
</script>

</body>
</html>