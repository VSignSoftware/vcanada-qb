<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

if(isset($_REQUEST['id']))
{
	$id = $_REQUEST['id'];
	// validate id

	if($id!='NEW')
	{
		if(empty($id))
		{
			die('Invalid group.');
		}
		if(!ctype_digit($id))
		{
			die('Invalid group.');
		}
		$group = $vujade->get_groups(false,$id);
		if($group['count']!=1)
		{
			die('Invalid group.');
		}
		$checked = explode(',',$group['employee_ids']);
	}
}
else
{
	// new group
	$group['name']="Create new group";
	$id = "NEW";
}

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}
if($action==1)
{
	// new or edit group
	if($id=="NEW")
	{
		// new
		$vujade->create_row('group_tasks');
		$id = $vujade->row_id;
		$vujade->messages[]='Group Added';
	}
	else
	{
		$vujade->messages[]='Group Updated';
	}

	$vujade->update_row('group_tasks',$id,'name',$_POST['name']);
	$vujade->update_row('group_tasks',$id,'employee_ids',implode(',',$_POST['employee']));
	$group = $vujade->get_groups(false,$id);
	$checked = $_POST['employee'];
}

$employees = $vujade->get_employees(5);
if($employees['error']==0)
{
	unset($employees['error']);
}
else
{
	die('You cannot use this page until you have entered at least one employee.');
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = $group['name'] . " - ";
if($group['name']=="Create new group")
{
	$group['name']='';
}
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=11;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<div class="panel panel-primary panel-border top">
	                <div class="panel-body bg-light">

	                	<p><a href = "groups.php" class = "btn btn-primary btn-xs">Previous</a></p>

						<?php
						$vujade->show_messages();
						$vujade->show_errors();
						?>

						<form method = "post" action = "group.php" id = "form">
							<input type = "hidden" name = "action" value = "1">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">

							<b>Group Name</b>
							<br>
							<input type = "text" name = "name" id = "name" class = "form-control" value = "<?php print $group['name']; ?>">
							<br>

							<b>Employees</b>
							<br>
							<?php
							foreach($employees as $edata)
							{
								$is_checked='';
								if(in_array($edata['employee_id'],$checked))
								{
									$is_checked='checked';
								}
								print '<input type = "checkbox" name = "employee[]" id = "" value = "'.$edata['employee_id'].'" class = "" '.$is_checked.'>'.$edata['fullname'].'<br>';
								$is_checked='';
							}
							?>
							<a href = "groups.php" id = "cancel" class = "btn btn-danger">Cancel</a> 
							<a href = "#" id = "save" class = "btn btn-success">Save</a> 
						</form>
					</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    });

    // save button
    $('#save').click(function(e)
    {
    	e.preventDefault();
    	$('#form').submit();
    });
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
