<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
# permissions
$po_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($po_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$poid = $_REQUEST['poid'];
$po = $vujade->get_purchase_order($poid);
if($po['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

// tax list id
$tax_list_id = $vujade->get_qb_list_id('Sales Tax');

// available accounts
$available_accounts=$vujade->get_qb_accounts('Purchase Orders');
if($available_accounts['error']=="0")
{
	$show_accounts=1;
}
else
{
	$show_accounts=0;
}
unset($available_accounts['error']);

// vendor 
$vendor = $vujade->get_vendor($po['vendor_id'],'ListID');

$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# save a new item
if($action==1)
{
	# get item details
	$item_id = $_REQUEST['item_id'];
	$item = $vujade->get_item($item_id);
	if($item['error']=="0")
	{

		$rate = $item['cost'];
		$desc = $item['description']; 

		$desc = $vujade->clean($desc);

		$list_id = $item['list_id'];

		$project_id=$_REQUEST['project'];

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$fakeid2 = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_purchaseorder_lineitem` (`Parent_ID`,`TxnLineID`,`ItemRef_ListID`,`Rate`,`Desc`,`Quantity`,`ReceivedQuantity`,`Amount`,`account`,`project_id`,`Other1`,`IsNew`) VALUES ('$poid','$fakeid','$list_id','$rate','$desc','1','0','$rate','$account','$project_id','$fakeid2','true')";
		$vujade->generic_query($sql,$debug); 
		
		$notes = $_REQUEST['notes'];
		$tax = $_REQUEST['tax']; // percentage
		$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax,'ID');
		$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes,'ID');
	}
}

# update an item 
if($action==2)
{
	$db_id=$_REQUEST['item_id'];
	$qty = $_REQUEST['qty'];
	$qty_recvd = $_REQUEST['qty_recvd'];
	$account = $_REQUEST['account'];
	$unit_price = $_REQUEST['unit_price'];
	$line_total=$unit_price*$qty;

	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'Quantity',$qty,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'ReceivedQuantity',$qty_recvd,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'account',$account,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'Rate',$unit_price,'TxnLineID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'Amount',$line_total,'TxnLineID');

	$project_id=$_REQUEST['project_id'];
	$s[] = $vujade->update_row('quickbooks_purchaseorder_lineitem',$db_id,'project_id',$project_id,'TxnLineID');

	$notes = $_REQUEST['notes'];
	$tax = $_REQUEST['tax']; // percentage
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax,'ID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes,'ID');
}

# delete the item 
if($action==3)
{
	$db_id=$_REQUEST['item_id'];
	$s[] = $vujade->delete_row('quickbooks_purchaseorder_lineitem',$db_id,0,'TxnLineID');
	$notes = $_REQUEST['notes'];
	$tax = $_REQUEST['tax']; // percentage
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax,'ID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes,'ID');
}

# update notes, tax and total
if($action==4)
{
	$notes = $_REQUEST['notes'];
	$subtotal = $_REQUEST['subtotal'];
	$tax = $_REQUEST['tax']; // percentage
	$tax_amount = $tax*$subtotal;
	$st2 = $subtotal+$tax_amount;
	$total = $st2;
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'subtotal',$subtotal);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax_amount',$tax_amount);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'TotalAmount',$total);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes);

	# database id of the duplicate copy in the costing purchase orders table
	$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);

	# update the amount
	$s[] = $vujade->update_row('costing_purchase_orders',$cpo['database_id'],'cost',$total);
}

# done button was pressed
if($action == 5)
{
	# also update notes, tax, deposit and total
	$notes = $_REQUEST['notes'];
	$subtotal = $_REQUEST['subtotal'];
	$tax = $_REQUEST['tax']; // percentage
	$tax_amount = $tax*$subtotal;
	$st2 = $subtotal+$tax_amount;
	$total = $st2;
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'subtotal',$subtotal);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax_amount',$tax_amount);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'TotalAmount',$total);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes);

	# database id of the duplicate copy in the costing purchase orders table
	$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);

	# update the amount
	$s[] = $vujade->update_row('costing_purchase_orders',$cpo['database_id'],'cost',$total);

	// can't queue qb unless this purchase order has line items
	// if edit sequence ! null, queue mod, else queue new add
	$has_items = $vujade->get_materials_for_purchase_order($poid);
	if($has_items['error']=="0")
	{

		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;

		// qb add
		if(empty($po['edit_sequence']))
		{

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_ADD_PURCHASEORDER, $poid);
		}
		else
		{
			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_MOD_PURCHASEORDER, $poid);
		}
	}
}

# previous button was pressed
if($action == 6)
{
	# also update notes, tax, deposit and total
	$notes = $_REQUEST['notes'];
	$subtotal = $_REQUEST['subtotal'];
	$tax = $_REQUEST['tax']; // percentage
	$tax_amount = $tax*$subtotal;
	$st2 = $subtotal+$tax_amount;
	$total = $st2;
	
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'subtotal',$subtotal);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax_amount',$tax_amount);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'TotalAmount',$total);
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes);

	# database id of the duplicate copy in the costing purchase orders table
	$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);

	# update the amount
	$s[] = $vujade->update_row('costing_purchase_orders',$cpo['database_id'],'cost',$total);
}

# add misc item
if($action==7)
{
	$desc=$_POST['misc_item'];
	$desc = $vujade->clean($desc);
	$qty=$_POST['misc_item_qty'];
	$qty = $vujade->clean($qty);
	$rate=$_POST['misc_item_up'];
	$rate = $vujade->clean($rate);
	$tax=$_POST['tax'];
	$tax = $vujade->clean($tax);

	# add item to misc inventory
	$vujade->create_row('misc_materials');
	$row_id = $vujade->row_id;
	$s[]=$vujade->update_row('misc_materials',$row_id,'inventory_id','MISC');
	$s[]=$vujade->update_row('misc_materials',$row_id,'description',$misc_item);
	$s[]=$vujade->update_row('misc_materials',$row_id,'unit_price',$misc_item_up);

	$nmlist_id = $vujade->get_qb_list_id('Non-Inventory Purchase','quickbooks_noninventoryitem');
	$list_id = $nmlist_id['list_id'];

	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$fakeid2 = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_purchaseorder_lineitem` (`Parent_ID`,`TxnLineID`,`ItemRef_ListID`,`Rate`,`Desc`,`Quantity`,`ReceivedQuantity`,`Amount`,`account`,`project_id`,`Other1`,`IsNew`) VALUES ('$poid','$fakeid','$list_id','$rate','$desc','$qty','0','$rate','$account','$id','$fakeid2','true')";
	$vujade->generic_query($sql,$debug); 
		
	$notes = $_REQUEST['notes'];
	$tax = $_REQUEST['tax']; // percentage
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'tax',$tax,'ID');
	$s[] = $vujade->update_row('quickbooks_purchaseorder',$poid,'Memo',$notes,'ID');
}

$po = $vujade->get_purchase_order($poid);

// materials cost
$materials_cost = 0;
$items = $vujade->get_materials_for_purchase_order($poid);
if($items['error']=="0")
{
	unset($items['error']);
	foreach($items as $i)
	{
		if($i['inventory_id']!="MISC")
		{
			# total
			$line = $i['unit_price']*$i['qty'];
			$materials_cost = $materials_cost + $line;
		}
		else
		{
			$materials_cost = $materials_cost + ($i['unit_price']*$i['qty']);
		}
	}
	$items['error']=0;
}

// update tax
if($action>0)
{
	// new tax
	if($action!=7)
	{
		$tax = $_REQUEST['tax']; // percentage
	}
	$tax_amount=$tax*$materials_cost;
	$tax_amount=round($tax_amount,2);

	$tax_account = $_REQUEST['tax_account'];

	// clear out any existing tax line items 
	$dq = "DELETE FROM `quickbooks_purchaseorder_lineitem` WHERE `Parent_ID` = '$poid' AND `Desc` = 'Sales Tax'";
	$s[]=$vujade->generic_query($dq,0,2);

	// create a new line item for the tax
	$list_id_2=$tax_list_id['list_id'];
	$fakeid3 = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$fakeid4 = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_purchaseorder_lineitem` 
	(
		`Parent_ID`,
		`TxnLineID`,
		`ItemRef_ListID`,
		`Rate`,
		`Desc`,
		`Quantity`,
		`ReceivedQuantity`,
		`Amount`,
		`account`,
		`project_id`,
		`Other1`,
		`IsNew`
		) 
		VALUES 
		(
			'$poid',
			'$fakeid3',
			'$list_id_2',
			'1.00',
			'Sales Tax',
			'1',
			'0',
			'$tax_amount',
			'$tax_account',
			'$id',
			'$fakeid4',
			'true'
			)";
	$s[]=$vujade->generic_query($sql,0,2); 
}
else
{
	$tax_data = $vujade->get_sales_tax_for_po($poid);
	if($tax_data['error']=="0")
	{
		if( ($tax_amount!=0) && ($materials_cost!=0) )
		{
			$tax_amount=$tax_data['total'];
			$tax=$tax_amount/$materials_cost;
			$tax=round($tax,2);
		}
		else
		{
			$tax=0;
			$tax_amount=0;
		}
	}
	else
	{
		$tax=0;
		$tax_amount=0;
	}
}

if($action==5)
{
	$vid = $vendor['database_id'];
	$vujade->page_redirect('vendor.php?id='.$vid.'&tab=5');
}

if($action==6)
{
	$vujade->page_redirect('edit_blank_purchase_order.php?poid='.$poid);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 0;
$section = 0;
$title = "Edit Purchase Order Materials - ";
$charset='<meta charset="ISO-8859-1">';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Materials - <?php print $po['purchase_order_id']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">

		<!-- vendor and ship to -->
		<div class = "row">
			<div class = "col-md-6">
				<div class="panel panel-primary">
				  <div class="panel-heading">
				    <span class="panel-title">To: </span>
				    <div class="widget-menu pull-right">
				    </div>
				  </div>
				  <div class="panel-body" style = "height:200px;">
				    <?php
					if($vendor['error']=="0")
					{
						print '<strong>';
						print $vendor['name'];
						print '</strong>';
						print '<br>';
						print $vendor['address_1'];
						if(!empty($vendor['address_2']))
						{
							print ', ' . $vendor['address_1'];
						}
						print '<br>';
						print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];
						print '<br>';
						print $vendor['phone'];
						?>
						<br>
						<br>
						<strong>Vendor Contact: </strong><br>
						<?php
						$vendor_contact = $vujade->get_vendor_contact($po['vendor_contact_id']);
						if($vendor_contact['error']=="0")
						{
							print $vendor_contact['fullname'].'<br>';
							print $vendor_contact['phone1'];
						}
					}
					else
					{
						//print $vendor['error'];
					}
					?>
				  </div>
				</div>
			</div>

			<div class = "col-md-6">
				<div class="panel panel-primary">
				  <div class="panel-heading">
				    <span class="panel-title">Ship To: </span>
				    <div class="widget-menu pull-right">
				    </div>
				  </div>
				  <div class="panel-body" style = "height:200px;">
				    <?php
				    print $po['company'].'<br>';
					print $po['address_1'];
					if(!empty($po['address_2']))
					{
						print ', '.$po['address_2'];
					}
					print '<br>';
					print $po['city'].', '.$po['state'].' '.$po['zip'];
					?>
				  </div>
				</div>
			</div>
		</div>

		<!-- po data -->
		<div class = "well">
			<table width = "100%">
				<tr>
					<td><strong>PO No.:</strong>
					</td>
					<td><?php print $po['purchase_order_id']; ?>
					</td>
					<td><strong>Date:</strong>
					</td>
					<td><?php print $po['date']; ?>
					</td>
					<td><strong>Revised: </strong>
					</td>
					<td><?php print $po['date_revised']; ?>
					</td>
				</tr>
				<tr>
					<td colspan = "6">&nbsp;
					</td>
				</tr>
				<tr>
					<td><strong>Written By: </strong>
					</td>
					<td><?php print $po['ordered_by']; ?>
					</td>
					<td><strong>Type: </strong> 
					</td>
					<td>Materials
					</td>
					<td>
					</td>
					<td>
					</td>
				</tr>
			</table>
		</div>

		<!-- materials in this po -->
		<div class = "row">
			<div class = "col-md-12">
				<div style = "width:100%;min-height:100px;max-height:450px;overflow:auto;clear:both;" id = "materials_container">
					<table class = "table">
					<tr>
						<td valign = "top" class = "bordered size100">Item #</td>
						<td valign = "top" class = "bordered size200">Description</td>
						<td valign = "top" class = "bordered size50">Account</td>
						<td valign = "top" class = "bordered size50">Job No.</td>
						<td valign = "top" class = "bordered size50">Qty</td>
						<td valign = "top" class = "bordered size50">Unit Price</td>
						<td valign = "top" class = "bordered size50">Total</td>
						<td valign = "top" class = "bordered size100">Job Name</td>
						<td valign = "top" class = "bordered size50">Qty Rcvd</td>
						<td valign = "top" class = "bordered size100">&nbsp;</td>
					</tr>
					<?php
					$materials_cost = 0;
					if($items['error']=="0")
					{
						unset($items['error']);
						foreach($items as $i)
						{
							if($i['inventory_id']!="MISC")
							{
								print '<tr>';

								$fitem = $vujade->get_item($i['inventory_id'],'ListID');

								print '<td class = "bordered size100">';
								if($fitem['error']=="0")
								{
									print $fitem['inventory_id'];
								}
								else
								{
									print $i['inventory_id'];
								}
								print '</td>';
								
								print '<td class = "bordered size200">';
								print $i['description'];
								print '</td>';
								
								// account
								print '<td valign = "top" class = "bordered size100">';
								print '<select class = "account" name = "account" id = "account'.$i['database_id'].'">';
								if(!empty($i['account']))
								{
									print '<option value = "'.$i['account'].'" selected = "selected">'.$i['account'].'</option>';
								}
								print '<option value = "">-Select-</option>';
								if($show_accounts==1)
								{
									foreach($available_accounts as $available_acc)
									{
										print '<option value = "'.$available_acc['label'].'">'.$available_acc['label'].'</option>';
									}
								}
								print '</select>';
								print '</td>';

								# job number
								print '<td class = "bordered size100">';
								print '<input type="text" name = "project" id = "project'.$i['database_id'].'" value = "'.$i['project_id'].'">';
								print '</td>';

								# qty
								print '<td class = "bordered size100">';
								print '<input style = "width:30px;" type = "text" name = "qty" id = "qty'.$i['database_id'].'" value = "'.$i['qty'].'">';
								print '</td>';

								# unit price
								print '<td class = "bordered size100">';
								print '$<input type = "text" name = "unit_price" style = "width:50px;" id = "unit_price'.$i['database_id'].'" value = "'.@number_format($i['unit_price'],2,'.',',').'">';
								print '</td>';
								
								# total
								print '<td class = "bordered size100">';
								$line = $i['unit_price']*$i['qty'];
								print '$'. number_format($line,2,'.',',');
								print '</td>';

								# job name
								print '<td class = "bordered size100">';
								print $project['site'];
								print '</td>';

								# qty recvd
								print '<td class = "bordered size100">';
								print '<input style = "width:30px;" type = "text" name = "qty_recvd" id = "qty_recvd'.$i['database_id'].'" value = "'.$i['qty_recvd'].'">';
								print '</td>';

								print '<td class = "bordered size100">';
								
								print '<a href = "#" id = "'.$i['database_id'].'" class = "plus-update btn btn-xs btn-success">Update</a> ';
								print '<a href = "#" id = "'.$i['database_id'].'" class = "plus-delete btn btn-xs btn-danger">Delete</a>';
								print '</td>';
								print '</tr>';

								$materials_cost = $materials_cost + $line;
							}
							else
							{
								print '<tr>';

								print '<td class = "bordered size100">';
								print $i['inventory_id'];
								print '</td>';
								
								print '<td class = "bordered size200">';
								print $i['description'];
								print '</td>';
								
								// account
								print '<td valign = "top" class = "bordered size100">';
								print '<select class = "account" name = "account" id = "account'.$i['database_id'].'">';
								if(!empty($i['account']))
								{
									print '<option value = "'.$i['account'].'" selected = "selected">'.$i['account'].'</option>';
								}
								print '<option value = "">-Select-</option>';
								if($show_accounts==1)
								{
									foreach($available_accounts as $available_acc)
									{
										print '<option value = "'.$available_acc['label'].'">'.$available_acc['label'].'</option>';
									}
								}
								print '</select>';
								print '</td>';

								# job number
								print '<td class = "bordered size100">';
								print '<input type="text" name = "project" id = "project'.$i['database_id'].'" value = "'.$i['project_id'].'">';
								print '</td>';

								# qty
								print '<td class = "bordered size100">';
								print '<input style = "width:30px;" type = "text" name = "qty" id = "qty'.$i['database_id'].'" value = "'.$i['qty'].'">';
								print '</td>';

								# unit price
								print '<td class = "bordered size100">';
								print '$<input type = "text" name = "unit_price" style = "width:50px;" id = "unit_price'.$i['database_id'].'" value = "'.@number_format($i['unit_price'],2,'.',',').'">';
								print '</td>';
								
								# total
								print '<td class = "bordered size100">';
								print '$'.@number_format($i['unit_price']*$i['qty'],2,'.',',');
								print '</td>';

								$materials_cost = $materials_cost + ($i['unit_price']*$i['qty']);

								# job name
								print '<td class = "bordered size100">';
								print $project['site'];
								print '</td>';

								# qty recvd
								print '<td class = "bordered size100">';
								print '<input style = "width:30px;" type = "text" name = "qty_recvd" id = "qty_recvd'.$i['database_id'].'" value = "'.$i['qty_recvd'].'">';
								print '</td>';

								print '<td class = "bordered size100">';

								print '<a href = "#" id = "'.$i['database_id'].'" class = "plus-update btn btn-xs btn-success">Update</a> ';
								print '<a href = "#" id = "'.$i['database_id'].'" class = "plus-delete btn btn-xs btn-danger">Delete</a>';
								print '</td>';
								print '</tr>';
							}
						}
					}
					else
					{
						print '<tr>
						<td colspan = "10" class = "bordered size100" style = "width:100%;text-align:center;"><em>Use the form below to add materials to this purchase order.</em></td></tr>';	
					}
					?>
					</table>
				</div>
			</div>
		</div>
		
		<!-- searchable list of materials -->
		<div class = "row">
			<div class = "col-md-12">
				<form id = "inventory_form_1">
					<table class = "table">
						<tr>
							<td colspan = "5">
								<div class = "row">
									<div style = "float:left;margin-right:5px;width:200px;">
										<input type = "text" name = "filter" id = "filter" class = "form-control input-sm" style = "width:200px;"> 
									</div>

									<div style = "float:left;margin-right:5px;width:200px;">
										<select name = "filter_field" id = "filter_field" class = "form-control" style = "width:200px;">
											<option value = "5">ALL</option>
											<option value = "1">ID</option>
											<option value = "2">Description</option>
											<option value = "3">Size</option>
											<option value = "4">Cost</option>
										</select>  
									</div>

									<div style = "float:left;margin-right:5px;width:200px;">
										<input type = "button" value = "Reset" id = "filter_reset" class = "btn btn-primary btn-sm">
									</div>
								</div>
							</td>
						</tr>
						<tr>
							<td colspan = "5" id = "inventory_table">
								<div style = "overflow:auto;width:100%;height:200px;">
									<?php
									$materials = $vujade->get_inventory($sort=1,$start=1,$no_limit=1);
									if($materials['error']=='0')
									{
										unset($materials['error']);
										print '<table id = "itable" class = "table">';
										print '<thead>';
										print '<tr>';
										print '<th>ID';
										print '</th>';
										print '<th>Description';
										print '</th>';
										print '<th>Size';
										print '</th>';
										print '<th>Cost';
										print '</th>';
										print '<th>';
										print '&nbsp;';
										print '</th>';
										print '</tr>';
										print '</thead>';
										print '<tbody>';
										foreach($materials as $m)
										{
											if(empty($m['inventory_id']))
											{
												$m['inventory_id']=$m['database_id'];
											}
											print '<tr>';
											print '<td>';
											print $m['inventory_id'];
											print '</td>';
											print '<td>';
											print $m['description'];
											print '</td>';
											print '<td>';
											print $m['size'];
											print '</td>';
											print '<td>';
											$c = trim($m['cost']);
											@$c = number_format($c,2,'.',',');
											print '$'.$c;
											print '</td>';
											print '<td>';
											print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-xs btn-primary">+</a>';
											print '</td>';
											print '</tr>';
										}
										print '</tbody></table>';
									}
									?>
								</div>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>

		<!-- misc items -->
		<div class = "row" style = "margin-top:15px;">
			<div class = "col-md-12">
				<form method = "post" action = "edit_blank_purchase_order_materials.php">
					<input type = "hidden" name = "action" value = "7">
					<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
					<input type = "hidden" name = "poid" value = "<?php print $poid; ?>">
					<strong>Miscellaneous Item</strong><br>
					<div class = "row">
						<div class = "col-md-9">
							<input type = "text" name = "misc_item" id = "misc_item" placeholder = "Non-Stock Item" value = "" class = "form-control input-sm">
						</div>

						<div class = "col-md-1">
							<input type = "text" name = "misc_item_qty" id = "misc_item_qty" value = "" class = "form-control input-sm" placeholder = "QTY">
						</div>	

						<div class = "col-md-1">
							<input type = "text" name = "misc_item_up" id = "misc_item_up" value = "" class = "form-control input-sm" placeholder = "Price">
						</div>

						<div class = "col-md-1">
							<input type = "submit" value = "Add" class = "btn btn-success btn-sm">
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<!-- notes, subtotal, tax -->
		<form method = "post" action = "edit_blank_purchase_order_materials.php" id = "done">
			<table width = "100%" style = "margin-top:15px;">
				<tr>
					<td width = "75%" valign = "top">
						<strong>Notes</strong><br>
						<textarea name = "notes" id = "notes" class = "form-control" style = "height:118px;">
							<?php print $po['notes']; ?>
						</textarea>
					</td>
					<td valign = "top">
						<div style = "margin-left:10px;border:1px solid #cecece;padding:5px;height:200px;">
							<table width = "100%">
								<tr>
									<td>Subtotal:</td>
									<td>
										<input type = "text" name = "st" value = "<?php print @number_format($materials_cost,2); ?>" style = "width:100px;" readonly = "readonly">
										<input type = "hidden" name = "subtotal" id = "subtotal" value = "<?php print $materials_cost; ?>">
									</td>
								</tr>
								<tr>

									<?php
									$tax_amount=$materials_cost*$po['tax'];
									?>

									<td>Tax Rate:</td>
									<td><input type = "text" name = "tax" value = "<?php print $po['tax']; ?>" id = "tax" style = "width:100px;"></td>
								</tr>
								<tr>
									<td>Tax Amount:</td>
									<td><input type = "text" name = "tax_amount" value = "<?php print @number_format($tax_amount,2); ?>" id = "tax_amount" readonly = "readonly" style = "width:100px;"></td>
								</tr>
								<tr>
									<td>
										Account:
									</td>
									<td>
										<select name = "tax_account" id = "tax_account">
											<?php
											if(!empty($tax_data['account']))
											{
												print '<option value = "'.$tax_data['account'].'" selected = "selected">'.$tax_data['account'].'</option>';
											}
											else
											{
												if(!empty($_REQUEST['tax_account']))
												{
													print '<option value = "'.$_REQUEST['tax_account'].'" selected = "selected">'.$_REQUEST['tax_account'].'</option>';
												}
											}
											print '<option value = "">-Select-</option>';
											if($show_accounts==1)
											{
												foreach($available_accounts as $available_acc)
												{
													print '<option value = "'.$available_acc['label'].'">'.$available_acc['label'].'</option>';
												}
											}
											?>
										</select> 
									</td>
								</tr>
								<tr>
									<td>Total: </td>
									<?php $total = $materials_cost+$tax_amount; ?>
									<td>$<?php print @number_format($total,2,'.',','); ?></td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><a id = "ud" href = "#" class = "plus-photo btn btn-sm btn-success" style = "">Update</a></td>
								</tr>
							</table>
						</div>
					</td>	
				</tr>
			</table>

			<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
			<input type = "hidden" name = "poid" value = "<?php print $poid; ?>">
			<input type = "hidden" name = "action" id = "action" value = "5">

			<!-- previous and done buttons -->
			<div style = "margin-top:15px;">
				<input type = "submit" value = "PREVIOUS" id = "previous" class = "btn btn-primary">

				<input type = "submit" value = "SAVE AND COMPLETE" id = "sbt" class = "btn btn-success">
			</div>

		</form>
		
    </div>
</div>

</section>
</section>

<!-- End: Main -->
<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{
	"use strict";

	// Init Theme Core    
	Core.init();

	function isNumber(n) 
	{
	  	return !isNaN(parseFloat(n)) && isFinite(n);
	}

	// update button in the line items
	$('.plus-update').click(function(e)
	{
		e.preventDefault();
		var poid = "<?php print $poid; ?>";
		var item_id = this.id;
		var notes = $('#notes').val();
		var tax = $('#tax').val();
		var deposit = "";
		var st = $('#subtotal').val();
		var project_id = $('#project'+item_id).val();
		var account = $('#account'+item_id).val();
		var qty = $('#qty'+item_id).val();
		var qty_recvd = $('#qty_recvd'+item_id).val();
		var unit_price = $('#unit_price'+item_id).val();

		// account must have a selected value
		if(account=='')
		{
			alert('Please select an account.');
			$('#account'+item_id).css('border','1px solid red');
			return false;
		}

		// validate tax as decimal
		$('#tax').css('background-color','');
		if(tax=='')
		{

		}
		else
		{
			var isvalid = isNumber(tax);
			if(isvalid==false)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is invalid. Please enter a decimal value less than the whole number 1. Do not enter any other characters in this field.');
				return false;
			} 

			if(tax>0.99)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is not a valid percentage.Please enter a decimal value less than the whole number 1.');
				return false;
			}
		}
		var url = "edit_blank_purchase_order_materials.php?action=2&project_id="+project_id+"&poid="+poid+"&item_id="+item_id+"&notes="+notes+"&tax="+tax+"&deposit="+deposit+"&subtotal="+st+"&account="+account+"&qty="+qty+"&qty_recvd="+qty_recvd+"&unit_price="+unit_price;

		//alert(url);

		window.location.href=url;
	});

	// delete button in the line items
	$('.plus-delete').click(function(e)
	{
		e.preventDefault();
		var poid = "<?php print $poid; ?>";
		var item_id = this.id;
		var notes = $('#notes').val();
		var tax = $('#tax').val();
		var deposit = "";
		var st = $('#subtotal').val();
		var project_id = "<?php print $id; ?>";
		var account = $('#account'+item_id).val();
		var qty = $('#qty'+item_id).val();
		var qty_recvd = $('#qty_recvd'+item_id).val();
		// validate tax as decimal
		$('#tax').css('background-color','');
		if(tax=='')
		{

		}
		else
		{
			var isvalid = isNumber(tax);
			if(isvalid==false)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is invalid. Please enter a decimal value less than the whole number 1. Do not enter any other characters in this field.');
				return false;
			} 

			if(tax>0.99)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is not a valid percentage.Please enter a decimal value less than the whole number 1.');
				return false;
			}
		}
		var url = "edit_blank_purchase_order_materials.php?action=3&project_id="+project_id+"&poid="+poid+"&item_id="+item_id+"&notes="+notes+"&tax="+tax+"&deposit="+deposit+"&subtotal="+st+"&account="+account+"&qty="+qty+"&qty_recvd="+qty_recvd;

		//alert(url);

		window.location.href=url;
	});

	// pressing any plus button on the available inventory table
	$('.plus').click(function(e)
	{
		e.preventDefault();
		var poid = "<?php print $poid; ?>";
		var item_id = this.id;
		var notes = $('#notes').val();
		var tax = $('#tax').val();
		var deposit = "";
		var st = $('#subtotal').val();
		var project_id = "<?php print $id; ?>";
		// validate tax as decimal
		$('#tax').css('background-color','');
		if(tax=='')
		{

		}
		else
		{
			var isvalid = isNumber(tax);
			if(isvalid==false)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is invalid. Please enter a decimal value less than the whole number 1. Do not enter any other characters in this field.');
				return false;
			} 

			if(tax>0.99)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is not a valid percentage.Please enter a decimal value less than the whole number 1.');
				return false;
			}
		}
		var url = "edit_blank_purchase_order_materials.php?action=1&project_id="+project_id+"&poid="+poid+"&item_id="+item_id+"&notes="+notes+"&tax="+tax+"&deposit="+deposit+"&subtotal="+st;

		window.location.href=url;
	});

	// update button at the bottom of the page
	$('#ud').click(function()
	{
		var poid = "<?php print $poid; ?>";
		var notes = $('#notes').val();
		var tax = $('#tax').val();
		var deposit = "";
		var st = $('#subtotal').val();
		var project_id = "<?php print $id; ?>";
		var tax_account = $('#tax_account').val();
		// validate tax as decimal
		$('#tax').css('background-color','');
		if(tax=='')
		{

		}
		else
		{
			var isvalid = isNumber(tax);
			if(isvalid==false)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is invalid. Please enter a decimal value less than the whole number 1. Do not enter any other characters in this field.');
				return false;
			} 

			if(tax>0.99)
			{
				$('#tax').css('background-color','red');
				alert('The tax amount you entered is not a valid percentage.Please enter a decimal value less than the whole number 1.');
				return false;
			}
		}
		var url = "edit_blank_purchase_order_materials.php?action=4&project_id="+project_id+"&poid="+poid+"&notes="+notes+"&tax="+tax+"&deposit="+deposit+"&subtotal="+st+"&tax_account="+tax_account;;
		window.location.href=url;
	});

	// previous button was clicked
	$('#previous').click(function(e)
	{
		e.preventDefault();

		// all accounts must have a value
		var error = 0;
	    $(".account").each(function() 
	    {
	        if($(this).val() == "") 
	        {
	            error++;   
	        }
	    });

	    if(error>0)
	    {
	    	alert('Please select an account.');
	    	$(".account").each(function() 
		    {
		        $(this).css('border','1px solid red');
		    });
			return false;
	    }

		$('#action').val(6);
		$('#done').submit();
	});

	// done button 
	$('#sbt').click(function(e)
	{
		e.preventDefault();

		// all accounts must have a value
		var error = 0;
	    $(".account").each(function() 
	    {
	        if($(this).val() == "") 
	        {
	            error++;   
	        }
	    });

	    if(error>0)
	    {
	    	alert('Please select an account.');
	    	$(".account").each(function() 
		    {
		        $(this).css('border','1px solid red');
		    });
			return false;
	    }

		$('#action').val(5);
		$('#done').submit();
	});

	var n = $('#notes').html();
    $("#notes").html($.trim(n));

    // filter the list of materials
	$("#inventory_form_1 input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) 
        {
            $('button[type=submit] .default').click();
            //alert('enter or return key clicked... filtering...');
        	
        	var poid="<?php print $poid; ?>";
        	var project_id = "<?php print $id; ?>";
			var filter = $('#filter').val();
			var filter_field = $('#filter_field').val();
			var loading = '<img src = "images/loading.gif" width = "100" height = "100" style = "margin-left:100px;margin-top:100px;">';
			$('#itable').remove();
			$('#inventory_table').html('');
			$('#inventory_table').html(loading);
			$.post( "jq.po_inventory_filter.php", { filter: filter, filter_field: filter_field, reset: 0, poid: poid, project_id: project_id, redirect: 4 })
			  .done(function( data ) 
			  {
			  		$('#inventory_table').html(data);
			  });    
            return false;
        }
    });

	$('#filter_reset').click(function(e)
	{
		e.preventDefault();
		var loading = '<img src = "images/loading.gif" width = "100" height = "100" style = "margin-left:100px;margin-top:100px;">';
		$('#itable').remove();
		$('#inventory_table').html('');
		$('#inventory_table').html(loading);
		var poid="<?php print $poid; ?>";
		var project_id = "<?php print $id; ?>";
		$.post( "jq.po_inventory_filter.php", { reset: 1, poid: poid, project_id: project_id, redirect: 4 })
		  .done(function( data ) 
		  {
		  		$('#inventory_table').html(data);
		  });
	});
	
	// change button colors
	$('.plus-update').each(function()
	{
		$(this).removeClass('btn-primary').addClass('btn-success');
	});

	$('.plus').each(function()
	{
		$(this).removeClass('btn-primary').addClass('btn-success');
	});

});
</script>
<style>
#itable tbody{
	display: block;
  	overflow: auto;
  	width: 100%;
  	height: 160px;
}
#itable thead {
	display: block;
	height: 40px;
	font-weight: bold;
	width: 100%;
}

#itable thead tr{
	display: block;
	width: 100%;
	height: 100%;
}
#itable thead tr th {
	display: block;	
	float: left;
}
#itable thead tr th:nth-child(1), #itable tbody tr td:nth-child(1) {
	width: 20%;
}
#itable thead tr th:nth-child(2), #itable tbody tr td:nth-child(2) {
	width: 45%;
}
#itable thead tr th:nth-child(3), #itable tbody tr td:nth-child(3) {
	width: 15%;
}
#itable thead tr th:nth-child(4), #itable tbody tr td:nth-child(4) {
	width: 15%;
}
#itable thead tr th:nth-child(5), #itable tbody tr td:nth-child(5) {
	width: 5%;
}

#itable{
	overflow-y: hidden;
	height: 100%;
	width: 100%;
}

</style>
<!-- END: PAGE SCRIPTS -->
</body>
</html>