<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
$s=array();

# save new
if($action==1)
{
	$date=$_POST['date'];
	$cost=str_replace(",", "", $_POST['cost']);
	$type=$_POST['type'];
	$vendor=$_POST['vendor'];
	$invoice=$_POST['invoice'];
	$check=$_POST['check'];
	$po_number=$_POST['po_number'];

	$vujade->create_row('costing_purchase_orders');
	$row_id = $vujade->row_id;
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'project_id',$id);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'date',$date);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'cost',$cost);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'type',$type);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'vendor_name',$vendor);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'invoice_number',$invoice);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'check_number',$check);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'ledger_po',$po_number);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'purchase_order_id',$po_number);
}
# update
if($action==2)
{
	$li_date=$_POST['li_date'];
	$cost=str_replace(",", "", $_POST['cost']);
	$invoice=$_POST['invoice'];
	$check=$_POST['check'];
	$po_number=$_POST['po_number'];

	$row_id=$_POST['costing_poid'];
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'date',$li_date);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'cost',$cost);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'invoice_number',$invoice);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'check_number',$check);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'ledger_po',$po_number);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'purchase_order_id',$po_number);
}
# delete
if($action==3)
{
	$row_id=$_REQUEST['costing_poid'];
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id,'deleted',1);
}

$shop_order = $vujade->get_shop_order($id, 'project_id');

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 17;
$cmenu = 3;
$section = 3;
$title = "Costing Ledger - ".$project['project_id'].' - '.$project['site'].' - ';;
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <?php require_once('project_left_tray.php'); ?>
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">
            <div class="pl15 pr15" style = "width:100%;">
              <?php require_once('project_right_tray.php'); ?>
              
              <div class="panel panel-primary panel-border top">
                <div class="panel-heading">
                  <span class="panel-title"></span>
                  <div class="widget-menu pull-right">
                    	<a href = "print_costing_ledger.php?id=<?php print $id; ?>" target = "_blank" class = "btn btn-sm btn-primary">Print</a> 
                  </div>
                </div>
                <div class="panel-body bg-light">
                	<table class = "table">
						<tr>
							<td>Date</td>
							<td>Cost</td>
							<td>Account</td>
							<td>Vendor/Description</td>
							<td>Invoice #</td>
							<td>Check #</td>
							<td>PO #</td>
							<td>&nbsp;</td>
						</tr>

						<?php
						# get all existing purchase orders
						$pos = $vujade->get_costing_purchase_orders($id);
						if($pos['error']=="0")
						{
							$subtotal = 0;
							$total_materials = 0;
							$total_outsource = 0;
							$total_subcontract = 0;
							unset($pos['error']);
							foreach($pos as $po)
							{
								if(!empty($po['vendor_name']))
								{
									$vendor = array();
									$vendor['name']=$po['vendor_name'];
								}
								else	
								{
									// get by ID column
									$tvendor1 = $vujade->get_vendor($po['vendor']);

									// get by list id
									$tvendor2 = $vujade->get_vendor($po['vendor'],'ListID');

									if($tvendor1['error']=="0")
									{
										$vendor=$tvendor1;
									}
									if($tvendor2['error']=="0")
									{
										$vendor=$tvendor2;
									}

								}
								?>

								<tr>
									<form method = "post" action = "costing_ledger.php">
									<td valign = "top">
										<input type = "text" name = "li_date" id = "li_date" value = "<?php print $po['date']; ?>" style = "width:100px;">
									</td>
									<td valign = "top"><input type = "text" name = "cost" id = "cost" value = "<?php print @number_format($po['cost'],2,'.',','); ?>" style = "width:75px;"></td>
									<td valign = "top"><?php print $po['type']; ?></td>
									<td valign = "top"><?php print $vendor['name']; ?></td>
									<td valign = "top"><input type = "text" name = "invoice" id = "invoice" value = "<?php print $po['invoice_number']; ?>" style = "width:75px;"></td>
									<td valign = "top"><input type = "text" name = "check" id = "check" value = "<?php print $po['check_number']; ?>" style = "width:75px;"></td>

									<td valign = "top">
										<input type = "text" name = "po_number" id = "po_number" value = "<?php print $po['purchase_order_id']; ?>" style = "width:75px;">
									</td>

									<td valign = "top">
										<?php
										# must have edit permission 
										if($projects_permissions['edit']==1)
										{
											?>
											<input type = "hidden" name = "action" value = "2">
											<input type = "hidden" name = "id" value = "<?php print $id; ?>">
											<input type = "hidden" name = "costing_poid" value = "<?php print $po['database_id']; ?>">
											<input type = "submit" value = "Update" class = "btn btn-primary btn-xs">
									<?php } ?>
									</form> 
									<br>
									<?php
									# must have delete permission 
									if($projects_permissions['delete']==1)
									{
										$form_id = $po['database_id'];
										?>
										<form method = "post" action = "costing_ledger.php">
											<input type = "hidden" name = "action" value = "3">
											<input type = "hidden" name = "id" value = "<?php print $id; ?>">
											<input type = "hidden" name = "costing_poid" value = "<?php print $po['database_id']; ?>">
											<a id="<?php print $form_id; ?>" class = "delete btn btn-danger btn-xs" href = "#">Delete</a>
										</form> 
									<?php } ?>
									</td>
								</tr>
								<?php
								$subtotal+=$po['cost'];
								if($po['type']=="Materials")
								{
									$total_materials+=$po['cost'];
								}
								if($po['type']=="Outsource")
								{
									$total_outsource+=$po['cost'];
								}
								if($po['type']=="Subcontract")
								{
									$total_subcontract+=$po['cost'];
								}
							}
						}
						?>

					<?php
					# must have create permission 
					if($projects_permissions['create']==1)
					{
						?>
						<!-- new costing purchase order -->
						<tr>
							<td colspan = "7">&nbsp;</td>
						</tr>
						<form method = "post" action = "costing_ledger.php">
						<tr style = "padding-bottom:2px;border-bottom:1px solid #cecece;">
							<td valign = "top">
								<input type = "text" name = "date" id = "date" value = "" class = "" style = "width:100px;">
							</td>
							<td valign = "top">
								<input type = "text" class = "cost" name = "cost" id = "cost" value = "" style = "width:75px;">
							</td>
							<td valign = "top">
								<select name = "type" id = "type">
									<option value = "">-Select-</option>
									<option value = "Materials">Materials</option>
									<option value = "Outsource">Outsource</option>
									<option value = "Subcontract">Subcontract</option>
								</select>
							</td>
							<td valign = "top">
								<input type = "text" name = "vendor" id = "vendor" value = "" style = "width:125px;">
							</td>
							<td valign = "top">
								<input type = "text" name = "invoice" id = "invoice" value = "" style = "width:75px;">
							</td>
							<td valign = "top">
								<input type = "text" name = "check" id = "check" value = "" style = "width:75px;">
							</td>

							<td valign = "top">
								<input type = "text" name = "po_number" id = "po_number" value = "" style = "width:75px;">
							</td>

							<td valign = "top">
								<input type = "hidden" name = "action" value = "1">
								<input type = "hidden" name = "id" value = "<?php print $id; ?>">
								<input type = "submit" value = "Add" class = "btn btn-success btn-xs">
							</form> 
							<br>

							</td>
						</tr>
					<?php } ?>
					</table>

					<p style = "width:100%;height:15px;">&nbsp;</p>

					<table class = "table">
						<tr>
							<td>Direct Purchase</td>
							<td>$<?php print @number_format($total_materials,2,'.',','); ?></td>
							<td>Outsource</td>
							<td>$<?php print @number_format($total_outsource,2,'.',','); ?></td>
							<td>Subcontract</td>
							<td>$<?php print @number_format($total_subcontract,2,'.',','); ?></td>
							<td>Subtotal</td>
							<td>$<?php print @number_format($subtotal,2,'.',','); ?></td>
						</tr>
					</table>
                </div>
                </div>
            </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>

  <!-- delete ledger item modal -->
  <div id = "delete-modal" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">WARNING</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete this ledger item? This action cannot be undone.</p>
        <!--<input type = "text" id = "test" class = "form-control">-->
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button> 
        <a href = "" id = "confirm-delete" class="btn btn-success">Yes</a>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

  <!-- End: Main -->
  <!-- BEGIN: PAGE SCRIPTS -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {
    	"use strict";
    
    	// Init Theme Core    
    	Core.init();

    	//var n = $('#notes').html();
    	//$("#notes").html($.trim(n));

    	// datepickers
	    $(".dp").datepicker();

	    // change the focus to the cost box
	    $('#date').datepicker(
	    {
	        onClose: function(dateText, inst) 
	        {
	            $('.cost').focus();
	        }
	    });

	    // delete button click (show modal)
	    $('.delete').click(function(e)
	    {
	    	e.preventDefault();
	    	var form_id = this.id;
	    	$('#delete-modal').modal('show');
	    	var href = 'costing_ledger.php?id=<?php print $id; ?>&action=3&costing_poid='+form_id;
	    	$('#confirm-delete').attr('href',href);
	    });
    
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>