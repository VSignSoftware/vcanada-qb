<?php
$active_jobs = $vujade->get_projects_by_status(16,array(),1);

$jobs_on_budget=0; 
$now = strtotime('now');
$thirty_days = strtotime('-30 days');
$closed_jobs = $vujade->get_closed_projects($now,$thirty_days,false,2);

//print $closed_jobs['sql'];
if($closed_jobs['error']=="0")
{
	$pids = array();
	foreach($closed_jobs as $cj)
	{
		$pids[]=$cj['project_id'];
	}
	$total_closed_jobs = count($pids);
	foreach($pids as $id)
	{
		$pos = $vujade->get_purchase_orders($id);
		$cpos = $vujade->get_costing_purchase_orders($id);
		if($cpos['error']=="0")
		{
			$subtotal = 0;
			$total_dp = 0;
			$total_materials = 0;
			$total_outsource = 0;
			$total_subcontract = 0;
			unset($cpos['error']);
			foreach($cpos as $cpo)
			{
				$subtotal+=$cpo['cost'];
				if($cpo['type']=="Materials")
				{
					$total_materials+=$cpo['cost'];
					$total_dp+=$cpo['cost'];
				}
				if($cpo['type']=="Outsource")
				{
					$total_outsource+=$cpo['cost'];
				}
				if($cpo['type']=="Subcontract")
				{
					$total_subcontract+=$cpo['cost'];
				}
			}
		}

		$inventory_cost = 0;
		$items = $vujade->get_materials_for_costing($id);
		if($items['error']=="0")
		{
			unset($items['error']);
			foreach($items as $i)
			{
				$line = $i['cost']*$i['qty'];
				$inventory_cost += $line;
			}
			$indt = $inventory_cost * $project['indeterminant'];
		}
		$tm = $total_dp+$inventory_cost+$indt;

		# labor
		$raw_labor = 0;
		$labor_rider = 0;
		$total_labor = 0;
		$tcdata = $vujade->get_timecards_for_project($id);
		if($tcdata['error']=="0")
		{
			unset($tcdata['error']);
			foreach($tcdata as $tc)
			{
				# employee 
				$employee_id = $tc['employee_id'];

				# employee's rate
				$current_rate = $vujade->get_employee_current_rate($employee_id);

				# type of work
				$work_type = $tc['type'];

				# rate for type of work
				$rate_data = $vujade->get_labor_rate($work_type,'id');

				# hours worked
				$total_time = 0;
				$st_total = 0;
				$ot_total = 0;
				$dt_total = 0;
				if(!empty($tc['start']))
				{
					$ts = $tc['date'];
					$begints = strtotime($ts.' '.$tc['start']);
					$endts = strtotime($ts.' '.$tc['end']);
					$diff = $endts-$begints;
					$diff = $diff / 60;
					$diff = $diff / 60;
					$line_total=$diff;
					//$st_total+=$diff;
					if($diff<=8)
					{
						$st_total=$diff;
					}
					if( ($diff > 8) && ($diff <= 12) )
					{
						$ot_total = $diff - 8;
						$st_total = 8;
					}

					if($diff > 12)
					{
						$ot_total = 4;
						$st_total = 8;
						$dt_total = $diff - 12;
					}
				}
				else
				{
					$line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
					$st_total+=$tc['standard_time'];
					$ot_total+=$tc['over_time'];
					$dt_total+=$tc['double_time'];

				}
				$total_time+=$line_total;

				# raw labor 
				$st_rate_total = $st_total * $current_rate['rate'];
				$ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
				$dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
				$raw_labor += $st_rate_total + $ot_rate_total + $dt_rate_total;

				# labor rider
				$labor_rider_rate = $rate_data['rate']-$current_rate['rate'];
				$labor_rider += $labor_rider_rate * $total_time;
			}
			$total_labor += $labor_rider + $raw_labor;
		}

		$col1_subtotal = $tm + $total_labor + $total_outsource + $total_subcontract;

		# general overhead
		$general_overhead = 0;
		$general_overhead = $tm + $total_labor;
		$general_overhead = $general_overhead * $general_overhead_percentage;

		# buyout overhead
		$buyout_overhead = ($total_outsource + $total_subcontract) * $buyout_overhead_percentage;

		# total cost
		$total_cost = 0;
		$total_cost = $col1_subtotal + $general_overhead + $buyout_overhead;

		# base sale, additional billing, permit, permit acquisition, totla sale price
		$base_sale = 0;
		$additional_billing = 0;
		$permit = 0;
		$permit_acquisition = 0;
		$discount = 0;
		$total_sale_price = 0;
		$bsa=array();
		// Additional billing = Engineering + Change Orders + Shipping - discount. 
		$project_invoices = $vujade->get_invoices_for_project($id);
		if($project_invoices['error']=="0")
		{
			unset($project_invoices['error']);
			foreach($project_invoices as $pi)
			{
				if($pi['costing']==1)
				{
					$invoiceid=$pi['database_id'];
					
					$firstinvoice=$pi;

					// not legacy
					if($pi['is_legacy']==0)
					{
						$st=0;

						// invoice types
						// 1: illuminated and lump sum
						if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==1) )
						{
							// labor
							$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

							// parts 
							$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

							// total
							$st = $labor+$parts; 

							$parts=0;

							// selling price text
							$selling_price_text = "Selling Price";
						}

						// 2: illuminated sign parts and labor
						if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==2) )
						{
							$show_parts=true;

							// labor
							$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

							// parts 
							$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

							// selling price text
							$selling_price_text = "Labor Only Price";
						}

						// 3: non-illuminated lump sum
						if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==1) )
						{
							$show_parts=false;

							$st = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

							$parts=0;

							// selling price text
							$selling_price_text = "Selling Price";
						}

						// 4: non-illuminated sign parts and labor
						if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==2) )
						{
							$show_parts=true;

							// labor
							$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

							// parts 
							$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

							// selling price text
							$selling_price_text = "Labor Only Price";
						}

						// 5: service with parts and labor
						if( ($firstinvoice['type_1']==3) && ($firstinvoice['type_2']==2) )
						{
							$show_parts=true;

							// labor
							$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

							// parts 
							$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

							// selling price text
							$selling_price_text = "Labor Only Price";
						}
					
						// payments
						$payments = $vujade->get_invoice_payments($invoiceid);
						if($payments['error']=="0")
						{
							unset($payments['error']);
							$pt = 0;
							foreach($payments as $payment)
							{
								$pt+=$payment['amount'];
							}
						}
						else
						{
							$pt=0;
						}

						// tax amount
						// this will be changing in the future
						//$tax_amount += $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
						//$st+=$tax_amount;

						$tax_amount=$pi['sales_tax'];

						$tax_rate=$pi['tax_rate'];

						// change orders
						$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

						// non-taxable co
						$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);
						$co+=$ntco;

						// engineering
						$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

						// permits
						$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

						// permit acq
						$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

						// shipping
						$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

						// discount
						//$discount+=$pi['discount'];
						//$discount = str_replace("-","",$discount);

						$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

						// retention
						$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
						$retention=str_replace("-","",$retention);

						// deposit
						$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
						//$deposit = str_replace("-","",$deposit);

						$base_sale += $st;
						$base_sale += $parts;
						$bsa[]=$st;
						$bsa[]=$parts;
						$ab = $engineering+$co+$shipping;
						//$ab=$ab+$discount;
						$additional_billing += $ab;
						$permit += $permits;
						$permit_acquisition += $pa;
					}
					else
					{
						// legacy
						$st=0;

						// legacy invoices will have: manufacture & install, labor, parts

						// check for manufacture and install
						$manufacture =  $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

						// labor
						$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

						// parts 
						$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

						// payments
						$payments = $vujade->get_invoice_payments($invoiceid);
						if($payments['error']=="0")
						{
							unset($payments['error']);
							$pt = 0;
							foreach($payments as $payment)
							{
								$pt+=$payment['amount'];
							}
						}
						else
						{
							$pt=0;
						}

						// tax amount
						$tax_amount=$firstinvoice['sales_tax'];

						$tax_rate=$pi['tax_rate'];

						// change orders
						$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

						// engineering
						$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

						// permits
						$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

						// permit acq
						$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

						// shipping
						$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

						// discount
						$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

						// retention
						$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
						$retention=str_replace("-","",$retention);

						// deposit
						$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
						$deposit = str_replace("-","",$deposit);

						$base_sale += $manufacture;
						$base_sale += $labor;
						$base_sale += $parts;
						$bsa[]=$manufacture;
						$bsa[]=$labor;
						$bsa[]=$parts;
						$ab = $engineering+$co+$shipping;
						//$ab=$ab+$discount;
						$additional_billing += $ab;
						$permit += $permits;
						$permit_acquisition += $pa;
					}
				}
			}
			foreach($bsa as $bamnt)
			{
				$total_sale_price+=$bamnt;
			}

			if($discount<0)
			{
				$total_sale_price += $ab+$permits+$pa+$discount;
			}
			else
			{
				$total_sale_price += $ab+$permits+$pa-$discount;
				$discount = 0-$discount;
			}
			$net_profit = $total_sale_price - $total_cost - $project['commission'];

			// $markup = $total_sale_price/$total_cost;
			// net profit / sales price
			$markup = @($net_profit / $total_sale_price);
			//$markup = round($markup,2);
			$markup = floor($markup*100)/100;
			if($markup>=$setup['on_budget'])
			{
				$jobs_on_budget++;
			}
			$markup+=1;
		}
	}
	$jobs_on_budget=$jobs_on_budget/$total_closed_jobs;
	$jobs_on_budget=$jobs_on_budget*100;
	$jobs_on_budget=number_format($jobs_on_budget,1);
}
?>