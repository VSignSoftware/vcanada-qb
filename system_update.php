<?php 
session_start();

//error_reporting(-1);

define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

if($_SESSION['username']!="Administrator")
{
	$vujade->page_redirect('error.php?m=1');
}

$setup=$vujade->get_setup();

// run only on qb servers
if($setup['is_qb']==1)
{
	
}

// run on all servers
//$vujade->update_estimates(2);
//$vujade->messages[]="update_estimates function ran.";

//$vujade->helper_copy_customer_contacts();
//$vujade->messages[]="helper_copy_customer_contacts method ran.";

//die;

// nothing running...
// $vujade->messages[]="No scripts configured to run at this time.";

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Administrator Settings - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=12;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<h3>System Update</h3>

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
