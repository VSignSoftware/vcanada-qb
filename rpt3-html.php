<?php
session_start();

ini_set('memory_limit','250M');

define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
 
$begin = $_REQUEST['begin'];
$end = $_REQUEST['end'];
$labor_type = $_REQUEST['labor_type'];
$title = "Labor Sort Report";

# check empty
$vujade->check_empty($begin,'Begin Date');
$vujade->check_empty($end,'Begin End');
$vujade->check_empty($department,'Department');

$e = $vujade->get_error_count();
if($e>0)
{
	$action=0;
}

$pateHTML .= '
 
 <!DOCTYPE html>
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>PDF Report</title>
 <meta name="description" content="">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <style>
 .bordered
 {
 padding-bottom:3px;
 border-bottom: 1px solid #cecece;
 }
 .header
 {
 font-weight: bold;
 font-size: 14px;
 }
 .header_graybg
 {
 background-color: #cecece;
 font-weight: bold;
 font-size: 14px;
 padding-bottom:3px;
 border-bottom: 1px solid #cecece;
 }
 
 
 table {font-family: DejaVuSansCondensed; font-size: 11pt; line-height: 1.2;
 margin-top: 2pt; margin-bottom: 5pt;
 border-collapse: collapse;  }
 
 thead {	font-weight: bold; vertical-align: bottom; }
 tfoot {	font-weight: bold; vertical-align: top; }
 thead td { font-weight: bold; }
 tfoot td { font-weight: bold; }
 
 thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }
 
 .headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
 .footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
 
 th {	font-weight: bold;
 vertical-align: top;
 text-align:left;
 padding-left: 2mm;
 padding-right: 2mm;
 padding-top: 0.5mm;
 padding-bottom: 0.5mm;
 }
 
 td {
 	padding-left: 2mm;
	vertical-align: top;
	text-align:left;
	padding-right: 2mm;
	padding-top: 0.5mm;
	padding-bottom: 0.5mm;
 }
 
 th p { text-align: left; margin:0pt;  }
 td p { text-align: left; margin:0pt;  }
 
 .center-text{ text-align: center; }
 
 </style>
 
 </head>
 <body>
 <div id="mainContent">
 	<div id="content">
	<h2 class="">'.$title.'</h2>
';
         
# labor type data
$labor_info = $vujade->get_labor_type_for_timecard($labor_type);

# date/day info
$startts = strtotime($begin);
$endts = strtotime($end);
$days = $vujade->get_dates_in_range($startts,$endts);
$end_date = $end;
$mysqli = $vujade->mysqli;

$total_st=0;
$total_ot=0;
$total_dt=0;

$total_vacation=0;
$total_holiday=0;

$total_paid;

# heading row
$pateHTML.='

<span style = "font-weight:bold;font-size:16px;">Labor Sort '. $begin.' to '.$end.' - '.$labor_info['type'].'</span>
<table class = "table">
    <tr class = "header_graybg">
        <td>Job No</td>
        <td>Employee</td>
        <td>S.T.</td>
        <td>Rate</td>
        <td>O.T.</td>
        <td>Rate</td>
        <td>D.T.</td>
        <td>Rate</td>
        <td>Vac</td>
        <td>Hol</td>
        <td>Total Paid</td>
    </tr>';

# get all project ids in the time range
$employee_ids = array();
$project_ids = array();
$sql = "SELECT * FROM `timecards` WHERE `date_ts` >= '$startts' AND `date_ts` <= '$endts' AND `type` = '$labor_type' ORDER BY `project_id`,`date_ts`";
//print $sql;    
//print '<hr>';
//die;
$result = $mysqli->query($sql);
if( (!$result) || ($result->num_rows<1) )
{
    $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
    //print '<p>Error: '.$error.'</p>';
}
else
{
	//$step3_end = microtime(true);
	//$step3 = $step3_end - $time_start;
	//print "Time elapsed 3: ".$step3.'<hr>';
	$table_data = array();
    while($rows = $result->fetch_assoc())
    {
    	$table_rows = array();
        $eid=$rows['employee_id'];
        $rate_data = $vujade->get_employee_current_rate($eid);
        $person = $vujade->get_employee($eid,1);

        $date = $rows['date'];
        $date_ts = $rows['date_ts'];
        $type = $rows['type'];
        $project_id = $rows['project_id'];
        $start = $rows['start'];
        $end = $rows['end'];
        $standard_time = $rows['standard_time'];
        $over_time = $rows['over_time'];
        $double_time = $rows['double_time'];

        $skip = array('Holiday','Vacation');
        if(in_array($type, $skip))
        {
            if($type=="Holiday")
            {
                $day_holiday+=$standard_time;
            }
            if($type=="Vacation")
            {
                $day_vacation+=$standard_time;
            } 
        }
        else
        {
            $day_st+=$standard_time;
        }
        
        $day_ot+=$over_time;
        $day_dt+=$double_time;

        # 
        $day_total+=$standard_time;
        $day_total+=$over_time;
        $day_total+=$double_time;

        $table_rows['project_id']=$rows['project_id'];
        $table_rows['date']=$rows['date'];
        $table_rows['fullname']=$person['fullname'];
        $table_rows['st']=$day_st;
        $table_rows['st_rate']=$rate_data['rate'];
        $table_rows['st_pay']=$rate_data['rate'] * $day_st;
        $table_rows['ot']=$day_ot;
        $otrate = $rate_data['rate']*1.5;
        $table_rows['ot_rate']=$otrate;
        $table_rows['ot_pay']=$ot_pay = $otrate * $day_ot;
        $table_rows['dt']=$day_dt;
        $dtrate = $rate_data['rate']*2;
        $table_rows['dt_rate']=$dtrate;
        $table_rows['dt_pay']=$dt_pay = $dtrate * $day_dt;
        $table_rows['vacation']=$day_vacation;
        $v_pay = $rate_data['rate'] * $day_vacation;
        $table_rows['vacation_pay']=$v_pay;
        $h_pay = $rate_data['rate'] * $day_holiday;
        $table_rows['holiday']=$day_holiday;
        $table_rows['holiday_pay']=$h_pay;

        # total paid
        $st_pay = $rate_data['rate'] * $day_st;
        $ot_pay = $otrate * $day_ot;
        $dt_pay = $dtrate * $day_dt;
        $h_pay = $rate_data['rate'] * $day_holiday;
        $v_pay = $rate_data['rate'] * $day_vacation;
        $day_total_paid=($st_pay+$ot_pay+$dt_pay+$h_pay+$v_pay);
        $table_rows['day_total_paid']=$day_total_paid;
        $table_data[]=$table_rows;

        $total_st+=$day_st;
        $total_ot+=$day_ot;
        $total_dt+=$day_dt;

        $total_vacation+=$day_vacation;
        $total_holiday+=$day_holiday;

        $total_paid+=$day_total_paid; 
        $day_st=0;
        $day_ot=0;
        $day_dt=0;
        $day_vacation=0;
        $day_holiday=0;

        $day_total_paid=0;
        $day_total=0;
    }

   	$project_ids = array();
    foreach($table_data as $arr)
    {
    	$project_ids[]=$arr['project_id'];
    }

    $project_ids=array_unique($project_ids);
    //print_r($project_ids);
    foreach($project_ids as $project_id)
    {
        # row html output
        $pateHTML.= '<tr>';

        # blank cells
        $pateHTML.= '<td colspan = "11">';
        //$pateHTML.= $sql;
        $pateHTML.= $project_id.'<br>';
        $pateHTML.= $rows['date'];
        $pateHTML.= '</td>';
        $pateHTML.= '</tr>';

        foreach($table_data as $arr)
        {
        	if($project_id==$arr['project_id'])
        	{
        		$pateHTML.= '<tr>';
        		$pateHTML.= '<td>&nbsp;';
        		$pateHTML.= '</td>';

        		# name and date
	            $pateHTML.= '<td>';
	            $pateHTML.= $arr['fullname'];
	            $pateHTML.= '</td>';

	            # standard hour running total
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['st'],2,'.',',');
	            $pateHTML.= '</td>';

	            # standard rate
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['st_rate'],2,'.',',');
	            $pateHTML.= '</td>';

	            # overtime
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['ot'],2,'.',',');
	            $pateHTML.= '</td>';

	            # overtime rate
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['ot_rate'],2,'.',',');
	            $pateHTML.= '</td>';

	            # double time
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['dt'],2,'.',',');
	            $pateHTML.= '</td>';

	            # double time rate
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['dt_rate'],2,'.',',');
	            $pateHTML.= '</td>';

	            # vacation
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['vacation'],2,'.',',');
	            $pateHTML.= '</td>';

	            # holiday
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['holiday'],2,'.',',');
	            $pateHTML.= '</td>';

	            # day paid
	            $pateHTML.= '<td>';
	            $pateHTML.= number_format($arr['day_total_paid'],2,'.',',');
	            $pateHTML.= '</td>';

        		$pateHTML.= '</tr>';
        	}
        }
    }

    # summary rows
    $pateHTML.= '<tr>';
    $pateHTML.= '<td colspan = "11"><hr>';
    $pateHTML.= '</td>';
    $pateHTML.= '</tr>';
    $pateHTML.= '<tr>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= number_format($total_st,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= number_format($total_ot,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= number_format($total_dt,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= number_format($total_vacation,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= number_format($total_holiday,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '<td>';
    $pateHTML.= number_format($total_paid,2,'.',',');
    $pateHTML.= '</td>';
    $pateHTML.= '</tr>';
    $pateHTML.= '</table>';
}



$pateHTML .= '
   			</div>
    	</div>
    </body>
</html>';