<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$date1 = $_REQUEST['date1'];
	$date1ts = strtotime($date1);
	$date2 = $_REQUEST['date2'];
	$date2ts = strtotime($date2);
}
$nav = 5;
$title = "Payroll Summary Report";
 
$logo = $vujade->get_logo();
if(!isset($charset))
{
	$charset = "utf-8";
} 

$pateHTML = '';
 

$pateHTML .= '
	<!DOCTYPE html>
	<head>
	<meta charset="'.$charset.'">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>'.$title.'</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		.bordered {
			padding-bottom: 3px;
			border-bottom: 1px solid #cecece;
		}
		
		.header {
			font-weight: bold;
			font-size: 14px;
		}
		
		.header_graybg {
			background-color: #cecece;
			font-weight: bold;
			font-size: 14px;
			padding-bottom: 3px;
			border-bottom: 1px solid #cecece;
		}
		
		.button-container-style{
			margin-top: 20px; 
			padding-top:10px; 
			border-top: 1px #ccc solid;
		}
	</style>
		
</head>
<body>

	<div id="mainContent">

		<div id="content">
			<br style="clear: both;" />
			<h2 class="signInText">
				'.$title.'
			</h2>
			
			<div style="background-color: #989898; width: 100%">
				<div style="padding: 5px;">
					Time Cards for Dates '.$date1.' to '.$date2.'
				</div>
			</div>
';
 
			# display data date range
			if($action==1)
			{
				// depts 100-500
				$d1st = 0;
				$d1ot = 0;
				$d1dt = 0;
				$d1vacation = 0;
				$d1holiday = 0;
				$d1sick = 0;

				// dept 600
				$d2st = 0;
				$d2ot = 0;
				$d2dt = 0;
				$d2vacation = 0;
				$d2holiday = 0;
				$d2sick = 0;

				// total payroll
				$tst = 0;
				$tot = 0;
				$tdt = 0;
				$tvacation = 0;
				$tholiday = 0;
				$tsick = 0;
				 
				$pateHTML .= '
				<table width="100%">
				<tr class="bordered header">
					<td width="25%" colspan="2"></td>
					<td width="10%"><strong>Regular</strong></td>
					<td width="15%"><strong>Overtime</strong></td>
					<td width="15%"><strong>DBL Time</strong></td>
					<td width="6%"><strong>Vac</strong></td>
					<td width="10%"><strong>Holiday</strong></td>
					<td width="10%"><strong>Sick</strong></td>
				</tr>

				<tr class="header_graybg">
					<td colspan="2">All Departments</td>
					<td colspan="6">&nbsp;</td>
				</tr>
				';
				 
				$reportdata1 = $vujade->get_payroll_summary_report_data('All Departments',$date1ts,$date2ts);
				if($reportdata1['error']=="0")
				{
					unset($reportdata1['error']);
					foreach($reportdata1 as $data)
					{
						
						$pateHTML .= '
							<tr class="bordered">
								<td>'.$data['employee_id'].'</td>
								<td>'.$data['fullname'].'</td>
								<td> ';
									
								if($data['rate']!="Salary")
								{
									$pateHTML .= $data['standard_time'];
									$d1st+=$data['standard_time'];
								}
								else
								{
									$pateHTML .= 'Salary';
								}
								$pateHTML .= '
											</td>
											<td>'.$data['over_time'].'
											</td>
											<td>'.$data['double_time'].'
											</td>
											<td>'.$data['vacation'].'
											</td>
											<td>'.$data['holiday'].'
											</td>
											<td>'.$data['sick'].'<br>
											</td>
										</tr>
										';
							 
										$d1ot += $data['over_time'];
										$d1dt += $data['double_time'];
										$d1vacation += $data['vacation'];
										$d1holiday += $data['holiday'];
										$d1sick += $data['sick'];
							}
						}
				 
				$pateHTML .= '
				<!-- blank row -->
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>

				<tr class="header_graybg">
					<td colspan="2">OFFICERS - DEPT. 100</td>
					<td colspan="6">&nbsp;</td>
				</tr>';

				$reportdata1 = $vujade->get_payroll_summary_report_data('Officers Dept. 100',$date1ts,$date2ts);
				if($reportdata1['error']=="0")
				{
					unset($reportdata1['error']);
					foreach($reportdata1 as $data)
					{
						
						$pateHTML .= '
							<tr class="bordered">
								<td>'.$data['employee_id'].'</td>
								<td>'.$data['fullname'].'</td>
								<td> ';
									
								if($data['rate']!="Salary")
								{
									$pateHTML .= $data['standard_time'];
									$d1st+=$data['standard_time'];
								}
								else
								{
									$pateHTML .= 'Salary';
								}
								$pateHTML .= '
											</td>
											<td>'.$data['over_time'].'
											</td>
											<td>'.$data['double_time'].'
											</td>
											<td>'.$data['vacation'].'
											</td>
											<td>'.$data['holiday'].'
											</td>
											<td>'.$data['sick'].'<br>
											</td>
										</tr>
										';
							 
										$d1ot += $data['over_time'];
										$d1dt += $data['double_time'];
										$d1vacation += $data['vacation'];
										$d1holiday += $data['holiday'];
										$d1sick += $data['sick'];
							}
						}

				$pateHTML .= '<!-- blank row -->
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr><tr class="header_graybg">
					<td colspan="2">Clerical Dept. 200</td>
					<td colspan="6">&nbsp;</td>
				</tr>
				';
				 
				$reportdata2= $vujade->get_payroll_summary_report_data('Clerical Dept. 200',$date1ts,$date2ts);
				if($reportdata2['error']=="0")
				{
					unset($reportdata2['error']);
					foreach($reportdata2 as $data)
					{
						 
						$pateHTML .= '
				<tr class="bordered">
					<td>'.$data['employee_id'].'</td>
					<td>'.$data['fullname'].'</td>
					<td> 
					';
					if($data['rate']!="Salary")
					{
						$pateHTML .= $data['standard_time'];
						$d1st+=$data['standard_time'];
					}
					else
					{
						$pateHTML .= 'Salary';
					}
					 
					
					$pateHTML .= '
									</td>
									<td>'.$data['over_time'].'</td>
									<td>'.$data['double_time'].'</td>
									<td>'.$data['vacation'].'</td>
									<td>'.$data['holiday'].'</td>
									<td>'.$data['sick'].'<br></td>
								</tr>
								';
				 
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick += $data['sick'];
					}
				}
				 
				$pateHTML .= '
				<!-- blank row -->
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>

				<tr class="header_graybg">
					<td colspan="2">Design Dept. 300</td>
					<td colspan="6">&nbsp;</td>
				</tr>
				';
				 
				$reportdata3= $vujade->get_payroll_summary_report_data('Design Dept. 300',$date1ts,$date2ts);
				if($reportdata3['error']=="0")
				{
					unset($reportdata3['error']);
					foreach($reportdata3 as $data)
					{
						 
					$pateHTML .= '
					<tr class="bordered">
					<td>'.$data['employee_id'].'</td>
					<td>'.$data['fullname'].'</td>
					<td> 
					';
					if($data['rate']!="Salary")
					{
						$pateHTML .= $data['standard_time'];
						$d1st+=$data['standard_time'];
					}
					else
					{
						$pateHTML .= 'Salary';
					}
					$pateHTML .= '
									</td>
									<td>'.$data['over_time'].'
									</td>
									<td>'.$data['double_time'].'
									</td>
									<td>'.$data['vacation'].'
									</td>
									<td>'.$data['holiday'].'
									</td>
									<td>'.$data['sick'].'<br>
									</td>
								</tr>';
				
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick += $data['sick'];
					}
				}
				
				$pateHTML .= '
				<!-- blank row -->
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>

				<tr class="header_graybg">
					<td colspan="2">Sales Dept. 400</td>
					<td colspan="6">&nbsp;</td>
				</tr>';

				 
				$reportdata4= $vujade->get_payroll_summary_report_data('Sales Dept. 400',$date1ts,$date2ts);
				if($reportdata4['error']=="0")
				{
					unset($reportdata4['error']);
					foreach($reportdata4 as $data)
					{
						 
					$pateHTML .= '
					<tr class="bordered">
					<td>'.$data['employee_id'].'</td>
					<td>'.$data['fullname'].'</td>
					<td>';
					if($data['rate']!="Salary")
					{
						$pateHTML .= $data['standard_time'];
						$d1st+=$data['standard_time'];
					}
					else
					{
						$pateHTML .= 'Salary';
					}
					$pateHTML .= '
					</td>
					<td>'.$data['over_time'].'
					</td>
					<td>'.$data['double_time'].'
					</td>
					<td>'.$data['vacation'].'
					</td>
					<td>'.$data['holiday'].'
					</td>
					<td>'.$data['sick'].'<br>
					</td>
				</tr>';
				 
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick += $data['sick'];
					}
				}
				 
				$pateHTML .= '
				<!-- blank row -->
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>

				<tr class="header_graybg">
					<td colspan="2">Project Cord. Dept. 500</td>
					<td colspan="6">&nbsp;</td>
				</tr>
				';
				 
				$reportdata5= $vujade->get_payroll_summary_report_data('Project Cord. Dept. 500',$date1ts,$date2ts);
				if($reportdata5['error']=="0")
				{
					unset($reportdata5['error']);
					foreach($reportdata5 as $data)
					{
						 
						$pateHTML .= '
						<tr class="bordered">
					<td>'.$data['employee_id'].'</td>
					<td>'.$data['fullname'].'</td>
					<td>';
					if($data['rate']!="Salary")
					{
						$pateHTML .= $data['standard_time'];
						$d1st+=$data['standard_time'];
					}
					else
					{
						$pateHTML .= 'Salary';
					}
					$pateHTML .= '
					</td>
					<td>'.$data['over_time'].'
					</td>
					<td>'.$data['double_time'].'
					</td>
					<td>'.$data['vacation'].'
					</td>
					<td>'.$data['holiday'].'
					</td>
					<td>'.$data['sick'].'<br>
					</td>
				</tr>';
			 
				$d1ot += $data['over_time'];
				$d1dt += $data['double_time'];
				$d1vacation += $data['vacation'];
				$d1holiday += $data['holiday'];
				$d1sick += $data['sick'];
					}
				}
				$pateHTML .= '

				<!-- blank row -->
				<tr>
					<td colspan="8">&nbsp;</td>
				</tr>

				<tr class="header_graybg">
					<td colspan="2">Sign Mfg. Dept. 600</td>
					<td colspan="6">&nbsp;</td>
				</tr>

				';
				$reportdata6= $vujade->get_payroll_summary_report_data('Sign Mfg. Dept. 600',$date1ts,$date2ts);
				if($reportdata6['error']=="0")
				{
					unset($reportdata6['error']);
					foreach($reportdata6 as $data)
					{
						 
						$pateHTML .= '
							<tr class="bordered">
								<td>'.$data['employee_id'].'</td>
								<td>'.$data['fullname'].'</td>
								<td>';
					if($data['rate']!="Salary")
					{
						$pateHTML .=  $data['standard_time'];
						$d2st+=$data['standard_time'];
					}
					else
					{
						$pateHTML .= 'Salary';
					}
					$pateHTML .= '
					
					</td>
					<td>'.$data['over_time'].'
					</td>
					<td>'.$data['double_time'].'
					</td>
					<td>'.$data['vacation'].'
					</td>
					<td>'.$data['holiday'].'
					</td>
					<td>'.$data['sick'].'<br>
					</td>
				</tr>';
				
				 
				$d2ot += $data['over_time'];
				$d2dt += $data['double_time'];
				$d2vacation += $data['vacation'];
				$d2holiday += $data['holiday'];
				$d2sick += $data['sick'];
					}
				}

				$tst += $d1st+$d2st;
				$tot += $d1ot+$d2ot;
				$tdt += $d1dt+$d2dt;
				$tvacation += $d1vacation+$d2vacation;
				$tholiday += $d1holiday+$d2holiday;
				$tsick += $d1sick + $d2sick;

				$pateHTML .= '
								<!-- blank row -->
								<tr>
									<td colspan="8">&nbsp;</td>
								</tr> 
				
								<tr class="header_graybg">
									<td colspan="2">Total Dept 100, 200, 300, 400, 500 &amp; All</td>
									<td>'.$d1st.'</td>
									<td>'.$d1ot.'</td>
									<td>'.$d1dt.'</td>
									<td>'.$d1vacation.'</td>
									<td>'.$d1holiday.'</td>
									<td>'.$d1sick.'<br>
									</td>
								</tr>
				
								<tr class="header_graybg">
									<td colspan="2">Total Dept 600</td>
									<td>'.$d2st.'</td>
									<td>'.$d2ot.'</td>
									<td>'.$d2dt.'</td>
									<td>'.$d2vacation.'</td>
									<td>'.$d2holiday.'</td>
									<td>'.$d2sick.'<br>
									</td>
								</tr>
				
								<!-- blank row -->
								<tr>
									<td colspan="8">&nbsp;</td>
								</tr>
				
								<tr class="header_graybg">
									<td colspan="2">Total Payroll Hours</td>
									<td>'.$tst.'</td>
									<td>'.$tot.'</td>
									<td>'.$tdt.'</td>
									<td>'.$tvacation.'</td>
									<td>'.$tholiday.'</td>
									<td>'.$tsick.'<br>
									</td>
								</tr>
				
							</table>';
	 
			}
			 
$pateHTML .= '	
			<div class="button-container-style">&nbsp;</div>	
			</div>
		</div>

	</body>
</html>';

//echo $pateHTML;

