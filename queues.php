<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$title = "Queues - ";
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=4;
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Queues</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					
				</div>

	        	<div class="panel-body bg-light" style="overflow-x: auto !important;">
					<div class = "row">
						<div class = "col-md-12">

							<table>

								<tr>
									<td colspan = "4"><a href="report.php?type=17" style = "width:220px;" class="btn btn-sm btn-primary">Orders: All Pending</a>
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td><a href="report.php?type=1" style = "width:220px;margin-right:10px;" class="btn btn-sm btn-primary">Orders: Pending</a>
									</td>
									<td style = "margin-right:10px;"><a href="report.php?type=11" style = "width:220px;margin-right:10px;" class="btn btn-sm btn-primary">Orders: Pending Drawings</a>
									</td>
									<td><a href="report.php?type=12" style = "width:220px;margin-right:10px;" class="btn btn-sm btn-primary">Orders: Pending Estimates</a>
									</td>
									<td><a href="report.php?type=14" style = "width:220px;margin-right:10px;" class="btn btn-sm btn-primary">Orders: Pending Proposal Sent</a>
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td colspan = "4"><a href="report.php?type=16" style = "width:220px;" class="btn btn-sm btn-success">Orders: All Active</a>
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td><a href="report.php?type=3" style = "width:220px;" class="btn btn-sm btn-success">Orders: Active LL Approval</a>
									</td>
									<td><a href="report.php?type=4" style = "width:220px;" class="btn btn-sm btn-success">Orders: Active Permits</a>
									</td>
									<td><a href="report.php?type=9" style = "width:220px;" class="btn btn-sm btn-success">Orders: Active Survey</a>
									</td>
									<td><a href="report.php?type=5" style = "width:220px;" class="btn btn-sm btn-success">Orders: Active Manufacturing</a>
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td><a href="report.php?type=6" style = "width:220px;" class="btn btn-sm btn-success">Orders: Active Install</a>
									</td>
									<td><a href="report.php?type=15" style = "width:220px;" class="btn btn-sm btn-success">Orders: Service</a>
									</td>
									<td colspan = "2">&nbsp;
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td colspan = "4">&nbsp;
									</td>
								</tr>

								<tr>
									<td><a href="report.php?type=10" style = "width:220px;" class="btn btn-sm btn-warning">Orders: Ready to be turned in</a>
									</td>
									<td><a href="report.php?type=7" style = "width:220px;" class="btn btn-sm btn-warning">Orders: Turned in for billing</a>
									</td>
									<td><a href="report.php?type=8" style = "width:220px;" class="btn btn-sm btn-dark">Orders: Closed</a>
									</td>
									<td><a href="report.php?type=2" style = "width:220px;" class="btn btn-sm btn-dark">Orders: Did Not Sell</a>
									</td>
								</tr>
							</table>
								     
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

});
</script>

</body>
</html>