<?php
$jan=0;
$feb = 0;
$march = 0;
$april = 0;
$may = 0;
$june = 0;
$july = 0;
$aug = 0;
$sept = 0;
$oct = 0;
$nov = 0;
$dec = 0;

$start = strtotime($year.'-01-01');
$end = strtotime($year.'-12-31');

$janstart = $start;
$ld1 = date('t',strtotime($janstart));
$janend = date($year.'-01-'.$ld1);
$janend = strtotime($janend);

$febstart = strtotime($year.'-02-01');
$ld2 = date('t',strtotime($febstart));
$febend = date($year.'-02-'.$ld2);
$febend = strtotime($febend);

$marchstart = strtotime($year.'-03-01');
$ld3 = date('t',strtotime($marchstart));
$marchend = date($year.'-03-'.$ld3);
$marchend = strtotime($marchend);

$aprilstart = strtotime($year.'-04-01');
$ld4 = date('t',strtotime($aprilstart));
$aprilend = date($year.'-04-'.$ld4);
$aprilend = strtotime($aprilend);

$maystart = strtotime($year.'-05-01');
$ld5 = date('t',strtotime($maystart));
$mayend = date($year.'-05-'.$ld5);
$mayend = strtotime($mayend);

$junestart = strtotime($year.'-06-01');
$ld6 = date('t',strtotime($junestart));
$juneend = date($year.'-06-'.$ld6);
$juneend = strtotime($juneend);

$julystart = strtotime($year.'-07-01');
$ld7 = date('t',strtotime($julystart));
$julyend = date($year.'-07-'.$ld7);
$julyend = strtotime($julyend);

$augstart = strtotime($year.'-08-01');
$ld8 = date('t',strtotime($augstart));
$augend = date($year.'-08-'.$ld8);
$augend = strtotime($augend);

$septstart = strtotime($year.'-09-01');
$ld9 = date('t',strtotime($septstart));
$septend = date($year.'-09-'.$ld9);
$septend = strtotime($septend);

$octstart = strtotime($year.'-10-01');
$ld10 = date('t',strtotime($octstart));
$octend = date($year.'-10-'.$ld10);
$octend = strtotime($octend);

$novstart = strtotime($year.'-11-01');
$ld11 = date('t',strtotime($novstart));
$novend = date($year.'-11-'.$ld11);
$novend = strtotime($novend);

$decstart = strtotime($year.'-12-01');
$decend = $end;

$projects = $vujade->get_individual_yearly_sales_report($start,$end,$sp);

if($projects['error']=="0")
{
	unset($projects['error']);
	foreach($projects as $p)
	{
		$date_opened = strtotime($p['open_date']);
		if(($date_opened>=$janstart) && ($date_opened<=$janend))
		{
			$line = str_replace(',','',$p['line_total']);
			$jan+=$line;
		}
		if(($date_opened>=$febstart) && ($date_opened<=$febend))
		{
			$line = str_replace(',','',$p['line_total']);
			$feb+=$line;
		}
		if(($date_opened>=$marchstart) && ($date_opened<=$marchend))
		{
			$line = str_replace(',','',$p['line_total']);
			$march+=$line;
		}
		if(($date_opened>=$aprilstart) && ($date_opened<=$aprilend))
		{
			$line = str_replace(',','',$p['line_total']);
			$april+=$line;
		}
		if(($date_opened>=$maystart) && ($date_opened<=$mayend))
		{
			$line = str_replace(',','',$p['line_total']);
			$may+=$line;
		}
		if(($date_opened>=$junestart) && ($date_opened<=$juneend))
		{
			$line = str_replace(',','',$p['line_total']);
			$june+=$line;
		}
		if(($date_opened>=$julystart) && ($date_opened<=$julyend))
		{
			$line = str_replace(',','',$p['line_total']);
			$july+=$line;
		}
		if(($date_opened>=$augstart) && ($date_opened<=$augend))
		{
			$line = str_replace(',','',$p['line_total']);
			$aug+=$line;
		}
		if(($date_opened>=$septstart) && ($date_opened<=$septend))
		{
			$line = str_replace(',','',$p['line_total']);
			$sept+=$line;
		}
		if(($date_opened>=$octstart) && ($date_opened<=$octend))
		{
			$line = str_replace(',','',$p['line_total']);
			$oct+=$line;
		}
		if(($date_opened>=$novstart) && ($date_opened<=$novend))
		{
			$line = str_replace(',','',$p['line_total']);
			$nov+=$line;
		}
		if(($date_opened>=$decstart) && ($date_opened<=$decend))
		{
			$line = str_replace(',','',$p['line_total']);
			$dec+=$line;
		}
		$line=0;
	}
}
$total = $jan + $feb + $march + $april + $may + $june + $july + $aug + $sept + $oct + $nov + $dec;
?>

<!-- chart -->
<script type="text/javascript">
window.onload = function () 
{
	// pie chart colors
	CanvasJS.addColorSet("pie_chart_colors",
    [
    	"#0DAEE8",
		"#12FF2A",
		"#FFA60A"
    ]);

	// pie chart
	var chart = new CanvasJS.Chart("donut",
	{
		title:{
			text: "My Projects"
		},
		subtitles:[
		{
			text: "Your closed rate is <?php print @number_format($closed_rate,1); ?>%",
			verticalAlign: "bottom",
			fontSize:25
		}
		],
        animationEnabled: true,
		legend: {
			verticalAlign: "bottom",
			horizontalAlign: "center"
		},
		colorSet:  "pie_chart_colors",
		data: [
		{        
			type: "pie",
			indexLabelFontFamily: "Garamond",       
			indexLabelFontSize: 20,
			indexLabelFontWeight: "bold",
			startAngle:0,
			indexLabelFontColor: "MistyRose",       
			indexLabelLineColor: "darkgrey", 
			indexLabelPlacement: "inside", 
			toolTipContent: "{name}: {y}",
			showInLegend: true,
			indexLabel: "{y}", 
			dataPoints: [
				{  y: <?php print $pending_jobs; ?>, name: "Pending Jobs", legendMarkerType: "square"},
				{  y: <?php print $processed_jobs; ?>, name: "Processed Jobs", legendMarkerType: "square"}
				,
				{  y: <?php print $did_not_sell_jobs; ?>, name: "Did Not Sell", legendMarkerType: "square"}
			]
		}
		]
	});
	chart.render();

    var chart2 = new CanvasJS.Chart("chart",
    {
      title:{
        text: "Individual Yearly Sales for <?php print $sp.' - '.$year; ?>"
      },
      axisX:{
        interval: 1,
        intervalType: "month"
      },
      data: [
      {
        type: "line",
        dataPoints: [//array
        {
         x: new Date(<?php print $year; ?>, 00, 1),
         y: <?php print $jan; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}
       },
       {
         x: new Date(<?php print $year; ?>, 01, 1),
         y: <?php print $feb; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}
       }
       , {
         x: new Date(<?php print $year; ?>, 02, 1),
         y: <?php print $march; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       }, {
         x: new Date(<?php print $year; ?>, 03, 1),
         y: <?php print $april; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       }, {
         x: new Date(<?php print $year; ?>, 04, 1),
         y: <?php print $may; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       }, {
         x: new Date(<?php print $year; ?>, 05, 1),
         y: <?php print $june; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       }, {
         x: new Date(<?php print $year; ?>, 06, 1),
         y: <?php print $july; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       }, {
         x: new Date(<?php print $year; ?>, 07, 1),
         y: <?php print $aug; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       },{
         x: new Date(<?php print $year; ?>, 08, 1),
         y: <?php print $sept; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}

       },{
         x: new Date(<?php print $year; ?>, 09, 1),
         y: <?php print $oct; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}
       
       },{
         x: new Date(<?php print $year; ?>, 10, 1),
         y: <?php print $nov; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}
       
       },{
         x: new Date(<?php print $year; ?>, 11, 1),
         y: <?php print $dec; ?>,
         click: function(e){ window.location.href="report_individual_sales.php?sp=<?php print $emp['fullname']; ?>"}
       }
       ]
     }
     ]
    });

    chart2.render();

    // bar chart colors
    /* */
	CanvasJS.addColorSet("bar_chart_colors",
    [
    	"#18FF13",
		"#E8E106",
		"#FF5F04",
		"#FF090A"
    ]);

    // bar chart
    var chart3 = new CanvasJS.Chart("bar_chart",
	{
		animationEnabled: true,
		colorSet:  "bar_chart_colors",
		title:{
			text: "A/R Report"
		},
		data: [
		{
			type: "column", 
			dataPoints: [
				{ y: <?php print $gt1; ?>, label: "Current" },
				{ y: <?php print $gt2; ?>, label: "31 to 45 Days" },
				{ y: <?php print $gt3; ?>, label: "46 to 60 Days" },
				{ y: <?php print $gt4; ?>, label: "Over 60 Days" }
			]
		}
		]
	});

	chart3.render();
} 
</script>
<script type="text/javascript" src="vendor/plugins/canvas/canvasjs.min.js"></script>
<div id = "chart" name = "chart" style = "height:200px;margin-top:10px;">
</div>