<?php 

session_start();

define('SITE',1);

require_once('../library/class_library.php');

$vujade = new Vujade();

$vujade->connect();

$vujade->protect_page('login.php?m=2');

$project_id = $_REQUEST['project_id'];



# permissions

$permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');

if($permissions['read']!=1)

{

	$vujade->page_redirect('error.php?m=1');

}

if($permissions['edit']!=1)

{

	$vujade->page_redirect('error.php?m=1');

}

$documents_permissions = $vujade->get_permission($_SESSION['user_id'],'Documents');

if($documents_permissions['create']!=1)

{

	$vujade->page_redirect('error.php?m=1');

}



$project = $vujade->get_project($id,2);

if($project['error']!=0)

{

	$vujade->page_redirect('error.php?m=3');

}



$file_id = $_REQUEST['file_id'];

$doc = $vujade->get_document($file_id);

if($doc['error']!=0)

{

	$vujade->page_redirect('error.php?m=3');

}

$s = array();

$action = 0;

if(isset($_POST['action']))

{

	$action = $_POST['action'];

}

# update file type 

if($action==1)

{

	$new_type = $_POST['new_type'];

	$existing_type = $_POST['existing_type'];



	if(!empty($new_type))

	{

		# create the new type

		$vujade->create_row('document_types');

		$row_id = $vujade->row_id;

		$s[]=$vujade->update_row('document_types',$row_id,'name',$new_type);

		$s[]=$vujade->update_row('document_types',$row_id,'onetime',1);

		$s[]=$vujade->update_row('documents',$file_id,'file_type',$row_id);

	}



	# existing type was entered

	if( (empty($new_type)) && (!empty($existing_type)) )

	{

		$s[]=$vujade->update_row('documents',$file_id,'file_type',$existing_type);

	}



	# no type was entered

	if( (empty($new_type)) && (empty($existing_type)) )

	{

		$vujade->errors[]="No document type was selected or entered.";	

	}



	# check for errors

	$e = $vujade->get_error_count();

	if($e<=0)

	{

		# redirect

		$vujade->page_redirect('project_documents.php?id='.$project_id);

	}

}

if($action==2)

{

	$vujade->page_redirect('project_documents.php?id='.$project_id);

}

$nav = 0;

$title = "Document Type - ";

require_once('h.php');

?>
<section id="content_wrapper">
		<header id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        <?php print $title; ?>
                    </li>
                </ol>
            </div>
        </header>
        <section id="content" class="">

            <div class="theme-primary">

                <div class="panel heading-border panel-primary">

                    <div class="panel-body bg-light">
<?php
$vujade->show_errors();

$vujade->show_messages();

						?>
<form method = "post" action = "edit_document_type.php" id="np">

<input type = "hidden" id="action" name = "action" value = "1">

<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

<input type = "hidden" name = "file_id" value = "<?php print $file_id; ?>">
<div class="row">

    <div style="margin-bottom: 20px; overflow: hidden;">
        <div class="col-lg-6 col-sm-6 col-lg-offset-0 col-sm-offset-0">
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Document Type:</span>
                </div>
                <div class="col-lg-8">
                    <select name="existing_type" class="form-control">
                        <option value = "">-Select-</option>

						<?php

						# get existing folders

						$types = $vujade->get_document_types();

						if($types['error']=="0")

						{

							unset($types['error']);

							foreach($types as $t)

							{

								if($t['database_id']==$doc['file_type'])

								{

									print '<option value = "'.$t['database_id'].'" selected = "selected">'.$t['name'].'</option>';

								}

								else

								{

									print '<option value = "'.$t['database_id'].'">'.$t['name'].'</option>';

								}

							}

						}

						?>
                    </select>
                </div>
            </div>
        </div>
         <div class="col-lg-6 col-sm-6 col-lg-offset-0 col-sm-offset-0">
            <div class="row">
                <div class="col-lg-4">
                    <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">One-time Label:</span>
                </div>
                <div class="col-lg-8">
                    <input type="text" class="form-control" name = "new_type" id = "new_type">
                </div>
            </div>

        </div>
    </div>

    <div style="margin-top:15px;" class="col-sm-12">

            <div style = "float:left">

                <a href = "#" id = "back" class = "btn btn-lg btn-primary" style = "width:200px;">Cancel</a>

              </div>

              <div style = "float:right;">

                <a href = "#" id = "done" class = "btn btn-lg btn-primary" style = "width:200px;">Save</a>

              </div>

    </div>

</div>

</form>
</div>

                </div>


            </div>

        </section>

    </section>

<!-- jQuery -->
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>


    <!-- Theme Javascript -->
    <script src="assets/js/utility/utility.js"></script>
    <script src="assets/js/demo/demo.js"></script>
    <script src="assets/js/main.js"></script>
    <script type="text/javascript">

        jQuery(document).ready(function () {
            "use strict";

            // Init Theme Core
            Core.init();

            $('#back').click(function(e)
			{
				e.preventDefault();

				$('#action').val(2);

				$('#np').submit();

			});
            
            $('#done').click(function (e) {
                e.preventDefault();
                $('#action').val(1);
                $('#np').submit();
            });

        });

    </script>

</body>
</html>