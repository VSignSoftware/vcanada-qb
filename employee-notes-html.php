<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$title = "Employee Notes";
 
$logo = $vujade->get_logo();
if(!isset($charset))
{
	$charset = "utf-8";
} 

$employee = $vujade->get_employee($employee_id);

$employee['notes']=str_replace("\n",'<br>',$employee['notes']);
//$employee['notes']=str_replace('\r','<br>',$employee['notes']);

$pateHTML = '';
 
$pateHTML .= '
	<!DOCTYPE html>
	<head>
	<meta charset="'.$charset.'">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>'.$title.'</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
		.bordered {
			padding-bottom: 3px;
			border-bottom: 1px solid #cecece;
		}
		
		.header {
			font-weight: bold;
			font-size: 14px;
		}
		
		.header_graybg {
			background-color: #cecece;
			font-weight: bold;
			font-size: 14px;
			padding-bottom: 3px;
			border-bottom: 1px solid #cecece;
		}
		
		.button-container-style{
			margin-top: 20px; 
			padding-top:10px; 
			border-top: 1px #ccc solid;
		}
	</style>
		
</head>
<body>

	<div id="mainContent">

		<div id="content">
			<br style="clear: both;" />
			<h2 class="signInText">
				'.$title.'
			</h2>
			
			<div style="background-color: #989898; width: 100%">
				<div style="padding: 5px;">
					Employee Notes for '.$employee['fullname'].'
				</div>
			</div>

			<div style = "width: 100%;padding:5px;border:1px solid black;">'.$employee['notes'].'</div>';
		 
$pateHTML .= '	
			<div class="button-container-style">&nbsp;</div>	
			</div>
		</div>

	</body>
</html>';