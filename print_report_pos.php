<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

if(isset($_REQUEST['option']))
{
	$option=$_REQUEST['option'];
	if(!in_array($option, array(1,2,3)))
	{
		$option=1;
		$option_text='Day';
	}
	else
	{
		if($option==1)
		{
			$option_text='Day';
		}
		if($option==2)
		{
			$option_text='Week';
		}
		if($option==3)
		{
			$option_text='Month';
		}
	}
}
else
{
	$option=1;
	$option_text='Day';
}

$pos = $vujade->get_all_blank_purchase_orders($option);
if($pos['error']=="0")
{

	$title = 'Purchase Orders';
	$html = '<html><head><title>Purchase Orders</title>';
	$html.='<style>';
	$html.='
	body
	{
		font-size:12px;
		font-family:arial;
		color:black;
	}
	table 
	{
	    border-collapse: collapse;
	}
	table, td, th 
	{
	    border: 1px solid #cecece;
	}
	td
	{

	}
	';
	$html.='</style>';
	$html.='</head><body>';
	$header_1='<h2>Purchase Orders Created in the Past '.$option_text.'</h2>';

	unset($pos['error']);
	$html.='
	<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
			<thead>
				<tr style = "border-bottom:1px solid black;">
					<td valign = "top"><strong>Date</strong></td>
					<td valign = "top"><strong>PO#</strong></td>
					<td valign = "top"><strong>Vendor</strong></td>
					<td valign = "top"><strong>Items</strong></td>
					<td valign = "top"><strong>Jobs</strong></td>
					<td valign = "top"><strong>Cost</strong></td>
				</tr>
			</thead>

    <tbody style = "font-size:14px;">';
	foreach($pos as $i)
	{
		$link='';
        
        $html.= '<tr class = "clickableRow-row">';

        // date
        $html.= '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><a href = "'.$link.'">'.$i['date'].'</a></td>';

        // po id
        $html.= '<td valign = "top" class="clickableRow" href="'.$link.'" style = ""><a href = "'.$link.'">'.$i['purchase_order_id'].'</a></td>';

        // vendor
        //$vendor = $vujade->get_vendor($i['vendor_id'],'ListID');
        $html.= '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['vendor_name'].'</td>';

        // items
        $items = $vujade->get_materials_for_purchase_order($i['database_id']);
        $html.= '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';

        if($items['error']=="0")
        {
        	unset($items['error']);
        	$project_ids = array();
        	foreach($items as $item)
        	{
        		$project_ids[]=$item['project_id'];
        		$html.= $item['description'].'<br>';
        	}
        }

        $html.= '</td>';

        // jobs 
        $html.= '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
        if(count($project_ids)>0)
        {
        	foreach($project_ids as $pid)
        	{
        		$html.= $pid.'<br>';
        	}
        }
        $html.= '</td>';

        // cost
        $html.= '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
        $html.= $i['total'];
        $html.= '</td>';

		$html.= '</tr>';
	}
	$html.="</tbody></table></body></html>";

	$footer_1 = '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';

	# mpdf class (pdf output)
	include("mpdf60/mpdf.php");
	$mpdf = new mPDF('', 'LETTER-L', 0, 'Helvetica', 10, 10, 20, 10, 10);
	$mpdf->DefHTMLHeaderByName('header_1',$header_1);
	$mpdf->SetHTMLHeaderByName('header_1');
	$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
	$mpdf->SetHTMLFooterByName('footer_1');
	$mpdf->WriteHTML($html);

	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-purchase-orders.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
	 	$mpdf->Output($pdfname,'F');
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		$mpdf->Output('Purchase Orders.pdf','I');  
	}
}
else
{
	$vujade->set_error('No purchase orders found. ');
	$vujade->set_error($pos['error']);
	$vujade->show_errors();
}
?>
