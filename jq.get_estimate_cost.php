<?php
session_start();
print '<meta charset="ISO-8859-1">';
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$estimate_id = $_POST['estimate_id'];
$tfc = 0;
$total = 0;
if(isset($_REQUEST['total']))
{
	$total=$_REQUEST['total'];
}
$estimate = $vujade->get_estimate($estimate_id);
if($estimate['error']!="0")
{
	//$estimate['estimate_id']=$estimate_id;
}
# materials cost
$materials_cost = 0;

# buyout overhead
$setup = $vujade->get_setup();
if($setup['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

/*
$buyout_overhead_percentage=$estimate['overhead_buyout_rate'];
$general_overhead_percentage=$estimate['overhead_general_rate'];

$items = $vujade->get_materials_for_estimate($estimate['database_id']);
if($items['error']=="0")
{
	unset($items['error']);
	foreach($items as $i)
	{
		$line = $i['cost']*$i['qty'];
		$materials_cost = $materials_cost+$line;
	}
}

# labor cost
$labor_total = 0;
$labor = $vujade->get_labor_for_estimate($estimate['estimate_id'],1);
if($labor['error']=="0")
{
	unset($labor['error']);
	foreach($labor as $l)
	{
		$llt=$l['rate']*$l['hours'];
		$labor_total+=$llt;
		unset($llt);
	}
}

# buyouts
$buyout_cost = 0;
$buyouts = $vujade->get_buyouts($estimate['database_id']);
if($buyouts['error']=="0")
{
	unset($buyouts['error']);
	foreach($buyouts as $b)
	{
		$buyout_cost=$buyout_cost+$b['cost'];
	}
}

$indet = $materials_cost*$estimate['indeterminant'];
$go = $materials_cost + $indet + $labor_total;
$go = $go * $general_overhead_percentage;
$bo = $buyout_cost*$buyout_overhead_percentage;
$tfc = $materials_cost+$labor_total+$indet+$buyout_cost+$bo+$go;
$fc = $tfc;

print $materials_cost.'<br>';
print $indet.'<br>';
print $labor_total.'<br>';
print $buyout_cost.'<br>';
print $go.'<br>';
print $bo.'<br>';
*/

$fc = $_REQUEST['fc'];
$tfc = number_format($fc,2,'.',',');
if($total==0)
{
	print $tfc;
}
if($total==1)
{
	?>

	<table width = "100%" class = "table table-hover table-striped  table-bordered">
	<tr>
		<td>Labor Cost: </td>
		<td><strong>$<?php print number_format($labor_total,2,'.',','); ?></strong></td>
	</tr>
	<tr>
		<td>Materials:</td>
		<td><strong>$<?php print number_format($materials_cost,2,'.',','); ?></strong></td>
	</tr>
	<tr>
		<td>Buyouts:</td>
		<td><strong>$<?php print number_format($buyout_cost,2,'.',','); ?></strong></td>
	</tr>
	<tr>
		<td>Subtotal:</td>
		<td><strong>
			<?php
			$st = $buyout_cost+$materials_cost+$labor_total;
			print '$'.number_format($st,2,'.',',');
			?>
		</strong></td>
	</tr>

	<tr>
		<td>&nbsp;</td>
		<td>&nbsp;</td>
	</tr>

	<tr>
		<td>General Overhead: </td>
		<td><strong><?php print '$'.number_format($go,2,'.',','); ?></strong></td>
	</tr>
	<tr>
		<td>Buyout Overhead:</td>
		<td><strong>
			<?php
			print '$'. number_format($bo,2,'.',',');
			?>
		</strong></td>
	</tr>
	</table>

	<?php
}
if($total==2)
{
	$tier = $_REQUEST['tier'];
	if( ($tier==0) || ($tier=="") )
	{
		$tier=1;
	}
	$markups = $vujade->get_markups($tier,$sort=1);
	if($markups['error']=='0')
	{
		unset($markups['error']);
		print '<table class = "table table-hover table-striped table-bordered">';
		print '<tr class = "primary">';
		print '<th>';
		print 'M/U';
		print '</th>';
		print '<th>';
		print 'Base Sale';
		print '</th>';
		print '<th>';
		print 'Comm %';
		print '</td>';
		print '<th>';
		print 'Commission';
		print '</th>';
		print '<th>';
		print 'Total Sale';
		print '</th>';
		print '<th>';
		print 'Profit';
		print '</th>';
		print '</tr>';
		foreach($markups as $markup)
		{
			print '<tr>';
			print '<td>';
			print number_format($markup['markup'],2,'.',',');
			print '</td>';
			print '<td>';
			$bs = $markup['markup']*$fc;
			//$bs = round($bs,2);
			//print $fc.'<br>';
			print '$'.number_format($bs,2,'.',',');
			print '</td>';
			print '<td>';
			print $markup['commission_percent'].'%';
			print '</td>';
			print '<td>';
			$commission = $markup['commission_percent']*$bs;
			//$commission=round($commission,2);
			print '$'.number_format($commission,2,'.',',');
			print '</td>';
			print '<td>';
			$ts = $bs+$commission;
			print '$'.number_format($ts,2,'.',',');
			print '</td>';
			print '<td>';
			$profit = $bs-$fc;
			print '$'.number_format($profit,2,'.',',');
			print '</td>';
			print '</tr>';
		}
		print '</table>';
	}
}
if($total==3)
{
	$fc=$_REQUEST['fc'];
	$tier = $_REQUEST['tier'];
	if( ($tier==0) || ($tier=="") )
	{
		$tier=1;
	}
	$markups = $vujade->get_markups($tier,$sort=1);
	if($markups['error']=='0')
	{
		unset($markups['error']);
		print '<table class = "table table-hover table-striped table-bordered">';
		print '<tr class = "primary">';
		print '<th>';
		print 'M/U';
		print '</th>';
		print '<th>';
		print 'Base Sale';
		print '</th>';
		print '<th>';
		print 'Comm %';
		print '</td>';
		print '<th>';
		print 'Commission';
		print '</th>';
		print '<th>';
		print 'Total Sale';
		print '</th>';
		print '<th>';
		print 'Profit';
		print '</th>';
		print '</tr>';
		foreach($markups as $markup)
		{
			print '<tr>';
			print '<td>';
			print number_format($markup['markup'],2,'.',',');
			print '</td>';
			print '<td>';
			$bs = $markup['markup']*$fc;
			//$bs = round($bs,2);
			//print $fc.'<br>';
			print '$'.number_format($bs,2,'.',',');
			print '</td>';
			print '<td>';
			print $markup['commission_percent'].'%';
			print '</td>';
			print '<td>';
			$commission = $markup['commission_percent']*$bs;
			//$commission=round($commission,2);
			print '$'.number_format($commission,2,'.',',');
			print '</td>';
			print '<td>';
			$ts = $bs+$commission;
			print '$'.number_format($ts,2,'.',',');
			print '</td>';
			print '<td>';
			$profit = $bs-$fc;
			print '$'.number_format($profit,2,'.',',');
			print '</td>';
			print '</tr>';
		}
		print '</table>';
	}
}
?>