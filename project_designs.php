<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$designs_permissions = $vujade->get_permission($_SESSION['user_id'],'Designs');
if($designs_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);

if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

# active tab
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
	if(!in_array($tab, array(0,1,2)))
	{
		$tab=0;
	}
}
else
{
	$tab=0;
}

$show_designs=0; // default (project does not have designs)
$designs = $vujade->get_designs_for_project($id);
if($designs['error']=='0')
{
	unset($designs['error']);
	unset($designs['next_id']);
	$show_designs=1; // has at least one task
	$designids = array();
	foreach($designs as $design)
	{
		$designids[]=$design['database_id'];
	}
	end($designids);
	$fdid=key($designids);
	$firstdesign = $vujade->get_design($designids[$fdid]);
	$designid = $designids[$fdid];
}

if(isset($_REQUEST['designid']))
{
	$firstdesign = $vujade->get_design($_REQUEST['designid']);
	$designid = $_REQUEST['designid'];
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# make printable copy of design
if($action==1)
{
	// deprecated... delete
	//$vujade->messages[]='Print button clicked. Functionality not yet developed for this action.';	
}

# delete design
if($action==2)
{
	$did = $_REQUEST['designid'];
	$s = $vujade->delete_row('designs',$did);
	$show_designs=0; // default (project does not have designs)
	$designs = $vujade->get_designs_for_project($id);
	if($designs['error']=='0')
	{
		unset($designs['error']);
		unset($designs['next_id']);
		$show_designs=1; // has at least one task
		$designids = array();
		foreach($designs as $design)
		{
			$designids[]=$design['database_id'];
		}
		$firstdesign = $vujade->get_design($designids[0]);
		$designid = $designids[0];
	}
	else
	{
		# there are no designs
		$show_designs=0;
		$firstdesign['error']=1;
		$designid = 0;
	}
}

# delete a design file
if($action==3)
{
	$fid = $_REQUEST['fid'];
	$did = $_REQUEST['designid'];

	$filedata = $vujade->get_design_file($fid);
	unlink('uploads/design_files/'.$id.'/'.$filedata['file_name']);
	$s = $vujade->delete_row('files',$fid);

	$show_designs=0; // default (project does not have designs)
	$designs = $vujade->get_designs_for_project($id);
	if($designs['error']=='0')
	{
		unset($designs['error']);
		unset($designs['next_id']);
		$show_designs=1; // has at least one task
		$designids = array();
		foreach($designs as $design)
		{
			$designids[]=$design['database_id'];
		}
		$firstdesign = $vujade->get_design($designids[0]);
		$designid = $designids[0];
	}
	$tab=2;
}

$shop_order = $vujade->get_shop_order($id, 'project_id');

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$setup = $vujade->get_setup(1);
if($setup['design_email_message']=="")
{
	$default_msg = "Please find attached design for your review and processing.";
}
else
{
	$default_msg = $setup['design_email_message'];
}

if(empty($designid))
{
	$designid="0";
}

$section=3;
$menu=3;
$title = 'Designs - ' . $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">

<!-- begin: .tray-left -->
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left -->

<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;">

<div class="pl15 pr15" style = "width:100%;">

<?php require_once('project_right_tray.php'); ?>

<!-- main content for this page -->
<div class = "row">
<div class = "col-md-2" style = "width:200px;">
<div class="panel panel-primary panel-border top">
<div class="panel-heading">
<span class="panel-title">Designs</span>
<div class="widget-menu pull-right">
<?php
if( ($project['status']!="Closed") && ($designs_permissions['create']==1) )
{
?>
	<a href = "new_design.php?project_id=<?php print $id; ?>" class = "btn btn-primary btn-sm">New</a>
<?php } ?>
</div>
</div>
<div class="panel-body">
<?php
if($show_designs==1)
{
	print '<table>';
	unset($designs['error']);
	unset($designs['next_id']);
	foreach($designs as $design)
	{
		if($designid==$design['database_id'])
		{
			$bgcolor = "cecece";
		}
		else
		{
			$bgcolor = "ffffff";
		}
		print '<tr bgcolor = "'.$bgcolor.'">';
		print '<td valign = "top">';

		$n=strtotime('now');
		$fda = $n-86400;
		$rd = strtotime($design['request_date']);
		if($rd<$fda)
		{
			$color = "#f7584c";
		}
		else
		{
			$color="#288BC3";
		}
		print '<a class = "linknostyle" href = "project_designs.php?id='.$id.'&designid='.$design['database_id'].'">';
		print '<font color = "'.$color.'">'.$design['request_date'].'</font>';
		print '</a>';
		print '</td>';
		print '<td valign = "top">&nbsp;</td>';
		print '<td valign = "top">';
		print '<a class = "linknostyle" href = "project_designs.php?id='.$id.'&designid='.$design['database_id'].'">';
		print $design['design_id'];
		print '</a>';
		print '</td>';
		print '</tr>';
	}
	print '</table>';
}
?>
</div>
</div>
</div>

<div class = "col-md-9" style = "margin-left:5px;border:0px solid red;padding:0px;">
<?php
if($show_designs==1)
{
?>
	<div class="panel panel-primary panel-border top">
	<div class="panel-heading">
	<span class="panel-title">Selected Design</span>
	<div class="widget-menu pull-right">

	<?php
	if( ($project['status']!="Closed") && ($designs_permissions['edit']==1) )
	{
	?>
		<a class = "btn btn-xs btn-success" href = "edit_design.php?id=<?php print $id; ?>&designid=<?php print $designid; ?>" title = "Edit Design">Edit</a> 
	<?php
	}
	?>

	<a href = "#email-design-form" class = "btn btn-primary btn-xs" id = "email-design" style = "">Email</a>

	<!-- modal content -->
	<div id = "email-design-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:450px;">
		<form id = "email-form">
		<table class = "" style = "">
		<tr class = "">
		<td class = "">Send To</td>
		<td class = "">
		<input type = "text" name = "send_to" id = "send_to" style = "width:300px;margin-left:5px;" value = "<?php print $project['project_contact_email']; ?>" class = "form-control">
		</td>
		</tr>

		<tr id = "error_1" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "">Subject</td>
		<td class = "">
		<input type = "text" name = "subject" id = "subject" value = "<?php print $design['project_id']; ?> Design" style = "width:300px;margin-left:5px;" class = "form-control">
		</td>
		</tr>

		<tr id = "error_2" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "" valign="top"><span style = "margin-top:5px;">Message</span></td>
		<td class = "">
		<textarea name = "msg" id = "msg" style = "width:300px;height:100px;margin-left:5px;" class = "">
		<?php print $default_msg; ?>
		</textarea>
		</td>
		</tr>

		<tr id = "error_3" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "">&nbsp;</td>
		<td class = "">
		<div id = "working"></div>

		<a id = "send" class = "btn btn-lg btn-success">SEND</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
		</td>
		</tr>
		</table>
		</form>
	</div>

<a class = "btn btn-xs btn-primary" target = "_blank" href = "print_design.php?id=<?php print $id; ?>&designid=<?php print $designid; ?>" title = "Print Design">Print</a> 

<?php
if( ($project['status']!="Closed") && ($designs_permissions['edit']==1) )
{
?>

	<a class = "btn btn-xs btn-danger" href = "#delete-design-form" title = "Delete Design" id = "delete-design">Delete</a> 

	<!-- modal content -->
	<div id="delete-design-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Design</h1>
	<p>Are you sure you want to delete this design?</p>
	<p><a id = "" class="btn btn-lg btn-danger" href="project_designs.php?id=<?php print $id; ?>&designid=<?php print $designid; ?>&action=2">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
	</div>

<?php } ?>

</div>
</div>
<div class="panel-body">

<?php
if($show_designs==1)
{
	if( ($firstdesign['error']=='0') || ($firstdesign['error']=='') )
	{
		?>
		<div style = "padding:5px;">
		<table width = "100%">
			<tr>
				<td valign = "top" width = "25%">Number:
				</td>
				<td valign = "top"><strong>
				<?php print $firstdesign['design_id']; ?></strong>
				</td>

				<td valign = "top">Type:
				</td>

				<td valign = "top"><strong><?php print $firstdesign['type']; ?></strong>
				</td>

			</tr>

			<tr>
				<td colspan = "4">&nbsp; 
				</td>
			</tr>

			<tr>
				<td valign = "top">Assigned to:
				</td>
				<td valign = "top"><strong>
				<?php 
				$employee_id = $firstdesign['employee_id'];
				$employee=$vujade->get_employee($employee_id,2);
				if($employee['error']=="0")
				{
					print $employee['fullname'];
				}
				else
				{
					# error or none
				}
				?></strong>
				</td>

				<td valign = "top">Date Required:
				</td>

				<td valign = "top"><strong><?php print $firstdesign['request_required_date']; ?></strong>
				</td>

			</tr>

			<tr>
				<td colspan = "4">&nbsp; 
				</td>
			</tr>

			<tr>
				<td valign = "top">Priority:
				</td>
				<td valign = "top"><strong>
				<?php print $firstdesign['priority']; ?></strong>
				</td>

				<td valign = "top">Format:
				</td>

				<td valign = "top"><strong><?php print $firstdesign['type_sub']; ?></strong>
				</td>

			</tr>

			<tr>
				<td colspan = "4">&nbsp; 
				</td>
			</tr>

			<tr>
				<td valign = "top" colspan = "2">Status: </td>
				<td valign = "top" colspan = "1">Hours: </td>
				<td valign = "top" colspan = "1"><?php print $firstdesign['hours']; ?></td>
			</tr>
			<tr>
				<td valign = "top" colspan = "4">
				<?php 
				if($firstdesign['status']=="Submitted")
				{
					?>
					<div class = "alert alert-micro alert-primary">
						<?php print $firstdesign['status']; ?>
					</div>
					<?php
				}
				if($firstdesign['status']=="In Progress")
				{
					?>
					<div class = "alert alert-micro alert-success">
						<?php print $firstdesign['status']; ?>
					</div>
					<?php
				}
				if($firstdesign['status']=="Complete")
				{
					?>
					<div class = "alert alert-micro alert-dark" style = "background-color:black;">
						<?php print $firstdesign['status']; ?>
					</div>
					<?php
				}
				if($firstdesign['status']=="On Hold")
				{
					?>
					<div class = "alert alert-micro light" style = "background-color:white;color:black;">
						<?php print $firstdesign['status']; ?>
					</div>
					<?php
				}
				?>
				</td>
			</tr>

			<tr>
				<td colspan = "4">&nbsp; 
				</td>
			</tr>
		</table>

		<div class="panel" style = "margin-top:15px;">
		  <div class="panel-heading">
		  	Files 
		  	<span class = "pull-right">
		  		<a class = "btn btn-xs btn-primary" href = "new_design_file.php?project_id=<?php print $id; ?>&designid=<?php print $designid; ?>" title = "Add a new design file" style = "">+</a>
		  	</span>
		  </div>
		  <div class="panel-body">
	  		<div class="row">
	          <div class="col-md-12">
	            <table width = "100%" class = "table">
					<?php
					# get list of files associated with this design
					if(!empty($firstdesign['design_id']))
					{
						$design_files = $vujade->get_design_files($firstdesign['design_id']);
						unset($design_files['sql']);
						unset($design_files['param']);
						if($design_files['error']=="0")
						{
							unset($design_files['error']);
							foreach($design_files as $df)
							{
								print '<tr>';
								print '<td>';
								//print '<div style = "font-weight:bold;margin-top:5px;margin-bottom:5px;border-bottom:1px solid #cecece;width:100%;float:left;padding-bottom:10px;">';
								print '<a href = "download_design_file.php?file_id='.$df['database_id'].'&project_id='.$id.'" target="_blank">';
								print $df['file_name'];
								print '</a>';
								//print '</div>';
								print '</td>';
								print '<td><span class = "pull-right">';
								if($project['status']!="Closed")
								{
									if($designs_permissions['delete']==1)
									{
										$ddfr = mt_rand();
										print '<a href = "#'.$ddfr.'-delete-design-file-modal" class = "ddf btn btn-xs btn-danger" id = "delete-design-file" style = "padding-left:5px;padding-right:5px;">-</a>';
										print '</a></span>';

										?>
										<!-- modal content -->
										<div id="<?php print $ddfr; ?>-delete-design-file-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
										<h1>Delete Design File</h1>
										<p>Are you sure you want to delete this design file?</p>
										<p><a id = "" class="btn btn-lg btn-danger" href="project_designs.php?action=3&fid=<?php print $df['database_id']; ?>&designid=<?php print $designid; ?>&id=<?php print $id; ?>">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
										</div>
										<?php

									}
								}
								print '</td>';
								print '</tr>';
							}
						}
					}
					?>

				</table>
	          </div>
	        </div>
		  </div>
		</div>

		<div class="panel" style = "margin-top:15px;">
		  <div class="panel-heading">
		  	Description
		  </div>
		  <div class="panel-body">
	  		<div class="row">
	          <div class="col-md-12" style = "max-width:580px;overflow:auto;">
	          	<?php print $firstdesign['request_description']; ?>
	          </div>
	        </div>
		  </div>
		</div>

		</div>
		</div>
		<?php
	}
}
?>

</div>
</div>
<?php } ?>	
</div>

</section>
<!-- End: Content -->

</section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">

  var designid = <?php print $designid; ?>;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // modals
    $('#email-design').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#email-design-form',
		modal: true
	});

    $('#delete-design').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-design-form',
		modal: true
	});

	$('.ddf').magnificPopup({
		type: 'inline',
		preloader: false,
		//focus: '#delete-design-file-modal',
		modal: true
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	// notes remove tabs on first line
    var n = $('#notes').html();
    $("#notes").html($.trim(n));

    // email design popup functions
	var msg = $('#msg').html();
    $("#msg").html($.trim(msg));

    <?php
    if(!empty($designid))
    {
    	?>
	    $('#send').click(function(e)
	    {
	    	e.preventDefault();
		
	    	$('#error_1').hide();
	    	$('#error_2').hide();
	    	$('#error_3').hide();

			// send to, subject and message must not be blank
			var send_to = $('#send_to').val();
			var subject = $('#subject').val();
			var message = $('#msg').val();
			var error = 0;
			if(send_to=="")
			{	
				//$('#error_1').css('background-color','#C60F13');
				//$('#error_1').css('color','white');
				//$('#error_1').css('font-weight','bold');
				//$('#error_1').css('font-size','16px');
				//$('#error_1').css('padding','3px');
				//$('#error_1').css('width','200px');
				//$('#error_1').css('float','left');
				$('#error_1').show();
				error++;
			}
			else
			{
				$('#error_1').hide();
			}
			if(subject=="")
			{
				//$('#error_2').css('background-color','#C60F13');
				//$('#error_2').css('color','white');
				//$('#error_2').css('font-weight','bold');
				//$('#error_2').css('font-size','16px');
				//$('#error_2').css('padding','3px');
				$('#error_2').show();
				error++;
			}
			else
			{
				$('#error_2').hide();
			}
			if(message=="")
			{
				//$('#error_3').css('background-color','#C60F13');
				//$('#error_3').css('color','white');
				//$('#error_3').css('font-weight','bold');
				//$('#error_3').css('font-size','16px');
				//$('#error_3').css('padding','3px');
				$('#error_3').show();
				error++;
			}
			else
			{
				$('#error_3').hide();
			}

			if(error==0)
			{
				//$('#send').hide();
				$('#working').css('margin-bottom','5px');
				$('#working').css('padding','3px');
				$('#working').css('background-color','#E6A728');
				//$('#working').css('background-color','#36D86C');
				$('#working').css('color','white');
				$('#working').css('font-weight','bold');
				$('#working').css('font-size','16px');
				$('#working').html('Working...');
				$.post( "jq.send.php", { id: <?php print $designid; ?>, type: "design", send_to: send_to, subject: subject, message: message })
				.done(function( response ) 
				{
				    $('#working').css('background-color','#36D86C');
				    $('#working').html('');
				    $('#working').html(response);

				    // set time out
				    setTimeout(function(){ $.magnificPopup.close(); }, 2000);

				    // close modal


				});
			}
		});
	<?php } ?>
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>