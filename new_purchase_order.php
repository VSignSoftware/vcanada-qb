<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$po_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($po_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

if($_REQUEST['is_blank']==1)
{
	$is_blank=1;
}
else
{
	$project_id = $_REQUEST['project_id'];
	$id = $project_id;

	$project = $vujade->get_project($project_id,2);
	if($project['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$shop_order = $vujade->get_shop_order($id, 'project_id');
	$is_blank=0;
}

// next available purchase order id
$max = $vujade->get_next_purchase_order_id();
if($max==0)
{
	$poid = 10001;
}
else
{
	$poid = $max+1;
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# save 
if($action==1)
{
	$id=$_POST['id'];
	$vendor_id=$_POST['vendor_id'];
	$vendor_contact_id=$_POST['vendor_contact_id'];
	$date=$_POST['date'];
	$revised=$_POST['revised'];
	$type=$_POST['type'];

	$company=$_POST['company'];
	$address_1=$_POST['address_1'];
	$address_2=$_POST['address_2'];
	$city=$_POST['city'];
	$state=$_POST['state'];
	$zip=$_POST['zip'];

	$vujade->create_row('quickbooks_purchaseorder');
	$row_id = $vujade->row_id;
	$s = array();

	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'RefNumber',$poid,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'Other1',$id,'ID');

	$tempvendor = $vujade->get_vendor($vendor_id);
	$vendor_list_id = $tempvendor['qb_id'];
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorRef_ListID',$vendor_list_id,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorRef_FullName',$tempvendor['name'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorAddress_Addr1',$tempvendor['address_1'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorAddress_Addr2',$tempvendor['address_2'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorAddress_City',$tempvendor['city'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorAddress_State',$tempvendor['state'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorAddress_PostalCode',$tempvendor['zip'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'VendorAddress_Country',$tempvendor['country'],'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'vendor_contact_id',$vendor_contact_id,'ID');
	
	// qb date format
	$date = strtotime($date);
	$date = date('Y-m-d',$date);
	if($date=="1969-12-31")
	{
		$date = date('Y-m-d');
	}

	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'DueDate',$date,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'TxnDate',$date,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'date_revised',$revised,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'type',$type,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ordered_by',$emp['first_name'].' '.$emp['last_name'],'ID');
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'TxnID',$fakeid,'ID');

	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ShipAddress_Addr1',$company,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ShipAddress_Addr2',$address_1,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ShipAddress_Addr3',$address_2,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ShipAddress_City',$city,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ShipAddress_State',$state,'ID');
	$s[]=$vujade->update_row('quickbooks_purchaseorder',$row_id,'ShipAddress_PostalCode',$zip,'ID');

	# duplicate copy for costing
	$vujade->create_row('costing_purchase_orders');
	$row_id2 = $vujade->row_id;
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id2,'project_id',$id);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id2,'purchase_order_id',$poid);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id2,'date',$date);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id2,'type',$type);
	$s[]=$vujade->update_row('costing_purchase_orders',$row_id2,'vendor',$vendor_id);

	if($type=="Materials")
	{
		$vujade->page_redirect('new_purchase_order_materials.php?project_id='.$id.'&poid='.$row_id.'&is_blank='.$is_blank);
	}
	else
	{
		$vujade->page_redirect('new_purchase_order_outsource.php?project_id='.$id.'&poid='.$row_id.'&is_blank='.$is_blank);
	}
}
$company_info = $vujade->get_invoice_company_setup(2);
if($company_info['error']!="0")
{
	unset($company_info);
	$vujade->create_row('invoice_company_setup');
	$company_info=$vujade->get_invoice_company_setup($vujade->row_id);
}

$menu = 8;
$section = 3;
$title = "New Purchase Order - ".$poid.' - ';
//$charset='<meta charset="ISO-8859-1">';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">PO Setup</a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		
		<form method = "post" action = "new_purchase_order.php" id = "form">
		<input type = "hidden" name = "id" value = "<?php print $id; ?>">
		<input type = "hidden" name = "action" value = "1">
		<input type = "hidden" name = "is_blank" value = "<?php print $is_blank; ?>">

		<table width="100%">

			<tr>
				<td>
					<select name = "type" id = "type" class = "form-control" style = "width:200px;float:left;">
						<option value = "">-Select Type-</option>
						<option value = "Materials">Materials</option>
						<option value = "Outsource">Outsource</option>
						<option value = "Subcontract">Subcontract</option>
					</select>
				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
			</tr>
			
			<tr>
				<td>
					<div class = "well">
						<div class = "row">
							<div class = "col-md-7">
								<div class = "row">
									<div class = "col-md-3">
										<strong>Select Vendor:</strong> 
									</div>

									<div class = "col-md-5">
										<input type = "text" name = "search" id = "search" placeholder = "Search by Name or ID" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Search by Name or ID'" class = "form-control"> 
									</div>

									<div class = "col-md-4" style = "">
										<input type = "submit" name = "searchbtn" id = "searchbtn" value = "Search" class = "btn btn-success btn-sm"> 
										<input type = "submit" name = "reset" id = "reset" value = "Reset" class = "btn btn-primary btn-sm">
									</div>

								</div>

								<input type = "hidden" name = "vendor_id" id = "vendor_id">

								<div style = "height:150px;overflow:auto;" id = "vendor_list">
									<?php
									$vendors = $vujade->get_vendors(1,0,1);
									if($vendors['error']=="0")
									{
										unset($vendors['error']);
										print '<table class = "table">';
										print '<tr><td width = "15%"><strong>ID</strong></td><td width = "60%"><strong>Name</strong></td><td width = "25%"><strong>Rating</strong></td></tr>';
										foreach($vendors as $vendor)
										{
											print '<tr class = "clickableRow" id = "'.$vendor['database_id'].'">';

											print '<td valign = "top">';
											print $vendor['vendor_id'];
											print '</td>';
											
											print '<td valign = "top">';
											print $vendor['name'];
											print '</td>';

											print '<td valign = "top">';
											print $vendor['rating'];
											print '</td>';

											print '</tr>';
										}
										print '</table>';
									}
									?>
								</div>
							</div>

							<div class = "col-md-5">
								<div class="panel panel-primary">
								  <div class="panel-heading">
								    <span class="panel-title">To:</span>
								    <div class="widget-menu pull-right">
								    </div>
								  </div>
								  <div class="panel-body">
								  	<div id = "vendor_info">
									<em>Choose or search for a vendor from the list on the left.</em>
									</div>
								  </div>
								</div>
							</div>
						</div>
					</div>
				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td>
					<div class="panel panel-primary">
					  <div class="panel-heading">
					    <span class="panel-title">Ship To: </span>
					    <div class="widget-menu pull-right">
					    </div>
					  </div>
					  <div class="panel-body">
					    	<input type = "text" placeholder = "Company Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Company Name'" name = "company" id = "company" style = "width:410px;" value = "" class = "js_replace form-control">

						<br>

						<input type = "text" style = "width:410px;" name = "address_1" id = "address_1" value = "" placeholder = "Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Address'" class = "js_replace form-control"> 

						<br>

						<input type = "text" name = "city" id = "city" value = "" placeholder = "City" onfocus="this.placeholder = ''" onblur="this.placeholder = 'City'" class = "js_replace form-control" style = "width:200px;float:left;margin-right:5px;"> 

						<input type = "text" name = "state" id = "state" value = "" placeholder = "State" onfocus="this.placeholder = ''" onblur="this.placeholder = 'State'" class = "js_replace form-control" style = "width:100px;float:left;margin-right:5px;"> 

						<input type = "text" name = "zip" id = "zip" value = "" placeholder = "Zip" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Zip'" class = "js_replace form-control" style = "width:100px;float:left;margin-right:5px;"> 
					  </div>
					</div>
				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td>
					<div class = "well">

						<table width = "100%">
							<tr>
								<td>PO No.:
								</td>
								<td><?php print $poid; ?>
								</td>
							</tr>

							<tr><td>&nbsp;</td></tr>

							<tr>
								<td>Date:
								</td>
								<td><input type = "text" name = "date" class = "dp form-control" style = "width:200px;">
								</td>
							</tr>

							<tr><td>&nbsp;</td></tr>

							<tr>
								<td>Revised: 
								</td>
								<td><input class = "dp form-control" style = "width:200px;" type = "text" name = "revised">
								</td>
							</tr>
						</table>
					</div>
					<strong>Written By: </strong>
					<?php print $emp['fullname']; ?>
				</td>
			</tr>

			<tr>
				<td>&nbsp;</td>
			</tr>

			<tr>
				<td>
					<?php
					if($is_blank==1)
					{
						print '<a href = "purchase_orders.php" class = "btn btn-danger">CANCEL</a> ';
					}
					else
					{
						?>
						<a href = "project_purchase_orders.php?id=<?php print $id; ?>" class = "btn btn-danger">CANCEL</a> 
					<?php } ?>
					<input type = "submit" value = "SAVE AND CONTINUE" id = "sbt" class = "btn btn-success">
				</td>
			</tr>

		</table>

		</form>

    </div>
</div>

</section>
</section>

<!-- End: Main -->
<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{
	"use strict";

	// Init Theme Core    
	Core.init();

	//var n = $('#notes').html();
	//$("#notes").html($.trim(n));

	$(window).keydown(function(event)
	{
	    if(event.keyCode == 13) 
	    {
	      	event.preventDefault();
	      	return false;
	    }
	});

	$('#type').change(function()
	{
		if(this.value=="Materials")
		{
			$('#company').val("<?php print $company_info['name']; ?>");
			$('#address_1').val("<?php print $company_info['address_1']; ?>");
			$('#city').val("<?php print $company_info['city']; ?>");
			$('#state').val("<?php print $company_info['state']; ?>");
			$('#zip').val("<?php print $company_info['zip']; ?>");
		}
		if(this.value=="Outsource")
		{
			$('#company').val("<?php print $company_info['name']; ?>");
			$('#address_1').val("<?php print $company_info['address_1']; ?>");
			$('#city').val("<?php print $company_info['city']; ?>");
			$('#state').val("<?php print $company_info['state']; ?>");
			$('#zip').val("<?php print $company_info['zip']; ?>");
		}
		if(this.value=="Subcontract")
		{
			$('#company').val("<?php print $project['site']; ?>");
			$('#address_1').val("<?php print $project['address_1'] . ' '. $project['address_2']; ?>");
			$('#city').val("<?php print $project['city']; ?>");
			$('#state').val("<?php print $project['state']; ?>");
			$('#zip').val("<?php print $project['zip']; ?>");
		}
	});

	// date pickers
	$('.dp').datepicker();

	$('.clickableRow').click(function()
	{
		var id = this.id;
		//alert(id);
		$('#vendor_id').val(id);
		$.post("jq.get_vendor_data.php", 
		{ 
			id: id
		})
		.done(function( data ) 
		{
			$('#vendor_info').html('');
			$('#vendor_info').html(data);
		});
	});

	$('#searchbtn').click(function(e)
	{
		e.preventDefault();
		var search = $('#search').val();
		var loading = '<img src = "images/loading.gif" width = "50" height = "50" style = "margin-left:50px;margin-top:0px;">';
		$('#vendor_list').html('');
		$('#vendor_list').html(loading);
		$.post( "jq.find_vendor.php", { search: search })
		.done(function( data ) 
		{
	  		$('#vendor_list').html(data);
		});    
        return false;
	});

	// reset button
	$('#reset').click(function(e)
	{
		e.preventDefault();
		var loading = '<img src = "images/loading.gif" width = "50" height = "50" style = "margin-left:50px;margin-top:0px;">';
		$('#vendor_list').html('');
		$('#vendor_list').html(loading);
		$.post( "jq.reset_vendor.php" )
		  .done(function( data ) 
		  {
		  		$('#vendor_list').html(data);
		  });  
	});

	// submit button click
	$('#sbt').click(function()
	{
		var type = $('#type').val();
		var vendor = $('#vendor_id').val();
		var error=0;

		// type and vendor must be selected
		if(type=='')
		{
			alert('Please select a type');
			//$('#type').css('background-color','red');
			$('#type').css('border','1px solid red');
			//$('body').scrollTo('#type');
			$('#type').focus();
			return false;
			error++;
		}
		
		if(vendor=='')
		{
			alert('Please select a vendor');
			return false;
		}

		if(error==0)
		{
			//return false;
			$('#form').submit();
		}
	});

});
</script>
<!-- END: PAGE SCRIPTS -->
</body>
</html>