<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
if($action==1)
{
	$uploaddir = 'images/';
	$uploadfile = $uploaddir . basename($_FILES['logo']['name']);
	if(move_uploaded_file($_FILES['logo']['tmp_name'], $uploadfile)) 
	{
		$s1=1;
		$s2=$vujade->update_row('cms_logo',1,'url',$uploadfile);    
		$s = $s1+$s2;
		if($s!=2)
		{
			$vujade->errors[]="There was a problem updating the logo file. Please contact the website administrator for assistance.";
		}
		else
		{
			$success=1;
		}
	} 
	else 
	{
		$ec = $_FILES['logo']['error']; 
		if($ec==1)
		{
			$vujade->errors[]="The uploaded file is too large.";
		}
		if($ec==3)
		{
			$vujade->errors[]="The uploaded file was only partially uploaded.";
		}
		if($ec==4)
		{
			$vujade->errors[]="No file was uploaded.";
		}
		if($ec==6)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 6.";
		}
		# there is no error code 5....
		if($ec==7)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 7.";
		}
		if($ec==8)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 8.";
		}
	}
}
if($action==2)
{
	$text = $_POST['text'];
	$uploaddir = 'images/';
	$uploadfile = $uploaddir . basename($_FILES['slider']['name']);
	if(move_uploaded_file($_FILES['slider']['tmp_name'], $uploadfile)) 
	{
		$s1=1;
		$fileinfo = pathinfo($uploadfile);
		$newslider = "images/slider1.".$fileinfo['extension'];
		rename($uploadfile,$newslider);
		$s2=$vujade->update_row('cms_sliders',1,'photo',$newslider);    
		$s3=$vujade->update_row('cms_sliders',1,'quote',$text); 
		$s = $s1+$s2+$s3;
		if($s!=3)
		{
			$vujade->errors[]="There was a problem updating the slider file. Please contact the website administrator for assistance.";
		}
		else
		{
			$success=1;
		}
	} 
	else 
	{
		$ec = $_FILES['slider']['error']; 
		if($ec==1)
		{
			$vujade->errors[]="The uploaded file is too large.";
		}
		if($ec==3)
		{
			$vujade->errors[]="The uploaded file was only partially uploaded.";
		}
		if($ec==4)
		{
			$vujade->errors[]="No file was uploaded.";
			$s3=$vujade->update_row('cms_sliders',1,'quote',$text);
			$vujade->errors[]="The quotation was changed.";
		}
		if($ec==6)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 6.";
		}
		# there is no error code 5....
		if($ec==7)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 7.";
		}
		if($ec==8)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 8.";
		}
	}
}
if($action==3)
{
	$text = $_POST['text'];
	$uploaddir = 'images/';
	$uploadfile = $uploaddir.basename($_FILES['slider']['name']);
	if(move_uploaded_file($_FILES['slider']['tmp_name'], $uploadfile)) 
	{
		$fileinfo = pathinfo($uploadfile);
		$newslider = "images/slider2.".$fileinfo['extension'];
		rename($uploadfile,$newslider);
		$s1=1;
		$s2=$vujade->update_row('cms_sliders',2,'photo',$newslider);    
		$s3=$vujade->update_row('cms_sliders',2,'quote',$text); 
		$s = $s1+$s2+$s3;
		if($s!=3)
		{
			$vujade->errors[]="There was a problem updating the slider file. Please contact the website administrator for assistance.";
		}
		else
		{
			$success=1;
		}
	} 
	else 
	{
		$ec = $_FILES['slider']['error']; 
		if($ec==1)
		{
			$vujade->errors[]="The uploaded file is too large.";
		}
		if($ec==3)
		{
			$vujade->errors[]="The uploaded file was only partially uploaded.";
		}
		if($ec==4)
		{
			$vujade->errors[]="No file was uploaded.";
			$s3=$vujade->update_row('cms_sliders',2,'quote',$text);
			$vujade->errors[]="The quotation was changed.";
		}
		if($ec==6)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 6.";
		}
		# there is no error code 5....
		if($ec==7)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 7.";
		}
		if($ec==8)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 8.";
		}
	}
}
if($action==4)
{
	$text = $_POST['text'];
	$uploaddir = 'images/';
	$uploadfile = $uploaddir . basename($_FILES['slider']['name']);
	if(move_uploaded_file($_FILES['slider']['tmp_name'], $uploadfile)) 
	{
		$fileinfo = pathinfo($uploadfile);
		$newslider = "images/slider3.".$fileinfo['extension'];
		rename($uploadfile,$newslider);
		$s1=1;
		$s2=$vujade->update_row('cms_sliders',3,'photo',$newslider);    
		$s3=$vujade->update_row('cms_sliders',3,'quote',$text); 
		$s = $s1+$s2+$s3;
		if($s!=3)
		{
			$vujade->errors[]="There was a problem updating the slider file. Please contact the website administrator for assistance.";
		}
		else
		{
			$success=1;
		}
	} 
	else 
	{
		$ec = $_FILES['slider']['error']; 
		if($ec==1)
		{
			$vujade->errors[]="The uploaded file is too large.";
		}
		if($ec==3)
		{
			$vujade->errors[]="The uploaded file was only partially uploaded.";
		}
		if($ec==4)
		{
			$vujade->errors[]="No file was uploaded.";
			$s3=$vujade->update_row('cms_sliders',3,'quote',$text);
			$vujade->errors[]="The quotation was changed.";
		}
		if($ec==6)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 6.";
		}
		# there is no error code 5....
		if($ec==7)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 7.";
		}
		if($ec==8)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 8.";
		}
	}
}
if($action==5)
{
	$text = $_POST['text'];
	$uploaddir = 'images/';
	$uploadfile = $uploaddir . basename($_FILES['slider']['name']);
	if(move_uploaded_file($_FILES['slider']['tmp_name'], $uploadfile)) 
	{
		$fileinfo = pathinfo($uploadfile);
		$newslider = "images/slider4.".$fileinfo['extension'];
		rename($uploadfile,$newslider);
		$s1=1;
		$s2=$vujade->update_row('cms_sliders',4,'photo',$newslider);    
		$s3=$vujade->update_row('cms_sliders',4,'quote',$text); 
		$s = $s1+$s2+$s3;
		if($s!=3)
		{
			$vujade->errors[]="There was a problem updating the slider file. Please contact the website administrator for assistance.";
		}
		else
		{
			$success=1;
		}
	} 
	else 
	{
		$ec = $_FILES['slider']['error']; 
		if($ec==1)
		{
			$vujade->errors[]="The uploaded file is too large.";
		}
		if($ec==3)
		{
			$vujade->errors[]="The uploaded file was only partially uploaded.";
		}
		if($ec==4)
		{
			$vujade->errors[]="No file was uploaded.";
			$s3=$vujade->update_row('cms_sliders',4,'quote',$text);
			$vujade->errors[]="The quotation was changed.";
		}
		if($ec==6)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 6.";
		}
		# there is no error code 5....
		if($ec==7)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 7.";
		}
		if($ec==8)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 8.";
		}
	}
}
if($action==6)
{
	$text = $_POST['text'];
	$uploaddir = 'images/';
	$uploadfile = $uploaddir . basename($_FILES['slider']['name']);
	if(move_uploaded_file($_FILES['slider']['tmp_name'], $uploadfile)) 
	{
		$fileinfo = pathinfo($uploadfile);
		$newslider = "images/slider5.".$fileinfo['extension'];
		rename($uploadfile,$newslider);
		$s1=1;
		$s2=$vujade->update_row('cms_sliders',5,'photo',$newslider);    
		$s3=$vujade->update_row('cms_sliders',5,'quote',$text); 
		$s = $s1+$s2+$s3;
		if($s!=3)
		{
			$vujade->errors[]="There was a problem updating the slider file. Please contact the website administrator for assistance.";
		}
		else
		{
			$success=1;
		}
	} 
	else 
	{
		$ec = $_FILES['slider']['error']; 
		if($ec==1)
		{
			$vujade->errors[]="The uploaded file is too large.";
		}
		if($ec==3)
		{
			$vujade->errors[]="The uploaded file was only partially uploaded.";
		}
		if($ec==4)
		{
			$vujade->errors[]="No file was uploaded.";
			$s3=$vujade->update_row('cms_sliders',5,'quote',$text);
			$vujade->errors[]="The quotation was changed.";
		}
		if($ec==6)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 6.";
		}
		# there is no error code 5....
		if($ec==7)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 7.";
		}
		if($ec==8)
		{
			$vujade->errors[]="Could not upload file to the server. Error Code 8.";
		}
	}
}
if(isset($_REQUEST['f']))
{
	$f=$_REQUEST['f'];
	if(!in_array($f,array(1,2,3,4,5,6)))
	{
		$f=0;
	}
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Content Management System - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">
        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">
        	
        	<?php
        	$ss_menu=7;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

<?php
# default page load
if($f==0)
{
?>
	<table width = "200">
	<tr>
	<td>
	<a href = "cms.php?f=1" class = "accounting">Website Logo</a>	
	</td>
	</tr>
	<tr>
	<td>
	<a href = "cms.php?f=2" class = "accounting">Photo Slider 1</a>
	</td>
	</tr>
	<tr>
	<td>
	<a href = "cms.php?f=3" class = "accounting">Photo Slider 2</a>
	</td>
	</tr>
	<tr>
	<td>
	<a href = "cms.php?f=4" class = "accounting">Photo Slider 3</a>
	</td>
	</tr>
	<tr>
	<td>
	<a href = "cms.php?f=5" class = "accounting">Photo Slider 4</a>
	</td>
	</tr>
	<tr>
	<td>
	<a href = "cms.php?f=6" class = "accounting">Photo Slider 5</a>
	</td>
	</tr>
	</table>
<?php 
}
# logo upload
if($f==1)
{
	if($success==1)
	{
		print '<div class = "success">Logo was uploaded. <a href = "cms.php?f=1">Upload a different logo</a></div>';
	}
	else
	{
		$vujade->show_errors();
		?>
			<div class = "row">
				<div class = "col-md-6 well">
					<strong>Requirements</strong><br>
					Width: 155 pixels<br>
					Height: 45 pixels<br>
					Dots Per Inch (dpi): 72<br>
					File Types: .jpg, .png or .gif<br>
					<!--
					Header Background Color: <br>
					HEX: 3B3B3C<br>
					RGB: 59, 59, 60<br>
					-->
					<em>For best results, please upload a file that meets the above requirements.</em>
				</div>
				<div class = "col-md-6">
					<?php
					$logo = $vujade->get_logo(1);
					if($logo['error']=="0")
					{
						print '<img src = "'.$logo['url'].'" width = "155" height = "45"><br>';
						print 'Current Logo';
					}
					?>
				</div>
			</div>

			<hr>

			<div class = "row">
				<form method = "post" enctype="multipart/form-data" action = "cms.php">
					<input type = "hidden" name = "f" value = "1">
					<input type = "hidden" name = "action" value = "1">
					Change Your Logo: <input name="logo" type="file" />
		    		<input type="submit" class="btn btn-primary" value="Upload" />
				</form>
			</div>

		<?php
	}
	?>

	<?php
}
# slider 1 upload
if($f==2)
{
	if($success==1)
	{
		print '<div class = "success">Slider 1 was uploaded. Text block 1 was added.</div>';
	}
	else
	{
		$vujade->show_errors();
		$slider=$vujade->get_slider(1);
		if($slider['error']==0)
		{
			$photo = $slider['photo'];
			$text = $slider['quote'];
		}
		else
		{
			$photo=0;
			if(!isset($text))
			{
				$text = "";
			}
		}
		if(file_exists($photo))
		{
			print '<strong>Slider 1 Image:</strong><br>';
			print '<img border = "1" src = "timthumb.php?src='.$photo.'&w=930&h=230">';
			print '<br>';
		}
		else
		{
			print '<p><strong>No slider image yet. Please upload one.</strong><br></p>';
		}
		?>

		<form method = "post" enctype="multipart/form-data" action = "cms.php">
			<input type = "hidden" name = "f" value = "2">
			<input type = "hidden" name = "action" value = "2">
			Photo: <input name="slider" type="file" />
			<br>
			Text
			<br>
			<textarea name = "text" rows = "5" cols = "45" class="form-control"><?php print $text; ?></textarea>
			<br>
    		<input type="submit" class="btn btn-primary" value="Upload" />
		</form>

		<?php
	}
}
# slider 2 upload
if($f==3)
{
	if($success==1)
	{
		print '<div class = "success">Slider 2 was uploaded. Text block 2 was added. </div>';
	}
	else
	{
		$vujade->show_errors();
		$slider=$vujade->get_slider(2);
		if($slider['error']==0)
		{
			$photo = $slider['photo'];
			$text = $slider['quote'];
		}
		else
		{
			$photo=0;
			if(!isset($text))
			{
				$text = "";
			}
		}
		if(file_exists($photo))
		{
			print '<strong>Slider 2 Image:</strong><br>';
			print '<img border = "1" src = "timthumb.php?src='.$photo.'&w=930&h=230">';
			print '<br>';
		}
		else
		{
			print '<p><strong>No slider image yet. Please upload one.</strong><br></p>';
		}
		?>

		<form method = "post" enctype="multipart/form-data" action = "cms.php">
			<input type = "hidden" name = "f" value = "3">
			<input type = "hidden" name = "action" value = "3">
			Photo: <input name="slider" type="file" />
			<br>
			Text
			<br>
			<textarea name = "text" rows = "5" cols = "45" class="form-control"><?php print $text; ?></textarea>
			<br>
    		<input type="submit" class="btn btn-primary" value="Upload" />
		</form>

		<?php
	}
}
# slider 3 upload
if($f==4)
{
	if($success==1)
	{
		print '<div class = "success">Slider 3 was uploaded. Text block 3 was added. </div>';
	}
	else
	{
		$vujade->show_errors();
		$slider=$vujade->get_slider(3);
		if($slider['error']==0)
		{
			$photo = $slider['photo'];
			$text = $slider['quote'];
		}
		else
		{
			$photo=0;
			if(!isset($text))
			{
				$text = "";
			}
		}
		if(file_exists($photo))
		{
			print '<strong>Slider 3 Image:</strong><br>';
			print '<img border = "1" src = "timthumb.php?src='.$photo.'&w=930&h=230">';
			print '<br>';
		}
		else
		{
			print '<p><strong>No slider image yet. Please upload one.</strong></p>';
		}
		?>

		<form method = "post" enctype="multipart/form-data" action = "cms.php">
			<input type = "hidden" name = "f" value = "4">
			<input type = "hidden" name = "action" value = "4">
			Photo: <input name="slider" type="file" />
			<br>
			Text
			<br>
			<textarea name = "text" rows = "5" cols = "45" class="form-control"><?php print $text; ?></textarea>
			<br>
    		<input type="submit" class="btn btn-primary" value="Upload" />
		</form>

		<?php
	}
}
# slider 4 upload
if($f==5)
{
	if($success==1)
	{
		print '<div class = "success">Slider 4 was uploaded. Text block 4 was added. </div>';
	}
	else
	{
		$vujade->show_errors();
		$slider=$vujade->get_slider(4);
		if($slider['error']==0)
		{
			$photo = $slider['photo'];
			$text = $slider['quote'];
		}
		else
		{
			$photo=0;
			if(!isset($text))
			{
				$text = "";
			}
		}
		if(file_exists($photo))
		{
			print '<strong>Slider 4 Image:</strong><br>';
			print '<img border = "1" src = "timthumb.php?src='.$photo.'&w=930&h=230">';
			print '<br>';
		}
		else
		{
			print '<p><strong>No slider image yet. Please upload one.</strong></p>';
		}
		?>

		<form method = "post" enctype="multipart/form-data" action = "cms.php">
			<input type = "hidden" name = "f" value = "5">
			<input type = "hidden" name = "action" value = "5">
			Photo: <input name="slider" type="file" />
			<br>
			Text
			<br>
			<textarea name = "text" rows = "5" cols = "45" class="form-control"><?php print $text; ?></textarea>
			<br>
    		<input type="submit" class="btn btn-primary" value="Upload" />
		</form>

		<?php
	}
}
# slider 5 upload
if($f==6)
{
	if($success==1)
	{
		print '<div class = "success">Slider 5 was uploaded. Text block 5 was added. </div>';
	}
	else
	{
		$vujade->show_errors();
		$slider=$vujade->get_slider(5);
		if($slider['error']==0)
		{
			$photo = $slider['photo'];
			$text = $slider['quote'];
		}
		else
		{
			$photo=0;
			if(!isset($text))
			{
				$text = "";
			}
		}
		if(file_exists($photo))
		{
			print '<strong>Slider 5 Image:</strong><br>';
			print '<img border = "1" src = "timthumb.php?src='.$photo.'&w=930&h=230">';
			print '<br>';
		}
		else
		{
			print '<p><strong>No slider image yet. Please upload one.</strong></p>';
		}
		?>

		<form method = "post" enctype="multipart/form-data" action = "cms.php">
			<input type = "hidden" name = "f" value = "6">
			<input type = "hidden" name = "action" value = "6">
			Photo: <input name="slider" type="file" />
			<br>
			Text
			<br>
			<textarea name = "text"  class="form-control" rows = "5" cols = "45"><?php print $text; ?></textarea>
			<br>
    		<input type="submit" class="btn btn-primary" value="Upload" />
		</form>

		<?php
	}
}
?>
				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

 <!-- BEGIN: PAGE SCRIPTS -->

 <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    // reset all buttons to primary
   	$('.btn').each(function()
    {
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
