<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup=$vujade->get_setup();
$taxes = $vujade->get_tax_groups();

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = 'Group Sales Taxes - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Group Sales Taxes</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu pull-right">
						
					</div>
				</div>
	        	<div class="panel-body bg-light">
					<?php 
					if($taxes['count']>0)
					{
						unset($taxes['error']);
						unset($taxes['sql1']);
						unset($taxes['count']);
						?>
						<table id="datatable" class="employees-table table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>Name</strong></td>
									<td valign = "top"><strong>Description</strong></td>
									<td valign = "top"><strong>Rate</strong></td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">
						    <form method = "post" action = "taxes.php">	
							<?php
							foreach($taxes as $tax)
							{
						        print '<tr class = "">';

						        print '<td valign = "top" style = "">'.$tax['name'].'</td>';

						        print '<td class="" valign = "top" style = "">'.$tax['desc'].'</td>';

						        print '<td valign = "top" style = "">';
						        print '<span style = "">'.$tax['rate'].'</span>';
						        print $tax['sql2'];
						        print '</td>';

								print '</tr>';
							}

							?>
							</tbody>
						</table>

						<input type = "hidden" name = "save" value = "1">

						</form>

					<?php
					}
					else
					{
						$vujade->set_error('Could not fetch taxes from database. ');
						$vujade->set_error($taxes['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25000,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    /**/
    $(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    }); 

});
</script>

</body>
</html>