<?php
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();

// date for the legend
$d = '';
$legend = $vujade->get_legend_data();
if($legend['error']==0)
{
	unset($legend['error']);
	unset($legend['count']);
	// show radios for color
	$d.='<table>';
	$d.='<b style = "margin-left:5px;">Pick a new color for this job:</b><br><br>';
	foreach($legend as $l)
	{
		$hc=str_replace('#','',$l['hex']);
	    $d.='<tr>';
	    $d.='<td>';
	    $d.='<input type = "radio" name = "new-color" class = "radio-color" value = "'.$hc.'"></td>';
	    $d.='<td>';
	    $d.='<div style = "border:1px solid #eeeeee; width:100px;height:15px;background-color:'.$l['hex'].';display:block;float:left"></div>';
	    $d.='</td>';
	    $d.='<td> = ';
	    $d.=$l['explanation'];
	    $d.='</td>';
	    $d.='</tr>';
	    unset($hc);
	}
	$d.='</table>';
}

$ms = $vujade->get_manufacturing_schedule();
//$vujade->debug_array($ms);
//die;
if($ms['count']>0)
{
	// there is data to filter
	unset($ms['count']);
	unset($ms['error']);
	array_multisort($ms[6], SORT_NUMERIC, SORT_DESC);
	// get the last key in the array
	end($ms);        
	$last_key = key($ms);  
	reset($ms);
	$data='';
	
	foreach($ms as $key=> $m)
	{

		$color=$m['hex'];
        $text_color='black';

		// make these transparent
        if(!empty($m['mf_done']))
        {
        	$color='#000000';
        	$text_color='white';
        }
        $progress=0;
        if(!empty($m['project_id']))
        {
			$data.='{
	                id: "'.$m['project_id'].'",
	                text: "'.$m['project_id'].' '.$m['site'].'",
	                start_date: "'.$m['start'].'",
	                duration: "'.$m['days'].'",
	                progress: 0,
	                color:"'.$color.'",
	                textColor: "'.$text_color.'"
	            }';
	    }
        if($key!=$last_key)
        {
        	$data.=',';
        }
	}
}

//$vujade->debug_array($transparent);
//die;
?>
<!DOCTYPE html>
<head>
	<meta http-equiv="Content-type" content="text/html; charset=utf-8">
	<title>Manufacturing Schedule</title>
</head>
	<script src="gantt/codebase/dhtmlxgantt.js" type="text/javascript" charset="utf-8"></script>
	<script src="gantt/codebase/ext/dhtmlxgantt_marker.js" type="text/javascript" charset="utf-8"></script>
	<link rel="stylesheet" href="gantt/codebase/dhtmlxgantt.css" type="text/css" media="screen" title="no title" charset="utf-8">

	<style type="text/css">
		html, body{ height:100%; padding:0px; margin:0px; overflow: hidden;}
		.gantt_link_point
		{
			display:none;
		}
		.gantt_task_progress_drag
		{
			display:none;
		}
		.gantt_cal_lsection
		{
			display:none;
		}
		.gantt_time
		{
			display:none;
		}
		.gantt_mark
		{
			display:none;
		}
		.gantt_cal_light
		{
			top:0px;
		}
		.dhx_cal_cover
		{
			top:0px;
		}
		.view_project_btn_set {
		    color: #fff !important;
		    background-color: #5cb85c !important;
		    border-color: #4cae4c;
		    text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
		    -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,0.15),0 1px 1px rgba(0,0,0,0.075);
			box-shadow: inset 0 1px 0 rgba(255,255,255,0.15),0 1px 1px rgba(0,0,0,0.075);
			display: inline-block;
			padding: 9px 12px;
			padding-top: 7px;
			margin-bottom: 0;							
			text-align: center;
			vertical-align: middle;
			cursor: pointer;
			background-color: #d1dade;
			-webkit-border-radius: 3px;
			-webkit-border-radius: 3px;
			-webkit-border-radius: 3px;
			background-image: none !important;
			border: none;
			text-shadow: none;
			box-shadow: none;
			transition: all 0.12s linear 0s !important;
			font: 14px/20px "Helvetica Neue",Helvetica,Arial,sans-serif;
		}

		.view_project_btn_set:hover {
			background-color: #5cc85c !important;
		}

		/*
		.view_project_btn 
		{
		  -webkit-border-radius: 0;
		  -moz-border-radius: 0;
		  border-radius: 0px;
		  font-family: Arial;
		  color: #ffffff;
		  font-size: 16px;
		  background: #89CE78;
		  padding: 10px 20px 10px 20px;
		  text-decoration: none;
		  width:200px;
		}

		.view_project_btn:hover 
		{
		  background: #70CA63;
		  text-decoration: none;
		}
		.view_project_btn
		{
		    margin-top: 1px;
		    background-image:url("gantt/view_project.png");
		    width: 20px;
		}
		*/
	</style>
<body>

	<input type = "hidden" name = "clicked" id = "clicked">

	<div id="gantt_here" style='width:100%; height:100%;'></div>
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script type="text/javascript">
		// gantt data from php
        var data =  {
            data:[
                <?php print $data; ?>
            ]
        };

        // disable resize and linking to other projects (dependencies)
        gantt.config.drag_resize = false;
		gantt.config.drag_links = false;
		gantt.config.drag_progress = false;

		// column setup in left pane
        gantt.config.columns = [
		    {
		    	name:"text",       
		    	label:"Project",  
		    	width:"200", 
		    	tree:false 
		    },
		    
		];

		gantt.config.lightbox.sections = [
		    { name:"description", height:200, map_to:"text", type:"my_editor" , focus:true},
		];

		gantt.config.buttons_left = ["view_project_btn", "dhx_save_btn", "dhx_cancel_btn"];
		gantt.config.buttons_right = [];
		gantt.locale.labels["view_project_btn"] = "View Project";
		gantt.config.task_attribute = "id";

		// init gantt
		gantt.init("gantt_here");

		// add marker for today
		var markerId = gantt.addMarker({
		    start_date: new Date(), //a Date object that sets the marker's date
		    css: "today", //a CSS class applied to the marker
		    text: "Today", //the marker title
		    title: "Today" // the marker's tooltip
		});

		// custom content for the lightbox (the modal that appears on click of a project box)
		gantt.form_blocks["my_editor"] = 
		{
			render:function(sns) 
			{
		    	var formdata='<?php print $d; ?>';
		    	return formdata;
			},
			set_value:function(node, value, task) {
				//node.childNodes[1].value = value || "";
				//node.childNodes[4].value = task.users || "";
			},
			get_value:function(node, task) {
				//task.users = node.childNodes[4].value;
				//return node.childNodes[1].value;
			},
			focus:function(node) {
				//var a = node.childNodes[1];
				//a.select();
				//a.focus();
			}
	    };

	    // save button press in the lightbox
	    gantt.attachEvent("onLightboxSave", function(id, task, is_new)
	    {
		    var r = $('input[name=new-color]');
		    var v = r.filter(':checked').val();
		    // reload the entire page including the iframe
		    parent.location.href="schedule.php?action=1&project_id="+id+"&new_color="+v;
		    return true;
		});

	    // view project button click in lightbox
	    gantt.attachEvent("onLightboxButton", function(button_id, node, e){
	        if(button_id == "view_project_btn")
	        {
	            var id = gantt.getState().lightbox;
	            parent.location.href="project.php?id="+id;
		    	return true;
	        }
	    });

		// move an event
	    gantt.attachEvent("onAfterTaskDrag", function(id, mode, e)
		{
			// get the updated task
		    var t = gantt.getTask(id);

		    // new start date and duration
		    new_start_date = t.start_date;
		    duration = t.duration; 

		    // jquery post update the schedule / job status
		    $.post( "jq.gantt_update.php", { project_id: id, days: duration, new_start_date: new_start_date})
			  .done(function(r) 
			  {
			  		//$('#msg').html(r);
			  });
		});

	    // put today in view
		// gantt.showDate(<?php print date("d/m/Y"); ?>);
		var x = gantt.posFromDate(new Date());
		//var x = gantt.posFromDate(new Date("<?php print date('d/m/Y'); ?>"));
		gantt.scrollTo(x, 0);
		$('.today').focus();

		// move the lightbox to be in view
		gantt.attachEvent("onLightbox", function (task_id)
		{
			var top = $('#clicked').val();
		    $('.gantt_cal_light').css('top',top+'px');
		});

		// helper
		// double click on task; get y coordinate 
		// set y coordinate to hidden field so lightbox can be moved to it
		gantt.attachEvent("onTaskClick", function(id,e)
		{
		    $('#clicked').val(e.clientY);
		    return true;
		});

		// parse php data
		gantt.parse(data);

	</script>

	<script>
	/*
	$(document).ready(function()
	{
		$('#test').on('change', '#test', function()
		{
			alert('changed...');
		});
	});

	$(document).on('change', '#test', function (e) 
	{
    	alert('changed...');
	});
	*/
	</script>
</body>
</html>