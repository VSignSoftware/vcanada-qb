<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$customer_permissions = $vujade->get_permission($_SESSION['user_id'],'Clients');
if($customer_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);

// site setup
$setup=$vujade->get_setup();

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

//$vujade->print_r_array($_REQUEST);
//die;

// disble customer contacts
$contacts=0;
$is_new=0;

// new
if($action==1)
{
	$title = "New Customer - ";
	$is_new=1;
	$customer=array();
}

// save new / update
if($action==2)
{
	$is_new=$_REQUEST['is_new'];
	$name=trim($_POST['name']);

	//print_r($_POST);
	//die;

	if(empty($name))
	{
		$vujade->errors[]='Customer name cannot be empty';
		$title = "New Customer - ";
		$is_new=1;
		$customer=array();
	}
	else
	{
		// customer name must be unique if different 
		if($is_new==1)
		{
			// test to see if name is unique
			$test = $vujade->get_customer_by_name($name,0,1);
			//print_r($test);
			
			// customer exists
			if($test['count']==1)
			{
				$test['error']=1;
				//print $test['count'].'<br>';
				//print $test['error'];
			}
			else
			{
				$test['error']=0;
			}
			//die;
		}
		else
		{
			// if they changed the name, it must be unique
			$id = $_REQUEST['id'];
			$customer=$vujade->get_customer($id,'ID');
			if($customer['error']!='0')
			{
				die("Invalid Customer ID.");
			}
			else
			{
				if($customer['name']!=$name)
				{
					$test = $vujade->get_customer_by_name($name,1,1);
					$test['error']=$test['count'];
				}
				else
				{
					$test['error']=0;
				}
			}
		}

		//print_r($test);
		//die;

		if($test['error']=="0")
		{
			$salesperson=$_POST['salesperson'];
			$address_1=$_POST['address_1'];
			$address_2=$_POST['address_2'];
			$city=$_POST['city'];
			$state=$_POST['state'];
			$zip=$_POST['zip'];
			$country=$_POST['country'];
			$phone=$_POST['phone'];
			$email=$_POST['email'];
			$contact=$_POST['contact'];
			$one_time_only=$_POST['one_time_only'];
			if($is_new==1)
			{
				$vujade->create_row('quickbooks_customer');
				$id = $vujade->row_id;
			}
			else
			{
				$id = $_REQUEST['id'];
				$customer=$vujade->get_customer($id,'ID');
				if($customer['error']!='0')
				{
					die("Invalid Customer ID.");
				}
			}

			if($is_new==1)
			{
				$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
				$s[] = $vujade->update_row('quickbooks_customer',$id,'ListID',$fakeid);
			}
			
			$s[] = $vujade->update_row('quickbooks_customer',$id,'Name',$name);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'FullName',$name);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_Addr1',$name);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_Addr2',$address_1);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_Addr3',$address_2);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_City',$city);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_State',$state);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_PostalCode',$zip);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'BillAddress_Country',$country);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'Phone',$phone);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'Email',$email);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'is_one_time_only',$one_time_only);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'contact',$salesperson);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'IsActive','true');

			// tracking of who entered new customer
			$s[] = $vujade->update_row('quickbooks_customer',$id,'entered_by',$_SESSION['user_id']);
			$s[] = $vujade->update_row('quickbooks_customer',$id,'time_entered',strtotime('now'));
			$s[] = $vujade->update_row('quickbooks_customer',$id,'page_entered_on',$_SERVER['PHP_SELF']);

			// add to quickbooks
			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);

			if($is_new==1)
			{
				$Queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, $vujade->row_id);
			}
			else
			{
				// only execute if there is an editsequence and
				// there is not already an unsynced add in the queue 
				$is_queued=$vujade->check_for_qb_queue_object($id);
				if( ($customer['edit_sequence']!='') && (!$is_queued) )
				{
					$Queue->enqueue(QUICKBOOKS_MOD_CUSTOMER, $id);
				}
			}

			$vujade->messages[]="Customer Updated";
			$customer=$vujade->get_customer($id,'ID');
			$is_new=0;
			// enable customer contacts
			$contacts=1;
		}
		else
		{
			$vujade->errors[]="Customer name already exists for a different customer.";

			if($is_new==1)
			{
				$title = "New Customer - ";
				$is_new=1;
				$customer=array();
			}
			else
			{
				$action=0;
			}
		}
	}
}

// delete
if($action==3)
{
	$id = $_REQUEST['id'];
	$customer=$vujade->get_customer($id,'ID');
	$success = $vujade->update_row('quickbooks_customer',$id,'IsActive','false','ID');

	// quickbooks mod
	// only if edit sequence is not empty
	if(!empty($customer['edit_sequence']))
	{
		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;

		// create item on qb server
		if(function_exists('date_default_timezone_set'))
		{
			date_default_timezone_set('America/Los_Angeles');
		}
		
		if (!QuickBooks_Utilities::initialized($dsn))
		{	
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}

		// insert into the quickbooks vendor table
		//$modid=$customer['local_id'];
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_MOD_CUSTOMER, $id);
	}
	else
	{
		// delete from the queue
		/*
		$q1 = "DELETE FROM `quickbooks_queue` WHERE `qb_action` = 'CustomerAdd' AND `ident` = '$id' AND `dequeue_datetime` IS NULL";
		$s[]=$vujade->generic_query($q1,0,2);

		$q2 = "DELETE FROM `quickbooks_queue` WHERE `qb_action` = 'CustomerMod' AND `ident` = '$id' AND `dequeue_datetime` IS NULL";
		$s[]=$vujade->generic_query($q2,0,2);
		*/
	}
	$vujade->page_redirect('customers.php?m=1');
}

// new contact
if($action==4)
{
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$title=$_POST['title'];
	$phone1=$_POST['phone1'];
	$phone2=$_POST['phone2'];
	$phone3=$_POST['phone3'];
	$fax1=$_POST['fax1'];
	$fax2=$_POST['fax2'];
	$email1=$_POST['email1'];
	$email2=$_POST['email2'];
	$notes=$_POST['notes'];
	$id = $_REQUEST['id'];

	$label_1 = $_REQUEST['label_1'];
	$label_2 = $_REQUEST['label_2'];
	$label_3 = $_REQUEST['label_3'];
	$label_4 = $_REQUEST['label_4'];

	# new row in contacts
	$vujade->create_row('customer_contact');
	$row_id = $vujade->row_id;
	$s[]=$vujade->update_row('customer_contact',$row_id,'customer_id',$id);
	$s[]=$vujade->update_row('customer_contact',$row_id,'first_name',$first_name);
	$s[]=$vujade->update_row('customer_contact',$row_id,'last_name',$last_name);
	$s[]=$vujade->update_row('customer_contact',$row_id,'title',$title);
	$s[]=$vujade->update_row('customer_contact',$row_id,'notes',$notes);
	$s[]=$vujade->update_row('customer_contact',$row_id,'phone1',$phone1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'phone2',$phone2);
	$s[]=$vujade->update_row('customer_contact',$row_id,'phone3',$phone3);
	$s[]=$vujade->update_row('customer_contact',$row_id,'fax1',$fax1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'fax2',$fax2);
	$s[]=$vujade->update_row('customer_contact',$row_id,'email1',$email1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'email2',$email2);

	$s[]=$vujade->update_row('customer_contact',$row_id,'label_1',$label_1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'label_2',$label_2);
	$s[]=$vujade->update_row('customer_contact',$row_id,'label_3',$label_3);
	$s[]=$vujade->update_row('customer_contact',$row_id,'label_4',$label_4);

	$vujade->messages[]="Contact added.";
	$action=0;
	$tab=2;
}

// edit contact
if($action==5)
{
	$cid=$_REQUEST['cid'];
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$title=$_POST['title'];
	$phone1=$_POST['phone1'];
	$phone2=$_POST['phone2'];
	$phone3=$_POST['phone3'];
	$fax1=$_POST['fax1'];
	$fax2=$_POST['fax2'];
	$email1=$_POST['email1'];
	$email2=$_POST['email2'];
	$notes=$_POST['notes'];

	$label_1 = $_REQUEST['c_label_1'];
	$label_2 = $_REQUEST['c_label_2'];
	$label_3 = $_REQUEST['c_label_3'];
	$label_4 = $_REQUEST['c_label_4'];

	$row_id = $cid;
	$s[]=$vujade->update_row('customer_contact',$row_id,'first_name',$first_name);
	$s[]=$vujade->update_row('customer_contact',$row_id,'last_name',$last_name);
	$s[]=$vujade->update_row('customer_contact',$row_id,'title',$title);
	$s[]=$vujade->update_row('customer_contact',$row_id,'notes',$notes);
	$s[]=$vujade->update_row('customer_contact',$row_id,'phone1',$phone1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'phone2',$phone2);
	$s[]=$vujade->update_row('customer_contact',$row_id,'phone3',$phone3);
	$s[]=$vujade->update_row('customer_contact',$row_id,'fax1',$fax1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'fax2',$fax2);
	$s[]=$vujade->update_row('customer_contact',$row_id,'email1',$email1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'email2',$email2);

	$s[]=$vujade->update_row('customer_contact',$row_id,'label_1',$label_1);
	$s[]=$vujade->update_row('customer_contact',$row_id,'label_2',$label_2);
	$s[]=$vujade->update_row('customer_contact',$row_id,'label_3',$label_3);
	$s[]=$vujade->update_row('customer_contact',$row_id,'label_4',$label_4);

	$vujade->messages[]="Contact updated.";
	$action=0;
	$tab=2;
}

// delete contact
if($action==6)
{
	$row_id = $_REQUEST['cid'];
	$s[]=$vujade->delete_row('customer_contact',$row_id);
	$vujade->messages[]="Contact Deleted";
	$id = $_REQUEST['id'];
	$customer=$vujade->get_customer($id,'ID');
	$tab=2;
}

// add note
if($action==7)
{
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$notes=$_POST['notes'];
	$is_new=$_REQUEST['is_new'];
	$id = $_POST['id'];
	if($is_new==0)
	{
		$vujade->create_row('customer_vendor_notes');
		$note_id = $vujade->row_id;
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'customer_vendor_id',$id);
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'customer_or_vendor',1);
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'date',date('m/d/Y'));
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'ts',strtotime('now'));
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'body',$notes);
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'user_id',$_SESSION['user_id']);
		$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'name_display',$employee['first_name'].' '.$employee['last_name']);
		$vujade->messages[]="Note Added";
		$is_new=0;
		$contacts=1;
		$tab=3;
		$action=0;
	}
	else
	{
		$vujade->errors[]='Notes cannot be added until the first tab is complete';
		$tab=3;
		$action=0;
	}
}

# delete document
if($action==8)
{
	$id = $_REQUEST['id'];
	$file_id = $_REQUEST['docid'];
	if(ctype_digit($file_id))
	{
		$filedata = $vujade->get_document($file_id);
		@unlink('uploads/customer_documents/'.$id.'/'.$filedata['file_name']);
		$s[] = $vujade->delete_row('documents',$file_id);
	}
	$vujade->messages[]="Document Deleted";
	$action=0;
	$tab=4;
}

// delete note
if($action==9)
{
	$nid = $_REQUEST['note_id'];
	if(ctype_digit($nid))
	{
		$s[] = $vujade->delete_row('customer_vendor_notes',$nid);
		$vujade->messages[]="Note Deleted";
	}
	$action=0;
	$tab=3;
}

// update note
if($action==10)
{
	$notes=$_POST['notes'];
	$id = $_REQUEST['id'];
	$note_id = $_POST['note_id'];
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'date',date('m/d/Y'));
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'ts',strtotime('now'));
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'body',$notes);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'user_id',$_SESSION['user_id']);
	$s[]=$vujade->update_row('customer_vendor_notes',$note_id,'name_display',$employee['first_name'].' '.$employee['last_name']);
	$vujade->messages[]="Note Updated";
	$is_new=0;
	$contacts=1;
	$tab=3;
	$action=0;
}

// existing
if($action==0)
{
	$id = $_REQUEST['id'];
	$customer=$vujade->get_customer($id,'ID');
	if($customer['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$title = $customer['name']." - ";

	// enable customer contacts
	$contacts=1;

	// notes 
	$notes = $vujade->get_notes($id,1);
}

// active tab
$valid = array(1,2,3,4,5,6);
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
}
if(!in_array($tab,$valid))
{
	$tab=6;
}

$emp=$employee;
$section=7;
require_once('h.php');
?>

<style>
.customer-address
{
	line-height: 50%;
	font-weight:bold;
}
</style>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $title; ?></a>
    </li>
  </ol>
</div>

<a href = "customers.php" class = "btn btn-primary btn-sm pull-right">&laquo; Back</a>

</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">
<div class="">
<?php
$vujade->show_messages();
$vujade->show_errors();
?>

<div class = "row">
<div class = "col-md-12">

<div class="panel" id="wide-panel">
	<div class="panel-heading" style = "border-bottom:none;">
		<ul class="nav panel-tabs panel-tabs-border panel-tabs-left" style = "border-bottom:none;">

		  <li class="<?php if($tab==6){ print 'active'; } ?>">
	        <a href="#tab6" data-toggle="tab">Overview</a>
	      </li>

	      <li class="<?php if($tab==1){ print 'active'; } ?>">
	        <a href="#tab1" data-toggle="tab">Details</a>
	      </li>

	      <?php
	      if($is_new==0)
	      {
	      	?>

	      <li class="<?php if($tab==2){ print 'active'; } ?>">
	        <a href="#tab2" data-toggle="tab">Contacts</a>
	      </li>
	      <li class="<?php if($tab==3){ print 'active'; } ?>">
	        <a href="#tab3" data-toggle="tab">Notes</a>
	      </li>
	      <li class="<?php if($tab==4){ print 'active'; } ?>">
	        <a href="#tab4" data-toggle="tab">Documents</a>
	      </li>
	      <?php } ?>
	    </ul>
	</div>

	<div class="panel-body">
	<div class="tab-content pn br-n">

	<!-- first tab: overview details -->
	<div id="tab6" class="tab-pane <?php if($tab==6){ print 'active'; } ?>"style = "">
	<div class="row" style = "">
		<div class = "col-md-6">
			<h2><?php print $customer['name']; ?></h2>
		</div>
		<div class = "col-md-3" style = "padding-top:15px;">
			<?php
			print '<p class = "customer-address">'.$customer['address_1'].'</p>';
			if(!empty($customer['address_2']))
			{
				print '<p class = "customer-address">'.$customer['address_2'].'</p>'; 
			}
			print '<p class = "customer-address">'.$customer['city'].', '.$customer['state']. ' '.$customer['zip'].'</p>';
			?>
		</div>
		<div class = "col-md-3" style = "padding-top:15px;">
			<?php
			print '<p class = "customer-address">'.$customer['salesperson'].'</p>';
			?>
		</div>
	</div>

	<div class = "row">
		<div class = "col-md-12">
			<?php
			print '<strong><u>Contacts</u></strong><br>';
			if($is_new==0)
			{
				if(!empty($_REQUEST['id']))
				{	
					$contacts = $vujade->get_customer_contacts($_REQUEST['id']);
				}
				//print_r($contacts);
				if($contacts['error']=="0")
				{
					print '<div class = "row">';
					print '<div class = "col-md-1">&nbsp;</div>';
					print '<div class = "col-md-11">';
					unset($contacts['error']);
					print '<table width = "500">';
					foreach($contacts as $c)
					{
						if(trim($c['fullname'])!='')
						{
							print '<tr>';
							print '<td colspan = "2">';
							print $c['fullname'];
							print '</td>';
							print '</tr>';

							print '<tr>';
							print '<td width = "10%">&nbsp;';
							print '</td>';
							print '<td width = "90%">';
							print $c['label_1'].': ';
							print $c['phone1'];
							print '</td>';
							print '</tr>';

							print '<tr>';
							print '<td>&nbsp;';
							print '</td>';
							print '<td>';
							print $c['label_2'].': ';
							print $c['phone2'];
							print '</td>';
							print '</tr>';

							print '<tr>';
							print '<td>&nbsp;';
							print '</td>';
							print '<td>';
							print $c['label_3'].': ';
							print $c['phone3'];
							print '</td>';
							print '</tr>';

							print '<tr>';
							print '<td>&nbsp;';
							print '</td>';
							print '<td>';
							print $c['label_4'].': ';
							print $c['fax1'];
							print '</td>';
							print '</tr>';

							print '<tr>';
							print '<td colspan = "2">&nbsp;';
							print '</td>';
							print '</tr>';

							print '<tr>';
							print '<td colspan = "2">&nbsp;';
							print '</td>';
							print '</tr>';
						}
					}
					print '</table>';
					print '</div>';
					print '</div>';
				}
			}
			?>
		
			<div>
				<?php
				print '<br><strong><u>Notes</u></strong><br>';
				if($notes['count']>0)
				{
					$has_notes=true;
					print '<table class = "table">';
					unset($notes['count']);
					unset($notes['error']);
					foreach($notes as $note)
					{
						print '<tr>';
						print '<td width = "25%">';
						print $note['date'];
						print '</td>';
						print '<td td width = "25%">';
						print $note['name_display'];
						print '</td>';
						print '<td td width = "50%">';
						print $note['body'];
						print '</td>';
						print '</tr>';
					}
					print '</table>';
				}	
				?>
			</div>
		
		</div>
	</div>

	<div class = "well" style = "margin-top:15px;">
		<strong><u>Documents</u></strong><br>
		<table class = "table">

		<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
		<td>Name</td>
		<td width = "">&nbsp;</td>
		<td width = "">&nbsp;</td>
		<td width = "">Date/Time</td>
		<td>&nbsp;</td>
		</tr>
		<?php
		# get docs for this customer
		$docs = $vujade->get_documents($id);
		if($is_new==0)
		{
			if($docs['error']=="0")
			{
				unset($docs['error']);
				foreach($docs as $doc)
				{
					# print out the file details; this one is not in a folder
					print '<tr>';

					// name
					print '<td valign = "top" width = "60%">';
					print '<a href = "download_document.php?customer=1&customer_id='.$id.'&file_id='.$doc['database_id'].'">';
					print $doc['file_name'];
					print '</a>';
					print '</td>';

					// type
					print '<td valign = "top">';
					$type_data = $vujade->get_document_type($doc['file_type']);
					print $type_data['name'];
					print '</td>';

					# edit button
					print '<td valign = "top">&nbsp;';
					print '</td>';

					// date
					print '<td valign = "top">';

					$ts = strtotime($doc['ts']);
					$formatted_ts = date('m/d/Y',$ts);
					print $formatted_ts;
					print '</td>';

					// delete
					print '<td style = "text-align:right;" valign = "top">';
					//print '<a class = "btn btn-danger btn-xs delete-document" data-href="customer_overview.php?id='.$id.'&action=1&docid='.$doc['database_id'].'" href = "#delete-document-modal">X</a>';
					print '&nbsp;';
					print '</td>';

					print '</tr>';

				}
			}
		}
		?>
		</table>
	</div>

	</div>

	<!-- tab 2 details -->
	<div id="tab1" class="tab-pane <?php if($tab==1){ print 'active'; } ?>"style = "border:0px solid red;">
	<div class="row" style = "border:0px solid red;">
	<div class="col-md-12">
		<!-- details -->
		<table class = "table" style = "width:100%;">
			<form method = "post" action = "<?php print $_SERVER['PHP_SELF']; ?>" id = "form">
			<input type = "hidden" name = "action" value = "2">
			<input type = "hidden" name = "tab" value = "1">
			<input type = "hidden" name = "is_new" value = "<?php print $is_new; ?>">
			<tr class = "">
			<td class = "" width="20%">
				ID
			</td>
			<td class = "">
				<?php print $customer['local_id']; ?>
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Name
			</td>
			<td class = "">
				<input type = "text" class = "form-control" name = "name" value = "<?php print $customer['name']; ?>">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Salesperson
			</td>
			<td class = "">
				<select name = "salesperson" class = "form-control">
					<?php
					if(isset($customer['salesperson']))
					{
						print '<option value = "'.$customer['salesperson'].'" selected = "selected">'.$customer['salesperson'].'</option>';
						print '<option value = "">-select-</option>';
					}
					else
					{
						print '<option value = "">-select-</option>';
					}
					$sp = $vujade->get_salespeople();
					if($sp['error']=='0')
					{
						unset($sp['error']);
						foreach($sp as $s)
						{
							print '<option value = "'.$s['fullname'].'">'.$s['fullname'].'</option>';
						}
					}
					?>
				</select>

			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Address Line 1
			</td>
			<td class = "">
				<input type = "text" name = "address_1" value = "<?php print $customer['address_1']; ?>" class = "form-control">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Address Line 2
			</td>
			<td class = "">
				<input type = "text" name = "address_2" value = "<?php print $customer['address_2']; ?>" class = "form-control">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				City
			</td>
			<td class = "">
				<input type = "text" name = "city" value = "<?php print $customer['city']; ?>" class = "form-control">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				State / Province
			</td>
			<td class = "">
				<select name = "state" class = "form-control" id = "state">
					<?php
					if(!empty($customer['state']))
					{
						print '<option selected value = "'.$customer['state'].'">'.$customer['state'].'</option>';
					}
					//print '<option value = "">-select-</option>';
					if($setup['country']=="USA")
	          	{
	              ?>
	              <option value="AL">Alabama</option>
	              <option value="AK">Alaska</option>
	              <option value="AZ">Arizona</option>
	              <option value="AR">Arkansas</option>
	              <option value="CA">California</option>
	              <option value="CO">Colorado</option>
	              <option value="CT">Connecticut</option>
	              <option value="DE">Delaware</option>
	              <option value="DC">District Of Columbia</option>
	              <option value="FL">Florida</option>
	              <option value="GA">Georgia</option>
	              <option value="HI">Hawaii</option>
	              <option value="ID">Idaho</option>
	              <option value="IL">Illinois</option>
	              <option value="IN">Indiana</option>
	              <option value="IA">Iowa</option>
	              <option value="KS">Kansas</option>
	              <option value="KY">Kentucky</option>
	              <option value="LA">Louisiana</option>
	              <option value="ME">Maine</option>
	              <option value="MD">Maryland</option>
	              <option value="MA">Massachusetts</option>
	              <option value="MI">Michigan</option>
	              <option value="MN">Minnesota</option>
	              <option value="MS">Mississippi</option>
	              <option value="MO">Missouri</option>
	              <option value="MT">Montana</option>
	              <option value="NE">Nebraska</option>
	              <option value="NV">Nevada</option>
	              <option value="NH">New Hampshire</option>
	              <option value="NJ">New Jersey</option>
	              <option value="NM">New Mexico</option>
	              <option value="NY">New York</option>
	              <option value="NC">North Carolina</option>
	              <option value="ND">North Dakota</option>
	              <option value="OH">Ohio</option>
	              <option value="OK">Oklahoma</option>
	              <option value="OR">Oregon</option>
	              <option value="PA">Pennsylvania</option>
	              <option value="RI">Rhode Island</option>
	              <option value="SC">South Carolina</option>
	              <option value="SD">South Dakota</option>
	              <option value="TN">Tennessee</option>
	              <option value="TX">Texas</option>
	              <option value="UT">Utah</option>
	              <option value="VT">Vermont</option>
	              <option value="VA">Virginia</option>
	              <option value="WA">Washington</option>
	              <option value="WV">West Virginia</option>
	              <option value="WI">Wisconsin</option>
	              <option value="WY">Wyoming</option>
	              <option value="">---------------</option>
	              <option value="">-Canadian Provinces-</option>
	              <option value="Alberta">Alberta</option>
	              <option value="British Columbia">British Columbia</option>
	              <option value="Manitoba">Manitoba</option>
	              <option value="New Brunswick">New Brunswick</option>
	              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
	              <option value="Northwest Territories">Northwest Territories</option>
	              <option value="Nova Scotia">Nova Scotia</option>
	              <option value="Nunavut">Nunavut</option>
	              <option value="Ontario">Ontario</option>
	              <option value="Prince Edward Island">Prince Edward Island</option>
	              <option value="Quebec">Quebec</option>
	              <option value="Saskatchewan">Saskatchewan</option>
	              <option value="Yukon">Yukon</option>
	            <?php 
		    	} 
		    	// canadian servers
		    	if($setup['country']=='Canada')
		    	{
		    		?>
		    		  <option value="">-Canadian Provinces-</option>
		              <option value="Alberta">Alberta</option>
		              <option value="British Columbia">British Columbia</option>
		              <option value="Manitoba">Manitoba</option>
		              <option value="New Brunswick">New Brunswick</option>
		              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
		              <option value="Northwest Territories">Northwest Territories</option>
		              <option value="Nova Scotia">Nova Scotia</option>
		              <option value="Nunavut">Nunavut</option>
		              <option value="Ontario">Ontario</option>
		              <option value="Prince Edward Island">Prince Edward Island</option>
		              <option value="Quebec">Quebec</option>
		              <option value="Saskatchewan">Saskatchewan</option>
		              <option value="Yukon">Yukon</option>
		              <option value="">---------------</option>
		              <option value="">-US States-</option>
		              <option value="AL">Alabama</option>
		              <option value="AK">Alaska</option>
		              <option value="AZ">Arizona</option>
		              <option value="AR">Arkansas</option>
		              <option value="CA">California</option>
		              <option value="CO">Colorado</option>
		              <option value="CT">Connecticut</option>
		              <option value="DE">Delaware</option>
		              <option value="DC">District Of Columbia</option>
		              <option value="FL">Florida</option>
		              <option value="GA">Georgia</option>
		              <option value="HI">Hawaii</option>
		              <option value="ID">Idaho</option>
		              <option value="IL">Illinois</option>
		              <option value="IN">Indiana</option>
		              <option value="IA">Iowa</option>
		              <option value="KS">Kansas</option>
		              <option value="KY">Kentucky</option>
		              <option value="LA">Louisiana</option>
		              <option value="ME">Maine</option>
		              <option value="MD">Maryland</option>
		              <option value="MA">Massachusetts</option>
		              <option value="MI">Michigan</option>
		              <option value="MN">Minnesota</option>
		              <option value="MS">Mississippi</option>
		              <option value="MO">Missouri</option>
		              <option value="MT">Montana</option>
		              <option value="NE">Nebraska</option>
		              <option value="NV">Nevada</option>
		              <option value="NH">New Hampshire</option>
		              <option value="NJ">New Jersey</option>
		              <option value="NM">New Mexico</option>
		              <option value="NY">New York</option>
		              <option value="NC">North Carolina</option>
		              <option value="ND">North Dakota</option>
		              <option value="OH">Ohio</option>
		              <option value="OK">Oklahoma</option>
		              <option value="OR">Oregon</option>
		              <option value="PA">Pennsylvania</option>
		              <option value="RI">Rhode Island</option>
		              <option value="SC">South Carolina</option>
		              <option value="SD">South Dakota</option>
		              <option value="TN">Tennessee</option>
		              <option value="TX">Texas</option>
		              <option value="UT">Utah</option>
		              <option value="VT">Vermont</option>
		              <option value="VA">Virginia</option>
		              <option value="WA">Washington</option>
		              <option value="WV">West Virginia</option>
		              <option value="WI">Wisconsin</option>
		              <option value="WY">Wyoming</option>
		    		<?php
		    	}	
		    	?>
						
				</select>
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Zip / Postal Code
			</td>
			<td class = "">
				<input type = "text" name = "zip" value = "<?php print $customer['zip']; ?>" class = "form-control">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Country
			</td>
			<td class = "">
				<input type = "text" name = "country" value = "<?php print $customer['country']; ?>" class = "form-control">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Phone
			</td>
			<td class = "">
				<input type = "text" name = "phone" value = "<?php print $customer['phone']; ?>" class = "form-control">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				Email
			</td>
			<td class = "">
				<input type = "text" name = "email" class = "form-control" value = "<?php print $customer['email']; ?>">
			</td>
			</tr>

			<tr class = "">
			<td class = "">
				One time only
			</td>
			<td class = "">
				<input type = "checkbox" name = "one_time_only" value = "1" id = "one_time_only" <?php if($customer['is_one_time_only']==1){ print 'checked="checked"'; } ?>>
			</td>
			</tr>

			<tr class = "">
			<td class = "">
			</td>
			<td class = "">
			<input type = "hidden" name = "id" value = "<?php print $id; ?>">
			<input type = "submit" id = "save" value = "SAVE" class = "btn btn-primary">

			<?php
			if($is_new==0)
			{
				?>
				<a class = "btn btn-danger pull-right" id = "delete-customer" href = "#delete-customer-modal">Delete</a>
			<?php } ?>

			<!-- delete modal -->
			<div id="delete-customer-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
			<h1>Delete Customer</h1>
			<p>Are you sure you want to delete this customer?</p>

			<a class="btn btn-lg btn-danger" href="customer.php?id=<?php print $id; ?>&action=3">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>

			</div>

			</form>
			</td>
			</tr>
		</table>
	</div>
	</div>
	</div>

	<!-- tab 3 contacts -->
	<div id="tab2" class="tab-pane <?php if($tab==2){ print 'active'; } ?>">
	<div class="row">
	<div class="col-md-12">
		<?php
		// contacts
		if($is_new==0)
		{
			?>
			<div class = "col-md-12">
				<a id = "new-contact" href = "#new-contact-modal" class = "btn btn-sm btn-primary pull-right">New</a>

				<!-- new contact modal -->
				<div id="new-contact-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
					<h1>New Customer Contact</h1>
					<form method = "post" action = "customer.php" id = "new-contact-form">

						<table class = "table">
							<tr class = "project_search_results">
							<td class = "project_search_results">
								First Name
							</td>
							<td class = "project_search_results">
								<input type = "text" name = "first_name" value = "" class = "form-control">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								Last Name
							</td>
							<td class = "project_search_results">
								<input type = "text" name = "last_name" class = "form-control" value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								Title
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "title"  value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								<select id="label_1" name="label_1" style="float: left;width: initial;" class = "form-control">
		                              <option value = "">-Select-</option>
		                              <option value="Mobile">Mobile</option>
		                              <option value="Work">Work</option>
		                              <option value="Home">Home</option>
		                              <option value="Car">Car</option>
		                              <option value="Email">Email</option>
		                              <option value="Work FAX">Work FAX</option>
		                              <option value="Home FAX">Home FAX</option>
		                              <option value="Pager">Pager</option>
		                              <option value="Company Main">Company Main</option>
		                              <option value="Other">Other</option>
		                              <option value="Other FAX">Other FAX</option>
		                              <option value="Assistant">Assistant</option>
		                              <option value="Callback">Callback</option>
		                              <option value="ISDN">ISDN</option>
		                              <option value="Main">Main</option>
		                              <option value="Radio">Radio</option>
		                              <option value="Telex">Telex</option>
		                              <option value="TDD/TTY">TDD/TTY</option>
	                            </select>
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "phone1"  value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								<select id="label_2" name="label_2" style="float: left;width: initial;" class = "form-control">
		                              <option value = "">-Select-</option>
		                              <option value="Mobile">Mobile</option>
		                              <option value="Work">Work</option>
		                              <option value="Home">Home</option>
		                              <option value="Car">Car</option>
		                              <option value="Email">Email</option>
		                              <option value="Work FAX">Work FAX</option>
		                              <option value="Home FAX">Home FAX</option>
		                              <option value="Pager">Pager</option>
		                              <option value="Company Main">Company Main</option>
		                              <option value="Other">Other</option>
		                              <option value="Other FAX">Other FAX</option>
		                              <option value="Assistant">Assistant</option>
		                              <option value="Callback">Callback</option>
		                              <option value="ISDN">ISDN</option>
		                              <option value="Main">Main</option>
		                              <option value="Radio">Radio</option>
		                              <option value="Telex">Telex</option>
		                              <option value="TDD/TTY">TDD/TTY</option>
	                            	</select>
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "phone2"  value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								<select id="label_3" name="label_3" style="float: left;width: initial;" class = "form-control">
		                              <option value = "">-Select-</option>
		                              <option value="Mobile">Mobile</option>
		                              <option value="Work">Work</option>
		                              <option value="Home">Home</option>
		                              <option value="Car">Car</option>
		                              <option value="Email">Email</option>
		                              <option value="Work FAX">Work FAX</option>
		                              <option value="Home FAX">Home FAX</option>
		                              <option value="Pager">Pager</option>
		                              <option value="Company Main">Company Main</option>
		                              <option value="Other">Other</option>
		                              <option value="Other FAX">Other FAX</option>
		                              <option value="Assistant">Assistant</option>
		                              <option value="Callback">Callback</option>
		                              <option value="ISDN">ISDN</option>
		                              <option value="Main">Main</option>
		                              <option value="Radio">Radio</option>
		                              <option value="Telex">Telex</option>
		                              <option value="TDD/TTY">TDD/TTY</option>
	                            	</select>
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "phone3"  value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								<select id="label_4" name="label_4" style="float: left;width: initial;" class = "form-control">
		                              <option value = "">-Select-</option>
		                              <option value="Mobile">Mobile</option>
		                              <option value="Work">Work</option>
		                              <option value="Home">Home</option>
		                              <option value="Car">Car</option>
		                              <option value="Email">Email</option>
		                              <option value="Work FAX">Work FAX</option>
		                              <option value="Home FAX">Home FAX</option>
		                              <option value="Pager">Pager</option>
		                              <option value="Company Main">Company Main</option>
		                              <option value="Other">Other</option>
		                              <option value="Other FAX">Other FAX</option>
		                              <option value="Assistant">Assistant</option>
		                              <option value="Callback">Callback</option>
		                              <option value="ISDN">ISDN</option>
		                              <option value="Main">Main</option>
		                              <option value="Radio">Radio</option>
		                              <option value="Telex">Telex</option>
		                              <option value="TDD/TTY">TDD/TTY</option>
	                            	</select>
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "fax1"  value = "">
							</td>
							</tr>

							<!--
							<tr class = "project_search_results">
							<td class = "project_search_results">
								FAX 2
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "fax2"  value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								Email 1
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "email1"  value = "">
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								Email 2
							</td>
							<td class = "project_search_results">
								<input class = "form-control" type = "text" name = "email2"  value = "">
							</td>
							</tr>
							-->

							<tr class = "project_search_results">
							<td class = "project_search_results" colspan = "2">
								Notes:<br>
								<textarea name = "notes" class = "form-control"></textarea>
							</td>
							</tr>

							<tr class = "project_search_results">
							<td class = "project_search_results">
								<input type = "hidden" name = "id" value = "<?php print $id; ?>">
								<input type = "hidden" name = "action" value = "4">
								<input type = "submit" value = "Save" class = "btn btn-lg btn-primary">
							</td>
							<td>
								<input class="popup-modal-dismiss btn btn-lg btn-danger" value = "Cancel" type = "submit">
							</td>
							</tr>
						</table>

					</form>
				</div>

				<table class = "table">
				<?php
				if(!empty($id))
				{
					$contacts = $vujade->get_customer_contacts($id);
					if($contacts['error']=="0")
					{
						unset($contacts['error']);
						foreach($contacts as $c)
						{
							if(trim($c['fullname'])!='')
							{
						?>
							<tr>
								<td valign = "top" width = "60%">
									<a href = "#edit-contact-modal" data-fname="<?php print $c['first_name']; ?>" data-lname="<?php print $c['last_name']; ?>" data-cid="<?php print $c['database_id']; ?>" data-title="<?php print $c['title']; ?>" data-phone1="<?php print $c['phone1']; ?>" data-phone2="<?php print $c['phone2']; ?>" data-phone3="<?php print $c['phone3']; ?>" data-fax1="<?php print $c['fax1']; ?>" data-fax2="<?php print $c['fax2']; ?>" data-email1="<?php print $c['email1']; ?>" data-email2="<?php print $c['email2']; ?>" data-notes="<?php print $c['notes']; ?>" data-label_1="<?php print $c['label_1']; ?>" data-label_2="<?php print $c['label_2']; ?>" data-label_3="<?php print $c['label_3']; ?>" data-label_4="<?php print $c['label_4']; ?>" class = "edit-contact"><?php print $c['fullname']; ?></a>
								</td>
								
								<td valign = "top" width = "20%">
									<a href = "#edit-contact-modal" data-fname="<?php print $c['first_name']; ?>" data-lname="<?php print $c['last_name']; ?>" data-cid="<?php print $c['database_id']; ?>" data-title="<?php print $c['title']; ?>" data-phone1="<?php print $c['phone1']; ?>" data-phone2="<?php print $c['phone2']; ?>" data-phone3="<?php print $c['phone3']; ?>" data-fax1="<?php print $c['fax1']; ?>" data-fax2="<?php print $c['fax2']; ?>" data-email1="<?php print $c['email1']; ?>" data-email2="<?php print $c['email2']; ?>" data-notes="<?php print $c['notes']; ?>" data-label_1="<?php print $c['label_1']; ?>" data-label_2="<?php print $c['label_2']; ?>" data-label_3="<?php print $c['label_3']; ?>" data-label_4="<?php print $c['label_4']; ?>" class = "btn btn-xs btn-primary edit-contact">
									EDIT
									</a>
								</td>
							
								<td valign = "top" width = "20%">
									<a id ="<?php print $c['database_id']; ?>" href = "#delete-contact-modal" class = "btn btn-xs btn-danger delete-contact" data-href = "<?php print 'customer.php?action=6&id=' . $id . '&cid=' . $c['database_id']; ?>">
									DELETE
									</a>
								</td>
							</tr>

						<?php
							} 
						} 
					} 
				}
				?>
				</table>
			</div>
		<?php } ?>
	</div>
	</div>
	</div>

	<!-- tab 4 (notes) -->
	<div id="tab3" class="tab-pane <?php if($tab==3){ print 'active'; } ?>">
		<div class="row">

			<style>
			.conversation-date.active,
			.contact-name.active{
				background: #e5e5e5;
				text-decoration: none !important;
			}
			@media(max-width: 1024px)
			{
				.tab-pane{
					width: 768px;
				}
			}
			</style>

			<!-- panel left (small) -->
			<div class = "col-md-2">
				<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "note.php?action=1&type=1&id=<?php print $id; ?>" class = "btn btn-xs btn-primary plus" style = "">+</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					if($has_notes)
					{
						$show=1;
						unset($notes['count']);
						unset($notes['error']);
						$first_note=$notes[0];
						foreach($notes as $note)
						{
							print '<a class = "contact-name" href = "contact-'.$note['id'].'" id = "'.$note['id'].'">';
							print $note['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
			</div>

			<!-- panel right (wider) -->
			<div class = "col-md-10">

				<?php
				if($show==1)
				{
					foreach($notes as $note)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $note['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $note['date'].' '.$note['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "note.php?action=2&type=1&nid=<?php print $note['id']; ?>&id=<?php print $id; ?>" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $note['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $note['id']; ?>">
						  <?php print $note['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $note['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-danger"
								   href="customer.php?action=9&note_id=<?= $note['id']; ?>&id=<?= $id; ?>&tab=3">YES</a>
								<a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
			</div>

		</div>
	</div>

	<!-- tab 5 documents -->
	<div id="tab4" class="tab-pane <?php if($tab==4){ print 'active'; } ?>">
		<div class="row">
		  <div class="col-md-12">
		  <a href = "customer_document.php?customer_id=<?php print $id; ?>" class = "btn btn-primary btn-xs pull-right">New Document</a>
			<table class = "table">

				<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
					<td>Name</td>
					<td width = "">&nbsp;</td>
					<td width = "">&nbsp;</td>
					<td width = "">Date/Time</td>
					<td>&nbsp;</td>
				</tr>
				<?php
				# get docs for this customer
				if(!empty($id))
				{
					$docs = $vujade->get_documents($id);
					if($docs['error']=="0")
					{
						unset($docs['error']);
						foreach($docs as $doc)
						{
							# print out the file details; this one is not in a folder
							print '<tr>';

							// name
							print '<td valign = "top" width = "60%">';
							print '<a href = "download_document.php?customer=1&customer_id='.$id.'&file_id='.$doc['database_id'].'">';
							print $doc['file_name'];
							print '</a>';
							print '</td>';

							// type
							print '<td valign = "top">';
							$type_data = $vujade->get_document_type($doc['file_type']);
							print $type_data['name'];
							print '</td>';

							# edit button
							print '<td valign = "top">&nbsp;';
							print '</td>';

							// date
							print '<td valign = "top">';

							$ts = strtotime($doc['ts']);
							$formatted_ts = date('m/d/Y',$ts);
							print $formatted_ts;
							print '</td>';

							// delete
							print '<td style = "text-align:right;" valign = "top">';
							print '<a class = "btn btn-danger btn-xs delete-document" data-href="customer.php?id='.$id.'&action=8&docid='.$doc['database_id'].'" href = "#delete-document-modal">X</a>';
							print '</td>';

							print '</tr>';

						}
					}
				}
			?>
			</table>
		  </div>
		</div>
	</div>

	<!-- delete document modal -->
	<div id="delete-document-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
		<h1>Delete Document</h1>
		<p>Are you sure you want to delete this document?</p>
		<p><a id = "delete-document-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
	</div>

</div>
</div>

</div>
</div>
</div>
</section>
</section>

<!-- view / edit contact modal -->
<div id="edit-contact-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1>Edit Customer Contact</h1>
<form method = "post" action = "customer.php" id = "">

	<table class = "table">
		<tr class = "">
		<td class = "">
			First Name
		</td>
		<td class = "">
			<input class = "form-control" type = "text" name = "first_name" id = "c_first_name" value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			Last Name
		</td>
		<td class = "">
			<input class = "form-control" type = "text" name = "last_name" id = "c_last_name" value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			Title
		</td>
		<td class = "">
			<input class = "form-control" type = "text" name = "title" id = "c_title" value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			<select id="c_label_1" name="c_label_1" style="float: left;width: initial;" class = "form-control">
              <option value = "">-Select-</option>
              <option value="Mobile">Mobile</option>
              <option value="Work">Work</option>
              <option value="Home">Home</option>
              <option value="Car">Car</option>
              <option value="Email">Email</option>
              <option value="Work FAX">Work FAX</option>
              <option value="Home FAX">Home FAX</option>
              <option value="Pager">Pager</option>
              <option value="Company Main">Company Main</option>
              <option value="Other">Other</option>
              <option value="Other FAX">Other FAX</option>
              <option value="Assistant">Assistant</option>
              <option value="Callback">Callback</option>
              <option value="ISDN">ISDN</option>
              <option value="Main">Main</option>
              <option value="Radio">Radio</option>
              <option value="Telex">Telex</option>
              <option value="TDD/TTY">TDD/TTY</option>
        	</select>
		</td>
		<td class = "">
			<input class = "form-control" type = "text" id = "c_phone1" name = "phone1"  value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			<select id="c_label_2" name="c_label_2" style="float: left;width: initial;" class = "form-control">
              <option value = "">-Select-</option>
              <option value="Mobile">Mobile</option>
              <option value="Work">Work</option>
              <option value="Home">Home</option>
              <option value="Car">Car</option>
              <option value="Email">Email</option>
              <option value="Work FAX">Work FAX</option>
              <option value="Home FAX">Home FAX</option>
              <option value="Pager">Pager</option>
              <option value="Company Main">Company Main</option>
              <option value="Other">Other</option>
              <option value="Other FAX">Other FAX</option>
              <option value="Assistant">Assistant</option>
              <option value="Callback">Callback</option>
              <option value="ISDN">ISDN</option>
              <option value="Main">Main</option>
              <option value="Radio">Radio</option>
              <option value="Telex">Telex</option>
              <option value="TDD/TTY">TDD/TTY</option>
        	</select>
		</td>
		<td class = "">
			<input class = "form-control" type = "text" id = "c_phone2" name = "phone2"  value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			<select id="c_label_3" name="c_label_3" style="float: left;width: initial;" class = "form-control">
              <option value = "">-Select-</option>
              <option value="Mobile">Mobile</option>
              <option value="Work">Work</option>
              <option value="Home">Home</option>
              <option value="Car">Car</option>
              <option value="Email">Email</option>
              <option value="Work FAX">Work FAX</option>
              <option value="Home FAX">Home FAX</option>
              <option value="Pager">Pager</option>
              <option value="Company Main">Company Main</option>
              <option value="Other">Other</option>
              <option value="Other FAX">Other FAX</option>
              <option value="Assistant">Assistant</option>
              <option value="Callback">Callback</option>
              <option value="ISDN">ISDN</option>
              <option value="Main">Main</option>
              <option value="Radio">Radio</option>
              <option value="Telex">Telex</option>
              <option value="TDD/TTY">TDD/TTY</option>
        	</select>
		</td>
		<td class = "">
			<input class = "form-control" type = "text" id = "c_phone3" name = "phone3"  value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			<select id="c_label_4" name="c_label_4" style="float: left;width: initial;" class = "form-control">
              <option value = "">-Select-</option>
              <option value="Mobile">Mobile</option>
              <option value="Work">Work</option>
              <option value="Home">Home</option>
              <option value="Car">Car</option>
              <option value="Email">Email</option>
              <option value="Work FAX">Work FAX</option>
              <option value="Home FAX">Home FAX</option>
              <option value="Pager">Pager</option>
              <option value="Company Main">Company Main</option>
              <option value="Other">Other</option>
              <option value="Other FAX">Other FAX</option>
              <option value="Assistant">Assistant</option>
              <option value="Callback">Callback</option>
              <option value="ISDN">ISDN</option>
              <option value="Main">Main</option>
              <option value="Radio">Radio</option>
              <option value="Telex">Telex</option>
              <option value="TDD/TTY">TDD/TTY</option>
        	</select>
		</td>

		<td class = "">
			<input class = "form-control" type = "text" name = "fax1" id = "c_fax1" value = "">
		</td>
		</tr>

		<!--

		<tr class = "">
		<td class = "">
			FAX 2
		</td>
		<td class = "">
			<input class = "form-control" type = "text" name = "fax2" id = "c_fax2" value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			Email 1
		</td>
		<td class = "">
			<input class = "form-control" type = "text" name = "email1" id = "c_email1" value = "">
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			Email 2
		</td>
		<td class = "">
			<input class = "form-control" type = "text" id = "c_email2" name = "email2"  value = "">
		</td>
		</tr>
		-->

		<tr class = "">
		<td class = "" colspan = "2">
			Notes:<br>
			<textarea name = "notes" id = "c_notes" class = "form-control"></textarea>
		</td>
		</tr>

		<tr class = "">
		<td class = "">
			<input type = "hidden" name = "id" value = "<?php print $id; ?>">
			<input type = "hidden" id = "cid" name = "cid" value = "">
			<input type = "hidden" name = "action" value = "5">
			<input type = "submit" value = "Save" class = "btn btn-primary">
		</td>
		<td>
			<input class="popup-modal-dismiss btn btn-lg btn-danger" value = "Cancel" type = "submit">
		</td>
		</tr>
	</table>

</form>
</div>

<!-- delete contact modal -->
<div id="delete-contact-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Contact</h1>
	<p>Are you sure you want to delete this contact?</p>
	<p><a id = "delete-contact-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	var HEADER = 60; // header height

	// focus on a contact
	$('.contact-name').click(function(e){
		e.preventDefault();

		var contact = $("#contact-"+this.id);

		if( $(this).hasClass('active') ){
			$(this).removeClass('active');
			contact.removeClass('panel-warning');
		} else {
			$('.contact-name').not( $(this) ).removeClass('active');
			$(this).addClass('active');

			$('.panel-contact').not( contact ).removeClass('panel-warning');
			contact.addClass('panel-warning');

			$('html, body').animate({
				scrollTop: contact.offset().top - HEADER
			}, 500);
		}
	});

	$('.delete-note').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '.delete-note-form',
		modal: true
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    // delete customer modal
    $('#delete-customer').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-customer-modal',
		modal: true
	});

    // new contact
    $('#new-contact').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#new-contact-modal',
		modal: true
	});

	// view / edit contact
	$('.edit-contact').click(function() 
    {
    	$('#cid').val($(this).data('cid'));
		$('#c_first_name').val($(this).data('fname'));
		$('#c_last_name').val($(this).data('lname'));
		$('#c_title').val($(this).data('title'));
		$('#c_phone1').val($(this).data('phone1'));
		$('#c_phone2').val($(this).data('phone2'));
		$('#c_phone3').val($(this).data('phone3'));
		$('#c_fax1').val($(this).data('fax1'));
		$('#c_label_1').val($(this).data('label_1'));
		$('#c_label_2').val($(this).data('label_2'));
		$('#c_label_3').val($(this).data('label_3'));
		$('#c_label_4').val($(this).data('label_4'));
		$('#c_notes').val($(this).data('notes'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#edit-contact-modal',
		modal: true
	});

    // delete contact
    $('.delete-contact').click(function() 
    {
		$('#delete-contact-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-contact-modal',
		modal: true
	});

	// delete document
    $('.delete-document').click(function() 
    {
		$('#delete-document-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-document-modal',
		modal: true
	});

});
</script>

</body>
</html>