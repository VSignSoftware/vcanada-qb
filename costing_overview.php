<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($permissions['read']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}
if($permissions['edit']!=1)
{
  	$vujade->page_redirect('error.php?m=1');
}
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
  	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);
$setup = $vujade->get_setup();
if($setup['error']!=0)
{
  	$vujade->page_redirect('error.php?m=3');
}
$buyout_overhead_percentage=$setup['buyout_overhead'];
$general_overhead_percentage=$setup['general_overhead'];
$machines_overhead_percentage=$setup['machine_overhead'];
$materials_overhead_percentage=$setup['materials_overhead'];

$action = 0;
if(isset($_REQUEST['action']))
{
  	$action = $_REQUEST['action'];
}
$employee = $vujade->get_employee($_SESSION['user_id']);
if($employee['error']!="0")
{
  $vujade->page_redirect('error.php?m=1');
}

// test if salesperson has been assigned
// if has, get sales person employee data
$semployee = $vujade->get_employee_by_name($project['salesperson']);
# update the notes
if($action==1)
{
  $notes = trim($_POST['notes']);
  $success = $vujade->update_row('projects',$id,'notes',$notes,'project_id');
  if($success==1)
  {
    $vujade->messages[]="Notes updated.";
  }
  $project = $vujade->get_project($id,2);
  if($project['error']!=0)
  {
    $vujade->page_redirect('error.php?m=3');
  }
}

# updates
if($action==2)
{
	$date_billed=$_POST['date_billed'];
	$commission=$_POST['commission'];
	$date_closed=$_POST['date_closed'];
	$s[]=$vujade->update_row('projects',$project['database_id'],'date_billed',$date_billed);
	$s[]=$vujade->update_row('projects',$project['database_id'],'commission',$commission);
	if(!empty($date_closed))
	{
		$s[] = $vujade->update_row('job_status',$js['database_id'],'is_closed',1);
		$s[] = $vujade->update_row('projects',$project['database_id'],'date_closed',$date_closed);

		$ts = strtotime($date_closed);
		$s[] = $vujade->update_row('projects',$project['database_id'],'date_closed_ts',$ts);

		$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Closed');
		$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',16);
	}
	$project = $vujade->get_project($id,2);
	if($project['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
}

# close the job
if($action==3)
{
	$s[] = $vujade->update_row('job_status',$js['database_id'],'is_closed',1);
	$s[] = $vujade->update_row('projects',$project['database_id'],'date_closed',date('m/d/Y'));
	$ts = strtotime(date('m/d/Y'));
	$s[] = $vujade->update_row('projects',$project['database_id'],'date_closed_ts',$ts);
	$project = $vujade->get_project($id,2);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Closed');
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',16);
	$project = $vujade->get_project($id,2);
	if($project['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	// clear the job status columns for ship date, install start and end date and service data
	$js = $vujade->get_job_status($id);
	if($js['error']!='0')
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$jsid = $js['database_id'];
	$s[]=$vujade->update_row('job_status',$jsid,'shipping_date','');
	$s[]=$vujade->update_row('job_status',$jsid,'service_date','');
	$s[]=$vujade->update_row('job_status',$jsid,'installation_date','');
	$s[]=$vujade->update_row('job_status',$jsid,'installation_end_date','');

	// remove calendar events
	$s[]=$vujade->delete_row('calendar',$id,1,'project_id');
}

# re-open the job
if($action==4)
{
	$s[] = $vujade->update_row('job_status',$js['database_id'],'is_closed',0);
	$s[] = $vujade->update_row('projects',$project['database_id'],'date_closed','');
	$s[] = $vujade->update_row('projects',$project['database_id'],'date_closed_ts','');
	$project = $vujade->get_project($id,2);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status',$project['status']);
	$status_number = $vujade->get_status_number($project['status']);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',$status_number);
	$project = $vujade->get_project($id,2);
	if($project['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 17;
$cmenu = 1;
$section = 3;
$title = "Costing - ".$project['project_id'].' - '.$project['site'].' - ';;
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left </aside>-->
<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;border:0px solid red;">
<div class="pl15 pr15" style = "width:100%;">
<?php require_once('project_right_tray.php'); ?>
<div class="panel panel-primary panel-border top">
<div class="panel-heading">
<span class="panel-title"></span>
<div class="widget-menu pull-right">
<a class="btn-sm btn-primary" href = "print_costing_overview.php?id=<?php print $id; ?>" target = "_blank">Print</a>
<?php 
if($project['status']!="Closed")
{
	# must have edit permission to close this order
	if($permissions['delete']==1)
	{
	?>
		<a class="btn-sm btn-primary" href="#close-order-form" id = "close_order">Close Order</a>
		<div id="close-order-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
		<h1>Close Order</h1>
		<p>Are you sure you want to close this project?</p>
		<p><a id = "" class="btn btn-lg btn-danger" href="costing_overview.php?id=<?php print $id; ?>&action=3">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
		</div>
	<?php
	}
}
else
{
	# must have edit permission to close this order
	if($permissions['delete']==1)
	{
	?>
		<a href = "#open-order-form" class = "btn-sm btn-primary" id = "open_order">Re-Open Order</a>
		<div id="open-order-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
		<h1>Re-Open Order</h1>
		<p>Are you sure you want to re-open this project?</p>
		<p><a id = "" class="btn btn-lg btn-danger" href="costing_overview.php?id=<?php print $id; ?>&action=4">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
		</div>
	<?php
	}
}
?>
</div>
</div>
<div class="panel-body bg-light">
<?php
$pos = $vujade->get_purchase_orders($id);

// get amounts from the ledger page (costing purchase orders)
// only use these amounts if they are not empty/0
// empty/0 means the ledger has not been filled in for this po
$cpos = $vujade->get_costing_purchase_orders($id);
if($cpos['error']=="0")
{
	$subtotal = 0;
	$total_dp = 0;
	$total_materials = 0;
	$total_outsource = 0;
	$total_subcontract = 0;
	unset($cpos['error']);
	foreach($cpos as $cpo)
	{
		$subtotal+=$cpo['cost'];
		if($cpo['type']=="Materials")
		{
			$total_materials+=$cpo['cost'];
			$total_dp+=$cpo['cost'];
		}
		if($cpo['type']=="Outsource")
		{
			$total_outsource+=$cpo['cost'];
		}
		if($cpo['type']=="Subcontract")
		{
			$total_subcontract+=$cpo['cost'];
		}
	}
}

$inventory_cost = 0;
$items = $vujade->get_materials_for_costing($id);
if($items['error']=="0")
{
	unset($items['error']);
	foreach($items as $i)
	{
		$line = $i['cost']*$i['qty'];
		$inventory_cost += $line;
	}
	$indt = $inventory_cost * $project['indeterminant'];
}
$tm = $total_dp+$inventory_cost+$indt;

$materials_overhead=$materials_overhead_percentage*$tm;

// machines
$machines_total=0;
$el_test = $vujade->get_machines_for_estimate($id);
if($el_test['error']=="0")
{
	unset($el_test['error']);
	foreach($el_test as $lt)
	{
		$line_total = $lt['rate']*$lt['hours'];
		$machines_total+=$line_total;
	}	
}

$machines_overhead=$machines_overhead_percentage*$machines_total;
//$machines_overhead=0;
//$machines_total=0;

# labor
$raw_labor = 0;
$labor_rider = 0;
$total_labor = 0;
$tcdata = $vujade->get_timecards_for_project($id);
if($tcdata['error']=="0")
{
	unset($tcdata['error']);
	foreach($tcdata as $tc)
	{
		# employee 
		$employee_id = $tc['employee_id'];

		# employee's rate
		$current_rate = $vujade->get_employee_current_rate($employee_id);

		# type of work
		$work_type = $tc['type'];

		# rate for type of work
		$rate_data = $vujade->get_labor_rate($work_type,'id');

		# hours worked
		$total_time = 0;
		$st_total = 0;
		$ot_total = 0;
		$dt_total = 0;
		if(!empty($tc['start']))
		{
			$ts = $tc['date'];
			$begints = strtotime($ts.' '.$tc['start']);
			$endts = strtotime($ts.' '.$tc['end']);
			$diff = $endts-$begints;
			$diff = $diff / 60;
			$diff = $diff / 60;
			$line_total=$diff;
			//$st_total+=$diff;
			if($diff<=8)
			{
				$st_total=$diff;
			}
			if( ($diff > 8) && ($diff <= 12) )
			{
				$ot_total = $diff - 8;
				$st_total = 8;
			}

			if($diff > 12)
			{
				$ot_total = 4;
				$st_total = 8;
				$dt_total = $diff - 12;
			}
		}
		else
		{
			$line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
			$st_total+=$tc['standard_time'];
			$ot_total+=$tc['over_time'];
			$dt_total+=$tc['double_time'];

		}
		$total_time+=$line_total;

		# raw labor 
		$st_rate_total = $st_total * $current_rate['rate'];
		$ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
		$dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
		$raw_labor += $st_rate_total + $ot_rate_total + $dt_rate_total;

		# labor rider
		$labor_rider_rate = $rate_data['rate']-$current_rate['rate'];
		$labor_rider += $labor_rider_rate * $total_time;
	}
	$total_labor += $labor_rider + $raw_labor;
}

$col1_subtotal = $tm + $total_labor + $total_outsource + $total_subcontract + $machines_total;

# general overhead
$general_overhead = 0;
//$general_overhead = $tm + $total_labor;
$general_overhead = $total_labor;
$general_overhead = $general_overhead * $general_overhead_percentage;

// 48.5% of Inventory + Indeterminent + Total Labor Cost
// $go = $subtotal + $indet + $labor_subtotal;
// $go = $go * $setup['general_overhead'];

# buyout overhead
$buyout_overhead = ($total_outsource + $total_subcontract) * $buyout_overhead_percentage;

# total cost
$total_cost = 0;
$total_cost = $col1_subtotal + $general_overhead + $buyout_overhead + $machines_overhead + $materials_overhead;

# base sale, additional billing, permit, permit acquisition, totla sale price
$base_sale = 0;
$additional_billing = 0;
$permit = 0;
$permit_acquisition = 0;
$discount = 0;
$total_sale_price = 0;
$bsa=array();
// Additional billing = Engineering + Change Orders + Shipping - discount. 
$project_invoices = $vujade->get_invoices_for_project($id);
if($project_invoices['error']=="0")
{
	unset($project_invoices['error']);
	foreach($project_invoices as $pi)
	{
		if($pi['costing']==1)
		{
			$invoiceid=$pi['database_id'];
			
			$firstinvoice=$pi;

			// not legacy
			if($pi['is_legacy']==1)
			{
				$st=0;

				// invoice types
				// 1: illuminated and lump sum
				if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==1) )
				{
					// labor
					$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// total
					$st = $labor+$parts; 

					$parts=0;

					// selling price text
					$selling_price_text = "Selling Price";
				}

				// 2: illuminated sign parts and labor
				if( ($firstinvoice['type_1']==1) && ($firstinvoice['type_2']==2) )
				{
					$show_parts=true;

					// labor
					$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// selling price text
					$selling_price_text = "Labor Only Price";
				}

				// 3: non-illuminated lump sum
				if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==1) )
				{
					$show_parts=false;

					$st = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

					$parts=0;

					// selling price text
					$selling_price_text = "Selling Price";
				}

				// 4: non-illuminated sign parts and labor
				if( ($firstinvoice['type_1']==2) && ($firstinvoice['type_2']==2) )
				{
					$show_parts=true;

					// labor
					$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// selling price text
					$selling_price_text = "Labor Only Price";
				}

				// 5: service with parts and labor
				if( ($firstinvoice['type_1']==3) && ($firstinvoice['type_2']==2) )
				{
					$show_parts=true;

					// labor
					$st = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

					// parts 
					$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

					// selling price text
					$selling_price_text = "Labor Only Price";
				}
			
				// payments
				$payments = $vujade->get_invoice_payments($invoiceid);
				if($payments['error']=="0")
				{
					unset($payments['error']);
					$pt = 0;
					foreach($payments as $payment)
					{
						$pt+=$payment['amount'];
					}
				}
				else
				{
					$pt=0;
				}

				// tax amount
				// this will be changing in the future
				//$tax_amount += $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
				//$st+=$tax_amount;

				$tax_amount=$pi['sales_tax'];

				$tax_rate=$pi['tax_rate'];

				// change orders
				$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

				// non-taxable co
				$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);
				$co+=$ntco;

				// engineering
				$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

				// permits
				$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

				// permit acq
				$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

				// shipping
				$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

				// discount
				//$discount+=$pi['discount'];
				//$discount = str_replace("-","",$discount);

				$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

				// retention
				$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
				$retention=str_replace("-","",$retention);

				// deposit
				$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
				//$deposit = str_replace("-","",$deposit);

				$base_sale += $st;
				$base_sale += $parts;
				$bsa[]=$st;
				$bsa[]=$parts;
				$ab = $engineering+$co+$shipping;
				//$ab=$ab+$discount;
				$additional_billing += $ab;
				$permit += $permits;
				$permit_acquisition += $pa;

				/* very old legacy
				// legacy
				$st=0;

				// legacy invoices will have: manufacture & install, labor, parts

				// check for manufacture and install
				$manufacture =  $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

				// labor
				$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

				// parts 
				$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

				// payments
				$payments = $vujade->get_invoice_payments($invoiceid);
				if($payments['error']=="0")
				{
					unset($payments['error']);
					$pt = 0;
					foreach($payments as $payment)
					{
						$pt+=$payment['amount'];
					}
				}
				else
				{
					$pt=0;
				}

				// tax amount
				$tax_amount=$firstinvoice['sales_tax'];

				$tax_rate=$pi['tax_rate'];

				// change orders
				$co += $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

				// engineering
				$engineering += $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

				// permits
				$permits += $vujade->get_invoice_line_items($invoiceid,'Permits',true);

				// permit acq
				$pa += $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

				// shipping
				$shipping += $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

				// discount
				$discount += $vujade->get_invoice_line_items($invoiceid,'Discount',true);

				// retention
				$retention += $vujade->get_invoice_line_items($invoiceid,'Retention',true);
				$retention=str_replace("-","",$retention);

				// deposit
				$deposit += $vujade->get_invoice_line_items($invoiceid,'Deposit',true);
				$deposit = str_replace("-","",$deposit);

				$base_sale += $manufacture;
				$base_sale += $labor;
				$base_sale += $parts;
				$bsa[]=$manufacture;
				$bsa[]=$labor;
				$bsa[]=$parts;
				$ab = $engineering+$co+$shipping;
				//$ab=$ab+$discount;
				$additional_billing += $ab;
				$permit += $permits;
				$permit_acquisition += $pa;
				*/
			}
			else
			{
				// not legacy

				// payments
				$payments = $vujade->get_invoice_payments($invoiceid);
				if($payments['error']=="0")
				{
					unset($payments['error']);
					$pt = 0;
					foreach($payments as $payment)
					{
						$pt+=$payment['amount'];
					}
				}
				else
				{
					$pt=0;
				}

				// tax amount
				$tax_amount=$pi['v_sales_tax'];

				$tax_rate=$pi['v_tax_rate'];

				// i4 amount
				$i4_amount+=$pi['i4_amount'];

				// engineering
				$engineering += $pi['v_engineering'];

				// permits
				$permits += $pi['v_permits'];

				// permit acq
				$pa += $pi['v_permit_acquisition'];

				// shipping
				$shipping += $pi['v_shipping'];

				// discount
				$discount += $pi['v_discount'];

				// retention
				$retention += $pi['v_retention'];
				$retention=str_replace("-","",$retention);

				// deposit
				$deposit += $pi['v_deposit'];
				$deposit = str_replace("-","",$deposit);

				$base_sale += $pi['v_sales_price'];
				$bsa[]=$pi['v_sales_price'];
				$ab = $engineering+$co+$shipping;
				$additional_billing += $ab;
				$permit += $permits;
				$permit_acquisition += $pa;
			}
		}
	}
	foreach($bsa as $bamnt)
	{
		$total_sale_price+=$bamnt;
	}

	if($discount<0)
	{
		$total_sale_price += $ab+$permits+$pa+$discount;
	}
	else
	{
		$total_sale_price += $ab+$permits+$pa-$discount;
		$discount = 0-$discount;
	}
	$net_profit = $total_sale_price - $total_cost - $project['commission'];

	// $markup = $total_sale_price/$total_cost;
	// net profit / sales price
	$markup = @($net_profit / $total_sale_price);
	//$markup = round($markup,2);
	$markup = floor($markup*100)/100;
	$markup+=1;
}
?>
                  <form method = "post" action = "costing_overview.php">
                  <div class="custom-container form">
                    <div class="row">
                      <div class="col-md-2"><span class="form-label">Date Opened</span></div>
                      <div class="col-md-2"><span class="form-label"><?php print $project['open_date']; ?></span></div>
                      <div class="col-md-2"><span class="form-label">Date Billed</span></div>
                      <div class="col-md-2"><input type='text' class="form-control datepicker-days"value = "<?php print $project['date_billed']; ?>" 
                        name = "date_billed" id = "date_billed" /></div>
                      <div class="col-md-2"><span class="form-label">Date Closed</span></div>
                      <div class="col-md-2"><input type='text' class="form-control datepicker-days" value = "<?php print $project['date_closed']; ?>"
                        name = "date_closed" id = "date_closed" /></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"><span class="form-label">Inventory</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($inventory_cost,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Base Sale</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($base_sale,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"><span class="form-label">Direct Purchases</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($total_dp,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Additional Billing</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($additional_billing,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"><span class="form-label">Indeterminate</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($indt,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Permit</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($permit,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Total Materials</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($tm,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"><span class="form-label">Permit Acquisition</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print number_format($permit_acquisition,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Discount</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print number_format($discount,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"><span class="form-label">Raw Labor</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($raw_labor,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Total Sale Price</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($total_sale_price,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"><span class="form-label">Labor Rider</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($labor_rider,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Total Labor</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($total_labor,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"><span class="form-label">Commission</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" name = "commission" id = "commission" value = "<?php print $project['commission']; ?>"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Machines</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($machines_total,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Outsource</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($total_outsource,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"><span class="form-label">Net Markup</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print round($markup,2); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Subcontract</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($total_subcontract,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Net Profit</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($net_profit,2,'.',','); ?>"></div>
                    </div>
                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Subtotal</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($col1_subtotal,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Materials Overhead</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($materials_overhead,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Labor Overhead</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($general_overhead,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Machines Overhead</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($machines_overhead,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>
                    
                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Outsource Overhead</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($buyout_overhead,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>

                    <div class="row">
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"><span class="form-label">Total Cost</span></div>
                      <div class="col-md-5ths"><input type="text" class="form-control" readonly="true" value = "<?php print @number_format($total_cost,2,'.',','); ?>"></div>
                      <div class="col-md-5ths"></div>
                      <div class="col-md-5ths"></div>
                    </div>
                    <?php
                    # must have edit permission to close this order
                    if($permissions['edit']==1)
                    {
                      ?>
                      <input type = "hidden" name = "id" value = "<?php print $id; ?>">
                      <input type = "hidden" name = "action" value = "2">
                    <div class="row">
                      <button class="btn btn-primary btn-lg pull-right">UPDATE</button>
                    </div>
                    <?php } ?>
                  </div>
                  </form>
                </div>
                </div>
              </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->
  <!-- BEGIN: PAGE SCRIPTS -->
<style>
  .col-xs-5ths,
  .col-sm-5ths,
  .col-md-5ths,
  .col-lg-5ths {
    position: relative;
    min-height: 1px;
    padding-right: 10px;
    padding-left: 10px;
  }
  .col-xs-5ths {
    width: 20%;
    float: left;
  }
  @media (min-width: 768px) {
    .col-sm-5ths {
      width: 20%;
      float: left;
    }
  }
  @media (min-width: 992px) {
    .col-md-5ths {
      width: 20%;
      float: left;
    }
  }
  @media (min-width: 1200px) {
    .col-lg-5ths {
      width: 20%;
      float: left;
    }
  }
  input[readonly="true"]{
    cursor: default !important;
  }
  .custom-container > .row{
    margin-bottom: 15px;
    padding: 0 15px;
  }
  .form-label{
    line-height: 39px;
  }
</style>
  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <script src="vendor/plugins/moment/moment.min.js"></script>
  <script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {
    "use strict";
    
    // Init Theme Core    
    Core.init();
    var n = $('#notes').html();
    $("#notes").html($.trim(n));
    $('#date_billed').val('<?php print $project['date_billed']; ?>');
    $('#date_closed').val('<?php print $project['date_closed']; ?>');
    $('.datepicker-days').datetimepicker({
      datePickdate: true,
      pickTime: false
    });
    $('#close_order').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#close-order-form',
      modal: true
    });
    $('#open_order').magnificPopup({
      type: 'inline',
      preloader: false,
      focus: '#open-order-form',
      modal: true
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
      e.preventDefault();
      $.magnificPopup.close();
    });
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>
