<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$s=array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$id = $_POST['id'];
	$type = $_POST['type'];
	$rate = $_POST['rate'];
	$order=$_POST['order'];
	$s[]=$vujade->update_row('labor',$id,'type',$type);
	$s[]=$vujade->update_row('labor',$id,'rate',$rate);
	$s[]=$vujade->update_row('labor',$id,'order',$order);
	$vujade->messages[]='Labor type updated.';
}
if($action==2)
{
	$id = $_POST['id'];
	$success=$vujade->delete_row('labor',$id);
	if($success==1)
	{
		$vujade->messages[]='Labor type deleted.';
	}
}
if($action==3)
{
	$type = $_POST['type'];
	$rate = $_POST['rate'];
	$order=$_POST['order'];
	$vujade->create_row('labor');
	$id = $vujade->row_id;
	$s[]=$vujade->update_row('labor',$id,'type',$type);
	$s[]=$vujade->update_row('labor',$id,'rate',$rate);
	$s[]=$vujade->update_row('labor',$id,'order',$order);
	$vujade->messages[]='Labor type created.';
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Estimate Labor Types - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=1;
        	require_once('site_setup_menu.php');
        	?>
        	
        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

				<i>Use this form to add, edit or delete labor types and rates for the estimate page.</i><br>

				<table class = "table">

				<tr>
				<td><strong>Labor Type</strong></td>
				<td><strong>Rate</strong></td>
				<td><strong>Order</strong></td>
				<td><strong>Save</strong></td>
				<td><strong>&nbsp;</strong></td>
				</tr>

				<form method = "post" action = "labor_setup.php">
				<tr>
				<td>
					<input type = "text" name = "type" style = "width:300px;height:30px;">
				</td>
				<td>
					<input type = "text" name = "rate" style = "width:100px;height:30px;">
				</td>
				<td>
					<input type = "text" name = "order" style = "width:100px;height:30px;">
				</td>
				<td>
				<input type = "hidden" name = "action" value = "3">
				<input type = "submit" value = "Save" class = "btn btn-primary" style = "">
				</form>
				</td>
				<td>
				&nbsp;
				</td>
				</tr>

				<tr>
				<td colspan="5">&nbsp;</td>
				</tr>

				<tr>
				<td><strong>Labor Type</strong></td>
				<td><strong>Rate</strong></td>
				<td><strong>Order</strong></td>
				<td><strong>Update</strong></td>
				<td><strong>Delete</strong></td>
				</tr>

				<?php
				$types = $vujade->get_labor_types();
				if($types['error']=="0")
				{
					unset($types['error']);
					foreach($types as $type)
					{
						print '<tr>';
						print '<td>';
						print '<form method = "post" action = "labor_setup.php">';
						print '<input type = "hidden" name = "action" value = "1">';
						print '<input type = "hidden" name = "id" value = "'.$type['database_id'].'">';
						print '<input type = "text" name = "type" value = "'.$type['type'].'" style = "width:300px;height:30px;">';
						print '</td>';
						
						print '<td>';
						print '<input type = "text" name = "rate" value = "'.$type['rate'].'" style = "width:100px;height:30px;">';
						print '</td>';

						print '<td>';
						print '<input type = "text" name = "order" value = "'.$type['order'].'" style = "width:100px;height:30px;">';
						print '</td>';

						print '<td>';
						print '<input type = "submit" value = "Update" class = "btn btn-primary">';
						print '</form>';
						print '</td>';

						print '<td>';
						print '<form method = "post" action = "labor_setup.php">';
						print '<input type = "hidden" name = "action" value = "2">';
						print '<input type = "hidden" name = "id" value = "'.$type['database_id'].'">';
						print '<input type = "submit" value = "Delete" class = "btn btn-danger">';
						print '</form>';
						print '</td>';

						print '</tr>';
					}
				}
				?>
				</table>
				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
