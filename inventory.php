<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$inventory_permissions = $vujade->get_permission($_SESSION['user_id'],'Inventory');
if($inventory_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

if(isset($_REQUEST['active']))
{
	$active = $_REQUEST['active'];
}
if(!in_array($active, array(1,2)))
{
	$active=1;
}
if($active==1)
{
	$act="true";
}
if($active==2)
{
	$act="false";
}
$inventory = $vujade->get_inventory(1,0,1,$act);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$title = "Inventory - ";
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Inventory</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
        	if($_REQUEST['m']==1)
        	{
        		$vujade->messages[]="Item deleted.";
        	}
        	$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<?php
					// active/inactive
					if($active==1)
					{
						print '<a href = "inventory.php?active=2" class = "btn btn-primary btn-sm">Show Deleted</a>';
					}
					if($active==2)
					{
						print '<a href = "inventory.php?active=1" class = "btn btn-primary btn-sm">Show Active</a>';
					}

					// create button
					if($inventory_permissions['create']==1)
					{
						?>
							<div class="widget-menu pull-right">
								<a href = "item.php?action=1" class = "btn btn-primary pull-right btn-sm">New</a>
							</div>
					<?php } ?>
				</div>
	        	<div class="panel-body bg-light">
					<?php 
					if($inventory['error']=="0")
					{
						unset($inventory['error']);
						?>
						<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>ID</strong></td>
									<td valign = "top"><strong>Description</strong></td>
									<td valign = "top"><strong>Category</strong></td>
									<td valign = "top"><strong>Type</strong></td>
									<td valign = "top"><strong>Size</strong></td>
									<td valign = "top"><strong>Cost</strong></td>
									<td valign = "top"><strong>Updated</strong></td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">
							<?php
							foreach($inventory as $i)
							{
								$link = 'item.php?id='.$i['database_id'];
						        print '<tr class = "clickableRow-row">';

						        print '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><a href = "'.$link.'">'.$i['inventory_id'].'</a></td>';

						        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = "">'.$i['description'].'</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['category'].'</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['type'].'</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
						        print $i['size'];
						        print '</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
						        print $i['cost'];
						        print '</td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$i['date_updated'].'</td>';

								print '</tr>';
							}

							?>
							</tbody>
						</table>
					<?php
					}
					else
					{
						$vujade->set_error('Could not fetch inventory from database. ');
						$vujade->set_error($inventory['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>
<script src="vendor/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"Tfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    $("tbody").on("click", ".clickableRow", function(){
    	window.document.location = $(this).attr("href");	
    }); 

});
</script>
<style>
.pagination {
	margin: 0px auto !important;
	padding-left: 20px !important;
}
</style>
</body>
</html>