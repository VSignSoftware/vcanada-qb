<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');
if($_SESSION['is_admin']==1)
{
	$permissions['read']=1;
}
if($permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();

// labor departments
$departments = $vujade->get_labor_types();

// production report data
$data = $vujade->get_production_report_data();

if($data['count']>0)
{
	unset($data['count']);
	unset($data['error']);
	unset($data['sql']);
	$estimates = array();
	foreach($data as $k => $v)
	{
		$temp = explode('^',$v);
		foreach($temp as $t)
		{
			if($t!='')
			{
				$estimates[]=$t;
			}
		}
	}
}

//print_r($estimates);

// generate temporary sql data for the report
$vujade->generic_query('truncate table temp_dept_hours');
foreach($estimates as $estimate_id)
{

	//print $estimate_id.'<br>';

	$estimate = $vujade->get_estimate($estimate_id,"estimate_id");
	$project = $vujade->get_project($estimate['project_id'],2);
	$job_status = $vujade->get_job_status($project['project_id']);
	if(!empty($job_status['installation_date']))
	{
		$ts = strtotime($job_status['installation_date']);
	}
	else
	{
		$ts=0;
	}
	if($project['status']=='Active Manufacturing')
	{
		$labor = $vujade->get_labor_for_estimate($estimate_id);

		if($labor['error']=="0")
		{
			foreach($labor as $l)
			{
				/*
				if(in_array($estimate_id,array('150162-03')))
				{
					print $estimate_id.'<br>';
					print $l['labor_id'].' '.$l['hours'].' '.$l['skipped'];
					print '<hr>';
				}
				*/
				if( ($l['labor_id']!="0") && ($l['labor_id']!='') && ($l['hours']!='') && ($l['hours']!='0') && ($l['skipped']=='0') )
				{
					
					$vujade->create_row('temp_dept_hours');
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'project',$estimate['project_id']);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'estimate',$estimate_id);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'labor_id',$l['labor_id']);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'hours',$l['hours']);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'ts',$ts);
				}
			}
		}

		// get temporary labor (this is set in the job status manufacturing tab)
		$labor2 = $vujade->get_temp_labor_for_estimate($estimate_id);
		if($labor2['error']=="0")
		{
			foreach($labor2 as $l)
			{
				if( ($l['labor_id']!="0") && ($l['labor_id']!='') )
				{
					$vujade->create_row('temp_dept_hours');
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'project',$estimate['project_id']);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'estimate',$estimate_id);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'labor_id',$l['labor_id']);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'hours',0);
					$s=$vujade->update_row('temp_dept_hours',$vujade->row_id,'ts',$ts);
				}
			}
		}
	}
}

//die;

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$title = 'Department Hours - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Department Hours</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "reports.php" class = "btn btn-primary btn-sm" style = "width:100px;">Back</a>

						<a href = "print_report_department_hours.php?section=ALL" target = "_blank" class = "btn btn-primary btn-sm" style = "width:100px;">Print All</a> 

						<a href = "print_report_department_hours.php?section=ALL-pb" target = "_blank" class = "btn btn-primary btn-sm pull-right" style = "width:200px;">Print All - 1 section per page</a>

					</div>
				</div>
	        	<div class="panel-body bg-light">
	        		<?php 
	        		if($departments['error']!="0")
	        		{
	        			$vujade->set_error('No labor departments found. ');
						$vujade->show_errors();
	        		}
	        		else
	        		{
		        		unset($departments['error']);
		        		print '<table class = "table" width = "100%">';
		        		foreach($departments as $dept)
		        		{
		    				print '<tr>';
		    				
		    				print '<td width = "50%" valign="top" style = "border:1px solid black;">';
		    				print '<h3>'.$dept['type'];

		    				print '<a href = "print_report_department_hours.php?section='.$dept['database_id'].'" target = "_blank" class = "btn btn-primary btn-sm pull-right" style = "width:100px;">Print Section</a>';

		    				print '</h3>';

		    				// print $dept['database_id'];
		    				
		    				// get job data and hours for every job in this department

		    				$section_data = $vujade->get_temp_dept_data($dept['database_id'],'',2);

		    				//print $section_data['sql'].'<br>';

		    				if($section_data['count']>0)
		    				{
		    					unset($section_data['count']);
		    					unset($section_data['error']);
		    					unset($section_data['sql']);

		    					$project_hours = 0;
		    					$remaining_hours = 0;
		    					$allotted_total = 0;
		    					$remaining_total = 0;

		    					print '<table width = "100%" class = "table table-border">';
			    				print '<tr>';
			    				print '<td width = "10%">';
			    				print '#';
			    				print '</td>';
			    				print '<td width = "60%">';
			    				print 'Name';
			    				print '</td>';
			    				print '<td width = "10%">';
			    				print 'Hours Allotted';
			    				print '</td>';
			    				print '<td width = "10%">';
			    				print 'Hours Remaining';
			    				print '</td>';

			    				print '<td width = "10%">';
			    				print 'Shipping Date';
			    				print '</td>';

			    				print '<td width = "10%">';
			    				print 'Due Date';
			    				print '</td>';
			    				print '</tr>';

			    				foreach($section_data as $sd)
		    					{
		    						$allotted_total+=$sd['hours'];

		    						$p = $vujade->get_project($sd['project'],2);
		    						$js = $vujade->get_job_status($sd['project']);

	    							print '<tr class = "click" id = "'.$sd['project'].'">';
	    							print '<td valign = "top" width = "10%">';
			    					print $sd['project'];
			    					print '</td>';

			    					// project name
			    					print '<td width = "70%">';
			    					print $p['site'];
			    					print '</td>';

			    					// hours allotted
			    					print '<td width = "10%">';
			    					print $sd['hours'];
			    					print '</td>';

			    					// remaining hours
		    						$tc_data = $vujade->get_actual_labor($dept['database_id'],$sd['project']);
		    						$remaining_hours=0;
			    					$remaining_hours=$sd['hours']-$tc_data['total'];
			    					print '<td width = "10%">';
			    					if($remaining_hours>0)
			    					{
			    						print $remaining_hours;
			    					}
			    					else
			    					{
			    						print '<font color = "red">'.$remaining_hours.'</font>';
			    					}
			    					print '</td>';

			    					$remaining_total+=$remaining_hours;

			    					// sghipping date
			    					print '<td width = "10%">';
			    					print $js['shipping_date'];
			    					print '</td>';

			    					// due date
			    					print '<td width = "10%">';
			    					print $js['installation_date'];
			    					print '</td>';

			    					print '</tr>';
			    					$remaining_hours=0;
		    					}

		    					// summary rows
		    					print '<tr>';
			    				print '<td colspan = "2" style = "text-align:right;font-weight:bold;">Total';
		    					print '</td>';
		    					print '<td colspan = "1">'.$allotted_total;
		    					print '</td>';
		    					print '<td colspan = "1">'.$remaining_total;
		    					print '</td>';
		    					print '<td colspan = "1">&nbsp;';
		    					print '</td>';
			    				print '</tr>';

		    					print '</table>';
		    				}

		    				print '</td>';

		    				print '</tr>';
		        		}
		        		print '</table>';
		        	}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    $('.click').click(function()
    {
    	var id = this.id;
    	window.location.href="project.php?id="+id;
    });

});
</script>

</body>
</html>