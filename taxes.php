<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// save updates
$save = 0;
if(isset($_REQUEST['save']))
{
	$save = $_REQUEST['save'];
}
if($save==1)
{
	if($accounting_permissions['edit']==1)
	{
		$success = array();

		// skip the following form post vars
		$skip_actions = array('action','sort','start','city','state','county','rate','save');
		foreach($_POST as $k => $v)
		{
			if(!in_array($k, $skip_actions))
			{
				# key is equal to the database id
				# value is equal to the rate
				
				# update each 
				$success[] = $vujade->update_row('tax',$k,'rate',$v);
			}
		}
		$vujade->messages[]="Taxes Updated";
	}
	else
	{
		$vujade->messages[]="You do not have permissions to update taxes.";
	}
}

$taxes = $vujade->get_all_taxes();

$setup=$vujade->get_setup();

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = 'City Tax Rates - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">City Tax Rates</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
        	if($_REQUEST['m']==1)
        	{
        		$vujade->messages[]="Tax rate updated";
        	}
			$vujade->show_messages();
			if($_REQUEST['m']==2)
        	{
        		$vujade->errors[]="Tax rate deleted";
        	}
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu pull-right">
						<a href = "tax.php?new=1" class = "btn btn-primary pull-right btn-sm">New</a>
					</div>
				</div>
	        	<div class="panel-body bg-light">
					<?php 
					if($taxes['error']=="0")
					{
						unset($taxes['error']);
						unset($taxes['sql']);
						?>
						<table id="datatable" class="employees-table table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>City</strong></td>
									<td valign = "top"><strong>State</strong></td>
									<?php
									if($setup['is_qb']!=1)
									{
									?>
										<td valign = "top"><strong>County</strong></td>
									<?php } ?>
									<td valign = "top"><strong>Rate</strong></td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">
						    <form method = "post" action = "taxes.php">	
							<?php
							foreach($taxes as $tax)
							{
								$link = 'tax.php?id='.$tax['database_id'];
						        print '<tr class = "clickableRow-row">';

						        print '<td valign = "top" style = ""><a href = "'.$link.'">'.$tax['city'].'</a></td>';

						        print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$tax['state'].'</td>';

						        if($setup['is_qb']!=1)
						        {
						        	print '<td class="clickableRow" href="'.$link.'" valign = "top" style = "">'.$tax['county'].'</td>';
						        }

						        print '<td valign = "top" style = "">';
						        
						        print '<span style = "">'.$tax['rate'].'</span>';

						        print '</td>';

								print '</tr>';
							}

							?>
							</tbody>
						</table>

						<input type = "hidden" name = "save" value = "1">

						</form>

					<?php
					}
					else
					{
						$vujade->set_error('Could not fetch taxes from database. ');
						$vujade->set_error($taxes['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    /**/
    $(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    }); 

});
</script>

</body>
</html>