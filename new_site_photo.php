<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['project_id'];
$project_id = $id;
$post_result = '';

if(isset($_POST['upload'])) 
{
    $post_result = $vujade->site_photo_upload($id, $_POST, $_FILES);
}

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$folder_id=$_REQUEST['folder_id'];

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# 
if($action==1)
{

}

$sfs = $vujade->get_site_photos($project_id,0,1);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$nav = 3;
$title = "Site Photos - ";
require_once('tray_header.php'); 
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
	<!-- Begin: Content -->
	<header id="topbar">

		<div class="topbar-left">

			<ol class="breadcrumb">

				<li class="crumb-active">

					<a href="#">Project #: <?php print $project_id; ?> Project Name: <?php print $project['site']; ?></a>

				</li>

			</ol>

		</div>

	</header>
<section id="content">

	<div id="photoContainer"  class="admin-form theme-primary">
		<div class="panel heading-border panel-primary">
			<div class="panel-body bg-light">
				<div style="overflow: hidden">
				<strong>Upload site photos</strong>
				<select id = "category" class="form-control" style="max-width: 200px; float:right; margin-right: 5px">
					<option value = "0">-Select-</option>
					<option value = "1">Survey</option>
					<option value = "2">Manufacturing</option>
					<option value = "3">Completion</option>
				</select>
				</div>
				<div style = "padding:5px;width:100%;">

					<!--<script src="vendor/plugins/dropzone/dropzone.min.js"></script>-->

					<div id = "dropzone_div" style = "display:none; width:100%;margin-bottom:15px;height:375px;overflow:auto;">
						<form id="dz" onclick="console.log($(this).attr('action','photo_upload.php?category=?'+ $('#category').val() +'&project_id=<?php print $project_id; ?>'))" action="photo_upload.php?category=1&project_id=<?php print $project_id; ?>" class="dropzone">
						    <div class="dz-message" style="width:100%;margin: 40px auto;text-align: center;">
                                <h4 class="hidden-sm hidden-xs">Drag Photos to Upload</h4>
                                <span class="hidden-sm hidden-xs">Or click to browse</span>
                                <h4 class="hidden-md hidden-lg">Tap to add photos</h4>
                            </div>
						</form>
					</div>
				</div>

				<div style="margin-top:15px;width:100%;clear:both;">
					<div style="float:left">   </div>
					<div style = "float:right;">
						<a href = "project_site_photos.php?id=<?php print $project_id; ?>" class = "button200 btn btn-lg btn-primary" style = "width:100px;">DONE</a>
					</div>

				</div>
			</div>
		</div>
	</div>


<!--		<div style = "width:920px;margin-bottom:15px;height:175px;overflow:auto;" id = "dz" class="">
		</div>-->

		</div>

		</div>

	</div>

<div class = "push">
</div>

<link rel="stylesheet" href="vendor/plugins/dropzone/css/dropzone.css">
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/dropzone/dropzone.min.js"></script>
<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
<!-- Theme Javascript --><script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script><script src="assets/js/main.js"></script>
<script>
$(document).ready(function()
{
	Core.init();
	function findBootstrapEnvironment() {
        var envs = ['xs', 'sm', 'md', 'lg'];

        var $el = $('<div>');
        $el.appendTo($('body'));

        for (var i = envs.length - 1; i >= 0; i--) {
            var env = envs[i];

            $el.addClass('hidden-'+env);
            if ($el.is(':hidden')) {
                $el.remove();
                return env;
            }
        }
    }
	$('#category').change(function()
	{
		var v = $(this).val();
		if(v!='')
		{
			$('#dropzone_div').show();
			var env = findBootstrapEnvironment();
			if(env == "xs" || env == "sm"){
			    //update text and update color
			    
			}
		}
		else
		{
			$('#dropzone_div').hide();
		}
	});

	var pid = <?php print $project_id; ?>;

	Dropzone.options.dz = 
	{
	  init: function() 
	  {
	    this.on("processing", function(file) {
	    	var formaction = 'photo_upload.php?category=' + $('#category').val() + '?&project_id=' + pid;
	      	this.options.url = formaction;
	    });

	    this.on("addedfile", function (file) 
  		{
        	var existing = [];
        	// get existing files and add to array
        	<?php
        	if($sfs['error']=="0")
        	{
        		unset($sfs['error']);
        		foreach($sfs as $df)
        		{
        			?>
        			existing.push("<?php print $df['filename']; ?>");
        			<?php
        		}
        	}
        	?>
        	var in_array = existing.indexOf(file.name);
    		if(in_array>=0)
    		{
    			if(confirm("A file with the same name " + file.name + " already exists for this design. Do you want to overwrite it?"))
    			{
    				this.processQueue();
                	//alert('1');
                	// user confirmed; do nothing further; upload.php handles overwriting
	            }
	            else
	            {
	            	this.removeFile(file);
                	return false;
	            }
    		}
    		else
    		{
    			//alert('not in array: '+file.name);
    			this.processQueue();
    		}
        });


	  }
	};
	
});
</script>
	</section>
</section><!-- BEGIN: PAGE SCRIPTS --><!-- jQuery -->
</body>
</html>
