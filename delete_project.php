<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# delete the project
if($action==1)
{
	$messages=$vujade->delete_project_data($id);
	foreach($messages as $msg)
	{
		$vujade->set_message($msg);
	}
}
if($action==0)
{
	$vujade->errors[]='<h3>Are you sure you want to delete this project? This action cannot be undone.</h3><a href = "delete_project.php?action=1&id='.$id.'" class = "btn btn-lg btn-danger" style = "width:100px;margin-right:15px;">YES</a> <a href = "project.php?id='.$id.'" class = "btn btn-lg btn-success" style = "width:100px;">CANCEL</a><center></p>';
}
$title = "Delete Project";
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$title = "Delete Project - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Delete Project <?php print $project['project_id']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
      	<?php 
		if($action==0)
		{
			$vujade->show_errors();
		} 
		if($action==1)
		{
			$vujade->show_errors();
			$vujade->show_messages();
		} 
		?>
      </section>

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    
		
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
