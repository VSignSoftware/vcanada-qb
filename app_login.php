<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();

$logo = $vujade->get_logo();
if(!isset($charset))
{
	$charset = "utf-8";
}

if( (isset($_REQUEST['u'])) && (isset($_REQUEST['p'])) )
{
	$username = $_REQUEST['u'];
	$password = $_REQUEST['p'];
	$vujade->check_empty($username,'Username');
	$vujade->check_empty($password,'Password');
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		$vujade->login($username,$password);
		//$e = $vujade->get_error_count();
	}
	//print $e;
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
<meta charset="<?php print $charset; ?>">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Login</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<?php
if($e>0)
{
	print '<div class = "alert alert-danger">';
	//$vujade->errors[]='request vars: '.$_REQUEST['u'].' '.urlencode($_REQUEST['p']);
	foreach($vujade->errors as $err)
	{
		print $err.'<br>';
	}
	print '</div>';
}
$vujade->show_errors();
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

</body>
</html>