<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// 
$project_id = $_REQUEST['project_id'];

// this estimate will be replaced with the old estimate
$estimateid = $_REQUEST['estimateid'];
$estimate = $vujade->get_estimate($estimateid);

// can't be invalid
if($estimate['error']!=0)
{
	$vujade->errors[]="Invalid estimate.";
	$vujade->show_errors();
	die;
}
?>

<!-- form -->
<form id = "form">
	<table class = "project_search_results" style = "width: 785px;">
		<tr class = "project_search_results">
			<td class = "project_search_results">Estimate Number:</td>
			<td class = "project_search_results">
				<input type = "text" name = "transfer_from" id = "transfer_from" style = "width:400px;">
			<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
			<div id = "error_2" style = "display:none;">This estimate number is invalid.</div>
			</td>
		</tr>

		<tr class = "project_search_results">
			<td class = "project_search_results">&nbsp;</td>
			<td class = "project_search_results">
				<div id = "working"></div>
				<input type = "submit" id = "transfer" value = "TRANSFER" class = "sbt200">
			</td>
		</tr>
	</table>
</form>

<!-- jquery to send proposal via ajax -->
<script src="js/jquery1.11.0.js"></script>
<script>
$(function() 
{

	$('#transfer_from').focus();

    $('#transfer').click(function(e)
    {
    	e.preventDefault();
	
    	var transfer_to = <?php print $estimateid; ?>;

		// validation
		var transfer_from = $('#transfer_from').val();
		var error = 0;
		if(transfer_from=="")
		{	
			$('#error_1').css('background-color','#C60F13');
			$('#error_1').css('color','white');
			$('#error_1').css('font-weight','bold');
			$('#error_1').css('font-size','16px');
			$('#error_1').css('padding','3px');
			$('#error_1').show();
			error++;
			return false;
		}
		else
		{
			$('#error_1').hide();
			// validate the old estimate id
			/* doesn't work right; validated now in jq.transfer_estimate.php
			$.post( "jq.check_estimate.php", { id: transfer_from })
			.done(function( response ) 
			{
			    //$('#working').css('background-color','#36D86C');
			    //$('#working').html('');
			    //$('#working').html(response);
			    if(response!=1)
			    {
			    	$('#error_2').css('background-color','#C60F13');
					$('#error_2').css('color','white');
					$('#error_2').css('font-weight','bold');
					$('#error_2').css('font-size','16px');
					$('#error_2').css('padding','3px');
					$('#error_2').show();
					error++;
					$('#error_2').append(' '+error);
					$('#e2').val('1');
			    }
			    else
			    {
			    	error = 0;
			    	$('#error_2').hide();
			    	$('#e2').val('0');
			    }
			});
			*/

			$('#working').css('padding','3px');
			$('#working').css('background-color','#E6A728');
			$('#working').css('color','white');
			$('#working').css('font-weight','bold');
			$('#working').css('font-size','16px');
			$('#working').html('Working...');
			$.post( "jq.transfer_estimate.php", { transfer_from: transfer_from, transfer_to: transfer_to })
			.done(function( response ) 
			{
			    $('#working').css('background-color','#36D86C');
			    $('#working').html('');
			    $('#working').html(response);
			    if(response=="Success")
			    {
			    	window.location="project_estimates.php?id=<?php print $project_id; ?>&estimateid=<?php print $estimateid; ?>";
			    }
			});
		}
	});

});

</script>