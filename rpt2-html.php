<?php
 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
 
$begin = $_REQUEST['begin'];
$end = $_REQUEST['end'];
$project_id = $_REQUEST['project_id'];
$title = "Labor Sort Report";

# check empty
$vujade->check_empty($begin,'Begin Date');
$vujade->check_empty($end,'Begin End');
$vujade->check_empty($project_id,'Project Number');

# validate project number
$project = $vujade->get_project($project_id,2);
if($project['error']!="0")
{
	$vujade->errors[]="Project Number is invalid.";
}

$e = $vujade->get_error_count();
if($e>0)
{
	$action=0;
}

$pateHTML .= '
 
 <!DOCTYPE html>
 <head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <title>PDF Report</title>
 <meta name="description" content="">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <style>
 .bordered
 {
 padding-bottom:3px;
 border-bottom: 1px solid #cecece;
 }
 .header
 {
 font-weight: bold;
 font-size: 14px;
 }
 .header_graybg
 {
 background-color: #cecece;
 font-weight: bold;
 font-size: 14px;
 padding-bottom:3px;
 border-bottom: 1px solid #cecece;
 }
 
 
 table {font-family: DejaVuSansCondensed; font-size: 11pt; line-height: 1.2;
 margin-top: 2pt; margin-bottom: 5pt;
 border-collapse: collapse;  }
 
 thead {	font-weight: bold; vertical-align: bottom; }
 tfoot {	font-weight: bold; vertical-align: top; }
 thead td { font-weight: bold; }
 tfoot td { font-weight: bold; }
 
 thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }
 
 .headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
 .footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
 
 th {	font-weight: bold;
 vertical-align: top;
 text-align:left;
 padding-left: 2mm;
 padding-right: 2mm;
 padding-top: 0.5mm;
 padding-bottom: 0.5mm;
 }
 
 td {
 	padding-left: 2mm;
	vertical-align: top;
	text-align:left;
	padding-right: 2mm;
	padding-top: 0.5mm;
	padding-bottom: 0.5mm;
 }
 
 th p { text-align: left; margin:0pt;  }
 td p { text-align: left; margin:0pt;  }
  .center-text{ text-align: center; }
 
 </style>
 
 </head>
 <body>
 <div id="mainContent">
 	<div id="content">
	 	<br style="clear: both;"/>
		 <h2 class="signInText  center-text">'.$title.'</h2>
';

# date/day info
$startts = strtotime($begin);
$endts = strtotime($end);
$days = $vujade->get_dates_in_range($startts,$endts);
$end_date = $end;
$mysqli = $vujade->mysqli;

$total_st=0;
$total_ot=0;
$total_dt=0;

$total_vacation=0;
$total_holiday=0;

$total_paid;

$pateHTML .= '
<center  class="center-text"><span style = "font-weight:bold;font-size:16px; text-align: center;">Labor Sort '.$begin.' to '.$end.'</span></center>
<table width = "100%">
<tr class = "header_graybg">
    <td width="15%">Job No</td>
    <td width="20%">Employee</td>
    <td width="6%">S.T.</td>
    <td width="6%">Rate</td>
    <td width="6%">O.T.</td>
    <td width="6%">Rate</td>
    <td width="6%">D.T.</td>
    <td width="6%">Rate</td>
    <td width="6%">Vac</td>
    <td width="6%">Hol</td>
    <td width="15%">Total Paid</td>
</tr>
<tr>
    <td colspan = "11"><strong>'.$project_id.'</strong></td>
</tr>';

$employee_ids = array();
    $sql = "SELECT * FROM `timecards` WHERE `date_ts` >= '$startts' AND `date_ts` <= '$endts' AND `project_id` = '$project_id' ORDER BY `date_ts`";    
$result = $mysqli->query($sql);
if( (!$result) || ($result->num_rows<1) )
{
    $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
    //print '<p>Error: '.$error.'</p>';
}
else
{
    while($rows = $result->fetch_assoc())
    {
        $employee_ids[]=$rows['employee_id'];
    }
    $employee_ids=array_unique($employee_ids);
    $c1 = count($employee_ids);
    if($c1>0)
    {
        foreach($employee_ids as $employee_id)
        {
            $person = $vujade->get_employee($employee_id,1);
            $rate_data = $vujade->get_employee_current_rate($employee_id);

            # loop through each day in the range and get hours worked for the project
            foreach($days as $day)
            {
                $day_total = 0;
                $day_total_paid; 

                $day_vacation=0;
                $day_holiday=0;

                $day_st=0;
                $day_ot=0;
                $day_dt=0;

                $ts = strtotime($day);
                $sql = "SELECT * FROM `timecards` WHERE `date_ts` = '$ts' AND `employee_id` = '$employee_id' AND `project_id` = '$project_id' ORDER BY `date_ts`"; 

                $result = $mysqli->query($sql);
                if( (!$result) || ($result->num_rows<1) )
                {
                    $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
                }
                else
                {
                    while($rows = $result->fetch_assoc())
                    {
                        $date = $rows['date'];
                        $date_ts = $rows['date_ts'];
                        $type = $rows['type'];
                        $project_id = $rows['project_id'];
                        $start = $rows['start'];
                        $end = $rows['end'];
                        $standard_time = $rows['standard_time'];
                        $over_time = $rows['over_time'];
                        $double_time = $rows['double_time'];

                        $skip = array('Holiday','Vacation');
                        if(in_array($type, $skip))
                        {
                            if($type=="Holiday")
                            {
                                $day_holiday+=$standard_time;
                            }
                            if($type=="Vacation")
                            {
                                $day_vacation+=$standard_time;
                            } 
                        }
                        else
                        {
                            $day_st+=$standard_time;
                        }
                        
                        $day_ot+=$over_time;
                        $day_dt+=$double_time;

                        # 
                        $day_total+=$standard_time;
                        $day_total+=$over_time;
                        $day_total+=$double_time;
                    }

                    # row html output
                    $pateHTML.= '<tr>';

                    # blank cells
                    $pateHTML.= '<td>';
                    $pateHTML.= '</td>';

                    # name and date
                    $pateHTML.= '<td>';
                    $pateHTML.= $person['fullname'];
                    $pateHTML.= '</td>';

                    # standard hour running total
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_st,2,'.',',');
                    $pateHTML.= '</td>';

                    # standard rate
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($rate_data['rate'],2,'.',',');
                    $pateHTML.= '</td>';

                    # overtime
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_ot,2,'.',',');
                    $pateHTML.= '</td>';

                    # overtime rate
                    $pateHTML.= '<td>';
                    $otrate = $rate_data['rate']*1.5;
                    $pateHTML.= @number_format($otrate,2,'.',',');
                    $pateHTML.= '</td>';

                    # double time
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_dt,2,'.',',');
                    $pateHTML.= '</td>';

                    # double time rate
                    $pateHTML.= '<td>';
                    $dtrate = $rate_data['rate']*2;
                    $pateHTML.= @number_format($dtrate,2,'.',',');
                    $pateHTML.= '</td>';

                    # vacation
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_vacation,2,'.',',');
                    $pateHTML.= '</td>';

                    # holiday
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_holiday,2,'.',',');
                    $pateHTML.= '</td>';

                    # total paid
                    $st_pay = $rate_data['rate'] * $day_st;
                    $ot_pay = $otrate * $day_ot;
                    $dt_pay = $dtrate * $day_dt;
                    $h_pay = $rate_data['rate'] * $day_holiday;
                    $v_pay = $rate_data['rate'] * $day_vacation;
                    $day_total_paid=($st_pay+$ot_pay+$dt_pay+$h_pay+$v_pay);
                    $pateHTML.= '<td>';
                    $pateHTML.= @number_format($day_total_paid,2,'.',',');
                    $pateHTML.= '</td>';
                    $pateHTML.= '</tr>';

                    $total_st+=$day_st;
                    $total_ot+=$day_ot;
                    $total_dt+=$day_dt;

                    $total_vacation+=$day_vacation;
                    $total_holiday+=$day_holiday;

                    $total_paid+=$day_total_paid;
                }
            }
        }

        $pateHTML.= '</table>';

        //$pateHTML.= '<table width = "100%">';

        $pateHTML.='<div style = "width:100%;text-align:right;font-weight:bold;">';
        $pateHTML.='<div style = "width:270px;text-align:right;float:left;">';
        $pateHTML.= @number_format($total_st,2,'.',',');
        $pateHTML.='</div>';

        $pateHTML.='<div style = "width:95px;text-align:right;float:left;">';
        $pateHTML.= @number_format($total_ot,2,'.',',');
        $pateHTML.='</div>';
        $pateHTML.='<div style = "width:92px;text-align:right;float:left;">';
        $pateHTML.= @number_format($total_dt,2,'.',',');
        $pateHTML.='</div>';
        $pateHTML.='<div style = "width:94px;text-align:right;float:left;">';
        $pateHTML.= @number_format($total_vacation,2,'.',',');
        $pateHTML.='</div>';
        $pateHTML.='<div style = "width:44px;text-align:right;float:left;">';
        $pateHTML.= @number_format($total_holiday,2,'.',',');
        $pateHTML.='</div>';
        $pateHTML.='<div style = "width:85px;text-align:left;float:left;margin-left:14px;">';
        $pateHTML.= @number_format($total_paid,2,'.',',');
        $pateHTML.='</div>';

        $pateHTML.='</div>';

        /*
        # summary rows
        $pateHTML.= '<tr>';
        $pateHTML.= '<td colspan = "11"><hr>';
        $pateHTML.= '</td>';
        $pateHTML.= '</tr>';
        $pateHTML.= '<tr>';
        $pateHTML.= '<td>';
        $pateHTML.= '</td>';
        $pateHTML.= '<td>';
        $pateHTML.= '</td>';
        $pateHTML.= '<td colspan = "2">';
        $pateHTML.= @number_format($total_st,2,'.',',');
        $pateHTML.= '</td>';
        $pateHTML.= '<td colspan = "2">';
        $pateHTML.= @number_format($total_ot,2,'.',',');
        $pateHTML.= '</td>';
        $pateHTML.= '<td colspan = "2">';
        $pateHTML.= @number_format($total_dt,2,'.',',');
        $pateHTML.= '</td>';
        $pateHTML.= '<td colspan = "2">';
        $pateHTML.= @number_format($total_vacation,2,'.',',');
        $pateHTML.= '</td>';
        $pateHTML.= '<td colspan = "2">';
        $pateHTML.= @number_format($total_holiday,2,'.',',');
        $pateHTML.= '</td>';
        $pateHTML.= '<td colspan = "2">';
        $pateHTML.= @number_format($total_paid,2,'.',',');
        $pateHTML.= '</td>';
        $pateHTML.= '</tr>';
        $pateHTML.= '</table>';
        */
    }
}     
    
$pateHTML .= '
   			</div>
    	</div>
    </body>
</html>';
    