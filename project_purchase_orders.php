<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$po_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($po_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup(1);

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$show=0;
$pos = $vujade->get_purchase_orders($id);
if($pos['error']=='0')
{
	unset($pos['error']);
	$show=1;
	$posids = array();
	foreach($pos as $po)
	{
		$posids[]=$po['database_id'];
	}
	$posids=array_unique($posids);
	sort($posids);
	$firstpo = $vujade->get_purchase_order($posids[0]);
	if($firstpo['error']!="0")
	{
		$show=0;
		$firstpo['error']=1;
	}
	else
	{
		$poid = $posids[0];
	}
}
else
{
	$show=0;
	$firstpo['error']=1;
}

if(isset($_REQUEST['poid']))
{
	$firstpo = $vujade->get_purchase_order($_REQUEST['poid']);
	$poid = $_REQUEST['poid'];
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

$s = array();
# delete purchase order
if($action==1)
{
	if($setup['is_qb']==1)
	{
		$poid = $_REQUEST['poid'];
		$po = $vujade->get_purchase_order($poid);
		$list_id = $po['list_id'];
		$transaction_id = $po['transaction_id'];
		
		# delete the duplicate copy in costing
		$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);
		$s[]=$vujade->delete_row('costing_purchase_orders',$cpo['database_id']);

		// delete the mysql parent row
		// delete_row($table,$id,$no_limit=0,$idcol='id')
		$s[]=$vujade->delete_row('quickbooks_purchaseorder',$poid,0,'ID');

		# delete all line items
		$sql = "DELETE FROM `quickbooks_purchaseorder_lineitem` WHERE `Parent_ID` = '$poid'";
		$vujade->generic_query($sql,$debug);

		// delete from mysql qb queue
		$sql = "DELETE FROM `quickbooks_queue` WHERE `ident` = '$poid' AND `dequeue_datetime` IS NULL";
		$vujade->generic_query($sql,$debug);

		// delete from quickbooks
		if(!empty($po['edit_sequence']))
		{
			// qb config
			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_DEL_TXN, $transaction_id, 0, 'PurchaseOrder');
		}
	}
	else
	{
		// non qb version
		$poid = $_REQUEST['poid'];
		$po = $vujade->get_purchase_order($poid);

		# delete the duplicate copy in costing
		$cpo = $vujade->get_costing_purchase_order($po['purchase_order_id']);
		$s[]=$vujade->delete_row('costing_purchase_orders',$cpo['database_id']);

		$s[]=$vujade->delete_row('purchase_orders',$poid);

		# delete all line items (if materials)
		if($po['type']=="Materials")
		{
			$sql = "DELETE FROM `purchase_order_items` WHERE `purchase_order_id` = '$poid'";
			$vujade->generic_query($sql);
		}
		else
		{
			# delete all outsourced items and all subcontracted items
			$sql = "DELETE FROM `purchase_order_outsource` WHERE `purchase_order_id` = '$poid'";
			$vujade->generic_query($sql);
		}
	}
	
	$show=0;
	$pos = $vujade->get_purchase_orders($id);
	if($pos['error']=='0')
	{
		unset($pos['error']);
		$show=1;
		$posids = array();
		foreach($pos as $po)
		{
			$posids[]=$po['database_id'];
		}
		$firstpo = $vujade->get_purchase_order($posids[0]);
		$poid = $posids[0];
		$posids=array_unique($posids);
	}
	else
	{
		$show=0;
		$firstpo['error']=1;
	}
}

if($setup['purchase_order_email_message']=="")
{
	$default_msg = "Please find attached Proposal ".$proposal['proposal_id']." for your review and approval.";
}
else
{
	$default_msg = $setup['purchase_order_email_message'];
}

// vendor map search button
$vendor_map_search=true;

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 8;
$section = 3;
$title = "Purchase Orders - ".$project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">
<?php require_once('project_left_tray.php'); ?>
<div class="tray tray-center" style = "width:100%;border:0px solid red;">
<div class="pl15 pr15" style = "width:100%;">
<?php require_once('project_right_tray.php'); ?>

<div class="row">

	<!-- left hand side list of po's -->
	<div class="col-md-2" style="width:200px;">
		<div class="panel panel-primary panel-border top">
			<div class="panel-heading">
				<span class="panel-title">PO List</span>
				<div class="widget-menu pull-right">
					<?php
						if($po_permissions['create']==1)
						{
							?>
							<a class = "btn btn-sm btn-primary" href = "new_purchase_order.php?project_id=<?php print $id; ?>" title = "Add a new purchase order">New</a>
					<?php } ?>
				</div>
			</div>
			<div class="panel-body">
				<?php
				if($show==1)
				{
					print '<table>';
					unset($pos['error']);
					foreach($posids as $pid)
					{
						$po = $vujade->get_purchase_order($pid);
						if($po['error']=="0")
						{
							unset($po['error']);
							if($poid==$pid)
							{
								$bgcolor = "cecece";
							}
							else
							{
								$bgcolor = "ffffff";
							}
							print '<tr bgcolor = "'.$bgcolor.'">';
							print '<td valign = "top">';
							$n=strtotime('now');
							$tdd = strtotime($po['date']);
							if($n>$tdd)
							{
								$color = "#f7584c";
							}
							else
							{
								$color="#288BC3";
							}
							$display_date = date('m/d/Y',$tdd);
							print '<a class = "linknostyle" href = "project_purchase_orders.php?id='.$id.'&poid='.$po['database_id'].'">';
							print '<font color = "'.$color.'">'.$display_date.'</font>';
							print '</a>';

							print '</td>';
							print '<td valign = "top">&nbsp;</td>';
							print '<td valign = "top">';

							print '<a class = "linknostyle" href = "project_purchase_orders.php?id='.$id.'&poid='.$po['database_id'].'">';
							print $po['purchase_order_id'];
							print '</a>';

							print '</td>';
							print '</tr>';
						}
					}
					print '</table>';
				}
				?>
			</div>
		</div>
	</div>

	<!-- right hand side detail of selected po -->
	<div id = "" class="col-md-9" style="margin-left:5px;border:0px solid red;padding:0px;">

		<?php
		if($show==1)
		{
			?>
			<div class="panel panel-primary panel-border top">
				<div class="panel-heading">
					<span class="panel-title">PO #<?php print $firstpo['purchase_order_id']; ?></span>
					<div class="widget-menu pull-right">

						<!-- edit btn -->
						<?php
						if($po_permissions['edit']==1)
						{
						?>
							<a class = "btn btn-sm btn-success" href = "edit_purchase_order.php?project_id=<?php print $id; ?>&poid=<?php print $poid; ?>" title = "Edit Purchase Order">Edit</a>
						<?php } ?>

						<!-- email btn -->
						<a class = "plus-update email-btn btn-sm btn-primary btn" id = "email-po" href="#email-po-form">Email</a>

						<!-- email modal -->
						<div id = "email-po-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:450px;">
							<!-- form -->
							<form id = "email-form">
								<table class = "table">
									<tr class = "project_search_results">
										<td class = "project_search_results">Send To</td>
										<td class = "project_search_results">
											<input type = "text" name = "send_to" id = "send_to" class = "form-control" value = "">
										<div id = "error_1" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
										</td>
									</tr>

									<tr class = "project_search_results">
										<td class = "project_search_results">Subject</td>
										<td class = "project_search_results">
											<input type = "text" name = "subject" id = "subject" value = "<?php print $po['project_id']; ?> Purchase Order" class = "form-control">
											<div id = "error_2" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
										</td>
									</tr>

									<tr class = "project_search_results">
										<td valign = "top">Message</td>
										<td class = "project_search_results">
											<textarea name = "msg" id = "msg" class = "form-control" style = "height:200px;">
												<?php print $default_msg; ?>
											</textarea><div id = "error_3" style = "display:none;" class = "alert alert-danger">This field cannot be empty.</div>
										</td>
									</tr>

									<tr class = "project_search_results">
										<td class = "project_search_results">&nbsp;</td>
										<td class = "project_search_results">
											<div id = "working"></div>
											<a href = "#" id = "send" class = "btn btn-success btn-lg">Send</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						
						<!-- print btn -->
						<a class = "btn btn-sm btn-primary" href = "print_purchase_order.php?id=<?php print $id; ?>&poid=<?php print $poid; ?>" target = "_blank" title = "Print">Print</a>  

						<!-- delete btn -->
						<?php
						if($po_permissions['delete']==1)
						{
						?>

							<a class = "btn btn-sm delete-btn btn-danger" href = "#delete-po-form" title = "Delete Purchase Order" id = "delete_po">Delete</a> 

							<!-- delete modal -->
							<div id = "delete-po-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "">
								<h1>Delete Purchase Order</h1>
								<strong>Are you sure you want to delete this purchase order?</strong>
								<br>
								<a href = "project_purchase_orders.php?action=1&id=<?php print $id; ?>&poid=<?php print $poid; ?>" id = "" class = "btn btn-danger btn-lg">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>
							</div>

						<?php } ?>

					</div>
				</div>

				<!-- selected po content -->
				<div class="panel-body">
					<table width = "100%">
					<tr>
						<td width = "50%" valign = "top">
							<!-- vendor info -->
							<?php
							if($setup['is_qb']==1)
							{
								$vendor = $vujade->get_vendor($firstpo['vendor_id'],'ListID');
							}
							else
							{
								$vendor = $vujade->get_vendor($firstpo['vendor_id']);
							}
							if($vendor['error']=="0")
							{
								print '<strong>';
								print $vendor['name'];
								print '</strong>';
								print '<br>';
								print $vendor['address_1'];
								if(!empty($vendor['address_2']))
								{
									print ', ' . $vendor['address_1'];
								}
								print '<br>';
								print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];
								?>
								<br>
								<br>
								<strong>Vendor Contact: </strong><br>
								<?php
								$vendor_contact = $vujade->get_vendor_contact($firstpo['vendor_contact_id']);
								if($vendor_contact['error']=="0")
								{
									print $vendor_contact['fullname'];
								}
							}
							else
							{
								//print $vendor['error'];
							}
							?>
						</td>

						<td width = "50%" valign = "top">
							<!-- ship to info -->
							<strong>Ship To: </strong><br>
							<?php
							print $firstpo['company'].'<br>';
							print $firstpo['address_1'];
							if(!empty($firstpo['address_2']))
							{
								print ', '.$firstpo['address_2'];
							}
							print '<br>';
							print $firstpo['city'].', '.$firstpo['state'].' '.$firstpo['zip'];
							?>
						</td>
					</tr>
				</table>

				<p>
					<div align = "right">
						Written By: <?php print $firstpo['ordered_by']; ?>
						<br>
						Revised: <?php print $firstpo['date_revised']; ?>
					</div>
				</p>

				<style>
				.bordered
				{
					padding:5px;
					border:1px solid #cecece;
				}

				.size50
				{
					width:50px;
				}

				.size100
				{
					width:100px;		
				}

				.size200
				{
					width:200px;
				}
				</style>

				<?php
				if($firstpo['type']=="Materials")
				{
					?>
					<div style = "width:100%;height:515px;overflow:auto;">
					<table width = "100%">
						<tr>
							<td class = "bordered size100">Item #</td>
							<td class = "bordered size200" style = "width:400px;">Description</td>
							<td class = "bordered size100">Qty</td>
							<td class = "bordered size100">Total</td>
							<td class = "bordered size100">Qty Recvd</td>
						</tr>

						<?php
						$materials_cost = 0;
						$items = $vujade->get_materials_for_purchase_order($firstpo['database_id']);
						if($items['error']=="0")
						{
							unset($items['error']);
							foreach($items as $i)
							{
								// only show items that were assigned a project id that is equal to the project id of this project
								if($i['project_id']==$id)
								{
									$line = $i['qty'] * $i['unit_price'];
									if($setup['is_qb']==1)
									{
										$fitem = $vujade->get_item($i['inventory_id'],'ListID');
										if($fitem['error']=="0")
										{
											$i['inventory_id']=$fitem['inventory_id'];
										}
									}
									?>
									<tr>
										<td class = "bordered size100"><?php print $i['inventory_id']; ?></td>
										<td class = "bordered size200" style = "width:400px;"><?php print nl2br($i['description']); ?></td>
										<td class = "bordered size100"><?php print $i['qty']; ?></td>
										<td class = "bordered size100"><?php print number_format($line,2); ?></td>
										<td class = "bordered size100"><?php print $i['qty_recvd']; ?></td>
									</tr>
									<?php
									$materials_cost+=$line;
								}
							}
						}
						?>
					</table>
					<div style = "border:1px solid #cecece;padding:5px;width:200px;float:right;margin-top:15px;">
					<table width = "100%">
						<tr>
							<td>Subtotal:</td>
							<td>$<?php print @number_format($materials_cost,2); ?>
							</td>
						</tr>
						<tr>
							<td>Tax:</td>
							<td><?php print $firstpo['tax']; ?></td>
						</tr>
						<tr>
							<td>Tax Amount:</td>
							<td>$<?php print @number_format($firstpo['tax_amount'],2); ?></td>
						</tr>
						<tr>
							<td>Total:</td>
							<?php $t = $materials_cost+$firstpo['tax_amount']; ?>
							<td>$<?php print @number_format($t,2,'.',','); ?></td>
						</tr>
					</table>
					</div>
					<?php
				}
				if( ($firstpo['type']=="Outsource") || ($firstpo['type']=="Subcontract") )
				{
					?>
					<div style = "width:100%;height:515px;overflow:auto;">
					<table width = "100%">
						<tr>
							<td class = "bordered size200" style = "width:355px;">Description</td>
							<td class = "bordered size100" style = "width:150px;">Date Required</td>
						</tr>
						<?php
						if($setup['is_qb']==1)
						{
							$outsourced = $vujade->get_materials_for_purchase_order($poid);
						}
						else
						{
							$outsourced = $vujade->get_outsourced_items($poid);
						}
						if($outsourced['error']=="0")
						{
							unset($outsourced['error']);
							foreach($outsourced as $outsource)
							{
							?>
								<tr>
									<td class = "bordered size200" style = "width:355px;"><?php print $outsource['description']; ?></td>
									<td class = "bordered size100" style = "width:150px;"><?php print $outsource['date_required']; ?></td>
								</tr>
							<?php
							}
						}	
						?>

						<tr>
							<td colspan = "2">
							<table style = "margin-top:15px;">
								<tr>
									<td width = "75%">
										<strong>Notes</strong><br>
										<div name = "notes" id = "notes" style = "width:370px;height:100px;padding:5px;overflow:auto;border:1px solid #cecece;">
											<?php print $firstpo['notes']; ?>
										</div>
									</td>

									<td valign = "top">
										<div style = "margin-left:10px;border:1px solid #cecece;padding:5px;margin-top:15px;">
											Quoted Price:<br>
											$<?php print @number_format($firstpo['price_quote'],2,'.',','); ?>
										</div>
									</td>	
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</div>
				<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>

</div>
</section>
<!-- End: Content -->
</section>
</div>
<!-- End: Main -->
<!-- BEGIN: PAGE SCRIPTS -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/moment/moment.min.js"></script>
<script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{
	"use strict";

	// Init Theme Core    
	Core.init();

	// modal: email
	$('#email-po').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#email-po-form',
		modal: true
	});

	var msg = $('#msg').html();
    $("#msg").html($.trim(msg));

    $('#send').click(function(e)
    {
    	e.preventDefault();
	
		// send to, subject and message must not be blank
		var send_to = $('#send_to').val();
		var subject = $('#subject').val();
		var message = $('#msg').val();
		var error = 0;
		if(send_to=="")
		{	
			$('#error_1').show();
			error++;
		}
		else
		{
			$('#error_1').hide();
		}
		if(subject=="")
		{
			$('#error_2').show();
			error++;
		}
		else
		{
			$('#error_2').hide();
		}
		if(message=="")
		{
			$('#error_3').show();
			error++;
		}
		else
		{
			$('#error_3').hide();
		}

		if(error==0)
		{
			//$('#send').hide();
			$('#working').css('padding','3px');
			$('#working').css('background-color','#E6A728');
			//$('#working').css('background-color','#36D86C');
			$('#working').css('color','white');
			$('#working').css('font-weight','bold');
			$('#working').css('font-size','16px');
			$('#working').html('Working...');
			$.post( "jq.send.php", { id: <?php print $poid; ?>, type: "purchase_order", send_to: send_to, subject: subject, message: message })
			.done(function( response ) 
			{
			    $('#working').css('background-color','#36D86C');
			    $('#working').html('');
			    $('#working').html(response);

			    if(response=="Message sent!")
			    {
			    	setTimeout(function()
			    	{
					    $.magnificPopup.close();
					}, 2000);
			    }

			});
		}
	});

	// modal: delete
	$('#delete_po').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-po-form',
		modal: true
	});

	// dismiss modals
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});
</script>
<!-- END: PAGE SCRIPTS -->
</body>
</html>