<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($projects_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

// admin permission (count of accounting permissions)
$count_permissions = $projects_permissions['read']+$projects_permissions['edit']+$projects_permissions['delete'];

$admin = $_REQUEST['admin'];

$project_id = $_REQUEST['id'];
$id=$project_id;
$project = $vujade->get_project($project_id,2);
if($project['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);
$shop_order = $vujade->get_shop_order($id, 'project_id');

$charset="ISO-8859-1";
$html = '<!DOCTYPE html>
    <head>
    <meta charset="'.$charset.'">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Job Costs</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/print_costing_style.css">
    </head>
    <body>

    <div class="container_1">';
    $header_1 = '
    <div class = "header"><h1>Job Costs: Labor for Project # '.$project_id.'</h1></div>';

        $header_1.='<table width = "100%" style = "border-bottom:0px;">
            <tr style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">
                <td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">Date</td>
                <td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">Hours</td>
                <td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">Department</td>
                <td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">Employee</td>';
        if( ($admin==1) && ($count_permissions==3) )
        {
            $header_1.='<td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">Cost</td></tr>';
        }
        else
        {
            $header_1.='<td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">&nbsp;</td></tr>';
        }
        
            # labor
            $raw_labor = 0;
            $labor_rider = 0;
            $total_labor = 0;
            $tcdata = $vujade->get_timecards_for_project($id,"","","",1);
            if($tcdata['error']=="0")
            {
                $html .='';
                unset($tcdata['error']);
                $last = end($tcdata);
                $current_labor_type = $tcdata[0]['type'];
                $labor_section_total = 0;
                $labor_section_hours_total = 0;
                foreach($tcdata as $tc)
                {
                    # employee 
                    $employee_id = $tc['employee_id'];
                    $empl = $vujade->get_employee($employee_id);

                    # employee's rate
                    $current_rate = $vujade->get_employee_current_rate($employee_id);

                    # type of work
                    $work_type = $tc['type'];

                    # rate for type of work
                    $rate_data = $vujade->get_labor_rate($work_type,'id');

                    # hours worked
                    $total_time = 0;
                    $st_total = 0;
                    $ot_total = 0;
                    $dt_total = 0;
                    
                    $line_total = $tc['standard_time'] + $tc['over_time'] + $tc['double_time'];
                    $st_total+=$tc['standard_time'];
                    $ot_total+=$tc['over_time'];
                    $dt_total+=$tc['double_time'];

                    $total_time+=$line_total;

                    # raw labor 
                    $st_rate_total = $st_total * $current_rate['rate'];
                    $ot_rate_total = $ot_total * ($current_rate['rate'] * 1.5);
                    $dt_rate_total = $dt_total * ($current_rate['rate'] * 2);
                    $raw_labor = $st_rate_total + $ot_rate_total + $dt_rate_total;

                    $total_labor += $raw_labor;

                    if($tc['type']==$current_labor_type)
                    {
                        $current_labor_type=$tc['type'];
                        $labor_section_total+=$raw_labor;
                        $labor_section_hours_total+=$line_total;
                    }
                    else
                    {
                        $html.='<tr><td style = "padding-bottom:5px;font-size:18px;"><strong>Dept. Hours</strong></td><td style = "padding-bottom:5px;font-size:18px;">'.$labor_section_hours_total.'</td><td>&nbsp;</td>';
                        if( ($admin==1) && ($count_permissions==3) )
                        {
                            $html.='<td style = "padding-bottom:5px;font-size:18px;"><strong>Dept. Subtotal</strong></td><td style = "padding-bottom:5px;font-size:18px;"><strong>'.@number_format($labor_section_total,2,'.',',').'</strong></td>';
                        }
                        else
                        {
                            $html.='<td>&nbsp;</td><td>&nbsp;</td>';
                        }
                        $html.='</tr>';
                        $html.='<tr><td colspan = "5">&nbsp;</td></td>';
                        $html.='</tr>';
                        unset($current_labor_type);
                        $current_labor_type=$tc['type'];
                        $labor_section_total=0;
                        $labor_section_total=$raw_labor;
                        $labor_section_hours_total=$line_total;
                    }

                    $html.='<tr>';
                    
                    $html.='<td style = "padding-bottom:5px;border-bottom:1px solid #cecece;">';
                    $html.=$tc['date'];
                    $html.='</td>';

                    $html.='<td style = "padding-bottom:5px;border-bottom:1px solid #cecece;">';
                    $html.=$line_total;
                    $html.='</td>';

                    $html.='<td style = "padding-bottom:5px;border-bottom:1px solid #cecece;">';
                    $html.=$tc['type_name'];
                    $html.='</td>';

                    $html.='<td style = "padding-bottom:5px;border-bottom:1px solid #cecece;">';
                    $html.=$empl['fullname'];
                    $html.='</td><td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;">';

                    if( ($admin==1) && ($count_permissions==3) )
                    {
                        $html.=@number_format($raw_labor,2,'.',',');
                    }
                    else
                    {
                        $html.='&nbsp;';
                    }
                    $html.='</td>';
                    $html.='</tr>';

                    if($tc==$last)
                    {
                        /*
                        $html.='<tr><td colspan = "3" style = "padding-bottom:5px;"></td><td><strong>Dept. Subtotal</strong></td>';
                        $html.='<td style = "padding-bottom:5px;font-weight:bold;"><strong>'.@number_format($labor_section_total,2,'.',',').'</strong></td>';
                        $html.='</tr>';
                        */

                        $html.='<tr><td style = "padding-bottom:5px;" style = "padding-bottom:5px;font-size:18px;"><strong>Dept. Hours</strong></td><td style = "padding-bottom:5px;" style = "padding-bottom:5px;font-size:18px;">'.$labor_section_hours_total.'</td><td>&nbsp;</td>';
                        if( ($admin==1) && ($count_permissions==3) )
                        {
                            $html.='<td style = "padding-bottom:5px;font-size:18px;"><strong>Dept. Subtotal</strong></td><td style = "padding-bottom:5px;font-size:18px;"><strong>'.@number_format($labor_section_total,2,'.',',').'</strong></td>';
                        }
                        else
                        {
                            $html.='<td>&nbsp;</td><td>&nbsp;</td>';
                        }

                        $html.='</tr>';
                        $html.='<tr><td colspan = "5" style = "padding-bottom:5px;">&nbsp;</td></td>';
                        $html.='</tr>';
                    }
                }
            }

            $html.='<tr>
                <td colspan = "5">&nbsp;</td>
            </tr>
            <tr style = "padding-top:5px;border-top:1px solid #cecece;">
                <td colspan = "3" style = ""></td>';
            if( ($admin==1) && ($count_permissions==3) )
            {
                $html.='<td style = "padding-bottom:5px;font-size:18px;border-bottom:1px solid #cecece;"><strong>Sum of Labor Cost</strong></td><td style = "padding-bottom:5px;border-bottom:1px solid #cecece;font-weight:bold;font-size:18px;">
                    '.@number_format($total_labor,2,'.',',').'</td>';
            }
            else
            {
                $html.='<td>&nbsp;</td><td>&nbsp;</td>';
            }
            $html.='</tr></table>';

$html.='</div></body></html>';

$footer_1='<div style = "text-align:center;width:100%">Page {PAGENO}</div>';

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 40, 40, 10);
//$mpdf->setAutoTopMargin = 'stretch';
//$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($html);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-costing-labor.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output('Costing Labor.pdf','I'); 
}
?>