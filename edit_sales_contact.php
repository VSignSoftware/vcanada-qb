<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id=$_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$contact_id = $_REQUEST['contact_id'];

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# edit a contact
if($action==1)
{
	$name = $_REQUEST['name'];
	$title = $_REQUEST['title'];
	$phone = $_REQUEST['phone'];
	$cell = $_REQUEST['cell'];
	$fax = $_REQUEST['fax'];
	$other = $_REQUEST['other'];
	$label_1 = $_REQUEST['label_1'];
	$label_2 = $_REQUEST['label_2'];
	$label_3 = $_REQUEST['label_3'];
	$label_4 = $_REQUEST['label_4'];
	$address = $_REQUEST['address'];
	$city = $_REQUEST['city'];
	$state = $_REQUEST['state'];
	$zip = $_REQUEST['zip'];

	$s = array();
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'project_id',$id);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'name',$name);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'phone',$phone);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'cell',$cell);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'fax',$fax);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'other',$other);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'title',$title);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'label_1',$label_1);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'label_2',$label_2);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'label_3',$label_3);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'label_4',$label_4);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'address',$address);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'city',$city);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'state',$state);
	$s[]=$vujade->update_row('sales_contacts',$contact_id,'zip',$zip);
	$vujade->page_redirect('contacts.php?id='.$id.'&contact_id='.$contact_id);
}
$contact = $vujade->get_sales_contact($contact_id);
$setup=$vujade->get_setup();
$shop_order = $vujade->get_shop_order($id, 'project_id');
$section=3;
$menu = 16;
$title = "Edit Contact - ";
require_once('h.php');
//require_once('project_toolbar.php');
?>

<!-- Start: Content-Wrapper -->

<section id="content_wrapper">

	<!-- Start: Topbar -->

	<header id="topbar">

		<div class="topbar-left">

			<ol class="breadcrumb">

				<li class="crumb-active">

					<a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>

				</li>

			</ol>

		</div>

	</header>

	<!-- Begin: Content -->
	<section id="content" class="table-layout animated fadeIn">

		<div class="admin-form theme-primary">

			<div class="panel heading-border panel-primary">

				<div class="panel-body bg-light">

					<div id="" style = "margin-top:15px;">
						<style>
							#form label{
								margin: 0 0 10px;
							}
						</style>

						<!-- form -->
						<form id = "form" class = "" method = "post" action = "edit_sales_contact.php">

							<div class = "row">
								<div class = "col-md-4">
									<input type = "text" name = "name" id = "name" class="form-control" style = "width:300px;margin-bottom:15px;" placeholder = "Name" value = "<?php print $contact['name']; ?>">

									<label class="field select" style = "width:300px;margin-bottom:15px;">
		                            <select id="title" name="title">
		                            	<?php
		                            	if(!empty($contact['title']))
		                            	{
		                            		print '<option value = "'.$contact['title'].'">'.$contact['title'].'</option>';
		                            	}
		                            	?>
		                              <option value = "">-Title-</option>
		                              <option value="Architect">Architect</option>
		                              <option value="Client">Client</option>
		                              <option value="Contractor">Contractor</option>
		                              <option value="Landlord">Landlord</option>
		                              <option value="Designer">Designer</option>
		                              <option value="Other">Other</option>
		                            </select>
		                            <i class="arrow double"></i>
		                            </label>

		                            <div class="form-group">
		                            	<div class="col-sm-12" style = "padding:0px;margin:0px;margin-bottom:15px;">
										    <div class="input-group">
									      		<select id="label_1" name="label_1" style="float: left;width: initial;" class = "form-control">
									      			<?php
					                            	if(!empty($contact['label_1']))
					                            	{
					                            		print '<option value = "'.$contact['label_1'].'">'.$contact['label_1'].'</option>';
					                            	}
					                            	?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            	</select>
	
										      	<input type = "text" name = "phone" id = "phone" class="form-control" style="float: left;width: 150px;" placeholder="Number" value = "<?php print $contact['phone']; ?>">
										    </div>
										</div>
									</div>

									<div class="form-group">
		                            	<div class="col-sm-12" style = "padding:0px;margin:0px;margin-bottom:15px;">
										    <div class="input-group">
									      		<select id="label_2" name="label_2" style="float: left;width: initial;" class = "form-control">
									      			<?php
					                            	if(!empty($contact['label_2']))
					                            	{
					                            		print '<option value = "'.$contact['label_2'].'">'.$contact['label_2'].'</option>';
					                            	}
					                            	?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            	</select>
	
										      	<input type = "text" name = "cell" id = "cell" class="form-control" style="float: left;width: 150px;" placeholder="Number" value = "<?php print $contact['cell']; ?>">
										    </div>
										</div>
									</div>

									<div class="form-group">
		                            	<div class="col-sm-12" style = "padding:0px;margin:0px;margin-bottom:15px;">
										    <div class="input-group">
									      		<select id="label_3" name="label_3" style="float: left;width: initial;" class = "form-control">
									      			<?php
					                            	if(!empty($contact['label_3']))
					                            	{
					                            		print '<option value = "'.$contact['label_3'].'">'.$contact['label_3'].'</option>';
					                            	}
					                            	?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            	</select>
	
										      	<input type = "text" name = "fax" id = "fax" class="form-control" style="float: left;width: 150px;" placeholder="Number" value = "<?php print $contact['fax']; ?>">
										    </div>
										</div>
									</div>

									<div class="form-group">
		                            	<div class="col-sm-12" style = "padding:0px;margin:0px;">
										    <div class="input-group">
									      		<select id="label_4" name="label_4" style="float: left;width: initial;" class = "form-control">
									      			<?php
					                            	if(!empty($contact['label_4']))
					                            	{
					                            		print '<option value = "'.$contact['label_4'].'">'.$contact['label_4'].'</option>';
					                            	}
					                            	?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            	</select>
	
										      	<input type = "text" name = "other" id = "other" class="form-control" style="float: left;width: 150px;" placeholder="Number" value = "<?php print $contact['other']; ?>">
										    </div>
										</div>
									</div>

									<input type = "hidden" name = "action" value = "1">
									<input type = "hidden" name = "contact_id" value = "<?php print $contact_id; ?>">
									<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
								</div>

								<div class = "col-md-8">
									<input type = "text" name = "address" id = "address" class="form-control" style = "margin-bottom:15px;" placeholder = "Address" value = "<?php print $contact['address']; ?>">

									<input type = "text" name = "city" id = "city" class="form-control" style = "margin-bottom:15px;" placeholder = "City" value = "<?php print $contact['city']; ?>">

									<label class="field select" style = "width:300px;margin-bottom:15px;">
		                            <select id="state" name="state">
		                              <?php
		                              if(isset($contact['state']))
		                              {
		                                  print '<option value = "'.$contact['state'].'" selected = "selected">'.$contact['state'].'</option>';
		                              }
		                              if($setup['country']=="USA")
			              	  		  {
		                              ?>
		                              <option value = "">-State-</option>
		                              <option value="AL">Alabama</option>
		                              <option value="AK">Alaska</option>
		                              <option value="AZ">Arizona</option>
		                              <option value="AR">Arkansas</option>
		                              <option value="CA">California</option>
		                              <option value="CO">Colorado</option>
		                              <option value="CT">Connecticut</option>
		                              <option value="DE">Delaware</option>
		                              <option value="DC">District Of Columbia</option>
		                              <option value="FL">Florida</option>
		                              <option value="GA">Georgia</option>
		                              <option value="HI">Hawaii</option>
		                              <option value="ID">Idaho</option>
		                              <option value="IL">Illinois</option>
		                              <option value="IN">Indiana</option>
		                              <option value="IA">Iowa</option>
		                              <option value="KS">Kansas</option>
		                              <option value="KY">Kentucky</option>
		                              <option value="LA">Louisiana</option>
		                              <option value="ME">Maine</option>
		                              <option value="MD">Maryland</option>
		                              <option value="MA">Massachusetts</option>
		                              <option value="MI">Michigan</option>
		                              <option value="MN">Minnesota</option>
		                              <option value="MS">Mississippi</option>
		                              <option value="MO">Missouri</option>
		                              <option value="MT">Montana</option>
		                              <option value="NE">Nebraska</option>
		                              <option value="NV">Nevada</option>
		                              <option value="NH">New Hampshire</option>
		                              <option value="NJ">New Jersey</option>
		                              <option value="NM">New Mexico</option>
		                              <option value="NY">New York</option>
		                              <option value="NC">North Carolina</option>
		                              <option value="ND">North Dakota</option>
		                              <option value="OH">Ohio</option>
		                              <option value="OK">Oklahoma</option>
		                              <option value="OR">Oregon</option>
		                              <option value="PA">Pennsylvania</option>
		                              <option value="RI">Rhode Island</option>
		                              <option value="SC">South Carolina</option>
		                              <option value="SD">South Dakota</option>
		                              <option value="TN">Tennessee</option>
		                              <option value="TX">Texas</option>
		                              <option value="UT">Utah</option>
		                              <option value="VT">Vermont</option>
		                              <option value="VA">Virginia</option>
		                              <option value="WA">Washington</option>
		                              <option value="WV">West Virginia</option>
		                              <option value="WI">Wisconsin</option>
		                              <option value="WY">Wyoming</option>
		                              <option value="">---------------</option>
							              <option value="">-Canadian Provinces-</option>
							              <option value="Alberta">Alberta</option>
							              <option value="British Columbia">British Columbia</option>
							              <option value="Manitoba">Manitoba</option>
							              <option value="New Brunswick">New Brunswick</option>
							              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
							              <option value="Northwest Territories">Northwest Territories</option>
							              <option value="Nova Scotia">Nova Scotia</option>
							              <option value="Nunavut">Nunavut</option>
							              <option value="Ontario">Ontario</option>
							              <option value="Prince Edward Island">Prince Edward Island</option>
							              <option value="Quebec">Quebec</option>
							              <option value="Saskatchewan">Saskatchewan</option>
							              <option value="Yukon">Yukon</option>
							        <?php 
							    	} 
							    	// canadian servers
							    	if($setup['country']=='Canada')
							    	{
							    		?>
							    		  <option value="">-Canadian Provinces-</option>
							              <option value="Alberta">Alberta</option>
							              <option value="British Columbia">British Columbia</option>
							              <option value="Manitoba">Manitoba</option>
							              <option value="New Brunswick">New Brunswick</option>
							              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
							              <option value="Northwest Territories">Northwest Territories</option>
							              <option value="Nova Scotia">Nova Scotia</option>
							              <option value="Nunavut">Nunavut</option>
							              <option value="Ontario">Ontario</option>
							              <option value="Prince Edward Island">Prince Edward Island</option>
							              <option value="Quebec">Quebec</option>
							              <option value="Saskatchewan">Saskatchewan</option>
							              <option value="Yukon">Yukon</option>
							              <option value="">---------------</option>
							              <option value="">-US States-</option>
							              <option value="AL">Alabama</option>
							              <option value="AK">Alaska</option>
							              <option value="AZ">Arizona</option>
							              <option value="AR">Arkansas</option>
							              <option value="CA">California</option>
							              <option value="CO">Colorado</option>
							              <option value="CT">Connecticut</option>
							              <option value="DE">Delaware</option>
							              <option value="DC">District Of Columbia</option>
							              <option value="FL">Florida</option>
							              <option value="GA">Georgia</option>
							              <option value="HI">Hawaii</option>
							              <option value="ID">Idaho</option>
							              <option value="IL">Illinois</option>
							              <option value="IN">Indiana</option>
							              <option value="IA">Iowa</option>
							              <option value="KS">Kansas</option>
							              <option value="KY">Kentucky</option>
							              <option value="LA">Louisiana</option>
							              <option value="ME">Maine</option>
							              <option value="MD">Maryland</option>
							              <option value="MA">Massachusetts</option>
							              <option value="MI">Michigan</option>
							              <option value="MN">Minnesota</option>
							              <option value="MS">Mississippi</option>
							              <option value="MO">Missouri</option>
							              <option value="MT">Montana</option>
							              <option value="NE">Nebraska</option>
							              <option value="NV">Nevada</option>
							              <option value="NH">New Hampshire</option>
							              <option value="NJ">New Jersey</option>
							              <option value="NM">New Mexico</option>
							              <option value="NY">New York</option>
							              <option value="NC">North Carolina</option>
							              <option value="ND">North Dakota</option>
							              <option value="OH">Ohio</option>
							              <option value="OK">Oklahoma</option>
							              <option value="OR">Oregon</option>
							              <option value="PA">Pennsylvania</option>
							              <option value="RI">Rhode Island</option>
							              <option value="SC">South Carolina</option>
							              <option value="SD">South Dakota</option>
							              <option value="TN">Tennessee</option>
							              <option value="TX">Texas</option>
							              <option value="UT">Utah</option>
							              <option value="VT">Vermont</option>
							              <option value="VA">Virginia</option>
							              <option value="WA">Washington</option>
							              <option value="WV">West Virginia</option>
							              <option value="WI">Wisconsin</option>
							              <option value="WY">Wyoming</option>
							    		<?php
							    	}
							    	?>
		                            </select>
		                            <i class="arrow double"></i>
		                            </label> 

									<input type = "text" name = "zip" id = "zip" class="form-control" style = "width:100px;" placeholder = "Zip / Postal Code" value = "<?php print $contact['zip']; ?>"> 
								</div>
							</div>

							<div style = "margin-top:15px; overflow: hidden;">
								<div style = "float:left">
									<a id = "cancel" href = "#" class = "btn btn-lg btn-danger" style = "width:100px;">CANCEL</a> <a id = "save" class = "btn btn-lg btn-success" style = "width:100px; border-radius: 3px;">SAVE</a>
								</div>
								<div style = "float:right;">
									
								</div>
							</div>

						</form>

					</div>

				</div>
			</div>
		</div>
	</section>
	<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function()
	{
		"use strict";

		// Init Theme Core
		Core.init();

		$('#cancel').click(function(e)
		{
			e.preventDefault();
			window.location="contacts.php?id=<?php print $id; ?>";
		});

		$('#save').click(function(e)
		{
			e.preventDefault();
			$('#form').submit();
		});
	});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>