<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

if(isset($_REQUEST['tier_id']))
{
	$tier_id = $_REQUEST['tier_id'];
	$valid = array(1,2,3,4);
	if(!in_array($tier_id, $valid))
	{
		$tier_id=1;
	}
}
else
{
	$tier_id = 1;
}

$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
# update the tier
if($action==1)
{
	$s = array(); // success array

	$tier_label=$_POST['tier_label'];

	# update the tier label
	$s[]=$vujade->update_row('commission_setup',$tier_id,'tier_label',$tier_label);

	if($tier_id==1)
	{
		$db_ids = $_POST['db_ids'];
		$markup_number = $_POST['markup_number'];
		$commission=$_POST['commission'];
		
		$commission_data = array_map(null, $db_ids, $markup_number, $commission);

		foreach($commission_data as $update)
		{
			$id = $update[0];
			$mn = $update[1];
			$c = $update[2];
			$s[]=$vujade->update_row('commission',$id,'markup',$mn);
			$s[]=$vujade->update_row('commission',$id,'commission_percent',$c);
		}

		$c = count($s);
		$vujade->messages[]="Commission updated.";
	}

	if($tier_id==2)
	{
		$db_ids = $_POST['db_ids'];
		$markup_number = $_POST['markup_number'];
		$commission=$_POST['commission'];
		
		$commission_data = array_map(null, $db_ids, $markup_number, $commission);

		foreach($commission_data as $update)
		{
			$id = $update[0];
			$mn = $update[1];
			$c = $update[2];
			$s[]=$vujade->update_row('commission',$id,'markup',$mn);
			$s[]=$vujade->update_row('commission',$id,'commission_percent',$c);
		}

		$c = count($s);
		$vujade->messages[]="Commission updated.";
	}

	if($tier_id==3)
	{
		$db_ids = $_POST['db_ids'];
		$markup_number = $_POST['markup_number'];
		$commission=$_POST['commission'];
		
		$commission_data = array_map(null, $db_ids, $markup_number, $commission);

		foreach($commission_data as $update)
		{
			$id = $update[0];
			$mn = $update[1];
			$c = $update[2];
			$s[]=$vujade->update_row('commission',$id,'markup',$mn);
			$s[]=$vujade->update_row('commission',$id,'commission_percent',$c);
		}

		$c = count($s);
		$vujade->messages[]="Commission updated.";
	}

	if($tier_id==4)
	{
		$db_ids = $_POST['db_ids'];
		$markup_number = $_POST['markup_number'];
		$commission=$_POST['commission'];
		
		$commission_data = array_map(null, $db_ids, $markup_number, $commission);

		foreach($commission_data as $update)
		{
			$id = $update[0];
			$mn = $update[1];
			$c = $update[2];
			$s[]=$vujade->update_row('commission',$id,'markup',$mn);
			$s[]=$vujade->update_row('commission',$id,'commission_percent',$c);
		}

		$c = count($s);
		$vujade->messages[]="Commission updated.";
	}


	$tier_data = $vujade->get_tier_setup($tier_id);
}
else
{
	$tier_data = $vujade->get_tier_setup($tier_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Commission Setup - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=1;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

					<i>Fill in or update the form below or select a different tier:</i>
					<br>
					<?php
					$tiers = $vujade->get_commission_tiers();
					if($tiers['error']=='0')
					{
						print '<p style = "padding:10px;border:1px solid #cecece;">';
						unset($tiers['error']);
						foreach($tiers as $tier)
						{
							print '<a class = "" href = "commission_setup.php?tier_id='.$tier['tier_id'].'" style = "margin-right:50px;">';
							print $tier['tier_label'];
							print '</a> ';
							//print '<span style = "width:30px;height:30px;border:1px solid red;display:inline block;">&nbsp;</span>';
						}
						print '</p>';
					}
					else
					{
						$vujade->errors[]=$tiers['error'];
						$vujade->show_errors();
						die;
					}
					?>

					<form method = "post" action = "commission_setup.php">
					<input type = "hidden" name = "action" value = "1">
					<input type = "hidden" name = "tier_id" value = "<?php print $tier_id; ?>">
					<table class = "table">
						<tr>
							<td><strong>Commission Tier Label: </strong></td>
							<td><input type = "text" name = "tier_label" value = "<?php print $tier_data['tier_label']; ?>" style = "width:300px;"></td>
						</tr>

						<?php
						$markups = $vujade->get_markups($tier_id);
						if($markups['error']=='0')
						{
							unset($markups['error']);
							foreach($markups as $markup)
							{
								print '<tr>';

								print '<input type = "hidden" name = "db_ids[]" value = "'.$markup['database_id'].'">';

								print '<td>Markup ';

								print '<input type = "text" name = "markup_number[]" value = "'.$markup['markup'].'" style = "width:100px;">';
								print '</td>';

								print '<td>Percent: <input type = "text" name = "commission[]" value = "'.$markup['commission_percent'].'" style = "width:100px;"></td>';
								print '</tr>';
							}
						}
						?>
					</table>

					<input type = "submit" value = "UPDATE" class = "btn btn-primary">
					</form>

				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
