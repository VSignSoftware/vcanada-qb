<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$po_permissions = $vujade->get_permission($_SESSION['user_id'],'Purchase Orders');
if($po_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup=$vujade->get_setup(1);

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$show=0;
$pos = $vujade->get_purchase_orders($id);
if($pos['error']=='0')
{
	unset($pos['error']);
	$show=1;
	$posids = array();
	foreach($pos as $po)
	{
		$posids[]=$po['database_id'];
	}
	$posids=array_unique($posids);
	sort($posids);
	$firstpo = $vujade->get_purchase_order($posids[0]);
	$poid = $posids[0];
}
else
{
	$show=0;
	$firstpo['error']=1;
}

if(isset($_REQUEST['poid']))
{
	$firstpo = $vujade->get_purchase_order($_REQUEST['poid']);
	$poid = $_REQUEST['poid'];
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 17;
$cmenu = 5;
$section = 3;
$title = "Costing Purchase Orders - ".$project['project_id'].' - '.$project['site'].' - ';
//$charset = '<meta charset="ISO-8859-1">';
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">
<?php require_once('project_left_tray.php'); ?>
<div class="tray tray-center" style = "width:100%;border:0px solid red;">
<div class="pl15 pr15" style = "width:100%;">
<?php require_once('project_right_tray.php'); ?>

<div class="row">

	<!-- left hand side list of po's -->
	<div class="col-md-2" style="width:200px;">
		<div class="panel panel-primary panel-border top">
			<div class="panel-heading">
				<span class="panel-title">PO List</span>
			</div>
			<div class="panel-body">
				<?php
				if($show==1)
				{
					print '<table>';
					unset($pos['error']);
					foreach($posids as $pid)
					{
						$po = $vujade->get_purchase_order($pid);
						if($po['error']=="0")
						{
							unset($po['error']);
							if($poid==$pid)
							{
								$bgcolor = "cecece";
							}
							else
							{
								$bgcolor = "ffffff";
							}
							print '<tr bgcolor = "'.$bgcolor.'">';
							print '<td valign = "top">';
							$n=strtotime('now');
							$tdd = strtotime($po['date']);
							if($n>$tdd)
							{
								$color = "#f7584c";
							}
							else
							{
								$color="#288BC3";
							}
							$display_date = date('m/d/Y',$tdd);
							print '<a class = "linknostyle" href = "costing_purchase_orders.php?id='.$id.'&poid='.$po['database_id'].'">';
							print '<font color = "'.$color.'">'.$display_date.'</font>';
							print '</a>';

							print '</td>';
							print '<td valign = "top">&nbsp;</td>';
							print '<td valign = "top">';

							print '<a class = "linknostyle" href = "costing_purchase_orders.php?id='.$id.'&poid='.$po['database_id'].'">';
							print $po['purchase_order_id'];
							print '</a>';

							print '</td>';
							print '</tr>';
						}
					}
					print '</table>';
				}
				?>
			</div>
		</div>
	</div>

	<!-- right hand side detail of selected po -->
	<div id = "" class="col-md-9" style="margin-left:5px;border:0px solid red;padding:0px;">

		<?php
		if($show==1)
		{
			?>
			<div class="panel panel-primary panel-border top">
				<div class="panel-heading">
					<span class="panel-title">PO #<?php print $firstpo['purchase_order_id']; ?></span>
					<div class="widget-menu pull-right">

						<!-- edit btn -->
						<?php
						if($po_permissions['edit']==1)
						{
						?>
							<a class = "btn btn-sm btn-primary" href = "edit_purchase_order.php?project_id=<?php print $id; ?>&poid=<?php print $poid; ?>" title = "Edit Purchase Order">Edit</a>
						<?php } ?>
						
						<!-- print btn -->
						<a class = "btn btn-sm btn-primary" href = "print_purchase_order.php?id=<?php print $id; ?>&poid=<?php print $poid; ?>" target = "_blank" title = "Print">Print</a>  

					</div>
				</div>

				<!-- selected po content -->
				<div class="panel-body">
					<table width = "100%">
					<tr>
						<td width = "50%" valign = "top">
							<!-- vendor info -->
							<?php
							if($setup['is_qb']==1)
							{
								$vendor = $vujade->get_vendor($firstpo['vendor_id'],'ListID');
							}
							else
							{
								$vendor = $vujade->get_vendor($firstpo['vendor_id']);
							}
							if($vendor['error']=="0")
							{
								print '<strong>';
								print $vendor['name'];
								print '</strong>';
								print '<br>';
								print $vendor['address_1'];
								if(!empty($vendor['address_2']))
								{
									print ', ' . $vendor['address_1'];
								}
								print '<br>';
								print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];
								?>
								<br>
								<br>
								<strong>Vendor Contact: </strong><br>
								<?php
								$vendor_contact = $vujade->get_vendor_contact($firstpo['vendor_contact_id']);
								if($vendor_contact['error']=="0")
								{
									print $vendor_contact['fullname'];
								}
							}
							else
							{
								//print $vendor['error'];
							}
							?>
						</td>

						<td width = "50%" valign = "top">
							<!-- ship to info -->
							<strong>Ship To: </strong><br>
							<?php
							print $firstpo['company'].'<br>';
							print $firstpo['address_1'];
							if(!empty($firstpo['address_2']))
							{
								print ', '.$firstpo['address_2'];
							}
							print '<br>';
							print $firstpo['city'].', '.$firstpo['state'].' '.$firstpo['zip'];
							?>
						</td>
					</tr>
				</table>

				<p>
					<div align = "right">
						Written By: <?php print $firstpo['ordered_by']; ?>
						<br>
						Revised: <?php print $firstpo['date_revised']; ?>
					</div>
				</p>

				<style>
				.bordered
				{
					padding:5px;
					border:1px solid #cecece;
				}

				.size50
				{
					width:50px;
				}

				.size100
				{
					width:100px;		
				}

				.size200
				{
					width:200px;
				}
				</style>

				<?php
				if($firstpo['type']=="Materials")
				{
					?>
					<div style = "width:100%;height:515px;overflow:auto;">
					<table width = "100%">
						<tr>
							<td class = "bordered size100">Item #</td>
							<td class = "bordered size200" style = "width:400px;">Description</td>
							<td class = "bordered size100">Qty</td>
							<td class = "bordered size100">Total</td>
							<td class = "bordered size100">Qty Recvd</td>
						</tr>

						<?php
						$materials_cost = 0;
						$items = $vujade->get_materials_for_purchase_order($firstpo['database_id']);
						if($items['error']=="0")
						{
							//print '<tr><td>';
							//print_r($items);
							//print '</td></tr>';
							unset($items['error']);
							foreach($items as $i)
							{
								// only show items that were assigned a project id that is equal to the project id of this project
								if($i['project_id']==$id)
								{
									$line = $i['qty'] * $i['unit_price'];
									if($setup['is_qb']==1)
									{
										$fitem = $vujade->get_item($i['inventory_id'],'ListID');
										if($fitem['error']=="0")
										{
											$i['inventory_id']=$fitem['inventory_id'];
										}
									}
									?>
									<tr>
										<td class = "bordered size100"><?php print $i['inventory_id']; ?></td>
										<td class = "bordered size200" style = "width:400px;"><?php print nl2br($i['description']); ?></td>
										<td class = "bordered size100"><?php print $i['qty']; ?></td>
										<td class = "bordered size100"><?php print number_format($line,2); ?></td>
										<td class = "bordered size100"><?php print $i['qty_recvd']; ?></td>
									</tr>
									<?php
									$materials_cost+=$line;
								}
							}
						}
						?>
					</table>
					<div style = "border:1px solid #cecece;padding:5px;width:200px;float:right;margin-top:15px;">
					<table width = "100%">
						<tr>
							<td>Subtotal:</td>
							<td>$<?php print @number_format($materials_cost,2); ?>
							</td>
						</tr>
						<tr>
							<td>Tax:</td>
							<td><?php print $firstpo['tax']; ?></td>
						</tr>
						<tr>
							<td>Tax Amount:</td>
							<td>$<?php print @number_format($firstpo['tax_amount'],2); ?></td>
						</tr>
						<tr>
							<td>Total:</td>
							<?php $t = $materials_cost+$firstpo['tax_amount']; ?>
							<td>$<?php print @number_format($t,2,'.',','); ?></td>
						</tr>
					</table>
					</div>
					<?php
				}
				if( ($firstpo['type']=="Outsource") || ($firstpo['type']=="Subcontract") )
				{
					?>
					<div style = "width:100%;height:515px;overflow:auto;">
					<table width = "100%">
						<tr>
							<td class = "bordered size200" style = "width:355px;">Description</td>
							<td class = "bordered size100" style = "width:150px;">Date Required</td>
						</tr>
						<?php
						if($setup['is_qb']==1)
						{
							$outsourced = $vujade->get_materials_for_purchase_order($poid);
						}
						else
						{
							$outsourced = $vujade->get_outsourced_items($poid);
						}
						if($outsourced['error']=="0")
						{
							unset($outsourced['error']);
							foreach($outsourced as $outsource)
							{
							?>
								<tr>
									<td class = "bordered size200" style = "width:355px;"><?php print $outsource['description']; ?></td>
									<td class = "bordered size100" style = "width:150px;"><?php print $outsource['date_required']; ?></td>
								</tr>
							<?php
							}
						}	
						?>

						<tr>
							<td colspan = "2">
							<table style = "margin-top:15px;">
								<tr>
									<td width = "75%">
										<strong>Notes</strong><br>
										<div name = "notes" id = "notes" style = "width:370px;height:100px;padding:5px;overflow:auto;border:1px solid #cecece;">
											<?php print $firstpo['notes']; ?>
										</div>
									</td>

									<td valign = "top">
										<div style = "margin-left:10px;border:1px solid #cecece;padding:5px;margin-top:15px;">
											Quoted Price:<br>
											$<?php print @number_format($firstpo['price_quote'],2,'.',','); ?>
										</div>
									</td>	
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</div>
				<?php } ?>
				</div>
			</div>
		<?php } ?>
	</div>

</div>
</section>
<!-- End: Content -->
</section>
</div>
<!-- End: Main -->
<!-- BEGIN: PAGE SCRIPTS -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="vendor/plugins/moment/moment.min.js"></script>
<script src="vendor/plugins/datepicker/js/bootstrap-datetimepicker.js"></script>
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{
	"use strict";

	// Init Theme Core    
	Core.init();

});
</script>
<!-- END: PAGE SCRIPTS -->
</body>
</html>