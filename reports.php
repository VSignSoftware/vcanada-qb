<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$title = "Reports - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<b>Sales</b><br>

        	<a id = "closed_jobs-link" href = "report_monthly_sales.php" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Monthly Sales Report</a> 

        	<a id = "closed_jobs-link" href = "volume_report.php" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Volume Report</a> 

        	<hr>

        	<b>Production</b><br>
        	<a id = "closed_jobs-link" href = "report_department_hours.php" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Department Hours</a> 

        	<a id = "closed_jobs-link" href = "report_pos.php" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Purchase Orders</a> 
        	<hr>

        	<b>Accounting</b><br>	

        	<a id = "closed_jobs-link" href = "report_closed_jobs.php" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Closed Jobs</a> 
        	
        	<a id = "closed_jobs-link" href = "print_report_ar.php" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;" target = "_blank">A/R Report</a> 

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<!-- main content for this page 
				<div class = "well">
					
				</div>
				-->

            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
