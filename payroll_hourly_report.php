<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['create']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Payroll Hourly Report - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

  <!-- Begin: Content -->
  <section id="content" class="table-layout animated fadeIn">

    <!-- begin: .tray-left -->
    <aside class="tray /*tray-left*/ tray250 p30" id = "left_tray">

	    <div id = "menu_2" style = "">
	    	<a class = "glyphicons glyphicons-left_arrow" href = "accounting.php" id = "back" style = "margin-bottom:10px;"></a>
			<br>
			
			<a href = "print_time_cards.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Print Time Cards</a>
	      	<br>

	      	<a href = "enter_time.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Enter Time</a>
	      	<br>

	      	<a href = "payroll_summary.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Payroll Summary</a>
	      	<br>

	      	<a href = "payroll_hourly_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Hourly Report</a>
	      	<br>

	      	<a href = "payroll_labor_sort_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Labor Report</a>
	      	<br>
		</div>

    </aside>
    <!-- end: .tray-left -->

    <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="pl20 pr50">

        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				
	        	<div class="panel-body bg-light">

	        		<table>
	        			<tr>
	        				<td>
	        					<strong>Hourly Payroll Report for Dates: </strong>
	        				</td>
	        				<td>&nbsp;</td>
	        				<td>
	        					<input type = "text" class = "dp form-control" style = "width:200px;float:left;" name = "date1" id = "date1" value = "<?php print $date1; ?>"> 
	        				</td>

	        				<td>&nbsp;</td>

	        				<td>
	        					<input type = "text" class = "dp form-control" style = "width:200px;float:left;" name = "date2" id = "date2" value = "<?php print $date2; ?>"> 
	        				</td>
	        				
	        				<td>&nbsp;</td>
	        				<td>
	        					<input type = "submit" id = "go" value = "Run Report" class = "btn btn-sm btn-primary">
	        				</td>
	        			</tr>
	        		</table>

<hr>
<?php
# 
if($action==1)
{
	?>
	<table class = "table">
    <tr class = "">
        <td>Employee</td>
        <td>Job No</td>
        <td>S.T.</td>
        <td>Rate</td>
        <td>O.T.</td>
        <td>Rate</td>
        <td>D.T.</td>
        <td>Rate</td>
        <td>Vac</td>
        <td>Hol</td>
        <td>Total Paid</td>
    </tr>

    <?php

	# get report for the time range
    $date1 = $_REQUEST['date1'];
    $date1ts = strtotime($date1);
    $date2 = $_REQUEST['date2'];
    $date2ts = strtotime($date2);

    $ids = $vujade->get_employees(1);
    if($ids['error']=="0")
    {
        unset($ids['error']);
        foreach($ids as $id)
        {
            $eid = $id['database_id'];
            $fullname = $id['fullname'];
            $rate_data = $vujade->get_employee_current_rate($eid);
            $rate=$rate_data['rate'];
            $vujade->get_timecards($date1ts,$date2ts,$eid,$fullname,$rate);
        }
        # grand total line
        ?>
        <tr class = "">
            <td colspan = "2">&nbsp;</td>
            <td><?php print @number_format($vujade->grand_total_st,2,'.',','); ?></td>
            <td>&nbsp;</td>
            <td><?php print @number_format($vujade->grand_total_ot,2,'.',','); ?></td>
            <td>&nbsp;</td>
            <td><?php print @number_format($vujade->grand_total_dt,2,'.',','); ?></td>
            <td>&nbsp;</td>
            <td><?php print @number_format($vujade->grand_total_vacation,2,'.',','); ?></td>
            <td><?php print @number_format($vujade->grand_total_holiday,2,'.',','); ?></td>
            <td><?php print @number_format($vujade->grand_total_paid,2,'.',','); ?></td>
        </tr>
        </table>

<?php
	}
}
?>

<?php if($action==1){ ?>
<div class="">
	<a class="btn btn-primary" target="_blank" href="pdf.php?page=payroll-hourly&action=1&date1=<?php echo $date1; ?>&date2=<?php echo $date2; ?>">Print</a>
</div>
<?php } ?>

</div>
</div>
</div>
</div>
</section>
</section>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/jquery.timepicker.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // datepickers
    $(".dp").datepicker();

    $('#go').click(function()
    {
    	var date1 = $('#date1').val();
    	var date2 = $('#date2').val();
    	if(date1=="")
    	{
    		alert('Date 1 must be selected.');
    		return false;
    	}
    	if(date2=="")
    	{
    		alert('Date 2 must be selected.');
    		return false;
    	}
    	// refresh the page
    	var href = "payroll_hourly_report.php?action=1&date1="+date1+"&date2="+date2;
    	window.location.href = href;
    });
	
});
</script>

</body>
</html>