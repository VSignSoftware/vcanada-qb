<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$vendors_permissions = $vujade->get_permission($_SESSION['user_id'],'Vendors');
if($vendors_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}

$vendor_id=$_REQUEST['vendor_id'];

if(isset($_REQUEST['contact_id']))
{
	$contact_id=$_REQUEST['contact_id'];
	$c = $vujade->get_vendor_contact($contact_id);
	if($c['error']!=0)
	{
		$vujade->page_redirect('error.php?m=3');
	}
	$is_new=0;
	$title = $c['fullname'].' - ';
}
else
{
	$is_new=1;
	$title = 'New Vendor Contact - ';
}

# new / edit contact 
if($action==1)
{
	$is_new=$_REQUEST['is_new'];
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$title=$_POST['title'];
	$phone1=$_POST['phone1'];
	$phone2=$_POST['phone2'];
	$phone3=$_POST['phone3'];
	$fax1=$_POST['fax1'];
	$email1=$_POST['email1'];
	$email2=$_POST['email2'];
	$notes=$_POST['notes'];

	$label_1 = $_REQUEST['label_1'];
	$label_2 = $_REQUEST['label_2'];
	$label_3 = $_REQUEST['label_3'];
	$label_4 = $_REQUEST['label_4'];

	if($is_new==1)
	{
		$vujade->create_row('vendor_contact');
		$row_id = $vujade->row_id;
		$s[]=$vujade->update_row('vendor_contact',$row_id,'vendor_id',$vendor_id);
	}	
	else
	{
		$row_id = $_POST['contact_id'];
	}
	
	$s[]=$vujade->update_row('vendor_contact',$row_id,'first_name',$first_name);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'last_name',$last_name);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'title',$title);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'notes',$notes);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'phone1',$phone1);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'phone2',$phone2);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'phone3',$phone3);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'fax1',$fax1);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'email1',$email1);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'email2',$email2);

	$s[]=$vujade->update_row('vendor_contact',$row_id,'label_1',$label_1);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'label_2',$label_2);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'label_3',$label_3);
	$s[]=$vujade->update_row('vendor_contact',$row_id,'label_4',$label_4);

	$vujade->page_redirect('vendor.php?id='.$vendor_id.'&tab=2&m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $title; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "vendor.php?id=<?php print $vendor_id; ?>" class = "btn btn-primary btn-sm">&laquo; Back</a>
					</div>
				</div>

	        	<div class="panel-body bg-light">
					<div class = "row">
						<div class = "col-md-12">

							<table class = "table">
								<form method = "post" action = "<?php print $_SERVER['PHP_SELF']; ?>">
								<tr class = "project_search_results">
								<td class = "project_search_results">
									First Name
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "first_name" value = "<?php print $c['first_name']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Last Name
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "last_name"  value = "<?php print $c['last_name']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									Title
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "title"  value = "<?php print $c['title']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									<select id="label_1" name="label_1" style="float: left;width: initial;" class = "form-control">
										<?php
										if(!empty($c['label_1']))
										{
											print '<option selected value = "'.$c['label_1'].'">'.$c['label_1'].'</option>';
										}
										?>

					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            </select>
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "phone1"  value = "<?php print $c['phone1']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									<select id="label_2" name="label_2" style="float: left;width: initial;" class = "form-control">
										<?php
										if(!empty($c['label_2']))
										{
											print '<option selected value = "'.$c['label_2'].'">'.$c['label_2'].'</option>';
										}
										?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            </select>
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "phone2"  value = "<?php print $c['phone2']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									<select id="label_3" name="label_3" style="float: left;width: initial;" class = "form-control">
										<?php
										if(!empty($c['label_3']))
										{
											print '<option selected value = "'.$c['label_3'].'">'.$c['label_3'].'</option>';
										}
										?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            </select>
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "phone3"  value = "<?php print $c['phone3']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									<select id="label_4" name="label_4" style="float: left;width: initial;" class = "form-control">
										<?php
										if(!empty($c['label_4']))
										{
											print '<option selected value = "'.$c['label_4'].'">'.$c['label_4'].'</option>';
										}
										?>
					                              <option value = "">-Select-</option>
					                              <option value="Mobile">Mobile</option>
					                              <option value="Work">Work</option>
					                              <option value="Home">Home</option>
					                              <option value="Car">Car</option>
					                              <option value="Email">Email</option>
					                              <option value="Work FAX">Work FAX</option>
					                              <option value="Home FAX">Home FAX</option>
					                              <option value="Pager">Pager</option>
					                              <option value="Company Main">Company Main</option>
					                              <option value="Other">Other</option>
					                              <option value="Other FAX">Other FAX</option>
					                              <option value="Assistant">Assistant</option>
					                              <option value="Callback">Callback</option>
					                              <option value="ISDN">ISDN</option>
					                              <option value="Main">Main</option>
					                              <option value="Radio">Radio</option>
					                              <option value="Telex">Telex</option>
					                              <option value="TDD/TTY">TDD/TTY</option>
				                            </select>
								</td>
								<td class = "project_search_results">
									<input class = "form-control" type = "text" name = "fax1"  value = "<?php print $c['fax1']; ?>">
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results" colspan = "2">
									Notes:<br>
									<textarea class = "form-control" name = "notes" id = "notes" rows = "5"><?php print $c['notes']; ?></textarea>
								</td>
								</tr>

								<tr class = "project_search_results">
								<td class = "project_search_results">
									&nbsp;
								</td>
								<td class = "project_search_results">
									<input type = "hidden" name = "vendor_id" value = "<?php print $vendor_id; ?>">
									<input type = "hidden" name = "contact_id" value = "<?php print $contact_id; ?>">
									<input type = "hidden" name = "is_new" value = "<?php print $is_new; ?>">
									<input type = "hidden" name = "action" value = "1">
									<input type = "submit" value = "SAVE" class = "btn btn-primary">
									</form>
								</td>
								</tr>
								</table>
										     
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>


<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // prevent indent on textareas
	var notes = $('#notes').html();
	$("#notes").html($.trim(notes));
    
});
</script>

</body>
</html>