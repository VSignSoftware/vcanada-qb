<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
if($project['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}
$design_id = $_REQUEST['designid'];
$design = $vujade->get_design($design_id);
if($design['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}

$type1 = preg_match("/Presentation/", $design['type']) ? 1 : 0;
$type2 = preg_match("/Const. Dwgs./", $design['type']) ? 1 : 0;
$type3 = preg_match("/Production/", $design['type']) ? 1 : 0;
$type4 = preg_match("/Permits Required/", $design['type']) ? 1 : 0;
$f1 = preg_match("/11x17/", $design['type_sub']) ? 1 : 0;
$f2 = preg_match("/8.5x11/", $design['type_sub']) ? 1 : 0;
$f3 = preg_match("/photocomp/", $design['type_sub']) ? 1 : 0;
$f4 = preg_match("/thumbnail/", $design['type_sub']) ? 1 : 0;
$f5 = preg_match("/8.5x14/", $design['type_sub']) ? 1 : 0;

$charset="ISO-8859-1";
$html = '
            <!DOCTYPE html>
            <head>
            <meta charset="'.$charset.'">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Design Request</title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/print_design_style.css">
            </head>
            <body>
            ';

$html.='<h3>Request:</h3>';
$html.='<p>'.$design['request_description'].'</p>';
$html.='</body></html>';

$header_1 ='
            <div class = "header"><h1>Design Request</h1></div>
            <div class = "header2">
            <div class = "checkbox">';

            # checkboxes
            $header_1 .= '<input type = "checkbox"';
            if($type1==1)
            {
                $header_1 .= ' checked = "checked"';
            }
            $header_1 .= '>Presentation
            </div>

            <div class = "checkbox">
            <input type = "checkbox"';
            if($type2==1)
            {
                $header_1 .= ' checked = "checked"';
            }
            $header_1 .= '>Construction
            </div>

            <div class = "checkbox">
            <input type = "checkbox"';
            if($type3==1)
            {
                $header_1 .= ' checked = "checked"';
            }
            $header_1 .= '>Production
            </div>

            <div class = "checkbox" style = "width:20%;">
            <input type = "checkbox"';
            if($type4==1)
            {
                $header_1 .= ' checked = "checked"';
            }
            $header_1 .= '>Permits Required
            </div>

            <div class = "checkbox">
            <input type = "checkbox"';
            if($f3==1)
            {
                $header_1 .= ' checked = "checked"';
            }
            $header_1 .= '>Photocomp
            </div>

            <div class = "checkbox_last">
            <input type = "checkbox"';
            if($f4==1)
            {
                $header_1 .= ' checked = "checked"';
            }
            $header_1 .= '>Thumbnail
            </div>
            </div>
            ';

# design number, project name, address, other details
$header_1.='<div class = "row3">';
$header_1.='<table width = "100%">';
$header_1.='<tr>';
$header_1.='<td class = "first" valign = "top">';
$header_1.=$design['design_id'];
$header_1.='<br>';
$title = (strlen($project['site']) > 40) ? substr($project['site'],0,37).'...' : $project['site'];
$header_1.=$title;
$header_1.='</td>';

$header_1.='<td class = "second" valign = "top">';
$header_1.='<table width = "100%">
<tr>
<td valign = "top" class = "width170">
Site:<br>
<table><tr><td class = "mr">';
$header_1.=$project['address_1'];
if(!empty($project['address_2']))
{
    $header_1.=', '.$project['address_2'];  
}
$header_1.='<br>';
$header_1.=$project['city'].', '.$project['state'].' '.$project['zip'];
$header_1.='</td></tr></table>
</td>

<td valign = "top" class = "third">
Project Contact:<br>
<table><tr><td class = "mr">';
$header_1.=$project['project_contact'].'&nbsp;';
$header_1.='</td></tr></table>
Salesperson:<br>
<table><tr><td class = "mr">';
$header_1.=$project['salesperson'];
$header_1.='</td></tr></table>
</td>

<td valign = "top" class = "width215">
<table width = "100%">
<tr>
<td width = "60%" valign = "top">
Priority:
</td>
<td width = "40%" valign = "top" class = "textright">
'.$design['priority'].'
</td>
</tr>

<tr>
<td width = "60%" valign = "top">
Date Submitted:
</td>
<td width = "40%" valign = "top" class = "textright">
'.$design['request_date'].'
</td>
</tr>

<tr>
<td width = "60%" valign = "top">
Date Required:
</td>
<td width = "40%" valign = "top" class = "textright">
'.$design['request_required_date'].'
</td>
</tr>
</table>
</td>

</tr>
</table>';
$header_1.='</td>';
$header_1.='</tr>';
$header_1.='</table>';

$header_1.='</div>';

# footer
$footer_1.='<div class = "footer_top">';

# notes references and completion date
$footer_1.='<table width = "100%" class = "bt">';
$footer_1.='<tr>';
$footer_1.='<td class = "footer_left" valign = "top">Notes:';
$footer_1.='</td>';
$footer_1.='<td class = "footer_right" valign = "top">';
$footer_1.='<table width = "50%">';
$footer_1.='<tr>';
$footer_1.='<td valign = "top" class = "alignright">';
$footer_1.='Completion Date:<br>';
$footer_1.='Designer:<br>';
$footer_1.='Time:';
$footer_1.='</td>';
$footer_1.='</tr>';
$footer_1.='</table>';
$footer_1.='</td>';
$footer_1.='</tr>';
$footer_1.='</table></div>';
$footer_1 .= '<div class = "pager">Page {PAGENO}</div>';

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 80, 40, 10);
//$mpdf->setAutoTopMargin = 'stretch';
//$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($html);

// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-design.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output($project_id.'.pdf','I'); 
}

?>