<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$vendors = $vujade->get_vendors(1,0,1);
if($vendors['error']=="0")
{
	unset($vendors['error']);
	//unset($vendors['result_count']);
	print '<table class = "table">';
	print '<tr><td width = "15%"><strong>ID</strong></td><td width = "85%"><strong>Name</strong></td></tr>';
	foreach($vendors as $vendor)
	{
		print '<tr class = "clickableRow" id = "'.$vendor['database_id'].'">';

		print '<td valign = "top">';
		print $vendor['database_id'];
		print '</td>';
		
		print '<td valign = "top">';
		print $vendor['name'];
		print '</td>';

		print '</tr>';
	}
	print '</table>';
}
else
{
	print '<em>No results matched your search.</em>';
	//print $vendor['error'];
}
?>
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$('.clickableRow').click(function()
	{
		var id = this.id;
		$('#vendor_id').val(id);
		$.post("jq.get_vendor_data.php", 
		{ 
			id: id
		})
		.done(function( data ) 
		{
			$('#vendor_info').html('');
			$('#vendor_info').html(data);
		});
	});

	// vendor contact change...?

});	
</script>