<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$setup = $vujade->get_setup();
$state_sales_tax=$setup['state_sales_tax'];
$invoice_setup = $vujade->get_invoice_setup();

$s = array();
$posted = 0;

// shop order
$so = $vujade->get_shop_order($id,$idcol="project_id");
$proposal = $vujade->get_proposal($so['proposal'],'proposal_id');

# create a new invoice
if(!isset($_REQUEST['invoice_id']))
{
	# determine what the invoice number is
	$invoices = $vujade->get_invoices_for_project($project_id);
	if($invoices['error']!='0')
	{
		$next_id=$project_id."-1";
	}
	else
	{
		$next_id=$project_id."-".$invoices['next_id'];
	}
	$vujade->create_row('quickbooks_invoice');
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'project_id',$project_id);
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'RefNumber',$next_id);
	
	# get correct customer info for this project
	$listiderror=0;
	$localiderror=0;
	// try to get by list id
    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
    if($customer1['error']!="0")
    {
    	$listiderror++;
    	unset($customer1);
    }
    else
    {
    	$customer=$customer1;
    }
    // try to get by local id
    $customer2 = $vujade->get_customer($project['client_id'],'ID');
    if($customer2['error']!="0")
    {
    	$localiderror++;
    	unset($customer2);
    }
    else
    {
    	$customer=$customer2;
    }

    $iderror=$listiderror+$localiderror;

    if($iderror<2)
    {
    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
    }
    else
    {
    	// can't find customer or no customer on file
    }

	$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'CustomerRef_ListID',$customer['database_id']);

	//$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID','80000004-1323807210');

	// get and set the sales tax code 
	// qb tax state is the same as shipping address
	if($project['state']==$setup['qb_tax_state'])
	{
		$tax_data = $vujade->QB_get_sales_tax($project['city']);
	}
	else
	{
		// not the same
		$tax_data = $vujade->QB_get_sales_tax('Out of State');
	}
	if($tax_data['error']=="0")
	{
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data['ListID']);
	}
	else
	{
		$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
		$s[]=$vujade->update_row('quickbooks_invoice',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data2['ListID']);
	}

	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$s[] = $vujade->update_row('quickbooks_invoice',$vujade->row_id,'TxnID',$fakeid);

	$invoice_id = $next_id;
	$invoice_db_id = $vujade->row_id;
}
else
{
	$invoice_id = $_REQUEST['invoice_id'];
	$invoice_db_id = $_REQUEST['invoice_db_id'];
}

// page has been posted for processing
if(isset($_REQUEST['action']))
{

	$date=$_POST['date'];
	if(empty($date))
	{
		$date = date('m/d/Y');
	}
	
	// fix the date; must be in this format: mm-dd-yyyy
	$qbdate = strtotime($date);
	$qbdate = date('Y-m-d',$qbdate);

	$memo = $_POST['description'];
	//$memo=strip_tags($memo);
	//$memo=str_replace('&nbsp','',$memo);
	//$memo=str_replace(';','',$memo);
	$type=$_POST['type'];
	$type2=$_POST['type2'];
	$lump_sum=str_replace(',', '', $_POST['lump_sum']);
	$parts = str_replace(',', '', $_POST['parts']);
	$labor = str_replace(',', '', $_POST['labor']);
	$tax_rate=$_POST['tax_rate'];
	$tax_amount=str_replace(',', '', $_POST['tax_amount']);

	$change_orders=str_replace(',', '', $_POST['change_orders']);
	$engineering=str_replace(',', '', $_POST['engineering']);
	$permits=str_replace(',', '', $_POST['permits']);
	$permit_acquisition=str_replace(',', '', $_POST['permit_acquisition']);
	$shipping=str_replace(',', '', $_POST['shipping']);
	$retention=str_replace(',', '', $_POST['retention']);
	$retention=str_replace('-', '', $retention);
	
	// discount
	$discount=str_replace(',', '', $_POST['discount']);
	$discount=str_replace('-', '', $discount);
	$discount='-'.$discount;

	// deposit
	$deposit=$_POST['deposit'];
	$deposit=str_replace(',', '', $deposit);
	$deposit=str_replace('-', '', $deposit);
	$deposit='-'.$deposit;
	
	$cc_processing_fees = str_replace(',', '', $_POST['cc_processing_fees']);
	$po_id = $_POST['po_id'];
	$invoice_id=$_POST['invoice_id'];
	$invoice_db_id = $_REQUEST['invoice_db_id'];

	$invoice=$vujade->get_invoice($invoice_db_id);
	$dbid = $invoice_db_id;

	// purchase order number is saved in the shop order table
	// disabled per Aaron's request, 4-29-2016
	// this is now saved to the qb table
	// $s[]=$vujade->update_row('shop_orders',$so['database_id'],'po_number',$po_id);

	// update the parent record
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'TxnDate',$qbdate);
			$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ts',strtotime($date));
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'advanced_deposit',$advanced_deposit);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'advanced_deposit_rate',$setup['advanced_deposit']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_description',$memo);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'tax',$tax_rate);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'type_1',$type);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'type_2',$type2);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'v_web_sales_tax',$tax_amount);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'po_number',$po_id);

	// resale option: update tax 
	$resale = $_POST['resale'];
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'is_resale',$resale);

	// resale is yes (1), is the qb home state the same as the project shipping state?
	if($resale==1)
	{
		$tax_data = $vujade->QB_get_sales_tax('Resale');
		if($tax_data['error']=="0")
		{
			$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID',$tax_data['ListID']);

			// non taxable list id
			$tax_list_id = '80000002-1322849508';
		}
	}
	else
	{
		// resale is no (0)
		// check if the qb home state is the same as project state
		if($project['state']==$setup['qb_tax_state'])
		{
			$tax_data = $vujade->QB_get_sales_tax($project['city']);
			$tax_list_id = $tax_data['ListID'];
			// Set ItemSalesTaxRef.
            $s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID',$tax_data['ListID']);
            // Change tax_list_id to Non-Taxable for List Items.
            // $tax_list_id='80000002-1322849508';
            $tax_list_id='';
		}
		else
		{
			// not the same 
			// set ItemSalesTaxRef to Out of State.
			$tax_list_id='80000003-1323807176';
            $s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID',$tax_data['ListID']);
            // Change tax_list_id to Non-Taxable for List Items.
            $tax_list_id='80000002-1322849508';
		}
	}

	// ship to address
	/*
	ShipAddress_Addr1	
	ShipAddress_Addr2	
	ShipAddress_Addr3	
	ShipAddress_Addr4	
	ShipAddress_Addr5	
	ShipAddress_City	
	ShipAddress_State	
	ShipAddress_PostalCode	
	ShipAddress_Country
	*/

	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_Addr1',$project['site']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_Addr2',$project['address_1']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_City',$project['city']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_State',$project['state']);
	$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ShipAddress_PostalCode',$project['zip']);

	/*
	print $project['site'].'<br>';
	print $project['address_1'].'<br>';
	print $project['city'].', ';
	print $project['state'].' ';
	print $project['zip'].'<br>';;
	// county
	$county=$vujade->get_county($project['city']);
	if($county['error']=="0")
	{
		print $county['county'];
	}
	*/

	// create line items

	// variations

	// new logic 2-17-2016
	// only split the change order into taxable and non-taxable if illuminated
	/*
	Brad and I went through several steps with this invoice today.  What we have found is that the system is splitting all change order amounts into the two categories (taxable and non-taxable).  
	This should only happen if the invoice was marked “Illuminated”.  
	We tested a lot of things and have found that if the salesperson accidentally marks the proposal as “Illuminated”, that setting carries across to the amount in the change order line.  It doesn’t affect the sale price, but anything in the change order line is taxed incorrectly at the illuminated rate even though the “non-illuminated” box was checked when creating the invoice.
	*/

	// 1: illuminated and lump sum
	if( ($type==1) && ($type2==1) )
	{
		$labor_only = $lump_sum - ($lump_sum*$invoice_setup['sale_price_1']);
		$parts_cost = $lump_sum * $invoice_setup['sale_price_1'];

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_FullName','Resale');

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID','');

		/* change orders update
		It could happen in any lump sum contract.  It should not happen in parts & labor jobs.

		IE: variations 1 and 3 ONLY

		You would need to split the CO into two items when sending to QB just like the sale price is split.

		So $100 x .33 goes as Change Order 
		$100 x (100-.33) goes as Non-Taxable CO

		*/

		// non-taxable co
		$ntco = $change_orders;
		$change_orders=$change_orders*$state_sales_tax;
		$ntco = $ntco-$change_orders;

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Non-Taxable CO','$ntco',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 
	}

	// 2: illuminated sign parts and labor
	if( ($type==1) && ($type2==2) )
	{
		$labor_only = $labor;
		$parts_cost = $parts;

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_FullName','Resale');

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID','');
	}

	// 3: non-illuminated lump sum
	if( ($type==2) && ($type2==1) )
	{
		$selling_price = $lump_sum*$invoice_setup['sale_price_2'];
		$invoice_type="Manufacture & Install";
		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','$invoice_type','$selling_price',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_FullName','Resale');

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID','');

		/* change orders update
		It could happen in any lump sum contract.  It should not happen in parts & labor jobs.

		IE: variations 1 and 3 ONLY

		You would need to split the CO into two items when sending to QB just like the sale price is split.

		So $100 x .33 goes as Change Order 
		$100 x (100-.33) goes as Non-Taxable CO

		*/

		// non-taxable co
		//$ntco = $change_orders;
		//$change_orders=$change_orders*$state_sales_tax;
		//$ntco = $ntco-$change_orders;

		// do not split change orders
		/*
		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Non-Taxable CO','$ntco',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug);
		*/
	}

	// 4: non-illuminated sign parts and labor
	if( ($type==2) && ($type2==2) )
	{
		$labor_only = $labor;
		$parts_cost = $parts;

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_FullName','Resale');

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID','');
	}

	// 5: service with parts and labor
	if( ($type==3) && ($type2==2) )
	{
		$labor_only = $labor;
		$parts_cost = $parts;

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Labor Only','$labor_only',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`,`Desc`) VALUES ('$invoice_db_id','Parts','$parts_cost',1,'$fakeid','$memo')";
		$vujade->generic_query($sql,$debug); 

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_FullName','Resale');

		//$s[]=$vujade->update_row('quickbooks_invoice',$dbid,'ItemSalesTaxRef_ListID','');
	}
	
	// change order
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Change Order','$change_orders',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// engineering
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Engineering','$engineering',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// permits
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Permits','$permits',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// permit acq.
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Permit Acquisition','$permit_acquisition',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// retention
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Retention','$retention',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// shipping
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Shipping','$shipping',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// discount
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Discount','$discount',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	/* sales tax (don't send; qb to calculate)
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Sales Tax','$tax_amount',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 
	*/

	// deposit
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','Deposit','$deposit',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 

	// credit card processing fees
	$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$sql = "INSERT INTO `quickbooks_invoice_lineitem` (`Parent_ID`,`ItemRef_FullName`,`Amount`,`Quantity`,`TxnLineID`) VALUES ('$invoice_db_id','CC Processing Fee','$cc_processing_fees',1,'$fakeid')";
	$vujade->generic_query($sql,$debug); 
	
	// get every line item's database id and update the tax 
	$lines = $vujade->get_invoice_line_items($invoice_db_id,'ALL');
	$lines_cnt = count($lines);
	if($lines_cnt>0)
	{
		foreach($lines as $line_item)
		{
			$line_item_id = $line_item['database_id'];
			if($setup['site']=='ISA')
			{
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$line_item_id,'SalesTaxCodeRef_ListID',$tax_list_id,'TxnLineID');
			}
			if($setup['site']=="Tru Lite")
			{
				$s[]=$vujade->update_row('quickbooks_invoice_lineitem',$line_item_id,'SalesTaxCodeRef_FullName','Non-Taxable Sales','TxnLineID');
			}
		}
	}
	else
	{
		$error = "No line items found. Invoice Database ID: ".$invoice_db_id;
		die($error);
	}

	// send the invoice data to quickbooks
	// qb config
	require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
	$dsn = $vujade->qbdsn;

	// create item on qb server
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('America/Los_Angeles');
	}
	
	if (!QuickBooks_Utilities::initialized($dsn))
	{	
		// Initialize creates the neccessary database schema for queueing up requests and logging
		QuickBooks_Utilities::initialize($dsn);
		// This creates a username and password which is used by the Web Connector to authenticate
		QuickBooks_Utilities::createUser($dsn, $user, $pass);
	}

	// insert into the quickbooks vendor table
	$Queue = new QuickBooks_WebConnector_Queue($dsn);
	$Queue->enqueue(QUICKBOOKS_ADD_INVOICE, $invoice_db_id);

	$vujade->page_redirect('project_invoices.php?id='.$id.'&invoiceid='.$invoice_db_id);
}
else
{
	// page has not yet been posted for processing

	// set all to zero/null except where noted
	$advanced_deposit=0;
	$date='';
	$discount=0;
	//$selling_price = $so['selling_price'];
	
	$parts = 0;
	$change_orders=0;
	$engineering=0;
	$permits=0;
	$permit_acquisition=0;
	$shipping=0;
	$retention=0;
	$deposit=0;
	$tax_amount=$so['tax_amount'];
	$cc_processing_fees=0;

	// default tax rate
	// city sales tax
	$test_tax_rate = $vujade->get_tax_for_city($project['city'],$project['state']);
	if($test_tax_rate['error']=="0")
	{
		$tax_rate=$test_tax_rate['rate'];
	}
	else
	{
		$tax_rate=0;
	}

	$memo="";

	// payments amount
	$pmt=0;
	// this value will be inserted into the deposit field 
	// all payments made on all advanced deposits
	// and subtracted from the subtotal / balance due
	$adinvoices = $vujade->get_invoices_for_project($project['project_id']);
	if($adinvoices['error']=="0")
	{
		foreach($adinvoices as $adinvoice)
		{
			if($adinvoice['advanced_deposit']==1)
			{
				// get payments 
				$payment_data = $vujade->get_invoice_payments($adinvoice['database_id']);
				foreach($payment_data as $pd)
				{
					$pmt+=$pd['amount'];
				}
			}
		}
	}

	$deposit=$pmt;
	$total_due=$tax_amount+$selling_price;
	$subtotal2=0;
	$subtotal1=0;
	$total_payments=0;
	$balance_due=$tax_amount+$selling_price-$pmt;
	$po_id = $so['po_number'];
}

$section=3;
$menu=17;
$cmenu=1;
$title = 'New Invoice - '.$project['project_id'] . ' - ';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">
		<form method = "post" action = "new_invoice.php" id = "np">
		<div class = "row" style = "margin-bottom:15px;">
			<div class = "col-md-4">
				<strong>Number: <?php print $invoice_id; ?> (Proposal: 
				<?php print $shop_order['proposal']; ?>)
				</strong>
			</div>
			<div class = "col-md-8 pull-right">
				<strong>Date: <input type = "text" name = "date" value = "<?php print $date; ?>" class = "dp" style = "width:150px;margin-rigth:10px;"></strong> 
				<strong>PO #: <input type = "text" name = "po_id" id = "po_id" value = "<?php print $po_id; ?>" class = "" style = "width:150px;"></strong> 
			</div>
		</div>

		<div class = "row">
			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Billing Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
					# get correct customer info for this project
					$listiderror=0;
					$localiderror=0;
					// try to get by list id
				    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
				    if($customer1['error']!="0")
				    {
				    	$listiderror++;
				    	unset($customer1);
				    }
				    else
				    {
				    	$customer=$customer1;
				    }
				    // try to get by local id
				    $customer2 = $vujade->get_customer($project['client_id'],'ID');
				    if($customer2['error']!="0")
				    {
				    	$localiderror++;
				    	unset($customer2);
				    }
				    else
				    {
				    	$customer=$customer2;
				    }

				    $iderror=$listiderror+$localiderror;

				    if($iderror<2)
				    {
				    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
				    }
				    else
				    {
				    	// can't find customer or no customer on file

				    }
		            if($customer['error']=="0")
		            {
		            	print $customer['name'].'<br>';
		            	print $customer['address_1'].'<br>';
		            	if(!empty($customer['address_2']))
		            	{
		            		print $customer['address_2'].'<br>';
		            	}
		            	print $customer['city'].', '.$customer['state'].' '.$customer['zip'].'<br>';
		            }
					?>
				  </div>
				</div>
			</div>

			<div class="col-md-6" style = "width:49%;">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="padding: 0 8px">
                        <span class="panel-title" style="font-size: 13px; font-weight:600;">Job Information</span>
                    </div>
				  	<div class="panel-body" style = "height:150px;">
				    <?php
		            # get site location for this project
					print $project['site'].'<br>';
					print $project['address_1'].'<br>';
					print $project['city'].', ';
					print $project['state'].' ';
					print $project['zip'].'<br>';;
					// county
					$county=$vujade->get_county($project['city'],$project['state']);
					if($county['error']=="0")
					{
						print $county['county'];
					}
					?>
				  </div>
				</div>
			</div>
		</div>
	
		<input type = "hidden" id = "action" value = "1" name = "action">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">

		<div class = "row">
			<div class = "col-md-12">
			<strong>Description:</strong>
			<br>
			
			<center>
			<textarea id = "description" name = "description">
			<?php
			print $project['description'];
			?>
			</textarea>
			
			<!-- ckeditor -->
			<script src="vendor/plugins/ckeditor/ckeditor.js"></script>
			<script>
	        CKEDITOR.replace('description');
			CKEDITOR.config.toolbar = [
				{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
				{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl' ] },
				'/',
				{ name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
				{ name: 'colors', items: [ 'TextColor', 'BGColor' ] }
			];
			CKEDITOR.config.fontSize_sizes = '7/7pt;8/8pt;9/9pt;10/10pt;11/11pt;12/12pt;13/13pt;14/14pt;';
	        CKEDITOR.config.removePlugins = 'elementspath';
	        CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
	        CKEDITOR.config.tabSpaces = 5;
	        CKEDITOR.config.height = 550;
	        CKEDITOR.config.disableNativeSpellChecker = false;
	        </script>
			</div>
		</div>

		<p style = "height:15px;">&nbsp;</p>

		<!-- bottom block -->
		<div style = "clear:both;width:100%;margin-top:15px;margin-left:10px;margin-right:10px;">

			<style>
			.short
			{
				width:75px;
			}
			.bordered-row
			{
				border-bottom:1px solid #cecece;
				padding-bottom: 5px;
				height:30px;
			}
			</style>

			<!-- left block: input boxes -->
			<div class = "row">

				<div style = "clear:both;width:100%;margin-top:15px;margin-bottom:5px;margin-left:10px;">

				<div id = "" style = "width:100%;margin-top:5px;margin-bottom:5px;">
					<strong>Step 1: Select Invoice Type</strong><br>
					<input type = "radio" name = "type" class = "type" value = "1"><?php print $invoice_setup['label_1']; ?><br>
					<input type = "radio" name = "type" class = "type" value = "2"><?php print $invoice_setup['label_2']; ?><br>
					<input type = "radio" name = "type" class = "type" value = "3"><?php print $invoice_setup['label_3']; ?>
				</div>

				<hr>

				<div id = "step_2" style = "display:none;width:100%;margin-top:5px;margin-bottom:5px;">
					<strong>Step 2: Lump Sum or Labor and Parts</strong><br>
					<input type = "radio" name = "type2" id = "lump_sum_radio" class = "type2" value = "1">Lump Sum<br>
					<input type = "radio" name = "type2" id = "labor_and_parts_radio" class = "type2" value = "2">Labor and Parts<br>
					<hr>
				</div>

				<div id = "step_3" style = "display:none;width:100%;margin-top:5px;margin-bottom:5px;">
					<strong>Step 3: Line Items</strong><br>
					
					<input type = "hidden" id = "htype1"> 
					<input type = "hidden" id = "htype2">

					<br>

					Accepted Proposal Amount: $
					<?php 
					$am=$proposal['amounts']-$proposal['tax_total'];
					print @number_format($am,2); 
					?>
					<br>

					<!-- bottom block -->
					<div style = "clear:both;width:100%;margin-top:15px;margin-left:10px;margin-right:10px;">

						<!-- left block: input boxes -->
						<div style = "float:left; width: 650px; height:300px; font-weight:bold;">

							<div id = "input_boxes">

								<!-- left -->
								<div style = "float:left;margin-right:15px;width:300px;padding:3px;height:310px;">
									<table width = "100%">
									
									<tr id = "lump_sum_row" style = "display:none;">
										<td>Lump Sum: </td>
										<td><input type = "text" name = "lump_sum" id = "lump_sum" value = "<?php print $selling_price; ?>" class = "short"></td>
									</tr>

									<tr id = "labor_row" style = "display:none;">
										<td>Labor: </td>
										<td><input type = "text" name = "labor" id = "labor" value = "" class = "short"></td>
									</tr>

									<tr id = "parts_row" style = "display:none;">
										<td>Parts: </td>
										<td><input type = "text" name = "parts" id = "parts" value = "" class = "short"></td>
									</tr>

									<tr>
										<td>Change Order: </td>
										<td><input type = "text" name = "change_orders" id = "change_orders" value = "<?php print @number_format($change_orders,2,'.',','); ?>" class = "short"></td>
									</tr>

									<tr>
										<td>Engineering: </td>
										<td><input type = "text" name = "engineering" id = "engineering" value = "<?php print @number_format($engineering,2,'.',','); ?>" class = "short"></td>
									</tr>
									<tr>
										<td>Permits: </td>
										<td><input type = "text" name = "permits" id = "permits" value = "<?php print @number_format($permits,2,'.',','); ?>" class = "short"></td>
									</tr>

									<tr>
										<td>Permit Acquisition: </td>
										<td><input type = "text" class = "short" name = "permit_acquisition" id = "permit_acquisition" value = "<?php print @number_format($permit_acquisition,2,'.',','); ?>"></td>
									</tr>

									<tr>
										<td>Shipping: </td>
										<td><input type = "text" class = "short" name = "shipping" id = "shipping" value = "<?php print @number_format($shipping,2,'.',','); ?>"></td>
									</tr>

									<tr>
										<td>Credit Card Processing: </td>
										<td><input type = "text" class = "short" name = "cc_processing_fees" id = "cc_processing_fees" value = "<?php print @number_format($cc_processing_fees,2,'.',','); ?>"></td>
									</tr>
					
									<tr>
										<td>
											Discount: <br>
											Subtotal: 
										</td>
										<td>
											<input type = "text" class = "short" name = "discount" id = "discount" value = "<?php print @number_format($discount,2,'.',','); ?>">
											<br>
											<input type = "text" class = "short" name = "subtotal1" id = "subtotal1" value = "<?php print @number_format($subtotal1,2,'.',','); ?>" disabled>
										</td>
									</tr>

									</table>
								</div>

								<!-- right -->
								<div style = "float:right;width:300px;padding:3px;height:310px;">
									<table width = "100%">
										<tr>
											<td>Tax Rate: </td>
											<td><input type = "text" name = "tax_rate" class = "short" id = "tax_rate" value = "<?php print round($tax_rate,4); ?>"></td>
										</tr>
										
										<tr>
											<td>Tax: </td>
											<td><input type = "text" name = "tax_amount" id = "tax_amount" value = "" class = "short" disabled></td>
										</tr>

										<tr>
											<td>Subtotal: </td>
											<td><input type = "text" name = "subtotal2" id = "subtotal2" class = "short" value = "<?php print @number_format($subtotal2,2,'.',','); ?>" disabled></td>
										</tr>

										<tr>
											<td>Deposit: </td>
											<td>
												<?php 
												if($advanced_deposit==1)
												{
												?>
												<input type = "text" class = "short" name = "deposit" id = "deposit" disabled value = "">
												<?php
												}
												else
												{
													?>
													<input type = "text" class = "short" name = "deposit" id = "deposit" value = "<?php print @number_format($deposit,2,'.',','); ?>">
													<?php
												}
												?>
											</td>
										</tr>

										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>

										<tr>
											<td>Retention: </td>
											<td><input type = "text" class = "short" name = "retention" id = "retention" value = "<?php print @number_format($retention,2,'.',','); ?>"></td>
										</tr>

										<tr>
											<td>Total Due: </td>
											<td><input type = "text" class = "short" name = "total_due" id = "total_due" value = "" disabled></td>
										</tr>

										<tr>
											<td>&nbsp;</td>
											<td>&nbsp;</td>
										</tr>

										<tr>
											<td colspan = "2">
												<center>
													<a href = "#" class = "btn btn-primary" style = "" id = "update_totals">UPDATE TOTALS</a>
												</center>
											</td>
										</tr>

									</table>
								</div>
							</div>

						</div>
						
						<!-- right hand side (payments) -->
						<div style = "float:right; width: 230px; height:300px; font-weight:bold;">
							
						</div>

					</div>

					<hr style = "clear:both;width:97%;">

					<strong>Step 4: Is this invoice resale?</strong><br>
					<input type = "radio" class = "resale" name = "resale" id = "resale" class = "" value = "1">Yes
					<br><input type = "radio" class = "resale" name = "resale" id = "resale" class = "" value = "0" checked>No

					<hr style = "clear:both;width:97%;">

					<input type = "hidden" name = "invoice_id" id = "invoice_id" value = "<?php print $invoice_id; ?>">

					<input type = "hidden" name = "invoice_db_id" id = "invoice_db_id" value = "<?php print $invoice_db_id; ?>">

					<center></center>
					<input type = "submit" value = "SAVE AND CLOSE" id = "done" class = "btn btn-primary" style = ""><br>
					<em>Steps 1, 2 and 4 cannot be changed once you press this button.</em>
					
					</form>

				</div>
			</div>

			</div>	
			</div>

		</div>

    </div>
</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	$('.dp').datepicker();

    var d = $('#description').html();
    $("#description").html($.trim(d));

    $('.type').change(function()
	{
		$('#step_2').hide();
		$('#step_3').hide();

		$('#labor_and_parts_radio').prop("checked", false);
		$('#lump_sum_radio').prop("checked", false);

		// reset values in step 3
		$('#lump_sum').val('');
		$('#labor').val('');
		$('#parts').val('');

		$('#step_2').show();

		var option = $(this).val();
		if(option==3)
		{
			// service was selected; 
			// auto select parts and labor and show step 3
			$('#labor_and_parts_radio').prop("checked", true);
			$('#htype2').val(2);
			$('#step_3').show();
			$('#labor_row').show();
			$('#parts_row').show();
			$('#lump_sum_row').hide();
		}
		$('#htype1').val(option);
	});

	// step 2 change
	$('.type2').change(function()
	{
		$('#step_3').hide();

		// reset values in step 3
		$('#lump_sum').val('');
		$('#labor').val('');
		$('#parts').val('');

		$('#lump_sum_row').hide();
		$('#labor_row').hide();
		$('#parts_row').hide();

		var option = $(this).val();
		var type1 = $('#htype1').val();
    	//var type2 = $('#htype2').val();

		// error trap: can't be lump sum if type1 = 3
		if(type1==3 && option==1) 
		{
			alert('Only labor and parts dollar amounts are available for service/install/survey invoices.');
			return false;
		}

		// lump sum
		if(option==1)
		{
			$('#lump_sum_row').show();
		}

		// parts and labor
		if(option==2)
		{
			$('#labor_row').show();
			$('#parts_row').show();
		}

		$('#step_3').show();

		$('#htype2').val(option);

	});
		
	// resale change
	$('.resale').change(function()
	{
		var v = $(this).val();
		if(v==1)
		{
			$('#tax_amount').val('0');
			$('#tax_rate').val('0');
		}
	});

	$('body').on('click','#update_totals',function(e)
	{
		e.preventDefault();

    	// type
    	var type1 = $('#htype1').val();
    	var type2 = $('#htype2').val();
    	//var type1 = $('input[name=type]:checked', '#form').val();
    	//var type2 = $('input[name=type2]:checked', '#form').val();
    	//alert(type1+' '+type2);
    	if(type2==1)
    	{
    		var lump_sum=parseFloat($('#lump_sum').val().replace(/,/g, ""));
    		var parts=0;
    		var labor=0;
    	}
    	if(type2==2)
    	{
    		var parts=parseFloat($('#parts').val().replace(/,/g, ""));;
    		var labor=parseFloat($('#labor').val().replace(/,/g, ""));;
    		var lump_sum=0;
    	}

    	// column 1 values
    	var change_orders=parseFloat($('#change_orders').val().replace(/,/g, ""));
    	var engineering=parseFloat($('#engineering').val().replace(/,/g, ""));
    	var permits=parseFloat($('#permits').val().replace(/,/g, ""));
    	var permit_acquisition=parseFloat($('#permit_acquisition').val().replace(/,/g, ""));
    	var shipping=parseFloat($('#shipping').val().replace(/,/g, ""));
    	var discount=parseFloat($('#discount').val().replace(/,/g, ""));
    	var cc=parseFloat($('#cc_processing_fees').val().replace(/,/g, ""));

    	// column 2 values
    	var tax_rate=parseFloat($('#tax_rate').val().replace(/,/g, ""));
    	var deposit=parseFloat($('#deposit').val().replace(/,/g, ""));
    	var retention=parseFloat($('#retention').val().replace(/,/g, ""));

    	// advanced deposit
		if($('#advanced_deposit').is(':checked'))
		{
			var adv=1;
		}
		else
		{
			var adv=0;
		}

		var resale = $("input[name='resale']:checked").val();

		// asynch post to php script for correct calculations
    	$.post("jq.format_price.php", { action: 1, lump_sum: lump_sum, parts: parts, labor: labor, co: change_orders, e: engineering, p: permits, pa: permit_acquisition, shipping: shipping, discount: discount, tax_rate: tax_rate, deposit: deposit, retention: retention, adv: adv, cc: cc, type1: type1, type2: type2, resale: resale })
		.done(function(result) 
		{
			$('#input_boxes').html('');
			$('#input_boxes').html(result);
		});
	});

	$('#done').click(function()
	{
		$('#tax_amount').prop('disabled',false);
		$('#form').submit();
	});

});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
