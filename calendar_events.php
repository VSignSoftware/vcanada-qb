<?php
session_start();
//error_reporting(E_ALL);
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	die('no permissions');
}
$admin_permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');

# get all events
$event_ids=$vujade->get_all_events();
unset($event_ids['error']);
$event_ids=array_unique($event_ids);

# build and output json data
$json = array();
foreach($event_ids as $id)
{
	$event = $vujade->get_event_data($id);
	$e = array();
	$e['id']=$event['id'].'-'.$event['column'];
	$e['title'].='<div class = "event-title">'.$event['project_id'].$event['type'].' '.$event['site'].'</div>';
	if($projects_permissions['edit']==1)
	{
		if(!empty($event['bottom_border']))
		{
			$e['title'].='<a href = "#openModal">
			<div class = "cp" id = "'.$event['id'].'" style = "border:1px solid gray;margin-bottom:2px;width:98%;margin-left:0px;height:8px;background-color:'.$event['bottom_border'].'">&nbsp;</div></a>';
		}
		if( (strtolower($event['bottom_border']=="#ffffff")) == "#ffffff")
		{
			$e['title'].='<a href = "#openModal">
			<div class = "cp" id = "'.$event['id'].'" style = "width:98%;margin-left:0px;height:8px;background-color:#FFFFFF;border:1px solid gray;margin-bottom:2px;">&nbsp;</div></a>';
		}
		if(empty($event['bottom_border']))
		{
			$e['title'].='<a href = "#openModal">
			<div class = "cp" id = "'.$event['id'].'" style = "width:98%;margin-left:0px;height:8px;background-color:#FFFFFF;border:1px solid gray;margin-bottom:2px;">&nbsp;</div></a>';
		}
	}
	else
	{
		if(!empty($event['bottom_border']))
		{
			$e['title'].='<a href = "#openModal">
			<div class = "cp" id = "'.$event['id'].'" style = "border:1px solid gray;margin-bottom:2px;width:98%;margin-left:0px;height:8px;background-color:'.$event['bottom_border'].'">&nbsp;</div></a>';
		}
		if( (strtolower($event['bottom_border']=="#ffffff")) == "#ffffff")
		{
			$e['title'].='<a href = "#openModal">
			<div class = "cp" id = "'.$event['id'].'" style = "width:98%;margin-left:0px;height:8px;background-color:#FFFFFF;border:1px solid gray;margin-bottom:2px;">&nbsp;</div></a>';
		}
		if(empty($event['bottom_border']))
		{
			$e['title'].='<a href = "#openModal">
			<div class = "cp" id = "'.$event['id'].'" style = "width:98%;margin-left:0px;height:8px;background-color:#FFFFFF;border:1px solid gray;margin-bottom:2px;">&nbsp;</div></a>';
		}
	}
	if($event['color']=="green")
	{
		$e['borderColor']="#1c7249";
	}
	if($event['color']=="black")
	{
		$e['borderColor']="#000000";
	}
	if($event['color']=="orange")
	{
		$e['borderColor']="#FF9900";
	}
	$e['backgroundColor']="#FFFFFF";
	$e['textColor']="#000000";
	$e['url']="project_job_status.php?id=".$event['project_id'];
	if($editable==1)
	{
		$e['editable']=true;
		$e['startEditable']=true;
		$e['durationEditable']=true;
	}
	if($editable==2)
	{
		$e['editable']=false;
		$e['startEditable']=false;
		$e['durationEditable']=false;
	}
	if($event['is_all_day']==1)
	{
		$e['allDay']=true;
		$e['start']=$event['start'];
		if(empty($event['end']))
		{
			$e['end']=$event['end'];
		}
		else
		{
			$e['end']=$event['end'];
		}
	}
	else
	{
		$e['allDay']=false;
		$e['start']=$event['start'].'T'.$event['start_hour'];
		$e['end']=$event['end'].'T'.$event['end_hour'];
	}
	$json[]=$e;
}

$show_ms=$_REQUEST['show_ms'];

// admin only view
if($admin_permissions['read']==1)
{
	// view of these must also be switched on
	if($show_ms==1)
	{
		// create an event for every saturday in the current year and set the total dollar amount of installation jobs for the previous week into each
		$ms_start = strtotime('today - 1 years'); 
		$ms_end = strtotime("today + 1 years");
		$saturday = strtotime("saturday", $ms_start);
		while($saturday <= $ms_end) 
		{
		    // get a total dollar amount for every installation job that is in the current week
		    $sunday = $saturday-518400;
		    $monday = $saturday-432000;
		    $tuesday = $saturday-345600;
		    $wednesday = $saturday-259200;
		    $thursday = $saturday-172800;
		    $friday = $saturday-86400;

		    // convert these to dates that the totaling function will accept
		    $sunday = date('Y-m-d',$sunday);
		    $monday = date('Y-m-d',$monday);
		    $tuesday = date('Y-m-d',$tuesday);
		    $wednesday = date('Y-m-d',$wednesday);
		    $thursday = date('Y-m-d',$thursday);
		    $friday = date('Y-m-d',$friday);
		    $sat = date('Y-m-d',$saturday);
		    $date_str = $sunday.','.$monday.','.$tuesday.','.$wednesday.','.$thursday.','.$friday.','.$sat;
		    $total = $vujade->get_ms_total($date_str);
		    $e = array();
		    //$e['start'] = $sat.'T23:59:59'; // specifying an end of the day time will put this event last in the day column
		    $e['start'] = $sat;
		    $e['end'] = $e['start'];
		    $saturday = strtotime("+1 weeks", $saturday);
			$e['id']='week_total';
			$e['title']='Week Total: '.$total;
			$e['borderColor']="#8FBC8F";
			$e['backgroundColor']="#8FBC8F";
			$e['textColor']="#FFFFFF";
			$e['allDay']=true;
			$e['editable']=false;
			$e['startEditable']=false;
			$e['durationEditable']=false;
			$json[]=$e;
		}
	}
}

// calendar works the best with json data; output in json format
//$json=utf8_encode($json);
print json_encode($json);
?>