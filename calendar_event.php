<?php
session_start();
//error_reporting(E_ALL);
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$id = $_REQUEST['id'];

// 91-installation
$idpieces = explode('-',$id);
$event_id = $idpieces[0];

$event = $vujade->get_event_data($event_id);

$view=$_REQUEST['v'];
$start=$_REQUEST['s'];
//$start = strtotime($start);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['edit']!=1)
{
	die('You do not have permission to edit this event.');
	//$vujade->page_redirect('error.php?m=1');
}

$calendar_permissions = $vujade->get_permission($_SESSION['user_id'],'Calendar');
if($calendar_permissions['edit']!=1)
{
    die('You do not have permission to edit this event.');
}
?>

<form id = "form">
<p>Please pick the color code of your choice:</p>
<?php 
$legend = $vujade->get_legend_data();
if($legend['error']==0)
{
	unset($legend['error']);
	unset($legend['count']);
	// show radios for color
	$d.='<table>';
	$d.='<b style = "margin-left:5px;">Pick a new color for this job:</b><br><br>';
	foreach($legend as $l)
	{
		$hc=str_replace('#','',$l['hex']);
	    $d.='<tr>';
	    $d.='<td>';
	    $d.='<input type = "radio" name = "color" class = "color" value = "'.$hc.'"></td>';
	    $d.='<td>';
	    $d.='<div style = "border:1px solid #eeeeee; width:100px;height:15px;background-color:'.$l['hex'].';display:block;float:left"></div>';
	    $d.='</td>';
	    $d.='<td> = ';
	    $d.=$l['explanation'];
	    $d.='</td>';
	    $d.='</tr>';
	    unset($hc);
	}
	$d.='</table>';
	print $d;
}
?>

<input type = "hidden" name = "event_id" value = "<?php print $event_id; ?>" id = "event_id">

</form>

<br>

<a href = "project.php?id=<?php print $event['project_id']; ?>" class = "btn btn-success">View Project</a> <button id = "close-modal" class = "btn btn-danger">Cancel</button>

<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script>

/*
// save button
$('#save').click(function(e)
{
	e.preventDefault();
	var id = "<?php print $event_id; ?>";
	var c = $('input[name=color]:checked', '#form').val()
	$.post("calendar_color_update.php", { id: id, hex: c })
	.done(function( data ) 
	{
		//$('#result2').css('border','2px solid #3275cd');
	    $('#result2').html(data);
	    $('#result2').show();
	    $('#done-p').show();
	});
});
*/
// done button
/*
$('#done').click(function(e)
{
	e.preventDefault();
	var id = "<?php print $event_id; ?>";
	window.location.href = "calendar_view_fix.php?eventid="+id;
});
*/
</script>

<!--
<p style = "clear:both;">2) Press the save button to save the change:<br><a href = "#" class = "btn btn-sm btn-success" id = "save">SAVE</a></p>

<div id = "result" class = "alert alert-success" style = "display:none;"></div>
<div id = "result2" class = "alert alert-success" style = "display:none;"></div>

<p style = "clear:both;display:none;" id = "done-p">3) Press the done button to exit and refresh the calendar:<br><a href = "calendar.php?view=<?php print $view; ?>&start=<?php //print $start; ?>" class = "btn btn-sm btn-success" id = "done">DONE</a></p>
-->
