<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

$setup = $vujade->get_setup();
$invoice_setup = $vujade->get_invoice_setup();
$state_sales_tax=$setup['state_sales_tax'];

$action = $_REQUEST['action'];
if($action==1)
{

	$type1 = $_REQUEST['type1'];
	$type2 = $_REQUEST['type2'];
	$lump_sum = $_REQUEST['lump_sum'];
	$parts = $_REQUEST['parts'];
	$labor = $_REQUEST['labor'];

	// left side
	$change_orders = $_REQUEST['co'];
	$engineering = $_REQUEST['e'];
	$permits = $_REQUEST['p'];
	$permit_acquisition = $_REQUEST['pa'];
	$shipping = $_REQUEST['shipping'];
	$discount = $_REQUEST['discount'];
	$discount=str_replace('-','',$discount);
	$cc_processing_fees = $_REQUEST['cc'];

	// right side
	$retention = $_REQUEST['retention'];
	$retention=str_replace('-','',$retention);	
	$advanced_deposit=$_REQUEST['adv'];
	$tax_rate = $_REQUEST['tax_rate'];
	$deposit = $_REQUEST['deposit'];

	$resale=$_REQUEST['resale'];

	// cases

	// 1: illuminated and lump sum
	if( ($type1==1) && ($type2==1) )
	{
		// taxable
		// $taxable=($lump_sum*$invoice_setup['sale_price_1'])+($change_orders*$invoice_setup['sale_price_7']);

		$taxable=$lump_sum+$change_orders;

		// tax amount 
		// $tax_amount = ($taxable * $tax_rate) * $state_sales_tax;
		$tax_amount=$taxable*$state_sales_tax*$tax_rate;

		if($resale==1)
		{
			$tax_amount=0;
		}

		// sum left hand side
		$subtotal1 = $lump_sum+$change_orders+$engineering+$permits+$permit_acquisition+$shipping+$cc_processing_fees-$discount;

		// subtotal right hand side
		$subtotal2 = $subtotal1 + $tax_amount;

		// total due
		$total_due = $subtotal2 - $retention - $deposit;

	}

	// 2: illuminated sign parts and labor
	if( ($type1==1) && ($type2==2) )
	{
		// taxable
		$taxable1 = $labor*$invoice_setup['labor_1']*$tax_rate;
		//$taxable2 = $parts*$invoice_setup['parts_1']*$tax_rate;
		$taxable2 = $parts*$tax_rate;

		// tax amount 
		$tax_amount = $taxable1+$taxable2;

		if($resale==1)
		{
			$tax_amount=0;
		}

		// sum left hand side
		$subtotal1 = $labor+$parts+$change_orders+$engineering+$permits+$permit_acquisition+$shipping+$cc_processing_fees-$discount;

		// subtotal right hand side
		$subtotal2 = $subtotal1 + $tax_amount;

		// total due
		$total_due = $subtotal2 - $retention - $deposit;
	}

	// 3: non-illuminated lump sum
	if( ($type1==2) && ($type2==1) )
	{

		// taxable
		//$taxable=($lump_sum*$invoice_setup['sale_price_2'])+($change_orders*$invoice_setup['sale_price_8']);
		$taxable=$lump_sum+$change_orders;

		// tax amount 
		//$tax_amount = ($taxable * $tax_rate) * $state_sales_tax;
		$tax_amount=$taxable*$tax_rate;

		if($resale==1)
		{
			$tax_amount=0;
		}

		// sum left hand side
		$subtotal1 = $lump_sum+$change_orders+$engineering+$permits+$permit_acquisition+$shipping+$cc_processing_fees-$discount;

		// subtotal right hand side
		$subtotal2 = $subtotal1 + $tax_amount;

		// total due
		$total_due = $subtotal2 - $retention - $deposit;
	}

	// 4: non-illuminated sign parts and labor
	if( ($type1==2) && ($type2==2) )
	{
		// taxable
		$taxable1 = $labor*$invoice_setup['labor_2']*$tax_rate;
		//$taxable2 = $parts*$invoice_setup['parts_2']*$tax_rate;
		$taxable2 = $parts*$tax_rate;

		// tax amount 
		$tax_amount = $taxable1+$taxable2;

		if($resale==1)
		{
			$tax_amount=0;
		}

		// sum left hand side
		$subtotal1 = $labor+$parts+$change_orders+$engineering+$permits+$permit_acquisition+$shipping+$cc_processing_fees-$discount;

		// subtotal right hand side
		$subtotal2 = $subtotal1 + $tax_amount;

		// total due
		$total_due = $subtotal2 - $retention - $deposit;
	}

	// 5: service with parts and labor
	if( ($type1==3) && ($type2==2) )
	{
		// taxable
		$taxable1 = $labor*$invoice_setup['labor_3']*$tax_rate;
		//$taxable2 = $parts*$invoice_setup['parts_3']*$tax_rate;
		$taxable2 = $parts*$tax_rate;

		// tax amount 
		$tax_amount = $taxable1+$taxable2;

		if($resale==1)
		{
			$tax_amount=0;
		}

		// sum left hand side
		$subtotal1 = $labor+$parts+$change_orders+$engineering+$permits+$permit_acquisition+$shipping+$cc_processing_fees-$discount;

		// subtotal right hand side
		$subtotal2 = $subtotal1 + $tax_amount;

		// total due
		$total_due = $subtotal2 - $retention - $deposit;
	}

	if($resale==1)
	{
		$tax_rate=0;
	}

	//print_r($_REQUEST);
	//print '<hr>Tax Amount: '.$tax_amount;
	//die;

?>

<!-- left block: input boxes -->
<div style = "float:left; width: 650px; height:300px; font-weight:bold;">

	<div id = "input_boxes">

		<!-- left -->
		<div style = "float:left;margin-right:15px;width:300px;padding:3px;height:310px;">
			<table width = "100%">
			
			<tr id = "lump_sum_row" style = "<?php if($type2!=1){ print 'display:none;'; } ?>">
				<td>Lump Sum: </td>
				<td><input type = "text" name = "lump_sum" id = "lump_sum" value = "<?php print $lump_sum; ?>" class = "short"></td>
			</tr>

			<tr id = "labor_row" style = "<?php if($type2!=2){ print 'display:none;'; } ?>">
				<td>Labor: </td>
				<td><input type = "text" name = "labor" id = "labor" value = "<?php print $labor; ?>" class = "short"></td>
			</tr>

			<tr id = "parts_row" style = "<?php if($type2!=2){ print 'display:none;'; } ?>">
				<td>Parts: </td>
				<td><input type = "text" name = "parts" id = "parts" value = "<?php print $parts; ?>" class = "short"></td>
			</tr>

			<tr>
				<td>Change Order: </td>
				<td><input type = "text" name = "change_orders" id = "change_orders" value = "<?php print @number_format($change_orders,2,'.',','); ?>" class = "short"></td>
			</tr>

			<tr>
				<td>Engineering: </td>
				<td><input type = "text" name = "engineering" id = "engineering" value = "<?php print @number_format($engineering,2,'.',','); ?>" class = "short"></td>
			</tr>
			<tr>
				<td>Permits: </td>
				<td><input type = "text" name = "permits" id = "permits" value = "<?php print @number_format($permits,2,'.',','); ?>" class = "short"></td>
			</tr>

			<tr>
				<td>Permit Acquisition: </td>
				<td><input type = "text" class = "short" name = "permit_acquisition" id = "permit_acquisition" value = "<?php print @number_format($permit_acquisition,2,'.',','); ?>"></td>
			</tr>

			<tr>
				<td>Shipping: </td>
				<td><input type = "text" class = "short" name = "shipping" id = "shipping" value = "<?php print @number_format($shipping,2,'.',','); ?>"></td>
			</tr>

			<tr>
				<td>Credit Card Processing: </td>
				<td><input type = "text" class = "short" name = "cc_processing_fees" id = "cc_processing_fees" value = "<?php print @number_format($cc_processing_fees,2,'.',','); ?>"></td>
			</tr>

			<tr>
				<td>
					Discount: <br>
					Subtotal: 
				</td>
				<td>
					<input type = "text" class = "short" name = "discount" id = "discount" value = "<?php print @number_format($discount,2,'.',','); ?>">
					<br>
					<input type = "text" class = "short" name = "subtotal1" id = "subtotal1" value = "<?php print @number_format($subtotal1,2,'.',','); ?>" disabled>
				</td>
			</tr>

			</table>
		</div>

		<!-- right -->
		<div style = "float:right;width:300px;padding:3px;height:310px;">
			<table width = "100%">
				<tr>
					<td>Tax Rate: </td>
					<td><input type = "text" name = "tax_rate" class = "short" id = "tax_rate" value = "<?php print round($tax_rate,4); ?>"></td>
				</tr>
				
				<tr>
					<td>Tax: </td>
					<td><input type = "text" name = "tax_amount" id = "tax_amount" value = "<?php print @number_format($tax_amount,2,'.',','); ?>" class = "short" disabled></td>
				</tr>

				<tr>
					<td>Subtotal: </td>
					<td><input type = "text" name = "subtotal2" id = "subtotal2" class = "short" value = "<?php print @number_format($subtotal2,2,'.',','); ?>" disabled></td>
				</tr>

				<tr>
					<td>Deposit: </td>
					<td>
						<?php 
						if($advanced_deposit==1)
						{
						?>
						<input type = "text" class = "short" name = "deposit" id = "deposit" disabled value = "">
						<?php
						}
						else
						{
							?>
							<input type = "text" class = "short" name = "deposit" id = "deposit" value = "<?php print @number_format($deposit,2,'.',','); ?>">
							<?php
						}
						?>
					</td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td>Retention: </td>
					<td><input type = "text" class = "short" name = "retention" id = "retention" value = "<?php print @number_format($retention,2,'.',','); ?>"></td>
				</tr>

				<tr>
					<td>Total Due: </td>
					<td><input type = "text" class = "short" name = "total_due" id = "total_due" value = "<?php print @number_format($total_due,2,'.',','); ?>" disabled></td>
				</tr>

				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>

				<tr>
					<td colspan = "2">
						<center>
							<a href = "#" class = "btn btn-primary" style = "" id = "update_totals">UPDATE TOTALS</a>
						</center>
					</td>
				</tr>

			</table>
		</div>
	</div>

</div>

<!-- right hand side (payments) -->
<div style = "float:right; width: 230px; height:300px; font-weight:bold;">
	
</div>


<?php
}
if($action==2)
{
	$n = $_POST['n'];
	$n = str_replace(",", "", $n);
	print @number_format($n,2,'.',',');
}
if($action==3)
{
	$n = $_POST['n'];
	$n = str_replace(",", "", $n);
	$n=str_replace('-','',$n);
	print $n;
}
?>