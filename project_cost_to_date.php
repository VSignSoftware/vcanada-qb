<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$id = $_REQUEST['id'];

// page requires "manager" permission
$permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');
if($permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$js = $vujade->get_job_status($id);

$setup = $vujade->get_setup();
if($setup['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$buyout_overhead_percentage=$setup['buyout_overhead'];
$general_overhead_percentage=$setup['general_overhead'];
$materials_overhead_percentage=$setup['materials_overhead'];
$machines_overhead_percentage=$setup['machines_overhead'];

$shop_order = $vujade->get_shop_order($id, 'project_id');

// only get data for estimates that are marked in the shop order
$estimate_ids = array();
$estimate_ids_2 = array();
$ex = explode('^',$shop_order['estimates']);
foreach($ex as $eid)
{
	if(!empty($eid))
	{
		$estimate_ids_2[]=$eid;
		$estimate = $vujade->get_estimate($eid,'estimate_id');
		if($estimate['error']==0)
		{
			$estimate_ids[]=$estimate['database_id'];
		}	
	}
}

/*
print_r($estimate_ids);
print '<hr>';
print_r($estimate_ids_2);
die;
*/

$c = count($estimate_ids);
$c2 = count($estimate_ids_2);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$menu=14;
$title = $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left 
        <aside class="tray tray-left tray320 p20">
		-->
        <?php require_once('project_left_tray.php'); ?>

        <!-- end: .tray-left </aside>-->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">

            <div class="pl15 pr15" style = "width:100%;">

            	<?php require_once('project_right_tray.php'); ?>

            	<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				  </div>
				  <div class="panel-body">
					<table style = "" class = "table">
						<tr>
							<td><strong><u>Labor Hours</u></strong></td>
							<td><strong><u>Estimated</u></strong></td>
							<td><strong><u>Actual</u></strong></td>
						</tr>

						<?php
						// get estimated and actual labor (sum of all labor in all estimates for this job + sum of all labor in time cards)
						unset($estimates['error']);
						$estimated_hours = $vujade->get_total_estimated_hours($id,$estimate_ids_2);

						//print 'Estimates: <br>';
						//print_r($estimates);
						//print 'Estimated hours: <br>';
						// print_r($estimated_hours);
						unset($estimated_hours['error']);
						$c = count($estimated_hours);
						//print 'c: '.$c;
						//die;

						$actual_hours = $vujade->get_total_actual_hours($id);
						//print_r($actual_hours);
						//die;
						// has estimates
						if($c>0)
						{
							//unset($estimated_hours['error']);
							
							$lts=$vujade->get_labor_types();
							unset($lts['error']);
							foreach($lts as $k => $v)
							{
								$nk=$v['type'];
								$fha[$nk]=0;
							}

							foreach($estimated_hours as $eh)
							{
								if(!empty($eh['time_type']))
								{
									$t = $eh['time_type'];
									$h = $eh['time_hours'];
									$fha[$t]+=$h;
								}
							}

							$etotal=0;
							$atotal=0;

							foreach($fha as $k => $v)
							{
								$ahline = $actual_hours[$k];
								$ahsum = $ahline['sum_st']+$ahline['sum_ot']+$ahline['sum_dt'];
								print '<tr>';
								print '<td>'.$k;
								print '</td>';
								print '<td>'.$v;
								print '</td>';
								print '<td class = "actual-hours">'.$ahsum;
								print '</td>';
								print '</tr>';

								$etotal+=$v;
								$atotal+=$ahsum;
							}

							// total hours
							print '<tr>';
							print '<td><strong>TOTAL</strong>';
							print '</td>';
							print '<td><strong>'.$etotal;
							print '</strong></td>';
							print '<td><strong>'.$atotal;
							print '</strong></td>';
							print '</tr>';
						}
						else
						{
							// does not have estimates; only show estimated hours
							$actual_hours = $vujade->get_total_actual_hours($id);
							unset($actual_hours['error']);

							$lts=$vujade->get_labor_types();
							unset($lts['error']);

							$etotal=0;
							$atotal=0;

							foreach($lts as $v)
							{
								print '<tr>';
								print '<td>'.$v['type'];
								print '</td>';
								print '<td>0';
								print '</td>';

								print '<td class = "actual-hours">';
								foreach($actual_hours as $ah)
								{
									$line_total=0;
									if($ah['type_id']==$v['database_id'])
									{
										$atotal+=$ah['sum_st']+$ah['sum_ot']+$ah['sum_dt'];
										$line_total+=$ah['sum_st']+$ah['sum_ot']+$ah['sum_dt'];
										if($line_total=='0')
										{
											print '0';
										}
										else
										{
											print $line_total;
										}
										$line_total=0;
									}
								}
								print '</td>';
								print '</tr>';
							}

							// total hours
							print '<tr>';
							print '<td><strong>TOTAL</strong>';
							print '</td>';
							print '<td><strong>0';
							print '</strong></td>';
							print '<td><strong>'.$atotal;
							print '</strong></td>';
							print '</tr>';

						}
						?>

						<tr><td colspan = "3" style = "background-color:#cecece;">&nbsp;</td></tr>

						<tr>
							<td><strong>Inventory and Outsource</strong></td>
							<td><strong>Estimated</strong></td>
							<td><strong>Actual</strong></td>
						</tr>

						<?php
						// get estimated and actual labor inventory and outsource
						$estimated_inventory = $vujade->get_estimated_inventory_and_buyouts($estimate_ids);
						if($estimated_inventory['error']==0)
						{
							unset($estimated_inventory['error']);
							//print_r($estimated_inventory);
							//die;
							/*
							Array ( 
							[0] => 1.25 
							[1] => 13.25 
							[2] => 3.1 
							[3] => 1.25 
							[4] => 13.25 
							[5] => 3.1 )
							*/
							$sum1 = array_sum($estimated_inventory);

							// actual inventory cost
							// actual is from the costing overview page
							
							// outsource
							$cpos = $vujade->get_costing_purchase_orders($id);
							if($cpos['error']=="0")
							{
								$subtotal = 0;
								$total_dp = 0;
								$total_materials = 0;
								$total_outsource = 0;
								$total_subcontract = 0;
								unset($cpos['error']);
								foreach($cpos as $cpo)
								{
									$subtotal+=$cpo['cost'];
									if($cpo['type']=="Materials")
									{
										$total_materials+=$cpo['cost'];
										$total_dp+=$cpo['cost'];
									}
									if($cpo['type']=="Outsource")
									{
										$total_outsource+=$cpo['cost'];
									}
									if($cpo['type']=="Subcontract")
									{
										$total_subcontract+=$cpo['cost'];
									}
								}
							}

							// total materials
							$inventory_cost = 0;
							$items = $vujade->get_materials_for_costing($id);
							if($items['error']=="0")
							{
								unset($items['error']);
								foreach($items as $i)
								{
									$line = $i['cost']*$i['qty'];
									$inventory_cost += $line;
								}
								$indt = $inventory_cost * $project['indeterminant'];
							}
							$tm = $total_dp+$inventory_cost+$indt;

							$sum2 = $tm + $total_subcontract + $total_outsource;

							print '<tr>';
							print '<td style = "background-color:#cecece;">&nbsp;';
							print '</td>';
							print '<td>$'.@number_format($sum1,2,'.',',');
							print '</td>';
							print '<td>$'.@number_format($sum2,2,'.',',');
							print '</td>';
							print '</tr>';
						}
						?>

					</table>
				  </div>
				</div>

            </div>
        </div>
      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    /* fixes indent on textareas
    var n = $('#notes').html();
    $("#notes").html($.trim(n));
   	*/
   	$('.actual-hours').each(function()
   	{
   		var v = $(this).html();
   		//alert(v);
   		//return false;
   		if(v=='')
   		{
   			$(this).html('0');
   		}
   	});
		
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
