<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$customer_permissions = $vujade->get_permission($_SESSION['user_id'],'Clients');
if($customer_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($customer_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup=$vujade->get_setup();

$id = $_REQUEST['project_id'];

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
if(isset($_REQUEST['pe']))
{
	$existing = 1;
}
else
{
	$existing=0;
}

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
# create new customer
if($action==1)
{
	$company=trim($_POST['company']); // company name
	if(empty($company))
	{
		$vujade->errors[]='Customer name cannot be empty';
	}
	else
	{

		// customer name must be unique
		// test to see if name is unique
		$test = $vujade->get_customer_by_name($company,0,1);
		//print_r($test);
		
		// customer exists
		if($test['count']==1)
		{
			$test['error']=1;
			//print $test['count'].'<br>';
			//print $test['error'];
		}
		else
		{
			$test['error']=0;
		}
		//die;

		if($test['error']=="0")
		{
			$address_1=$_POST['b_address_1'];
			$address_2=$_POST['b_address_2'];
			$city=$_POST['b_city'];
			$state=$_POST['b_state'];
			$zip=$_POST['b_zip'];
			$one_time_only = $_POST['one_time_only'];

			$vujade->create_row('quickbooks_customer');
			$client_id = $vujade->row_id;

			$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'ListID',$fakeid);
			
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'Name',$company);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'FullName',$company);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_Addr1',$company);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_Addr2',$address_1);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_Addr3',$address_2);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_City',$city);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_State',$state);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'BillAddress_PostalCode',$zip);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'is_one_time_only',$one_time_only);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'IsActive','true');
			
			// tracking of who entered new customer
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'entered_by',$_SESSION['user_id']);
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'time_entered',strtotime('now'));
			$s[] = $vujade->update_row('quickbooks_customer',$client_id,'page_entered_on',$_SERVER['PHP_SELF']);

			// add to quickbooks
			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}

			// insert into the quickbooks vendor table
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_ADD_CUSTOMER, $vujade->row_id);
			
			$vujade->messages[]="Customer Updated";
			$customer=$vujade->get_customer($client_id,'ID');
			
			// contact
			$nc_fname=$_POST['nc_fname'];
			$nc_lname=$_POST['nc_lname'];
			$nc_phone=$_POST['nc_phone'];
			$nc_email=$_POST['nc_email'];

			# create a new contact record
			$vujade->create_row('customer_contact');
			$rowid = $vujade->row_id;
			$s[]=$vujade->update_row('customer_contact',$rowid,'customer_id',$client_id);
			$s[]=$vujade->update_row('customer_contact',$rowid,'first_name',$nc_fname);
			$s[]=$vujade->update_row('customer_contact',$rowid,'last_name',$nc_lname);
			$s[]=$vujade->update_row('customer_contact',$rowid,'phone1',$nc_phone);
			$s[]=$vujade->update_row('customer_contact',$rowid,'email1',$nc_email);

			# set the client contact id 
			$s[]=$vujade->update_row('projects',$project['database_id'],'ccid',$rowid);
			$s[]=$vujade->update_row('projects',$project['database_id'],'client_id',$client_id);

			if($existing==1)
			{
				$vujade->page_redirect('edit_project_scope.php?id='.$id.'&cid='.$client_id.'&ccid='.$rowid);
			}
			else
			{
				$vujade->page_redirect('new_project_scope.php?id='.$id.'&cid='.$client_id.'&ccid='.$rowid);
			}

		}
		else
		{
			$vujade->errors[]="Customer name already exists for a different customer.";
		}
	}
}

if($action==2)
{
	if($existing==1)
	{
		$vujade->page_redirect('edit_project_customer.php?id='.$id);
	}
	else
	{
		$vujade->page_redirect('new_project_customer.php?id='.$id);
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=2;
$title = "New Project - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                <div class = "row">

                  <div class = "col-md-6" style = "margin-bottom:15px;">
                    <span class="label label-default">Step 1: Project Setup</span> <span class="label label-primary">Step 2: Customer Selection</span> <span class="label label-default">Step 3: Scope of Work</span> 
                  </div>

                  <div class = "col-md-6">
                    	<div class="checkbox-custom" style = "">
	                    	<input type = "checkbox" name = "copy_address" id = "copy_address" value = "" style = "" class = "">
	                    	<label for="copy_address"> Same information</label>
	                	</div>
                  </div>
                </div>

                  <form method="post" action="new_customer.php" id="form">
 
                    <div class="row">

                      <!-- left side -->
                      <div class="col-md-6">

                        <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "company" id = "company" value = "" placeholder="Customer Name">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "b_address_1" id = "b_address_1" value = "" placeholder="Billing Address Line 1">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "b_address_2" id = "b_address_2" value = "" placeholder="Billing Address Line 2">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "b_city" id = "b_city" value = "" placeholder="Billing City">
					      </label>
					    </div>

						<div class="section">
					      <label class="field select">
					        <select id="b_state" name="b_state">
							  <?php
							  if($setup['country']=="USA")
				              {
				              ?>
					              <option value="">-US States-</option>
					              <option value="AL">Alabama</option>
					              <option value="AK">Alaska</option>
					              <option value="AZ">Arizona</option>
					              <option value="AR">Arkansas</option>
					              <option value="CA">California</option>
					              <option value="CO">Colorado</option>
					              <option value="CT">Connecticut</option>
					              <option value="DE">Delaware</option>
					              <option value="DC">District Of Columbia</option>
					              <option value="FL">Florida</option>
					              <option value="GA">Georgia</option>
					              <option value="HI">Hawaii</option>
					              <option value="ID">Idaho</option>
					              <option value="IL">Illinois</option>
					              <option value="IN">Indiana</option>
					              <option value="IA">Iowa</option>
					              <option value="KS">Kansas</option>
					              <option value="KY">Kentucky</option>
					              <option value="LA">Louisiana</option>
					              <option value="ME">Maine</option>
					              <option value="MD">Maryland</option>
					              <option value="MA">Massachusetts</option>
					              <option value="MI">Michigan</option>
					              <option value="MN">Minnesota</option>
					              <option value="MS">Mississippi</option>
					              <option value="MO">Missouri</option>
					              <option value="MT">Montana</option>
					              <option value="NE">Nebraska</option>
					              <option value="NV">Nevada</option>
					              <option value="NH">New Hampshire</option>
					              <option value="NJ">New Jersey</option>
					              <option value="NM">New Mexico</option>
					              <option value="NY">New York</option>
					              <option value="NC">North Carolina</option>
					              <option value="ND">North Dakota</option>
					              <option value="OH">Ohio</option>
					              <option value="OK">Oklahoma</option>
					              <option value="OR">Oregon</option>
					              <option value="PA">Pennsylvania</option>
					              <option value="RI">Rhode Island</option>
					              <option value="SC">South Carolina</option>
					              <option value="SD">South Dakota</option>
					              <option value="TN">Tennessee</option>
					              <option value="TX">Texas</option>
					              <option value="UT">Utah</option>
					              <option value="VT">Vermont</option>
					              <option value="VA">Virginia</option>
					              <option value="WA">Washington</option>
					              <option value="WV">West Virginia</option>
					              <option value="WI">Wisconsin</option>
					              <option value="WY">Wyoming</option>
					              <option value="">---------------</option>
					              <option value="">-Canadian Provinces-</option>
					              <option value="Alberta">Alberta</option>
					              <option value="British Columbia">British Columbia</option>
					              <option value="Manitoba">Manitoba</option>
					              <option value="New Brunswick">New Brunswick</option>
					              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
					              <option value="Northwest Territories">Northwest Territories</option>
					              <option value="Nova Scotia">Nova Scotia</option>
					              <option value="Nunavut">Nunavut</option>
					              <option value="Ontario">Ontario</option>
					              <option value="Prince Edward Island">Prince Edward Island</option>
					              <option value="Quebec">Quebec</option>
					              <option value="Saskatchewan">Saskatchewan</option>
					              <option value="Yukon">Yukon</option>
					        <?php 
					    	} 
					    	// canadian servers
					    	if($setup['country']=='Canada')
					    	{
					    		?>
					    		  <option value="">-Canadian Provinces-</option>
					              <option value="Alberta">Alberta</option>
					              <option value="British Columbia">British Columbia</option>
					              <option value="Manitoba">Manitoba</option>
					              <option value="New Brunswick">New Brunswick</option>
					              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
					              <option value="Northwest Territories">Northwest Territories</option>
					              <option value="Nova Scotia">Nova Scotia</option>
					              <option value="Nunavut">Nunavut</option>
					              <option value="Ontario">Ontario</option>
					              <option value="Prince Edward Island">Prince Edward Island</option>
					              <option value="Quebec">Quebec</option>
					              <option value="Saskatchewan">Saskatchewan</option>
					              <option value="Yukon">Yukon</option>
					              <option value="">---------------</option>
					              <option value="">-US States-</option>
					              <option value="AL">Alabama</option>
					              <option value="AK">Alaska</option>
					              <option value="AZ">Arizona</option>
					              <option value="AR">Arkansas</option>
					              <option value="CA">California</option>
					              <option value="CO">Colorado</option>
					              <option value="CT">Connecticut</option>
					              <option value="DE">Delaware</option>
					              <option value="DC">District Of Columbia</option>
					              <option value="FL">Florida</option>
					              <option value="GA">Georgia</option>
					              <option value="HI">Hawaii</option>
					              <option value="ID">Idaho</option>
					              <option value="IL">Illinois</option>
					              <option value="IN">Indiana</option>
					              <option value="IA">Iowa</option>
					              <option value="KS">Kansas</option>
					              <option value="KY">Kentucky</option>
					              <option value="LA">Louisiana</option>
					              <option value="ME">Maine</option>
					              <option value="MD">Maryland</option>
					              <option value="MA">Massachusetts</option>
					              <option value="MI">Michigan</option>
					              <option value="MN">Minnesota</option>
					              <option value="MS">Mississippi</option>
					              <option value="MO">Missouri</option>
					              <option value="MT">Montana</option>
					              <option value="NE">Nebraska</option>
					              <option value="NV">Nevada</option>
					              <option value="NH">New Hampshire</option>
					              <option value="NJ">New Jersey</option>
					              <option value="NM">New Mexico</option>
					              <option value="NY">New York</option>
					              <option value="NC">North Carolina</option>
					              <option value="ND">North Dakota</option>
					              <option value="OH">Ohio</option>
					              <option value="OK">Oklahoma</option>
					              <option value="OR">Oregon</option>
					              <option value="PA">Pennsylvania</option>
					              <option value="RI">Rhode Island</option>
					              <option value="SC">South Carolina</option>
					              <option value="SD">South Dakota</option>
					              <option value="TN">Tennessee</option>
					              <option value="TX">Texas</option>
					              <option value="UT">Utah</option>
					              <option value="VT">Vermont</option>
					              <option value="VA">Virginia</option>
					              <option value="WA">Washington</option>
					              <option value="WV">West Virginia</option>
					              <option value="WI">Wisconsin</option>
					              <option value="WY">Wyoming</option>
					    		<?php
					    	}
					    	?>
					        </select>
					        <i class="arrow double"></i>
					        </label>
					    </div>

						<div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "b_zip" id = "b_zip" value = "" placeholder="Billing Zip / Province" maxlength="10">
					      </label>
					    </div>
	
						<div class="section">
                          <div class="checkbox-custom mb5">
                            <input type = "checkbox" name = "one_time_only" id = "one_time_only" value = "1">
                            <label for="one_time_only"> One time only client</label>
                          </div>
                        </div>

                      </div>

                      <!-- right side -->
                      <div class="col-md-6">
                        
                        <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_fname" id = "nc_fname" value = "" placeholder="Contact First Name">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_lname" id = "nc_lname" value = "" placeholder="Contact Last Name">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_phone" id = "nc_phone" value = "" placeholder="Contact Phone">
					      </label>
					    </div>

					    <div class="section">
					      <label class="field">
					        <input class="gui-input" type = "text" name = "nc_email" id = "nc_email" value = "" placeholder="Contact Email">
					      </label>
					    </div>  	

                      </div>

                    </div>

                    <input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
					<input type = "hidden" name = "pe" value = "<?php print $existing; ?>">
					<input type = "hidden" name = "action" id = "action" value = "1">

                    <div style = "margin-top:15px;clear:both;">
                      <div style = "float:left">
                        <a href = "#" id = "back" class = "btn btn-lg btn-danger" style = "width:100px;">BACK</a> 
                        <a href = "#" id = "done" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND CONTINUE</a>
                      </div>
                      <div style = "float:right;">
                      </div>  
                    </div>

                  </form>
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    // copy address from project setup
    $('#copy_address').click(function(e)
    {
    	if($(this).is(':checked'))
    	{
    		// get from job and populate fields
    		$.post( "jq.get_project_data.php", { id: '<?php print $id; ?>'})
			  .done(function(data) 
			  {
			  		// parse  
			  		var j = JSON.parse(data);
			  		//alert(j.city);

			  		$('#company').val(j.company);
			  		$('#b_address_1').val(j.address_1);
					$('#b_address_2').val(j.address_2);
					$('#b_city').val(j.city);
					$('#b_state').val(j.state);
					$('#b_zip').val(j.zip);
					$('#nc_fname').val(j.project_contact_fn);
					$('#nc_lname').val(j.project_contact_ln);
					$('#nc_phone').val(j.project_contact_phone);
					$('#nc_email').val(j.project_contact_email);
			  });
    	}
    	else
    	{
    		// clear fields
    		$('#company').val('');
    		$('#b_address_1').val('');
			$('#b_address_2').val('');
			$('#b_city').val('');
			$('#b_state').val('');
			$('#b_zip').val('');
			$('#nc_fname').val('');
			$('#nc_lname').val('');
			$('#nc_phone').val('');
			$('#nc_email').val('');
    	}
    });

    $('#back').click(function(e)
	{
		e.preventDefault();
		$('#action').val(2);
		$('#form').submit();
	});

	$('#done').click(function(e)
	{
		e.preventDefault();
		$('#action').val(1);
		$('#form').submit();
	});
 
});
</script>

</body>

</html>