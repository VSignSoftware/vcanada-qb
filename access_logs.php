<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// user must be admin to access this page
if(!isset($_SESSION['admin_id']))
{
	$vujade->page_redirect('logout.php');
}
if($_SESSION['is_admin']!=1)
{
	$vujade->page_redirect('logout.php');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
// 
if($action==1)
{
	
}
$section=0;
$title = "Access Logs - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
<ol class="breadcrumb">
<li class="crumb-active">
<a href = "#">Access Logs</a>
</li>
</ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="admin-form theme-primary">
<?php
$result = $vujade->delete_log();
print_r($result);
print '<hr>';
$result2 = $vujade->delete_search_log();
print_r($result2);
//if($result['success']==1)
//{
//	print 'Activity Log deleted.';
//}
?>
</div>

</section>
</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

   	 
});
</script>

</body>

</html>