<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
print '<meta charset="ISO-8859-1">';
$poid = $_REQUEST['poid'];
$po = $vujade->get_purchase_order($poid);
$item_id = $_REQUEST['item_id'];
$project_id=$_REQUEST['project_id'];
$project = $vujade->get_project($project_id,2);
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
$s = array();
# add a new item to this purchase order
if($action==1)
{
	# get item details
	$item = $vujade->get_item($item_id);
	if($item['error']=="0")
	{
		$vujade->create_row('purchase_order_items');
		$row_id = $vujade->row_id;
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'purchase_order_id',$poid);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'inventory_id',$item['inventory_id']);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'unit_price',$item['cost']);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'description',$item['description']);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'qty',1);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'qty_recvd',1);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'total',$item['cost']);
		$s[]=$vujade->update_row('purchase_order_items',$row_id,'account',$account);
	}
	$action = 0;
}
# default
# get the items that have been added to this estimate previously
if($action==0)
{
	?>
	<table class = "table">
		<tr>
			<td class = "bordered size100">Item #</td>
			<td class = "bordered size200">Description</td>
			<td class = "bordered size50">Account</td>
			<td class = "bordered size50">Job No.</td>
			<td class = "bordered size50">Qty</td>
			<td class = "bordered size50">Unit Price</td>
			<td class = "bordered size50">Total</td>
			<td class = "bordered size100">Job Name</td>
			<td class = "bordered size50">Qty Rcvd</td>
			<td class = "bordered size100">&nbsp;</td>
		</tr>

	<?php
	$materials_cost = 0;
	$items = $vujade->get_materials_for_purchase_order($poid);
	if($items['error']=="0")
	{
		unset($items['error']);
		foreach($items as $i)
		{
			?>
			<form method = "post" action = "new_purchase_order_materials.php">
			<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
			<input type = "hidden" name = "poid" value = "<?php print $poid; ?>">
			<input type = "hidden" name = "action" value = "1">
			<input type = "hidden" name = "item_database_id" value = "<?php print $i['database_id']; ?>">
			<?php
			print '<tr>';

			print '<td class = "bordered size100">';
			print $i['inventory_id'];
			print '</td>';
			
			print '<td class = "bordered size200">';
			print $i['description'];
			print '</td>';
			
			print '<td class = "bordered size100">';
			print '<select name = "account" class = "form-control">';
			if(!empty($i['account']))
			{
				print '<option value = "'.$i['account'].'" selected = "selected">'.$i['account'].'</option>';
			}
			print '<option value = "">-Select-</option>';
			print '<option value = "1180 Inventory">1180 Inventory</option>';
			print '<option value = "4270 Small Tools">4270 Small Tools</option>';
			print '<option value = "4280 Shop Expenses">4280 Shop Expenses</option>';
			print '<option value = "4300 Shop Supplies">4300 Shop Supplies</option>';
			print '<option value = "4350 Safety">4350 Safety</option>';
			print '</select>';
			print '</td>';

			# job number
			print '<td class = "bordered size100">';
			print $project['project_id'];
			print '</td>';

			# qty
			print '<td class = "bordered size100">';
			print $i['qty'];
			print '</td>';

			# unit price
			print '<td class = "bordered size100">';
			print '$'. number_format($i['unit_price'],2,'.',',');
			print '</td>';
			
			# total
			print '<td class = "bordered size100">';
			$line = $i['unit_price']*$i['qty'];
			print '$'. number_format($line,2,'.',',');
			print '</td>';

			# job name
			print '<td class = "bordered size100">';
			print $project['site'];
			print '</td>';

			# qty recvd
			print '<td class = "bordered size100">';
			print $i['qty_recvd'];
			print '</td>';

			print '<td class = "bordered size100">';
			
			// update button
			print '<input type = "submit" value = "Update" class = "btn btn-primary btn-sm"> ';
			print '</form>';

			// delete button
			print '<form method = "post" action = "new_purchase_order_materials.php">';
			print '<input type = "hidden" name = "" value = "">';
			print '<input type = "submit" value = "Delete" class = "btn btn-danger btn-sm"> ';
			print '</form>';
			print '</td>';
			print '</tr>';

			$materials_cost = $materials_cost + $line;
		}
	}
	else
	{
		print '<tr>
		<td colspan = "10" class = "bordered size100" style = "width:100%;text-align:center;"><em>Use the form below to add materials to this purchase order.</em></td></tr>';	
	}
	print '</table>';
}
?>

<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$('.plus-delete').click(function(e)
	{
		e.preventDefault();
		var loading = '<img src = "images/loading.gif" width = "100" height = "100" style = "margin-left:100px;margin-top:100px;">';
		$('#e_items').html('');
		$('#e_items').html(loading);
		$('#tfc').html('');
		var estimate_id = "<?php print $estimate_id; ?>";
		var db_id = this.id;
		$.post( "jq.estimate_items.php", { action: 3, estimate_id: estimate_id, item_id: 0, db_id: db_id })
		  .done(function( data ) 
		  {
		  		$('#e_items').html(data);
		  });
	});

	// update total button
	$('.plus-update').click(function()
	{
		// this gets each item quantity
		var qtys = $('.quantity').map(function(index) 
		{
		    return this.value; 
		});
		
		// this gets each item id
		var ids = $('.plus-delete').map(function(index) 
		{
		    return this.id; 
		});

		// this combines both into an array with the id as the key, quantity as the value
		var array3 = {};
		$.each(ids, function(i) {
		    array3[ids[i]] = qtys[i];
		});

		// this loops through all then creates hidden form fields containing only the id and the qty
		$.each(array3, function( index, value ) 
		{
			//alert( index + ": " + value );
			//concat = concat+
			$('<input>').attr({
			    type: 'hidden',
			    id: 'items',
			    name: 'items[]',
			    value: index+"^"+value
			}).appendTo('#nd');
		});

		$('#action').val('3');
		$('#nd').submit();
	});

});	
</script>