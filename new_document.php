<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$documents_permissions = $vujade->get_permission($_SESSION['user_id'],'Documents');
if($documents_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['project_id'];
$project_id = $id;
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$folder_id=$_REQUEST['folder_id'];
$type_id=$_REQUEST['type_id'];
$typename = $_REQUEST['typename'];

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# 
if($action==1)
{
}

// existing documents
$docs = $vujade->get_documents($id);

$nav = 3;
$title = "Documents - ";
require_once('h.php');
?>
<section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Project #: <?php print $project_id; ?> Project Name: <?php print $project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->
      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">
                  <strong>Upload documents</strong>
                 
                  <div style = "padding:5px;width:100%;">
					<link rel="stylesheet" href="vendor/plugins/dropzone/css/dropzone.css">	
					<script src="vendor/plugins/dropzone/dropzone.min.js"></script>	
					<div id = "dropzone_div" style = "width:100%;margin-bottom:15px;height:375px;overflow:auto;">
						<form action="upload.php?type=3&project_id=<?php print $project_id; ?>&folder_id=<?php print $folder_id; ?>&type_id=<?php print $type_id; ?>" id="dropzone-area" class="dropzone">
							<div class="dz-message" style="width:100%;margin: 40px auto;text-align: center;">
	                            <h4 class="hidden-sm hidden-xs">Drag Documents to Upload</h4>
	                            <span class="hidden-sm hidden-xs">Or click to browse</span>
	                            <h4 class="hidden-md hidden-lg">Tap to add documents</h4>
	                        </div>
                        </form>

					</div>
					</div>
                    <div style = "margin-top:15px;">
                      <div style = "float:left">
                        &nbsp;
                      </div>
                      <div style = "float:right;">
                        <a href = "project_documents.php?id=<?php print $project_id; ?>" id = "done" class = "btn btn-lg btn-primary" style = "width:100px;">DONE</a>
                      </div>  
                    </div>
                </div>
              </div>
            </div>
	    </section>
	</section>
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
Dropzone.options.dropzoneArea = 
{ 
  // The configuration we've talked about above
  //autoProcessQueue: false,
  //uploadMultiple: false,
  //parallelUploads: 1,
  //maxFiles: 100,
  // url: "upload.php?project_id=<?php print $id; ?>&type=1&design_id=<?php print $design['design_id']; ?>",
  init: function () 
  {
  		this.on("addedfile", function (file) 
  		{
        	var existing = [];
        	// get existing files and add to array
        	<?php
        	if($docs['error']=="0")
        	{
        		unset($docs['error']);
        		foreach($docs as $df)
        		{
        			?>
        			existing.push("<?php print $df['file_name']; ?>");
        			<?php
        		}
        	}
        	?>
        	var in_array = existing.indexOf(file.name);
    		if(in_array>=0)
    		{
    			if(confirm("A file with the same name " + file.name + " already exists for this design. Do you want to overwrite it?"))
    			{
    				this.processQueue();
                	//alert('1');
                	// user confirmed; do nothing further; upload.php handles overwriting
	            }
	            else
	            {
	            	this.removeFile(file);
                	return false;
	            }
    		}
    		else
    		{
    			//alert('not in array: '+file.name);
    			this.processQueue();
    		}
        });
    }
}

jQuery(document).ready(function() 
{
    "use strict";
    // Init Theme Core    
    Core.init();
    
});
</script>
</body>
</html>