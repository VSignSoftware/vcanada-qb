<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	
	unset($_POST['action']);
	$white = $_POST['1'];
	$yellow = $_POST['2'];
	$blue1 = $_POST['3'];
	$green = $_POST['4'];
	$blue2 = $_POST['5'];
	$red = $_POST['6'];

	$s[]=$vujade->update_row('calendar_legend',1,'explanation',$white);
	$s[]=$vujade->update_row('calendar_legend',2,'explanation',$yellow);
	$s[]=$vujade->update_row('calendar_legend',3,'explanation',$blue1);
	$s[]=$vujade->update_row('calendar_legend',4,'explanation',$green);
	$s[]=$vujade->update_row('calendar_legend',5,'explanation',$blue2);
	$s[]=$vujade->update_row('calendar_legend',6,'explanation',$red);

	$vujade->messages[]="Color Legend Updated.";
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Calendar Legend - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=9;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<form method = "post" action = "calendar_legend.php">
					<?php
					$data = $vujade->get_legend_data();
					if($data['error']=="0")
					{
						unset($data['error']);
						foreach($data as $legend)
						{
							print $legend['color'].'<div style = "float:left;margin-right:5px;width:30px;height:15px;border:1px solid #eeeeee;background-color:'.$legend['hex'].';">&nbsp;</div><br>';
							print '<input type = "text" name = "'.$legend['id'].'" value = "'.$legend['explanation'].'" style = "width:400px;"><br><br>';
						}
					}
					else
					{
						// create six rows
						//$q = "insert into calendar_legend (id,) values ('1','2','3','4','5','6')";
						//$vujade->generice_query($q);
					}
					?>
					<input type = "hidden" name = "action" value = "1">
					<input type = "submit" value = "SAVE" class = "btn btn-primary">
				</form>
				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
