<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if(isset($_REQUEST['tab']))
{
	$tab = $_REQUEST['tab'];
}
else
{
	$tab = 0;
}
$deleted=0;
$employee = $vujade->get_employee($id);
if($employee['is_admin']==1)
{
	$vujade->page_redirect('error.php?m=5');
}
$current_rate = $vujade->get_employee_current_rate($id);
$s = array();

# update username
if($action==1)
{
	$u = $_POST['u'];
	$vujade->check_empty($u,'Username');
	$unique = $vujade->check_username_is_unique($u);
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		# they are reviewing their own record
		if($_SESSION['user_id']==$id)
		{
			unset($_SESSION['username']);
			$_SESSION['username']=$u;
		}
		
		#update
		$success = $vujade->update_row('employees',$id,'username',$u);
		if($success==1)
		{
			$vujade->set_message('Username updated');
		}
	}
}

# update password
if($action==2)
{
	$p = $_POST['p'];
	$vujade->check_empty($p,'Password');
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		# encrypt
		$p = crypt($p);

		#update
		$s1 = $vujade->update_row('employees',$id,'password',$p);
		$s2 = $vujade->update_row('employees',$id,'pw_changed',1);
		$s3 = $vujade->update_row('employees',$id,'pw_encrypted',1);

		# clear password change first time flag
		unset($_SESSION['pw_changed']);
		$_SESSION['pw_changed']=1;
		$success = $s1+$s2+$s3;
		if($success==3)
		{
			$vujade->set_message('Password updated');
		}
	}
}

# update email settings
if($action==3)
{
	$reply_to = $_POST['reply_to'];
	$sig = $_POST['sig'];
	$send_tasks_to_email=$_POST['send_tasks_to_email'];
	$bcc=$_POST['bcc'];

	// validation
	$vujade->check_empty($reply_to,'Reply To');
	$vujade->check_empty($sig,'Signature');

	// optional / only for sales people
	$status_change_task=$_POST['status_change_task'];
	$status_change_email=$send_tasks_to_email;

	$e = $vujade->get_error_count();
	if($e<=0)
	{
		$s = array();

		#update
		$s[] = $vujade->update_row('employees',$id,'email_reply_to',$reply_to);
		$s[] = $vujade->update_row('employees',$id,'email_signature',$sig);
		$s[] = $vujade->update_row('employees',$id,'send_tasks_to_email',$send_tasks_to_email);
		$s[] = $vujade->update_row('employees',$id,'status_change_task',$status_change_task);
		$s[] = $vujade->update_row('employees',$id,'status_change_email',$status_change_email);
		$s[] = $vujade->update_row('employees',$id,'bcc',$bcc);
		$vujade->set_message('Settings changed.');
		$tab=1;
	}
}

# update the profile
if($action==4)
{
	$website_user = $_POST['website_user'];
	$first_name=$_POST['first_name'];
	$success = $vujade->update_row('employees',$id,'first_name',$first_name);
	$middle=$_POST['middle'];
	$success = $vujade->update_row('employees',$id,'middle',$middle);
	$last_name=$_POST['last_name'];
	$success = $vujade->update_row('employees',$id,'last_name',$last_name);
	$dob=$_POST['dob'];
	$success = $vujade->update_row('employees',$id,'dob',$dob);
	$ssn=$_POST['ssn'];
	$success = $vujade->update_row('employees',$id,'ssn',$ssn);
	$gender=$_POST['gender'];
	$success = $vujade->update_row('employees',$id,'gender',$gender);
	$street_1=$_POST['street_1'];
	$success = $vujade->update_row('employees',$id,'street_1',$street_1);
	$city=$_POST['city'];
	$success = $vujade->update_row('employees',$id,'city',$city);
	$state=$_POST['state'];
	$success = $vujade->update_row('employees',$id,'state',$state);
	$zip=$_POST['zip'];
	$success = $vujade->update_row('employees',$id,'zip',$zip);
	$home_phone=$_POST['home_phone'];
	$success = $vujade->update_row('employees',$id,'home_phone',$home_phone);
	$cell_phone=$_POST['cell_phone'];
	$success = $vujade->update_row('employees',$id,'cell_phone',$cell_phone);
	$email=$_POST['email'];
	$success = $vujade->update_row('employees',$id,'email',$email);
	$dl_number=$_POST['dl_number'];
	$success = $vujade->update_row('employees',$id,'dl_number',$dl_number);
	$dl_expiration=$_POST['dl_expiration'];
	$success = $vujade->update_row('employees',$id,'dl_expiration',$dl_expiration);
	$start_date=$_POST['start_date'];
	$success = $vujade->update_row('employees',$id,'start_date',$start_date);
	$area=$_POST['area'];
	$success = $vujade->update_row('employees',$id,'area',$area);
	$department=$_POST['department'];
	$success = $vujade->update_row('employees',$id,'department',$department);
	$print_time_card=$_POST['print_time_card'];
	$success = $vujade->update_row('employees',$id,'print_time_card',$print_time_card);
	$termination_date=$_POST['termination_date'];
	$success = $vujade->update_row('employees',$id,'termination_date',$termination_date);
	$cobra_notice=$_POST['cobra_notice'];
	$success = $vujade->update_row('employees',$id,'cobra_notice',$cobra_notice);
	$fulltime = $_POST['fulltime'];
	$success = $vujade->update_row('employees',$id,'fulltime',$fulltime);
	$is_salary = $_POST['is_salary'];
	$success = $vujade->update_row('employees',$id,'is_salary',$is_salary);
	$po_notify = $_POST['po_notify'];
	$success = $vujade->update_row('employees',$id,'po_notify',$po_notify);
	$success = $vujade->update_row('employees',$id,'website_user',$website_user);

	# rate
	$rate = $_POST['rate'];
	if($current_rate['error']=="0")
	{
		$success = $vujade->update_row('employee_rates',$current_rate['database_id'],'rate',$rate);
		$current_rate = $vujade->get_employee_current_rate($id);
	}
	else
	{
		$vujade->create_row('employee_rates');
		$row_id = $vujade->row_id;
		$success = $vujade->update_row('employee_rates',$row_id,'employee_id',$id);
		$success = $vujade->update_row('employee_rates',$row_id,'rate',$rate);
		$current_rate = $vujade->get_employee_current_rate($id);
	}

	/* employee id (not same as database id )*/
	$employee_id=$_POST['employee_id'];
	if( (!empty($employee_id)) && ($employee_id!=0) && (ctype_digit($employee_id)))
	{
		if($employee_id!=$employee['employee_id'])
		{
			$testemployee = $vujade->get_employee($employee_id,2);
			if($testemployee['error']=="0")
			{
				# it is not unique
				$vujade->errors[]="Employee ID already exists. Please choose a different ID.";
			}
			else
			{
				$success = $vujade->update_row('employees',$id,'employee_id',$employee_id);
			}
		}
	}
	else
	{
		$vujade->errors[]="Employee ID is invalid.";
	}
	
	$access = $_POST['access'];
	#update
	if($access==0)
	{
		$success = $vujade->update_row('employees',$id,'access',0);
		$vujade->set_message('Access updated');
	}
	if($access==1)
	{
		$success = $vujade->update_row('employees',$id,'access',1);
		$vujade->set_message('Access updated');
	}

	$e = $vujade->get_error_count();
	if($e<=0)
	{
		$vujade->set_message('Profile Updated');
	}
	$tab = 2;
}

# delete employee
if($action==5)
{
	$deleted = $vujade->delete_row('employees',$id);
	$employee['error']=1;
	$vujade->page_redirect('employees.php?m=1');
}

# save the notes
// deprecated; notes are handled differently (5-20-2015)
if($action==6)
{
	die; 
	$notes = $_POST['notes'];
	$s[]=$vujade->update_row('employees',$id,'notes',$notes);
	$vujade->set_message('Notes saved');
	$tab = 2;
}

# update the permissions
if($action==7)
{
	
	# accounting permissions
	$accounting_read=$_POST['accounting_read'];
	$accounting_edit=$_POST['accounting_edit'];
	//$accounting_create=$_POST['accounting_create'];
	$accounting_delete=$_POST['accounting_delete'];

	# find the row id
	$accounting_p = $vujade->get_permission($id,'Accounting');

	# row id does not exist; create new
	if($accounting_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$accounting_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$accounting_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$accounting_p['database_id'],'section','Accounting');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$accounting_p['database_id'],'permission_read',$accounting_read);
	$s[] = $vujade->update_row('employee_permissions',$accounting_p['database_id'],'permission_edit',$accounting_edit);
	$s[] = $vujade->update_row('employee_permissions',$accounting_p['database_id'],'permission_create',$accounting_edit);
	$s[] = $vujade->update_row('employee_permissions',$accounting_p['database_id'],'permission_delete',$accounting_delete);

	# client section permissions
	$clients_read=$_POST['clients_read'];
	$clients_edit=$_POST['clients_edit'];
	//$clients_create=$_POST['clients_create'];
	$clients_delete=$_POST['clients_delete'];

	# find the row id
	$clients_p = $vujade->get_permission($id,'Clients');

	# row id does not exist; create new
	if($clients_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$clients_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$clients_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$clients_p['database_id'],'section','Clients');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$clients_p['database_id'],'permission_read',$clients_read);
	$s[] = $vujade->update_row('employee_permissions',$clients_p['database_id'],'permission_edit',$clients_edit);
	$s[] = $vujade->update_row('employee_permissions',$clients_p['database_id'],'permission_create',$clients_edit);
	$s[] = $vujade->update_row('employee_permissions',$clients_p['database_id'],'permission_delete',$clients_delete);

	# design section permissions
	$designs_read=$_POST['designs_read'];
	$designs_edit=$_POST['designs_edit'];
	//$designs_create=$_POST['designs_create'];
	$designs_delete=$_POST['designs_delete'];

	# find the row id
	$designs_p = $vujade->get_permission($id,'Designs');

	# row id does not exist; create new
	if($designs_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$designs_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$designs_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$designs_p['database_id'],'section','Designs');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$designs_p['database_id'],'permission_read',$designs_read);
	$s[] = $vujade->update_row('employee_permissions',$designs_p['database_id'],'permission_edit',$designs_edit);
	$s[] = $vujade->update_row('employee_permissions',$designs_p['database_id'],'permission_create',$designs_edit);
	$s[] = $vujade->update_row('employee_permissions',$designs_p['database_id'],'permission_delete',$designs_delete);

	# employee records section permissions
	$er_read=$_POST['er_read'];
	$er_edit=$_POST['er_edit'];
	//$er_create=$_POST['er_create'];
	$er_delete=$_POST['er_delete'];

	# find the row id
	$er_records_p = $vujade->get_permission($id,'Employee Records');

	# row id does not exist; create new
	if($er_records_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$er_records_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$er_records_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$er_records_p['database_id'],'section','Employee Records');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$er_records_p['database_id'],'permission_read',$er_read);
	$s[] = $vujade->update_row('employee_permissions',$er_records_p['database_id'],'permission_edit',$er_edit);
	$s[] = $vujade->update_row('employee_permissions',$er_records_p['database_id'],'permission_create',$er_edit);
	$s[] = $vujade->update_row('employee_permissions',$er_records_p['database_id'],'permission_delete',$er_delete);

	# estimates section permissions
	$estimates_read=$_POST['estimates_read'];
	$estimates_edit=$_POST['estimates_edit'];
	//$estimates_create=$_POST['estimates_create'];
	$estimates_delete=$_POST['estimates_delete'];

	# find the row id
	$estimates_p = $vujade->get_permission($id,'Estimates');

	# row id does not exist; create new
	if($estimates_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$estimates_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$estimates_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$estimates_p['database_id'],'section','Estimates');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$estimates_p['database_id'],'permission_read',$estimates_read);
	$s[] = $vujade->update_row('employee_permissions',$estimates_p['database_id'],'permission_edit',$estimates_edit);
	$s[] = $vujade->update_row('employee_permissions',$estimates_p['database_id'],'permission_create',$estimates_edit);
	$s[] = $vujade->update_row('employee_permissions',$estimates_p['database_id'],'permission_delete',$estimates_delete);

	# inventory permissions
	$inventory_read=$_POST['inventory_read'];
	$inventory_edit=$_POST['inventory_edit'];
	//$inventory_create=$_POST['inventory_create'];
	$inventory_delete=$_POST['inventory_delete'];

	# find the row id
	$inventory_p = $vujade->get_permission($id,'Inventory');

	# row id does not exist; create new
	if($inventory_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$inventory_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$inventory_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$inventory_p['database_id'],'section','Inventory');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$inventory_p['database_id'],'permission_read',$inventory_read);
	$s[] = $vujade->update_row('employee_permissions',$inventory_p['database_id'],'permission_edit',$inventory_edit);
	$s[] = $vujade->update_row('employee_permissions',$inventory_p['database_id'],'permission_create',$inventory_edit);
	$s[] = $vujade->update_row('employee_permissions',$inventory_p['database_id'],'permission_delete',$inventory_delete);

	# invoices permissions
	$invoices_read=$_POST['invoices_read'];
	$invoices_edit=$_POST['invoices_edit'];
	//$invoices_create=$_POST['invoices_create'];
	$invoices_delete=$_POST['invoices_delete'];

	# find the row id
	$invoices_p = $vujade->get_permission($id,'Invoices');

	# row id does not exist; create new
	if($invoices_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$invoices_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$invoices_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$invoices_p['database_id'],'section','Invoices');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$invoices_p['database_id'],'permission_read',$invoices_read);
	$s[] = $vujade->update_row('employee_permissions',$invoices_p['database_id'],'permission_edit',$invoices_edit);
	$s[] = $vujade->update_row('employee_permissions',$invoices_p['database_id'],'permission_create',$invoices_edit);
	$s[] = $vujade->update_row('employee_permissions',$invoices_p['database_id'],'permission_delete',$invoices_delete);

	# projects
	$projects_read=$_POST['projects_read'];
	$projects_edit=$_POST['projects_edit'];
	//$projects_create=$_POST['projects_create'];
	$projects_delete=$_POST['projects_delete'];

	# find the row id
	$projects_p = $vujade->get_permission($id,'Projects');

	# row id does not exist; create new
	if($projects_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$projects_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$projects_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$projects_p['database_id'],'section','Projects');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$projects_p['database_id'],'permission_read',$projects_read);
	$s[] = $vujade->update_row('employee_permissions',$projects_p['database_id'],'permission_edit',$projects_edit);
	$s[] = $vujade->update_row('employee_permissions',$projects_p['database_id'],'permission_create',$projects_edit);
	$s[] = $vujade->update_row('employee_permissions',$projects_p['database_id'],'permission_delete',$projects_delete);

	# proposals
	$proposals_read=$_POST['proposals_read'];
	$proposals_edit=$_POST['proposals_edit'];
	//$proposals_create=$_POST['proposals_create'];
	$proposals_delete=$_POST['proposals_delete'];

	# find the row id
	$proposals_p = $vujade->get_permission($id,'Proposals');

	# row id does not exist; create new
	if($proposals_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$proposals_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$proposals_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$proposals_p['database_id'],'section','Proposals');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$proposals_p['database_id'],'permission_read',$proposals_read);
	$s[] = $vujade->update_row('employee_permissions',$proposals_p['database_id'],'permission_edit',$proposals_edit);
	$s[] = $vujade->update_row('employee_permissions',$proposals_p['database_id'],'permission_create',$proposals_edit);
	$s[] = $vujade->update_row('employee_permissions',$proposals_p['database_id'],'permission_delete',$proposals_delete);

	# purchase orders
	$purchase_orders_read=$_POST['purchase_orders_read'];
	$purchase_orders_edit=$_POST['purchase_orders_edit'];
	//$purchase_orders_create=$_POST['purchase_orders_create'];
	$purchase_orders_delete=$_POST['purchase_orders_delete'];

	# find the row id
	$purchase_orders_p = $vujade->get_permission($id,'Purchase Orders');

	# row id does not exist; create new
	if($purchase_orders_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$purchase_orders_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$purchase_orders_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$purchase_orders_p['database_id'],'section','Purchase Orders');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$purchase_orders_p['database_id'],'permission_read',$purchase_orders_read);
	$s[] = $vujade->update_row('employee_permissions',$purchase_orders_p['database_id'],'permission_edit',$purchase_orders_edit);
	$s[] = $vujade->update_row('employee_permissions',$purchase_orders_p['database_id'],'permission_create',$purchase_orders_edit);
	$s[] = $vujade->update_row('employee_permissions',$purchase_orders_p['database_id'],'permission_delete',$purchase_orders_delete);

	# shop orders
	$shop_orders_read=$_POST['shop_orders_read'];
	$shop_orders_edit=$_POST['shop_orders_edit'];
	//$shop_orders_create=$_POST['shop_orders_create'];
	$shop_orders_delete=$_POST['shop_orders_delete'];

	# find the row id
	$shop_orders_p = $vujade->get_permission($id,'Shop Orders');

	# row id does not exist; create new
	if($shop_orders_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$shop_orders_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$shop_orders_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$shop_orders_p['database_id'],'section','Shop Orders');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$shop_orders_p['database_id'],'permission_read',$shop_orders_read);
	$s[] = $vujade->update_row('employee_permissions',$shop_orders_p['database_id'],'permission_edit',$shop_orders_edit);
	$s[] = $vujade->update_row('employee_permissions',$shop_orders_p['database_id'],'permission_create',$shop_orders_edit);
	$s[] = $vujade->update_row('employee_permissions',$shop_orders_p['database_id'],'permission_delete',$shop_orders_delete);

	# turn in for billing
	$tifb=$_POST['tifb'];

	# find the row id
	$tifb_p = $vujade->get_permission($id,'Turned in for billing');

	# row id does not exist; create new
	if($tifb_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$tifb_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$tifb_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$tifb_p['database_id'],'section','Turned in for billing');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$tifb_p['database_id'],'permission_read',$tifb);
	
	# vendors
	$vendors_read=$_POST['vendors_read'];
	$vendors_edit=$_POST['vendors_edit'];
	//$vendors_create=$_POST['vendors_create'];
	$vendors_delete=$_POST['vendors_delete'];

	# find the row id
	$vendors_p = $vujade->get_permission($id,'Vendors');

	# row id does not exist; create new
	if($vendors_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$vendors_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$vendors_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$vendors_p['database_id'],'section','Vendors');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$vendors_p['database_id'],'permission_read',$vendors_read);
	$s[] = $vujade->update_row('employee_permissions',$vendors_p['database_id'],'permission_edit',$vendors_edit);
	$s[] = $vujade->update_row('employee_permissions',$vendors_p['database_id'],'permission_create',$vendors_edit);
	$s[] = $vujade->update_row('employee_permissions',$vendors_p['database_id'],'permission_delete',$vendors_delete);

	# administrator privilege
	$admin=$_POST['admin'];

	//print $admin.'<br>';

	# find the row id
	$admin_p = $vujade->get_permission($id,'Admin');

	//print_r($admin_p).'<br>';

	# row id does not exist; create new
	if($admin_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$admin_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$admin_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$admin_p['database_id'],'section','Admin');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$admin_p['database_id'],'permission_read',$admin);

	// cost to date
	$ctd=$_POST['ctd'];

	# find the row id
	$ctd_p = $vujade->get_permission($id,'Cost To Date');

	# row id does not exist; create new
	if($ctd_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$ctd_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$ctd_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$ctd_p['database_id'],'section','Cost To Date');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$ctd_p['database_id'],'permission_read',$ctd);

	// design sort
	$design_sort=$_POST['design_sort'];

	# find the row id
	$design_sort_p = $vujade->get_permission($id,'Design Sort');

	# row id does not exist; create new
	if($design_sort_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$design_sort_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$design_sort_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$design_sort_p['database_id'],'section','Design Sort');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$design_sort_p['database_id'],'permission_read',$design_sort);

	// estimate sort
	$estimate_sort=$_POST['estimate_sort'];

	# find the row id
	$estimate_sort_p = $vujade->get_permission($id,'Estimate Sort');

	# row id does not exist; create new
	if($estimate_sort_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$estimate_sort_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$estimate_sort_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$estimate_sort_p['database_id'],'section','Estimate Sort');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$estimate_sort_p['database_id'],'permission_read',$estimate_sort);

	// additional estimate permissions 
	// estimate sort
	$estimate_add=$_POST['estimate_add'];

	# find the row id
	$estimate_add_p = $vujade->get_permission($id,'Estimate Full Access');

	# row id does not exist; create new
	if($estimate_add_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$estimate_add_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$estimate_add_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$estimate_add_p['database_id'],'section','Estimate Full Access');
	}

	# update existing row
	$s[] = $vujade->update_row('employee_permissions',$estimate_add_p['database_id'],'permission_read',$estimate_add);

	# calendar
	$calendar_read=$_POST['calendar_read'];
	$calendar_edit=$_POST['calendar_edit'];
	//$calendar_delete=$_POST['calendar_delete'];
	$calendar_p = $vujade->get_permission($id,'Calendar');
	if($calendar_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$calendar_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$calendar_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$calendar_p['database_id'],'section','Calendar');
	}
	$s[] = $vujade->update_row('employee_permissions',$calendar_p['database_id'],'permission_read',$calendar_read);
	$s[] = $vujade->update_row('employee_permissions',$calendar_p['database_id'],'permission_edit',$calendar_edit);

	# documents
	$documents_read=$_POST['documents_read'];
	$documents_edit=$_POST['documents_edit'];
	$documents_delete=$_POST['documents_delete'];
	$documents_p = $vujade->get_permission($id,'Documents');
	if($documents_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$documents_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$documents_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$documents_p['database_id'],'section','Documents');
	}
	$s[] = $vujade->update_row('employee_permissions',$documents_p['database_id'],'permission_read',$documents_read);
	$s[] = $vujade->update_row('employee_permissions',$documents_p['database_id'],'permission_edit',$documents_edit);
	$s[] = $vujade->update_row('employee_permissions',$documents_p['database_id'],'permission_create',$documents_edit);
	$s[] = $vujade->update_row('employee_permissions',$documents_p['database_id'],'permission_delete',$documents_delete);

	# production files
	$files_read=$_POST['files_read'];
	$files_edit=$_POST['files_edit'];
	$files_delete=$_POST['files_delete'];
	$files_p = $vujade->get_permission($id,'Production Files');
	if($files_p['error']!="0")
	{
		$vujade->create_row('employee_permissions');
		$files_p['database_id']=$vujade->row_id;
		$vujade->row_id="";
		$s[] = $vujade->update_row('employee_permissions',$files_p['database_id'],'employee_id',$id);
		$s[] = $vujade->update_row('employee_permissions',$files_p['database_id'],'section','Production Files');
	}
	$s[] = $vujade->update_row('employee_permissions',$files_p['database_id'],'permission_read',$files_read);
	$s[] = $vujade->update_row('employee_permissions',$files_p['database_id'],'permission_edit',$files_edit);
	$s[] = $vujade->update_row('employee_permissions',$files_p['database_id'],'permission_create',$files_edit);
	$s[] = $vujade->update_row('employee_permissions',$files_p['database_id'],'permission_delete',$files_delete);
	$vujade->set_message('Permissions updated');
	$tab = 4;
}

# delete a document
if($action==8)
{
	# delete the file
	$tab=5;
	$file_id = $_REQUEST['docid'];
	if(ctype_digit($file_id))
	{
		$filedata = $vujade->get_document($file_id);
		@unlink('uploads/employee_documents/'.$id.'/'.$filedata['file_name']);
		$s[] = $vujade->delete_row('documents',$file_id);
	}
}

if($deleted==0)
{
	$employee = $vujade->get_employee($id);
}

# delete note
if($action==9)
{
	$note_id = $_REQUEST['note_id'];
	$deleted_note = $vujade->delete_row('employee_notes',$note_id);
	//$employee['error']=1;
	$tab=3;
}

// new employee (deprecated; not in use)
if($action==10)
{
	$action=0;
	$tab=0;
	$vujade->create_row('employees');
	$id = $vujade->row_id;
	$employee['fullname']="New Employee";
	$max = $vujade->get_max_employee_id();
	$employee_id = $max+1;
	$success = $vujade->update_row('employees',$id,'employee_id',$employee_id);
	$employee['employee_id']=$employee_id;
}

$setup = $vujade->get_setup();
$max = $setup['max_site_users'];
$current_user_count = 0;
$all_employees = $vujade->get_employees();
foreach($all_employees as $all_e)
{
	$is_website_user = $all_e['website_user'];
	$current_user_count+=$is_website_user;
}
if($current_user_count==0)
{
	$cap_reached=1;
}
if($current_user_count>=$max)
{
	$cap_reached=1;
}
else
{
	$cap_reached=0;
}

// new employee
if($action==11)
{
	$action=0;
	$tab=2;
	$first_name=$_POST['first_name'];
	$last_name=$_POST['last_name'];
	$username=$_POST['u'];
	$password=$_POST['p'];
	$password=crypt($password);
	$vujade->create_row('employees');
	$id = $vujade->row_id;
	$max = $vujade->get_max_employee_id();
	$employee_id = $max+1;
	$success = $vujade->update_row('employees',$id,'employee_id',$employee_id);
	$success = $vujade->update_row('employees',$id,'first_name',$first_name);
	$success = $vujade->update_row('employees',$id,'last_name',$last_name);
	$success = $vujade->update_row('employees',$id,'username',$username);
	$success = $vujade->update_row('employees',$id,'password',$password);
	$success = $vujade->update_row('employees',$id,'access',1);
	$success = $vujade->update_row('employees',$id,'pw_changed',1);
	$success = $vujade->update_row('employees',$id,'pw_encrypted',1);

	if($cap_reached==0)
	{
		$success = $vujade->update_row('employees',$id,'website_user',1);
	}
	//$success = $vujade->update_row('employees',$id,'',1);
	//$employee['employee_id']=$employee_id;
	//$employee['fullname']=$first_name+" "+$last_name;
	$employee = $vujade->get_employee($id);
}

$max = $setup['max_site_users'];
$current_user_count = 0;
$all_employees = $vujade->get_employees();
foreach($all_employees as $all_e)
{
	$is_website_user = $all_e['website_user'];
	$current_user_count+=$is_website_user;
}
if($current_user_count==0)
{
	$cap_reached=1;
}
if($current_user_count>=$max)
{
	$cap_reached=1;
}
else
{
	$cap_reached=0;
}

$user = $vujade->get_employee($_SESSION['user_id']);
$emp=$user;
$section=9;
$title = $employee['fullname'] . " - ";
require_once('h.php');
?>

	<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $employee['fullname']; ?></a>
            </li>
          </ol>
          <a href = "employees.php" class = "btn btn-xs btn-primary pull-right">&laquo;Back to list</a>
        </div>
      </header>
      <!-- End: Topbar -->

	  <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>

			<div class="panel heading-border panel-primary">
	        	<div class="panel-body bg-light">
	        		
					<div class="panel" id="wide-panel">
						<div class="panel-heading">
							<ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
								<li class="<?php if($tab==0){ print 'active'; } ?>">
									<a href="#tab1" data-toggle="tab">Website Access</a>
								</li>

								<li class="<?php if($tab==1){ print 'active'; } ?>">
									<a href="#tab2" data-toggle="tab">Email Settings</a>
								</li>
								<?php
								if($er_permissions['edit']==1)
								{
								?>
									<li class="<?php if($tab==2){ print 'active'; } ?>">
										<a href="#tab3" data-toggle="tab">Employee Info</a>
									</li>

									<li class="<?php if($tab==3){ print 'active'; } ?>">
										<a href="#tab4" data-toggle="tab">Notes</a>
									</li>

									<li class="<?php if($tab==4){ print 'active'; } ?>">
										<a href="#tab5" data-toggle="tab">Permissions</a>
									</li>

									<li class="<?php if($tab==5){ print 'active'; } ?>">
										<a href="#tab6" data-toggle="tab">Documents</a>
									</li>
								<?php
								}
								if($er_permissions['delete']==1)
								{
									print '<a href = "#delete_employee" id = "delete" class = "btn btn-xs btn-danger pull-right" style = "margin-right:5px;"><i class = "glyphicons glyphicons-bin"></i></a>';
								}
								?>
							</ul>
						</div>

<div class="panel-body">
<div class="tab-content pn br-n">

<!-- tab 1 username and password -->
<div id="tab1" class="tab-pane <?php if($tab==0){ print 'active'; } ?>">
	<div class="row">
		<div class="col-md-12">

			<div class = "row">
				<form method = "post" action = "employee.php" class = "form-horizontal">
				<input type = "hidden" name = "action" value = "1">
				<input type = "hidden" name = "id" value = "<?php print $id; ?>">
				<div class="col-md-1" style = "margin-top:10px;">
					<strong>Username</strong>
				</div>

				<div class="col-md-4">
					<input type = "text" name = "u" id = "username" value = "<?php print $employee['username']; ?>" class = "form-control" style = "width:300px;">
				</div>

				<div class = "col-md-2">
					<input type = "submit" id = "change_un" value = "Update Username" class = "btn btn-primary" style = "width:150px;">
				</div>
				</form>
			</div>
			
		</div>
	</div>

	<div class="row" style = "margin-top:15px;">
		<div class="col-md-12">
			<div class = "row">
				<form method = "post" action = "employee.php" class = "form-horizontal">
				<input type = "hidden" name = "action" value = "2">
				<input type = "hidden" name = "id" value = "<?php print $id; ?>">
				<div class="col-md-1" style = "margin-top:10px;">
					<strong>Password</strong>
				</div>

				<div class="col-md-4">
					<input type = "text" name = "p" id = "password" value = "*****" class = "form-control" style = "width:300px;">
				</div>

				<div class = "col-md-2">
					<input type = "submit" id = "change_pw" value = "Update Password" class = "btn btn-primary" style = "width:150px;">
				</div>
				</form>
			</div>
			
		</div>
	</div>

</div>

<!-- tab 2 email settings -->
<div id="tab2" class="tab-pane <?php if($tab==1){ print 'active'; } ?>">
	<div class="row">
		<div class="col-md-12">
			<div class = "row">
				<form method = "post" action = "employee.php" class = "form-horizontal">
				<input type = "hidden" name = "action" value = "3">
				<input type = "hidden" name = "id" value = "<?php print $id; ?>">
				<div class="col-md-2" style = "margin-top:10px;">
					<strong>Email Reply To</strong>
				</div>

				<div class="col-md-10">
					<input type = "text" name = "reply_to" id = "reply_to" value = "<?php print $employee['email_reply_to']; ?>" class = "form-control" style = "width:500px;">
				</div>
			</div>

			<div class = "row" style = "margin-top:15px;">
				<div class="col-md-2" style = "margin-top:10px;">
					<strong>Signature</strong>
				</div>

				<div class="col-md-10">
					<textarea name = "sig" id = "sig" style = "width:500px;height:200px;">
						<?php 
						print $employee['email_signature']; 
						?>
					</textarea>
				</div>
			</div>

			<div class = "row" style = "margin-top:15px;">
				<div class = "col-md-3">
					<strong>Send tasks to my email?</strong>
				</div>
				<div class = "col-md-9">
					<input type = "radio" name = "send_tasks_to_email" value = "1" <?php if($employee['send_tasks_to_email']==1){ print 'checked="checked"'; }?>> Yes<br>
					<input type = "radio" name = "send_tasks_to_email" value = "0" <?php if($employee['send_tasks_to_email']==0){ print 'checked="checked"'; }?>> No
					
				</div>
			</div>

			<?php
			// if user is sales person, show this:
			if( ($employee['department']=="All Departments") || ($employee['department']=="Sales Dept. 400") )
			{
				?>

				<div class = "row" style = "margin-top:15px;">
					<div class = "col-md-3">
						<strong>Sales: Send me status change tasks?</strong>
					</div>
					<div class = "col-md-9">
						<input type = "radio" name = "status_change_task" value = "1" <?php if($employee['status_change_task']==1){ print 'checked="checked"'; }?>> Yes<br>
						<input type = "radio" name = "status_change_task" value = "0" <?php if($employee['status_change_task']==0){ print 'checked="checked"'; }?>> No
					</div>
				</div>
			<?php } ?>

			<div class = "row" style = "margin-top:15px;">
				<div class = "col-md-3">
					<strong>Send me a BCC email of all emails I send through the V Sign System? (Designs, Invoices, Photos, Proposals, Purchase Orders)</strong>
				</div>
				<div class = "col-md-9">
					<input type = "radio" name = "bcc" value = "1" <?php if($employee['bcc']==1){ print 'checked="checked"'; }?>> Yes<br>
					<input type = "radio" name = "bcc" value = "0" <?php if($employee['bcc']==0){ print 'checked="checked"'; }?>> No
				</div>
			</div>

			<div class = "row" style = "margin-top:15px;">
				<div class = "col-md-2">
				</div>
				<div class = "col-md-10" style = "width:522px;text-align:right;">
					<input type = "submit" id = "change_un" value = "Save" class = "btn btn-primary" style = "width:150px;">
				</div>
			</div>

			</form>
		</div>
	</div>
</div>

<!-- tab 3 personal info -->
<div id="tab3" class="tab-pane <?php if($tab==2){ print 'active'; } ?>">
	<div class="row">
	<div class="col-md-12">
	<form method = "post" action = "employee.php">
		<table class = "table">
			<tr class = "">
				<td class = "">ID</td>
				<td class = "">
					<div class = "col-md-3">
						<input type = "text" class = "form-control"  name = "employee_id" value = "<?php print $employee['employee_id']; ?>"  size = "" style = "width:150px;">
					</div>
					<div class = "col-md-9">
				  		<div class = "" style = "margin-top:10px;">
				  			<strong style = "color:#ED7764;"><i class = "glyphicons glyphicons-warning_sign"></i> Changing this ID number will delete any existing tasks to and from this user.</strong>
				  		</div>
				  	</div>
				</td>
			</tr>

			<?php
			if($cap_reached==0)
			{
			?>
			<tr class = "">
				<td class = "">Website User</td>
				<td class = "">
				<input type = "radio" name = "website_user" <?php if($employee['website_user']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
				<input type = "radio" name = "website_user" <?php if($employee['website_user']==0){ print 'checked = "checked"';} ?> value = "0"> No</td>
			</tr>
			<?php
			}
			if($cap_reached==1)
			{
				if($employee['website_user']==0)
				{
				?>
				<tr class = "">
				<td class = "" colspan = "2">
					<div class = "alert alert-danger">Cannot grant website access to this user. Maximum number of website users has been reached. Please contact Vujade Software to upgrade your account for more users: 844-My-V-Sign.</div>
				</td>
				</tr>
				<?php
				}
			}
			if($cap_reached==1)
			{
				if($employee['website_user']==1)
				{
				?>
				<tr class = "">
					<td class = "">Website User</td>
					<td class = "">
					<input type = "radio" name = "website_user" <?php if($employee['website_user']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
					<input type = "radio" name = "website_user" <?php if($employee['website_user']==0){ print 'checked = "checked"';} ?> value = "0"> No</td>
				</tr>
				<?php
				}
			}
			?>
			<tr class = "">
				<td class = "">Active Employee</td>
				<td class = "">
				<input type = "radio" name = "access" <?php if($employee['access']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
				<input type = "radio" name = "access" <?php if($employee['access']==0){ print 'checked = "checked"';} ?> value = "0"> No
				</td>
			</tr>

			<tr class = "">
				<td class = "">First Name</td>
				<td class = ""><input type = "text" class = "form-control"  name = "first_name" value = "<?php print $employee['first_name']; ?>" size = "60"></td>
			</tr>
			<tr class = "">
				<td class = "">Middle Initial</td>
				<td class = ""><input type = "text" class = "form-control"  name = "middle" value = "<?php print $employee['middle']; ?>" style = "width:50px;"></td>
			</tr>
			<tr class = "">
				<td class = "">Last Name</td>
				<td class = ""><input type = "text" class = "form-control"  name = "last_name" value = "<?php print $employee['last_name']; ?>" size = "60"></td>
			</tr>

			<tr class = "">
				<td class = "">DOB</td>
				<td class = ""><input type = "text" class = "form-control"  name = "dob" value = "<?php print $employee['dob']; ?>" style = "width:150px;"></td>
			</tr>

			<tr class = "">
				<td class = "">SSN#</td>
				<td class = ""><input type = "text" class = "form-control"  name = "ssn" value = "<?php print $employee['ssn']; ?>" style = "width:150px;"></td>
			</tr>

			<tr class = "">
				<td class = "">Gender</td>
				<td class = "">
					<input type = "radio" name = "gender" value = "Male" <?php if($employee['gender']=="Male"){ print 'checked = "checked"'; } ?>>Male <input type = "radio" name = "gender" value = "Female" <?php if($employee['gender']=="Female"){ print 'checked = "checked"'; } ?>>Female
				</td>
			</tr>

			<tr class = "">
				<td class = "">Address</td>
				<td class = ""><input type = "text" class = "form-control"  name = "street_1" value = "<?php print $employee['street_1']; ?>" size = "60"></td>
			</tr>
			<tr class = "">
				<td class = "">City</td>
				<td class = ""><input type = "text" class = "form-control"  name = "city" value = "<?php print $employee['city']; ?>" size = "60"></td>
			</tr>
			<tr class = "">
				<td class = "">State / Province</td>
				<td class = "">
		            <select id="state" name="state" style = "width:200px;" class = "form-control">
		              <?php
		              if(isset($employee['state']))
		              {
		                  print '<option value = "'.$employee['state'].'" selected = "selected">'.$employee['state'].'</option>';
		              }
		              if($setup['country']=="USA")
		              {
		              ?>
			              <option value="">-US States-</option>
			              <option value="AL">Alabama</option>
			              <option value="AK">Alaska</option>
			              <option value="AZ">Arizona</option>
			              <option value="AR">Arkansas</option>
			              <option value="CA">California</option>
			              <option value="CO">Colorado</option>
			              <option value="CT">Connecticut</option>
			              <option value="DE">Delaware</option>
			              <option value="DC">District Of Columbia</option>
			              <option value="FL">Florida</option>
			              <option value="GA">Georgia</option>
			              <option value="HI">Hawaii</option>
			              <option value="ID">Idaho</option>
			              <option value="IL">Illinois</option>
			              <option value="IN">Indiana</option>
			              <option value="IA">Iowa</option>
			              <option value="KS">Kansas</option>
			              <option value="KY">Kentucky</option>
			              <option value="LA">Louisiana</option>
			              <option value="ME">Maine</option>
			              <option value="MD">Maryland</option>
			              <option value="MA">Massachusetts</option>
			              <option value="MI">Michigan</option>
			              <option value="MN">Minnesota</option>
			              <option value="MS">Mississippi</option>
			              <option value="MO">Missouri</option>
			              <option value="MT">Montana</option>
			              <option value="NE">Nebraska</option>
			              <option value="NV">Nevada</option>
			              <option value="NH">New Hampshire</option>
			              <option value="NJ">New Jersey</option>
			              <option value="NM">New Mexico</option>
			              <option value="NY">New York</option>
			              <option value="NC">North Carolina</option>
			              <option value="ND">North Dakota</option>
			              <option value="OH">Ohio</option>
			              <option value="OK">Oklahoma</option>
			              <option value="OR">Oregon</option>
			              <option value="PA">Pennsylvania</option>
			              <option value="RI">Rhode Island</option>
			              <option value="SC">South Carolina</option>
			              <option value="SD">South Dakota</option>
			              <option value="TN">Tennessee</option>
			              <option value="TX">Texas</option>
			              <option value="UT">Utah</option>
			              <option value="VT">Vermont</option>
			              <option value="VA">Virginia</option>
			              <option value="WA">Washington</option>
			              <option value="WV">West Virginia</option>
			              <option value="WI">Wisconsin</option>
			              <option value="WY">Wyoming</option>
			              <option value="">---------------</option>
			              <option value="">-Canadian Provinces-</option>
			              <option value="Alberta">Alberta</option>
			              <option value="British Columbia">British Columbia</option>
			              <option value="Manitoba">Manitoba</option>
			              <option value="New Brunswick">New Brunswick</option>
			              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
			              <option value="Northwest Territories">Northwest Territories</option>
			              <option value="Nova Scotia">Nova Scotia</option>
			              <option value="Nunavut">Nunavut</option>
			              <option value="Ontario">Ontario</option>
			              <option value="Prince Edward Island">Prince Edward Island</option>
			              <option value="Quebec">Quebec</option>
			              <option value="Saskatchewan">Saskatchewan</option>
			              <option value="Yukon">Yukon</option>
			        <?php 
			    	} 
			    	// canadian servers
			    	if($setup['country']=='Canada')
			    	{
			    		?>
			    		  <option value="">-Canadian Provinces-</option>
			              <option value="Alberta">Alberta</option>
			              <option value="British Columbia">British Columbia</option>
			              <option value="Manitoba">Manitoba</option>
			              <option value="New Brunswick">New Brunswick</option>
			              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
			              <option value="Northwest Territories">Northwest Territories</option>
			              <option value="Nova Scotia">Nova Scotia</option>
			              <option value="Nunavut">Nunavut</option>
			              <option value="Ontario">Ontario</option>
			              <option value="Prince Edward Island">Prince Edward Island</option>
			              <option value="Quebec">Quebec</option>
			              <option value="Saskatchewan">Saskatchewan</option>
			              <option value="Yukon">Yukon</option>
			              <option value="">---------------</option>
			              <option value="">-US States-</option>
			              <option value="AL">Alabama</option>
			              <option value="AK">Alaska</option>
			              <option value="AZ">Arizona</option>
			              <option value="AR">Arkansas</option>
			              <option value="CA">California</option>
			              <option value="CO">Colorado</option>
			              <option value="CT">Connecticut</option>
			              <option value="DE">Delaware</option>
			              <option value="DC">District Of Columbia</option>
			              <option value="FL">Florida</option>
			              <option value="GA">Georgia</option>
			              <option value="HI">Hawaii</option>
			              <option value="ID">Idaho</option>
			              <option value="IL">Illinois</option>
			              <option value="IN">Indiana</option>
			              <option value="IA">Iowa</option>
			              <option value="KS">Kansas</option>
			              <option value="KY">Kentucky</option>
			              <option value="LA">Louisiana</option>
			              <option value="ME">Maine</option>
			              <option value="MD">Maryland</option>
			              <option value="MA">Massachusetts</option>
			              <option value="MI">Michigan</option>
			              <option value="MN">Minnesota</option>
			              <option value="MS">Mississippi</option>
			              <option value="MO">Missouri</option>
			              <option value="MT">Montana</option>
			              <option value="NE">Nebraska</option>
			              <option value="NV">Nevada</option>
			              <option value="NH">New Hampshire</option>
			              <option value="NJ">New Jersey</option>
			              <option value="NM">New Mexico</option>
			              <option value="NY">New York</option>
			              <option value="NC">North Carolina</option>
			              <option value="ND">North Dakota</option>
			              <option value="OH">Ohio</option>
			              <option value="OK">Oklahoma</option>
			              <option value="OR">Oregon</option>
			              <option value="PA">Pennsylvania</option>
			              <option value="RI">Rhode Island</option>
			              <option value="SC">South Carolina</option>
			              <option value="SD">South Dakota</option>
			              <option value="TN">Tennessee</option>
			              <option value="TX">Texas</option>
			              <option value="UT">Utah</option>
			              <option value="VT">Vermont</option>
			              <option value="VA">Virginia</option>
			              <option value="WA">Washington</option>
			              <option value="WV">West Virginia</option>
			              <option value="WI">Wisconsin</option>
			              <option value="WY">Wyoming</option>
			    		<?php
			    	}
			    	?>
		            </select>
				</td>
			</tr>
			<tr class = "">
				<td class = "">Zip Code</td>
				<td class = ""><input type = "text" class = "form-control"  name = "zip" value = "<?php print $employee['zip']; ?>" style = "width:150px;"></td>
			</tr>
			<tr class = "">
				<td class = "">Home Phone</td>
				<td class = ""><input type = "text" class = "form-control"  name = "home_phone" value = "<?php print $employee['home_phone']; ?>" style = "width:150px;"></td>
			</tr>
			<tr class = "">
				<td class = "">Cellphone</td>
				<td class = ""><input type = "text" class = "form-control"  name = "cell_phone" value = "<?php print $employee['cell_phone']; ?>" style = "width:150px;"></td>
			</tr>
			<tr class = "">
				<td class = "">Email</td>
				<td class = ""><input type = "text" class = "form-control"  name = "email" value = "<?php print $employee['email']; ?>"></td>
			</tr>
			<tr class = "">
				<td class = "">Driver's License #</td>
				<td class = ""><input type = "text" class = "form-control"  name = "dl_number" value = "<?php print $employee['dl_number']; ?>" style = "width:150px;"></td>
			</tr>
			<tr class = "">
				<td class = "">Expiration</td>
				<td class = ""><input type = "text" class = "form-control"  name = "dl_expiration" value = "<?php print $employee['dl_expiration']; ?>" style = "width:150px;"></td>
			</tr>
			
			<tr class = "">
				<td class = "">Start Date</td>
				<td class = ""><input type = "text" class = "form-control"  name = "start_date" value = "<?php print $employee['start_date']; ?>" style = "width:150px;"></td>
			</tr>

			<tr class = "">
				<td class = "">Area</td>
				<td class = "">
				<?php
				$areas = $vujade->get_areas();
				if($areas['error']==0)
				{
					print '<select name = "area" class = "form-control" style = "width:200px;">';
					if(isset($employee['area']))
					{
						print '<option value = "'.$employee['area'].'" selected = "selected">'.$employee['area'].'</option>';
						print '<option value = "">-Select-</option>';
					}
					unset($areas['error']);
					foreach($areas as $area)
					{
						print '<option value = "'.$area['name'].'">'.$area['name'].'</option>';
					}
					print '</select>';
				}
				else
				{
					?>
					<input type = "text" class = "form-control"  name = "area" value = "<?php print $employee['area']; ?>" style = "width:150px;"></td>
					<?php
				}
				?>
			</tr>

			<tr class = "">
				<td class = "">Department</td>
				<td class = "">

				<?php
				$departments = $vujade->get_departments();
				if($departments['error']==0)
				{
					print '<select name = "department" style = "width:200px;" class = "form-control">';
					if(isset($employee['department']))
					{
						print '<option value = "'.$employee['department'].'" selected = "selected">'.$employee['department'].'</option>';
						print '<option value = "">-Select-</option>';
					}
					unset($departments['error']);
					foreach($departments as $department)
					{
						print '<option value = "'.$department['name'].'">'.$department['name'].'</option>';
					}
					print '</select>';
				}
				else
				{
					?>
					<input type = "text" class = "form-control"  name = "department" value = "<?php print $employee['department']; ?>" style = "width:150px;"></td>
					<?php
				}
				?>
			</tr>

			<tr class = "">
				<td class = "">Fulltime</td>
				<td class = "">

				<input type = "radio" name = "fulltime" <?php if($employee['fulltime']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
				<input type = "radio" name = "fulltime" <?php if($employee['fulltime']==2){ print 'checked = "checked"';} ?> value = "2"> No
			</tr>

			<tr class = "">
				<td class = "">Salary or Hourly</td>
				<td class = "">

				<input type = "radio" name = "is_salary" <?php if($employee['is_salary']==1){ print 'checked = "checked"';} ?> value = "1"> Salary
				<input type = "radio" name = "is_salary" <?php if($employee['is_salary']==0){ print 'checked = "checked"';} ?> value = "0"> Hourly
			</tr>

			<tr class = "">
				<td class = "">Current Rate</td>
				<td class = "">
				<input type = "text" class = "form-control"  name = "rate" value = "<?php print $current_rate['rate']; ?>" style = "width:150px;">
				</td>
			</tr>

			<tr class = "">
				<td class = "">Print time card</td>
				<td class = "">
				<input type = "radio" name = "print_time_card" <?php if($employee['print_time_card']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
				<input type = "radio" name = "print_time_card" <?php if($employee['print_time_card']==2){ print 'checked = "checked"';} ?> value = "2"> No
				</td>
			</tr>

			<tr class = "">
				<td class = "">Process Order Notification</td>
				<td class = "">
				<input type = "radio" name = "po_notify" <?php if($employee['po_notify']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
				<input type = "radio" name = "po_notify" <?php if($employee['po_notify']==2){ print 'checked = "checked"';} ?> value = "2"> No
				</td>
			</tr>

			<tr class = "">
				<td class = "">Termination Date</td>
				<td class = ""><input type = "text" class = "form-control"  name = "termination_date" value = "<?php print $employee['termination_date']; ?>"></td>
			</tr>
			<tr class = "">
				<td class = "">Cobra Notice</td>
				<td class = "">
				<input type = "radio" name = "cobra_notice" <?php if($employee['cobra_notice']==1){ print 'checked = "checked"';} ?> value = "1"> Yes
				<input type = "radio" name = "cobra_notice" <?php if($employee['cobra_notice']==2){ print 'checked = "checked"';} ?> value = "2"> No
				</td>
			</tr>

			<tr class = "">
			<td colspan = "2">
			<input type = "hidden" name = "action" value = "4">
			<input type = "hidden" name = "id" value = "<?php print $id; ?>">
			<input type = "submit" value = "Update" class = "btn btn-primary" style = "width:150px;margin-top:15px;">
			</td>
			</tr>

		</table>
	</form>
	</div>
	</div>
</div>

<!-- tab 4 Notes -->
<div id="tab4" class="tab-pane <?php if($tab==3){ print 'active'; } ?>">

	<div style="width: 100%;">
		<style>
			.conversation-date.active,
			.contact-name.active{
				background: #e5e5e5;
				text-decoration: none !important;
			}
			.small-ph
			{
				line-height: 12px;
				height:30px;
				border:1px solid red;
			}
		</style>
		<div class="row">
			<div class="col-md-2">
				<div class="panel panel-primary">
					<div class="panel-heading" style = "padding-left:10px;padding-right:10px; padding-top:10px;padding-bottom:10px;margin-top:0px;border-top:4px solid #4A89DC;">
						<div class="widget-menu pull-right">
							<a href = "employee_note.php?action=0&employee_id=<?php print $id; ?>" class = "btn btn-xs btn-primary" style = "">+</a>
						</div>
					</div>
					<div class="panel-body">
						<?php
						$dates = $vujade->get_employee_notes($id);
						if($dates['error']=="0")
						{
							unset($dates['error']);
							unset($dates['count']);
							$show=1;
							foreach($dates as $date)
							{
								print '<a class = "contact-name" href = "#" id = "'.$date['id'].'">';
								print $date['date'].'</a><br>';
							}
						}
						else
						{
							$show=0;
						}
						?>
					</div>
				</div>
			</div>

			<div id = "contact-list" class="col-md-9" style="margin-left:5px;border:0px solid red;padding:0px;">
				<?php
				if($show==1)
				{
					foreach($dates as $c)
					{
						print '<div class="panel-contact panel panel-primary" id = "contact-'.$c['id'].'">';

						print '<div class="panel-heading note-heading" id = "contact-heading-'.$c['id'].'" style = "padding-left:10px;padding-right:10px; padding-top:10px;padding-bottom:10px;margin-top:0px;border-top:4px solid #4A89DC;">';

						print '<div style = "float:left;width:60%;color:#2876A5;font-weight:bold;">';
						print $c['date'].' - '.$c['created_by_name'];
						print '</div>';

						print '<div class="widget-menu pull-right">';
						?>
							<a href = "employee_note.php?note_id=<?php print $c['id']; ?>&employee_id=<?php print $id; ?>" class = "btn btn-xs btn-primary">EDIT</a>
							<a class = "btn btn-xs btn-danger delete-note" id="delete_note" href="#delete_note_form_<?= $c['id']; ?>" title = "Delete Note">Delete</a>

							<!-- modal for delete note -->
							<div id="delete_note_form_<?= $c['id']; ?>" class="delete-contact-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
								<h1>Delete Contact</h1>
								<p>Are you sure you want to delete this note?</p>
								<p>
									<a id = "" class="btn btn-lg btn-danger"
									   href="employee.php?action=9&note_id=<?= $c['id']; ?>&id=<?= $id; ?>&tab=3">YES</a>
									<a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>
								</p>
							</div>

						<?php
						print '</div>';
						print '</div>';

						print '<div class = "panel-body" id = "contact-box-'.$c['id'].'">';
						print '<div class = "row">';
						print '<div class = "col-md-12">';
						print $c['note'];
						print '</div>';
						print '</div>';
						print '</div>';
						print '</div>';
					}
				}
				?>
				</div>

			
		</div>
	</div>
</div>

<!-- tab 5 Permissions -->
<div id="tab5" class="tab-pane <?php if($tab==4){ print 'active'; } ?>">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
		<div class="col-md-12">
		<style>
		.label_box
		{
			width:200px;
			background-color: #3B3F4F;
			margin-right:10px;
			padding:5px;
			color:white;
		}
		.bordered
		{
			padding-bottom:5px;
			border-bottom:1px solid black;
		}
		</style>

		<div class = "row">
			<div class = "col-md-9">
				<a href = "#pg-modal" id = "pg" class = "pull-right" style = "font-weight:bold;">Permissions Guide</a> 
			</div>
			<div class = "col-md-3">
				<a href = "#" id = "check_all" class = "btn btn-primary btn-xs pull-right">Check All</a> <a href = "#" id = "uncheck_all" class = "btn btn-primary btn-xs pull-right" style = "display:none;">Uncheck All</a>
			</div>
		</div>

		<table class = "table">

			<form method = "post" action = "employee.php">

			<!-- accounting -->
			<tr class = "bordered">
				<td class = "label_box">Accounting</td>
				<td>
					<?php
					$accounting_permissions = $vujade->get_permission($id,'Accounting');
					?>
					<input type = "checkbox" class = "toggler"  name = "accounting_read" value = "1" <?php if($accounting_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "accounting_edit" value = "1" <?php if($accounting_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "accounting_delete" value = "1" <?php if($accounting_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  

				</td>
			</tr>

			<!-- calendar -->
			<tr class = "bordered">
				<td class = "label_box">Calendar</td>
				<td>
					<?php
					$calendar_permissions = $vujade->get_permission($id,'Calendar');
					?>
					<input type = "checkbox" class = "toggler"  name = "calendar_read" value = "1" <?php if($calendar_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "calendar_edit" value = "1" <?php if($calendar_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 
				</td>
			</tr>

			<!-- clients -->
			<tr class = "bordered">
				<td class = "label_box">Clients</td>
				<td>
					<?php
					$clients_permissions = $vujade->get_permission($id,'Clients');
					?>
					<input type = "checkbox" class = "toggler"  name = "clients_read" value = "1" <?php if($clients_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "clients_edit" value = "1" <?php if($clients_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "clients_delete" value = "1" <?php if($clients_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  

				</td>
			</tr>

			<!-- designs -->
			<tr class = "bordered">
				<td class = "label_box">Designs</td>
				<td>
					<?php
					$designs_permissions = $vujade->get_permission($id,'Designs');
					?>
					<input type = "checkbox" class = "toggler"  name = "designs_read" value = "1" <?php if($designs_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "designs_edit" value = "1" <?php if($designs_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "designs_delete" value = "1" <?php if($designs_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  

					<?php
					$ds_permissions = $vujade->get_permission($id,'Design Sort');
					?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type = "checkbox" class = "toggler"  name = "design_sort" value = "1" <?php if($ds_permissions['read']==1){ print 'checked = "checked"'; } ?>>Design Sort 

				</td>
			</tr>

			<!-- documents -->
			<tr class = "bordered">
				<td class = "label_box">Documents</td>
				<td>
					<?php
					$documents_permissions = $vujade->get_permission($id,'Documents');
					?>
					<input type = "checkbox" class = "toggler"  name = "documents_read" value = "1" <?php if($documents_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "documents_edit" value = "1" <?php if($documents_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "documents_delete" value = "1" <?php if($documents_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- employee records -->
			<tr class = "bordered">
				<td class = "label_box">Employee Records</td>
				<td>
					<?php
					$er_permissions = $vujade->get_permission($id,'Employee Records');
					?>
					<input type = "checkbox" class = "toggler"  name = "er_read" value = "1" <?php if($er_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "er_edit" value = "1" <?php if($er_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "er_delete" value = "1" <?php if($er_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- estimates -->
			<tr class = "bordered">
				<td class = "label_box">Estimates</td>
				<td>
					<?php
					$estimates_permissions = $vujade->get_permission($id,'estimates');
					?>
					<input type = "checkbox" class = "toggler"  name = "estimates_read" value = "1" <?php if($estimates_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "estimates_edit" value = "1" <?php if($estimates_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create  

					<input type = "checkbox" class = "toggler"  name = "estimates_delete" value = "1" <?php if($estimates_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete 

					<?php
					// estimate sort
					$es_permissions = $vujade->get_permission($id,'Estimate Sort');
					?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type = "checkbox" class = "toggler"  name = "estimate_sort" value = "1" <?php if($es_permissions['read']==1){ print 'checked = "checked"'; } ?>>Estimate Sort 

					<?php
					// estimate full access
					$efa_permissions = $vujade->get_permission($id,'Estimate Full Access');
					?>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type = "checkbox" class = "toggler"  name = "estimate_add" value = "1" <?php if($efa_permissions['read']==1){ print 'checked = "checked"'; } ?>>Full Access 
				</td>
			</tr>

			<!-- Inventory -->
			<tr class = "bordered">
				<td class = "label_box">Inventory</td>
				<td>
					<?php
					$inventory_permissions = $vujade->get_permission($id,'Inventory');
					?>
					<input type = "checkbox" class = "toggler"  name = "inventory_read" value = "1" <?php if($inventory_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "inventory_edit" value = "1" <?php if($inventory_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "inventory_delete" value = "1" <?php if($inventory_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- Invoices -->
			<tr class = "bordered">
				<td class = "label_box">Invoices</td>
				<td>
					<?php
					$invoices_permissions = $vujade->get_permission($id,'Invoices');
					?>
					<input type = "checkbox" class = "toggler"  name = "invoices_read" value = "1" <?php if($invoices_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "invoices_edit" value = "1" <?php if($invoices_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "invoices_delete" value = "1" <?php if($invoices_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- production files -->
			<tr class = "bordered">
				<td class = "label_box">Production Files</td>
				<td>
					<?php
					$files_permissions = $vujade->get_permission($id,'Production Files');
					?>
					<input type = "checkbox" class = "toggler"  name = "files_read" value = "1" <?php if($files_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "files_edit" value = "1" <?php if($files_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "files_delete" value = "1" <?php if($files_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- Projects -->
			<tr class = "bordered">
				<td class = "label_box">Projects</td>
				<td>
					<?php
					$projects_permissions = $vujade->get_permission($id,'Projects');
					?>
					<input type = "checkbox" class = "toggler"  name = "projects_read" value = "1" <?php if($projects_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "projects_edit" value = "1" <?php if($projects_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "projects_delete" value = "1" <?php if($projects_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- Proposals -->
			<tr class = "bordered">
				<td class = "label_box">Proposals</td>
				<td>
					<?php
					$proposals_permissions = $vujade->get_permission($id,'Proposals');
					?>
					<input type = "checkbox" class = "toggler"  name = "proposals_read" value = "1" <?php if($proposals_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "proposals_edit" value = "1" <?php if($proposals_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "proposals_delete" value = "1" <?php if($proposals_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- Purchase Orders -->
			<tr class = "bordered">
				<td class = "label_box">Purchase Orders</td>
				<td>
					<?php
					$purchase_orders_permissions = $vujade->get_permission($id,'Purchase Orders');
					?>
					<input type = "checkbox" class = "toggler"  name = "purchase_orders_read" value = "1" <?php if($purchase_orders_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "purchase_orders_edit" value = "1" <?php if($purchase_orders_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "purchase_orders_delete" value = "1" <?php if($purchase_orders_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- Shop Orders -->
			<tr class = "bordered">
				<td class = "label_box">Shop Orders</td>
				<td>
					<?php
					$shop_orders_permissions = $vujade->get_permission($id,'Shop Orders');
					?>
					<input type = "checkbox" class = "toggler"  name = "shop_orders_read" value = "1" <?php if($shop_orders_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "shop_orders_edit" value = "1" <?php if($shop_orders_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 

					<input type = "checkbox" class = "toggler"  name = "shop_orders_delete" value = "1" <?php if($shop_orders_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- Shop Orders -- turn in for billing -->
			<tr class = "bordered">
				<td class = "label_box">Turn in for billing</td>
				<td>
					<?php
					$tifb = $vujade->get_permission($id,'Turned in for billing');
					?>
					<input type = "checkbox" class = "toggler"  name = "tifb" value = "1" <?php if($tifb['read']==1){ print 'checked = "checked"'; } ?>>Yes
				</td>
			</tr>

			<!-- Vendors -->
			<tr class = "bordered">
				<td class = "label_box">Vendors</td>
				<td>
					<?php
					$vendors_permissions = $vujade->get_permission($id,'vendors');
					?>
					<input type = "checkbox" class = "toggler"  name = "vendors_read" value = "1" <?php if($vendors_permissions['read']==1){ print 'checked = "checked"'; } ?>>Read 

					<input type = "checkbox" class = "toggler"  name = "vendors_edit" value = "1" <?php if($vendors_permissions['edit']==1){ print 'checked = "checked"'; } ?>>Edit / Create 
					
					<input type = "checkbox" class = "toggler"  name = "vendors_delete" value = "1" <?php if($vendors_permissions['delete']==1){ print 'checked = "checked"'; } ?>>Delete  
				</td>
			</tr>

			<!-- admin -->
			<tr class = "bordered">
				<td class = "label_box">Manager</td>
				<td>
					<?php
					$admin = $vujade->get_permission($id,'Admin');
					?>
					<input type = "checkbox" class = "toggler"  name = "admin" value = "1" <?php if($admin['read']==1){ print 'checked = "checked"'; } ?>>Yes
				</td>
			</tr>

			<tr>
				<td colspan = "2">&nbsp;
				</td>
			</tr>

			<tr>
				<td>&nbsp;
				</td>
				<td>
					<input type = "hidden" name = "action" value = "7">
					<input type = "hidden" name = "id" value = "<?php print $id; ?>">
					<input type = "submit" value = "Save" class = "btn btn-primary" style = "width:150px;">
					</form>
				</td>
			</tr>

		</table>
		</div>
	</div>
		</div>
	</div>
</div>

<!-- tab 6 Documents -->
<div id="tab6" class="tab-pane <?php if($tab==5){ print 'active'; } ?>">
	<div class="row">
		<div class="col-md-12">
			<p><a href = "new_employee_document.php?employee_id=<?php print $id; ?>" class = "btn btn-primary">New Document</a></p>
		<?php
		# get list of documents on file
		?>
		<table class = "table">
			<tr>
				<td colspan = "4">&nbsp;</td>
			</tr>
			<tr style = "font-weight:bold;">
				<td width = "60%">Name</td>
				<td width = "">&nbsp;</td>
				<td width = "">&nbsp;</td>
				<td width = "">Date/Time</td>
				<td width = "" style = "text-align:right;">&nbsp;</td>
			</tr>
			<?php
			# get all docs for this employee and display as table row with delete button
			$docs = $vujade->get_documents($id);
			if($docs['error']=="0")
			{
				unset($docs['error']);
				foreach($docs as $doc)
				{
					# print out the file details; this one is not in a folder
					print '<tr>';

					// name
					print '<td valign = "top" width = "60%">';
					print '<a href = "download_document.php?employee=1&employee_id='.$id.'&file_id='.$doc['database_id'].'">';
					print $doc['file_name'];
					print '</a>';
					print '</td>';

					// type
					print '<td valign = "top">';
					$type_data = $vujade->get_document_type($doc['file_type']);
					print $type_data['name'];
					print '</td>';

					# edit button
					print '<td valign = "top">&nbsp;';
					print '</td>';

					// date
					print '<td valign = "top">';
					//print $doc['ts'];
					
					$ts = strtotime($doc['ts']);
					$formatted_ts = date('m/d/Y',$ts);
					print $formatted_ts;
					print '</td>';

					// delete
					print '<td style = "text-align:right;" valign = "top">';
					print '<a href = "#delete_document_modal" class = "btn btn-xs btn-danger delete_document ddoc" id = "'.$doc['database_id'].'"><i class = "glyphicons glyphicons-bin"></i></a>';
					print '</td>';
					print '</tr>';
				}
			}
		?>
		</table>

		</div>
	</div>
</div>

</div>
</div>
</div>

</div>
</div>
</div>
</section>
</section>

<!-- modal for delete employee -->
<div id="delete_employee" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1>Delete Employee</h1>
<p>Are you sure you want to delete this employee?</p>
<p><a id = "" class="btn btn-lg btn-danger" href="employee.php?id=<?php print $id; ?>&action=5">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- modal for delete document -->
<div id="delete_document_modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1>Delete Document</h1>
<p>Are you sure you want to delete this document?</p>
<p><a id = "delete_document_yes" class="btn btn-lg btn-danger" href="#">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- modal permissions guide -->
<div id="pg-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<a class="close-gl btn btn-xs btn-danger pull-right" href = "#">Close</a>
<h1>Permissions Guide</h1>
<div class = "row">
	<div class = "col-md-12">
		When a box is check marked, it is allowing the user to do what the description is.<br>
		Read - The ability to view the page(s) but not edit or delete.<br>
		Edit/Create – The ability to modify the page(s) and create new page(s).<br>
		Delete – The ability to delete page(s) from the particular section.<br>
		<br>
		<b><u>Designs</u></b><br>
		Design Sort – The ability to change the waiting list in the design queue.<br>
		<br>
		<b><u>Estimates</u></b><br>
		Estimate Sort - The ability to change the waiting list in the estimate queue.<br>
		Full Access – Allows the user past the description page and allows them to create and modify
		estimates.
		<br>
		<br>
		<b><u>Turn in for billing</u></b><br>
		Allows the user to turn a job from active into the accounting department for billing
		<br>
		<br>
		<b><u>Manager</u></b><br>
		The manager button allows the user to review information that the typical user is not allowed to.
		<br>
		The following is what it is allowed to view:<br>
		Calendar – The dollar number associated with each job this week<br>
		Volume Report – Allows the user to see how each sales person is doing. Monthly / yearly activity<br>
		Department Hours – The ability to see how many hours in each department<br>
		<br>
		<b><u>Access to other areas of the system if the following is checked –</u></b><br>
		Site setup – All boxes checked in the accounting and Employee Records sections
		Monthly Sales Report – Read and edit /create in proposals section must be checked<br>
		A/R Report - Read and edit /create in proposals section must be checked<br>
		Purchase Orders Report – Read and Edit/Create in Purchase Orders section must be checked<br>
		Closed Jobs – Read in Accounting Section
		</div>
	</div>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- modal -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    var sig = $('#sig').html();
    $("#sig").html($.trim(sig));
    
    $('#pg').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#pg-modal',
		modal: true
	});

    // modal: delete employee
    $('#delete').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete_employee',
		modal: true
	});

    // delete a note
    $('.delete-note').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete_note_modal',
		modal: true
	});

    var HEADER = 60; // header height

	// focus on a note
	$('.contact-name').click(function(e){
		e.preventDefault();

		var contact = $("#contact-"+this.id);

		var id = this.id;		

		$('.note-heading').each(function()
		{
			$(this).css('border-top','4px solid #4A89DC');
			$(this).removeClass('active-note');
		});

		$('#contact-heading-'+id).css('border-top','4px solid #F6BB42');
		$('#contact-heading-'+id).addClass('active-note');

		$('html, body').animate({
				scrollTop: $('.active-note').offset().top - HEADER
			}, 500);

		/*

		if( $(this).hasClass('active') )
		{
			$(this).removeClass('active');
			contact.removeClass('panel-warning');
			//contact.css('border-top','0px solid #4A89DC');
		} 
		else 
		{
			$('.contact-name').not( $(this) ).removeClass('active');
			$(this).addClass('active');

			$('.panel-contact').not( contact ).removeClass('panel-warning ph-yellow');
			contact.addClass('panel-warning');

			//contact.css('border-top','4px solid #F6BB42');

			$('html, body').animate({
				scrollTop: contact.offset().top - HEADER
			}, 500);
		}
		*/
	});

	$('.delete-contact').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '.delete-contact-form',
		modal: true
	});

    // delete a document
    $('.delete_document').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete_document_modal',
		modal: true
	});

    // helpers; magnific popup can't get id correctly with class
    $('.ddoc').click(function(e)
    {
    	e.preventDefault();
    	var id = this.id;
		var url = "employee.php?id=<?php print $id; ?>&action=8&docid="+id;
		$('#delete_document_yes').prop('href',url);
    });

	// modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    // close the guidelines
	$(document).on('click', '.close-gl', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	// password box focus
    $('#password').focus(function()
    {
    	$(this).val('');
    });

    // focus out
    $('#password').focusout(function()
    {
    	var val = $(this).val();
    	if(val=="")
    	{
    		$(this).val('*****');
    	}
    });

    // debug issue with label of check all / uncheck all
    // should be 41 checkboxes checked

    var cked = 0;
    $('.toggler').each(function()
    {
    	if($('input.toggler').not(':checked').length > 0)
    	{

    	}
    	else
    	{
    		cked++;
    	}
    });

    if(cked==41)
    {
    	$('#check_all').hide();
    	$('#uncheck_all').show();
    }
    else
    {
    	$('#check_all').show();
    	$('#uncheck_all').hide();
    }

   	$('.toggler').change(function()
   	{
   		var cked2 = 0;
	    $('.toggler').each(function()
	    {
	    	if($('input.toggler').not(':checked').length > 0)
	    	{

	    	}
	    	else
	    	{
	    		cked2++;
	    	}
	    });

	    if(cked2==41)
	    {
	    	$('#check_all').hide();
	    	$('#uncheck_all').show();
	    }
	    else
	    {
	    	$('#check_all').show();
	    	$('#uncheck_all').hide();
	    }
   	});

   	// focus on a note
    $('.date-no-class').click(function(e)
    {
    	e.preventDefault();
    	var focus_id = this.id;
    	$("#c-"+focus_id).focus();
    	$('.conversation-box').each(function()
    	{
    		$(this).css('background-color','white');
    	});
    	$('#c-'+focus_id).scrollintoview();
    	$( "#c-"+focus_id ).animate({
		    color: "black",
		    backgroundColor: "#989898"
		});
		$( "#cb-"+focus_id+" conversation-body" ).css('color','white');
    });


	$('#check_all').click(function(e)
    {
    	e.preventDefault();
    	$('.toggler').each(function()
    	{
		    $(this).prop('checked', 'checked');
		});
		$('#check_all').hide();
		$('#uncheck_all').show();
    });

    $('#uncheck_all').click(function(e)
    {
    	e.preventDefault();
    	$('.toggler').each(function()
    	{
		    $(this).prop('checked', '');
		});
    	$('#uncheck_all').hide();
    	$('#check_all').show();
    });

});
</script>

</body>

</html>