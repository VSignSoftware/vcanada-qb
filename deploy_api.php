<?php
// Report all PHP errors
//error_reporting(-1);

// Same as error_reporting(E_ALL);
//ini_set('error_reporting', E_ALL);

// ssh u/p
require_once('../library/sshinfo.php');

// hash (last changed on 7-31-2016)
$valid_hash = '5733a10ab1c538dba297460ad1d9b9af8f6a0edb62aac8a4aa04154122f47d33';
$valid_v=array(1,2);
if(isset($_REQUEST['k']))
{
	$key = $_REQUEST['k'];
	if($key==$valid_hash)
	{
		$v=$_REQUEST['v'];
		if(!in_array($v,$valid_v))
		{
			die('Invalid request. '.__LINE__);
		}
		else
		{
			if($v==1)
			{
				// get from demo 2
				$class_files_url = "https://demo.vsignsoftware.com/deployments/class_library.zip"; 
				$site_files_url = "https://demo.vsignsoftware.com/deployments/site.zip"; 
				//$scp_cmd = 'scp root@demo.vsignsoftware.com:';
			}
			if($v==2)
			{
				// get from isa
				$class_files_url = "https://isa.vujade.net/deployments/class_library.zip"; 
				$site_files_url = "https://isa.vujade.net/deployments/site.zip"; 
				//$scp_cmd = 'scp root@isa.vujade.net:';
			}

			// scp your_username@remotehost.edu:foobar.txt /local/dir
			// php sec library 
			set_include_path(get_include_path() . PATH_SEPARATOR . 'phpseclibrary');
			include('Net/SSH2.php');
			$ssh = new Net_SSH2('localhost'); 
			if(!$ssh->login($u, $p)) 
			{ 
			    exit('Login Failed');
			}

			// chmod the folders
			$chmod1 = $ssh->exec('chmod 0777 /var/www/zips');
			$chmod2 = $ssh->exec('chmod 0777 /var/www/zips/cl');
			$chmod3 = $ssh->exec('chmod 0777 /var/www/zips/site');

			// test copy 1 file
			//$r = $ssh->exec($scp_cmd.'test.php /var/www');
			//print $r;
			//$pwd = $ssh->exec('pwd');
			//print $pwd;
			// $ssh->exec('rm /var/library/'.$l);

			$ch = curl_init();
			$source = $class_files_url;
			//curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_URL, $source);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

			$data = curl_exec ($ch);
			curl_close ($ch);

			$destination1 = "/var/www/zips/cl_deploy.zip";
			$file = fopen($destination1, "w");
			fputs($file, $data);
			fclose($file);

			$ch = curl_init();
			$source = $site_files_url;
			curl_setopt($ch, CURLOPT_URL, $source);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			$data = curl_exec ($ch);
			curl_close ($ch);

			//var_dump($data);
			//print '<br>';
			//var_dump($ch);
			//print '<br>';

			$destination2 = "/var/www/zips/site_deploy.zip";
			$file = fopen($destination2, "w");
			fputs($file, $data);
			fclose($file);

			// extract file content 
			$path = '/var/www/zips/cl/';

			$zip = new ZipArchive;
			$res = $zip->open($destination1);
			$status = $zip->status;
			//print getcwd().'<br>';
			//print 'Status: '.$status.'<br>';
			if($res === TRUE) 
			{
				$zip->extractTo($path);

				$status = $zip->status;
				//print 'Status: '.$status.'<br>';

				$zip->close();

				//unlink($destination1);		
				print 'Class Library Updated.<br>';
			} 
			else 
			{
				$status = $zip->status;
				//print 'Status: '.$status.'<br>';
				echo "Couldn't open $destination1<br>";
			}

			// extract file content 
			$path2 = "/var/www/zips/site/";

			$zip2 = new ZipArchive;
			$res2 = $zip2->open($destination2);

			if ($res2 === TRUE) 
			{
				$zip2->extractTo($path2);
				$zip2->close();

				//unlink($destination2);	
				print 'Site Files Updated.<br>';
			} 
			else 
			{
				echo "Couldn't open $destination2<br>";
			}

			// move the files
			$lf = scandir($path);
			foreach($lf as $l)
			{
				if( ($l!='.') && ($l!='..') && (!is_dir($l)) )
				{
					// delete the old file
					$ssh->exec('rm /var/library/'.$l);

					// move the new file to the old location
					$ssh->exec('mv '.$path.$l.' /var/library/'.$l);
				}
			}

			$sf = scandir($path2);
			foreach($sf as $l)
			{
				if( ($l!='.') && ($l!='..') && (!is_dir($l)) )
				{

					// delete the old file
					$ssh->exec('rm /var/www/'.$l);

					// move the new file to the old location
					$ssh->exec('mv '.$path2.$l.' /var/www/'.$l);
				}
			}

			// unlink the zip files
			unlink('/var/www/zips/cl_deploy.zip');
			unlink('/var/www/zips/site_deploy.zip');
			
			// change permissions back 
			$chmod1 = $ssh->exec('chmod 0755 /var/www/zips');
			$chmod2 = $ssh->exec('chmod 0755 /var/www/zips/cl');
			$chmod3 = $ssh->exec('chmod 0755 /var/www/zips/site');

			$ssh->disconnect();
		}
	}
	else
	{
		die('Invalid request. '.__LINE__);
	}
}
else
{
	die('Invalid request. '.__LINE__);
}
?>