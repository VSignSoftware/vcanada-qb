<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$documents_permissions = $vujade->get_permission($_SESSION['user_id'],'Documents');
if($documents_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	if($documents_permissions['delete']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}
	# delete the file
	$file_id = $_REQUEST['file_id'];
	if(ctype_digit($file_id))
	{
		$filedata = $vujade->get_document($file_id);
		@unlink('uploads/documents/'.$id.'/'.$filedata['file_name']);
		$s[] = $vujade->delete_row('documents',$file_id);
		$folder_id=$filedata['folder_id'];
		if($folder_id!=0)
		{
			# if no files exist in the folder any longer, delete the folder also
			$files_in_folder = $vujade->get_files_in_folder($folder_id,$id,'documents');
			if($files_in_folder['error']!="0")
			{
				$s[] = $vujade->delete_row('document_file_folders',$folder_id);
			}
		}
	}
}
# delete the folder and all files in it
if($action==2)
{
	if($documents_permissions['delete']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}
	$folder_id = $_REQUEST['folder_id'];
	$file_ids = array();
	# get all files in the folder and delete
	$files_in_folder = $vujade->get_files_in_folder($folder_id,$id,'documents');
	if($files_in_folder['error']=="0")
	{
		foreach($files_in_folder as $fif)
		{
			$file_ids[]=$fif['database_id'];
		}
		foreach($file_ids as $fid)
		{
			$filedata = $vujade->get_production_file($fid);
			@unlink('uploads/documents/'.$id.'/'.$filedata['file_name']);
			$s[] = $vujade->delete_row('documents',$fid);
		}
	}
	# delete the folder
	$s[] = $vujade->delete_row('document_file_folders',$folder_id);
}
$shop_order = $vujade->get_shop_order($id, 'project_id');
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 10;
$nav = 3;
$title = "Documents - ";
require_once('tray_header.php');
?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">
<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">
<!-- begin: .tray-left -->
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left -->
<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;">
<div class="pl15 pr15" style = "width:100%;">
<?php require_once('project_right_tray.php'); ?>
<!-- main content for this page -->
<div class = "row">
<div class = "col-md-12" style = "">
<div class="panel panel-primary panel-border top">
<div class="panel-heading">
<span class="panel-title">Documents</span>
<div class="widget-menu pull-right">
<?php
if($documents_permissions['create']==1) 
{
	?>
	<a href = "new_document_folder.php?project_id=<?php print $id; ?>" class = "btn-sm btn-primary">Add Document</a> 
<?php } ?>

<!-- download all as zip file -->
<a href = "download_document.php?file_id=1&project_id=<?php print $id; ?>&zip=1" class = "btn-sm btn-primary">Export All</a>

</div>
</div>
<div class="panel-body">
	<style>
		td{
			padding: 2px 5px;
		}
	</style>
<table width = "100%">
				<tr>
					<td colspan = "4">&nbsp;</td>
				</tr>
				<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
					<td width = "60%">Name</td>
					<td width = "">Type</td>
					<td width = "">&nbsp;</td>
					<td width = "">Date/Time</td>
					<td width = "" style = "text-align:right;">&nbsp;</td>
				</tr>
				<?php
				# get all files for this project and display as table row with delete button
				$docs = $vujade->get_documents($id);
				if($docs['error']=="0")
				{
					unset($docs['error']);
					$folders = array();
					foreach($docs as $doc)
					{
						if( (empty($doc['folder_id'])) || ($doc['folder_id']==0) )
						{
							# print out the file details; this one is not in a folder
							print '<tr>';
							// name
							print '<td valign = "top" width = "60%">';
							print '<a class = "d" href = "download_document.php?project_id='.$id.'&file_id='.$doc['database_id'].'">';
							print $doc['file_name'];
							print '</a>';
							print '</td>';
							// type
							print '<td valign = "top">';
							$type_data = $vujade->get_document_type($doc['file_type']);
							print $type_data['name'];
							print '</td>';
							# edit button
							print '<td valign = "top">';
							if($documents_permissions['edit']==1)
							{
								print '<a class = "plus-update" href = "edit_document_type.php?project_id='.$id.'&file_id='.$doc['database_id'].'">Edit</a>';
							}
							print '</td>';
							// date
							print '<td valign = "top">';
							//print $doc['ts'];
							
							$ts = strtotime($doc['ts']);
							//$formatted_ts = date('m/d/Y g:i a',$ts);
							$formatted_ts = date('m/d/Y',$ts);
							print $formatted_ts;
							print '</td>';
	
							// delete
							print '<td style = "text-align:right;" valign = "top">';
							if($documents_permissions['delete']==1)
							{
								print '<a class = "btn btn-xs btn-danger delete-document" href = "#delete-document-form" title = "Delete document" data-href = "project_documents.php?action=1&id=' . $id . '&file_id=' . $doc['database_id'] . '">-</a>';
							}
							print '</td>';
							print '</tr>';
						}
						else
						{
							# put the folder id and file id into separate arrays (to be combined to one array)
							$folders[]=$doc['folder_id'];
						}
					}
					print '<tr><td colspan = "4">&nbsp;</td></tr>';
					$c1 = count($folders);
					if($c1>0)
					{
						$folders = array_unique($folders);
						foreach($folders as $folder)
						{
							# get all files that are in this folder
							$files_in_folder = $vujade->get_files_in_folder($folder,$id,'documents');
							if($files_in_folder['error']=="0")
							{
								$folder_info = $vujade->get_file_folder($folder,'document_file_folders');
								$folder_name=$folder_info['name'];
								?>
								
								<!-- file row header -->
								<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
								<td colspan = "3" ><?php print $folder_name; ?></td>
								<td colspan = "1" style = "text-align:right;">
								<?php
								if($documents_permissions['delete']==1)
								{
									?>
									<a class = "btn btn-xs btn-danger delete-folder" href = "#delete-folder-form" title = "Delete folder" data-href = "<?php print 'project_documents.php?action=2&id=' . $id . '&folder_id=' . $folder_info['database_id']; ?>">-</a>
								<?php } ?>
								</td>
								</tr>
								<?php
								# output each file that is in this folder
								foreach($files_in_folder as $fif)
						    	{
						    		unset($fif['error']);
						    		if(!empty($fif['file_name']))
						    		{
						    			$doc_type = $vujade->get_document_type($fif['file_type']);
						    			?>
						    			<tr>
						    				<td width = "60%" valign = "top"><a class = "d" href = "download_document.php?project_id=<?php print $id; ?>&file_id=<?php print $fif["database_id"]; ?>" style = "margin-left:15px;"><?php print $fif["file_name"];?></a>
						    				</td>
						    				
						    				<td width = "" valign = "top"><?php print $doc_type['name']; ?>
						    				</td>
						    				<?php
						    				print '<td valign = "top" width = "">';
						    				if($documents_permissions['edit']==1)
											{
												print '
												<a class = "plus-update" href = "edit_document_type.php?project_id='.$id.'&file_id='.$doc['database_id'].'">Edit</a>';
											}
											print '</td>';
											?>
						    				<td width = "" style = "" valign = "top">
						    					<?php 
						    					$ts = strtotime($fif['ts']);
												$formatted_ts = date('m/d/Y',$ts);
												print $formatted_ts;
						    					?>
						    				</td>
						    				<td width = "" valign = "top">
						    				<?php
						    				if($documents_permissions['delete']==1)
											{
												?>
						    					<a class = "btn btn-xs btn-danger delete-document" href = "#delete-document-form" title = "Delete document" data-href = "<?php print 'project_documents.php?action=1&id=' . $id . '&file_id=' . $fif['database_id']; ?>">-</a>
						    				<?php } ?>
						    				</td>
						    			</tr>
						    			<?php
						    		}
						    	}
						    }
						    print '<tr><td colspan = "4">&nbsp;</td></tr>';
						}
					}
				}
			?>
			</table>
</div>
</div>
</div>
</div>
</section>
</section>
  </div>
  <div id="delete-document-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Document</h1>
	<p>Are you sure you want to delete this document?</p>
	<p><a id = "delete-document-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
	</div>
	<div id="delete-folder-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Folder</h1>
	<p>Are you sure you want to delete this folder?</p>
	<p><a id = "delete-folder-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
	</div>
  <!-- End: Main -->
  <!-- BEGIN: PAGE SCRIPTS -->
  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {
    "use strict";
    // Init Theme Core    
    Core.init();
    var n = $('#notes').html();
    $("#notes").html($.trim(n));
    $('.delete-document').click(function() {
		$('#delete-document-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-document-form',
		modal: true
	});
	$('.delete-folder').click(function() {
		$('#delete-folder-yes').attr('href', $(this).data('href'));
	}).magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-folder-form',
		modal: true
	});
	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});
	
  });
  </script>
  <!-- END: PAGE SCRIPTS -->
</body>
</html>