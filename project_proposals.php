<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$proposals_permissions = $vujade->get_permission($_SESSION['user_id'],'Proposals');
if($proposals_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$setup = $vujade->get_setup();
$shop_order = $vujade->get_shop_order($id, 'project_id');
$show_estimates=0; // default (project does not have estimates)
$estimates = $vujade->get_estimates_for_project($id);
$estimate_ids = $vujade->get_estimate_ids_for_project($id);

# get selected proposal or get first proposal found
$show_proposals=0;

if(isset($_REQUEST['proposalid']))
{
	$firstproposal = $vujade->get_proposal($_REQUEST['proposalid']);
	$proposalid = $_REQUEST['proposalid'];
	$proposals = $vujade->get_proposals_for_project($id);
	unset($proposals['error']);
	unset($proposals['next_id']);
	$show_proposals=1;
}
else
{
	$proposals = $vujade->get_proposals_for_project($id);
	if($proposals['error']=='0')
	{
		unset($proposals['error']);
		unset($proposals['next_id']);

		//print_r($proposals);
		$show_proposals=1; // has at least one proposal
		$proposalsids = array();
		foreach($proposals as $proposal)
		{
			$proposalsids[]=$proposal['database_id'];
			//print $proposal['database_id'].'<br>';
		}
		end($proposalsids);
		$fpid=key($proposalsids);
		$firstproposal = $vujade->get_proposal($proposalsids[$fpid]);
		$proposalid = $proposalsids[$fpid];
		//print 'proposal id: '.$proposalid.'<br>';
	}
	else
	{
		$show_proposals=0;
		$firstproposal['error']=1;
	}	
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# delete proposal
if($action==1)
{
	$proposalid = $_REQUEST['proposalid'];

	# get proposal info 
	$proposal = $vujade->get_proposal($proposalid);

	if($proposal['error']==0)
	{
		# delete proposal designs
		$s1 = $vujade->delete_row('proposal_drawings',$proposal['proposal_id'],1,'proposal_id');
		# delete proposal items
		$s2 = $vujade->delete_row('proposal_items',$proposal['proposal_id'],1,'proposal_id');
	}
	else
	{
		print '<strong>System Error</strong><br>';
		print 'Message: '.$proposal['error'];
		print '<br>Please contact the system administrator and report this error.';
		die;
	}

	# delete the proposal
	$s3 = $vujade->delete_row('proposals',$proposalid);

	# get the proposals and the first proposal
	$proposals = $vujade->get_proposals_for_project($id);
	//print_r($proposals).'<br>';
	if($proposals['error']=='0')
	{
		unset($proposals['error']);
		unset($proposals['next_id']);
		$show_proposals=1; // has at least one proposal
		$proposalsids = array();
		foreach($proposals as $proposal)
		{
			$proposalsids[]=$proposal['database_id'];
		}
		$proposalsids=array_unique($proposalsids);
		$firstproposal = $vujade->get_proposal($proposalsids[0]);
		$proposalid = $proposalsids[0];
		//print '<hr>';
		//print_r($firstproposal);
	}
	else
	{
		unset($proposal);
		unset($proposalid);
		unset($firstproposal);
		$show_proposals=0;
		$firstproposal['error']=1;
	}
	//unset($proposalid);
}

$setup = $vujade->get_setup(1);
if($setup['proposal_email_message']=="")
{
	$default_msg = "Please find attached Proposal ".$proposalid." for your review and approval.";
}
else
{
	$default_msg = $setup['proposal_email_message'];
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$menu = 5;
$nav = 3;
$title = "Proposals - ";
//$charset="ISO-8859-1";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">

<!-- begin: .tray-left -->
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left -->

<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;">

<div class="pl15 pr15" style = "width:100%;">

<?php require_once('project_right_tray.php'); ?>

<!-- main content for this page -->
<div class = "row">
<div class = "col-md-4">
<div class="panel panel-primary panel-border top">
<div class="panel-heading">
<span class="panel-title">Proposals</span>
<div class="widget-menu pull-right">
<?php
if($project['status']!="Closed")
{
	if($proposals_permissions['create']==1)
	{
	?>
		<a class = "btn btn-primary btn-sm" href = "new_proposal.php?project_id=<?php print $id; ?>">New</a>
	<?php } ?>
<?php } ?>
</div>
</div>
<div class="panel-body">
<?php
if($show_proposals==1)
{
	print '<table>';
	unset($proposals['error']);
	foreach($proposals as $proposal)
	{
		if(!empty($proposal['proposal_id']))
		{

			if($proposalid==$proposal['database_id'])
			{
				$bgcolor = "cecece";
			}
			else
			{
				$bgcolor = "ffffff";
			}
			print '<tr bgcolor = "'.$bgcolor.'">';
			$color = "#f7584c";

			print '<td valign = "top">';
			print '<a class = "linknostyle" href = "project_proposals.php?id='.$id.'&proposalid='.$proposal['database_id'].'">';
			print '<font color = "'.$color.'">'.$proposal['proposal_date'].'</font>';
			print '</a>';
			print '</td>';
			print '<td valign = "top">&nbsp;</td>';
			print '<td valign = "top">';
			print '<a class = "linknostyle" href = "project_proposals.php?id='.$id.'&proposalid='.$proposal['database_id'].'">';
			print $proposal['proposal_id'];
			print '</a>';
			print '</td>';
			print '</tr>';
			
		}
	}
	print '</table>';
}
?>
</div>
</div>
</div>

<div class = "col-md-8" style = "border:0px solid red;">
<?php
if($show_proposals==1)
{
?>
	<div class="panel panel-primary panel-border top">
	<div class="panel-heading">
	<span class="panel-title">Selected Proposal</span>
	<div class="widget-menu pull-right">

<?php
if($show_proposals==1)
{
	if($project['status']!="Closed")
	{
		if($proposals_permissions['edit']==1)
		{
	?>
		<a class = "btn btn-xs btn-success" href = "edit_proposal.php?project_id=<?php print $id; ?>&proposalid=<?php print $proposalid; ?>" title = "Edit Proposal">Edit</a>
<?php } ?>

<?php } ?>

<a href = "#email-proposal-form" class = "btn btn-primary btn-xs" id = "email-proposal" style = "">Email</a>

	<!-- modal content -->
	<div id = "email-proposal-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:450px;">
		<form id = "email-form">
		<table class = "" style = "">
		<tr class = "">
		<td class = "">Send To</td>
		<td class = "">
		<input type = "text" name = "send_to" value = "<?php print $project['project_contact_email']; ?>" id = "send_to" style = "width:300px;margin-left:5px;" class = "form-control">
		</td>
		</tr>

		<tr id = "error_1" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-bottom:0; margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "">Subject</td>
		<td class = "">
		<input type = "text" name = "subject" id = "subject" value = "<?php print $id; ?> Proposal" style = "width:300px;margin-left:5px;" class = "form-control">
		</td>
		</tr>

		<tr id = "error_2" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-bottom:0;margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "" valign="top"><span style = "margin-top:5px;">Message</span></td>
		<td class = "">
		<textarea name = "msg" id = "msg" style = "width:300px;height:100px;margin-left:5px;" class = "form-control">
		<?php print $default_msg; ?>
		</textarea>
		</td>
		</tr>

		<tr id = "error_3" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-bottom:0;margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "">Design</td>
		<td class = "">
		<select name = "design" id = "design" class="form-control" style="margin-left: 5px; width: 300px;">
			<option value = "">-Select-</option>
			<?php
			// designs for this project
			$designs = $vujade->get_designs_for_project($id);
			if($designs['error']=="0")
			{
				unset($designs['error']);
				unset($designs['next_id']);
				foreach($designs as $design)
				{
					print '<option value = "'.$design['database_id'].'">'.$design['design_id'].'</option>';
				}
			}
			?>
		</select>
		</td>
		</tr>

		<tr id = "error_2" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-danger" style = "margin-bottom:0;margin-left:5px;margin-top:5px;">This field cannot be empty.</div></td></tr>

		<tr id = "success" style = "display:none;"><td>&nbsp;</td><td style = ""><div class = "alert alert-success" style = "margin-bottom:0;margin-left:5px;margin-top:5px;"></div></td></tr>

		<tr><td>&nbsp;</td><td>&nbsp;</td></tr>

		<tr class = "">
		<td class = "">&nbsp;</td>
		<td class = "">
		<div id = "working"></div>

		<a id = "send" class = "btn btn-lg btn-success">SEND</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">CANCEL</a>
		</td>
		</tr>
		</table>
		</form>
	</div>
<?php } ?>

<a class = "btn btn-xs btn-primary" href = "print_proposal.php?id=<?php print $id; ?>&proposalid=<?php print $proposalid; ?>" target = "_blank" title = "Print Proposal">Print</a> 

<?php
if($project['status']!="Closed")
{
	if($proposals_permissions['delete']==1)
	{
?>

	<a class = "btn btn-xs btn-danger" href = "#delete-proposal-form" title = "Delete Proposal" id = "delete-proposal">Delete</a> 

	<!-- modal content -->
	<div id="delete-proposal-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
	<h1>Delete Proposal</h1>
	<p>Are you sure you want to delete this proposal?</p>
	<p><a id = "" class="btn btn-lg btn-danger" href="project_proposals.php?id=<?php print $id; ?>&proposalid=<?php print $proposalid; ?>&action=1">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
	</div>

<?php } ?>
<?php } ?>
<?php
}
?>

</div>
</div>
<div class="panel-body">

<?php
if( ($firstproposal['error']=='0') || ($firstproposal['error']=='') )
{

	// set tax rate for project to 1 for canadian only servers
	if($setup['country']=="Canada")
	{
		$firstproposal['tax_rate']=1;
	}
?>
		<div style = "padding:5px;">
		<table width = "100%">
			<tr>
				<td valign = "top" width = "25%">Proposal #:
				</td>
				<td valign = "top"><strong>
				<?php print $firstproposal['proposal_id']; ?></strong>
				</td>

				<td valign = "top">Date:
				</td>

				<td valign = "top"><strong><?php print $firstproposal['proposal_date']; ?></strong>
				</td>

			</tr>

			<tr>
				<td colspan = "4">&nbsp; 
				</td>
			</tr>

			<tr>
				<td valign = "top">Expiration Date:
				</td>
				<td valign = "top"><strong>
				<?php 
				print $firstproposal['expiration_date'];
				?></strong>
				</td>

				<td valign = "top">Drawing #:
				</td>

				<td valign = "top"><strong><?php print $firstproposal['drawing_numbers']; ?></strong>
				</td>

			</tr>

			<tr>
				<td colspan = "4">&nbsp; 
				</td>
			</tr>
		</table>

		<div class="panel" style = "margin-top:15px;">
		  <div class="panel-heading">
		  	Description/Amount
		  </div>
		  <div class="panel-body">
	  		<div class="row">
	          <div class="col-md-12">
	            <table class = "table" style = "table-layout: fixed;border:0px solid red;">
					<?php
					# get list of files associated with this design
					$items = $vujade->get_items_for_proposal($firstproposal['proposal_id']);

					// tax rate for work location
					$pcity = $project['city'];
					$pstate = $project['state'];
					if((!empty($pcity)) && (!empty($pstate)) )
					{
						$tax_data = $vujade->get_tax_for_city($pcity,$pstate);
					}

					if($items['error']=="0")
					{
						$tax_total=0;
						$tax_line=0;
						$subtotal = 0;
						unset($items['error']);
						//print '<table width = "100%">';
						foreach($items as $item)
						{
							//print '<tr><td colspan = "2">';
							//print $firstproposal['tax_rate'];
							//print '</td></tr>';
							if(!empty($item['tax_label_rate']))
	                     	{
	                     		$tax_line=$item['amount']*$item['tax_label_rate']*$firstproposal['tax_rate'];
	                     		//print @number_format($tax_line,2);
	                     	}
	                     	else
	                     	{
	                     		//print '0.00';
	                     	}
	                     	$tax_total+=$tax_line;
	                     	$tax_line=0;

							$subtotal+=$item['amount'];
							print '<tr>';
							print '<td style = "word-wrap:normal;width:85%;overflow-x: auto;">';
							$item['item'] = str_replace("&nbsp;", '&nbsp; ', $item['item']);
							print $item['item'];
							print '</td>';
							print '<td><span class = "pull-right">';
							if(!empty($item['amount']))
							{
								print '$'.@number_format($item['amount'],2,'.',',');
							}
							print '</span></td>';
							print '</tr>';
						}
					}
					?>

				</table>
	          </div>
	        <?php
	        if($firstproposal['tax_total']>0)
	        {
	        	$tax_total=$firstproposal['tax_total'];
	        }
	        $total = $subtotal + $tax_total;

			print '<table width = "100%" style = "margin-left:10px;margin-top:10px;">';
			print '<tr>';
			print '<td valign = "top">';
			print 'Subtotal: ';
			print '</td>';
			print '<td valign = "top">$';
			print @number_format($subtotal,2,'.',',');
			print '</td>';
			print '<td valign = "top">';
			print 'Tax: ';
			print '</td>';
			print '<td valign = "top">$';
			//print @number_format($firstproposal['tax_total'],2,'.',',');
			print @number_format($tax_total,2,'.',',');
			print '</td>';
			print '<td valign = "top">';
			print 'Total: ';
			print '</td>';
			print '<td valign = "top">$'; 
			print @number_format($total,2,'.',',');
			print '</td>';
			print '</tr>';
			if(empty($firstproposal['deposit_rate']))
			{
				$deposit_rate=$setup['advanced_deposit'];
				$deposit_amount = $total*$deposit_rate;
			}
			else
			{
				$deposit_rate=$firstproposal['deposit_rate'];
				$deposit_amount = $total*$deposit_rate;
			}
			$deposit_rate=$deposit_rate*100;
			print '<tr>';
			print '<td colspan = "4">&nbsp;';
			print '</td>';
			print '<td>'.$deposit_rate.'%:</td>';
			print '</td>';
			print '<td>$';
			print @number_format($deposit_amount,2,'.',',');
			print '</td>';
			print '</tr>';
			print '</table>';
	          ?>
	        </div>
		  </div>
		</div>
		</div>
		<?php
	}
?>
		</div>

</div>
</div>
</div>

</section>
<!-- End: Content -->

</section>

  </div>
  <!-- End: Main -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  var proposalid = "<?php print $proposalid; ?>";
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    $('#email-proposal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#email-proposal-form',
		modal: true
	});

	$('#delete-proposal').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete-proposal-form',
		modal: true
	});

	$(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	var msg = $('#msg').html();

    $("#msg").html($.trim(msg));

    $('#send').click(function(e){

    	e.preventDefault();

		// send to, subject and message must not be blank

		var send_to = $('#send_to').val();
		var subject = $('#subject').val();
		var message = $('#msg').val();
		var design = $('#design').val();
		var error = 0;

		if(send_to==""){
			$('#error_1').show();
			error++;
		}

		else{
			$('#error_1').hide();
		}

		if(subject==""){
			$('#error_2').show();
			error++;
		}
		else{
			$('#error_2').hide();
		}

		if(message==""){
			$('#error_3').show();
			error++;
		}
		else{
			$('#error_3').hide();
		}
		if(error==0){
			//$('#send').hide();
			$('#working').html('Working...').css({'margin-bottom': '5px'});

			$.post( "jq.send.php", { id: <?php print $proposalid; ?>, type: "proposal", send_to: send_to, subject: subject, message: message, design: design })

			.done(function( response ){
			    $('#working').css('background-color','#36D86C');
			    $('#working').html('');
			    $('#working').html(response);
			    $.magnificPopup.close();
			});
		}
	});

});
</script>
</body>
</html>