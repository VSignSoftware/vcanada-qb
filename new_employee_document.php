<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// employee id
$id = $_REQUEST['employee_id'];
$employee = $vujade->get_employee($id);

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
# 
if($action==1)
{
    
}

$user = $vujade->get_employee($_SESSION['user_id']);
$emp=$user;
$section=9;
$title = "New Employee Document - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">New Employee Document</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                	<div style = "padding:5px;width:100%;">
					<link rel="stylesheet" href="vendor/plugins/dropzone/css/dropzone.css">	
					<script src="vendor/plugins/dropzone/dropzone.min.js"></script>	

					<div style = "width:100%;margin-bottom:15px;height:375px;">
						<form action="upload.php?type=4&employee_id=<?php print $id; ?>" class="dropzone dropzone-sm"></form>
					</div>

					<div align = "right">
						<a href = "employee.php?id=<?php print $id; ?>&tab=5" class = "btn btn-primary" style = "width:150px;">DONE</a>
					</div>
                  
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();
   
});
</script>

</body>
</html>