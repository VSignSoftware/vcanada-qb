<?php 
session_start();
error_reporting(0);
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// setup
error_reporting(0);
$setup=$vujade->get_setup();

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');

if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if(!isset($_REQUEST['nav']))
{
	$nav=1;
}
else
{
	$nav=$_REQUEST['nav'];
}
if(!in_array($nav, array(1,2,3)))
{
	$nav=1;
}
if($nav==1)
{
	$title = "Accounting - ";
}
if($nav==2)
{
	$title = "Time Cards - ";
}
if($nav==3)
{
	$title = "Admin - ";
}

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}
if($action==1)
{
	error_reporting(0);
	require_once('/var/www/qb-bridge/qbbridgeimportfunctions.php');
	_queueImports();
}
if($action==2)
{
	// time card updater
	// $vujade->update_timecards();
	// $vujade->page_redirect('');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray /*tray-left*/ tray320 p20" id = "left_tray">

        	<div id = "menu_1">
        		<b>Accounting</b><br>	

	        	<a href = "purchase_orders.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Purchase Orders</a>
	          	<br>

	          	<?php
	          	if($setup['country']!='Canada')
	          	{
	          	?>

		          	<a href = "taxes.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">City Sales Tax</a>
		          	<br>

		          	<a href = "group_taxes.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Group Sales Tax</a>
		          	<br>

	          	<?php } ?>

	          	<a href = "#" id = "time_cards" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Time Cards</a>
	          	<br>

	          	<a href = "employees.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Employees</a>
	          	<br>

	          	<?php
				$pcount = 0;
				$pcount+=$accounting_permissions['read'];
				$pcount+=$accounting_permissions['edit'];
				$pcount+=$accounting_permissions['delete'];

				$pcount+=$er_permissions['read'];
				$pcount+=$er_permissions['edit'];
				$pcount+=$er_permissions['delete'];

				//print $pcount;
				if($setup['is_qb']==1)
				{
					?>
					<a href = "accounting.php?action=1" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Quickbooks Sync</a>
	          		<br>
	          		<?php
				}
				?>

				<hr>

				<b>System Settings</b><br>	
				<a href = "site_setup.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Site Setup</a>
	          	<br>

	          	<?php
	          	if($pcount==6)
				{
					?>
					<a href = "log.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">User Access Log</a>
	          		<br>
	          		<?php
				}
				?>
	          	<br>
			</div>

			<div id = "menu_2" style = "display:none;">
				<a class = "glyphicons glyphicons-left_arrow" href = "#" id = "back" style = "margin-bottom:10px;"></a>
				<br>
				<a href = "print_time_cards.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Print Time Cards</a>
	          	<br>

	          	<a href = "enter_time.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Enter Time</a>
	          	<br>

	          	<a href = "payroll_summary.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Payroll Summary</a>
	          	<br>

	          	<a href = "payroll_hourly_report.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Hourly Report</a>
	          	<br>

	          	<a href = "payroll_labor_sort_report.php" class = "btn btn-lg btn-primary" style = "width:250px;margin-bottom:15px;">Labor Report</a>
	          	<br>
			</div>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<div class = "alert alert-success" style = "display:none;" id = "timecards-update-success">Time Cards Updated.
            	</div>

            	<!-- main content for this page 
				<div class = "well">
					
				</div>
				-->

            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // click time cards reloads the 
    $('#time_cards').click(function(e)
    {
    	e.preventDefault();

    	// run time cards updater
    	$.post( "jq.update_timecards.php", { action: 1 })
		  .done(function( r ) 
		  {
		  	 if(r==1)
		  	 {
		  	 	$('#timecards-update-success').show();
		  	 	$('#menu_1').hide();
    			$('#menu_2').show();
    			$('#menu_2').addClass('animated fadeIn');
		  	 }
		  });
    });

    $('#back').click(function(e)
    {
    	e.preventDefault();
    	$('#timecards-update-success').hide();
    	$('#menu_1').show();
    	$('#menu_2').hide();
    	$('#menu_1').addClass('animated fadeIn');
    });
		
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

  <?php $vujade->script_stop_time(); ?>

</body>

</html>
