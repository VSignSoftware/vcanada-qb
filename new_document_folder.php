<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
# permissions
$documents_permissions = $vujade->get_permission($_SESSION['user_id'],'Documents');
if($documents_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$id = $_REQUEST['project_id'];
$project_id = $id;
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# 
if($action==1)
{
	# new folder or existing folder, new type or existing type
	$new_folder = $_POST['new_folder'];
	$existing_folder = $_POST['existing_folder'];
	$new_type = $_POST['new_type'];
	$existing_type = $_POST['existing_type'];
	# new folder was typed in 
	# (existing folder is ignored even if something is selected)
	if(!empty($new_folder))
	{
		# create the new folder
		$vujade->create_row('document_file_folders');
		$row_id = $vujade->row_id;
		$folder_id = $row_id;
		$s[]=$vujade->update_row('document_file_folders',$row_id,'name',$new_folder);
		$s[]=$vujade->update_row('document_file_folders',$row_id,'project_id',$project_id);
	}
	# new folder was left blank; existing folder was selected
	if( (empty($new_folder)) && (!empty($existing_folder)) )
	{
		$folder_id=$existing_folder;
	}
	# no folder was selected
	if( (empty($new_folder)) && (empty($existing_folder)) )
	{
		//$vujade->errors[]="No folder selected.";	
	}
	# new type was entered 
	if(!empty($new_type))
	{
		# create the new type
		$vujade->create_row('document_types');
		$row_id = $vujade->row_id;
		$type_id = $row_id;
		$s[]=$vujade->update_row('document_types',$row_id,'name',$new_type);
		$s[]=$vujade->update_row('document_types',$row_id,'onetime',1);
	}
	
	# existing type was entered
	if( (empty($new_type)) && (!empty($existing_type)) )
	{
		$type_id=$existing_type;
	}
	# no type was entered
	if( (empty($new_type)) && (empty($existing_type)) )
	{
		$vujade->errors[]="No document type was selected or entered.";	
	}
	# check for errors
	$e = $vujade->get_error_count();
	if($e<=0)
	{
		# redirect
		$vujade->page_redirect('new_document.php?project_id='.$project_id.'&folder_id='.$folder_id.'&type_id='.$type_id);
	}
}
# skip
if($action==2)
{
	$vujade->page_redirect('new_document.php?folder_id=0&project_id='.$project_id.'&type_id=0');
}
$nav = 3;
$title = "Document Folders - ";
require_once('h.php');
?>
 <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
		<header id="topbar">
            <div class="topbar-left">
                <ol class="breadcrumb">
                    <li class="crumb-active">
                        Project #: <?php print $project_id; ?> Project Name: <?php print $project['site']; ?>
                    </li>
                </ol>
            </div>
        </header>
        <section id="content" class="">
            <div class="theme-primary">
                <div class="panel heading-border panel-primary">
                    <div class="panel-body bg-light">
                        <p><em>Please choose a new folder to be created or an existing folder to used. This step is optional. <br>Press the "Skip" button to skip this step. 
						</em></p>	
						<p><em>Specify the document type from the pull down list or enter a new document type in the box.  
						</em></p>	
						<?php
						$vujade->show_errors();
						?>
						<form method = "post" action = "new_document_folder.php" id="np">
						<input type = "hidden" name = "action" id="action" value = "1">
						<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
                            <div class="row">
                                <div style="margin-bottom: 20px; overflow: hidden;">
                                    <div class="col-lg-6 col-sm-6 col-lg-offset-0 col-sm-offset-0">
                                        <div class="row"  style="margin-bottom: 20px;">
                                            <div class="col-lg-4">
                                                <span style="line-height: 39px">New Folder:</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name="new_folder">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Existing Folder:</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <select name="existing_folder" class="form-control">
                                                    <option value = "">-Select-</option>
													<?php
													# get existing folders
													$folders = $vujade->get_document_folders($project_id);
													if($folders['error']=="0")
													{
														unset($folders['error']);
														foreach($folders as $f)
														{
															print '<option value = "'.$f['database_id'].'">'.$f['name'].'</option>';
														}
													}
													?>
                                                </select>
                                            </div>
                                        </div>
                                        
                                    </div>
                                    </div>
                                </div>

                                <div class = "row">
                                	<div class="col-lg-6 col-sm-6 col-lg-offset-0 col-sm-offset-0">
                                        <div class="row" style="margin-bottom: 20px;">
                                            <div class="col-lg-4">
                                                <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">Document Type:</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <select name="existing_type" class="form-control">
                                                    <option value = "">-Select-</option>
													<?php
													# get existing folders
													$types = $vujade->get_document_types();
													if($types['error']=="0")
													{
														unset($types['error']);
														foreach($types as $t)
														{
															print '<option value = "'.$t['database_id'].'">'.$t['name'].'</option>';
														}
													}
													?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <span style="display: block; margin-bottom:5px;  vertical-align: middle; line-height: 39px">One-time Label:</span>
                                            </div>
                                            <div class="col-lg-8">
                                                <input type="text" class="form-control" name = "new_type" id = "new_type">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div style="margin-top:15px;" class="col-lg-12">
                                        <div style = "float:left">
					                        <a href = "#" id = "back" class = "btn btn-lg btn-danger" style = "width:200px;">Skip this step</a>
					                      </div>
					                      <div style = "float:right;">
					                        <a href = "#" id = "done" class = "btn btn-lg btn-primary" style = "width:200px;">Save and Continue</a>
					                      </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </section>
    <!-- jQuery -->
    <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
    <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
    <!-- Theme Javascript -->
    <script src="assets/js/utility/utility.js"></script>
    <script src="assets/js/demo/demo.js"></script>
    <script src="assets/js/main.js"></script>
    <script type="text/javascript">
        jQuery(document).ready(function () {
            "use strict";
            // Init Theme Core
            Core.init();
            $('#back').click(function(e)
			{
				e.preventDefault();
				$('#action').val(2);
				$('#np').submit();
			});
            
            $('#done').click(function (e) {
                e.preventDefault();
                $('#action').val(1);
                $('#np').submit();
            });
        });
    </script>
</body>
</html>