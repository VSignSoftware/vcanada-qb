<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();

$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');

if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$files_permissions = $vujade->get_permission($_SESSION['user_id'],'Production Files');
if($files_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$s = array();
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	if($files_permissions['delete']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}

	# delete the file
	$file_id = $_REQUEST['file_id'];
	if(ctype_digit($file_id))
	{
		$filedata = $vujade->get_production_file($file_id);
		unlink('uploads/production_files/'.$id.'/'.$filedata['file_name']);
		$s[] = $vujade->delete_row('production_files',$file_id);
		$folder_id=$filedata['folder_id'];
		if($folder_id!=0)
		{
			# if no files exist in the folder any longer, delete the folder also
			$files_in_folder = $vujade->get_files_in_folder($folder_id,$id);
			if($files_in_folder['error']!="0")
			{
				$s[] = $vujade->delete_row('file_folders',$folder_id);
			}
		}
	}
}

# delete the folder and all files in it
if($action==2)
{
	if($files_permissions['delete']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}

	$folder_id = $_REQUEST['folder_id'];
	$file_ids = array();

	# get all files in the folder and delete
	$files_in_folder = $vujade->get_files_in_folder($folder_id,$id);
	if($files_in_folder['error']=="0")
	{
		foreach($files_in_folder as $fif)
		{
			$file_ids[]=$fif['database_id'];
		}
		foreach($file_ids as $fid)
		{
			$filedata = $vujade->get_production_file($fid);
			unlink('uploads/'.$filedata['file_name']);
			$s[] = $vujade->delete_row('production_files',$fid);
		}
	}

	# delete the folder
	$s[] = $vujade->delete_row('file_folders',$folder_id);
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 9;
$nav = 3;
$title = "Production Files - ";
?>

<?php require_once('tray_header.php'); ?>
<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">

<!-- begin: .tray-left -->
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left -->

<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;">

<div class="pl15 pr15" style = "width:100%;">

<?php require_once('project_right_tray.php'); ?>

<div id="" style = "">

<!-- middle of page -->
<div id="contentBottom" style = "">

<!-- left hand nav list -->
<div style = "">
<div style = "">
<div style = "" class="panel panel-primary panel-border top">
<div class="panel-heading">
<span class = "panel-title">Production Files</span>
<div class = "widget-menu pull-right">
<?php if( ($projects_permissions['create']==1) && ($files_permissions['create']==1) ) : ?>
<a href = "new_production_file_folder.php?project_id=<?php print $id; ?>" class = "upload-new btn-sm btn-primary">Add File</a>
<?php endif; ?>
</div>
</div>
<div class="panel-body">
<style>
td
{
	padding: 2px 5px;
}
</style>
<table width = "100%">
<tr>
<td colspan = "4">&nbsp;</td>
</tr>
<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
<td width = "50%">Name</td>
<td width = "10%">Type</td>
<td width = "35%">Date/Time</td>
<td width = "5%" style = "text-align:right;">&nbsp;</td>
</tr>

<?php
# get all files for this project and display as table row with delete button
$production_files = $vujade->get_production_files($id);

if($production_files['error']=="0")
{
	unset($production_files['error']);
	$folders = array();
	//$pfiles = array();
	foreach($production_files as $pf)
	{
		if( (empty($pf['folder_id'])) || ($pf['folder_id']==0) )
		{
			# print out the file details; this one is not in a folder
			print '<tr>';

			// name
			print '<td>';
			print '<a class = "d" href = "download_production_file.php?project_id='.$id.'&file_id='.$pf['database_id'].'">';
			print $pf['file_name'];
			print '</a>';
			print '</td>';

			// type
			print '<td>';
			print $pf['file_type'];
			print '</td>';

			// date
			print '<td>';
			print $pf['ts'];
			print '</td>';

			// delete
			print '<td style = "text-align:right;">';
			if($files_permissions['delete']==1)
			{
				$rand=mt_rand();
				print '<a href = "#'.$rand.'-delete-file-form" class = "delete-file btn btn-xs btn-danger" id = "'.$pf['database_id'].'">-</a>';

				?>
				<div id="<?php print $rand; ?>-delete-file-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
					<h1>Delete File</h1>
					<p>Are you sure you want to delete this file?</p>
					<p><a id = "delete-file-yes" class="btn btn-lg btn-danger" href="project_production_files.php?file_id=<?php print $pf["database_id"]; ?>&action=1&id=<?php print $id; ?>">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
				</div>
				<?php
			}
			print '</td>';
			print '</tr>';
		}
		else
		{
			# put the folder id and file id into separate arrays (to be combined to one array)
			$folders[]=$pf['folder_id'];
			//$pfiles[]=$pf['database_id'];
		}
	}
	print '<tr><td colspan = "4">&nbsp;</td></tr>';
	$c1 = count($folders);
	if($c1>0)
	{
		$folders = array_unique($folders);
		foreach($folders as $folder)
		{
			# get all files that are in this folder
			$files_in_folder = $vujade->get_files_in_folder($folder,$id);
			if($files_in_folder['error']=="0")
			{
				$folder_info = $vujade->get_file_folder($folder);
				$folder_name=$folder_info['name'];
				?>

				<!-- file row header -->
				<tr style = "font-weight:bold;border-bottom:1px solid gray;padding:5px;">
					<td colspan = "3"><?php print $folder_name; ?></td>
					<td colspan = "1" style = "text-align:right;">
						<?php
						if($files_permissions['delete']==1)
						{
							$rand2 = mt_rand();
							?>
							<a data-href = "project_production_files.php?action=2&id=<?php print $id; ?>&folder_id=<?php print $folder_info['database_id']; ?>" class = "delete-folder btn btn-xs btn-danger" style="text-align:right;" id = "<?php print $folder_info['database_id']; ?>" href="#<?php print $rand2; ?>-delete-folder-form">-</a>

							<div id="<?php print $rand2; ?>-delete-folder-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
								<h1>Delete Folder</h1>
								<p>Are you sure you want to delete this folder?</p>
								<p><a id = "delete-folder-yes" class="btn btn-lg btn-danger" href="">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
							</div>

						<?php } ?>
					</td>
				</tr>

				<?php
				# output each file that is in this folder
				foreach($files_in_folder as $fif)
		    	{
		    		unset($fif['error']);
		    		if(!empty($fif['file_name']))
		    		{
		    			?>
						<tr>
							<td width = "50%"><a class = "d" href = "download_production_file.php?project_id=<?php print $id; ?>&file_id=<?php print $fif["database_id"]; ?>" style = "margin-left:15px;"><?php print $fif["file_name"];?></a></td><td width = "10%"><?php print $fif["file_type"]; ?></td><td width = "35%" style = ""><?php print $fif["ts"]; ?></td><td style="text-align:right" width = "5%">
							<?php
							if($files_permissions['delete']==1)
							{
								$rand3=mt_rand();
							?>
								<a data-href = "project_production_files.php?file_id=<?php print $fif["database_id"]; ?>&action=1&id=<?php print $id; ?>" class = "delete-file btn btn-xs btn-danger" style = "text-align:right" id = "<?php print $fif["database_id"]; ?>" href="#<?php print $rand3; ?>-delete-file-form">-</a> 

								<div id="<?php print $rand3; ?>-delete-file-form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
									<h1>Delete File</h1>
									<p>Are you sure you want to delete this file?</p>
									<p><a id = "delete-file-yes" class="btn btn-lg btn-danger" href="project_production_files.php?file_id=<?php print $fif["database_id"]; ?>&action=1&id=<?php print $id; ?>">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
								</div>

							<?php } ?>
						</td>
						</tr>
					<?php
		    		}
		    	}
		    }
		    print '<tr><td colspan = "4">&nbsp;</td></tr>';
		}
	}
}
?>
</table>
</div>
</div>
</div>

</div>
</div>				
</div>

<div class = "push">
</div>

<div id = "dialog">
</div>
</section>
</section>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(function() {
		// date picker
		$('#date').datepicker();
	});
</script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script>
$(document).ready(function(){
	Core.init();
	$(function() 
	{
		// prevent indents on text areas
		var n = $('#notes').html();
	    $("#notes").html($.trim(n));

	    // delete 1 file clicked

		$('.delete-file').click(function() {
			$('#delete-file-yes').attr('href', $(this).data('href'));
		}).magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#delete-file-form',
			modal: true
		});

		$('.delete-folder').click(function() {
			$('#delete-folder-yes').attr('href', $(this).data('href'));
		}).magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#delete-folder-form',
			modal: true
		});

		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	});
});

</script>
</body>
</html>