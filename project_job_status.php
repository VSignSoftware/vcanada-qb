<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
if($employee['error']!="0")
{
	$vujade->page_redirect('error.php?m=1');
}

// test if salesperson has been assigned
// if has, get sales person employee data
$semployee = $vujade->get_employee_by_name($project['salesperson']);

# determine if a record exists in job status for this project
$js = $vujade->get_job_status($id);
if($js['error']!="0")
{
	$vujade->create_row('job_status');
	$js['error']=0;
	$js['database_id']=$vujade->row_id;
	$s[]=$vujade->update_row('job_status',$js['database_id'],'project_id',$id);
}

# active tab
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
	if(!in_array($tab, array(0,1,2,3,4,5,6,7)))
	{
		$tab=0;
	}
}
else
{
	// determine what tab to go to by status
	// pending
	$tab=0;
	$tcurrent_status = $project['status'];
	$tstatus_number = $vujade->get_status_number($tcurrent_status);
	if($tstatus_number<=6)
	{
		$tab=0;
	}

	if($tstatus_number==7)
	{
		$tab=1;
	}
	if($tstatus_number==8)
	{
		$tab=2;
	}
	if($tstatus_number==9)
	{
		$tab=3;
	}
	if($tstatus_number==10)
	{
		$tab=4;
	}
	if($tstatus_number==11)
	{
		$tab=5;
	}
	if($tstatus_number==12)
	{
		$tab=6;
	}
	if($tstatus_number>12)
	{
		$tab=0;
	}

	// override
	// if the job is active install, check if done dates of previous tabs 
	// are completed; if not show the first tab that does not have a done date
	//$vujade->debug_array($js);
	//die;
	if($tab==5)
	{
		// do in reverse order
		if(empty($js['shipping_done']))
		{
			$tab=4;
		}
		if(empty($js['manufacturing_done']))
		{
			$tab=3;
		}
		if(empty($js['survey_done']))
		{
			$tab=2;
		}
		if(empty($js['city_permits_done']))
		{
			$tab=1;
		}
		if(empty($js['ll_approval_done']))
		{
			$tab=0;
		}
	}
}

$s = array();
$action = 0;
$action2 = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if(isset($_REQUEST['action2']))
{
	$action2 = $_REQUEST['action2'];
}

// permissions check
if($action>0)
{
	if($projects_permissions['create']!=1)
	{
		$vujade->page_redirect('error.php?m=1');
	}
}

// is job pending or active?
$is_pending=0;
if(preg_match('/Pending/',$project['status']))
{
	$is_pending=1;
}

// site administrator account data 
$admin_data = $vujade->get_employee('Administrator',3);

# save the LL approval
if($action==1)
{
	$date = $_POST['ll_approval_date'];

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'ll_approval_done',$date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'turned_in_for_billing',0);
		if(!empty($date))
		{
			$s[]=$vujade->update_row('job_status',$js['database_id'],'turned_in_for_billing',0);
			$s[]=$vujade->update_row('job_status',$js['database_id'],'process_order',0);
		}
		else
		{
			$s[]=$vujade->update_row('job_status',$js['database_id'],'process_order',1);
		}
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');

		$status = "Active Permits";
		// send user status change task
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_task']==1) )
		{
			// task
			$vujade->create_row('tasks');
			$taskid = $vujade->row_id;
			$s[]=$vujade->update_row('tasks',$taskid,'project_id',$id);
			$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',date('m/d/Y'));
			$s[]=$vujade->update_row('tasks',$taskid,'task_message','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'task_subject','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'created_by',$admin_data['employee_id']);
			$ts = time();
			$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);

			// user task
			$vujade->create_row('user_tasks');
			$nid = $vujade->row_id;
			$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
			$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$id);
		} 

		// send user email when status changed
		// removed
		/* */
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_email']==1) && ($semployee['status_change_task']==1) )
		{
			// project data to be included in task sent to email
			$pdata = 'Project #: '.$project['project_id'].'<br>';
			$pdata .= 'Project Name: '.$project['site'].'<br><br>';
			$pdata .= 'Project status changed to '.$status;
			if(!empty($employee['email_reply_to']))
			{
				require_once('phpmailer/PHPMailerAutoload.php');
				try
				{
					$sig = str_replace("\n", "<br>", $semployee['email_signature']);
					$msg= "<br><br><br><br><br>".$sig; 
					$msg.= "<br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
					$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "https://demo.vujade.net/images/email_logo.png"></a></p>';

					$mail = new PHPMailer();

					$mail->isSMTP();
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					$mail->Host = $vujade->smtp_host;
					$mail->Port = 25;
					$mail->SMTPAuth = false;

					$mail->setFrom($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addReplyTo($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addAddress($semployee['email_reply_to'], $semployee['fullname']);
					$mail->Subject = 'Project '.$project['project_id'].' status changed to '.$status;
					$mail->msgHTML($pdata.'<br><br>'.$msg);
					$mail->AltBody = 'This is a plain-text message body';
					$mail->send();
				} 
				catch (phpmailerException $e) 
				{
				  	echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
				  	echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
		}
		
	}
	else
	{
		// job is pending; store in temp table
		
		// does a temp record exist?
		// if it does not, this method creates one
		// this method returns the row id and data for the temp row
		$temp_js_row = $vujade->get_js_temp_row($id);
		//print_r($temp_js_row);

		// update the temp row with the new date
		$row_id=$temp_js_row['id'];
		//print $row_id.' '.$date;
		$res = $vujade->update_js_temp_row($row_id,'ll_approval_date',$date);
	}
	
	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=1;
	}
	else
	{
		$tab=0;
	}
	$js = $vujade->get_job_status($id);
}

# update the city permits
if($action==2)
{
	$date = $_POST['city_permits_date'];
	$permit_fee = $_POST['permit_fee'];
	$acquisition_fee = $_POST['acquisition_fee'];
	$permit_number = $_POST['permit_number'];

	// these can be saved regardless of status
	$s[]=$vujade->update_row('job_status',$js['database_id'],'permit_fee',$permit_fee);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'acquisition_fee',$acquisition_fee);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'permit_number',$permit_number);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'city_permits_done',$date);

		$status = "Active Survey";
		// send user status change task
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_task']==1) && ($semployee['status_change_task']==1) )
		{
			// task
			$vujade->create_row('tasks');
			$taskid = $vujade->row_id;
			$s[]=$vujade->update_row('tasks',$taskid,'project_id',$id);
			$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',date('m/d/Y'));
			$s[]=$vujade->update_row('tasks',$taskid,'task_message','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'task_subject','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'created_by',$admin_data['employee_id']);
			$ts = time();
			$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);

			// user task
			$vujade->create_row('user_tasks');
			$nid = $vujade->row_id;
			$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
			$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$id);
		} 

		// send user email when status changed
		/* removed */
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_email']==1) && ($semployee['status_change_task']==1) )
		{

			// project data to be included in task sent to email
			$pdata = 'Project #: '.$project['project_id'].'<br>';
			$pdata .= 'Project Name: '.$project['site'].'<br><br>';
			$pdata .= 'Project status changed to '.$status;
			if(!empty($employee['email_reply_to']))
			{
				require_once('phpmailer/PHPMailerAutoload.php');
				try
				{
					$sig = str_replace("\n", "<br>", $semployee['email_signature']);
					$msg= "<br><br><br><br><br>".$sig; 
					$msg.= "<br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
					$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "https://demo.vujade.net/images/email_logo.png"></a></p>';

					$mail = new PHPMailer();

					$mail->isSMTP();
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					$mail->Host = $vujade->smtp_host;
					$mail->Port = 25;
					$mail->SMTPAuth = false;

					$mail->setFrom($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addReplyTo($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addAddress($semployee['email_reply_to'], $semployee['fullname']);
					$mail->Subject = 'Project '.$project['project_id'].' status changed to '.$status;
					$mail->msgHTML($pdata.'<br><br>'.$msg);
					$mail->AltBody = 'This is a plain-text message body';
					$mail->send();
				} 
				catch (phpmailerException $e) 
				{
				  	echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
				  	echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
		}
		
	}
	else
	{
		// job is pending; store in temp table
		$temp_js_row = $vujade->get_js_temp_row($id);
		$row_id=$temp_js_row['id'];
		$res = $vujade->update_js_temp_row($row_id,'city_permits_date',$date);
	}

	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=2;
	}
	else
	{
		$tab=1;
	}

	$js = $vujade->get_job_status($id);
}

# update survey
if($action==3)
{
	$done_date = $_POST['survey_date'];
	$start_date = $_POST['survey_date_2'];

	// these can be updated regardless of the project status
	$s[]=$vujade->update_row('job_status',$js['database_id'],'survey_notes',$notes);
	$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'survey_done',$done_date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'survey_date',$start_date);

		$status = "Active Manufacturing";
		// send user status change task
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_task']==1) )
		{
			// task
			$vujade->create_row('tasks');
			$taskid = $vujade->row_id;
			$s[]=$vujade->update_row('tasks',$taskid,'project_id',$id);
			$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',date('m/d/Y'));
			$s[]=$vujade->update_row('tasks',$taskid,'task_message','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'task_subject','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'created_by',$admin_data['employee_id']);
			$ts = time();
			$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);

			// user task
			$vujade->create_row('user_tasks');
			$nid = $vujade->row_id;
			$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
			$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$id);
		} 

		/* removed */
		// send user email when status changed
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_email']==1) && ($semployee['status_change_task']==1) )
		{
			// project data to be included in task sent to email
			$pdata = 'Project #: '.$project['project_id'].'<br>';
			$pdata .= 'Project Name: '.$project['site'].'<br><br>';
			$pdata .= 'Project status changed to '.$status;
			if(!empty($employee['email_reply_to']))
			{
				require_once('phpmailer/PHPMailerAutoload.php');
				try
				{
					$sig = str_replace("\n", "<br>", $semployee['email_signature']);
					$msg= "<br><br><br><br><br>".$sig; 
					$msg.= "<br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
					$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "https://demo.vujade.net/images/email_logo.png"></a></p>';

					$mail = new PHPMailer();

					$mail->isSMTP();
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					$mail->Host = $vujade->smtp_host;
					$mail->Port = 25;
					$mail->SMTPAuth = false;

					$mail->setFrom($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addReplyTo($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addAddress($semployee['email_reply_to'], $semployee['fullname']);
					$mail->Subject = 'Project '.$project['project_id'].' status changed to '.$status;
					$mail->msgHTML($pdata.'<br><br>'.$msg);
					$mail->AltBody = 'This is a plain-text message body';
					$mail->send();
				} 
				catch (phpmailerException $e) 
				{
				  	echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
				  	echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
		}

		// calendar event
		# determine if there is an existing survey event
		$event = $vujade->event_exists($id,'survey');
		if($event['exists']=="1")
		{
            // delete if done date is sent
            if(!empty($done_date))
            {
            	$s[]=$vujade->delete_row('calendar',$event['id']);
            }
            else
            {
            	// update
            	$s[]=$vujade->update_row('calendar',$event['id'],'start',$start_date);
	            $s[]=$vujade->update_row('calendar',$event['id'],'end',$start_date);
	            $s[]=$vujade->update_row('calendar',$event['id'],'bottom_border','#FFFFFF');
            }
		}
		else
		{
			# create new calendar event
			if(empty($done_date))
			{
				$vujade->create_row('calendar');
				$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$start_date);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$start_date);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','survey');
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'bottom_border','#FFFFFF');
	        }
		}
	}
	else
	{
		// job is pending; store in temp table
		$temp_js_row = $vujade->get_js_temp_row($id);
		$row_id=$temp_js_row['id'];
		$res1 = $vujade->update_js_temp_row($row_id,'survey_done_date',$date);
		$res2 = $vujade->update_js_temp_row($row_id,'survey_date',$date);
	}

	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=3;
	}
	else
	{
		$tab=2;
	}

	$js = $vujade->get_job_status($id);
}

# update manufacturing
if($action==4)
{
	$date = $_POST['manufacturing_date'];

	// days of manufacturing
	$days_of_manufacturing = $_POST['days_of_manufacturing'];

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'manufacturing_done',$date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');

		if($days_of_manufacturing>0)
		{
			$s[]=$vujade->update_row('job_status',$js['database_id'],'days_of_manufacturing',$days_of_manufacturing);

			// dom table
		}

		$status = "Active Install";
		// send user status change task
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_task']==1) )
		{
			// task
			$vujade->create_row('tasks');
			$taskid = $vujade->row_id;
			$s[]=$vujade->update_row('tasks',$taskid,'project_id',$id);
			$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',date('m/d/Y'));
			$s[]=$vujade->update_row('tasks',$taskid,'task_message','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'task_subject','Project '.$id.' status changed to '.$status);
			$s[]=$vujade->update_row('tasks',$taskid,'created_by',$admin_data['employee_id']);
			$ts = time();
			$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);

			// user task
			$vujade->create_row('user_tasks');
			$nid = $vujade->row_id;
			$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
			$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$semployee['employee_id']);
			$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$id);
		} 

		// send user email when status changed
		if( ($project['salesperson']==$semployee['fullname']) && ($semployee['status_change_email']==1) && ($semployee['status_change_task']==1) )
		{
			// project data to be included in task sent to email
			$pdata = 'Project #: '.$project['project_id'].'<br>';
			$pdata .= 'Project Name: '.$project['site'].'<br><br>';
			$pdata .= 'Project status changed to '.$status;
			if(!empty($employee['email_reply_to']))
			{
				require_once('phpmailer/PHPMailerAutoload.php');
				try
				{
					$sig = str_replace("\n", "<br>", $semployee['email_signature']);
					$msg= "<br><br><br><br><br>".$sig; 
					$msg.= "<br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
					$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "https://demo.vujade.net/images/email_logo.png"></a></p>';

					$mail = new PHPMailer();

					$mail->isSMTP();
					$mail->SMTPDebug = 0;
					$mail->Debugoutput = 'html';
					$mail->Host = $vujade->smtp_host;
					$mail->Port = 25;
					$mail->SMTPAuth = false;

					$mail->setFrom($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addReplyTo($semployee['email_reply_to'], $semployee['fullname']);
					$mail->addAddress($semployee['email_reply_to'], $semployee['fullname']);
					$mail->Subject = 'Project '.$project['project_id'].' status changed to '.$status;
					$mail->msgHTML($pdata.'<br><br>'.$msg);
					$mail->AltBody = 'This is a plain-text message body';
					$mail->send();
				} 
				catch (phpmailerException $e) 
				{
				  	echo $e->errorMessage(); //Pretty error messages from PHPMailer
				} catch (Exception $e) {
				  	echo $e->getMessage(); //Boring error messages from anything else!
				}
			}
		}
		
	}
	else
	{
		// job is pending; store in temp table
		$temp_js_row = $vujade->get_js_temp_row($id);
		$row_id=$temp_js_row['id'];
		$res = $vujade->update_js_temp_row($row_id,'manufacturing_date',$date);
	}

	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=4;
	}
	else
	{
		$tab=3;
	}

	$js = $vujade->get_job_status($id);
}

# update shipping
if($action==5)
{
	$date = $_POST['shipping_date']; // done date
	$date2 = $_POST['shipping_date_2']; // shipped date
	if(!empty($date))
	{
		$date2="";
	}

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'shipping_done',$date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'shipping_date',$date2);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'calendar_start_date',$date2);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');

		# update the calendar
		if(!empty($date2))
		{
			# determine if there is a shipping event
			$event = $vujade->event_exists($id,'shipping');
			if($event['exists']=="1")
			{
				$s[]=$vujade->update_row('calendar',$event['id'],'start',$date2);
	            $s[]=$vujade->update_row('calendar',$event['id'],'end',$date2);
			}
			else
			{
				# create new calendar event
				$vujade->create_row('calendar');
				$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$date2);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$date2);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','shipping');
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
			}
		}
		else
		{
			# the done date was provided; remove the event from the calendar
			$event = $vujade->event_exists($id,'shipping');
			$s[]=$vujade->delete_row('calendar',$event['id']);
		}
	}
	else
	{
		// job is pending; store in temp table
		$temp_js_row = $vujade->get_js_temp_row($id);
		$row_id=$temp_js_row['id'];
		$res = $vujade->update_js_temp_row($row_id,'shipping_done_date',$date);
		$res = $vujade->update_js_temp_row($row_id,'shipping_date',$date2);
	}

	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=5;
	}
	else
	{
		$tab=4;
	}
	$js = $vujade->get_job_status($id);
}

# update installation
if($action==6)
{
	$date = $_POST['installation_date'];
	$date2 = $_POST['installation_date_2']; // this is the start date
	$end_date = $_POST['installation_end_date'];
	if(!empty($date))
	{
		$date2="";
	}

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_done',$date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date',$date2);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_end_date',$end_date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'calendar_start_date',$date2);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date_ts',strtotime($date2));

		# update the calendar
		if(!empty($date2))
		{
			# determine if there is a shipping event
			$event = $vujade->event_exists($id,'installation');
			if($event['exists']=="1")
			{
				$s[]=$vujade->update_row('calendar',$event['id'],'start',$date2);
	            if(!empty($end_date))
	            {
	            	$sd = strtotime($date2);
	            	$ed = strtotime($end_date);
	            	if($ed>$sd)
	            	{
	            		$end = $edNew86400;
	            		$end = date('m/d/Y',$end);
	            		$s[]=$vujade->update_row('calendar',$event['id'],'end',$end);
	            	}
	            	
	            	//$s[]=$vujade->update_row('calendar',$event['id'],'start_hour',"00:00:01");
	            	//$s[]=$vujade->update_row('calendar',$event['id'],'end_hour',"23:59:59");
	            }
	            else
	            {
	            	$s[]=$vujade->update_row('calendar',$event['id'],'end',$date2);
	            }
			}
			else
			{
				# create new calendar event
				$vujade->create_row('calendar');
				$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$date2);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$end_date);
	            if(!empty($end_date))
	            {
	            	//$s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$end_date);
	            	$sd = strtotime($date2);
	            	$ed = strtotime($end_date);
	            	if($ed>$sd)
	            	{
	            		$end = $edNew86400;
	            		$end = date('m/d/Y',$end);
	            		$s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$end);
	            	}
	            }
	            else
	            {
	            	$s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$date2);
	            }

	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','installation');
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
			}
		}
		else
		{
			# the done date was provided; remove the event from the calendar
			$event = $vujade->event_exists($id,'installation');
			$s[]=$vujade->delete_row('calendar',$event['id']);
		}
	}
	else
	{
		// job is pending; store in temp table
		$temp_js_row = $vujade->get_js_temp_row($id);
		$row_id=$temp_js_row['id'];
		$res1 = $vujade->update_js_temp_row($row_id,'install_done_date',$date);
		$res2 = $vujade->update_js_temp_row($row_id,'install_start_date',$date2);
		$res3 = $vujade->update_js_temp_row($row_id,'install_end_date',$end_date);
	}

	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=6;
	}
	else
	{
		$tab=5;
	}
	$js = $vujade->get_job_status($id);
}

# update invoice
if($action==7)
{
	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=8;
	}
	else
	{
		$tab=7;
	}

	$js = $vujade->get_job_status($id);
}

# update service
if($action==8)
{
	$date = $_POST['service_date'];
	$date2 = $_POST['service_date_2'];
	if(!empty($date))
	{
		$date2="";
	}

	// if job is not pending
	if($is_pending==0)
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_done',$date);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_date',$date2);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'calendar_start_date',$date2);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');
		# update the calendar
		if(!empty($date2))
		{
			# determine if there is a shipping event
			$event = $vujade->event_exists($id,'service');
			if($event['exists']=="1")
			{
				$s[]=$vujade->update_row('calendar',$event['id'],'start',$date2);
	            $s[]=$vujade->update_row('calendar',$event['id'],'end',$date2);
			}
			else
			{
				# create new calendar event
				$vujade->create_row('calendar');
				$s[]=$vujade->update_row('calendar',$vujade->row_id,'project_id',$id);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'start',$date2);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'end',$date2);
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'type','service');
	            $s[]=$vujade->update_row('calendar',$vujade->row_id,'is_all_day',1);
			}
		}
		else
		{
			# the done date was provided; remove the event from the calendar
			$event = $vujade->event_exists($id,'service');
			$s[]=$vujade->delete_row('calendar',$event['id']);
		}
	}
	else
	{
		// job is pending; store in temp table
		$temp_js_row = $vujade->get_js_temp_row($id);
		$row_id=$temp_js_row['id'];
		$res1 = $vujade->update_js_temp_row($row_id,'service_done_date',$date);
		$res2 = $vujade->update_js_temp_row($row_id,'service_date',$date2);
	}

	if($action2==1)
	{
		header('location:schedule.php');
		die;
	}

	if($date=="N/A")
	{
		$tab=7;
	}
	else
	{
		$tab=6;
	}
	$js = $vujade->get_job_status($id);
}

// delete a note
if($action==9)
{
	$note_id=$_REQUEST['note_id'];
	$s = $vujade->delete_row('job_status_notes',$note_id);
}

if($action==10)
{
	if($projects_permissions['edit']==1)
	{
		$notes = trim($_POST['notes']);
		$success = $vujade->update_row('projects',$id,'notes',$notes,'project_id');
		if($success==1)
		{
			$vujade->messages[]="Notes updated.";
		}
		$project = $vujade->get_project($id,2);
		if($project['error']!=0)
		{
			$vujade->page_redirect('error.php?m=3');
		}
	}
	else
	{
		$vujade->page_redirect('error.php?m=1');
	}
}

// user checked / unchecked one of the labor types on the manufacturing tab
if($action==11)
{

}

$shop_order = $vujade->get_shop_order($id, 'project_id');
$project = $vujade->get_project($id,2);

// is job pending or active?
$is_pending=0;
if(preg_match('/Pending/',$project['status']))
{
	$is_pending=1;
}

// one time update every time this page loads (project status, status number)
// this only runs if the job status is not pending
if($is_pending==0)
{
	@$s[]=$vujade->update_row('projects',$project['database_id'],'project_status',$project['status']);
	$status_number = $vujade->get_status_number($project['status']);
	@$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',$status_number);
}
else
{
	// get the temporary dates and set them to the js array
	$temp_data = $vujade->get_js_temp_row($id);
	$js['city_permits_done']=$temp_data['city_permits_date'];
	$js['survey_done']=$temp_data['survey_done_date'];
	$js['survey_date']=$temp_data['survey_date'];
	$js['ll_approval_done']=$temp_data['ll_approval_date'];
	$js['manufacturing_done']=$temp_data['manufacturing_date'];
	$js['shipping_done']=$temp_data['shipping_done_date'];
	$js['shipping_date']=$temp_data['shipping_date'];
	$js['installation_done']=$temp_data['install_done_date'];
	$js['installation_date']=$temp_data['install_start_date'];
	$js['installation_end_date']=$temp_data['install_end_date'];
	$js['service_done']=$temp_data['service_done_date'];
	$js['service_date']=$temp_data['service_date'];
}

$start = date('Y-m-d');
$_SESSION['start']=$start;
$_SESSION['view']="month";

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$section=3;
$menu=7;
$title = 'Job Status - ' . $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

<!-- Begin: Content -->
<section id="content" class="table-layout animated fadeIn">

<!-- begin: .tray-left -->
<?php require_once('project_left_tray.php'); ?>
<!-- end: .tray-left -->

<!-- begin: .tray-center -->
<div class="tray tray-center" style = "width:100%;">

<div class="pl15 pr15" style = "width:100%;">

<?php require_once('project_right_tray.php'); ?>

<!-- main content for this page -->
<div style="width: 100%">
<style>
.conversation-date.active,
.contact-name.active{
	background: #e5e5e5;
	text-decoration: none !important;
}
</style>

<!-- tabs -->
<div class="row">
<div class = "col-md-12">
<div class="panel">
<div class="panel-heading">
<ul class="nav panel-tabs-border panel-tabs panel-tabs-left" style="display:block;">
<li class="<?php if($tab==0){ print 'active'; } ?>">
<a href="#tab1" data-toggle="tab">LL Approval</a>
</li>
<li class="<?php if($tab==1){ print 'active'; } ?>">
<a href="#tab2" data-toggle="tab">City Permits</a>
</li>
<li class="<?php if($tab==2){ print 'active'; } ?>">
<a href="#tab3" data-toggle="tab">Survey</a>
</li>
<li class="<?php if($tab==3){ print 'active'; } ?>">
<a href="#tab4" data-toggle="tab">Manuf.</a>
</li>
<li class="<?php if($tab==4){ print 'active'; } ?>">
<a href="#tab5" data-toggle="tab">Shipping</a>
</li>
<li class="<?php if($tab==5){ print 'active'; } ?>">
<a href="#tab6" data-toggle="tab">Install</a>
</li>
<li class="<?php if($tab==6){ print 'active'; } ?>">
<a href="#tab7" data-toggle="tab">Service</a>
</li>
<li class="<?php if($tab==7){ print 'active'; } ?>">
<a href="#tab8" data-toggle="tab">Inv. Notes</a>
</li>
</ul>
</div>
<style>
@media(max-width: 1024px){
	.tab-pane{
		width: 768px;
	}
}
.conversation-body
{
	width:603px;
	word-wrap: break-word;
	border:none;
}
</style>
<div class="panel-body">
<div class="tab-content pn br-n">

	<!-- ll approval -->
	<div id="tab1" class="tab-pane <?php if($tab==0){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form1">
	      	<table>
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input type = "text" style = "margin-left:5px;width:150px;" class = "dp form-control" id = "ll_approval_date" name = "ll_approval_date" value = "<?php print $js['ll_approval_done'];?>">
	      			</td>
	      			<td>&nbsp;
	      			</td>
	      		</tr>
	      	</table>
	      	<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "1">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "0">
							<input type = "submit" value = "SAVE DATE" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-1" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_1">
	      			</td>
	      		</tr>
	      	</table>
	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=1" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,1);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=1" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=0">YES</a>
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- city permits -->
	<div id="tab2" class="tab-pane <?php if($tab==1){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form2">
	      	<table>
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "city_permits_date" name = "city_permits_date" value = "<?php print $js['city_permits_done'];?>">
	      			</td>
	      			<td style = "font-weight:bold;padding-left:5px;">
						Permit Fee: 
					</td>
					<td style = "font-weight:bold;">
						<input type = "text" name = "permit_fee" value = "<?php print $js['permit_fee'];?>" style = "width:75px;" class = "form-control"> 
					</td>
					<td style = "font-weight:bold;padding-left:5px;">
						Acquisition Fee: 
					</td>
					<td>
						<input type = "text" name = "acquisition_fee" value = "<?php print $js['acquisition_fee'];?>" style = "width:75px;" class = "form-control">
					</td>
					<td style = "font-weight:bold;padding-left:5px;"> 
						Permit Number:
					</td>
					<td>
						<input type = "text" name = "permit_number" value = "<?php print $js['permit_number'];?>" style = "width:100px;" class = "form-control"> 
					</td>
	      		</tr>
	      		</table>
	      		<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "2">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "1">
							<input type = "submit" value = "SAVE DATE" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-2" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td colspan = "5">
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_2">
	      			</td>
	      		</tr>
	      	</table>
	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=2" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,2);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=2" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=1">YES</a>
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- survey -->
	<div id="tab3" class="tab-pane <?php if($tab==2){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form3">
	      	<table>
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "survey_date" name = "survey_date" value = "<?php print $js['survey_done'];?>">
	      			</td>

	      			<td style = "padding-left:5px;"><strong>Survey Date: </strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "survey_date_2" name = "survey_date_2" value = "<?php print $js['survey_date'];?>">
	      			</td>

	      			<td>&nbsp;</td>
	      		</tr>
	      	</table>
	      	<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "3">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "2">
							<input type = "submit" value = "SAVE DATE" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-3" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_3">
	      			</td>

	      			<td>&nbsp;</td>

	      			<td>
	      				<input type = "button" id = "ssf" value = "SITE SURVEY FORM" class = "calendar_button btn btn-primary">
	      			</td>

	      		</tr>
	      	</table>
	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=3" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,3);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=3" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=2">YES</a>
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- manufacturing -->
	<div id="tab4" class="tab-pane <?php if($tab==3){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form4">
	      	<table width = "70%">
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "manufacturing_date" name = "manufacturing_date" value = "<?php print $js['manufacturing_done'];?>">
	      			</td>
	      			<td>&nbsp;</td>
	      			<td>&nbsp;</td>
	      			<?php
      				if($is_pending==0)
      				{
      					?>
      					<td><strong>Days of Manufacturing:</strong></td>
		      			<td>&nbsp;</td>
		      			<td>
		      				<input style = "margin-left:5px;width:150px;" type = "number" class = "form-control" id = "days_of_manufacturing" name = "days_of_manufacturing" value = "<?php print $js['days_of_manufacturing'];?>">
		      			</td>
      					<?php	
      				}
      				?>
	      			<td>&nbsp;</td>
	      		</tr>
	      	</table>
	      	<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "4">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "3">
							<input type = "submit" value = "SAVE DATE" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-4" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_4">
	      			</td>
	      		</tr>
	      	</table>

	      </div>
	    </div>

	    <?php
		if($project['status']=="Active Manufacturing")
		{
	    	if($shop_order['estimates']!='')
	    	{
	    		$estimates=$shop_order['estimates'];
	    		$estimates = explode('^',$estimates);
	    		foreach($estimates as $k => $v)
	    		{
	    			if(empty($v))
	    			{
	    				unset($estimates[$k]);
	    			}
	    		}
	    		$est_cnt = count($estimates);
	    	}

	    	if($est_cnt>0)
	    	{
	    		$departments = $vujade->get_labor_types();
	    		unset($departments['error']);

	    		// determine which ones to check
	    		$checked_labor_ids = array();
	    		foreach($estimates as $estimate)
	    		{
	    			//$checked_labor_ids[]=$estimate;
	    			$labor = $vujade->get_labor_for_estimate($estimate);
	    			/*
	    			Array ( [error] => 0 [0] => 
	    				Array ( 
	    					[database_id] => 182522 
	    					[estimate_id] => 140109-01 
	    					[hours] => 1 
	    					[type] => 1 - Field Check 
	    					[rate] => 43 
	    					[labor_id] => 1 
	    					[skipped] => 0 ) 
					*/
					if($labor['error']=="0")
					{
						unset($labor['error']);
						foreach($labor as $l)
						{
							$hours = $l['hours'];
							if( ($hours>0) && ($l['skipped']==0) )
							{
								$checked_labor_ids[]=$l['labor_id'];
							}
						}
					}
					// check the temp estimates time table
					$labor2 = $vujade->get_temp_labor_for_estimate($estimate);
					if($labor2['count']>0)
					{
						unset($labor2['error']);
						unset($labor2['count']);
						foreach($labor2 as $l)
						{
							$checked_labor_ids[]=$l['labor_id'];
						}
					}
				}

	    		print '<div class = "" style = "padding:5px;border:1px solid #30363E;margin-top:15px;margin-bottom:15px;overflow:auto;height:130px;">';

	    		$x=1;
	    		print '<table class = "table" width = "100%">';
		    	foreach($departments as $dept)
		    	{
		    		$ck="";
		    		if($x==1)
		    		{
		    			print '<tr>';
		    		}
		    		if(in_array($dept['database_id'],$checked_labor_ids))
		    		{
		    			$ck="checked";
		    		}
		    		print '<td><input type = "checkbox" class = "cb-labor" id = "" name = "labor[]" data-estimate = "" value = "'.$dept['database_id'].'" '.$ck.'>'.$dept['type'];
		    		$xNewNew;
		    		if($x==4)
		    		{
		    			$x=1;
		    			print '</tr>';
		    		}
		    		$ck='';
		    	}
		    	print '</table>';
				print '</div>';
	    	}
		}
		?>
	    
		<div class = "" id = "laborstr">
			<?php 
			//print_r($checked_labor_ids); 
			?>
		</div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=4" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,4);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=4" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=3">YES</a> 
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- shipping -->
	<div id="tab5" class="tab-pane <?php if($tab==4){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form5">
	      	<table>
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "shipping_date" name = "shipping_date" value = "<?php print $js['shipping_done'];?>">
	      			</td>

	      			<td>&nbsp;</td>

	      			<td><strong>Shipping Date:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "shipping_date_2" name = "shipping_date_2" value = "<?php print $js['shipping_date'];?>">
	      			</td>
	      		</tr>
	      	</table>
	      	<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "5">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "4">
							<input type = "submit" value = "SAVE DATE" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-5" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_5">
	      			</td>
	      		</tr>
	      	</table>

	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=5" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,5);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=5" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=4">YES</a> 
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- install -->
	<div id="tab6" class="tab-pane <?php if($tab==5){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form6">
	      	<table>
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "installation_date" name = "installation_date" value = "<?php print $js['installation_done'];?>">
	      			</td>

	      			<td>&nbsp;</td>

	      			<td><strong>Start Date:</strong>
	      			</td>
	      			<td>

	      				<?php 
	      				if(empty($js['installation_date']))
	      				{ 
	      					$install_start_date = $shop_order['due_date']; 
	      				}
	      				else
	      				{ 
	      					$install_start_date = $js['installation_date'];
	      				}

	      				if($project['status']=="Closed")
	      				{
	      					$install_start_date = '';
	      				}
	      				?>

	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "installation_date_2" name = "installation_date_2" value = "<?php print $install_start_date; ?>">
	      			</td>

	      			<td>&nbsp;
	      			</td>

	      			<td><strong>End Date:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "installation_end_date" name = "installation_end_date" value = "<?php print $js['installation_end_date'];?>">
	      			</td>
	      		</tr>
	      	</table>
	      	<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "6">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "5">
							<input type = "submit" value = "SAVE DATE" id = "form5-save" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-6" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_6">
	      			</td>

	      			<td>&nbsp;</td>

	      			<td>
	      				<input type = "button" id = "bcf" value = "CUSTOMER ACCEPTANCE FORM" class = "calendar_button btn btn-primary">
	      			</td>
	      		</tr>
	      	</table>

	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=6" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,6);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=6" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=5">YES</a> 
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- service -->
	<div id="tab7" class="tab-pane <?php if($tab==6){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form7">
	      	<table>
	      		<tr>
	      			<td><strong>Done:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "service_date" name = "service_date" value = "<?php print $js['service_done'];?>">
	      			</td>

	      			<td>&nbsp;</td>

	      			<td><strong>Service Date:</strong>
	      			</td>
	      			<td>
	      				<input style = "margin-left:5px;width:150px;" type = "text" class = "dp form-control" id = "service_date_2" name = "service_date_2" value = "<?php if(empty($js['service_date'])){ print $shop_order['service_date']; }else{ print $js['service_date'];}?>">
	      			</td>
	      		</tr>
	      	</table>
	      	<table style = "margin-top:15px;">
	      		<tr>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "hidden" name = "action" value = "8">
							<input type = "hidden" name = "id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "tab" value = "0">
							<input type = "submit" value = "SAVE DATE" class = "btn btn-success" style = "">
							</form> 
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<?php
						if($projects_permissions['create']==1)
						{
							?>
							<input type = "submit" value = "N/A" class = "calendar_button btn btn-primary" id = "na-7" style = "margin-left:15px;margin-right:15px;padding-left:5px;padding-right:5px;">
							<?php
						}
						?>
	      			</td>
	      			<td>
	      				<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_7">
	      			</td>

	      			<td>&nbsp;</td>

	      			<td>
	      				<input type = "button" id = "bsc" value = "SERVICE COMPLETION REPORT" class = "calendar_button btn btn-primary">
	      			</td>
	      		</tr>
	      	</table>

	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=7" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,7);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=7" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=6">YES</a> 
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

	<!-- invoice notes -->
	<div id="tab8" class="tab-pane <?php if($tab==7){ print 'active'; } ?>">
	    <div class="row">
	      <div class="col-md-12">
	      <form method = "post" action = "project_job_status.php" id = "form8">
	      	
	      	<input type = "submit" value = "SCHEDULE / CALENDAR" class = "btn btn-primary calendar_button" id = "calendar_btn_8">

	      </form>
	      </div>
	    </div>

	    <!-- notes -->
	    <div class = "row" style = "margin-top:15px;">

	    	<!-- panel left (small) -->
	    	<div class = "col-md-3">
	    		<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				    <span class="panel-title">Notes</span>
				    <div class="widget-menu pull-right">
				    <a href = "new_js_note.php?project_id=<?php print $id; ?>&section=8" class = "btn btn-xs btn-primary plus" style = "">New</a>  
				    </div>
				  </div>
				  <div class="panel-body">
					<?php
					$dates = $vujade->get_job_status_notes($id,8);
					if($dates['error']=="0")
					{
						$show=1;
						unset($dates['error']);
						foreach($dates as $date)
						{
							print '<a class = "contact-name" href = "contact-'.$date['id'].'" id = "'.$date['id'].'">';
							print $date['date'].'</a><br>';
						}
					}
					else
					{
						$show=0;
					}
					?>  
				  </div>
				</div>
	    	</div>

	    	<!-- panel right (wider) -->
	    	<div class = "col-md-9">

	    		<?php
				if($show==1)
				{
					foreach($dates as $date)
					{
						?>
						<div class="panel-contact panel panel-primary panel-border top" id = "contact-<?php print $date['id']; ?>">
						  <div class="panel-heading">
						    <span class="panel-title"><?php print $date['date'].' '.$date['name_display']; ?>
						    </span>
						    <div class="widget-menu pull-right">
						      	<a href = "edit_js_note.php?nid=<?php print $date['id']; ?>&project_id=<?php print $id; ?>&section=8" class = "plus btn btn-xs btn-primary">EDIT</a> 

								<a href = "#delete-note-form-<?= $date['id']; ?>" id = "delete_note" class = "delete-note btn btn-xs btn-danger">DELETE</a>
						    </div>
						  </div>
						  <div class="panel-body conversation-body" id = "cb-<?php print $date['id']; ?>">
						  <?php print $date['body']; ?>
						  </div>
						</div>

						<!-- modal for delete note -->
						<div id="delete-note-form-<?= $date['id']; ?>" class="delete-note-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>Delete Note</h1>
							<p>Are you sure you want to delete this note?</p>
							<p>
								<a id = "" class="btn btn-lg btn-success"
								   href="project_job_status.php?action=9&note_id=<?= $date['id']; ?>&id=<?= $id; ?>&tab=7">YES</a> 
								<a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">NO</a>
							</p>
						</div>

						<?php
					}
				}
				?>
	    	</div>

	    </div>
	</div>

</div>
</div>
</div>
</div>
				
</div>
</div>
</div>
</section>
<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- artwork error modal -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="" id = "date-error">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id = "" style = "padding:10px;">
	
    	<div id = "error-text" class = "alert alert-danger"></div>
    	<br>
    	<a href = "#" id = "close-date-error" class = "btn btn-danger" style = "margin-bottom:10px;">Close</a>

    </div>
  </div>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
	$(function()
	{
		"use strict";

		// Init Theme Core
		Core.init();

		var n = $('#notes').html();
    	$("#notes").html($.trim(n));

		// date picker
		$('.dp').datepicker();

		// check box change in the manufacturing tab
    	$('.cb-labor').change(function()
    	{
    		$('#laborstr').html('');
    		var v = $(this).val();
    		var checked = '';
    		// checked or unchecked
    		if($(this).is(":checked"))
    		{
    			checked=1;
    		}
    		else
    		{
    			checked=0;
    		}

    		$.post( "jq.update_temp_labor.php", { pid: "<?php print $id; ?>", labor_id: v, checked: checked })
			.done(function(r) 
			{
			    if(r!=1)
			    {
			    	$('#laborstr').html(r);
			    }
			});
    	});

		var HEADER = 60; // header height

		// focus on a contact
		$('.contact-name').click(function(e){
			e.preventDefault();

			var contact = $("#contact-"+this.id);

			if( $(this).hasClass('active') ){
				$(this).removeClass('active');
				contact.removeClass('panel-warning');
			} else {
				$('.contact-name').not( $(this) ).removeClass('active');
				$(this).addClass('active');

				$('.panel-contact').not( contact ).removeClass('panel-warning');
				contact.addClass('panel-warning');

				$('html, body').animate({
					scrollTop: contact.offset().top - HEADER
				}, 500);
			}
		});

		$('.delete-note').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '.delete-note-form',
			modal: true
		});

		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});


		$('#na-1').click(function(e)
		{
			e.preventDefault();
			$('#ll_approval_date').val('N/A');
			$('#form1').submit();
		});

		$('#na-2').click(function(e)
		{
			e.preventDefault();
			$('#city_permits_date').val('N/A');
			$('#form2').submit();
		});

		$('#na-3').click(function(e)
		{
			e.preventDefault();
			$('#survey_date').val('N/A');
			$('#survey_date_2').val('N/A');
			$('#form3').submit();
		});

		$('#na-4').click(function(e)
		{
			e.preventDefault();
			$('#manufacturing_date').val('N/A');
			$('#form4').submit();
		});

		$('#na-5').click(function(e)
		{
			e.preventDefault();
			$('#shipping_date').val('N/A');
			$('#shipping_date_2').val('N/A');
			$('#form5').submit();
		});

		$('#na-6').click(function(e)
		{
			e.preventDefault();
			$('#installation_date').val('N/A');
			$('#installation_date_2').val('N/A');
			$('#installation_end_date').val('N/A');
			$('#form6').submit();
		});

		$('#na-7').click(function(e)
		{
			e.preventDefault();
			$('#service_date').val('N/A');
			$('#service_date_2').val('N/A');
			$('#form7').submit();
		});

		$('#bcf').click(function(e)
		{
			e.preventDefault();
			var id = "<?php print $id; ?>";
			window.open('blank_ca_form.php?id='+id,'_blank');
		});

		$('#ssf').click(function(e)
		{
			e.preventDefault();
			var id = "<?php print $id; ?>";
			window.open('survey_form.php?id='+id,'_blank');
		});

	    $('#calendar_btn_1').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form1');
		});

		$('#calendar_btn_2').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form2');
		});

		$('#calendar_btn_3').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form3');
		});

		$('#calendar_btn_4').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form4');
		});

		$('#calendar_btn_5').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form5');
		});

		$('#calendar_btn_6').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form6');
		});

		$('#calendar_btn_7').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form7');
		});

		$('#calendar_btn_8').click(function()
		{
			$('<input>').attr({
			    type: 'hidden',
			    id: 'action2',
			    name: 'action2',
			    value: '1'
			}).appendTo('#form8');
		});

		$('#ca_btn').click(function(e)
		{
			e.preventDefault();
			var project_id="<?php print $id; ?>";
			window.location.href = "blank_ca_form.php?id="+project_id;
		});

	    $('#bsc').click(function(e)
		{
			e.preventDefault();
			var project_id="<?php print $id; ?>";

			//var site = window.location.hostname;

			var win = window.open('sc_form.php?id='+project_id, '_blank');
			if(win)
			{
			    //Browser has allowed it to be opened
			    win.focus();
			}
			else
			{
			    //Broswer has blocked it
			    alert('Please allow popups for this site');
			}
		});

	    $('#close-date-error').click(function(e)
	    {
	    	e.preventDefault();
	    	$('#date-error').modal('hide');
	    });

	    // install end date cannot be > start date
	    $('#form5-save').click(function(e)
	    {
	    	e.preventDefault();
	    	var start=document.getElementById("installation_date_2").value;
			var end=document.getElementById("installation_end_date").value;
			var errors=0;
			if( (new Date(end).getTime() < new Date(start).getTime()))
			{
			    //alert('Install end date cannot be before start date.');
			    var t = 'Install end date cannot be before start date.';
			    $('#error-text').html(t);
			    $('#date-error').modal('show');
			    errors++;
			}
			if( (new Date(start).getTime() > new Date(end).getTime()))
			{
			    //alert('Install start date cannot be after end date.');
			    var t = 'Install start date cannot be after end date.';
			    $('#error-text').html(t);
			    $('#date-error').modal('show');
			    errors++;
			}
			if(errors==0)
			{
				$('#form6').submit();
			}
			else
			{
				return false;
			}
	    });

	});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
