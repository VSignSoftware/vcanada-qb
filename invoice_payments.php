<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$debug=1;
$qb_ar_account = $vujade->get_qb_accounts('Invoice Payments AR');
$qb_deposit_to_account = $vujade->get_qb_accounts('Invoice Payments Deposit');

# permissions
$invoices_permissions = $vujade->get_permission($_SESSION['user_id'],'Invoices');
if($invoices_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$invoiceid = $_REQUEST['invoiceid'];
$invoice = $vujade->get_invoice($invoiceid);
$txn_id=$invoice['txnid'];

$setup = $vujade->get_setup();
$invoice_setup = $vujade->get_invoice_setup();

$s = array();

$success=0;

$action=0;
if(isset($_POST['action']))
{
	$action=$_POST['action'];
}

// save new payment
if($action==1)
{
	$date=$_POST['date'];
	// correct format: 2011-12-05
	$date = strtotime($date);
	$date = date('Y-m-d',$date);
	if($date=="1969-12-31")
	{
		$date = date('Y-m-d');
	}

	$amount=$_POST['amount'];
	$reference=$_POST['reference'];
	$payment_met = $_POST['payment_method'];

	$vujade->create_row('quickbooks_receivepayment');
	$row_id = $vujade->row_id;

	# get correct customer info for this project
	$listiderror=0;
	$localiderror=0;
	// try to get by list id
    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
    if($customer1['error']!="0")
    {
    	$listiderror++;
    	unset($customer1);
    }
    else
    {
    	$customer=$customer1;
    }
    // try to get by local id
    $customer2 = $vujade->get_customer($project['client_id'],'ID');
    if($customer2['error']!="0")
    {
    	$localiderror++;
    	unset($customer2);
    }
    else
    {
    	$customer=$customer2;
    }

    $iderror=$listiderror+$localiderror;
    if($iderror<2)
    {
    	// $customer_contact = $vujade->get_contact($project['client_contact_id']);
    }
    else
    {
    	// can't find customer or no customer on file
    }

	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'CustomerRef_ListID',$customer['database_id'],'ID');
	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'ARAccountRef_ListID',$qb_ar_account[0]['list_id'],'ID');
	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'TxnDate',$date,'ID');
	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'TotalAmount',$amount,'ID');

	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'Memo',$invoiceid,'ID');

	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'RefNumber',$reference,'ID');

	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'DepositToAccountRef_ListID',$qb_deposit_to_account[0]['list_id'],'ID');
	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'PaymentMethodRef_ListID',$payment_met,'ID');

	// dummy id
	$dummy = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'TxnID',$dummy,'ID');

	//$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'',$,'ID');

	/*
	Possible update: 
	-PaymentMethodRef(ListID or FullName): I don't think we use this on Vujade, but I believe Stephanie does on QB. We can discuss whether we want to implement this, but it's there just in case.
	*/

	$vujade_id = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
	//$sql = "INSERT INTO `quickbooks_receivepayment_appliedtotxn` (`Parent_ID`,`Target_ID`,`TxnID`,`Amount`,`vujade_id`,`memo`,`TxnDate`) VALUES ('$row_id','$invoiceid','$txn_id','$amount','$vujade_id','$reference','$date')";
	$sql = "INSERT INTO `quickbooks_receivepayment_appliedtotxn` (`Parent_ID`,`Target_ID`,`TxnID`,`Amount`,`vujade_id`,`RefNumber`) VALUES ('$row_id','$invoiceid','$txn_id','$amount','$vujade_id','$reference')";
	$vujade->generic_query($sql,$debug); 

	// queue up quickbooks
	require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
	$dsn = $vujade->qbdsn;

	// create item on qb server
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('America/Los_Angeles');
	}
	
	if (!QuickBooks_Utilities::initialized($dsn))
	{	
		// Initialize creates the neccessary database schema for queueing up requests and logging
		QuickBooks_Utilities::initialize($dsn);
		// This creates a username and password which is used by the Web Connector to authenticate
		QuickBooks_Utilities::createUser($dsn, $user, $pass);
	}
	$Queue = new QuickBooks_WebConnector_Queue($dsn);
	$Queue->enqueue(QUICKBOOKS_ADD_RECEIVEPAYMENT, $row_id, 0);
	$success=1;
	$success_text="Payment Added!";
}

// edit existing payment
if($action==2)
{
	$date=$_POST['date'];
	$date = strtotime($date);
	$date = date('Y-m-d',$date);
	if($date=="1969-12-31")
	{
		$date = date('Y-m-d');
	}

	$amount=$_POST['amount'];
	$reference=$_POST['reference'];
	$payment_met = $_POST['payment_method'];

	$line_id=$_POST['line_id'];
	$rp_id=$_POST['rp_id'];
	// get data of this payment by id
	$p = $vujade->get_invoice_payment($rp_id,2);
	if($p['error']=="0")
	{
		// update row in quickbooks_receivepayment
		$row_id = $rp_id;
		$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'TxnDate',$date,'ID');
		$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'TotalAmount',$amount,'ID');
		$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'RefNumber',$reference,'ID');
		$s[]=$vujade->update_row('quickbooks_receivepayment',$row_id,'PaymentMethodRef_ListID',$payment_met,'ID');

		//$sql = "UPDATE `quickbooks_receivepayment_appliedtotxn` SET `TxnDate` = '$date', `Amount` = '$amount', `memo` = '$reference' WHERE `Parent_ID` = '$row_id' AND `TxnID` = '$txn_id'";
		$sql = "UPDATE `quickbooks_receivepayment_appliedtotxn` SET `Amount` = '$amount', `RefNumber` = '$reference' WHERE `Parent_ID` = '$row_id' AND `TxnID` = '$txn_id'";
		$vujade->generic_query($sql,$debug); 
		/*
		// update row in quickbooks_receivepayment_appliedtotxn
		$s[]=$vujade->update_row('quickbooks_receivepayment_appliedtotxn',$line_id,'TxnDate',$date,'vujade_id');
		$s[]=$vujade->update_row('quickbooks_receivepayment_appliedtotxn',$line_id,'Amount',$amount,'vujade_id');
		$s[]=$vujade->update_row('quickbooks_receivepayment_appliedtotxn',$line_id,'memo',$reference,'vujade_id');
		*/

		if(!empty($p['edit_sequence']))
		{

			// queue up quickbooks
			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_MOD_RECEIVEPAYMENT, $row_id, 0);
		}
		$success=1;
		$success_text="Payment Re-saved!";
	}
}

// delete payment
if($action==3)
{
	//$line_id=$_POST['line_id'];
	// get data of this payment by id
	//$p = $vujade->get_invoice_payment($line_id);
	$rp_id=$_POST['rp_id'];
	$p2 = $vujade->get_invoice_payment($rp_id,2);

	//print $line_id.' '.$txn_id.'<br>';
	//print_r($p).
	//print '<br>';
	//print_r($p2);
	//die;

	/*
	this is the row in quickbooks_receivepayment_appliedtotxn
	array $p
	Array ( 
	[error] => 0 
	[database_id] => 
	[date] => 2014-12-24 
	[amount] => 14.00 
	[reference] => 
	[rp_id] => 1403 // id of receive
	) 
	
	this is the row in quickbooks_receivepayment
	array $p2
	Array ( 
	[error] => 0 
	[database_id] => 1403 
	[payment_method] => 80000002-1322849505 
	[reference] => Test 5 
	[edit_sequence] => 1419439839 
	)


	Array ( [error] => 0 [sql] => SELECT * FROM `quickbooks_receivepayment_appliedtotxn` WHERE `vujade_id` = '' [database_id] => [date] => 2014-12-24 [amount] => 14.00 [reference] => [rp_id] => 1403 )

	Array ( [error] => 0 [sql] => SELECT * FROM `quickbooks_receivepayment` WHERE `ID` = '1403' [database_id] => 1403 [payment_method] => 80000002-1322849505 [reference] => Test 5 [edit_sequence] => 1419439839 )

	*/

	if($p2['error']=="0")
	{
		// delete quickbooks_receivepayment_appliedtotxn row
		
		$sql = "DELETE FROM `quickbooks_receivepayment_appliedtotxn` WHERE `Parent_ID` = '$rp_id' AND `TxnID` = '$txn_id'";
		$vujade->generic_query($sql,$debug); 

		// delete the related row in quickbooks_receivepayment 
		$t2="quickbooks_receivepayment";
		$s[]=$vujade->delete_row($t2,$rp_id,$no_limit=0,$idcol='ID');

		// delete from the queue
		$ident = $rp_id;
		$sql = "DELETE FROM `quickbooks_queue` WHERE `qb_action` = 'ReceivePaymentAdd' AND `ident` = '$ident' AND `dequeue_datetime` IS NULL";
		$vujade->generic_query($sql,1); 
		$sql2 = "DELETE FROM `quickbooks_queue` WHERE `qb_action` = 'ReceivePaymentMod' AND `ident` = '$ident' AND `dequeue_datetime` IS NULL";
		$vujade->generic_query($sql2,1); 

		// delete from quickbooks_receivepayment_appliedtotxn_linkedtxn
		$sql = "DELETE FROM `quickbooks_receivepayment_appliedtotxn_linkedtxn` WHERE `Parent_Parent_ID` = '$rp_id' AND `Parent_TxnID` = '$txn_id'";
		$vujade->generic_query($sql,1); 

		// delete form quickbooks if there is an edit sequence
		if(!empty($p2['edit_sequence']))
		{

			// $Queue->enqueue(QUICKBOOKS_DEL_TRANSACTION, $listID, 0, 'ReceivePayment');
			// queue up quickbooks
			require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
			$dsn = $vujade->qbdsn;

			// create item on qb server
			if(function_exists('date_default_timezone_set'))
			{
				date_default_timezone_set('America/Los_Angeles');
			}
			
			if (!QuickBooks_Utilities::initialized($dsn))
			{	
				// Initialize creates the neccessary database schema for queueing up requests and logging
				QuickBooks_Utilities::initialize($dsn);
				// This creates a username and password which is used by the Web Connector to authenticate
				QuickBooks_Utilities::createUser($dsn, $user, $pass);
			}
			$delete_id = $p2['delete_id'];
			$Queue = new QuickBooks_WebConnector_Queue($dsn);
			$Queue->enqueue(QUICKBOOKS_DEL_TRANSACTION, $delete_id, 0, 'ReceivePayment');
		}
		$success=1;
		$success_text="Payment Deleted!";
	}
	else
	{
		// error
	}
}

// create or alter / edit credit memo
// deprecated
if($action==4)
{
	die;
	// create new credit memo
	if(empty($_POST['credit_memo_id']))
	{
		$vujade->create_row('quickbooks_creditmemo');
		$credit_memo_id=$vujade->row_id;
		$dbid=$vujade->row_id;
		$s[] = $vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'project_id',$project_id);
		$s[] = $vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'invoice_id',$invoice['invoice_id']);
		$s[] = $vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'TxnDate',date('Y-m-d'),'ID');
		$s[] = $vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'ts',strtotime('now'),'ID');

		# get correct customer info for this project
		$listiderror=0;
		$localiderror=0;
		// try to get by list id
	    $customer1 = $vujade->get_customer($project['client_id'],'ListID');
	    if($customer1['error']!="0")
	    {
	    	$listiderror++;
	    	unset($customer1);
	    }
	    else
	    {
	    	$customer=$customer1;
	    }
	    // try to get by local id
	    $customer2 = $vujade->get_customer($project['client_id'],'ID');
	    if($customer2['error']!="0")
	    {
	    	$localiderror++;
	    	unset($customer2);
	    }
	    else
	    {
	    	$customer=$customer2;
	    }

	    $iderror=$listiderror+$localiderror;

	    if($iderror<2)
	    {
	    	$customer_contact = $vujade->get_contact($project['client_contact_id']);
	    }
	    else
	    {
	    	// can't find customer or no customer on file
	    }

		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'CustomerRef_ListID',$customer['database_id'],'ID');

		// get and set the sales tax code 
		// qb tax state is the same as shipping address
		if($project['state']==$setup['qb_tax_state'])
		{
			$tax_data = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
		}
		else
		{
			// not the same
			$tax_data = $vujade->QB_get_sales_tax('Out of State');
		}
		if($tax_data['error']=="0")
		{
			$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data['ListID'],'ID');
			$tax_rate=$tax_data['rate']; 
		}
		else
		{
			$tax_data2 = $vujade->QB_get_sales_tax('Out of State');
			$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'ItemSalesTaxRef_ListID',$tax_data2['ListID'],'ID');
			$tax_rate=$tax_data2['rate'];
		}

		$fakeid = substr(str_shuffle(MD5(microtime())), 0, mt_rand(4, 36));
		$s[] = $vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'TxnID',$fakeid,'ID');

		// shipping address
		$s[]=$vujade->update_row('quickbooks_creditmemo',$dbid,'ShipAddress_Addr1',$project['site'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$dbid,'ShipAddress_Addr2',$project['address_1'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$dbid,'ShipAddress_City',$project['city'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$dbid,'ShipAddress_State',$project['state'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$dbid,'ShipAddress_PostalCode',$project['zip'],'ID');

		// other updates
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'v_tax_rate',$tax_rate,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'i4_label',$invoice_setup['label_4'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'i4_rate',$invoice_setup['sale_price_4'],'ID');

		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_5',$_POST['is_taxable_5'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_6',$_POST['is_taxable_6'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_7',$_POST['is_taxable_7'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_8',$_POST['is_taxable_8'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_9',$_POST['is_taxable_9'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_10',$_POST['is_taxable_10'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'is_taxable_11',$_POST['is_taxable_11'],'ID');

		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_5',$invoice_setup['sale_price_5'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_6',$invoice_setup['sale_price_6'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_7',$invoice_setup['sale_price_7'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_8',$invoice_setup['sale_price_8'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_9',$invoice_setup['sale_price_9'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_10',$invoice_setup['sale_price_10'],'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_rate_11',$invoice_setup['sale_price_11'],'ID');

		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_5',0,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_6',0,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_7',0,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_8',0,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_9',0,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_10',0,'ID');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$vujade->row_id,'taxable_amount_11',0,'ID');
		
		// a/r account
		$ar_account_data=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
		$s[]=$vujade->update_row('quickbooks_creditmemo',$credit_memo_id,'ARAccountRef_ListID',$ar_account_data['list_id'],'ID');

		$query2 = "INSERT INTO
					`quickbooks_creditmemo_lineitem`
				(
					`Parent_ID`,
					`ItemRef_FullName`,
					`Amount`,
					`Quantity`,
					`TxnLineID`,
					`Desc`
				) VALUES (
					'" . $credit_memo_id . "',
					'Manufacture & Install','0.00','1','".$fakeid."',''
				)";
		
		$vujade->generic_query($query2);
		
		require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
		$dsn = $vujade->qbdsn;
		
		// create item on qb server
		if(function_exists('date_default_timezone_set'))
		{
			date_default_timezone_set('America/Los_Angeles');
		}
		
		if (!QuickBooks_Utilities::initialized($dsn))
		{
			// Initialize creates the neccessary database schema for queueing up requests and logging
			QuickBooks_Utilities::initialize($dsn);
			// This creates a username and password which is used by the Web Connector to authenticate
			QuickBooks_Utilities::createUser($dsn, $user, $pass);
		}
		$Queue = new QuickBooks_WebConnector_Queue($dsn);
		$Queue->enqueue(QUICKBOOKS_ADD_CREDITMEMO, $vujade->row_id, 0);
	}
	else
	{
		// credit memo is not new; 
		$credit_memo_id=$_POST['credit_memo_id'];
		// validate 
		$test_cm=$vujade->get_credit_memo($credit_memo_id,'ID');
		if($test_cm['count']!=1)
		{
			$vujade->page_redirect('error.php?m=9');
		}
	}

	// credit memo amount, notes, tax ("Yes" or "No"), tax account
    $credit_memo=$_POST['credit_memo'];
	$credit_memo_notes=$_POST['credit_memo_notes'];
	$cm_tax=$_POST['cm_tax'];
	// $cm_tax_account=$_POST['cm_tax_account'];
	
	$s[]=$vujade->update_row('quickbooks_creditmemo',$credit_memo_id,'v_credit_memo',$credit_memo,'ID');
	$s[]=$vujade->update_row('quickbooks_creditmemo',$credit_memo_id,'v_credit_memo_notes',$credit_memo_notes,'ID');
	$s[]=$vujade->update_row('quickbooks_creditmemo',$credit_memo_id,'cm_tax',$cm_tax,'ID');

	$taxcode = "";
	
	if($cm_tax == "Yes")
	{
		$sql = "SELECT `ListID` FROM `quickbooks_salestaxcode` WHERE `Name` = 'Tax'";
		$result = $vujade->generic_query($sql);
		$arr = mysqli_fetch_assoc($result);
		$taxcode = $arr['ListID'];
	}
	else if($cm_tax == "No")
	{
		$sql = "SELECT `ListID` FROM `quickbooks_salestaxcode` WHERE `Name` = 'Non'";
		$result = $vujade->generic_query($sql);
		$arr = mysqli_fetch_assoc($result);
		$taxcode = $arr['ListID'];
	}
	else 
	{
		//??????? Insert error handling here as desired.	
	}
	
	// taxed: the value of this is "Yes" or "No"
	//$s[]=$vujade->update_row('quickbooks_creditmemo',$credit_memo_id,'',$cm_tax,'ID');
	$sql = "UPDATE `quickbooks_creditmemo_lineitem` SET `SalesTaxCodeRef_ListID` = '" . $arr['ListID'] . "' WHERE `Parent_ID` = '" . $credit_memo_id . "'";
	$vujade->generic_query($sql);

	// tax account
	//$s[]=$vujade->update_row('quickbooks_creditmemo',$credit_memo_id,'ARAccountRef_ListID',$cm_tax_account,'ID');
	//$sql = "SELECT `ListID` from `quickbooks_account` WHERE `Name` = '" . $cm_tax_account . "'";
	//$result = $vujade->generic_query($sql);
	//$arr = mysqli_fetch_assoc($result);
	//$sql = "UPDATE `quickbooks_creditmemo` SET `ARAccountRef_ListID` = '" . $arr['ListID'] . "' WHERE `ID` = " . $credit_memo_id;
	//$vujade->generic_query($sql);
	
	// all other updates go here...
	$str = html_entity_decode(strip_tags($credit_memo_notes));
	$str2 = preg_replace('~\x{00a0}~siu',' ',$str);
	$sql = "UPDATE `quickbooks_creditmemo_lineitem` SET `Amount` = " . $credit_memo . ", `Desc` = '" . $str2 . "' WHERE `Parent_ID` = '" . $credit_memo_id . "'";
	$vujade->generic_query($sql);
	
	require_once 'qb-bridge/quickbooks-php/QuickBooks.php';
	$dsn = $vujade->qbdsn;
	
	// create item on qb server
	if(function_exists('date_default_timezone_set'))
	{
		date_default_timezone_set('America/Los_Angeles');
	}
	
	if (!QuickBooks_Utilities::initialized($dsn))
	{
		// Initialize creates the neccessary database schema for queueing up requests and logging
		QuickBooks_Utilities::initialize($dsn);
		// This creates a username and password which is used by the Web Connector to authenticate
		QuickBooks_Utilities::createUser($dsn, $user, $pass);
	}
	$Queue = new QuickBooks_WebConnector_Queue($dsn);
	$Queue->enqueue(QUICKBOOKS_MOD_CREDITMEMO, $credit_memo_id, 0);

	// total to show at bottom of credit memo section
	$credit_memo_tax_total=0;
	$credit_memo_total=$credit_memo+$credit_memo_tax_total;

	$vujade->messages[]='Credit memo updated.';

	//die;
}

// payment methods options
$payment_methods = $vujade->get_invoice_payment_methods();
unset($payment_methods['error']);

// qb accounts data
$qb_accounts = $vujade->get_qb_accounts('',true);
if($qb_accounts['count']<=0)
{
	$vujade->page_redirect('error.php?m=8');
}
unset($qb_accounts['count']);
unset($qb_accounts['error']);
unset($qb_accounts['sql']);

$section=3;
$menu=17;
$cmenu=1;
$title = 'Invoice Payments - '.$invoice['invoice_id'] . ' - ';
require_once('h.php');
?>

<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Payments for Invoice #<?php print $invoice['invoice_id']; ?></a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

<div class="theme-primary">

<?php 
$vujade->show_errors();
$vujade->show_messages();
?>

<div class="panel heading-border panel-primary">
	<div class="panel-body bg-light">

		<?php
		if($success==1)
		{
			print '<div class = "alert alert-success">'.$success_text.'</div>';
		}
		?>

		<table class = "table">
			<tr>
				<td>Date
				</td>
				<td>Amount
				</td>
				<td>Reference
				</td>
				<td>Payment Method
				</td>
				<td>&nbsp;
				</td>
				<td>&nbsp;
				</td>
			</tr>
			<?php
			// existing payments
			$payments = $vujade->get_invoice_payments($invoiceid,2);
			$pt = 0;
			if($payments['error']=="0")
			{
				unset($payments['error']);
				foreach($payments as $payment)
				{
					$pt+=$payment['amount'];
					?>
					<form method = "post" action = "invoice_payments.php">
						<input type = "hidden" name = "action" value = "2">
						<input type = "hidden" name = "invoiceid" value = "<?php print $invoiceid; ?>">
						<input type = "hidden" name = "id" value = "<?php print $project_id; ?>">
						<input type = "hidden" name = "line_id" value = "<?php print $payment['database_id']; ?>">
						<input type = "hidden" name = "rp_id" value = "<?php print $payment['rp_id']; ?>">
						<input type = "hidden" name = "row_id" value = "<?php print $payment['database_id']; ?>">
						<tr>
							<td><input type = "text" name = "date" value = "<?php print $payment['date']; ?>" class = "dp">
							</td>
							<td><input type = "text" name = "amount" value = "<?php print $payment['amount']; ?>">
							</td>
							<td>
								<input type = "text" name = "reference" value = "<?php print $payment['reference']; ?>">
							</td>
							<td>
								<select name = "payment_method">
									<?php
									if(!empty($payment['payment_method']))
									{
										$pmdata=$vujade->get_invoice_payment_method($payment['payment_method']);
										print '<option value = "'.$payment['payment_method'].'">'.$pmdata['name'].'</option>';
									}
									print '<option value = "">-Select-</option>';
									foreach($payment_methods as $pms)
									{
										print '<option value = "'.$pms['list_id'].'">'.$pms['name'].'</option>';
									}
									?>
								</select>
							</td>
							<td>
								<input type = "submit" value = "RE-SAVE" class = "btn btn-success pull-right">
								</form>
							</td>
							<td>
								<form method = "post" action = "invoice_payments.php">
								<input type = "hidden" name = "action" value = "3">
								<input type = "hidden" name = "invoiceid" value = "<?php print $invoiceid; ?>">
								<input type = "hidden" name = "id" value = "<?php print $project_id; ?>">
								<input type = "hidden" name = "line_id" value = "<?php print $payment['database_id']; ?>">
								<input type = "hidden" name = "rp_id" value = "<?php print $payment['rp_id']; ?>">
								<input type = "hidden" name = "row_id" value = "<?php print $payment['database_id']; ?>">
								<input type = "submit" value = "DELETE" class = "btn btn-danger pull-right">
								</form>
							</td>
						</tr>
					<?php
				}
			}
			print '<tr><td colspan = "5" style = "text-align:right;font-weight:bold;font-size:14px;">Total Paid: $'.@number_format($pt,2,'.',',').'</td></tr>';
			?>

			<!-- new payment form -->
			<form method = "post" action = "invoice_payments.php">
				<input type = "hidden" name = "action" value = "1">
				<input type = "hidden" name = "invoiceid" value = "<?php print $invoiceid; ?>">
				<input type = "hidden" name = "id" value = "<?php print $project_id; ?>">
				<tr>
					<td><input type = "text" name = "date" class = "dp">
					</td>
					<td><input type = "text" name = "amount">
					</td>
					<td><input type = "text" name = "reference">
					</td>
					<td>
						<select name = "payment_method">
							<option value = "">-Select-</option>
							<?php
							foreach($payment_methods as $pms)
							{
								print '<option value = "'.$pms['list_id'].'">'.$pms['name'].'</option>';
							}
							?>
						</select>
					</td>
					<td>
						<input type = "submit" value = "ADD" class = "btn btn-success pull-right">
					</td>
					<td>
						&nbsp;
					</td>
				</tr>
			</form>

				<tr>
					<td colspan = "5">
						<input type = "submit" value = "DONE" style = "" id = "done" class = "btn btn-primary pull-right">
					</td>
				</tr>

				
		</table>

		<!-- credit memo -->
		<h3>Credit Memos</h3>
		<table class = "table">
			<tr>
				<td>Date
				</td>
				<td>Amount
				</td>
				<td>&nbsp;
				</td>
				<td>&nbsp;
				</td>
				<td>&nbsp;
				</td>
				<td>&nbsp;
				</td>
			</tr>
			<?php
			$cm_total=0;
			$credit_memos = $vujade->get_credit_memos($invoice['invoice_id']);
			if($credit_memos['count']>0)
			{
				unset($credit_memos['error']);
				unset($credit_memos['count']);
				unset($credit_memos['sql']);
				foreach($credit_memos as $credit_memo)
				{
					$cm_total+=$credit_memo['amount'];
					?>
						<tr>
							<td>
								<input type = "text" disabled value = "<?php print $credit_memo['date']; ?>" class = "dp">
							</td>
							<td>
								<input type = "text" disabled value = "<?php print $credit_memo['amount']; ?>">
							</td>
							<td>
								&nbsp;
							</td>
							<td>
								&nbsp;
							</td>
							<td>
								&nbsp;
							</td>
							<td>
								&nbsp;
							</td>
						</tr>
					<?php
				}
			}
			print '<tr><td colspan = "5" style = "text-align:right;font-weight:bold;font-size:14px;">Total: $'.@number_format($cm_total,2,'.',',').'</td></tr>';
			?>
		</table>


    </div>
</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
	Core.init();

	$('.dp').datepicker();

	$('#done').click(function()
	{
		var link = "project_invoices.php?id=<?php print $project_id; ?>&invoiceid=<?php print $invoiceid; ?>";
		window.location.href=link;
	});

	$('#done2').click(function()
	{
		var link = "project_invoices.php?id=<?php print $project_id; ?>&invoiceid=<?php print $invoiceid; ?>";
		window.location.href=link;
	});

	// save-credit-memo button
	$('#save-credit-memo').click(function(e)
	{
		e.preventDefault();
		$('#form2').submit();
	});

});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
