<?php
session_start();
print '<meta charset="ISO-8859-1">';
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$poid = $_REQUEST['poid'];
$project_id = $_REQUEST['project_id'];
$reset = $_REQUEST['reset'];
$blank = $_REQUEST['blank'];

// controls where to redirect when item is added from this page
$redirect = 1;
if(isset($_REQUEST['redirect']))
{
	$redirect = $_REQUEST['redirect'];
}
if(!in_array($redirect,array(1,2,3,4)))
{
	$redirect=1;
}

if($reset==1)
{
	$materials = $vujade->get_inventory($sort=1,$start=1,$no_limit=1);
	if($materials['error']=='0')
	{
		unset($materials['error']);
		print '<div style = "overflow:auto;width:100%;height:200px;">';
		print '<table id = "itable" class = "table">';
		print '<tr>';
		print '<td>ID';
		print '</td>';
		print '<td>Description';
		print '</td>';
		print '<td>Size';
		print '</td>';
		print '<td>Cost';
		print '</td>';
		print '<td>';
		print '&nbsp;';
		print '</td>';
		print '</tr>';
		foreach($materials as $m)
		{
			if(!empty($m['cost']))
			{
				print '<tr>';
				print '<td>';
				print $m['inventory_id'];
				print '</td>';
				print '<td>';
				print $m['description'];
				print '</td>';
				print '<td>';
				print $m['size'];
				print '</td>';
				print '<td>';
				//print $m['cost'];
				@$c = number_format($m['cost'],2,'.',',');
				print '$'.$c;
				print '</td>';
				print '<td>';
				print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-success btn-xs">+</a>';
				print '</td>';
				print '</tr>';
			}
		}
		print '</table>';
		print '</div>';
	}
}
else
{
	$filter = $_POST['filter'];
	$filter_field = $_POST['filter_field'];
	if($filter_field==1)
	{
		$col="inventory_id";
	}
	if($filter_field==2)
	{
		$col="description";
	}
	if($filter_field==3)
	{
		$col="size";
	}
	if($filter_field==4)
	{
		$col="cost";
	}
	if($filter_field==5)
	{
		$col="all";
	}
	$materials = $vujade->filter_inventory($filter,$col);
	if($materials['error']=='0')
	{
		unset($materials['error']);
		print '<div style = "overflow:auto;width:100%;height:200px;">';
		print '<table id = "itable" class = "table">';
		print '<tr>';
		print '<td>ID';
		print '</td>';
		print '<td>Description';
		print '</td>';
		print '<td>Size';
		print '</td>';
		print '<td>Cost';
		print '</td>';
		print '<td>';
		print '&nbsp;';
		print '</td>';
		print '</tr>';
		foreach($materials as $m)
		{
			if(!empty($m['cost']))
			{
				print '<tr>';
				print '<td>';
				print $m['inventory_id'];
				print '</td>';
				print '<td>';
				print $m['description'];
				print '</td>';
				print '<td>';
				print $m['size'];
				print '</td>';
				print '<td>';
				@$c = number_format($m['cost'],2,'.',',');
				print '$'.$c;
				print '</td>';
				print '<td>';
				print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-success btn-xs">+</a>';
				print '</td>';
				print '</tr>';
			}
		}
		print '</table>';
		print '</div>';
	}
}
?>

<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script type="text/javascript">
$(document).ready(function()
{
	$('.plus').click(function(e)
	{
		e.preventDefault();
		var poid = "<?php print $poid; ?>";
		var item_id = this.id;
		var notes = $('#notes').val();
		var tax = $('#tax').val();
		var deposit = "";
		var st = $('#subtotal').val();
		var project_id = "<?php print $project_id; ?>";
		var blank = "<?php print $blank; ?>";

		var url = "edit_purchase_order_materials.php?action=1&project_id="+project_id+"&poid="+poid+"&item_id="+item_id+"&blank="+blank;

		
		window.location.href=url;
	});
});	
</script>