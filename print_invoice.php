<?php
session_start();
error_reporting(0);
define('SITE',1);
require_once('../library/class_library.php');

$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$project_id = $_REQUEST['id'];
$project = $vujade->get_project($project_id,2);
$setup = $vujade->get_invoice_company_setup(2);
$setup2 = $vujade->get_setup();
$invoice_database_id=$_REQUEST['invoiceid'];
$invoiceid=$invoice_database_id;
$invoice = $vujade->get_invoice($invoice_database_id);

$logo_data = $vujade->get_logo(3);
$logo = '<img src = "./'.$logo_data['url'].'" width = "260" height = "60">';

# get correct customer info for this project
$listiderror=0;
$localiderror=0;
// try to get by list id
$customer1 = $vujade->get_customer($project['client_id'],'ListID');
if($customer1['error']!="0")
{
    $listiderror++;
    unset($customer1);
}
else
{
    $customer=$customer1;
}
// try to get by local id
$customer2 = $vujade->get_customer($project['client_id'],'ID');
if($customer2['error']!="0")
{
    $localiderror++;
    unset($customer2);
}
else
{
    $customer=$customer2;
}

$iderror=$listiderror+$localiderror;

if($iderror<2)
{
    $customer_contact = $vujade->get_contact($project['client_contact_id']);
}
else
{
    // can't find customer or no customer on file
}

$terms = $vujade->get_terms(3);
$notes = $vujade->get_terms(4);
$shop_order = $vujade->get_shop_order($project_id,'project_id');

$tax_rate=$invoice['v_tax_rate'];
$advanced_deposit=$invoice['advanced_deposit'];

// subtotal 1 (selling price)
$sp=0;

// not legacy
if($invoice['is_legacy']==0)
{
	$sp = $invoice['v_sales_price'];

	// tax amount
	$tax_amount = $invoice['v_tax_amount'];

	// i4 amount
	$i4_amount = $invoice['i4_amount'];

	// engineering
	$engineering = $invoice['v_engineering'];

	// permits
	$permits = $invoice['v_permits'];

	// permit acq
	$pa = $invoice['v_permit_acquisition'];

	// shipping
	$shipping = $invoice['v_shipping'];

	// discount
	$discount = $invoice['v_discount'];

	// retention
	$retention = $invoice['v_retention'];

	// deposit
	$deposit = $invoice['v_deposit'];

	// cc processing fees
	$cc = $invoice['v_cc_processing'];
}

// legacy
if($invoice['is_legacy']==1)
{
	//print_r($invoice);
	//die;
	// invoice types
	// 1: illuminated and lump sum
	if( ($invoice['type_1']==1) && ($invoice['type_2']==1) )
	{
		// labor
		$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

		// parts 
		$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

		// total
		$sp = $labor+$parts; 

		$parts=0;

		// selling price text
		$selling_price_text = "Selling Price";
	}

	// 2: illuminated sign parts and labor
	if( ($invoice['type_1']==1) && ($invoice['type_2']==2) )
	{
		// labor
		$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

		// parts 
		$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

		// selling price text
		$selling_price_text = "Labor Only Price";
	}

	// 3: non-illuminated lump sum
	if( ($invoice['type_1']==2) && ($invoice['type_2']==1) )
	{
		$sp = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

		$parts=0;

		// selling price text
		$selling_price_text = "Selling Price";
	}

	// 4: non-illuminated sign parts and labor
	if( ($invoice['type_1']==2) && ($invoice['type_2']==2) )
	{
		// labor
		$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

		// parts 
		$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

		// selling price text
		$selling_price_text = "Labor Only Price";
	}

	// 5: service with parts and labor
	if( ($invoice['type_1']==3) && ($invoice['type_2']==2) )
	{
		// labor
		$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

		// parts 
		$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

		// selling price text
		$selling_price_text = "Labor Only Price";
	}

	// tax amount
	// this will be changing in the future
	// $tax_amount = $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
	$tax_amount=$invoice['sales_tax'];

	// change orders
	$co = $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

	// non-taxable co
	$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);
	$co+=$ntco;

	// engineering
	$engineering = $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

	// permits
	$permits = $vujade->get_invoice_line_items($invoiceid,'Permits',true);

	// permit acq
	$pa = $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

	// shipping
	$shipping = $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

	// discount
	$discount=$vujade->get_invoice_line_items($invoiceid,'Discount',true);

	// retention
	$retention = $vujade->get_invoice_line_items($invoiceid,'Retention',true);

	// deposit
	$deposit = $vujade->get_invoice_line_items($invoiceid,'Deposit',true);

	// cc processing fees
	$cc = $vujade->get_invoice_line_items($invoiceid,'CC Processing Fee',true);

}

// payments
$payments = $vujade->get_invoice_payments($invoiceid);
if($payments['error']=="0")
{
    unset($payments['error']);
    $pt = 0;
    foreach($payments as $payment)
    {
        
        $pt+=$payment['amount'];
    }
}
else
{
    $pt=0;
}

// credit memos
$credit_memos=$vujade->get_credit_memos($invoice['invoice_id']);
if($credit_memos['count']>0)
{
	unset($credit_memos['sql']);
	unset($credit_memos['error']);
	unset($credit_memos['count']);
	foreach($credit_memos as $credit_memo)
	{
		$pt+=$credit_memo['amount'];
	}
}

// subtotal 
$subtotal2=$sp+$tax_amount;

$column_1_st=$sp+$engineering+$permits+$pa+$co+$shipping+$parts+$cc+$i4_amount;
$column_1_st=$column_1_st+$discount;
$column_2_st=$column_1_st+$tax_amount;
$column_2_st_2=$column_2_st-$deposit-$retention;

if($invoice['is_legacy']==1)
{
	$balance_due=$column_2_st_2-$pt;
}
else
{
	if($invoice['advanced_deposit']==1)
	{
		$balance_due=$deposit-$pt;
	}
	else
	{
		$balance_due=$invoice['v_balance_due']-$pt;
	}
}

$charset="ISO-8859-1";
$html = '
            <!DOCTYPE html>
            <head>
            <meta charset="'.$charset.'">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <title>Invoice</title>
            <meta name="description" content="">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <link rel="stylesheet" href="css/print_invoice_style.css">
            <style>
		    .marker
		    {
		    	background-color: yellow;
		    }
		    </style>
            </head>
            <body>
            <div id="mainContent">
            <div id="content">';

# logo and invoice number
$html.='<table width = "100%">';
$html.= '<tr>';
$html.='<td valign = "top" width = "70%">';
$html.=$logo;
$html.='</td>';
$html.='<td valign = "top" width = "30%">';
$html.='<h1><center>Invoice</center></h1>';
$html.='
        <table width = "100%">
        <tr>
        <td class = "firstbox" valign = "top" width = "50%"><center>Date</center></td>
        <td class = "firstbox" valign = "top" width = "50%"><center>Invoice #</center></td>
        </tr>
        <tr>
        <td class = "firstbox"><center>'.$invoice['date'].'</center></td>
        <td class = "firstbox"><center>'.$invoice['invoice_id'].'</center></td>
        </tr>
        </table>
';

$html.='</td>';
$html.='</tr>';
$html.='</table>';

if(!empty($setup['address_1']))
{
	$html.='<table width = "100%">';
	$html.= '<tr>';
	$html.='<td valign = "top" width = "50%">';
	$html.=$setup['name'];
	$html.='</td>';
	$html.='<td valign = "top" width = "50%" class = "alignright" style = "padding-right:30px;">Contractor #';
	$html.=$setup['license_number'];
	$html.='</td>';
	$html.='</tr>';
	$html.= '<tr>';
	$html.='<td valign = "top" style = "">';
	$compinfo = $setup['address_1'];
	if(!empty($setup['address_2']))
	{
	    $compinfo .= ", ".$setup['address_2'].", ";
	}
	$compinfo .= ' '.$setup['city'].', '.$setup['state'].' '.$setup['zip'].'<br>';
	$html.=$compinfo;
	$html.='</td>';
	$html.='<td valign = "top" width = "50%" class = "alignright" style = "padding-right:30px;">&nbsp;';
	$html.='</td>';
	$html.='</tr>';
	$html.= '<tr>';
	$html.='<td valign = "top" style = "">';
	$html.=$setup['phone'];
	$html.='</td>';
	$html.='<td valign = "top" width = "50%" class = "alignright" style = "padding-right:30px;">&nbsp;';
	$html.='</td>';
	$html.='</tr>';
	$html.='</table>';
}

# bill to box, job site box
$html.='
        <table width = "100%">
        <tr>

        <td width = "45%" valign = "top" class = "firstbox">
        <center>
        Bill To
        </center>
        </td>

        <td width = "10%" valign = "top">&nbsp;
        </td>

        <td width = "45%" valign = "top" class = "firstbox">
        <center>
        Job site
        </center>
        </td>
        </tr> 

        <tr>
        <td class = "firstbox">
            <center>'.$customer['name'].'<br>
            '.$customer['address_1'];
            if(!empty($customer['address_2']))
            {
                $html.=', '.$customer['address_2'];
            }
            $html.='<br>'.$customer['city'].', '.$customer['state'].' '.$customer['zip'].'
        </center>
        </td>

        <td width = "10%" valign = "top">&nbsp;
        </td>

        <td class = "firstbox">
        <center>'.$project['site'].'<br>
        '.$project['address_1'];
        if(!empty($project['address_2']))
        {
            $html.=', '.$project['address_2'];
        }
        $html.='<br>'.$project['city'].', '.$project['state'].' '.$project['zip'].'
        </center>
        </td>
        </tr> 
        </table>
';

# po number, rep, ordered by
$html.='
        <table width = "100%">
        <tr>
        <td width = "33%" valign = "top" class = "firstbox">
        <center>
        P.O. Number
        </center>
        </td>

        <td width = "33%" valign = "top" class = "firstbox">
        <center>
        Rep
        </center>
        </td>

        <td width = "33%" valign = "top" class = "firstbox">
        <center>
        Ordered By
        </center>
        </td>
        </tr>

        <tr>
        <td width = "33%" valign = "top" class = "firstbox">
        <center>'.$invoice['po_number'].'
        </center>
        </td>

        <td width = "33%" valign = "top" class = "firstbox">
        <center>'.$project['salesperson'].'
        </center>
        </td>

        <td width = "33%" valign = "top" class = "firstbox">
        <center>'.$project['project_contact'].'
        </center>
        </td>
        </tr>
        </table>
';

if($invoice['advanced_deposit']==1)
{
    $html.='<center><strong><font style = "color:red;font-size:16px;">** Advance Deposit Billing **</font></strong></center><br>';
}

// not legacy
if($invoice['is_legacy']==0)
{
	// line items
	$lines = $vujade->get_invoice_line_items($invoice['database_id'],'',false,2);

	if($lines['count']>0)
	{
		unset($lines['count']);
		unset($lines['error']);
		$html.='<table width = "100%">';
		$html.='<tr><td><b>Item</b></td><td><b>Amount</b></td></tr>';
		foreach($lines as $line)
		{
			$html.='<tr>';
			$html.='<td width = "80%">';
			$html.=$line['line_description'];
			$html.='</td>';
			if(!empty($line['line_amount']))
			{
				$html.='<td>$';
				$html.=number_format($line['line_amount'],2);
				$html.='</td>';
			}
			else
			{
				$html.='<td>&nbsp;';
				$html.='</td>';
			}
			$html.='</tr>';
		}
		$html.='</table>';
		$html.='<hr>';
	}

	// amounts
	$html.='<table width = "100%">';
	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>Item</strong>';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>Price</strong>';
	$html.='</td>';
	$html.='</tr>';

	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">Sales Price';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='$'.@number_format($sp,2,'.',',');
	$html.='</td>';
	$html.='</tr>';

	if(!empty($engineering))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Engineering';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($engineering,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($permits))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Permits';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($permits,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($pa))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Permit Acquisition';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($pa,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($co))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Change Order';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($co,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($i4_amount))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.=$invoice['i4_label'];
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($i4_amount,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($shipping))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Shipping';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($shipping,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($tax_amount))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Sales Tax';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($tax_amount,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($discount))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Discount';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='-$'.@number_format($discount,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}
	if($advanced_deposit==1)
	{
		if(!empty($deposit))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Deposit Due';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($deposit,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}
	}
	if($advanced_deposit==0)
	{
		if(!empty($deposit))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Deposit';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($deposit,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}
	}

	if(!empty($retention))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Retention';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='-$'.@number_format($retention,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($pt))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Payments';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='-$'.@number_format($pt,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	//print $balance_due.' '.$pt;
	//die;
	//if($advanced_deposit==1)
	//{
	//	$balance_due=$balance_due+$cc-$pt;
	//}

	if(!empty($cc))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Credit Card Fees';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($cc,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='</td>';
	$html.='</tr>';

	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>Total Amount Due</strong>';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>$'.@number_format($balance_due,2,'.',',');
	$html.='</strong></td>';
	$html.='</tr>';

	$html.='</table>';
}

// legacy
if($invoice['is_legacy']==1)
{
	$invoice['memo']=trim($invoice['memo']);
	$invoice['memo']=$vujade->limit_words($invoice['memo']);
	$html.='<strong>Description: </strong><br>'.$invoice['memo'];

	# items and price
	$html.='<table width = "100%">';
	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>Item</strong>';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>Price</strong>';
	$html.='</td>';
	$html.='</tr>';

	if($invoice['advanced_deposit']==0)
	{
		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.=$selling_price_text;
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='$'.@number_format($sp,2,'.',',');
		$html.='</td>';
		$html.='</tr>';
	}
	else
	{
		$sp = $shop_order['selling_price'];
		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">Selling Price';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='$'.@number_format($sp,2,'.',',');
		$html.='</td>';
		$html.='</tr>';
	}

	// older legacy
	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">Manufacture & Install';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='$'.@number_format($manufacture,2,'.',',');
	$html.='</td>';
	$html.='</tr>';

	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">Labor';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='$'.@number_format($labor,2,'.',',');
	$html.='</td>';
	$html.='</tr>';

	if($parts>0)
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Parts';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($parts,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($engineering))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Engineering';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($engineering,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($permits))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Permits';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($permits,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($pa))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Permit Acquisition';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($pa,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($co))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Change Order';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($co,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($i4_amount))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.=$invoice['i4_label'];
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($i4_amount,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($shipping))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Shipping';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($shipping,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($tax_amount))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Sales Tax';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($tax_amount,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($discount))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Discount';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='-$'.@number_format($discount,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}
	if($advanced_deposit==1)
	{
		if(!empty($deposit))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Deposit Due';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $deposit=str_replace('-','',$deposit);
		    $html.='$'.@number_format($deposit,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}
	}
	if($advanced_deposit==0)
	{
		if(!empty($deposit))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Deposit';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $deposit=str_replace('-','',$deposit);
		    $html.='-$'.@number_format($deposit,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}
	}

	if(!empty($retention))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Retention';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='-$'.@number_format($retention,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($pt))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Payments';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='-$'.@number_format($pt,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	if(!empty($cc))
	{
	    $html.='<tr>';
	    $html.='<td width = "60%" valign = "top">';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='Credit Card Fees';
	    $html.='</td>';
	    $html.='<td width = "20%" valign = "top">';
	    $html.='$'.@number_format($cc,2,'.',',');
	    $html.='</td>';
	    $html.='</tr>';
	}

	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='</td>';
	$html.='</tr>';

	$html.='<tr>';
	$html.='<td width = "60%" valign = "top">';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>Total Amount Due</strong>';
	$html.='</td>';
	$html.='<td width = "20%" valign = "top">';
	$html.='<strong>$'.@number_format($balance_due,2,'.',',');
	$html.='</strong></td>';
	$html.='</tr>';
	
}

$html.='</table>';

# terms
$html.='<p><strong>Terms:</strong><br>';
$tms=$terms['terms'];
$html.=$tms;
$html.='</p>';

// special case for this customer
if($setup2['site']=="Sparkle Signs")
{
	$html.='<table><tr>';
	$html.='<td><img src = "images/bbb_logo.jpg" width = "50"></td>';
	$html.='<td><img src = "images/tsa_logo.jpg" width = "200"></td>';
	$html.='<td><img src = "images/isa_logo.jpg" width = "200"></td>';
	$html.='</tr></table>';
}

$html.='';

$html.='</div></div>';
$html.='</body></html>';

//print $html;
//die;

# mpdf class (pdf output)
require_once("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 10, 10, 10);
//$mpdf->debug = true; 
//$mpdf->showImageErrors = true;
$mpdf->setAutoTopMargin = 'stretch';
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->WriteHTML($html);

// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-invoice.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output($project_id.'.pdf','I'); 
}
?>