<?php 
session_start();
error_reporting(E_ALL & ~E_NOTICE);
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($estimates_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$estimateid=$_REQUEST['estimateid'];

$charset="ISO-8859-1";

require_once('mobile_detect.php');
$detect = new Mobile_Detect;

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# save the materials (next button was pressed)
if($action==1)
{
	$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];
	$items = $_POST['items'];
	$s = array();
	if(!empty($_POST['items']))
	{
	    foreach($_POST['items'] as $str)
	    {
	    	$pieces = explode('^', $str);
	    	$db_id=$pieces[0];
	    	$qty=$pieces[1];
	    	# update the quantity for each item
	    	$s[] = $vujade->update_row('estimates_material',$db_id,'qty',$qty);
	    }
	}
	$vujade->page_redirect('estimate_labor.php?project_id='.$project_id.'&estimateid='.$estimateid);
}

# previous button was pressed
if($action==2)
{
	//$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];
	$items = $_POST['items'];
	if(!empty($items))
	{
		$s = array();
	    foreach($_POST['items'] as $str)
	    {
	    	$pieces = explode('^', $str);
	    	$db_id=$pieces[0];
	    	$qty=$pieces[1];
	    	# update the quantity for each item
	    	$s[] = $vujade->update_row('estimates_material',$db_id,'qty',$qty);
	    }
	}
	$vujade->page_redirect('edit_estimate.php?project_id='.$project_id.'&estimateid='.$estimateid);
}

# recalculate
if($action==3)
{
	//$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];
	$items = $_POST['items'];
	$s = array();
    foreach($_POST['items'] as $str)
    {
    	$pieces = explode('^', $str);
    	$db_id=$pieces[0];
    	$qty=$pieces[1];
    	# update the quantity for each item
    	$s[] = $vujade->update_row('estimates_material',$db_id,'qty',$qty);
    }
}

// done button pressed
if($action==4)
{
	//$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];
	$items = $_POST['items'];
	if(!empty($items))
	{
		$s = array();
	    foreach($_POST['items'] as $str)
	    {
	    	$pieces = explode('^', $str);
	    	$db_id=$pieces[0];
	    	$qty=$pieces[1];
	    	# update the quantity for each item
	    	$s[] = $vujade->update_row('estimates_material',$db_id,'qty',$qty);
	    }
	}
	$vujade->page_redirect('project_estimates.php?id='.$project_id.'&estimateid='.$estimateid);
}

$estimate = $vujade->get_estimate($estimateid);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$section=3;
$menu=4;
$title = "Estimate Material - ";
$charset = "ISO-8859-1";
require_once('h.php');
?>
<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Estimate <?php print $estimate['estimate_id']; ?> Materials</a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

	<div class="admin-form theme-primary">

	    <?php 
	    $vujade->show_errors();
	    $vujade->show_messages();
	    ?>

	    <form method = "post" action = "estimate_material.php" id = "nd" class = "form-inline">
		<input type = "hidden" name = "action" id = "action" value = "">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
		<input type = "hidden" value = "<?php print $estimateid; ?>" name = "estimateid">
		<div class="panel heading-border panel-primary">
            <div class="panel-body bg-light">
		
				<!-- description -->
				<div style = "width:100%;height:50px;overflow-x:auto;margin-bottom:15px;">
					<?php print $estimate['description']; ?>
				</div>

				<!-- search bar -->
				<div style = "margin-bottom:10px;">
					<input type = "text" name = "filter" id = "filter" class = "form-control" style = "width:300px;">
					<select name = "filter_field" id = "filter_field" class = "form-control" style = "width:100px;">
						<option value = "5">ALL</option>
						<option value = "1">ID</option>
						<option value = "2">Description</option>
						<option value = "3">Size</option>
						<option value = "4">Cost</option>
					</select> 
					<a id = "filter_reset" class = "btn btn-primary">RESET</a>
				</div>

				<div class = "row">

					<!-- material list (left side) -->
					<div class = "col-md-6 spl-col-md-6">
						<div id = "inventory_table" >							
							<!-- table header -->
							<div class = "row little-small" style = "height:40px;background-color:#CADDF5;border:1px solid #5D9CEC;color:#2d6ca2;padding-top:10px;font-weight:bold;">
								<div class="col-md-3 col-sm-3 col-xs-3">
									ID
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									Description
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2">
									Size
								</div>
								<div class="col-md-2 col-sm-2 col-xs-2">
									Cost
								</div>
								<div class="col-md-1 col-sm-1 col-xs-1">
									&nbsp;
								</div>

							</div>	

							<!-- table body -->			
							<div style = "height:800px;overflow-y:auto;">
								<?php
								$materials = $vujade->get_inventory($sort=1,$start=1,$no_limit=1);
								if($materials['error']=="0")
								{
									unset($materials['error']);
									foreach($materials as $m)
									{
										if(empty($m['inventory_id']))
										{
											$m['inventory_id']=$m['database_id'];
										}
										print '<div class="row little-small" style = "border-bottom:1px solid #cecece;">';
										print '<div class="col-md-3 col-sm-3 col-xs-3">';
										print $m['inventory_id'];
										print '</div>';
										print '<div class="col-md-4 col-sm-4 col-xs-4">';
										print $m['description'];
										print '</div>';
										print '<div class="col-md-2 col-sm-2 col-xs-2">';
										print $m['size'];
										print '</div>';
										print '<div class="col-md-2 col-sm-2 col-xs-2">';
										$c = trim($m['cost']);
										$c = @number_format($c,2,'.',',');
										print '$'.$c;
										print '</div>';
										print '<div class="col-md-1 col-sm-1 col-xs-1">';
										print '<a href = "#" id = "'. $m['database_id'].'" class = "plus btn btn-primary btn-xs">+</a>';
										print '</div>';
										print '</div>';
									}
								}
								else
								{
									print '<div class = "alert alert-danger">';
									print($materials['error']);
									print '</div>';
								}
								?>
							</div>
						</div>
					</div>
				
					<!-- materials added to this estimate (right side) -->
					<div class = "col-md-6 spl-col-md-6" id = "e_items">						
						<div class = "row little-small" style = "height:40px;background-color:#CADDF5;border:1px solid #5D9CEC;color:#2d6ca2;padding-top:10px;font-weight:bold;"
						>
							<div class="col-md-2 col-sm-2 col-xs-2">
								ID
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3">
								Description
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								Cost
							</div>

							<!-- -->
							<div class="col-md-2 col-sm-2 col-xs-2 text-right">
								Meas
							</div>

							<div class="col-md-2 col-sm-2 col-xs-2">
								QTY
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								&nbsp;
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								&nbsp;
							</div>								
						</div>

						<div style="height:800px;overflow-y:auto; margin-top:3px;">
						<?php
						$materials_cost = 0;
						$items = $vujade->get_materials_for_estimate($estimateid);
						if($items['error']=="0")
						{
							unset($items['error']);							
							foreach($items as $i)
							{
								print '<div class="row little-small" style = "border-bottom:1px solid #cecece;">';

								// id
								print '<div class="col-md-2 col-sm-2 col-xs-2">';
								print $i['inventory_id'];
								print '</div>';

								// description
								print '<div class="col-md-3 col-sm-3 col-xs-3">';
								print $i['description'];
								print '</div>';

								// item cost
								print '<div class="col-md-1 col-sm-1 col-xs-1">';
								print '$'. @number_format($i['cost'],2,'.',',');
								print '</div>';

								/*  */
								// unit of measure
								print '<div class="col-md-2 col-sm-2 col-xs-2 text-right">';
								if($setup['is_qb']==1)
								{
									$line_item = $vujade->get_item($i['inventory_id'],'Name');
								}
								else
								{
									$line_item = $vujade->get_item($i['inventory_id'],'inventory_id');
								}
								print $line_item['uom'];
								print '</div>';

								// qty
								print '<div class="col-md-1 col-sm-1 col-xs-1">';

								// qty width for mobile
								if( ($detect->isMobile()) || ($detect->isTablet()) ) 
								{
									$style_width = "20";
								}
								else
								{
									// desktop can be wider
									$style_width = "40";
								}

								print '<input type = "text" name = "item_qty_'.$i['database_id'].'" id = "item_qty_'.$i['database_id'].'" value = "'.$i['qty'].'" class = "quantity" style = "width: '.$style_width.'px; padding: 0px;">';
								print '</div>';

								// line cost
								$line = $i['cost']*$i['qty'];
								print '<div class="col-md-2 col-sm-2 col-xs-2 ">';
								print '$'.@number_format($line,2,'.',',').' ';
								print '</div>';
								
								print '<div class="col-md-1 col-sm-1 col-xs-1 text-right cross-sign" style="padding-left: 10px;">';
								print '<a style = "" href = "#" id = "'. $i['database_id'].'" class = "plus-delete btn btn-danger btn-xs">X</a>';
								print '<input type = "hidden" name = "item_id_'.$i['database_id'].'" id = "item_id_'.$i['database_id'].'">';
								print '</div>';
								print '</div>';
								$materials_cost = $materials_cost + $line;
							}
							print '</div>';

							print '<div align = "right" style = "background-color:#FAFAFA;height:30px;padding-top:15px;">Materials Total: $'.@number_format($materials_cost,2,'.',',');
							print '<a href = "#" class = "plus-update btn-primary btn-xs" style = "margin-left:5px;">Recalculate</a>';
							print '</div>';
						}
						else
						{
							print '<div class = "alert alert-warning">No materials have been added to this estimate yet.</div>';
						}
						?>
						</div>
					</div>

				</div>
			</div>
		</div>

				<!-- buttons at bottom -->
				<div style = "margin-top:15px;">
					<a class = "btn btn-primary" href = "#" id = "prev" style = "">PREVIOUS</a> 

					<a class = "btn btn-success" href = "#" id = "next" style = "margin-left:15px;">SAVE AND CONTINUE</a> 

					<a class = "btn btn-success" href = "#" id = "done" style = "margin-left:15px;">SAVE AND COMPLETE</a> 
	
					</form>
		        </div>

			</div>
		</div>
	</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  //var estimateid = <?php print $estimateid; ?>;

  $(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // plus button
    $(document).on('click', '.plus', function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#e_items').html('');
		$('#e_items').html(loading);
		var estimate_id = "<?php print $estimateid; ?>";
		var item_id = this.id;
		$.post( "jq.estimate_items.php", { action: 1, estimate_id: estimate_id, item_id: item_id })
		  .done(function( data ) 
		  {
		  		$('#e_items').html(data);
		  });
	});

    // delete button
    $(document).on("click", ".plus-delete", function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#e_items').html('');
		$('#e_items').html(loading);
		var estimate_id = "<?php print $estimateid; ?>";
		var db_id = this.id;
		$.post( "jq.estimate_items.php", { action: 3, estimate_id: estimate_id, item_id: 0, db_id: db_id })
		  .done(function( data ) 
		  {
		  		$('#e_items').html(data);
		  });
	});

    // recalculate button
	$(document).on('click', '.plus-update', function(e)
	{

		e.preventDefault();

		// this gets each item quantity
		var qtys = $('.quantity').map(function(index) 
		{
		    return this.value; 
		});
		
		// this gets each item id
		var ids = $('.plus-delete').map(function(index) 
		{
		    return this.id; 
		});

		// this combines both into an array with the id as the key, quantity as the value
		var array3 = {};
		$.each(ids, function(i) {
		    array3[ids[i]] = qtys[i];
		});

		// this loops through all then creates hidden form fields containing only the id and the qty
		$.each(array3, function( index, value ) 
		{
			//alert( index + ": " + value );
			//concat = concat+
			$('<input>').attr({
			    type: 'hidden',
			    id: 'items',
			    name: 'items[]',
			    value: index+"^"+value
			}).appendTo('#nd');
		});

		$('#action').val('3');
		$('#nd').submit();
	});

	// previous button
	$('#prev').click(function()
	{
		// this gets each item quantity
		var qtys = $('.quantity').map(function(index) 
		{
		    return this.value; 
		});
		
		// this gets each item id
		var ids = $('.plus-delete').map(function(index) 
		{
		    return this.id; 
		});

		// this combines both into an array with the id as the key, quantity as the value
		var array3 = {};
		$.each(ids, function(i) {
		    array3[ids[i]] = qtys[i];
		});

		// this loops through all then creates hidden form fields containing only the id and the qty
		$.each(array3, function( index, value ) 
		{
			//alert( index + ": " + value );
			//concat = concat+
			$('<input>').attr({
			    type: 'hidden',
			    id: 'items',
			    name: 'items[]',
			    value: index+"^"+value
			}).appendTo('#nd');
		});

		$('#action').val('2');
		$('#nd').submit();
	});

	// next button
	$('#next').click(function()
	{
		// this gets each item quantity
		var qtys = $('.quantity').map(function(index) 
		{
		    return this.value; 
		});
		
		// this gets each item id
		var ids = $('.plus-delete').map(function(index) 
		{
		    return this.id; 
		});

		// this combines both into an array with the id as the key, quantity as the value
		var array3 = {};
		$.each(ids, function(i) {
		    array3[ids[i]] = qtys[i];
		});

		// this loops through all then creates hidden form fields containing only the id and the qty
		$.each(array3, function( index, value ) 
		{
			//alert( index + ": " + value );
			//concat = concat+
			$('<input>').attr({
			    type: 'hidden',
			    id: 'items',
			    name: 'items[]',
			    value: index+"^"+value
			}).appendTo('#nd');
		});

		$('#action').val('1');
		$('#nd').submit();
	});

	// next button
	$('#done').click(function()
	{
		// this gets each item quantity
		var qtys = $('.quantity').map(function(index) 
		{
		    return this.value; 
		});
		
		// this gets each item id
		var ids = $('.plus-delete').map(function(index) 
		{
		    return this.id; 
		});

		// this combines both into an array with the id as the key, quantity as the value
		var array3 = {};
		$.each(ids, function(i) {
		    array3[ids[i]] = qtys[i];
		});

		// this loops through all then creates hidden form fields containing only the id and the qty
		$.each(array3, function( index, value ) 
		{
			//alert( index + ": " + value );
			//concat = concat+
			$('<input>').attr({
			    type: 'hidden',
			    id: 'items',
			    name: 'items[]',
			    value: index+"^"+value
			}).appendTo('#nd');
		});

		$('#action').val('4');
		$('#nd').submit();
	});

	// filter the list of materials
	$("form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) 
        {
            $('button[type=submit] .default').click();
            //alert('enter or return key clicked... filtering...');
        	
        	var nextid="<?php print $estimateid; ?>";
			var filter = $('#filter').val();
			var filter_field = $('#filter_field').val();
			var loading = '<i class="fa fa-cog fa-spin"></i>';
			$('#inventory_table').html('');
			$('#inventory_table').html(loading);
			$.post( "jq.inventory_filter.php", { filter: filter, filter_field: filter_field, reset: 0, estimate_id: nextid })
			  .done(function( data ) 
			  {
			  		$('#inventory_table').html(data);
			  });    
            return false;
        }
    });

	$('#filter_reset').click(function(e)
	{
		e.preventDefault();
		$('#filter').val('');
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#itable').remove();
		$('#inventory_table').html('');
		$('#inventory_table').html(loading);
		var nextid="<?php print $estimateid; ?>";
		$.post( "jq.inventory_filter.php", { reset: 1, estimate_id: nextid })
		  .done(function( data ) 
		  {
		  		$('#inventory_table').html(data);
		  });
	});
	
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
