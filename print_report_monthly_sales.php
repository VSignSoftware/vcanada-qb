<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

ini_set("memory_limit","300M");

## permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Proposals');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();
$option_start = 2014;
$option_current_year = date('Y');

if(!isset($_REQUEST['date']))
{
	// start and end of the current month
	$test_date = date('m/d/Y');
	$date = date('m/01/Y');
}
else
{
	// user selected month and year or fiscal year
	$test_date = $_REQUEST['date'];
	$date = $_REQUEST['date'];

	// fiscal year
	if($test_date[0]=="F")
	{
		$year_value = $test_date[3].$test_date[4].$test_date[5].$test_date[6];
		$date="Fiscal Year ".$year_value;
		$fiscal_year_search=true;
	}
}

if(!$fiscal_year_search)
{
	// first day of the month
	$first = date('Y-m-01', strtotime($test_date));

	// last day of the month
	$last = date('Y-m-t', strtotime($test_date));

	$start = strtotime($first);
	$end = strtotime($last);
}
else
{
	// get by fiscal year

	// fiscal year does not start on Jan. 1
	if($setup['fiscal_year_start']!=1)
	{	
		// any year in the past 
		if($year_value<$option_current_year)
		{
			// start
			$y=$year_value-1;

			// end
			$y2=$year_value;
		}

		// current year
		if($year_value==$option_current_year)
		{
			// start
			$y=$option_current_year-1;

			// end
			$y2=$year_value;
		}

		// next year
		if($year_value>$option_current_year)
		{
			// start
			$y=$option_current_year;

			// end
			$y2=$year_value;
		}

		$m=$setup['fiscal_year_start'];
		$m2=$setup['fiscal_year_end'];
		$start = strtotime($y.'-'.$m.'-01');
		$date1 = $y2.'-'.$m2; 
		$d = date_create_from_format('Y-m',$date1); 
		$last_day = date_format($d, 't');
		$end = strtotime($y2.'-'.$m2.'-'.$last_day);
	}
	else
	{
		$y=$year_value;
		$m=$setup['fiscal_year_start'];
		$m2=$setup['fiscal_year_end'];
		$start = strtotime($y.'-'.$m.'-01');
		$date1 = $y.'-'.$m2; 
		$d = date_create_from_format('Y-m',$date1); 
		$last_day = date_format($d, 't');
		$end = strtotime($y2.'-'.$m2.'-'.$last_day);
	}
}

$projects = $vujade->get_projects_for_monthly_sales_report($start,$end);
if($projects['error']=="0")
{

	$title = 'Monthly Sales Report';
	$html = '<html><head><title>Monthly Sales Report</title>';
	$html.='<style>';
	$html.='
	body
	{
		font-size:12px;
		font-family:arial;
		color:black;
	}
	table 
	{
	    border-collapse: collapse;
	}
	table, td, th 
	{
	    border: 1px solid #cecece;
	}
	td
	{

	}
	';
	$html.='</style>';
	$html.='</head><body>';
	$header_1='<h2>Monthly Sales Report for '.$date.'</h2>';

	// determine which sales people to use
	$salespeople = array();
	foreach($projects as $project)
	{
		$salespeople[]=$project['salesperson'];
	}	
	$salespeople=array_unique($salespeople);		
	$salespeople['error']=0;
	//unset($salespeople['error']);
	$html.='
	<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
			<thead>
				<tr style = "border-bottom:1px solid black;">
					<td valign = "top"><strong>Open Date</strong></td>
					<td valign = "top"><strong>Job #</strong></td>
					<td valign = "top"><strong>Type</strong></td>
					<td valign = "top"><strong>Job Name</strong></td>';
					if($salespeople['error']=="0")
					{
						unset($salespeople['error']);
						foreach($salespeople as $sp)
						{
							if(!empty($sp))
							{
								$html.='<td valign = "top"><strong>'.$sp.'</strong></td>';
							}
						}
					}
					else
					{
						$html.='<td valign = "top"><strong><font color = "red">No sales people</font></strong></td>';
					}
	$html.='</tr></thead>';
	$html.='<tbody style = "font-size:14px;">';
	if($projects['error']=="0")
	{
		unset($projects['error']);		
		$html.='<tbody style = "font-size:14px;">';
		foreach($projects as $i)
		{

			$line_total=$i['line_total'];

			$link = 'project.php?id='.$i['project_id'];

	        $html.='<tr class = "clickableRow-row">';

	        // date
	        $html.='<td valign = "top">'.$i['open_date'].'</td>';

	        // job number
	        $html.='<td valign = "top">'.$i['project_id'].'</td>';

	        // type
	        $html.='<td valign = "top">'.$i['type'].'</td>';

	        // job name
	        $job_name=$vujade->trim_string($i['site'],30);
	        $html.='<td valign = "top">'.$job_name.'</td>';

	        // salespeople
	        if(count($salespeople)>0)
			{
				foreach($salespeople as $sp)
				{
					if(!empty($sp))
					{
						$html.='<td class="clickableRow line-total" id = '.$sp.' href="'.$link.'" valign = "top" style = "">';
						if($i['salesperson']==$sp)
						{
							$spsarray[]=$sp.'^'.$line_total;
							$html.='$'.$line_total;
						}
			        	$html.='</td>';
			        }
				}
			}
			else
			{
				$html.='<td class="clickableRow" href="'.$link.'" valign = "top" style = "">';
	        	$html.='</td>';
			}
		}
		//$html.='</tbody></table>';

		$html.='<tr>
					<td colspan="4">
					&nbsp;
					</td>';
			
		$total = 0;
		if(count($salespeople)>0)
		{
			foreach($salespeople as $sp)
			{
				if(!empty($sp))
				{
					$html.='<td><div style = "text-align:right;">';

					$n = $sp;
					$html.=$n.': $';
	        		$sp_total = 0;
	        		foreach($spsarray as $sps)
	        		{
	        			$line = explode('^',$sps);
	        			if($line[0]==$n)
	        			{
	        				$line[1]=str_replace(',','',$line[1]);
	        				$sp_total+=$line[1];
	        			}
	        		}
	        		$html.=@number_format($sp_total,2);
	        		$total+=$sp_total;
	        		$html.='</div></td>';
	        	}
			}
		}
		$html.='</tr>';
		$cols = count($salespeople);
		$cols+=2;
		$html.='<tr><td colspan ="'.$cols.'">&nbsp;</td><td><div style = "text-align:right;">Total: $'.@number_format($total,2).'</td></tr>';
		$html.='</tbody></table>';
	}
	else
	{
		$html.='No projects found.';
	}
	$html.="</tbody></table></body></html>";




	//print $html;
	//die;

	$footer_1 = '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';

	# mpdf class (pdf output)
	include("mpdf60/mpdf.php");
	$mpdf = new mPDF('', 'LETTER-L', 0, 'Helvetica', 10, 10, 20, 10, 10);
	$mpdf->DefHTMLHeaderByName('header_1',$header_1);
	$mpdf->SetHTMLHeaderByName('header_1');
	$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
	$mpdf->SetHTMLFooterByName('footer_1');
	$mpdf->WriteHTML($html);

	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-monthly-sales-report.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
	 	$mpdf->Output($pdfname,'F');
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		$mpdf->Output('Monthly Sales Report.pdf','I');  
	}
}
else
{
	$vujade->set_error('No projects found. ');
	$vujade->set_error($projects['error']);
	$vujade->show_errors();
}
?>