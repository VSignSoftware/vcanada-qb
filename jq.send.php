<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// employee info
$employee=$vujade->get_employee($_SESSION['user_id']);

// id
$id = $_REQUEST['id'];

// type
$type = $_REQUEST['type'];

// invoices
if($type == "invoice")
{
	$logo_data = $vujade->get_logo(3);
	$logo = '<img src = "'.$logo_data['url'].'" width = "260" height = "60">';

	$setup = $vujade->get_invoice_company_setup(2);
	$setup2 = $vujade->get_setup();
	$invoice_database_id=$id;
	$invoiceid=$id;
	$invoice = $vujade->get_invoice($invoice_database_id);

	$project_id = $invoice['project_id'];
	$project = $vujade->get_project($project_id,2);

	# get correct customer info for this project
	$listiderror=0;
	$localiderror=0;
	// try to get by list id
	$customer1 = $vujade->get_customer($project['client_id'],'ListID');
	if($customer1['error']!="0")
	{
	    $listiderror++;
	    unset($customer1);
	}
	else
	{
	    $customer=$customer1;
	}
	// try to get by local id
	$customer2 = $vujade->get_customer($project['client_id'],'ID');
	if($customer2['error']!="0")
	{
	    $localiderror++;
	    unset($customer2);
	}
	else
	{
	    $customer=$customer2;
	}

	$iderror=$listiderror+$localiderror;

	if($iderror<2)
	{
	    $customer_contact = $vujade->get_contact($project['client_contact_id']);
	}
	else
	{
	    // can't find customer or no customer on file
	}

	$terms = $vujade->get_terms(3);
	$notes = $vujade->get_terms(4);
	$shop_order = $vujade->get_shop_order($project_id,'project_id');

	$tax_rate=$invoice['v_tax_rate'];
	$advanced_deposit=$invoice['advanced_deposit'];

	// subtotal 1 (selling price)
	$sp=0;

	// not legacy
	if($invoice['is_legacy']==0)
	{
		$sp = $invoice['v_sales_price'];

		// tax amount
		$tax_amount = $invoice['v_tax_amount'];

		// i4 amount
		$i4_amount = $invoice['i4_amount'];

		// engineering
		$engineering = $invoice['v_engineering'];

		// permits
		$permits = $invoice['v_permits'];

		// permit acq
		$pa = $invoice['v_permit_acquisition'];

		// shipping
		$shipping = $invoice['v_shipping'];

		// discount
		$discount = $invoice['v_discount'];

		// retention
		$retention = $invoice['v_retention'];

		// deposit
		$deposit = $invoice['v_deposit'];

		// cc processing fees
		$cc = $invoice['v_cc_processing'];
	}

	// legacy
	if($invoice['is_legacy']==1)
	{
		//print_r($invoice);
		//die;
		// invoice types
		// 1: illuminated and lump sum
		if( ($invoice['type_1']==1) && ($invoice['type_2']==1) )
		{
			// labor
			$labor = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			// total
			$sp = $labor+$parts; 

			$parts=0;

			// selling price text
			$selling_price_text = "Selling Price";
		}

		// 2: illuminated sign parts and labor
		if( ($invoice['type_1']==1) && ($invoice['type_2']==2) )
		{
			// labor
			$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			// selling price text
			$selling_price_text = "Labor Only Price";
		}

		// 3: non-illuminated lump sum
		if( ($invoice['type_1']==2) && ($invoice['type_2']==1) )
		{
			$sp = $vujade->get_invoice_line_items($invoiceid,'Manufacture & Install',true);

			$parts=0;

			// selling price text
			$selling_price_text = "Selling Price";
		}

		// 4: non-illuminated sign parts and labor
		if( ($invoice['type_1']==2) && ($invoice['type_2']==2) )
		{
			// labor
			$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			// selling price text
			$selling_price_text = "Labor Only Price";
		}

		// 5: service with parts and labor
		if( ($invoice['type_1']==3) && ($invoice['type_2']==2) )
		{
			// labor
			$sp = $vujade->get_invoice_line_items($invoiceid,'Labor Only',true);

			// parts 
			$parts = $vujade->get_invoice_line_items($invoiceid,'Parts',true);

			// selling price text
			$selling_price_text = "Labor Only Price";
		}

		// tax amount
		// this will be changing in the future
		// $tax_amount = $vujade->get_invoice_line_items($invoiceid,'Sales Tax',true);
		$tax_amount=$invoice['sales_tax'];

		// change orders
		$co = $vujade->get_invoice_line_items($invoiceid,'Change Order',true);

		// non-taxable co
		$ntco = $vujade->get_invoice_line_items($invoiceid,'Non-Taxable CO',true);
		$co+=$ntco;

		// engineering
		$engineering = $vujade->get_invoice_line_items($invoiceid,'Engineering',true);

		// permits
		$permits = $vujade->get_invoice_line_items($invoiceid,'Permits',true);

		// permit acq
		$pa = $vujade->get_invoice_line_items($invoiceid,'Permit Acquisition',true);

		// shipping
		$shipping = $vujade->get_invoice_line_items($invoiceid,'Shipping',true);

		// discount
		$discount=$vujade->get_invoice_line_items($invoiceid,'Discount',true);

		// retention
		$retention = $vujade->get_invoice_line_items($invoiceid,'Retention',true);

		// deposit
		$deposit = $vujade->get_invoice_line_items($invoiceid,'Deposit',true);

		// cc processing fees
		$cc = $vujade->get_invoice_line_items($invoiceid,'CC Processing Fee',true);

	}

	// payments
	$payments = $vujade->get_invoice_payments($invoiceid);
	if($payments['error']=="0")
	{
	    unset($payments['error']);
	    $pt = 0;
	    foreach($payments as $payment)
	    {
	        
	        $pt+=$payment['amount'];
	    }
	}
	else
	{
	    $pt=0;
	}

	// credit memos
	$credit_memos=$vujade->get_credit_memos($invoice['invoice_id']);
	if($credit_memos['count']>0)
	{
		unset($credit_memos['sql']);
		unset($credit_memos['error']);
		unset($credit_memos['count']);
		foreach($credit_memos as $credit_memo)
		{
			$pt+=$credit_memo['amount'];
		}
	}

	// subtotal 
	$subtotal2=$sp+$tax_amount;

	$column_1_st=$sp+$engineering+$permits+$pa+$co+$shipping+$parts+$cc+$i4_amount;
	$column_1_st=$column_1_st+$discount;
	$column_2_st=$column_1_st+$tax_amount;
	$column_2_st_2=$column_2_st-$deposit-$retention;

	if($invoice['is_legacy']==1)
	{
		$balance_due=$column_2_st_2-$pt;
	}
	else
	{
		if($invoice['advanced_deposit']==1)
		{
			$balance_due=$deposit-$pt;
		}
		else
		{
			$balance_due=$invoice['v_balance_due']-$pt;
		}
	}

	$charset="ISO-8859-1";
	$html = '
	            <!DOCTYPE html>
	            <head>
	            <meta charset="'.$charset.'">
	            <meta http-equiv="X-UA-Compatible" content="IE=edge">
	            <title>Invoice</title>
	            <meta name="description" content="">
	            <meta name="viewport" content="width=device-width, initial-scale=1">
	            <link rel="stylesheet" href="css/print_invoice_style.css">
	            <style>
			    .marker
			    {
			    	background-color: yellow;
			    }
			    </style>
	            </head>
	            <body>
	            <div id="mainContent">
	            <div id="content">';

	# logo and invoice number
	$html.='<table width = "100%">';
	$html.= '<tr>';
	$html.='<td valign = "top" width = "70%">';
	$html.=$logo;
	$html.='</td>';
	$html.='<td valign = "top" width = "30%">';
	$html.='<h1><center>Invoice</center></h1>';
	$html.='
	        <table width = "100%">
	        <tr>
	        <td class = "firstbox" valign = "top" width = "50%"><center>Date</center></td>
	        <td class = "firstbox" valign = "top" width = "50%"><center>Invoice #</center></td>
	        </tr>
	        <tr>
	        <td class = "firstbox"><center>'.$invoice['date'].'</center></td>
	        <td class = "firstbox"><center>'.$invoice['invoice_id'].'</center></td>
	        </tr>
	        </table>
	';

	$html.='</td>';
	$html.='</tr>';
	$html.='</table>';

	if(!empty($setup['address_1']))
	{
		$html.='<table width = "100%">';
		$html.= '<tr>';
		$html.='<td valign = "top" width = "50%">';
		$html.=$setup['name'];
		$html.='</td>';
		$html.='<td valign = "top" width = "50%" class = "alignright" style = "padding-right:30px;">Contractor #';
		$html.=$setup['license_number'];
		$html.='</td>';
		$html.='</tr>';
		$html.= '<tr>';
		$html.='<td valign = "top" style = "">';
		$compinfo = $setup['address_1'];
		if(!empty($setup['address_2']))
		{
		    $compinfo .= ", ".$setup['address_2'].", ";
		}
		$compinfo .= ' '.$setup['city'].', '.$setup['state'].' '.$setup['zip'].'<br>';
		$html.=$compinfo;
		$html.='</td>';
		$html.='<td valign = "top" width = "50%" class = "alignright" style = "padding-right:30px;">&nbsp;';
		$html.='</td>';
		$html.='</tr>';
		$html.= '<tr>';
		$html.='<td valign = "top" style = "">';
		$html.=$setup['phone'];
		$html.='</td>';
		$html.='<td valign = "top" width = "50%" class = "alignright" style = "padding-right:30px;">&nbsp;';
		$html.='</td>';
		$html.='</tr>';
		$html.='</table>';
	}

	# bill to box, job site box
	$html.='
	        <table width = "100%">
	        <tr>

	        <td width = "45%" valign = "top" class = "firstbox">
	        <center>
	        Bill To
	        </center>
	        </td>

	        <td width = "10%" valign = "top">&nbsp;
	        </td>

	        <td width = "45%" valign = "top" class = "firstbox">
	        <center>
	        Job site
	        </center>
	        </td>
	        </tr> 

	        <tr>
	        <td class = "firstbox">
	            <center>'.$customer['name'].'<br>
	            '.$customer['address_1'];
	            if(!empty($customer['address_2']))
	            {
	                $html.=', '.$customer['address_2'];
	            }
	            $html.='<br>'.$customer['city'].', '.$customer['state'].' '.$customer['zip'].'
	        </center>
	        </td>

	        <td width = "10%" valign = "top">&nbsp;
	        </td>

	        <td class = "firstbox">
	        <center>'.$project['site'].'<br>
	        '.$project['address_1'];
	        if(!empty($project['address_2']))
	        {
	            $html.=', '.$project['address_2'];
	        }
	        $html.='<br>'.$project['city'].', '.$project['state'].' '.$project['zip'].'
	        </center>
	        </td>
	        </tr> 
	        </table>
	';

	# po number, rep, ordered by
	$html.='
	        <table width = "100%">
	        <tr>
	        <td width = "33%" valign = "top" class = "firstbox">
	        <center>
	        P.O. Number
	        </center>
	        </td>

	        <td width = "33%" valign = "top" class = "firstbox">
	        <center>
	        Rep
	        </center>
	        </td>

	        <td width = "33%" valign = "top" class = "firstbox">
	        <center>
	        Ordered By
	        </center>
	        </td>
	        </tr>

	        <tr>
	        <td width = "33%" valign = "top" class = "firstbox">
	        <center>'.$invoice['po_number'].'
	        </center>
	        </td>

	        <td width = "33%" valign = "top" class = "firstbox">
	        <center>'.$project['salesperson'].'
	        </center>
	        </td>

	        <td width = "33%" valign = "top" class = "firstbox">
	        <center>'.$project['project_contact'].'
	        </center>
	        </td>
	        </tr>
	        </table>
	';

	if($invoice['advanced_deposit']==1)
	{
	    $html.='<center><strong><font style = "color:red;font-size:16px;">** Advance Deposit Billing **</font></strong></center><br>';
	}

	// not legacy
	if($invoice['is_legacy']==0)
	{
		// line items
		$lines = $vujade->get_invoice_line_items($invoice['database_id'],'',false,2);
		if($lines['count']>0)
		{
			unset($lines['count']);
			unset($lines['error']);
			$html.='<table width = "100%">';
			$html.='<tr><td><b>Item</b></td><td><b>Amount</b></td></tr>';
			foreach($lines as $line)
			{
				$html.='<tr>';
				$html.='<td width = "80%">';
				$html.=$line['line_description'];
				$html.='</td>';
				if(!empty($line['line_amount']))
				{
					$html.='<td>$';
					$html.=number_format($line['line_amount'],2);
					$html.='</td>';
				}
				else
				{
					$html.='<td>&nbsp;';
					$html.='</td>';
				}
				$html.='</tr>';
			}
			$html.='</table>';
		}

		// amounts
		$html.='<table width = "100%">';
		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>Item</strong>';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>Price</strong>';
		$html.='</td>';
		$html.='</tr>';

		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">Sales Price';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='$'.@number_format($sp,2,'.',',');
		$html.='</td>';
		$html.='</tr>';

		if(!empty($engineering))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Engineering';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($engineering,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($permits))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Permits';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($permits,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($pa))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Permit Acquisition';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($pa,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($co))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Change Order';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($co,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($i4_amount))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.=$invoice['i4_label'];
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($i4_amount,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($shipping))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Shipping';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($shipping,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($tax_amount))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Sales Tax';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($tax_amount,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($discount))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Discount';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($discount,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}
		if($advanced_deposit==1)
		{
			if(!empty($deposit))
			{
			    $html.='<tr>';
			    $html.='<td width = "60%" valign = "top">';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $html.='Deposit Due';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $html.='$'.@number_format($deposit,2,'.',',');
			    $html.='</td>';
			    $html.='</tr>';
			}
		}
		if($advanced_deposit==0)
		{
			if(!empty($deposit))
			{
			    $html.='<tr>';
			    $html.='<td width = "60%" valign = "top">';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $html.='Deposit';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $html.='-$'.@number_format($deposit,2,'.',',');
			    $html.='</td>';
			    $html.='</tr>';
			}
		}

		if(!empty($retention))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Retention';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($retention,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($pt))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Payments';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($pt,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		//print $balance_due.' '.$pt;
		//die;
		//if($advanced_deposit==1)
		//{
		//	$balance_due=$balance_due+$cc-$pt;
		//}

		if(!empty($cc))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Credit Card Fees';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($cc,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='</td>';
		$html.='</tr>';

		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>Total Amount Due</strong>';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>$'.@number_format($balance_due,2,'.',',');
		$html.='</strong></td>';
		$html.='</tr>';

		$html.='</table>';
	}

	// legacy
	if($invoice['is_legacy']==1)
	{
		$invoice['memo']=trim($invoice['memo']);
		$invoice['memo']=$vujade->limit_words($invoice['memo']);
		$html.='<strong>Description: </strong><br>'.$invoice['memo'];

		# items and price
		$html.='<table width = "100%">';
		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>Item</strong>';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>Price</strong>';
		$html.='</td>';
		$html.='</tr>';

		if($invoice['advanced_deposit']==0)
		{
			$html.='<tr>';
			$html.='<td width = "60%" valign = "top">';
			$html.='</td>';
			$html.='<td width = "20%" valign = "top">';
			$html.=$selling_price_text;
			$html.='</td>';
			$html.='<td width = "20%" valign = "top">';
			$html.='$'.@number_format($sp,2,'.',',');
			$html.='</td>';
			$html.='</tr>';
		}
		else
		{
			$sp = $shop_order['selling_price'];
			$html.='<tr>';
			$html.='<td width = "60%" valign = "top">';
			$html.='</td>';
			$html.='<td width = "20%" valign = "top">Selling Price';
			$html.='</td>';
			$html.='<td width = "20%" valign = "top">';
			$html.='$'.@number_format($sp,2,'.',',');
			$html.='</td>';
			$html.='</tr>';
		}

		// older legacy
		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">Manufacture & Install';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='$'.@number_format($manufacture,2,'.',',');
		$html.='</td>';
		$html.='</tr>';

		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">Labor';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='$'.@number_format($labor,2,'.',',');
		$html.='</td>';
		$html.='</tr>';

		if($parts>0)
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Parts';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($parts,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($engineering))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Engineering';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($engineering,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($permits))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Permits';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($permits,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($pa))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Permit Acquisition';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($pa,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($co))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Change Order';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($co,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($i4_amount))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.=$invoice['i4_label'];
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($i4_amount,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($shipping))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Shipping';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($shipping,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($tax_amount))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Sales Tax';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($tax_amount,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($discount))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Discount';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($discount,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}
		if($advanced_deposit==1)
		{
			if(!empty($deposit))
			{
			    $html.='<tr>';
			    $html.='<td width = "60%" valign = "top">';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $html.='Deposit Due';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $deposit=str_replace('-','',$deposit);
			    $html.='$'.@number_format($deposit,2,'.',',');
			    $html.='</td>';
			    $html.='</tr>';
			}
		}
		if($advanced_deposit==0)
		{
			if(!empty($deposit))
			{
			    $html.='<tr>';
			    $html.='<td width = "60%" valign = "top">';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $html.='Deposit';
			    $html.='</td>';
			    $html.='<td width = "20%" valign = "top">';
			    $deposit=str_replace('-','',$deposit);
			    $html.='-$'.@number_format($deposit,2,'.',',');
			    $html.='</td>';
			    $html.='</tr>';
			}
		}

		if(!empty($retention))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Retention';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($retention,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($pt))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Payments';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='-$'.@number_format($pt,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		if(!empty($cc))
		{
		    $html.='<tr>';
		    $html.='<td width = "60%" valign = "top">';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='Credit Card Fees';
		    $html.='</td>';
		    $html.='<td width = "20%" valign = "top">';
		    $html.='$'.@number_format($cc,2,'.',',');
		    $html.='</td>';
		    $html.='</tr>';
		}

		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='</td>';
		$html.='</tr>';

		$html.='<tr>';
		$html.='<td width = "60%" valign = "top">';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>Total Amount Due</strong>';
		$html.='</td>';
		$html.='<td width = "20%" valign = "top">';
		$html.='<strong>$'.@number_format($balance_due,2,'.',',');
		$html.='</strong></td>';
		$html.='</tr>';
		
	}

	$html.='</table>';

	# terms
	$html.='<p><strong>Terms:</strong><br>';
	$tms=$terms['terms'];
	$html.=$tms;
	$html.='</p>';

	// special case for this customer
	if($setup2['site']=="Sparkle Signs")
	{
		$html.='<table><tr>';
		$html.='<td><img src = "images/bbb_logo.jpg" width = "50"></td>';
		$html.='<td><img src = "images/tsa_logo.jpg" width = "200"></td>';
		$html.='<td><img src = "images/isa_logo.jpg" width = "200"></td>';
		$html.='</tr></table>';
	}

	$html.='';

	$html.='</div></div>';
	$html.='</body></html>';

	//print $html;
	//die;

	# mpdf class (pdf output)
	include("mpdf60/mpdf.php");
	$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 10, 10, 10);
	$mpdf->setAutoTopMargin = 'stretch';
	$mpdf->setAutoBottomMargin = 'stretch';
	$mpdf->WriteHTML($html);
	$mpdf->Output('emailed_files/'.$invoice['invoice_id'].'.pdf','F'); 

	# build and send an email
	$send_to = $_REQUEST['send_to'].',';
	$send_to= preg_replace('/\s+/','',$send_to);
	$send_to = explode(',',$send_to);
	$subject = $_REQUEST['subject'];
	$msg = str_replace("\n", "<br>", $_REQUEST['message']);
	$sig = str_replace("\n", "<br>", $employee['email_signature']);
	$msg.= "<br><br><br><br><br>".$sig; 
	$msg.= "<br><br><br><br><br><br><br><br><br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
	$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "http://www.vsignsoftware.com/images/email_logo.png"></a></p>';
	require 'phpmailer/PHPMailerAutoload.php';
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = $vujade->smtp_host;
	$mail->Port = 25;
	$mail->SMTPAuth = false;
	$mail->Sender="vsign@vujade.net";
	$mail->setFrom($employee['email_reply_to'], $employee['fullname'],false);
	$mail->addReplyTo($employee['email_reply_to'], $employee['fullname']);
	foreach($send_to as $email)
	{
		if(!empty($email))
		{
			$mail->addAddress($email, 'Customer');
		}
	}

	// send bcc if employee has enabled this
	if( ($employee['bcc']==1) && (!empty($employee['email_reply_to'])) )
	{
		$mail->addBCC($employee['email_reply_to'], $employee['fullname']);
	}

	$mail->Subject = $subject;
	$mail->msgHTML($msg);
	$mail->AltBody = 'This is a plain-text message body';
	$mail->addAttachment('emailed_files/'.$invoice['invoice_id'].'.pdf');
	if (!$mail->send()) 
	{
	    echo "Mailer Error: " . $mail->ErrorInfo;
	} 
	else 
	{
	    echo "Message sent!";
	}
	// delete the file
	//unlink('emailed_files/'.$invoice['invoice_id'].'.pdf');
}

// proposals
if($type == "proposal")
{
	$proposal = $vujade->get_proposal($id);
	// can't be invalid
	if($proposal['error']!="0")
	{
		$vujade->errors[]="Invalid proposal.";
		$vujade->show_errors();
		die;
	}
	else
	{
		# generate the pdf (proposal)
		$setup = $vujade->get_setup();
		$proposal_database_id=$id;
		$project = $vujade->get_project($proposal['project_id'],2);
		$project_id = $proposal['project_id'];
		# get billing info for this project
		if($setup['is_qb']==1)
		{
			$listiderror=0;
			$localiderror=0;

			// try to get by list id
			$customer1 = $vujade->get_customer($project['client_id'],'ListID');
			if($customer1['error']!="0")
			{
			    $listiderror++;
			    unset($customer1);
			}
			else
			{
			    $customer=$customer1;
			}
			// try to get by local id
			$customer2 = $vujade->get_customer($project['client_id'],'ID');
			if($customer2['error']!="0")
			{
			    $localiderror++;
			    unset($customer2);
			}
			else
			{
			    $customer=$customer2;
			}

			$iderror=$listiderror+$localiderror;

			if($iderror<2)
			{
			    $customer_contact = $vujade->get_contact($project['client_contact_id']);
			}
			else
			{
			// can't find customer or no customer on file

			}
		}
		else
		{
			$customer = $vujade->get_customer($project['client_id']);
		}
		$customer_contact = $vujade->get_contact($project['client_contact_id']);

		$total = 0;
		$tax_total = 0;
		$tax_rate = 0;

		$items = $vujade->get_items_for_proposal($proposal['proposal_id']);
		$terms = $proposal['terms'];



		// $logo = $vujade->get_logo(2);
		$logo = "images/proposal_logo.jpg";

		$charset="ISO-8859-1";
		$html = '<!DOCTYPE html>
		    <head>
		    <meta charset="'.$charset.'">
		    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		    <title>Proposal</title>
		    <meta name="description" content="">
		    <meta name="viewport" content="width=device-width, initial-scale=1">
		    <link rel="stylesheet" href="css/print_proposal_style.css">
		    <style>
		    .marker
		    {
		    	background-color: yellow;
		    }
		    .gray
		    {
		    	
		    }
		    </style>
		    </head>
		    <body>
		    <div class="container">';

		// best fit options based on logo size
		$logo_width=300;

		// special case for sparkle
		if($setup['site']=='Sparkle Signs')
		{
			$logo_width=600;
		}
		if($setup['site']=='Scott Paragon')
		{
			$logo_width=200;
		}

		// tax rate for canada
		if($setup['country']=="Canada")
		{
			$proposal['tax_rate']=1;
		}

		# header
		$header_1 = '<div><div style = "float:left;width:405px;"><img width = "'.$logo_width.'" src = "'.$logo.'"></div><div style = "float:left;width:300px;"><h1 style = "text-align:right;margin-bottom:0px;">PROPOSAL</h1>';

		$header_1.='<div style = "text-align:right;margin-bottom:0px;">'.$proposal['proposal_id'].'</div>';
		$header_1.='<table width = "100%">';
		$header_1.='<tr>';
		$header_1.='<td valign = "top" width = "60%" class = "alignright">';
		$header_1.='Date:<br>';
		$header_1.='Expires:<br>';
		$header_1.='Drawing Numbers:<br>';
		$header_1.='</td>';
		$header_1.='<td valign = "top" width = "40%" class = "alignright">';
		$header_1.=$proposal['proposal_date'].'<br>';
		$header_1.=$proposal['expiration_date'].'<br>';
		$header_1.=$proposal['drawing_numbers'].'<br>';
		$header_1.='</td>';
		$header_1.='</tr>';
		$header_1.='</table>';

		$header_1.='</div></div>';

		$header_1.='<table width = "100%" style = "margin-top:15px;">';
		$header_1.='<tr>';

		# left col
		$header_1.='<td valign = "top" width = "50%">';
		$header_1.='<table width = "100%">';
		$header_1.='<tr>';
		$header_1.='<td valign = "top" class = "col1">';
		$header_1.='<strong>Project:</strong>';
		$header_1.='</td>';
		$header_1.='<td valign = "top" class = "col2">';
		$header_1.=$project['site'].'<br>';
		$header_1.=$project['address_1'].'<br>';
		$header_1.=$project['city'].', '.$project['state'].' '.$project['zip'].'<br>';
		$header_1.='</td>';
		$header_1.='</tr>';
		$header_1.='</table><br>';
		$header_1.='</td>';

		# right col
		$header_1.='<td valign = "top" width = "50%">';
		$header_1.='<table width = "100%">';
		$header_1.='<tr>';
		$header_1.='<td valign = "top" width = "" class = "col1">';
		$header_1.='<strong>Client:</strong>';
		$header_1.='</td>';
		$header_1.='<td valign = "top" class = "col2">';
		$header_1.=$customer['name'].'<br>';
		$header_1.=$customer['address_1'].'<br>';
		if(!empty($customer['address_2']))
		{
		    $header_1.=$customer['address_2'].'<br>';
		}
		$header_1.=$customer['city'].', '.$customer['state'].' '.$customer['zip'];
		$header_1.='</td>';
		$header_1.='</tr>';
		$header_1.='</table>';
		$header_1.='</td>';
		$header_1.='</tr>';
		$header_1.='</table>';

		$header_1.='<table>';
		$header_1.='<tr>';
		$header_1.='<td>';
		$header_1.='<table width = "100%">';
		$header_1.='<tr>';
		$header_1.='<td valign = "top" class = "col1">';
		$header_1.='<strong>Contact:</strong>';
		$header_1.='</td>';
		$header_1.='<td valign = "top" class = "col2" style = "margin-right:20px;">';
		$header_1.=$proposal['send_to_name'];
		$header_1.='</td>';
		$header_1.='<td valign = "top" class = "col2" style = "padding-left:20px;">';
		$header_1.=$proposal['send_to_phone'];
		$header_1.='</td>';
		$header_1.='</tr>';
		$header_1.='</table>';
		$header_1.='</td>';
		$header_1.='</tr>';
		$header_1.='</table>';

		if($setup['proposal_message']=='')
		{
		    $html.='<p style = "margin-top:15px;">We are pleased to offer this proposal for the following services at the above location.</p>';
		}
		else
		{
		    $html.='<p style = "margin-top:15px;">'.$setup['proposal_message'].'</p>';
		}

		# items and cost
		$html.='<div style = "width:100%; padding:0px; margin:0px;height:10px;border-bottom:1px solid black;">';
		$html.='<div style = "width:84%; padding:0px; margin:0px; float:left; font-weight:bold; height:5px;">Project Description:';
		$html.='</div>';
		$html.='<div style = "width:15%; padding:0px; margin:0px; float:right;  font-weight:bold; height:5px; text-align:right;">Item Total:';
		$html.='</div>';
		$html.='</div>';

		// tax rate for work location
		$pcity = $project['city'];
		$pstate = $project['state'];
		if((!empty($pcity)) && (!empty($pstate)) )
		{
			$tax_data = $vujade->get_tax_for_city($pcity,$pstate);
		}

		if($items['error']=="0")
		{
			$html.='<div style = "width:100%;border-bottom:1px solid black; padding-bottom:0px;padding-top: 0px;margin-top:5px;">';       
		    $subtotal = 0;
		    $tax_total=0;
			$tax_line=0;
		    unset($items['error']);
		    foreach($items as $item)
		    {

		    	if(!empty($item['tax_label_rate']))
		     	{
		     		$tax_line=$item['amount']*$item['tax_label_rate']*$proposal['tax_rate'];
		     	}

		     	$tax_total+=$tax_line;
		     	$tax_line=0;

		        $subtotal+=$item['amount'];

		        // 
		        // $item['item'] = str_replace("&nbsp;", '&nbsp; ', $item['item']);

		        $item['item'] = str_replace("<p>", '', $item['item']);
        		$item['item'] = str_replace("</p>", '<br>', $item['item']);

		        // table row
		    	$html .= '<div class = "" style="width: 100%;">';

		    	// item 
		    	$html .= '<div class = "" valign="top" style="width: 85%;margin: 0px; padding: 0px;float: left;">';
		    	$html.=$item['item'];
		    	$html.='</div>';

		    	// price
		    	$html.='<div class = "" style = "width:14%;text-align:right;margin: 0px;padding: 0px;float: right;">';
		    	if(!empty($item['amount']))
		        {
		            $html.='$'.@number_format($item['amount'],2,'.',',');
		        }
		    	$html.='</div>';
				$html .= "</div>";
		    }
		    $html.='</div>';
		}

		$html.='<table>';
		$html.='<tr class = "bb">';
		$html.='<td valign = "top" width = "70%" class = "">';
		$html.='';
		$html.='</td>';
		$html.='<td valign = "top" width = "20%" class = "alignright">';
		$html.='';
		$html.='</td>';
		$html.='</tr>';
		$html.='</table>';

		if($proposal['tax_total']>0)
		{
			$tax_total=$proposal['tax_total'];
		}

		$total = $subtotal + $tax_total;

		$html.='<table width = "100%">';
		$html.='<tr class = "">';
		$html.='<td valign = "top" width = "70%" style = "padding-left:100px;">';

		// deposit
		if(empty($proposal['deposit_rate']))
		{
		    $deposit_rate=$setup['advanced_deposit'];
		    $deposit_amount = $total*$deposit_rate;
		}
		else
		{
		    $deposit_rate=$proposal['deposit_rate'];
		    $deposit_amount = $total*$deposit_rate;
		}
		$deposit_rate=$deposit_rate*100;
		$html.='<center><strong>'.$deposit_rate.'% Deposit: <br>$'.@number_format($deposit_amount,2,'.',',').'</strong></center>';
		$html.='</td>';
		$html.='<td valign = "top" width = "15%" class = "alignright">';
		$html.='<strong>Subtotal: </strong><br>';
		$html.='<strong>Tax: </strong><br>';
		$html.='</td>';
		$html.='<td valign = "top" width = "15%" class = "alignright">';
		$html.='<strong>$'.@number_format($subtotal,2,'.',',').'</strong><br>';
		$html.='<strong>$'.@number_format($tax_total,2,'.',',').'</strong><br>';
		$html.='</td>';
		$html.='</tr>';

		$html.='<tr class = "">';
		$html.='<td valign = "top" width = "70%" class = "alignright">';
		$html.='&nbsp;';
		$html.='</td>';
		$html.='<td valign = "top" width = "15%" class = "alignright">';
		$html.='<strong>Total: </strong>';
		$html.='</td>';
		$html.='<td valign = "top" width = "15%" class = "alignright">';
		$html.='<strong>$'.@number_format($total,2,'.',',').'</strong>';
		$html.='</td>';
		$html.='</tr>';

		$html.='</table>';

		$pnt = 7;
		$terms = $proposal['terms'];
		$html.='<div>'.$terms.'</div>';

		// special case for this customer
		if($setup['site']=="Sparkle Signs")
		{
			$html.='<table><tr>';
			$html.='<td><img src = "images/bbb_logo.jpg" width = "50"></td>';
			$html.='<td><img src = "images/tsa_logo.jpg" width = "200"></td>';
			$html.='<td><img src = "images/isa_logo.jpg" width = "200"></td>';
			$html.='</tr></table>';
		}

		# footer
		$footer_1='<p><strong>Salesperson: '.$project['salesperson'].'</strong></p><br>';

		$footer_1.='<table width = "100%">';
		$footer_1.='<tr class = "">';

		$footer_1.='<td valign = "top" class = "t1">';
		$footer_1.="Buyer's Acceptance";
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "bb2">';
		$footer_1.='';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "t2">';
		$footer_1.='Title';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "bb2">';
		$footer_1.='';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "t2">';
		$footer_1.='Date';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "bb3">';
		$footer_1.='';
		$footer_1.='</td>';

		$footer_1.='</tr>';

		$footer_1.='<tr class = "">';
		$footer_1.='<td colspan = "6">&nbsp;';
		$footer_1.='</td>';
		$footer_1.='</tr>';

		$footer_1.='<tr class = "">';

		$footer_1.='<td valign = "top" class = "t1">';
		$footer_1.="Seller's Acceptance";
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "bb2">';
		$footer_1.='';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "t2">';
		$footer_1.='Title';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "bb2">';
		$footer_1.='';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "t2">';
		$footer_1.='Date';
		$footer_1.='</td>';

		$footer_1.='<td valign = "top" class = "bb3">';
		$footer_1.='';
		$footer_1.='</td>';

		$footer_1.='</tr>';
		$footer_1.='</table>';

		$footer_1.= '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';
		$internal_footer = '<div style="width: 100%;">';
		$internal_footer .='<p style="width: 60%;float:left;"><strong>Salesperson: '.$project['salesperson'].'</strong></p>';
		$internal_footer .= '<p style="width: 35%;float:right; margin-top: -4px;"><span>Buyer</span>____________' ;
		$internal_footer .= '<span >Seller</span>____________</p>';
		$internal_footer .= '</div>';
		$internal_footer .= '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';
		$html.='</div></body></html>';

		# mpdf class (pdf output)
		include("mpdf60/mpdf.php");
		$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 80, 45, 10);
		$mpdf->DefHTMLHeaderByName('header_1',$header_1);
		$mpdf->SetHTMLHeaderByName('header_1');
		$mpdf->DefHTMLFooterByName('footer_1', $internal_footer);
		$mpdf->SetHTMLFooterByName('footer_1');
		$mpdf->WriteHTML($html);
		$mpdf->SetHTMLFooter($footer_1);
		$mpdf->Output('emailed_files/'.$proposal['proposal_id'].'.pdf','F'); 

		// generate the design pdf
		$design_id = $_REQUEST['design'];
		if(!empty($design_id))
		{
			$design = $vujade->get_design($design_id);
			if($design['error']!="0")
			{
				//$vujade->page_redirect('error.php?m=3');
			}
			$attach_design = 1;
		}

		# build and send an email
		$send_to = $_REQUEST['send_to'].',';
		$send_to= preg_replace('/\s+/','',$send_to);
		$send_to = explode(',',$send_to);

		$subject = $_REQUEST['subject'];
		$msg = str_replace("\n", "<br>", $_REQUEST['message']);
		$sig = str_replace("\n", "<br>", $employee['email_signature']);
		$msg.= "<br><br><br><br><br>".$sig; 
		$msg.= "<br><br><br><br><br><br><br><br><br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
		$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "http://www.vsignsoftware.com/images/email_logo.png"></a></p>';
		require 'phpmailer/PHPMailerAutoload.php';
		$mail = new PHPMailer();
		$mail->isSMTP();
		$mail->SMTPDebug = 0;
		$mail->Debugoutput = 'html';
		$mail->Host = $vujade->smtp_host;
		$mail->Port = 25;
		$mail->SMTPAuth = false;
		$mail->setFrom($employee['email_reply_to'], $employee['fullname']);
		$mail->addReplyTo($employee['email_reply_to'], $employee['fullname']);
		foreach($send_to as $email)
		{
			if(!empty($email))
			{
				$mail->addAddress($email, 'Customer');
			}
		}

		// send bcc if employee has enabled this
		if( ($employee['bcc']==1) && (!empty($employee['email_reply_to'])) )
		{
			$mail->addBCC($employee['email_reply_to'], $employee['fullname']);
		}

		$mail->Subject = $subject;
		$mail->msgHTML($msg);
		$mail->AltBody = 'This is a plain-text message body';
		$mail->addAttachment('emailed_files/'.$proposal['proposal_id'].'.pdf');
		if($attach_design==1)
		{
			// $mail->addAttachment('emailed_files/'.$design['design_id'].'-DESIGN.pdf');

			// if design files exist, attach all design files for this design
			$file_dir = "uploads/design_files/".$design['project_id'];
			$design_files = $vujade->get_design_files($design['design_id']);
			if($design_files['error']=="0")
			{
				unset($design_files['error']);
				foreach($design_files as $df)
				{
					$mail->addAttachment($file_dir."/".$df['file_name']);
				}
			}
		}
		if (!$mail->send()) 
		{
		    echo "Mailer Error: " . $mail->ErrorInfo;
		} 
		else 
		{
		    echo "Message sent!";
		}
		// delete the file
		unlink('emailed_files/'.$proposal['proposal_id'].'.pdf');
		if($attach_design==1)
		{
			//unlink('emailed_files/'.$design['design_id'].'-DESIGN.pdf');
		}
	}
}

// purchase orders
if($type == "purchase_order")
{
	$po = $vujade->get_purchase_order($id);
	$poid = $po['database_id'];
	$project_id = $po['project_id'];
	$project = $vujade->get_project($project_id,2);
	$shop_order=$vujade->get_shop_order($project_id,'project_id');
	$vendor = $vujade->get_vendor($po['vendor_id'],'ListID');
	$vendor_contact = $vujade->get_vendor_contact($po['vendor_contact_id']);

	if($po['type']=="Outsource")
	{
	    $terms = $vujade->get_terms(5);
	}
	if($po['type']=="Subcontract")
	{
	    $terms = $vujade->get_terms(6);
	}
	$logo['url']="images/invoice_logo.jpg";
	$total = 0;
	$setup = $vujade->get_invoice_company_setup(3);

	# html
	$charset="ISO-8859-1";
	$html = '<!DOCTYPE html>
	    <head>
	    <meta charset="'.$charset.'">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <title>Purchase Order</title>
	    <meta name="description" content="">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <link rel="stylesheet" href="css/print_po_style.css">
	    </head>
	    <body>';

	# line items 
	$html.='<div class = "row" style = "padding-top:20px;">';

	if($po['type']=="Materials")
	{
	    $html.='<div class = "column_75_border_all">Item #:</div>';        
	    $html.='<div class = "column_300_border_all">Description:</div>';
	    $html.='<div class = "column_75_border_all_centered">Account:</div>';
	    $html.='<div class = "column_75_border_all_centered">Job No.:</div>';
	    $html.='<div class = "column_75_border_all_centered">Qty:</div>';
	    $html.='<div class = "column_75_border_all_centered">Unit Price:</div>';
	    $html.='<div class = "column_75_border_all_centered">Line Total:</div>';
	    $materials = $vujade->get_materials_for_purchase_order($po['database_id']);
	    unset($materials['error']);
	    foreach($materials as $item)
	    {
	        if($item['project_id']==$project_id)
	        {
		        $fitem = $vujade->get_item($item['inventory_id'],'ListID');
				if($fitem['error']=="0")
				{
					$item['inventory_id']=$fitem['inventory_id'];
				}
	            $html.='<div class = "column_75_border_all">'.$item['inventory_id'].'</div>';
	            $html.='<div class = "column_300_border_all">'.nl2br($item['description']).'</div>';
	            $html.='<div class = "column_75_border_all_centered">'.$item['account'].'</div>';
	            $html.='<div class = "column_75_border_all_centered">'.$project['project_id'].'</div>';
	            $html.='<div class = "column_75_border_all_centered">'.$item['qty'].'</div>';
	            $html.='<div class = "column_75_border_all_centered">'.@number_format($item['unit_price'],2,'.',',').'</div>';
	            $line_total = $item['unit_price']*$item['qty'];
	            $total+=$line_total;
	            $html.='<div class = "column_75_border_all_centered">'.@number_format($line_total,2,'.',',').'</div>';
	            $html.='<div class = "row_border_bottom"></div>';
	        }
	    }
	}
	else
	{
	    //$html.='<div class = "column_450_border_all">Description:</div>';
	    //$html.='<div class = "column_100_border_all_centered">Date Due:</div>';
	    //$html.='<div class = "column_100_border_all_centered">Amount:</div>';
	    //$html.='<div class = "row_border_bottom"></div>';
	    $outsourced = $vujade->get_materials_for_purchase_order($poid);
	    unset($outsourced['error']);
	    //$x=1;
	    $html.='<table width = "100%">';
	    $html.='<tr>';
	    $html.='<td valign = "top" width = "70%" style = "border:1px solid black;">Description:';
	    $html.='</td>';
	    $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">Date Due:';
	    $html.='</td>';
	    $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">Amount:';
	    $html.='</td>';
	    $html.='</tr>';
	    foreach($outsourced as $o)
	    {
	        if(empty($o['date_required']))
	        {
	            $o['date_required']='&nbsp;';
	        }
	        if(empty($o['amount']))
	        {
	            $o['amount']='0';
	        }

	        //$xd = $x."_date";
	        //$xamount = $x."_amount";

	        $html.='<tr>';
	        $html.='<td valign = "top" width = "70%" style = "border:1px solid black;">'.$o['description'];
	        $html.='</td>';
	        $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">'.$o['date_required'];
	        $html.='</td>';
	        $html.='<td valign = "top" width = "15%" style = "border:1px solid black;">$'.@number_format($o['amount'],2,'.',',');
	        $html.='</td>';
	        $html.='</tr>';
	        $total+=$o['amount'];
	        //$x++;
	    }
	    $html.='</table>';
	}

	$html.='<div class = "total_row">';
	$total+=$po['tax_amount'];
	$html.='Tax: $'.@number_format($po['tax_amount'],2,'.',',');
	$html.='<br>Total: $'.@number_format($total,2,'.',',');
	$html.='</div>';
	$html.='</div>';

	# spacer 
	$html.='<div class = "spacer_full_width"></div>';

	# terms
	if($po['type']!="Materials")
	{
	    $html.='<div class = "terms_header">Terms:</div>';
	    $html.= str_replace('<p>', '', $terms['terms']);
	}

	# end html
	$html.='</body></html>';

	# repeating header
	$header_1 = '<div class = "header" style = "border-bottom:4px solid black;border-top:0px;border-right:0px;border-left:0px;margin-bottom:10px;">';
	$header_1 .= '<div class = "header_padded_inner">';

	# logo
	$header_1 .= '<div class = "header_left" style = "margin-right:30px;margin-bottom:15px;">';
	$header_1 .= '<img src = "'.$logo['url'].'" width = "250" height = "67"><br>';
	$header_1 .= '</div>';

	# h1 and type
	$header_1 .= '<div style = "margin-left:15px;float:left;">';

	$header_1 .= '<div style = "width:100%;">';

	$header_1 .= '<div style = "float:left;width:320px;">';
	$header_1 .= '<h1>Purchase Order';
	if(!empty($po['date_revised']))
	{
	    $header_1.='<br><font style = "color:red;font-weight:bold;font-size:12px;">REVISED</font>';
	}
	else
	{
	    $header_1.='<br><font style = "color:white;font-weight:bold;font-size:12px;">REVISED</font>';
	}
	$header_1 .= '</h1>';
	$header_1 .= '</div>';

	$header_1 .= '<div style = "float:right;width:100px;">';
	$header_1 .= '<h1>'.$po['purchase_order_id'].'</h1>';
	$header_1 .= '</div>';
	$header_1 .= '<div style = "width:100px;text-align:right;margin-top:0px;">';

	$t1 = strtotime($po['date']);
	$f1 = date('m/d/Y',$t1);
	$t2 = strtotime($po['date_revised']);
	$f2 = date('m/d/Y',$t2);
	if($f1=='12/31/1969')
	{
		$f1=date('m/d/Y');
	}
	if($f2=='12/31/1969')
	{
		$f2=date('m/d/Y');
	}

	if(!empty($po['date_revised']))
	{
	    $header_1 .= $f2;
	}
	else
	{
	    $header_1 .= $f1;
	}
	$header_1 .= '</div>';
	$header_1 .= '</div>';
	$header_1 .= '</div>';

	$header_1 .= '<div style = "width:100%;">';
	$header_1 .= '<div style = "width:269px;float:left;">';
	$compinfo = $setup['address_1'];
	if(!empty($setup['address_2']))
	{
	    $compinfo .= ", ".$setup['address_2'];
	}
	$compinfo .= '<br>'.$setup['city'].', '.$setup['state'].' '.$setup['zip'];
	$header_1 .=$compinfo;
	$header_1 .= '</div>';

	$header_1 .= '<div style = "width:210px;float:left;">';
	$header_1 .=$setup['phone'];
	$header_1 .= '</div>';
	$header_1 .= '</div>';

	$header_1 .= '<div class = "row">';
	$header_1 .= '<div class = "header_left">';
	$header_1 .= '<div style = "float:left;width:125px;">Project #:</div><div style = "float:left;">'.$project['project_id'];
	$header_1 .= '</div>';
	$header_1 .= '<div style = "float:left;width:125px;">Project Name:</div><div style = "float:left;">'.$project['site'];
	$header_1 .= '</div>';
	$header_1 .= '<div style = "float:left;width:125px;">Design #:</div><div style = "float:left;">'.$shop_order['design'];
	$header_1 .= '</div>';
	$header_1 .= '</div>';

	$header_1 .= '<div class = "header_middle" style = "width:210px;">';
	$header_1 .= '<strong>To:</strong><br><div style = "margin-left:30px;">';
	$header_1 .= $vendor['name'].'<br>';
	$header_1 .= $vendor['address_1'];
	if(!empty($vendor['address_2']))
	{
	    $header_1 .= ', '.$vendor['address_2'];
	}
	$header_1 .= '<br>'.$vendor['city'].', '.$vendor['state'].' '.$vendor['zip'].'';
	$header_1 .= '<br>';
	$header_1 .= 'Contact: '.$vendor_contact['fullname'];
	$header_1 .= '<br>';

	// limit size of phone number 
	$phone=trim($vendor_contact['phone1']);
	$phone = $vujade->limit_chars($phone,12);
	$header_1 .= 'Phone: '.$phone;
	$header_1 .= '</div></div>';

	$header_1 .= '<div style = "width:210px;margin-left:10px;float:left;">';
	$header_1 .= '<strong>Ship To:</strong><br><div style = "margin-left:30px;">';
	$header_1 .= $po['company'].'<br>';
	$header_1 .= $po['address_1'];
	if(!empty($po['address_2']))
	{
	    $header_1 .= ', '.$po['address_2'];
	}
	$header_1 .= '<br>'.$po['city'].', '.$po['state'].' '.$po['zip'].'';
	$header_1 .= '</div></div>';
	$header_1 .= '</div>';
	$header_1 .= '</div>'; // end of header padded inner
	$header_1 .= '</div>'; // end of header div

	# repeating footer
	$footer_1 = '<div class = "footer">';

	$footer_1.= '<div class = "footer_col" style = "border:none;">'.$po['ordered_by'].'</div></div>';

	$footer_1.= '<div class = "footer_row">';
	$footer_1.= '<div class = "footer_col"> Ordered By:</div><div class = "spacer_30px"></div>';
	$footer_1.= '<div class = "footer_col"> Authorized Signature:</div><div class = "spacer_30px"></div>';
	$footer_1.= '<div class = "footer_col"> Acceptance Signature:</div>';
	$footer_1.='</div>';
	$footer_1.='Page {PAGENO}';
	$footer_1.='</div>';

	# mpdf class (pdf output)
	include("mpdf60/mpdf.php");
	$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 85, 30, 10);
	$mpdf->DefHTMLHeaderByName('header_1',$header_1);
	$mpdf->SetHTMLHeaderByName('header_1');
	$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
	$mpdf->SetHTMLFooterByName('footer_1');
	$mpdf->WriteHTML($html);
	$mpdf->Output('emailed_files/'.$po['purchase_order_id'].'.pdf','F'); 

	# build and send an email
	$send_to = $_REQUEST['send_to'].',';
	$send_to= preg_replace('/\s+/','',$send_to);
	$send_to = explode(',',$send_to);

	$subject = $_REQUEST['subject'];
	$msg = str_replace("\n", "<br>", $_REQUEST['message']);
	$sig = str_replace("\n", "<br>", $employee['email_signature']);
	$msg.= "<br><br><br><br><br>".$sig; 
	$msg.= "<br><br><br><br><br><br><br><br><br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
	$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "http://www.vsignsoftware.com/images/email_logo.png">></a></p>';
	require 'phpmailer/PHPMailerAutoload.php';
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = $vujade->smtp_host;
	$mail->Port = 25;
	$mail->SMTPAuth = false;
	$mail->setFrom($employee['email_reply_to'], $employee['fullname']);
	$mail->addReplyTo($employee['email_reply_to'], $employee['fullname']);
	foreach($send_to as $email)
	{
		if(!empty($email))
		{
			$mail->addAddress($email, 'Customer');
		}
	}

	// send bcc if employee has enabled this
	if( ($employee['bcc']==1) && (!empty($employee['email_reply_to'])) )
	{
		$mail->addBCC($employee['email_reply_to'], $employee['fullname']);
	}

	$mail->Subject = $subject;
	$mail->msgHTML($msg);
	$mail->AltBody = 'This is a plain-text message body';
	$mail->addAttachment('emailed_files/'.$po['purchase_order_id'].'.pdf');
	if (!$mail->send()) 
	{
	    echo "Mailer Error: " . $mail->ErrorInfo;
	} 
	else 
	{
	    echo "Message sent!";
	}
	// delete the file
	unlink('emailed_files/'.$po['purchase_order_id'].'.pdf');
}

// designs
if($type=="design")
{

	$design_id = $_REQUEST['id'];
	$design = $vujade->get_design($design_id);
	if($design['error']!="0")
	{
		print 'Invalid Design. '.$design_id;
		die;
	}

	$project = $vujade->get_project($design['project_id'],2);
	if($project['error']!="0")
	{
		print 'Invalid project id. '.$design['project_id'];
		die;
		//$vujade->page_redirect('error.php?m=3');
	}
	
	$type1 = preg_match("/Presentation/", $design['type']) ? 1 : 0;
	$type2 = preg_match("/Const. Dwgs./", $design['type']) ? 1 : 0;
	$type3 = preg_match("/Production/", $design['type']) ? 1 : 0;
	$type4 = preg_match("/Permits Required/", $design['type']) ? 1 : 0;
	$f1 = preg_match("/11x17/", $design['type_sub']) ? 1 : 0;
	$f2 = preg_match("/8.5x11/", $design['type_sub']) ? 1 : 0;
	$f3 = preg_match("/photocomp/", $design['type_sub']) ? 1 : 0;
	$f4 = preg_match("/thumbnail/", $design['type_sub']) ? 1 : 0;
	$f5 = preg_match("/8.5x14/", $design['type_sub']) ? 1 : 0;

	$charset="ISO-8859-1";
	$html = '
	            <!DOCTYPE html>
	            <head>
	            <meta charset="'.$charset.'">
	            <meta http-equiv="X-UA-Compatible" content="IE=edge">
	            <title>Design Request</title>
	            <meta name="description" content="">
	            <meta name="viewport" content="width=device-width, initial-scale=1">
	            <link rel="stylesheet" href="css/print_design_style.css">
	            </head>
	            <body>
	            ';

	$html.='<h3>Request:</h3>';
	$html.='<p>'.$design['request_description'].'</p>';
	$html.='</body></html>';

	$header_1 ='
	            <div class = "header"><h1>Design Request</h1></div>
	            <div class = "header2">
	            <div class = "checkbox">';

    # checkboxes
    $header_1 .= '<input type = "checkbox"';
    if($type1==1)
    {
        $header_1 .= ' checked = "checked"';
    }
    $header_1 .= '>Presentation
    </div>

    <div class = "checkbox">
    <input type = "checkbox"';
    if($type2==1)
    {
        $header_1 .= ' checked = "checked"';
    }
    $header_1 .= '>Construction
    </div>

    <div class = "checkbox">
    <input type = "checkbox"';
    if($type3==1)
    {
        $header_1 .= ' checked = "checked"';
    }
    $header_1 .= '>Production
    </div>

    <div class = "checkbox" style = "width:20%;">
    <input type = "checkbox"';
    if($type4==1)
    {
        $header_1 .= ' checked = "checked"';
    }
    $header_1 .= '>Permits Required
    </div>

    <div class = "checkbox">
    <input type = "checkbox"';
    if($f3==1)
    {
        $header_1 .= ' checked = "checked"';
    }
    $header_1 .= '>Photocomp
    </div>

    <div class = "checkbox_last">
    <input type = "checkbox"';
    if($f4==1)
    {
        $header_1 .= ' checked = "checked"';
    }
    $header_1 .= '>Thumbnail
    </div>
    </div>
    ';

	# design number, project name, address, other details
	$header_1.='<div class = "row3">';
	$header_1.='<table width = "100%">';
	$header_1.='<tr>';
	$header_1.='<td class = "first" valign = "top">';
	$header_1.=$design['design_id'];
	$header_1.='<br>';
	$title = (strlen($project['site']) > 40) ? substr($project['site'],0,37).'...' : $project['site'];
	$header_1.=$title;
	$header_1.='</td>';

	$header_1.='<td class = "second" valign = "top">';
	$header_1.='<table width = "100%">
	<tr>
	<td valign = "top" class = "width170">
	Site:<br>
	<table><tr><td class = "mr">';
	$header_1.=$project['address_1'];
	if(!empty($project['address_2']))
	{
	    $header_1.=', '.$project['address_2'];  
	}
	$header_1.='<br>';
	$header_1.=$project['city'].', '.$project['state'].' '.$project['zip'];
	$header_1.='</td></tr></table>
	</td>

	<td valign = "top" class = "third">
	Project Contact:<br>
	<table><tr><td class = "mr">';
	$header_1.=$project['project_contact'].'&nbsp;';
	$header_1.='</td></tr></table>
	Salesperson:<br>
	<table><tr><td class = "mr">';
	$header_1.=$project['salesperson'];
	$header_1.='</td></tr></table>
	</td>

	<td valign = "top" class = "width215">
	<table width = "100%">
	<tr>
	<td width = "60%" valign = "top">
	Priority:
	</td>
	<td width = "40%" valign = "top" class = "textright">
	'.$design['priority'].'
	</td>
	</tr>

	<tr>
	<td width = "60%" valign = "top">
	Date Submitted:
	</td>
	<td width = "40%" valign = "top" class = "textright">
	'.$design['request_date'].'
	</td>
	</tr>

	<tr>
	<td width = "60%" valign = "top">
	Date Required:
	</td>
	<td width = "40%" valign = "top" class = "textright">
	'.$design['request_required_date'].'
	</td>
	</tr>
	</table>
	</td>

	</tr>
	</table>';
	$header_1.='</td>';
	$header_1.='</tr>';
	$header_1.='</table>';

	$header_1.='</div>';

	# footer
	$footer_1.='<div class = "footer_top">';

	# notes references and completion date
	$footer_1.='<table width = "100%" class = "bt">';
	$footer_1.='<tr>';
	$footer_1.='<td class = "footer_left" valign = "top">Notes:';
	$footer_1.='</td>';
	$footer_1.='<td class = "footer_right" valign = "top">';
	$footer_1.='<table width = "50%">';
	$footer_1.='<tr>';
	$footer_1.='<td valign = "top" class = "alignright">';
	$footer_1.='Completion Date:<br>';
	$footer_1.='Designer:<br>';
	$footer_1.='Time:';
	$footer_1.='</td>';
	$footer_1.='</tr>';
	$footer_1.='</table>';
	$footer_1.='</td>';
	$footer_1.='</tr>';
	$footer_1.='</table></div>';
	$footer_1 .= '<div class = "pager">Page {PAGENO}</div>';

	/* customer does not want design sent 
	# mpdf class (pdf output)
	include("MPDF57/mpdf.php");
	$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', 10, 10, 80, 40, 10);
	//$mpdf->setAutoTopMargin = 'stretch';
	//$mpdf->setAutoBottomMargin = 'stretch';
	$mpdf->DefHTMLHeaderByName('header_1',$header_1);
	$mpdf->SetHTMLHeaderByName('header_1');
	$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
	$mpdf->SetHTMLFooterByName('footer_1');
	$mpdf->WriteHTML($html);
	$mpdf->Output('emailed_files/'.$design['design_id'].'.pdf','F'); 
	*/

	# build and send an email
	$send_to = $_REQUEST['send_to'].',';
	$send_to= preg_replace('/\s+/','',$send_to);
	$send_to = explode(',',$send_to);

	$subject = $_REQUEST['subject'];
	$msg = str_replace("\n", "<br>", $_REQUEST['message']);
	$sig = str_replace("\n", "<br>", $employee['email_signature']);
	$msg.= "<br><br><br><br><br>".$sig; 
	$msg.= "<br><br><br><br><br><br><br><br><br><br><p style = 'margin-left:300px;'>This email was generated by:<br>";
	$msg.= '<a href = "http://www.vsignsoftware.com"><img src = "http://www.vsignsoftware.com/images/email_logo.png"></a></p>';
	require 'phpmailer/PHPMailerAutoload.php';
	$mail = new PHPMailer();
	$mail->isSMTP();
	$mail->SMTPDebug = 0;
	$mail->Debugoutput = 'html';
	$mail->Host = $vujade->smtp_host;
	$mail->Port = 25;
	$mail->SMTPAuth = false;
	$mail->setFrom($employee['email_reply_to'], $employee['fullname']);
	$mail->addReplyTo($employee['email_reply_to'], $employee['fullname']);
	foreach($send_to as $email)
	{
		if(!empty($email))
		{
			$mail->addAddress($email, 'Customer');
		}
	}

	// send bcc if employee has enabled this
	if( ($employee['bcc']==1) && (!empty($employee['email_reply_to'])) )
	{
		$mail->addBCC($employee['email_reply_to'], $employee['fullname']);
	}
	
	$mail->Subject = $subject;
	$mail->msgHTML($msg);
	$mail->AltBody = 'This is a plain-text message body';
	
	//$mail->addAttachment('emailed_files/'.$design['design_id'].'.pdf');

	// if design files exist, attach all design files for this design
	$file_dir = "uploads/design_files/".$design['project_id'];
	$design_files = $vujade->get_design_files($design['design_id']);
	if($design_files['error']=="0")
	{
		unset($design_files['error']);
		foreach($design_files as $df)
		{
			$mail->addAttachment($file_dir."/".$df['file_name']);
		}
	}

	if (!$mail->send()) 
	{
	    echo "Mailer Error: " . $mail->ErrorInfo;
	} 
	else 
	{
	    echo "Message sent!";
	}
	// delete the file
	// unlink('emailed_files/'.$design['design_id'].'.pdf');
}
?>
