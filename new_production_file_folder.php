<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$files_permissions = $vujade->get_permission($_SESSION['user_id'],'Production Files');
if($files_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['project_id'];
$project_id = $id;

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$s = array();

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# new file folder
if($action==1)
{
	$name = $_POST['new_folder'];
	if(!empty($name))
	{
		$vujade->create_row('file_folders');
		$row_id = $vujade->row_id;
		$s[]=$vujade->update_row('file_folders',$row_id,'name',$name);
		$s[]=$vujade->update_row('file_folders',$row_id,'project_id',$project_id);
		$vujade->page_redirect('new_production_file.php?project_id='.$project_id.'&folder_id='.$row_id);
	}
	else
	{
		$vujade->errors[]="Folder name cannot be empty.";
	}
}

# existing file folder
if($action==2)
{
	$folder_id = $_POST['existing_folder'];
	if( (!empty($folder_id)) && ($folder_id!=0) )
	{
		$vujade->page_redirect('new_production_file.php?project_id='.$project_id.'&folder_id='.$folder_id);
	}
	else
	{
		$vujade->errors[]="No folder selected.";
	}
}

# skip
if($action==3)
{
	$vujade->page_redirect('new_production_file.php?folder_id=0&project_id='.$project_id);
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$nav = 3;
$title = "Production Files - ";
?>

<?php require_once('tray_header.php'); ?>

<!-- Start: Content-Wrapper -->

<section id="content_wrapper">

	<header id="topbar">
		<div class="topbar-left">
			<ol class="breadcrumb">
				<li class="crumb-active">
					Project #: <?php print $project_id; ?> Project Name: <?php print $project['site']; ?>
				</li>
			</ol>
		</div>
	</header>
<!-- Begin: Content -->

<section id="content" class="table-layout animated fadeIn">

<!-- begin: .tray-center -->

<div class="tray tray-center" style = "width:100%;"><div class="theme-primary" style = "width:100%;">

<?php // require_once('project_right_tray.php'); ?>

<div id="" class="panel heading-border panel-primary">

	<div id="projectDetailsContainer" class="panel-body bg-light">


		<div style = "" class="">

		<div style = "" class="">
		
		<p><em>Please choose a new folder to be created or an existing folder to used. This step is optional. <br>Press the "Skip" button to skip this step. 
		</em></p>		

		<?php
		$vujade->show_errors();
		?>

		<div class="">

			<div class="row" style="margin-bottom: 20px">

				<form method = "post" action = "new_production_file_folder.php">

						<div class="col-xs-6">
							<div class="row" style="margin-bottom: 20px">
								<div class="col-lg-4" style="">
									<span style="line-height: 39px">New Folder</span>
								</div>

								<div class="col-xs-8">
									<input type = "hidden" name = "action" value = "1">
									<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
									<input class="form-control" type = "text" name = "new_folder" style = "">
								</div>
							</div>
						</div>

						<div class="col-xs-6">
							<input type = "submit" value= "Save and Continue" class = "sbt200 btn-primary btn">
						</div>

				</form>

			</div>

			<div class="row" style="margin-bottom: 20px">

				<form method = "post" action = "new_production_file_folder.php">

					<div class = "col-xs-6">
						<div class="row">
							<div class="col-xs-4"  style="">
								<span style="display: block; margin-bottom:5px; vertical-align: middle; line-height: 39px">Existing Folder
							</div>

							<div class="col-xs-8">
									<input type = "hidden" name = "action" value = "2">
									<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
										<select class="form-control" name = "existing_folder" style = "height: 40px">
												<option value = "">-Select-</option>
												<?php
												# get existing folders
												$folders = $vujade->get_file_folders($project_id);
												if($folders['error']=="0")
												{
												unset($folders['error']);
												foreach($folders as $f)
												{
												print '<option value = "'.$f['database_id'].'">'.$f['name'].'</option>';
												}
												}
												?>
										</select>
							</div>
						</div>
					</div>

						<div class = "col-xs-6">
							<input type = "submit" value= "Save and Continue" class = "sbt200 btn-primary btn">
						</div>

				</form>

			</div>

			<div class="" style="margin-top:15px;">
				<div class="col-xs-12">
					<form method = "post" action = "new_production_file_folder.php">
						<input type = "hidden" name = "action" value = "3">
						<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
						<input type = "submit" style="width:200px;" value= "Skip this step" class = "sbt200 btn btn-danger btn-lg">
					</form>
				</div>
			</div>
		</div>
	</div>		
		
</div>

<div class = "push">

</div>

</section>

</section>
<!-- BEGIN: PAGE SCRIPTS --><!-- jQuery -->

<script src="vendor/jquery/jquery-1.11.1.min.js">

</script>

<script src="vendor/jquery/jquery_ui/jquery-ui.min.js">

</script><!-- Page Plugins -->

<script src="vendor/plugins/magnific/jquery.magnific-popup.js">

</script>
<!-- Theme Javascript -->

<script src="assets/js/utility/utility.js"></script>

<script src="assets/js/demo/demo.js"></script>

<script src="assets/js/main.js"></script>

</body>

</html>
<?php //require_once('f.php'); ?>