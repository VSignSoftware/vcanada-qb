<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$action=0;
if(isset($_REQUEST['action']))
{
	$action=$_REQUEST['action'];
}
if($action==1)
{
	$id = $_REQUEST['id'];
	$test_group = $vujade->get_groups(false,$id);
	//$vujade->debug_array($test_group);
	if($test_group['count']==1)
	{
		$s=$vujade->delete_row('group_tasks',$id);
		$vujade->messages[]='Group Deleted';
		//print $s;
	}
}

$groups = $vujade->get_groups();

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Group Tasks - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=11;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<div class="panel panel-primary panel-border top">
                	<div class="panel-body bg-light">

                	<?php
					$vujade->show_messages();
					//$vujade->show_errors();
					?>

	            	<p style = "width:100%;height:30px;"><a href = "group.php" class = "pull-right btn btn-success">New Group</a></p>

					<?php
					if($groups['count']>0)
					{

						$table = '<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%" style = "">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>Name</strong></td>
									<td valign = "top"><strong>Employees</strong></td>
									<td valign = "top">&nbsp;</td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">';

						unset($groups['count']);
						unset($groups['error']);
						foreach($groups as $g)
						{
							$link = 'group.php?id='.$g['id'];
							$table.='<tr>';
							
							// name 
							$table.='<td class = "click" href ="'.$link.'">';
							$table.=$g['name'];
							$table.='</td>';

							// employees
							$table.='<td class = "click" href ="'.$link.'">';
							$names = '';
							// employees in this group
							if(!empty($g['employee_ids']))
							{
								$eids = explode(',',$g['employee_ids']);
								foreach($eids as $eid)
								{
									if(!empty($eid))
									{
										$edata = $vujade->get_employee($eid,2);
										if($edata['error']=="0")
										{
											$names.=$edata['fullname'];
											$names.=',';
										}
									}
								}
							}
							$names=rtrim($names,',');
							$names = str_replace(',',', ',$names);
							$table.=$names;
							$table.='</td>';

							// delete
							$table.='<td>';
							$table.='<a href = "#" data-mfp-src="delete-group-modal" class = "btn btn-danger delete" data-id="'.$g['id'].'">Delete</a>';

							$table.='</td>';

							$table.='</tr>';
						}
						$table.='</tbody></table>';
						print $table;
					}
					?>
					</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <style>
	/* The Modal (background) */
	.modal {
	    display: none; /* Hidden by default */
	    position: fixed; /* Stay in place */
	    z-index: 1; /* Sit on top */
	    left: 0;
	    top: 0;
	    width: 100%; /* Full width */
	    height: 100%; /* Full height */
	    overflow: auto; /* Enable scroll if needed */
	    background-color: rgb(0,0,0); /* Fallback color */
	    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
	}

	/* Modal Content/Box */
	.modal-content {
	    background-color: #fefefe;
	    margin: 15% auto; /* 15% from the top and centered */
	    padding: 20px;
	    border: 1px solid #888;
	    width: 80%; /* Could be more or less, depending on screen size */
	    z-index: 1;
	}
</style>

    <!-- delete modal content -->
	<div id="delete-group-modal" class="modal" style = "display:none;">
		<div class = "modal-content">
		<h1>Delete Group</h1>
		<p>Are you sure you want to delete this group?</p>
		<p><a id = "delete-yes" class="btn btn-lg btn-success" href="#">YES</a> <a class="btn btn-lg btn-danger cancel-modal" href="#">NO</a></p>
		</div>
	</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Datatables -->
  <script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

  <!-- Datatables Bootstrap Modifications  -->
  <script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    });

    // Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    // click on group
    $("tbody").on("click", ".click", function()
    {
    	window.document.location = $(this).attr("href");	
    }); 

    // delete button (show modal)
    $('.delete').click(function(e)
    {
    	e.preventDefault();
    	var id = $(this).data("id");
    	$('#delete-yes').attr('href','groups.php?action=1&id='+id);
    	$('#delete-group-modal').show();
    });

    // hide modal
    $('.cancel-modal').click(function(e)
	{
		$('#delete-group-modal').hide();
	});

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
