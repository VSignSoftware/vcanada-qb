<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// they are logged in as admin and not using site as other user
// site admin does not want to see the normal user dashboard
if( ($_SESSION['is_admin']==1) && ($_SESSION['admin_id']==$_SESSION['user_id']) )
{
	// this page allows the admin to switch to viewing the site as an user
	header('location:admin.php');
	die;
}

// employee data
$emp = $vujade->get_employee($_SESSION['user_id']);

// permissions
$designs_permissions = $vujade->get_permission($_SESSION['user_id'],'Designs');
$ds_permissions = $vujade->get_permission($_SESSION['user_id'],'Design Sort');
$es_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Sort');
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
// page setup
$show=0;
if(isset($_REQUEST['show']))
{
    $show=$_REQUEST['show'];
    if(!in_array($show,array(1,2,3)))
    {
        $show=0;
    }
}

$setup = $vujade->get_setup();

// user clicked dismiss message
if($_REQUEST['action']==1)
{
	// delete any existing rows
	$uid = $_SESSION['user_id'];
	$vujade->generic_query("delete from customer_message where user_id = '$uid' limit 1");

	// create new and update dismiss status
	$vujade->create_row('customer_message');
	$s=array();
	$s[]=$vujade->update_row('customer_message',$vujade->row_id,'dismissed',1);
	$s[]=$vujade->update_row('customer_message',$vujade->row_id,'dismissed_ts',strtotime('now'));
	$s[]=$vujade->update_row('customer_message',$vujade->row_id,'user_id',$_SESSION['user_id']);
}

// user entered a project id to be redirected to the designs overview page for a job
if($_REQUEST['action']==2)
{
	$pid = $_REQUEST['pid'];
	$vujade->page_redirect('project_designs.php?id='.$pid);
}

// user entered a project id to be redirected to the photo upload page for a job
if($_REQUEST['action']==3)
{
	$pid = $_REQUEST['pid'];
	$vujade->page_redirect('new_site_photo.php?project_id='.$pid);
}

// customer message
$r1=file_get_contents('https://master.vujade.net/admin/cm.php?r=1');
$r2=file_get_contents('https://master.vujade.net/admin/cm.php?r=2');
$show_customer_message=0;

$cmdata=$vujade->get_customer_message_data($_SESSION['user_id']);
// to show the message, 
// a) the logged in person must not have dismissed the message (ie previously viewed it and clicked the dismiss button)
// b) the time stamp of the dismiss action must be greater than or equal to the start publish time stamp of the message
if($cmdata['count']==0)
{
	// show the message
	$show_customer_message=1;
}
else
{
	// user has a record; determine if conditions a and b are met
	if( ($cmdata['dismissed']==1) && ($cmdata['dismissed_ts']>=$r1) )
	{
		$show_customer_message=0;
	}
	else
	{
		$show_customer_message=1;
	}
}

$section=1;
$title = "Dashboard - ";
require_once('h.php');
?>
    <!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href="dashboard.php" id = "bread-crumb">Dashboard</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->
      <!-- Begin: Content -->
      <section id="content" class="">
        <!-- Sliders -->
        <div class="mw1000 center-block demo-block mt30">
            <style>
                .clickableRow:hover
                {
                    background-color: #83ACE2;
                }
                .v-centered
                {
                    text-align: center;
                }
                .widget-task:hover
                {
                	cursor:pointer;
                }
                #active-jobs:hover
                {
                	cursor:pointer;
                }
            </style>

            <?php
            // show customer message 
            if($show_customer_message==1)
            {
	            print '<div class="panel panel-tile br-a br-light">
	            	<div class="panel-body bg-light" style = "color: #3BAFDA;">
				  	<strong>'.$r2.'</strong> <br><a href = "dashboard.php?action=1" class="btn btn-danger btn-xs">Dismiss this message</a>
				</div></div>';
			}
			?>

            <!-- heading block of widgets -->
            <div class = "row">
            	<div class = "col-md-4">
            		<div class="widget-task panel bg-info of-h mb10" id = "widget-tasks">
						<div class="pn pl20 p5">
							<div class="icon-bg">
								<i class="glyphicons glyphicons-inbox"></i>
							</div>
							<h2 class="mt15 lh15">
							<b><span id = "head-widget-tasks"></span></b>
							</h2>
							<h5 class="text-muted">Tasks</h5>
						</div>
					</div>
            	</div>
            	<div class = "col-md-4">
            		<div class="widget-task panel bg-warning of-h mb10" id = "widget-designs">
						<div class="pn pl20 p5">
							<div class="icon-bg">
								<i class="imoon imoon-pencil"></i>
							</div>
							<h2 class="mt15 lh15">
							<b><span id = "head-widget-designs"></span></b>
							</h2>
							<h5 class="text-muted">Design Requests</h5>
						</div>
					</div>
            	</div>
            	<div class = "col-md-4">
            		<div class="widget-task panel bg-danger of-h mb10" id = "widget-estimates">
						<div class="pn pl20 p5">
							<div class="icon-bg">
								<i class="glyphicons glyphicons-coins"></i>
							</div>
							<h2 class="mt15 lh15">
							<b><span id = "head-widget-estimates"></span></b>
							</h2>
							<h5 class="text-muted">Estimate Requests</h5>
						</div>
					</div>
            	</div>
            </div>

            <?php
            // show graphs
            if($show==0)
            {
            	//print $emp['department'].'<br>';
            	// sales, officers, all departments
	            if( ($emp['department'] == "Sales Dept. 400") || ($emp['department'] == "Officers Dept. 100") || ($emp['department'] == "All Departments") )
	            {
	            	$sp=$emp['fullname'];
	            	$year = date('Y');
	            	$my_open_jobs=0;
	            	$my_active_jobs=0;
	            	$my_processed_jobs=0;
	            	$my_closed_jobs=0;

	            	if($setup['fiscal_year_start']!=1)
					{

						// fix for fiscal year that stretches over two years
						$current_year = date('Y');
						$current_month=date('n');
						/*
						bootz example:
						m = 9
						m2 = 8
						dashboard will only show between 9-2015 and 8-2016;
						problematic because if month is 10-2016, db won't show correct fiscal year
						*/
						$m=$setup['fiscal_year_start'];
						$m2=$setup['fiscal_year_end'];

						// fiscal month end is before the current month
						if($m2<$current_month)
						{
							$y=$current_year;
							$y2 = $current_year+1;
						}
						else
						{
							$y=$current_year-1;
							$y2 = $current_year;
						}

						//$y=$year-1;
						//$y2 = $year;
						//$m=$setup['fiscal_year_start'];
						//$m2=$setup['fiscal_year_end'];
						//print $y.'-'.$m.'-01<br>';
						$start = strtotime($y.'-'.$m.'-01');
						$date1 = $y2.'-'.$m2; 
						$d = @date_create_from_format('Y-m',$date1); 
						$last_day = @date_format($d, 't');
						$end = strtotime($y2.'-'.$m2.'-'.$last_day);
						//print $y2.'-'.$m2.'-'.$last_day.'<br>';

					}
					else
					{
						$start = strtotime($year.'-01-01');
						$end = strtotime($year.'-12-31');
					}

					//print $start.' - '.$end;
					//die;
					//print '<hr>';

					//print date('m/d/Y',$start);
					//print '<br>';
					//print date('m/d/Y',$end);
					//print '<hr>';

					$donutdata = $vujade->get_dashboard_projects($start,$end,$emp['fullname']);
					//print $donutdata['sql'].'<hr>';
					//die;
					$pjobs = $vujade->get_processed_projects($start,$end,$emp['fullname']);
					//print $pjobs['sql'].'<hr>';
					//die;
					$all_jobs=$donutdata['count'];
	            	$pending_jobs=$donutdata['is_pending'];
	            	$processed_jobs=$pjobs['count'];
	            	$did_not_sell_jobs=$donutdata['did_not_sell'];

	            	if($all_jobs>0)
	            	{
	            		$closed_rate=$processed_jobs/$all_jobs;
	            		$closed_rate = round($closed_rate,2);
	            		$closed_rate=$closed_rate*100;
	            	}
	            	else
	            	{
	            		$closed_rate="0.0";
	            	}

	            	// a/r report data for bar chart
	            	// grand totals
					$gt1=0;
					$gt2=0;
					$gt3=0;
					$gt4=0;

					// counts for non-ad invoice days overdue + totals
					$step1 = 0;
					$step2 = 0;
					$step3 = 0;
					$step4 = 0;
					$step1t = 0;
					$step2t = 0;
					$step3t = 0;
					$step4t = 0;
					$total=0;

					//die;

	            	$now = strtotime('now');
					$par1 = $vujade->get_ar_report_data();

					//print_r($donutdata);

					if($par1['error']=="0")
					{
						unset($par1['error']);	
						unset($par1['count']);
						unset($par1['sql']);	
						foreach($par1 as $i)
						{
							// advanced deposit
							if($i['ad']==1)
							{
								$overdue=0;
								$overdue = $now-$i['invoice_date_ts'];
								$overdue = $overdue/86400;
								$overdue = round($overdue);

						        // current (invoice is less than 30 days old)
								if($overdue<=30)
								{
									$gt1+=$i['balance_due'];
								}

						        // 31 to 45 Days
						        if( ($overdue>=31) && ($overdue<=45) )
								{
									$gt2+=$i['balance_due'];
									//print $i['project_id'].' '.$i['balance_due'].'<br>';
								}

						        // 46 to 60 Days
						        if( ($overdue>=46) && ($overdue<=60) )
								{
									$gt3+=$i['balance_due'];
								}

						        // over 60 days
						        if($overdue>60)
								{
									$gt4+=$i['balance_due'];
									//print $i['balance_due'].'<br>';
								}
						        //$total+=$i['balance_due'];
							}
							// not advanced deposit, but included in costing
							if( ($i['ad']==0) && ($i['costing']==1) )
							{
								$overdue=0;
								$overdue = $now-$i['invoice_date_ts'];
								$overdue = $overdue/86400;
								$overdue = round($overdue);

						        // current (invoice is less than 30 days old)
								if($overdue<=30)
								{
									$gt1+=$i['balance_due'];
								}

						        // 31 to 45 Days
						        if( ($overdue>=31) && ($overdue<=45) )
								{
									$gt2+=$i['balance_due'];
									//print $i['project_id'].' '.$i['balance_due'].'<br>';
								}

						        // 46 to 60 Days
						        if( ($overdue>=46) && ($overdue<=60) )
								{
									$gt3+=$i['balance_due'];
								}

						        // over 60 days
						        if($overdue>60)
								{
									$gt4+=$i['balance_due'];
									//print $i['balance_due'].'<br>';
								}
						        //$total+=$i['balance_due'];
							}

							// not advanced deposit and not included in costing
							// do not show
						}
					}
	            	?>
	            	<div class = "row">
	            		<div class = "col-md-6">
							<div id = "donut" name = "donut" style = "height:300px;">
							</div>
	            		</div>
	            		<div class = "col-md-6">
	            			<div id = "bar_chart" name = "bar_chart" style = "height:300px;">
							</div>
	            		</div>
	            	</div>

	            	<div class = "row">
	            		<div class = "col-md-12">
	            			<?php
	            			// chart individual sales report for current fiscal year or year

	            			// fiscal year does not start on Jan. 1
							if($setup['fiscal_year_start']!=1)
							{	
								require_once('db_widgets_a.php');
							}
							else
							{
								require_once('db_widgets_b.php');
							}
	            			?>
	            		</div>
	            	</div>

	            	<?php
	            }

	            // clerical, project managers
	            if( ($emp['department'] == "Clerical Dept. 200") || ($emp['department'] == "Project Cord. Dept. 500") )
	            {
	            	?>
	            	<div class = "row">
	            		<div class = "col-md-6">
	            			<div class="panel panel-tile text-center br-a br-light" id = "active-jobs">
							  <div class="panel-body bg-light">
							    <?php
							    require_once('db_widgets_c.php');
							    ?>
							    <h1 class="fs35 mbn"><?php print $active_jobs['count']; ?></h1>
							    <h6 class="text-system">Active Jobs</h6>
							  </div>
							</div>
	            		</div>
	            		<div class = "col-md-6">
	            			<div class="panel panel-tile text-center br-a br-light">
							  <div class="panel-body bg-light">
							    
							    <h1 class="fs35 mbn"><?php print $jobs_on_budget; ?>%</h1>
							    <h6 class="text-system">Jobs Closed on Budget</h6>
							  </div>
							</div>
	            		</div>
	            	</div>
	            	<?php
	            }

	            // sign manufacturing only
	            if($emp['department'] == "Sign Mfg. Dept. 600") 
	            {
	            	?>
	            	<div class = "row">
	            		<div class = "col-md-4">
	            		</div>

	            		<div class = "col-md-4">
	            			<div class="widget-task panel bg-success of-h mb10" id="">
								<div class="pn pl20 p5">
									<h2 class="mt15 lh15">
									Locate Drawings
									</h2>
									<table>
										<tr>
											<td>
												<input type = "text" name = "locate_drawings" id = "locate_drawings" class = "form-control" style = "width:200px;"> 
											</td>
											<td>
												<a class = "btn btn-primary btn-lg" style = "margin-left:5px;" href = "#" id = "locate_drawings_button">Go</a>
											</td>
										</tr>
									</table>
								</div>
							</div>
	            		</div>

	            		<div class = "col-md-4">
	            		</div>

	            	</div>
	            		
	            	<div class = "row">
	            		<div class = "col-md-4">
	            		</div>

	            		<div class = "col-md-4">
	            			<div class="widget-task panel bg-success of-h mb10" id="">
								<div class="pn pl20 p5">
									<h2 class="mt15 lh15">
									Upload Photos
									</h2>
									<table>
										<tr>
											<td>
												<input type = "text" name = "upload_photos" id = "upload_photos" class = "form-control" style = "width:200px;"> 
											</td>
											<td>
												<a class = "btn btn-primary btn-lg" style = "margin-left:5px;" href = "#" id = "upload_photos_button">Go</a>
											</td>
										</tr>
									</table>
								</div>
							</div>
	            		</div>

	            		<div class = "col-md-4">
	            		</div>

	            	</div>
	            	<?php
	            }

	            // designers only
	            if($emp['department']=='Design Dept. 300')
	            {
	            	?>
	            		<!-- design requests -->
			            <div id = "db-designs-1" class="admin-form theme-primary">
			            	<div class="panel heading-border panel-primary">
			                	<div class="panel-body bg-light">
									<div class = "row">
							            <div class="col-md-12">
							            	<h4 class = "">In Progress</h4>
						                    <?php
						                    $cts = date('m/d/Y');
						                    $cts = strtotime($cts);
						                    $design_count=0;
						                    if($designs_permissions['read']==1)
						                    {
						                        $in_progress = $vujade->get_designs($sort=2,$start=0,$status='In Progress');
						                        if($in_progress['error']==0)
						                        {
						                            unset($in_progress['error']);
						                            print '<table width = "870">';
						                            print '<tbody>';
						                            foreach($in_progress as $d)
						                            {
						                                if(!empty($d['design_id']))
						                                {
						                                    $project_id = explode('-',$d['design_id']);
						                                    $pid = $project_id[0];
						                                    $link = 'project_designs.php?designid='.$d['database_id'].'&id='.$pid;
						                                    print '<tr class="clickableRow" href="'.$link.'">';
						                                
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    print $d['design_id'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "40%">';
						                                    $project = $vujade->get_project($project_id[0],2);
						                                    print $project['site'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    print $project['city'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "15%">';

						                                    $design_types = @explode(" ",$d['type']);
						                                    foreach($design_types as $design_type)
						                                    {
						                                    	if(!empty($design_type))
						                                    	{
						                                    		print $design_type.'<br>';
						                                    	}
						                                    }
						                                    print '</td>';

						                                    // request date
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    if(empty($d['request_required_date']))
						                                    {
						                                    	print 'ASAP';
						                                    }
						                                    else
						                                    {
						                                    	$rrd = strtotime($d['request_required_date']);
							                                    if( ($d['request_required_date']!='ASAP') && ($rrd<$cts) )
							                                    {
							                                    	print '<font color = "red">';
							                                    	print $d['request_required_date'];
							                                    	print '</font>';
							                                    }
							                                    else
							                                    {
							                                    	print $d['request_required_date'];
							                                    }
						                                    }
						                                    print '</td>';

						                                    print '</tr>';
						                                    $design_count++;
						                                }
						                            }
						                            print '</tbody>';
						                            print '</table>';
						                        }

						                        // submitted designs
						                        print '<h4 class = "">Next Job</h4>';
						                        $designs = $vujade->get_designs($sort=2,$start=0,$status='Submitted');
						                        if($designs['error']==0)
						                        {
						                            unset($designs['error']);
						                            if($ds_permissions['read']==1)
						                            {
						                                print '<a href = "edit_design_sort_order.php" class = "btn btn-xs btn-primary" style = "margin-bottom:15px;">Edit Sort Order</a><br>';
						                            }
						                            print '<table width = "870">';
						                            print '<tbody>';
						                            foreach($designs as $d)
						                            {
						                                if(!empty($d['design_id']))
						                                {
						                                    $project_id = explode('-',$d['design_id']);
						                                    $pid = $project_id[0];
						                                    $link = 'project_designs.php?designid='.$d['database_id'].'&id='.$pid;
						                                    print '<tr class="clickableRow" href="'.$link.'">';
						                                
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    print $d['design_id'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "40%">';
						                                    $project = $vujade->get_project($project_id[0],2);
						                                    print $project['site'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    print $project['city'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    $design_types = @explode(" ",$d['type']);
						                                    foreach($design_types as $design_type)
						                                    {
						                                    	if(!empty($design_type))
						                                    	{
						                                    		print $design_type.'<br>';
						                                    	}
						                                    }
						                                    print '</td>';
						                                    
						                                    // request date
						                                    print '<td class = "" valign = "top" width = "15%">';

						                                    if(empty($d['request_required_date']))
						                                    {
						                                    	print 'ASAP';
						                                    }
						                                    else
						                                    {
						                                    	$rrd = strtotime($d['request_required_date']);
							                                    if( ($d['request_required_date']!='ASAP') && ($rrd<$cts) )
							                                    {
							                                    	print '<font color = "red">';
							                                    	print $d['request_required_date'];
							                                    	print '</font>';
							                                    }
							                                    else
							                                    {
							                                    	print $d['request_required_date'];
							                                    }
						                                    }
						                                    print '</td>';

						                                    print '</tr>';
						                                    $design_count++;
						                                }
						                            }
						                            print '</tbody>';
						                            print '</table>';
						                        }
						                        else
						                        {
						                            print $designs['error'];
						                        }

						                        unset($designs);

						                        // on hold designs
						                        print '<h4 class = "">On Hold</h4>';
						                        $designs = $vujade->get_designs($sort=2,$start=0,$status='On Hold');
						                        if($designs['error']==0)
						                        {
						                            unset($designs['error']);
						                            print '<table width = "870">';
						                            print '<tbody>';
						                            foreach($designs as $d)
						                            {
						                                if(!empty($d['design_id']))
						                                {
						                                    $project_id = explode('-',$d['design_id']);
						                                    $pid = $project_id[0];
						                                    $link = 'project_designs.php?designid='.$d['database_id'].'&id='.$pid;
						                                    print '<tr class="clickableRow" href="'.$link.'">';
						                                
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    print $d['design_id'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "40%">';
						                                    $project = $vujade->get_project($project_id[0],2);
						                                    print $project['site'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    print $project['city'];
						                                    print '</td>';
						                                    print '<td class = "" valign = "top" width = "15%">';
						                                    $design_types = @explode(" ",$d['type']);
						                                    foreach($design_types as $design_type)
						                                    {
						                                    	if(!empty($design_type))
						                                    	{
						                                    		print $design_type.'<br>';
						                                    	}
						                                    }
						                                    print '</td>';
						                                    
						                                    // request date
						                                    print '<td class = "" valign = "top" width = "15%">';

						                                    if(empty($d['request_required_date']))
						                                    {
						                                    	print 'ASAP';
						                                    }
						                                    else
						                                    {
						                                    	$rrd = strtotime($d['request_required_date']);
							                                    if( ($d['request_required_date']!='ASAP') && ($rrd<$cts) )
							                                    {
							                                    	print '<font color = "red">';
							                                    	print $d['request_required_date'];
							                                    	print '</font>';
							                                    }
							                                    else
							                                    {
							                                    	print $d['request_required_date'];
							                                    }
						                                    }
						                                    print '</td>';

						                                    print '</tr>';
						                                    $design_count++;
						                                }
						                            }
						                            print '</tbody>';
						                            print '</table>';
						                        }
						                        else
						                        {
						                            print $designs['error'];
						                        }
						                    }
						                    ?>
			            				</div>
							        </div>    
							    </div>
							</div>
						</div>	
	            	<?php
	            }
	        }
            ?>

            <!-- tasks -->
            <div id = "db-tasks" <?php if($show!=1){print 'style = "display:none;"';} ?> class="admin-form theme-primary">
            	<div class="panel heading-border panel-primary">
                	<div class="panel-body bg-light">
						<div class = "row">
				            <div class="col-md-12">
			                    <?php
			                    $task_count = 0;
			                    $cd = date('m/d/Y');
			                    $today = strtotime('now');
			                    $user = $vujade->get_user($_SESSION['user_id']);
			                    $tasks = $vujade->get_tasks_for_user($user['employee_id'],2);
			                    //print_r($tasks);
			                    if($tasks['error']=='0')
			                    {
			                        unset($tasks['error']);
			                        print '<table width = "870">';
			                        print '<tbody>';
			                        foreach($tasks as $task)
			                        {
			                            $tdd = strtotime($task['task_due_date']);
			                            $link = 'project_tasks.php?id='.$task['project_id'].'&taskid='.$task['database_id'];
			                            // sales task should only appear if current date = task date or after
			                            if($task['is_sales_task']==1)
			                            {
			                                
			                                if($today>=$tdd)
			                                {
			                                    
			                                    print '<tr class="clickableRow" href="'.$link.'">';
			                                    print '<td width = "" valign = "top" style = "">';
			                                    if($today>$tdd)
			                                    {
			                                        print '<font color = "red">';
			                                    }
			                                    print $task['task_due_date'];
			                                    if($today>$tdd)
			                                    {
			                                        print '</font>';
			                                    }
			                                    print '</td>';
			                                    print '<td width = "" valign = "top" style = "">';
			                                    print $task['project_id'];
			                                    print '</td>';
			                                    print '<td width = "" valign = "top" style = "">';
			                                    $project = $vujade->get_project($task['project_id'],2);
			                                    print $project['site'];
			                                    print '</td>';
			                                    print '<td width = "" valign = "top" style = "">';
			                                    print $task['task_subject'];
			                                    print '</td>';
			                                    print '</tr>';
			                                    $task_count++;
			                                }
			                            }
			                            else
			                            {
			                                // not a sales task and it should appear until it is done
			                                print '<tr class="clickableRow" href="'.$link.'">';
			                                print '<td width = "" valign = "top" style = "">';
			                                if($tdd<$today)
			                                {
			                                    print '<font color = "red">';
			                                }
			                                print $task['task_due_date'];
			                                if($tdd<$today)
			                                {
			                                    print '</font>';
			                                }
			                                print '</td>';
			                                print '<td width = "" valign = "top" style = "">';
			                                print $task['project_id'];
			                                print '</td>';
			                                print '<td width = "" valign = "top" style = "">';
			                                $project = $vujade->get_project($task['project_id'],2);
			                                print $project['site'];
			                                print '</td>';
			                                print '<td width = "" valign = "top" style = "">';
			                                print $task['task_subject'];
			                                print '</td>';
			                                print '</tr>';
			                                $task_count++;
			                            }
			                            
			                        }
			                        print '</tbody>';
			                        print '</table>';

			                        print '<p><center><a target = "_blank" href = "print_tasks.php" class = "btn btn-primary">Print</a>';

			                    }
			                    else
			                    {
			                        //print_r($tasks);  
			                    }
			                    ?>
				            </div>
				        </div>    
				    </div>
				</div>
			</div>	

			<!-- special styling for the design queue -->
			<style>
			.col1
			{
				width:100px;
				float:left;
			}
			.col2
			{
				width:290px;
				float:left;
				font-weight: bold;
			}
			.col3
			{
				width:120px;
				float:left;
			}
			.marginleft
			{
				margin-left:15px;
			}
			.bold
			{
				/* font-weight: bold; */
			}
			</style>

            <!-- design requests -->
            <div id = "db-designs" <?php if($show!=2){print 'style = "display:none;"';} ?> class="admin-form theme-primary">
            	<div class="panel heading-border panel-primary">
                	<div class="panel-body bg-light">
						<div class = "row">
				            <div class="col-md-12">
				            	<h4 class = "">In Progress</h4>
			                    <?php
			                    $cts = date('m/d/Y');
			                    $cts = strtotime($cts);
			                    $design_count=0;
			                    if($designs_permissions['read']==1)
			                    {
			                        $in_progress = $vujade->get_designs($sort=2,$start=0,$status='In Progress');
			                        if($in_progress['error']==0)
			                        {
			                            unset($in_progress['error']);
			                            foreach($in_progress as $d)
			                            {
			                                if(!empty($d['design_id']))
			                                {
			                                    $project_id = explode('-',$d['design_id']);
			                                    $pid = $project_id[0];
			                                    $link = 'project_designs.php?designid='.$d['database_id'].'&id='.$pid;
			                                    $project = $vujade->get_project($project_id[0],2);
			                                    $design_types = @explode(" ",$d['type']);
			                                    $dttext = '';
			                                    foreach($design_types as $design_type)
			                                    {
			                                    	if(!empty($design_type))
			                                    	{
			                                    		$dttext.=$design_type.'<br>';
			                                    	}
			                                    }
			                                    $designer = $vujade->get_employee($d['employee_id'],2);

			                                    print '<div class = "row clickableRow" href="'.$link.'">';
			                                    
			                                    print '<div class = "col1 marginleft">';
			                                    print $d['design_id'];
			                                    print '</div>';

			                                    print '<div class = "col2">';
			                                    print $project['site'];
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    print $project['city'];
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    print $dttext;
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    if(empty($d['request_required_date']))
			                                    {
			                                    	print 'ASAP';
			                                    }
			                                    else
			                                    {
			                                    	$rrd = strtotime($d['request_required_date']);
				                                    if( ($d['request_required_date']!='ASAP') && ($rrd<$cts) )
				                                    {
				                                    	print '<font color = "red">';
				                                    	print $d['request_required_date'];
				                                    	print '</font>';
				                                    }
				                                    else
				                                    {
				                                    	print $d['request_required_date'];
				                                    }
			                                    }
			                                    print '</div>';

			                                    print '<div class = "col3 bold">';
			                                    print $designer['fullname'];
			                                    print '</div>';
			                                    print '</div>';
			                                    $design_count++;
			                                }
			                            }
			                        }

			                        // submitted designs
			                        print '<h4 class = "">Next Job</h4>';
			                        $designs = $vujade->get_designs($sort=2,$start=0,$status='Submitted');
			                        if($designs['error']==0)
			                        {
			                            unset($designs['error']);
			                            if($ds_permissions['read']==1)
			                            {
			                                print '<a href = "edit_design_sort_order.php" class = "btn btn-xs btn-primary" style = "margin-bottom:15px;">Edit Sort Order</a><br>';
			                            }
			                            unset($designs['error']);
			                            foreach($designs as $d)
			                            {
			                                if(!empty($d['design_id']))
			                                {
			                                    $project_id = explode('-',$d['design_id']);
			                                    $pid = $project_id[0];
			                                    $link = 'project_designs.php?designid='.$d['database_id'].'&id='.$pid;
			                                    $project = $vujade->get_project($project_id[0],2);
			                                    $design_types = @explode(" ",$d['type']);
			                                    $dttext = '';
			                                    foreach($design_types as $design_type)
			                                    {
			                                    	if(!empty($design_type))
			                                    	{
			                                    		$dttext.=$design_type.'<br>';
			                                    	}
			                                    }
			                                    $designer = $vujade->get_employee($d['employee_id'],2);

			                                    print '<div class = "row clickableRow" href="'.$link.'">';
			                                    
			                                    print '<div class = "col1 marginleft">';
			                                    print $d['design_id'];
			                                    print '</div>';

			                                    print '<div class = "col2">';
			                                    print $project['site'];
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    print $project['city'];
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    print $dttext;
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    if(empty($d['request_required_date']))
			                                    {
			                                    	print 'ASAP';
			                                    }
			                                    else
			                                    {
			                                    	$rrd = strtotime($d['request_required_date']);
				                                    if( ($d['request_required_date']!='ASAP') && ($rrd<$cts) )
				                                    {
				                                    	print '<font color = "red">';
				                                    	print $d['request_required_date'];
				                                    	print '</font>';
				                                    }
				                                    else
				                                    {
				                                    	print $d['request_required_date'];
				                                    }
			                                    }
			                                    print '</div>';

			                                    print '<div class = "col3  bold">';
			                                    print $designer['fullname'];
			                                    print '</div>';
			                                    print '</div>';
			                                    $design_count++;
			                                }
			                            }
			                        }
			                        else
			                        {
			                            print $designs['error'];
			                        }

			                        unset($designs);

			                        // on hold designs
			                        print '<h4 class = "">On Hold</h4>';
			                        $designs = $vujade->get_designs($sort=2,$start=0,$status='On Hold');
			                        if($designs['error']==0)
			                        {

			                        	unset($designs['error']);
			                            foreach($designs as $d)
			                            {
			                                if(!empty($d['design_id']))
			                                {
			                                    $project_id = explode('-',$d['design_id']);
			                                    $pid = $project_id[0];
			                                    $link = 'project_designs.php?designid='.$d['database_id'].'&id='.$pid;
			                                    $project = $vujade->get_project($project_id[0],2);
			                                    $design_types = @explode(" ",$d['type']);
			                                    $dttext = '';
			                                    foreach($design_types as $design_type)
			                                    {
			                                    	if(!empty($design_type))
			                                    	{
			                                    		$dttext.=$design_type.'<br>';
			                                    	}
			                                    }
			                                    $designer = $vujade->get_employee($d['employee_id'],2);

			                                    print '<div class = "row clickableRow" href="'.$link.'">';
			                                    
			                                    print '<div class = "col1 marginleft">';
			                                    print $d['design_id'];
			                                    print '</div>';

			                                    print '<div class = "col2">';
			                                    print $project['site'];
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    print $project['city'];
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    print $dttext;
			                                    print '</div>';

			                                    print '<div class = "col3">';
			                                    if(empty($d['request_required_date']))
			                                    {
			                                    	print 'ASAP';
			                                    }
			                                    else
			                                    {
			                                    	$rrd = strtotime($d['request_required_date']);
				                                    if( ($d['request_required_date']!='ASAP') && ($rrd<$cts) )
				                                    {
				                                    	print '<font color = "red">';
				                                    	print $d['request_required_date'];
				                                    	print '</font>';
				                                    }
				                                    else
				                                    {
				                                    	print $d['request_required_date'];
				                                    }
			                                    }
			                                    print '</div>';

			                                    print '<div class = "col3 bold">';
			                                    print $designer['fullname'];
			                                    print '</div>';
			                                    print '</div>';
			                                    $design_count++;
			                                }
			                            }

			                            
			                        }
			                        else
			                        {
			                            print $designs['error'];
			                        }
			                    }
			                    ?>
            				</div>
				        </div>    
				    </div>
				</div>
			</div>	

            <!-- estimate requests -->
            <div id = "db-estimates" <?php if($show!=3){print 'style = "display:none;"';} ?> class="admin-form theme-primary">
            	<div class="panel heading-border panel-primary">
                	<div class="panel-body bg-light">
						<div class = "row">
				            <div class="col-md-12">
				            	<h4 class = "">In Progress</h4>
				            	<?php
		                        $estimate_count=0;
		                        if($estimates_permissions['read']==1)
		                        {
		                            $estimates = $vujade->get_estimates($sort=2,$start=0,$status='In Progress');
		                            if($estimates['error']==0)
		                            {
		                                unset($estimates['error']);
		                                print '<table class = "" width = "870">';
		                                print '<tbody>';
		                                foreach($estimates as $e)
		                                {
		                                    if(!empty($e['estimate_id']))
		                                    {
		                                        $project_id = explode('-',$e['estimate_id']);
		                                        $link = 'project_estimates.php?estimateid='.$e['database_id'].'&id='.$project_id[0];
		                                        print '<tr class="clickableRow" href="'.$link.'">';
		                                        print '<td valign = "top" width = "100">';
		                                        print $e['date'];
		                                        print '</td>';
		                                        print '<td valign = "top" width = "100">';
		                                        print $e['estimate_id'];
		                                        print '</td>';
		                                        print '<td valign = "top" width = "400">';
		                                        $project = $vujade->get_project($project_id[0],2);
		                                        print $project['site'];
		                                        print '</td>';

		                                        // salesperson
		                                        print '<td width = "170">';
		                                        print $project['salesperson'];
		                                    	print '</td>';

		                                        // request date
			                                    print '<td class = "" valign = "top" width = "100">';
			                                    $rrd = strtotime($e['required_date']);
			                                    if( ($e['required_date']!='ASAP') && ($rrd<$cts) )
			                                    {
			                                    	print '<font color = "red">';
			                                    	print $e['required_date'];
			                                    	print '</font>';
			                                    }
			                                    else
			                                    {
			                                    	print $e['required_date'];
			                                    }
			                                    print '</td>';

		                                        print '</tr>';
		                                        $estimate_count++;
		                                    }
		                                    // one blank row to fix sizing issues
		                                    /*
		                                    print '<tr>';
		                                    print '<td style = "opacity:0;">02/22/2017';
		                                    print '</td>';

		                                    print '<td style = "opacity:0;">170115-04';
		                                    print '</td>';

		                                    print '<td style = "opacity:0;">Berkshire Hathaway Home Services California Properties ';
		                                    print '</td>';

		                                    // 
		                                    print '<td style = "opacity:0;">';
		                                    print '</td>';

		                                    print '<td style = "opacity:0;">ASAP';
		                                    print '</td>';
		                                    print '</tr>';
		                                    */
		                                }
		                                print '</tbody>';
		                                print '</table>';
		                            }
		                            else
		                            {
		                                //print $estimates['error'];
		                            }
		                            print '<h4 class = "">Next Job</h4>';
		                            $estimates = $vujade->get_estimates($sort=2,$start=0,$status='Submitted');
		                            if($estimates['error']==0)
		                            {
		                                if($es_permissions['read']==1)
		                                {
		                                    print '<a href = "edit_estimate_sort_order.php" class = "btn btn-xs btn-primary" style = "margin-bottom:15px;">Edit Sort Order</a><br>';
		                                }
		                                unset($estimates['error']);
		                                print '<table class = "" width = "870">';
		                                print '<tbody>';
		                                foreach($estimates as $e)
		                                {
		                                    if(!empty($e['estimate_id']))
		                                    {
		                                        $project_id = explode('-',$e['estimate_id']);
		                                        $link = 'project_estimates.php?estimateid='.$e['database_id'].'&id='.$project_id[0];
		                                        print '<tr class="clickableRow" href="'.$link.'">';
		                                        print '<td valign = "top" width = "100">';
		                                        print $e['date'];
		                                        print '</td>';
		                                        print '<td valign = "top" width = "100">';
		                                        print $e['estimate_id'];
		                                        print '</td>';
		                                        print '<td valign = "top" width = "400">';
		                                        $project = $vujade->get_project($project_id[0],2);
		                                        print $project['site'];
		                                        print '</td>';

		                                        // salesperson
		                                        print '<td valign = "top" width = "170">';
		                                        print $project['salesperson'];
		                                        print '</td>';

		                                        // request date
			                                    print '<td class = "" valign = "top" width = "100">';

			                                    $rrd = strtotime($e['required_date']);
			                                    if( ($e['required_date']!='ASAP') && ($rrd<$cts) )
			                                    {
			                                    	print '<font color = "red">';
			                                    	print $e['required_date'];
			                                    	print '</font>';
			                                    }
			                                    else
			                                    {
			                                    	print $e['required_date'];
			                                    }
			                                    print '</td>';
		                                        
		                                        print '</tr>';
		                                        $estimate_count++;
		                                    }
		                                }
		                                print '</tbody>';
		                                print '</table>';
		                            }
		                            else
		                            {
		                                //print $estimates['error'];
		                            }

		                            // on hold
		                            print '<h4 class = "">On Hold</h4>';
		                            $estimates = $vujade->get_estimates($sort=2,$start=0,$status='On Hold');
		                            if($estimates['error']==0)
		                            {
		                                unset($estimates['error']);
		                                print '<table class = "" width = "870">';
		                                print '<tbody>';
		                                foreach($estimates as $e)
		                                {
		                                    if(!empty($e['estimate_id']))
		                                    {
		                                        $project_id = explode('-',$e['estimate_id']);
		                                        $link = 'project_estimates.php?estimateid='.$e['database_id'].'&id='.$project_id[0];
		                                        print '<tr class="clickableRow" href="'.$link.'">';
		                                        print '<td valign = "top" width = "100">';
		                                        print $e['date'];
		                                        print '</td>';
		                                        print '<td valign = "top" width = "100">';
		                                        print $e['estimate_id'];
		                                        print '</td>';
		                                        print '<td valign = "top" width = "400">';
		                                        $project = $vujade->get_project($project_id[0],2);
		                                        print $project['site'];
		                                        print '</td>';

		                                        // salesperson
		                                        print '<td valign = "top" width = "170">';
		                                        print $project['salesperson'];
		                                        print '</td>';

		                                        // request date
			                                    print '<td class = "" valign = "top" width = "100">';

			                                    $rrd = strtotime($e['required_date']);
			                                    if( ($e['required_date']!='ASAP') && ($rrd<$cts) )
			                                    {
			                                    	print '<font color = "red">';
			                                    	print $e['required_date'];
			                                    	print '</font>';
			                                    }
			                                    else
			                                    {
			                                    	print $e['required_date'];
			                                    }
			                                    print '</td>';
		                                        
		                                        print '</tr>';
		                                        $estimate_count++;
		                                    }
		                                }
		                                print '</tbody>';
		                                print '</table>';
		                            }
		                            else
		                            {
		                                //print $estimates['error'];
		                            }
		                        }
		                        ?>
            				</div>
				        </div>    
				    </div>
				</div>
			</div>
        </div>
      </section>
      <!-- End: Content -->
    </section>
  </div>
  <!-- End: Main -->
  <input type = "hidden" id = "task_count" value = "<?php print $task_count; ?>">
  <input type = "hidden" id = "design_count" value = "<?php print $design_count; ?>">
  <input type = "hidden" id = "estimate_count" value = "<?php print $estimate_count; ?>">
  <input type = "hidden" id = "show" value = "<?php print $show; ?>">
  <?php
  $_SESSION['task_count']=$task_count;
  $_SESSION['design_count']=$design_count;
  $_SESSION['estimate_count']=$estimate_count;
  ?>
  <!-- BEGIN: PAGE SCRIPTS -->
  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {
    "use strict";

    // Init Theme Core    
    Core.init();

    var show = $('#show').val();
    if(show!=0)
    {
        //$('#db-slider').hide();
    }
    $(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    });

    $('#tasks').click(function(e)
    {
    	window.document.location = 'dashboard.php?show=1';
    });

    $('#designs').click(function(e)
    {
    	window.document.location = 'dashboard.php?show=2';
    });

    $('#estimates').click(function(e)
    {
    	window.document.location = 'dashboard.php?show=3';
    });

    $('#badge-task-count').text($('#task_count').val());
    $('#head-widget-tasks').text($('#task_count').val());
    $('#widget-tasks').click(function(e)
    {
    	e.preventDefault();
    	window.document.location = "dashboard.php?show=1";
    });
    $('#badge-design-count').text($('#design_count').val());
    $('#head-widget-designs').text($('#design_count').val());
    $('#widget-designs').click(function(e)
    {
    	e.preventDefault();
    	window.document.location = "dashboard.php?show=2";
    });
    $('#badge-estimate-count').text($('#estimate_count').val());
    $('#head-widget-estimates').text($('#estimate_count').val());
    $('#widget-estimates').click(function(e)
    {
    	e.preventDefault();
    	window.document.location = "dashboard.php?show=3";
    });
    $('#donut').click(function(e)
    {
    	e.preventDefault();
    	window.document.location = "search.php?action=2&sp=<?php print $emp['fullname']; ?>";
    });
    $('#bar_chart').click(function(e)
    {
    	e.preventDefault();
    	window.open("print_report_ar.php");
    });
    $('#active-jobs').click(function(e)
    {
    	e.preventDefault();
    	window.document.location = "report.php?type=16";
    });

    // drawings go button clicked
	$('#locate_drawings_button').click(function(e)
    {
    	e.preventDefault();
    	var pid = $('#locate_drawings').val();
    	if(pid!='')
    	{
    		var url = 'dashboard.php?action=2&pid='+pid;
    		window.location.href = url;
    	}
    });

	// user pressed the enter key in the text field for drawings
	$("#locate_drawings").on('keyup', function(e) 
	{
	    if(e.keyCode == 13) 
	    {
		    var pid = $('#locate_drawings').val();
	    	if(pid!='')
	    	{
	    		var url = 'dashboard.php?action=2&pid='+pid;
	    		window.location.href = url;
	    	}
		}
	});

    $('#upload_photos_button').click(function(e)
    {
    	e.preventDefault();
    	var pid = $('#upload_photos').val();
    	if(pid!='')
    	{
    		var url = 'dashboard.php?action=3&pid='+pid;
    		window.location.href = url;
    	}
    });

    // user pressed the enter key in the text field for photos
	$("#upload_photos").on('keyup', function(e) 
	{
	    if(e.keyCode == 13) 
	    {
		    var pid = $('#upload_photos').val();
	    	if(pid!='')
	    	{
	    		var url = 'dashboard.php?action=3&pid='+pid;
	    		window.location.href = url;
	    	}
		}
	});

   
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

  <?php //$vujade->script_stop_time(); ?>

</body>
</html>