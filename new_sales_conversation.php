<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id=$_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# add a conversation
if($action==1)
{
	$date = $_REQUEST['date'];
	$msg = trim($_REQUEST['msg']);
	$s = array();
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$vujade->create_row('sales_conversations');
	$s[]=$vujade->update_row('sales_conversations',$vujade->row_id,'project_id',$id);
	$s[]=$vujade->update_row('sales_conversations',$vujade->row_id,'date',$date);
	$s[]=$vujade->update_row('sales_conversations',$vujade->row_id,'body',$msg);
	$s[]=$vujade->update_row('sales_conversations',$vujade->row_id,'user_id',$_SESSION['user_id']);
	$s[]=$vujade->update_row('sales_conversations',$vujade->row_id,'ts',strtotime($date));
	$s[]=$vujade->update_row('sales_conversations',$vujade->row_id,'name_display',$employee['fullname']);
	$vujade->page_redirect('sales.php?tab=0&id='.$id.'&conversation_id='.$vujade->row_id);
}

$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 13;
$title = "Sales - ";
require_once('h.php');
//require_once('project_toolbar.php');
?>


<!-- Start: Content-Wrapper -->

<section id="content_wrapper">

	<!-- Start: Topbar -->

	<header id="topbar">

		<div class="topbar-left">

			<ol class="breadcrumb">

				<li class="crumb-active">

					<a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>

				</li>

			</ol>

		</div>

	</header>

	<!-- Begin: Content -->
	<section id="content" class="table-layout animated fadeIn">

		<div class="admin-form theme-primary" style="display: block;">

			<div class="panel heading-border panel-primary">

				<div class="panel-body bg-light">

					<div id="" style = "margin-top:15px;">

						<!-- form -->
						<form id = "form" method = "post" action = "new_sales_conversation.php">
							<label for="date" style="margin: 0 0 15px;">Date:</label>
							<input type = "text" name = "date" id = "date" class="form-control" style = "width:300px;" value="">
							<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
							<br>
							<label for="msg" style="margin: 0 0 15px;">Message:</label>
							<textarea name = "msg" id = "msg" class="form-control ckeditor" style = "width: 100%;height:300px;"></textarea>
							<div id = "error_3" style = "display:none;">This field cannot be empty.</div>
							<input type = "hidden" name = "action" value = "1">
							<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
							<div style = "margin-top:15px;">
								<div style = "float:left">
									<a href = "#" id = "cancel" class = "btn btn-lg btn-danger" style = "width:100px;">CANCEL</a> <a id = "save" href = "#" class = "btn btn-lg btn-success" style = "width:100px; border-radius: 3px;">SAVE</a>
								</div>
								<div style = "float:right;">
									
								</div>
							</div>
						</form>

						<!-- ckeditor new version 4.5x -->
						<?php require_once('ckeditor.php'); ?>
					</div>

				</div>
			</div>
		</div>

	</section>
	<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function()
	{

		"use strict";

		// Init Theme Core
		Core.init();

		// date picker
		$('#date').datepicker();

		$('#cancel').click(function(e)
		{
			e.preventDefault();
			window.location="sales.php?tab=1&id=<?php print $id; ?>";
		});

		$('#save').click(function(e)
		{
			e.preventDefault();
			$('#form').submit();
		});
		
	});
</script>
<!-- END: PAGE SCRIPTS -->
</body>

</html>


