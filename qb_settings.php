<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$setup=$vujade->get_setup();

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$s[]=array();

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
if($action==1)
{
	$state=$_POST['state'];
	$s[] = $vujade->update_row('misc_config',1,'qb_tax_state',$state);
	$vujade->messages[]="Changes Saved.";
}

if($action==2)
{
	if($setup['site']=='Tru Lite')
	{

		$a1 = $_POST['a1'];
		$a2 = $_POST['a2'];
		$a3 = $_POST['a3'];
		$a4 = $_POST['a4'];
		$a5 = $_POST['a5'];
		$a6 = $_POST['a6'];
		$a7 = $_POST['a7'];

		$ac1=$vujade->get_qb_account('Inventory Asset','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac1['id'],'list_id',$a1);
		$acdata1=$vujade->get_inventory_account($a1);
		$s[]=$vujade->update_row('qb_accounts',$ac1['id'],'display_text',$acdata1['account_name']);

		$ac2=$vujade->get_qb_account('Office Supplies:Supplies','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac2['id'],'list_id',$a2);
		$acdata2=$vujade->get_inventory_account($a2);
		$s[]=$vujade->update_row('qb_accounts',$ac2['id'],'display_text',$acdata2['account_name']);

		$ac3=$vujade->get_qb_account('Office Supplies:Tools and Small Equipment','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac3['id'],'list_id',$a3);
		$acdata3=$vujade->get_inventory_account($a3);
		$s[]=$vujade->update_row('qb_accounts',$ac3['id'],'display_text',$acdata3['account_name']);

		$ac4=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
		$s[]=$vujade->update_row('qb_accounts',$ac4['id'],'list_id',$a4);
		$acdata4=$vujade->get_inventory_account($a4);
		$s[]=$vujade->update_row('qb_accounts',$ac4['id'],'display_text',$acdata4['account_name']);

		$ac5=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
		$s[]=$vujade->update_row('qb_accounts',$ac5['id'],'list_id',$a5);
		$acdata5=$vujade->get_inventory_account($a5);
		$s[]=$vujade->update_row('qb_accounts',$ac5['id'],'display_text',$acdata5['account_name']);

		$ac6=$vujade->get_qb_account('Cost of Goods Sold','Inventory COGS');
		$s[]=$vujade->update_row('qb_accounts',$ac6['id'],'list_id',$a6);
		$acdata6=$vujade->get_inventory_account($a6);
		$s[]=$vujade->update_row('qb_accounts',$ac6['id'],'display_text',$acdata6['account_name']);

		$ac7=$vujade->get_qb_account('11001 - Job Income','Inventory Income');
		$s[]=$vujade->update_row('qb_accounts',$ac7['id'],'list_id',$a7);
		$acdata7=$vujade->get_inventory_account($a7);
		$s[]=$vujade->update_row('qb_accounts',$ac7['id'],'display_text',$acdata7['account_name']);
	}
	else
	{

		$po_inventory = $_POST['po_inventory'];
		$po_safety = $_POST['po_safety'];
		$po_shop_expense = $_POST['po_shop_expense'];
		$po_shop_supplies = $_POST['po_shop_supplies'];
		$po_small_tools = $_POST['po_small_tools'];
		$po_wip = $_POST['po_wip'];

		$invoices_ar = $_POST['invoices_ar'];
		$invoices_deposit = $_POST['invoices_deposit'];

		$inventory_cogs = $_POST['inventory_cogs'];
		$inventory_sales = $_POST['inventory_sales'];

		$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac1['id'],'list_id',$po_inventory);
		$acdata1=$vujade->get_inventory_account($po_inventory);
		$s[]=$vujade->update_row('qb_accounts',$ac1['id'],'display_text',$acdata1['account_name']);

		$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac2['id'],'list_id',$po_safety);
		$acdata2=$vujade->get_inventory_account($po_safety);
		$s[]=$vujade->update_row('qb_accounts',$ac2['id'],'display_text',$acdata2['account_name']);

		$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac3['id'],'list_id',$po_shop_expense);
		$acdata3=$vujade->get_inventory_account($po_shop_expense);
		$s[]=$vujade->update_row('qb_accounts',$ac3['id'],'display_text',$acdata3['account_name']);

		$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac4['id'],'list_id',$po_shop_supplies);
		$acdata4=$vujade->get_inventory_account($po_shop_supplies);
		$s[]=$vujade->update_row('qb_accounts',$ac4['id'],'display_text',$acdata4['account_name']);

		$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac5['id'],'list_id',$po_small_tools);
		$acdata5=$vujade->get_inventory_account($po_small_tools);
		$s[]=$vujade->update_row('qb_accounts',$ac5['id'],'display_text',$acdata5['account_name']);

		$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
		$s[]=$vujade->update_row('qb_accounts',$ac6['id'],'list_id',$invoices_ar);
		$acdata6=$vujade->get_inventory_account($invoices_ar);
		$s[]=$vujade->update_row('qb_accounts',$ac6['id'],'display_text',$acdata6['account_name']);

		$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
		$s[]=$vujade->update_row('qb_accounts',$ac7['id'],'list_id',$invoices_deposit);
		$acdata7=$vujade->get_inventory_account($invoices_deposit);
		$s[]=$vujade->update_row('qb_accounts',$ac7['id'],'display_text',$acdata7['account_name']);

		$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
		$s[]=$vujade->update_row('qb_accounts',$ac8['id'],'list_id',$inventory_cogs);
		$acdata8=$vujade->get_inventory_account($inventory_cogs);
		$s[]=$vujade->update_row('qb_accounts',$ac8['id'],'display_text',$acdata8['account_name']);

		$ac9=$vujade->get_qb_account('Sales','Inventory Income');
		$s[]=$vujade->update_row('qb_accounts',$ac9['id'],'list_id',$inventory_sales);
		$acdata9=$vujade->get_inventory_account($inventory_sales);
		$s[]=$vujade->update_row('qb_accounts',$ac9['id'],'display_text',$acdata9['account_name']);

		$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
		$s[]=$vujade->update_row('qb_accounts',$ac10['id'],'list_id',$po_wip);
		$acdata10=$vujade->get_inventory_account($po_wip);
		$s[]=$vujade->update_row('qb_accounts',$ac10['id'],'display_text',$acdata10['account_name']);
	}
}

/*
// Adcorp Sign Systems
if($setup['site']=='Adcorp Sign Systems')
{
	$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
	$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
	$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
	$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
	$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
	$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
	$ac9=$vujade->get_qb_account('Sales','Inventory Income');
	$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
}

// Eagle Signs
if($setup['site']=='Eagle Signs')
{
	$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
	$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
	$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
	$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
	$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
	$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
	$ac9=$vujade->get_qb_account('Sales','Inventory Income');
	$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
}

// Signage
if($setup['site']=='Signage Solutions')
{
	$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
	$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
	$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
	$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
	$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
	$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
	$ac9=$vujade->get_qb_account('Sales','Inventory Income');
	$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
}

// Sparkle
if($setup['site']=='Sparkle Signs')
{
	$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
	$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
	$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
	$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
	$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
	$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
	$ac9=$vujade->get_qb_account('Sales','Inventory Income');
	$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
}

// ISA
if($setup['site']=='ISA')
{
	$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
	$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
	$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
	$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
	$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
	$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
	$ac9=$vujade->get_qb_account('Sales','Inventory Income');
	$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
}
*/

// Tru Lite
if($setup['site']=='Tru Lite')
{
	$ac1=$vujade->get_qb_account('Inventory Asset','Purchase Orders');
	$ac2=$vujade->get_qb_account('Office Supplies:Supplies','Purchase Orders');
	$ac3=$vujade->get_qb_account('Office Supplies:Tools and Small Equipment','Purchase Orders');
	$ac4=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac5=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac6=$vujade->get_qb_account('Cost of Goods Sold','Inventory COGS');
	$ac7=$vujade->get_qb_account('11001 - Job Income','Inventory Income');
}
else
{
	$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
	$ac2=$vujade->get_qb_account('Safety','Purchase Orders');
	$ac3=$vujade->get_qb_account('Shop Expense','Purchase Orders');
	$ac4=$vujade->get_qb_account('Shop Supplies','Purchase Orders');
	$ac5=$vujade->get_qb_account('Small Tools','Purchase Orders');
	$ac6=$vujade->get_qb_account('Accounts Receivable','Invoice Payments AR');
	$ac7=$vujade->get_qb_account('Accounts Receivable','Invoice Payments Deposit');
	$ac8=$vujade->get_qb_account('*Cost of Goods Sold','Inventory COGS');
	$ac9=$vujade->get_qb_account('Sales','Inventory Income');
	$ac10=$vujade->get_qb_account('Work in Process:WIP Materials','Purchase Orders');
}


$qb_accounts = $vujade->get_qb_accounts('',true);
//print_r($qb_accounts);
//die;
if($qb_accounts['count']<=0)
{
	$vujade->page_redirect('error.php?m=8');
}
unset($qb_accounts['count']);
unset($qb_accounts['error']);
unset($qb_accounts['sql']);
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=13;
$title = "QuickBooks Settings - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=13;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<h3>QuickBooks Home State:</h3>
                	<form method = "post" action = "qb_settings.php">
                	<input type = "hidden" name = "action" value = "1">
                		<table>
                			<tr>
                				<td>
                					<select name = "state" id = "state">
                						<?php
                						if(!empty($setup['qb_tax_state']))
                						{
                							print '<option value="'.$setup['qb_tax_state'].'">'.$setup['qb_tax_state'].'</option>';
                						}
                						?>
                						<option value="">-Select-</option>
										<option value="AL">Alabama</option>
										<option value="AK">Alaska</option>
										<option value="AZ">Arizona</option>
										<option value="AR">Arkansas</option>
										<option value="CA">California</option>
										<option value="CO">Colorado</option>
										<option value="CT">Connecticut</option>
										<option value="DE">Delaware</option>
										<option value="DC">District Of Columbia</option>
										<option value="FL">Florida</option>
										<option value="GA">Georgia</option>
										<option value="HI">Hawaii</option>
										<option value="ID">Idaho</option>
										<option value="IL">Illinois</option>
										<option value="IN">Indiana</option>
										<option value="IA">Iowa</option>
										<option value="KS">Kansas</option>
										<option value="KY">Kentucky</option>
										<option value="LA">Louisiana</option>
										<option value="ME">Maine</option>
										<option value="MD">Maryland</option>
										<option value="MA">Massachusetts</option>
										<option value="MI">Michigan</option>
										<option value="MN">Minnesota</option>
										<option value="MS">Mississippi</option>
										<option value="MO">Missouri</option>
										<option value="MT">Montana</option>
										<option value="NE">Nebraska</option>
										<option value="NV">Nevada</option>
										<option value="NH">New Hampshire</option>
										<option value="NJ">New Jersey</option>
										<option value="NM">New Mexico</option>
										<option value="NY">New York</option>
										<option value="NC">North Carolina</option>
										<option value="ND">North Dakota</option>
										<option value="OH">Ohio</option>
										<option value="OK">Oklahoma</option>
										<option value="OR">Oregon</option>
										<option value="PA">Pennsylvania</option>
										<option value="RI">Rhode Island</option>
										<option value="SC">South Carolina</option>
										<option value="SD">South Dakota</option>
										<option value="TN">Tennessee</option>
										<option value="TX">Texas</option>
										<option value="UT">Utah</option>
										<option value="VT">Vermont</option>
										<option value="VA">Virginia</option>
										<option value="WA">Washington</option>
										<option value="WV">West Virginia</option>
										<option value="WI">Wisconsin</option>
										<option value="WY">Wyoming</option>
									</select>				
	
                				</td>
                				<td>&nbsp;
                				</td>
                				<td>
                					<input type = "submit" value = "SAVE" class = "btn btn-primary">
                				</td>
                			</tr>
                		</table>
					</form>

				</div>
				</div>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<?php
                	if($setup['site']=="Tru Lite")
                	{
                		?>
	                	<h3>QuickBooks Accounts:</h3>
	                	<form method = "post" action = "qb_settings.php">
	                	<input type = "hidden" name = "action" value = "2">
	                		<table>
	                			<tr>
	                				<td>
	                					<h4>Purchase Orders</h4>
	                				</td>
	                				<td>&nbsp;				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>Inventory Asset</strong>
	                				</td>
	                				<td>
	                					
	                					<select name = "a1" id = "a1">
	                						<?php
	                						if(!empty($ac1['list_id']))
	                						{
	                							print '<option value="'.$ac1['list_id'].'">'.$ac1['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>Supplies</strong>
	                				</td>
	                				<td>
	                					<select name = "a2" id = "a2">
	                						<?php
	                						if(!empty($ac2['list_id']))
	                						{
	                							print '<option value="'.$ac2['list_id'].'">'.$ac2['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>Tools and Small Equipment</strong>
	                				</td>
	                				<td>
	                					<select name = "a3" id = "a3">
	                						<?php
	                						if(!empty($ac3['list_id']))
	                						{
	                							print '<option value="'.$ac3['list_id'].'">'.$ac3['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				</td>
	                				<td>&nbsp;
	                				</td>
	                				<td>
	                					&nbsp;
	                				</td>
	                			</td>

	                			<tr>
	                				<td>
	                					<h4>Invoice Payments</h4>
	                				</td>
	                				<td>&nbsp;				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>Invoice Payments AR</strong>
	                				</td>
	                				<td>
	                					<select name = "a4" id = "a4">
	                						<?php
	                						if(!empty($ac4['list_id']))
	                						{
	                							print '<option value="'.$ac4['list_id'].'">'.$ac4['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>Invoice Payments Deposit</strong>
	                				</td>
	                				<td>
	                					<select name = "a5" id = "a5">
	                						<?php
	                						if(!empty($ac5['list_id']))
	                						{
	                							print '<option value="'.$ac5['list_id'].'">'.$ac5['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				</td>
	                				<td>&nbsp;
	                				</td>
	                				<td>
	                					&nbsp;
	                				</td>
	                			</td>

	                			<tr>
	                				<td>
	                					<h4>Inventory</h4>
	                				</td>
	                				<td>&nbsp;				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>COGS</strong>
	                				</td>
	                				<td>
	                					<select name = "a6" id = "a6">
	                						<?php
	                						if(!empty($ac6['list_id']))
	                						{
	                							print '<option value="'.$ac6['list_id'].'">'.$ac6['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				<td>
	                					<strong>Sales</strong>
	                				</td>
	                				<td>
	                					<select name = "a7" id = "a7">
	                						<?php
	                						if(!empty($ac7['list_id']))
	                						{
	                							print '<option value="'.$ac7['list_id'].'">'.$ac7['display_text'].'</option>';
	                						}
	                						print '<option value = "">-Select-</option>';
	                						foreach($qb_accounts as $acct)
	                						{
	                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
	                						}
	                						?>
										</select>				
	                				</td>
	                			</tr>

	                			<tr>
	                				</td>
	                				<td>&nbsp;
	                				</td>
	                				<td>
	                					<input type = "submit" value = "SAVE" class = "btn btn-primary">
	                				</td>
	                			</td>

	                		</table>
						</form>
					<?php
					}
					else
					{
						?>
						<h3>QuickBooks Accounts:</h3>
		                	<form method = "post" action = "qb_settings.php">
		                	<input type = "hidden" name = "action" value = "2">
		                		<table>
		                			<tr>
		                				<td>
		                					<h4>Purchase Orders</h4>
		                				</td>
		                				<td>&nbsp;				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Inventory</strong>
		                				</td>
		                				<td>
		                					
		                					<select name = "po_inventory" id = "po_inventory">
		                						<?php
		                						$ac1=$vujade->get_qb_account('Inventory','Purchase Orders');
		                						
		                						if(!empty($ac1['list_id']))
		                						{
		                							print '<option value="'.$ac1['list_id'].'">'.$ac1['display_text'].'</option>';
		                						}
		                						
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Safety</strong>
		                				</td>
		                				<td>
		                					<select name = "po_safety" id = "po_safety">
		                						<?php
		                						if(!empty($ac2['list_id']))
		                						{
		                							print '<option value="'.$ac2['list_id'].'">'.$ac2['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Shop Expense</strong>
		                				</td>
		                				<td>
		                					<select name = "po_shop_expense" id = "po_shop_expense">
		                						<?php
		                						if(!empty($ac3['list_id']))
		                						{
		                							print '<option value="'.$ac3['list_id'].'">'.$ac3['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Shop Supplies</strong>
		                				</td>
		                				<td>
		                					<select name = "po_shop_supplies" id = "po_shop_supplies">
		                						<?php
		                						if(!empty($ac4['list_id']))
		                						{
		                							print '<option value="'.$ac4['list_id'].'">'.$ac4['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Small Tools</strong>
		                				</td>
		                				<td>
		                					<select name = "po_small_tools" id = "po_small_tools">
		                						<?php
		                						if(!empty($ac5['list_id']))
		                						{
		                							print '<option value="'.$ac5['list_id'].'">'.$ac5['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>WIP</strong>
		                				</td>
		                				<td>
		                					<select name = "po_wip" id = "po_wip">
		                						<?php
		                						if(!empty($ac10['list_id']))
		                						{
		                							print '<option value="'.$ac10['list_id'].'">'.$ac10['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				</td>
		                				<td>&nbsp;
		                				</td>
		                				<td>
		                					&nbsp;
		                				</td>
		                			</td>

		                			<tr>
		                				<td>
		                					<h4>Invoice Payments</h4>
		                				</td>
		                				<td>&nbsp;				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Accounts Receivable</strong>
		                				</td>
		                				<td>
		                					<select name = "invoices_ar" id = "invoices_ar">
		                						<?php
		                						if(!empty($ac6['list_id']))
		                						{
		                							print '<option value="'.$ac6['list_id'].'">'.$ac6['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Undeposited Funds</strong>
		                				</td>
		                				<td>
		                					<select name = "invoices_deposit" id = "invoices_deposit">
		                						<?php
		                						if(!empty($ac7['list_id']))
		                						{
		                							print '<option value="'.$ac7['list_id'].'">'.$ac7['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				</td>
		                				<td>&nbsp;
		                				</td>
		                				<td>
		                					&nbsp;
		                				</td>
		                			</td>

		                			<tr>
		                				<td>
		                					<h4>Inventory</h4>
		                				</td>
		                				<td>&nbsp;				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>COGS</strong>
		                				</td>
		                				<td>
		                					<select name = "inventory_cogs" id = "inventory_cogs">
		                						<?php
		                						if(!empty($ac8['list_id']))
		                						{
		                							print '<option value="'.$ac8['list_id'].'">'.$ac8['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				<td>
		                					<strong>Sales</strong>
		                				</td>
		                				<td>
		                					<select name = "inventory_sales" id = "inventory_sales">
		                						<?php
		                						if(!empty($ac9['list_id']))
		                						{
		                							print '<option value="'.$ac9['list_id'].'">'.$ac9['display_text'].'</option>';
		                						}
		                						print '<option value = "">-Select-</option>';
		                						foreach($qb_accounts as $acct)
		                						{
		                							print '<option value="'.$acct['list_id'].'">'.$acct['name'].'</option>';
		                						}
		                						?>
											</select>				
		                				</td>
		                			</tr>

		                			<tr>
		                				</td>
		                				<td>&nbsp;
		                				</td>
		                				<td>
		                					<input type = "submit" value = "SAVE" class = "btn btn-primary">
		                				</td>
		                			</td>

		                		</table>
							</form>
						<?php
					}
					?>
				</div>
				</div>

            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
