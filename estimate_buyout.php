<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($estimates_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id = $_REQUEST['project_id'];
$id = $project_id;
$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$estimateid=$_REQUEST['estimateid'];

$estimate = $vujade->get_estimate($estimateid);

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# previous button
if($action==1)
{
	$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];

	# merge 4 arrays into 1
	$posted = array_map(null, $_POST['buyout_id'], $_POST['vendor'], $_POST['description'], $_POST['cost']);
	$s=array();
	# loop through and update each database record 
	foreach($posted as $arr)
	{
		$row_id=$arr[0];
		$cost = str_replace(',', '', $arr[3]);
		$cost = str_replace('$', '', $arr[3]);

		$description = $arr[2];
		$subcontractor=$arr[1];
		if($row_id=="0")
		{
			// only create new one if not all blank
			if( (!empty($cost)) || ($description) || ($subcontractor) )
			{
				unset($row_id);
				$vujade->create_row('estimates_buyouts');
				$row_id=$vujade->row_id;
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'estimate_id',$estimate['database_id']);
			}
		}
		else
		{
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
		}
	}

	$vujade->page_redirect('estimate_machines.php?project_id='.$project_id.'&estimateid='.$estimateid);
}

# done button
if($action==2)
{
	$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];

	# merge 4 arrays into 1
	$posted = array_map(null, $_POST['buyout_id'], $_POST['vendor'], $_POST['description'], $_POST['cost']);
	$s=array();
	# loop through and update each database record 
	foreach($posted as $arr)
	{
		$row_id=$arr[0];
		$cost = str_replace(',', '', $arr[3]);
		$cost = str_replace('$', '', $arr[3]);
		$description = $arr[2];
		$subcontractor=$arr[1];
		if($row_id=="0")
		{
			// only create new one if not all blank
			if( (!empty($cost)) || ($description) || ($subcontractor) )
			{
				unset($row_id);
				$vujade->create_row('estimates_buyouts');
				$row_id=$vujade->row_id;
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'estimate_id',$estimate['database_id']);
			}
		}
		else
		{
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
		}
	}

	$vujade->page_redirect('project_estimates.php?id='.$project_id.'&estimateid='.$estimateid);
}

# update all button
if($action==3)
{
	$project_id=$_POST['project_id'];
	$estimateid=$_POST['estimateid'];

	# merge 4 arrays into 1
	$posted = array_map(null, $_POST['buyout_id'], $_POST['vendor'], $_POST['description'], $_POST['cost']);
	$s=array();
	# loop through and update each database record 
	foreach($posted as $arr)
	{
		$row_id=$arr[0];
		$cost = str_replace(',', '', $arr[3]);
		$cost = str_replace('$', '', $arr[3]);

		$description = $arr[2];
		$subcontractor=$arr[1];
		if($row_id=="0")
		{
			// only create new one if not all blank
			if( (!empty($cost)) || ($description) || ($subcontractor) )
			{
				unset($row_id);
				$vujade->create_row('estimates_buyouts');
				$row_id=$vujade->row_id;
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
				$s[]=$vujade->update_row('estimates_buyouts',$row_id,'estimate_id',$estimate['database_id']);
			}
		}
		else
		{
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'cost',$cost);
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'description',$description);
			$s[]=$vujade->update_row('estimates_buyouts',$row_id,'subcontractor',$subcontractor);
		}
	}

	//$vujade->page_redirect('estimate_labor.php?project_id='.$project_id.'&estimateid='.$estimateid);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

$section=3;
$menu=4;
$title = "Estimate Outsource";
$charset = "ISO-8859-1";
require_once('h.php');
?>
<section id="content_wrapper">

<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
  <ol class="breadcrumb">
    <li class="crumb-active">
      <a href = "#">Estimate <?php print $estimate['estimate_id']; ?> Outsource</a>
    </li>
  </ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">

	<div class="admin-form theme-primary">

	    <?php 
	    $vujade->show_errors();
	    $vujade->show_messages();
	    ?>

	    <form method = "post" action = "estimate_buyout.php" id = "nd" class = "form-inline">
		<input type = "hidden" name = "action" id = "action" value = "">
		<input type = "hidden" name = "project_id" value = "<?php print $project_id; ?>">
		<input type = "hidden" value = "<?php print $estimateid; ?>" name = "estimateid">
		<div class="panel heading-border panel-primary">
            <div class="panel-body bg-light">
		
				<div style = "width:100%;height:50px;overflow-x:auto;margin-bottom:15px;">
					<?php print $estimate['description']; ?>
				</div>

				<div class = "row">
					<div class = "col-md-12">
						<div style = "" id = "buyout_items">
							<table width = "100%" class = "table table-hover table-striped  table-bordered">
								<thead>
									<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
									<th>
										Vendor
									</th>
									<th>
										Description
									</th>
									<th>
										Amount
									</th>
									<th>
										&nbsp;
									</th>
									</tr>
									</thead>

									<tbody>
										<?php
										$buyout_total=0;
										$buyouts = $vujade->get_buyouts($estimateid);
										if($buyouts['error']=="0")
										{
											unset($buyouts['error']);
											foreach($buyouts as $buyout)
											{
												print '<tr>';
												print '<td>';
												print '<input type = "hidden" name = "buyout_id[]" value = "'.$buyout['database_id'].'">';
												print '<input type = "text" class = "existing-buyout-vendor form-control" value = "'.str_replace('"','&quot;',$buyout['subcontractor']).'" style = "width:100%;" name = "vendor[]">';
												print '</td>';

												print '<td>';
												print '<input type = "text" class = "existing-buyout-description form-control" value = "'.str_replace('"','&quot;',$buyout['description']).'" style = "width:100%;" name = "description[]">';
												print '</td>';

												print '<td>';
												print '<input type = "text" class = "existing-buyout-cost form-control" value = "'.$buyout['cost'].'" style = "" name = "cost[]">';
												print '</td>';

												print '<td>';
												print '&nbsp;';
												print '<a href = "#" id = "'.$buyout['database_id'].'" class = "plus-buyout-delete btn btn-xs btn-danger">X</a>';
												print '</td>';
												print '</tr>';
												$bcost = str_replace(',','',$buyout['cost']);
												$buyout_total=$buyout_total+$bcost;
											}
										}

										# blank row
										print '<tr>';

										print '<td>';

										print '<input type = "hidden" name = "buyout_id[]" value ="0">';

										print '<input type = "text" class = "new-buyout-vendor form-control" value = "" style = "width:100%;" name = "vendor[]">';
										print '</td>';

										print '<td>';
										print '<input type = "text" class = "new-buyout-description form-control" value = "" style = "width:100%;" name = "description[]">';
										print '</td>';

										print '<td>';
										print '<input type = "text" class = "new-buyout-cost form-control" value = "" style = "" name = "cost[]">';
										print '</td>';

										print '<td>';

										//print ' <a href = "#" id = "new-buyout" class = "plus-buyout-new btn btn-primary btn-xs">Save</a> ';
										print '&nbsp;';
										print '</td>';
										print '</tr>';
										?>
										<tr class = "dark">
											<td>&nbsp;</td>
											<td>&nbsp;</td>
											<td>Total:</td>
											<td>$<?php print number_format($buyout_total,2,'.',','); ?></td>
										</tr>
									</tbody>
							</table>
						</div>
					</div>
				</div>

		        <div style = "margin-top:15px;">
					<a class = "btn btn-primary" href = "#" id = "prev" style = "">PREVIOUS</a> 

					<a class = "btn btn-success" href = "#" id = "update" style = "margin-left:15px;">UPDATE ALL</a>  

					<a class = "btn btn-success" href = "#" id = "done" style = "margin-left:15px;">SAVE AND COMPLETE</a>  

			        </form> 
		        </div>

			</div>
		</div>
	</div>

</section>
</section>

<div style = "display:none;" id = "hidden_desc">
<?php print $project['description']; ?>
</div>

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Page Plugins -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  //var estimateid = <?php print $estimateid; ?>;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // prevent user from pressing return on qty fields
	$("form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) 
        {
            $('button[type=submit] .default').click();   
            return false;
        }
    });

	$('#update').click(function(e)
	{
		e.preventDefault();
		$('#action').val('3');
		$('#nd').submit();
	});

	$('.plus-buyout-new').click(function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#buyout_items').html('');
		$('#buyout_items').html(loading);
		var estimate_id = "<?php print $estimateid; ?>";
		var vendor = $(this).closest('tr').find('.new-buyout-vendor').val();
		var description = $(this).closest('tr').find('.new-buyout-description').val();
		var cost = $(this).closest('tr').find('.new-buyout-cost').val();
		$.post( "jq.buyout_items.php", { action: 1, estimate_id: estimate_id, vendor: vendor, description: description, cost: cost })
		  .done(function( data ) 
		  {
		  		$('#buyout_items').html(data);
		  });
	});

	$('.plus-buyout-delete').click(function(e)
	{
		e.preventDefault();
		var loading = '<i class="fa fa-cog fa-spin"></i>';
		$('#buyout_items').html('');
		$('#buyout_items').html(loading);
		$('#tfc').html('');
		var estimate_id = "<?php print $estimateid; ?>";
		var id = this.id;
		$.post( "jq.buyout_items.php", { action: 3, id: id, estimate_id: estimate_id })
		  .done(function( data ) 
		  {
		  		$('#buyout_items').html(data);
		  });
	});

	// previous button
	$('#prev').click(function()
	{
		$('#action').val('1');
		$('#nd').submit();
	});

	// done button
	$('#done').click(function()
	{
		$('#action').val('2');
		$('#nd').submit();
	});
	
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
