<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

$id = $_REQUEST['id'];

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
# update the scope
if($action==1)
{
	$description=$_POST['description'];
	$s = $vujade->update_row('projects',$id,'description',$description,'project_id');
	if($s==1)
	{
		$vujade->page_redirect('project.php?id='.$id);
	}
}
if($action==2)
{
	$description=$_POST['description'];
	$s = $vujade->update_row('projects',$id,'description',$description,'project_id');
	$vujade->page_redirect('edit_project_customer.php?id='.$id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=2;

$title = "Edit Project - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                  <p>
                    <span class="label label-default">Step 1: Project Setup</span> <span class="label label-default">Step 2: Customer Selection</span> <span class="label label-primary">Step 3: Scope of Work</span>
                  </p>

                  <form method="post" action="edit_project_scope.php" id="form">
                  
                    <div class="row">

                      <div class="col-md-12">
                      
                      		<textarea name = "description" id = "description" class = "ckeditor">
                      			<?php print ltrim($project['description']); ?>
							</textarea>
							
<!-- ckeditor new version 4.5x -->
<?php require_once('ckeditor.php'); ?>

                      </div>
                    </div>

                    <input type = "hidden" name = "id" value = "<?php print $id; ?>">
					<input type = "hidden" name = "action" id = "action" value = "1">

                    <div style = "margin-top:15px;">
                      <div style = "float:left">
                        <a href = "#" id = "back" class = "btn btn-lg btn-danger" style = "width:100px;">BACK</a> <a href = "#" id = "done" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND COMPLETE</a>
                      </div>
                      <div style = "float:right;">
                      </div>  
                    </div>

                  </form>
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    $('#back').click(function(e)
	{
		e.preventDefault();
		$('#action').val(2);
		$('#form').submit();
	});
	$('#done').click(function(e)
	{
		e.preventDefault();
		$('#action').val(1);
		$('#form').submit();
	});

	// fix the indent on the text area
    var d = $('#description').html();
    $("#description").html($.trim(d));
 
});
</script>

</body>

</html>