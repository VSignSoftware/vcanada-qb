<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$project_id=$_REQUEST['project_id'];
$id = $project_id;
$cid = $_REQUEST['cid'];
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# edit a conversation
if($action==1)
{
	$date = $_REQUEST['date'];
	$msg = trim($_REQUEST['msg']);
	$s = array();
	$employee = $vujade->get_employee($_SESSION['user_id']);
	$s[]=$vujade->update_row('sales_conversations',$cid,'date',$date);
	$s[]=$vujade->update_row('sales_conversations',$cid,'body',$msg);
	$s[]=$vujade->update_row('sales_conversations',$cid,'user_id',$_SESSION['user_id']);
	$s[]=$vujade->update_row('sales_conversations',$cid,'ts',strtotime($date));
	$s[]=$vujade->update_row('sales_conversations',$cid,'name_display',$employee['fullname']);
	$vujade->page_redirect('sales.php?tab=0&id='.$id.'&conversation_id='.$cid);
}
$conversation = $vujade->get_conversation($cid);

$shop_order = $vujade->get_shop_order($id, 'project_id');
$menu = 13;
$title = "Sales - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

	<!-- Start: Topbar -->

	<header id="topbar">

		<div class="topbar-left">

			<ol class="breadcrumb">

				<li class="crumb-active">

					<a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>

				</li>

			</ol>

		</div>

	</header>

	<!-- Begin: Content -->
	<section id="content" class="table-layout animated fadeIn">

		<div class="admin-form theme-primary" style="display: block;">

			<div class="panel heading-border panel-primary">

				<div class="panel-body bg-light">

					<div id="" style = "margin-top:15px;">

						<!-- form -->
						<form id = "form" method = "post" action = "edit_sales_conversation.php">
							<label for="date" style="margin: 0 0 15px;">Date:</label>
							<input type = "text" name = "date" id = "date" class="form-control" style = "width:300px;" value="<?php print $conversation['date']; ?>">
							<div id = "error_1" style = "display:none;">This field cannot be empty.</div>
							<br>
							<label for="msg" style="margin: 0 0 15px;">Message:</label>
							<textarea name = "msg" id = "msg" class="form-control ckeditor" style = "width: 100%;height:300px;"><?php print $conversation['body']; ?></textarea>
							<div id = "error_3" style = "display:none;">This field cannot be empty.</div>
							<input type = "hidden" name = "action" value = "1">
							<input type = "hidden" name = "project_id" value = "<?php print $id; ?>">
							<input type = "hidden" name = "cid" id = "cid" value = "<?php print $cid; ?>">
							<div style = "margin-top:15px;">
								<div style = "float:left">
									<a href = "#" id = "cancel" class = "btn btn-lg btn-danger" style = "width:100px;">CANCEL</a> <a id = "save" href = "#" class = "btn btn-lg btn-success" style = "width:100px; border-radius: 3px;">SAVE</a>
								</div>
								<div style = "float:right;">
									
								</div>
							</div>
						</form>

						<!-- ckeditor new version 4.5x -->
						<?php require_once('ckeditor.php'); ?>
					</div>

				</div>
			</div>
		</div>

	</section>
	<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function()
	{

		"use strict";

		// Init Theme Core
		Core.init();

		// date picker
		$('#date').datepicker();

		$('#cancel').click(function(e)
		{
			e.preventDefault();
			window.location="sales.php?tab=1&id=<?php print $id; ?>";
		});

		$('#save').click(function(e)
		{
			e.preventDefault();
			$('#form').submit();
		});

		var n = "<?php print $conversation['body']; ?>";
    	$("#msg").html($.trim(n));
	});
</script>
<!-- END: PAGE SCRIPTS -->
</body>

</html>
