<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$project_id = $_REQUEST['project_id'];
$project = $vujade->get_project($project_id,2);
$shop_order = $vujade->get_shop_order($project_id,'project_id');
$logo_data = $vujade->get_logo(3);
$logo = '<img src = "'.$logo_data['url'].'" width = "260" height = "60">';

$charset="ISO-8859-1";
$html = '<!DOCTYPE html>
    <head>
    <meta charset="'.$charset.'">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Shop Order</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="css/print_so_style.css">
	<style>
	.marker
	{
		background-color: yellow;
	}
	</style>
    </head>
    <body>';

$proposal_items = $vujade->get_items_for_proposal($project_id);

if($proposal_items['error'] == 0) 
{
    unset($proposal_items['error']);
    $html.='<p class = "tdfixed" style = "font-size:16px;border-bottom:0px solid black; border-top:2px solid black; padding-top:5px; padding-bottom:5px;"><strong>Description:</strong><br>';
    foreach($proposal_items as $item) 
    {
    	//$html.=$item['item'];
    	//$item['item'] = str_replace("&nbsp;", '&nbsp; ', $item['item']);
    	//$item['item'] = str_replace("<p>", '', $item['item']);
    	//$item['item'] = str_replace("</p>", '<br>', $item['item']);
    	$html.=$item['item'].'<br>';
    	// $html.=addcslashes($item['item']);
    	//$html.=$item['item'];
    	//$item['item']=str_replace("<br /> ","<br />",$item['item']);
    	//$html.=$item['item'];
    	//$html.=htmlentities($item['item']).'^<br>';
    }
    $html.='</p>';
}
else
{
	$html.='<p class = "tdfixed" style = "font-size:16px;border-bottom:0px solid black; border-top:2px solid black; padding-top:5px; padding-bottom:5px;"><strong>Description:</strong><br>'.$shop_order['description'].'</p>';
}

$html.='</body></html>';

$header_1 = '<htmlpageheader name="header_1">'.$logo.'<br>';
$header_1 .= '<div class = "header" style = "border:4px solid black;">';
$header_1 .= '<div class = "header_left">';
$header_1 .= '<h1 style = "font-size:34px;">'.$shop_order['type'].' Order</h1>';
$header_1 .= '</div>';
$header_1 .= '<div class = "header_right">';
$header_1 .= '<div class = "hr_col1">';
$header_1 .= 'Job No.';
$header_1 .= '</div>';
$header_1 .= '<div class = "hr_col2">';
$header_1 .= $project['project_id'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '<div class = "hr_col1">';
$header_1 .= 'Open Date';
$header_1 .= '</div>';
$header_1 .= '<div class = "hr_col2">';
$header_1 .= $shop_order['date_opened'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '<div class = "hr_col1">';
$header_1 .= 'Revised Date';
$header_1 .= '</div>';
$header_1 .= '<div class = "hr_col2">';
$header_1 .= $shop_order['date_revised'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '</div>';
$header_1 .= '</div>';

$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col75">';
$header_1 .= 'Job Name';
$header_1 .= '</div>';
$header_1 .= '<div class = "column_300_bb">';
$header_1 .= $project['site'].'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "spacer_30px">';
$header_1 .= '&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_130">';
$header_1 .= 'Project Manager';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_200_bb">';
$header_1 .= $project['project_manager'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '</div>';

$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col75">';
$header_1 .= 'Address';
$header_1 .= '</div>';
$header_1 .= '<div class = "column_300_bb">';
$header_1 .= $project['address_1'];
if(!empty($project['address_2']))
{
    $header_1 .= ', '.$project['address_2'];
}
$header_1 .= '&nbsp;</div>';

$header_1 .= '<div class = "spacer_30px">';
$header_1 .= '&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_130">';
$header_1 .= 'Salesperson';
$header_1 .= '</div>';
$header_1 .= '<div class = "column_200_bb">';
$header_1 .= $project['salesperson'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '</div>';

$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col75">';
$header_1 .= '&nbsp;';
$header_1 .= '</div>';
$header_1 .= '<div class = "column_300_bb">';
$header_1 .= $project['city'].', '.$project['state'].' '.$project['zip'].'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "spacer_30px">';
$header_1 .= '&nbsp;';
$header_1 .= '</div>';
$header_1 .= '</div>';

// blank row
$header_1 .= '<div class = "row" style = "width:100%;height:5px;">';
$header_1 .= '</div>';

// project contact row
$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col100" style = "width:110px;">';
$header_1 .= 'Project Contact';
$header_1 .= '</div>';

$pc = $vujade->limit_chars($project['project_contact'],20);
$sc = $vujade->limit_chars($project['site_contact'],20);
$header_1 .= '<div class = "col100b">';
$header_1 .= $pc.'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "col75" style = "width:50px;margin-left:15px;">';
$header_1 .= 'Phone';
$header_1 .= '</div>';

$header_1 .= '<div class = "col100b" style = "">';
$header_1 .= $project['project_contact_phone'].'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "col75" style = "width:50px;margin-left:20px;">';
$header_1 .= 'Email';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_200_bb" style = "width:270px">';
$header_1 .= $project['project_contact_email'].'&nbsp;';
$header_1 .= '</div>';

$header_1.='</div>';

// site contact row
$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col100" style = "width:110px;">';
$header_1 .= 'Site Contact';
$header_1 .= '</div>';

$header_1 .= '<div class = "col100b">';
$header_1 .= $sc.'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "col75" style = "width:50px;margin-left:15px;">';
$header_1 .= 'Phone';
$header_1 .= '</div>';

$header_1 .= '<div class = "col100b" style = "">';
$header_1 .= $project['site_contact_phone'].'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "col75" style = "width:50px;margin-left:20px;">';
$header_1 .= 'Email';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_200_bb" style = "width:270px">';
$header_1 .= $project['site_contact_email'].'&nbsp;';
$header_1 .= '</div>';

$header_1.='</div>';

// blank row
$header_1 .= '<div class = "row" style = "width:100%;height:5px;">';
$header_1 .= '</div>';

$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col100" style = "width:90px;">';
$header_1 .= 'Design #';
$header_1 .= '</div>';
$header_1 .= '<div class = "col100b">';
$header_1 .= $shop_order['design'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '<div class = "col75" style = "width:85px;margin-left:15px;">';
$header_1 .= 'Estimate(s)';
$header_1 .= '</div>';
$header_1 .= '<div class = "col300" style = "width:440px;">';
$header_1 .= str_replace('^', ' ', $shop_order['estimates']).'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '</div>';

$header_1 .= '<div class = "row">';
$header_1 .= '<div class = "col100" style = "width:90px;">';
$header_1 .= 'DATE DUE';
$header_1 .= '</div>';

$header_1 .= '<div class = "col100b">';
$header_1 .= $shop_order['due_date'].'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "col75" style = "width:95px;margin-left:15px;">';
$header_1 .= 'OR WEEK OF';
$header_1 .= '</div>';

if(!empty($shop_order['due_date']))
{
	$or_week_of='';
}
else
{
	$or_week_of=$shop_order['or_week_of'];
}

$header_1 .= '<div class = "col1002" style = "width:82px;margin-right:15px;">';
$header_1 .= $or_week_of.'&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "spacer_10px">';
$header_1 .= '&nbsp;';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_150_bt" style = "margin-left:5px;width:130px;">';
$header_1 .= 'DATE COMPLETED';
$header_1 .= '</div>';

$header_1 .= '<div class = "column_130_bb" style = "width:100px;">';
$header_1 .= $shop_order['closed_date'].'&nbsp;';
$header_1 .= '</div>';
$header_1 .= '</div></htmlpageheader>';

$footer_1 = '<htmlpagefooter name="footer_1"><div class = "footer">Page {PAGENO}</div></htmlpagefooter>';

//print $html;
//die;

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$margin_left="10";
$margin_right="10";
$margin_top="110";
$margin_bottom="5";
$margin_header="5";
$margin_footer="5";
$orientation="";
$mpdf = new mPDF('', 'LETTER', 0, 'Helvetica', $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);

//new mPDF($mode, $format, $font_size, $font, $margin_left, $margin_right, $margin_top, $margin_bottom, $margin_header, $margin_footer, $orientation);

//$mpdf->setAutoTopMargin = 'stretch';
//$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
//$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
//$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->defaultfooterline = 0;
//$page_total = $mpdf->AliasNbPages('[pagetotal]');
$mpdf->setFooter('Page {PAGENO} of {nb}');
$mpdf->WriteHTML($html);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-shop-order.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output('Shop Order.pdf','I'); 
}
?>