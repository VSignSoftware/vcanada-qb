<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

# permissions
$permissions = $vujade->get_permission($_SESSION['user_id'],'Admin');
if($_SESSION['is_admin']==1)
{
	$permissions['read']=1;
}
if($permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();

// labor departments
$departments = $vujade->get_labor_types();

$html='';

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;

if($departments['error']!="0")
{
	$vujade->set_error('No labor departments found. ');
	$vujade->show_errors();
}
else
{
	unset($departments['error']);
	$html.= '<html><head><title>Work Department Report</title>';
	$html.='<style>';
	$html.='
	body
	{
		font-size:12px;
		font-family:arial;
		color:black;
	}
	table 
	{
	    border-collapse: collapse;
	}
	table, td, th 
	{
	    border: 1px solid #cecece;
	}
	td
	{

	}
	';
	$html.='</style>';
	$html.='</head><body>';

	// print type
	$section = $_REQUEST['section'];
	
	// print all no page breaks between sections
	if($section=='ALL')
	{
		foreach($departments as $dept)
		{

			$html.='<h3>'.$dept['type'].'</h3>';

			// get job data and hours for every job in this department
			$section_data = $vujade->get_temp_dept_data($dept['database_id'],'',2);

			if($section_data['count']>0)
			{
				unset($section_data['count']);
				unset($section_data['error']);
				unset($section_data['sql']);

				$project_hours = 0;
				$remaining_hours = 0;
				$allotted_total = 0;
				$remaining_total = 0;

				$html.='<table width = "100%" class = "table table-border">';
				$html.='<tr>';
				$html.='<td>';
				$html.='#';
				$html.='</td>';
				$html.='<td>';
				$html.='Name';
				$html.='</td>';
				$html.='<td>';
				$html.='Hours Allotted';
				$html.='</td>';
				$html.='<td>';
				$html.='Hours Remaining';
				$html.='</td>';
				$html.='<td>';
				$html.='Shipping Date';
				$html.='</td>';
				$html.='<td>';
				$html.='Due Date';
				$html.='</td>';
				$html.='</tr>';

				foreach($section_data as $sd)
				{
					$allotted_total+=$sd['hours'];

					$p = $vujade->get_project($sd['project'],2);
					$js = $vujade->get_job_status($sd['project']);

					$html.='<tr>';
					$html.='<td valign = "top" width = "10%">';
					$html.=$sd['project'];
					$html.='</td>';

					// project name
					$html.='<td width = "60%">';
					$html.=$p['site'];
					$html.='</td>';

					// hours allotted
					$html.='<td width = "10%">';
					$html.=$sd['hours'];
					$html.='</td>';

					// remaining hours
					$tc_data = $vujade->get_actual_labor($dept['database_id'],$sd['project']);
					$remaining_hours=0;
					$remaining_hours=$sd['hours']-$tc_data['total'];
					$html.='<td width = "10%">';
					if($remaining_hours>0)
					{
						$html.=$remaining_hours;
					}
					else
					{
						$html.='<font color = "red">'.$remaining_hours.'</font>';
					}
					$html.='</td>';

					$remaining_total+=$remaining_hours;

					// due date
					$html.='<td width = "10%">';
					$html.=$js['shipping_date'];
					$html.='</td>';

					// due date
					$html.='<td width = "10%">';
					$html.=$js['installation_date'];
					$html.='</td>';

					$html.='</tr>';
					$remaining_hours=0;
				}

				// summary rows
				$html.='<tr>';
				$html.='<td colspan = "2">&nbsp;';
				$html.='</td>';
				$html.='<td colspan = "1">'.$allotted_total;
				$html.='</td>';
				$html.='<td colspan = "1">'.$remaining_total;
				$html.='</td>';
				$html.='<td colspan = "1">&nbsp;';
				$html.='</td>';
				$html.='</tr>';

				$html.='</table>';
			}
		}
	}

	// print all and print page breaks between sections
	if($section=="ALL-pb")
	{
		foreach($departments as $dept)
		{

			$html.='<h3>'.$dept['type'].'</h3>';

			// get job data and hours for every job in this department
			$section_data = $vujade->get_temp_dept_data($dept['database_id'],'',2);

			if($section_data['count']>0)
			{
				unset($section_data['count']);
				unset($section_data['error']);
				unset($section_data['sql']);

				$project_hours = 0;
				$remaining_hours = 0;
				$allotted_total = 0;
				$remaining_total = 0;

				$html.='<table width = "100%" class = "table table-border">';
				$html.='<tr>';
				$html.='<td>';
				$html.='#';
				$html.='</td>';
				$html.='<td>';
				$html.='Name';
				$html.='</td>';
				$html.='<td>';
				$html.='Hours Allotted';
				$html.='</td>';
				$html.='<td>';
				$html.='Hours Remaining';
				$html.='</td>';
				$html.='<td>';
				$html.='Due Date';
				$html.='</td>';
				$html.='</tr>';
				
				foreach($section_data as $sd)
				{
					$allotted_total+=$sd['hours'];

					$p = $vujade->get_project($sd['project'],2);
					$js = $vujade->get_job_status($sd['project']);

					$html.='<tr>';
					$html.='<td valign = "top" width = "10%">';
					$html.=$sd['project'];
					$html.='</td>';

					// project name
					$html.='<td width = "70%">';
					$html.=$p['site'];
					$html.='</td>';

					// hours allotted
					$html.='<td width = "10%">';
					$html.=$sd['hours'];
					$html.='</td>';

					// remaining hours
					$tc_data = $vujade->get_actual_labor($dept['database_id'],$sd['project']);
					$remaining_hours=0;
					$remaining_hours=$sd['hours']-$tc_data['total'];
					$html.='<td width = "10%">';
					if($remaining_hours>0)
					{
						$html.=$remaining_hours;
					}
					else
					{
						$html.='<font color = "red">'.$remaining_hours.'</font>';
					}
					$html.='</td>';

					$remaining_total+=$remaining_hours;

					// due date
					$html.='<td width = "10%">';
					$html.=$js['installation_date'];
					$html.='</td>';

					$html.='</tr>';
					$remaining_hours=0;
				}

				// summary rows
				$html.='<tr>';
				$html.='<td colspan = "2">&nbsp;';
				$html.='</td>';
				$html.='<td colspan = "1">'.$allotted_total;
				$html.='</td>';
				$html.='<td colspan = "1">'.$remaining_total;
				$html.='</td>';
				$html.='<td colspan = "1">&nbsp;';
				$html.='</td>';
				$html.='</tr>';

				$html.='</table>';
				$html.="<pagebreak />";
			}
		}
	}

	// print one section
	if(ctype_digit($section))
	{
		foreach($departments as $dept)
		{
			if($dept['database_id']==$section)
			{

				$html.='<h3>'.$dept['type'].'</h3>';

				// get job data and hours for every job in this department
				$section_data = $vujade->get_temp_dept_data($dept['database_id'],'',2);

				if($section_data['count']>0)
				{
					unset($section_data['count']);
					unset($section_data['error']);
					unset($section_data['sql']);

					$project_hours = 0;
					$remaining_hours = 0;
					$allotted_total = 0;
					$remaining_total = 0;

					$html.='<table width = "100%" class = "table table-border">';
					$html.='<tr>';
					$html.='<td>';
					$html.='#';
					$html.='</td>';
					$html.='<td>';
					$html.='Name';
					$html.='</td>';
					$html.='<td>';
					$html.='Hours Allotted';
					$html.='</td>';
					$html.='<td>';
					$html.='Hours Remaining';
					$html.='</td>';
					$html.='<td>';
					$html.='Due Date';
					$html.='</td>';
					$html.='</tr>';
					
					foreach($section_data as $sd)
					{
						$allotted_total+=$sd['hours'];

						$p = $vujade->get_project($sd['project'],2);
						$js = $vujade->get_job_status($sd['project']);

						$html.='<tr>';
						$html.='<td valign = "top" width = "10%">';
						$html.=$sd['project'];
						$html.='</td>';

						// project name
						$html.='<td width = "70%">';
						$html.=$p['site'];
						$html.='</td>';

						// hours allotted
						$html.='<td width = "10%">';
						$html.=$sd['hours'];
						$html.='</td>';

						// remaining hours
						$tc_data = $vujade->get_actual_labor($dept['database_id'],$sd['project']);
						$remaining_hours=0;
						$remaining_hours=$sd['hours']-$tc_data['total'];
						$html.='<td width = "10%">';
						if($remaining_hours>0)
						{
							$html.=$remaining_hours;
						}
						else
						{
							$html.='<font color = "red">'.$remaining_hours.'</font>';
						}
						$html.='</td>';

						$remaining_total+=$remaining_hours;

						// due date
						$html.='<td width = "10%">';
						$html.=$js['installation_date'];
						$html.='</td>';

						$html.='</tr>';
						$remaining_hours=0;
					}

					// summary rows
					$html.='<tr>';
					$html.='<td colspan = "2">&nbsp;';
					$html.='</td>';
					$html.='<td colspan = "1">'.$allotted_total;
					$html.='</td>';
					$html.='<td colspan = "1">'.$remaining_total;
					$html.='</td>';
					$html.='<td colspan = "1">&nbsp;';
					$html.='</td>';
					$html.='</tr>';
						
					$html.='</table>';
				}
			}
		}
	}

	$html.='</body></html>';

	$footer_1 = '<div style = "text-align:center;margin-top:10px;">Page {PAGENO}</div>';

	$header_1='<h2>Work Department Report</h2>';
	$header_1='<p>'.date('m/d/Y').'</p>';

	# mpdf class (pdf output)
	require_once("mpdf60/mpdf.php");
	$mpdf = new mPDF('', 'LETTER-L', 0, 'Helvetica', 10, 10, 20, 10, 10);
	$mpdf->DefHTMLHeaderByName('header_1',$header_1);
	$mpdf->SetHTMLHeaderByName('header_1');
	$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
	$mpdf->SetHTMLFooterByName('footer_1');
	$mpdf->WriteHTML($html);

	// download the pdf if phone or tablet
	require_once('mobile_detect.php');
	$detect = new Mobile_Detect;
	// Any mobile device (phones or tablets).
	if( ($detect->isMobile()) || ($detect->isTablet()) ) 
	{
		$pdfts = strtotime('now');
		$pdfname = 'mobile_pdf/'.$pdfts.'-department-hours.pdf';

		// set to mysql table (chron job deletes these files nightly after they are 1 day old)
		$vujade->create_row('mobile_pdf');
		$pdf_row_id = $vujade->row_id;
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
		$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
	 	$mpdf->Output($pdfname,'F');
	 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
	}
	else
	{
		$mpdf->Output('Department Hours.pdf','I');  
	}
}
?>