<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions: all accounting and all employee records
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($er_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if(isset($_REQUEST['id']))
{
	$id = $_REQUEST['id'];
}
else
{
	$id = 1;
}
if(!ctype_digit($id))
{
	$id = 1;
}
if($id == 0)
{
	$id = 1;
}
$setup = $vujade->get_setup(1);
if($setup['error']!="0")
{
	// problem
	print $setup['error'].'<br>';
	die('Setup record does not exist.');
}
$action = 0;
if(isset($_POST['action']))
{
	$action = $_POST['action'];
}
if($action==1)
{
	$s = array();
	$s[]=$vujade->update_row('misc_config',1,'invoice_email_message',$_POST['invoice_email_message']);
	$vujade->messages[]="Changes Saved.";
	$setup = $vujade->get_setup(1);
}
if($action==2)
{
	$s = array();
	$s[]=$vujade->update_row('misc_config',1,'proposal_email_message',$_POST['proposal_email_message']);
	$vujade->messages[]="Changes Saved.";
	$setup = $vujade->get_setup(1);
}
if($action==3)
{
	$s = array();
	$s[]=$vujade->update_row('misc_config',1,'purchase_order_email_message',$_POST['purchase_order_email_message']);
	$vujade->messages[]="Changes Saved.";
	$setup = $vujade->get_setup(1);
}
if($action==4)
{
	$s = array();
	$s[]=$vujade->update_row('misc_config',1,'proposal_message',$_POST['proposal_message']);
	$vujade->messages[]="Changes Saved.";
	$setup = $vujade->get_setup(1);
}
if($action==5)
{
	$s = array();
	$s[]=$vujade->update_row('misc_config',1,'design_email_message',$_POST['design_email_message']);
	$vujade->messages[]="Changes Saved.";
	$setup = $vujade->get_setup(1);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Custom Messages - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray tray-left tray100 p20" id = "left_tray" style = "width:200px;">

        	<?php
        	$ss_menu=10;
        	require_once('site_setup_menu.php');
        	?>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style="width: 100%;">

            <div class="pl20 pr50">

            	<?php
				$vujade->show_errors();
				$vujade->show_messages();
				?>

				<div class="panel panel-primary panel-border top">
                <div class="panel-body bg-light">

                	<!-- content goes here -->
                	<?php
					if($id==1)
					{
						?>
						<form method = "post" action = "custom_messages.php">
						<h3>Invoice Email Message</h3>	
						<textarea name = "invoice_email_message" class = "form-control ta"><?php print $setup['invoice_email_message']; ?></textarea>
						<br>
						<br>
						<input type = "hidden" name = "id" value = "<?php print $id; ?>">
						<input type = "hidden" name = "action" value = "1">
						<p><input type = "submit" value = "Save" class = "btn btn-primary"></p>
						</form>
					<?php
					}
					if($id==2)
					{
						?>
						<form method = "post" action = "custom_messages.php">
						<h3>Proposal Email Message</h3>
						<textarea class = "form-control ta" name = "proposal_email_message"><?php print $setup['proposal_email_message']; ?></textarea>
						<br>
						<br>
						<input type = "hidden" name = "id" value = "<?php print $id; ?>">
						<input type = "hidden" name = "action" value = "2">
						<p><input type = "submit" value = "Save" class = "btn btn-primary"></p>
						</form>
						<?php
					}
					if($id==3)
					{
						?>
						<form method = "post" action = "custom_messages.php">
						<h3>Purchase Order Email Message</h3>
						<textarea class = "form-control ta" name = "purchase_order_email_message"><?php print $setup['purchase_order_email_message']; ?></textarea>
						<br>
						<br>
						<input type = "hidden" name = "id" value = "<?php print $id; ?>">
						<input type = "hidden" name = "action" value = "3">
						<p><input type = "submit" value = "Save" class = "btn btn-primary"></p>
						</form>
					<?php
					}
					if($id==4)
					{
						?>
						<form method = "post" action = "custom_messages.php">
						<h3>Proposal Message</h3>
						<textarea class = "form-control ta" name = "proposal_message"><?php print $setup['proposal_message']; ?></textarea>
						<br>
						<br>
						<input type = "hidden" name = "id" value = "<?php print $id; ?>">
						<input type = "hidden" name = "action" value = "4">
						<p><input type = "submit" value = "Save" class = "btn btn-primary"></p>
						</form>
					<?php
					}
					if($id==5)
					{
						?>
						<form method = "post" action = "custom_messages.php">
						<h3>Design Email Message</h3>
						<textarea class = "form-control ta" name = "design_email_message"><?php print $setup['design_email_message']; ?></textarea>
						<br>
						<br>
						<input type = "hidden" name = "id" value = "<?php print $id; ?>">
						<input type = "hidden" name = "action" value = "5">
						<p><input type = "submit" value = "Save" class = "btn btn-primary"></p>
						</form>
					<?php
					}
					?>

				</div>
				</div>
            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // adjust link styles
    $('.btn-link').each(function()
    {
    	$(this).css('width','158px');
    	$(this).css('text-decoration','none');
    });

    // click on any button 
    $('.btn-link').click(function(e)
    {
    	e.preventDefault();

    	// reset all buttons to primary
    	$('.btn').each(function()
    	{
    		$(this).removeClass('btn-dark');
    		$(this).addClass('btn-primary');
    	});

    	// set this button to danger
    	$(this).removeClass('btn-primary');
    	$(this).addClass('btn-dark');

    	// hide all the menus
    	$('.ss-menu').each(function()
    	{
    		$(this).hide();
    		$(this).removeClass('animated fadeIn');
    	});

    	// show the correct menu
    	// get the menu to open
    	var id = this.id;
    	$('#'+id+'-menu').addClass('animated fadeIn');
    	$('#'+id+'-menu').show();
    	
    });

    $('.ta').css('height','300px');

  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
