<?php
session_start();
//ob_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
if(isset($_REQUEST['id']))
{
	$id = $_REQUEST['id'];
}
if(isset($_REQUEST['estimateid']))
{
	$estimateid = $_REQUEST['estimateid'];
}

# project
$project = $vujade->get_project($id,2);

# estimate
$estimate = $vujade->get_estimate($estimateid);

# materials 
$mats = $vujade->get_materials_for_estimate($estimate['database_id']);
$subtotal = 0;
if($mats['error']=="0")
{
	unset($mats['error']);
	$pHMTL = '';
	foreach($mats as $m1)
	{
		$line = $m1['qty']*$m1['cost'];
		$pHTML.= '<tr>';
		$pHTML.= '<td style = "border:1px solid black;" valign = "top">';
		$pHTML.= $m1['inventory_id'];
		$pHTML.= '</td>';
		$pHTML.= '<td style = "border:1px solid black;" valign = "top" width = "55%">';
		$pHTML.= $m1['description'];
		$pHTML.= '</td>';
		$pHTML.= '<td style = "border:1px solid black;" valign = "top">';
		$pHTML.= $m1['qty'];
		$pHTML.= '</td>';
		$pHTML.= '<td style = "border:1px solid black;" valign = "top">';
		$pHTML.= '$'.@number_format($line,2,'.',',');
		$pHTML.= '</td>';
		$pHTML.= '</tr>';
		
		$subtotal = $subtotal+$line;
	}
}

# buyouts
$buyouts = $vujade->get_buyouts($estimate['database_id']);

# setup data
$setup = $vujade->get_setup();
$buyout_overhead_percentage=$estimate['overhead_buyout_rate'];
$general_overhead_percentage=$estimate['overhead_general_rate'];
$machines_overhead_percentage=$estimate['overhead_machines_rate'];

$charset="ISO-8859-1";
$pageHTML = '
			<!DOCTYPE html>
			<head>
			<meta charset="'.$charset.'">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<title>PDF Report</title>
			<meta name="description" content="">
			<meta name="viewport" content="width=device-width, initial-scale=1">

			<style>
			.bordered
			{
			padding-bottom:3px;
			border-bottom: 1px solid #cecece;
			}
			.header
			{
			font-weight: bold;
			font-size: 14px;
			}
			.header_graybg
			{
			background-color: #cecece;
			font-weight: bold;
			font-size: 14px;
			padding-bottom:3px;
			border-bottom: 1px solid #cecece;
			}
			h1
			{
				padding-top:15px;
			}
			.heading
			{
				width:100%;
				background-color:black;
				color:white;
				text-align:center;
				margin-bottom:15px;
			}
			.footer
			{
				width:100%;
				text-align:center;
			}
			.heading_box
			{
				width:100%;
				color:black;
			}
			table {font-family: DejaVuSansCondensed; font-size: 11pt; line-height: 1.2;
			margin-top: 2pt; margin-bottom: 5pt;
			border-collapse: collapse;  }

			thead {	font-weight: bold; vertical-align: bottom; }
			tfoot {	font-weight: bold; vertical-align: top; }
			thead td { font-weight: bold; }
			tfoot td { font-weight: bold; }

			thead td, thead th, tfoot td, tfoot th { font-variant: small-caps; }

			.headerrow td, .headerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }
			.footerrow td, .footerrow th { background-gradient: linear #b7cebd #f5f8f5 0 1 0 0.2;  }

			th {	font-weight: bold;
			vertical-align: top;
			text-align:left;
			padding-left: 2mm;
			padding-right: 2mm;
			padding-top: 0.5mm;
			padding-bottom: 0.5mm;
			}

			td {
				padding-left: 2mm;
				vertical-align: top;
				text-align:left;
				padding-right: 2mm;
				padding-top: 0.5mm;
				padding-bottom: 0.5mm;
			}

			th p { text-align: left; margin:0pt;  }
			td p { text-align: left; margin:0pt;  }
			.center-text{ text-align: center; }

			.pcell1
			{
				float:left;
				width:100px;
			}
			.pcell2
			{
				float:left;
				width:300px;
				margin-left:10px;
			}
			.pcell3
			{
				float:left;
				width:410px;
				clear:both;
			}

			.pcell4
			{
				float:left;
				width:100px;
			}
			.pcell5
			{
				margin-left:10px;
				float:left;
				width:100px;
			}

			</style>

			</head>
			<body>

			<div id="mainContent">
			<div id="content">';

# description
$pageHTML.='<p><strong>Description:</strong><br>'.$estimate['description'].'</p>
';

# materials
$pageHTML.='<table width = "100%">
	<tr style = "background-color:#cecece;">
	<td valign = "top" style = "padding:5px;">
		Item No.
	</td>
	<td width = "66%" valign = "top" style = "padding:5px;">
		Description
	</td>
	<td valign = "top" style = "padding:5px;">
		Qty
	</td>
	<td valign = "top" style = "padding:5px;">
		Amount
	</td>
	</tr>';
$pageHTML.=$pHTML;
$pageHTML.='
	</table>
	<table width = "100%">
	<tr style = "border:1px solid black;text-align:right; padding:5px;">
		<td width = "100%" style = "text-align:right;"><strong>Indeterminate: ';
		$indet = $subtotal * $estimate['indeterminant'];
		$mat_total = $subtotal+$indet;

		$pageHTML.='$'.@number_format($indet,2,'.',',').' 
		 Subtotal: $'.@number_format($subtotal,2,'.',',').'<strong></td>
	</tr>
	</table>';

$pageHTML.='<p></p>';

# labor
$pageHTML.='<table width = "100%">
		<tr style = "background-color:#cecece;">
		<td width = "75%" valign = "top" style = "padding:5px;">
			Labor Description
		</td>
		<td valign = "top" style = "padding:5px;">
			Hours
		</td>
		<td valign = "top" style = "padding:5px;">
			Rate
		</td>
		<td valign = "top" style = "padding:5px;">
			Amount
		</td>
		</tr>';

$labor_subtotal=0;
$labor = $vujade->get_labor_for_estimate($estimate['estimate_id'],1);
if($labor['error']=="0")
{
	unset($labor['error']);
	foreach($labor as $lt)
	{
		$llt = $lt['rate']*$lt['hours'];
		$labor_subtotal+=$llt;
		$pageHTML .=  '<tr>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top" width = "66%">';
		$pageHTML .=  $lt['type'];
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top">';
		$pageHTML .=  $lt['hours'];
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top">';
		$pageHTML .= @number_format($lt['rate'],2,'.',',');
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top">';
		$pageHTML .=  '$'.@number_format($llt,2,'.',',');
		unset($llt);
		$pageHTML .=  '&nbsp;';
		$pageHTML .=  '</td>';
		$pageHTML .=  '</tr>';
	}
}

$pageHTML.='</table>
		<table width = "100%">
		<tr style = "border:1px solid black;text-align:right; padding:5px;">
			<td width = "100%" style = "text-align:right;"><strong>Subtotal: $'.@number_format($labor_subtotal,2,'.',',').'<strong></td>
		</tr>
		</table>';

$pageHTML.='<p></p>';

# machines
$pageHTML.='<table width = "100%">
		<tr style = "background-color:#cecece;">
		<td width = "75%" valign = "top" style = "padding:5px;">
			Machine Description
		</td>
		<td valign = "top" style = "padding:5px;">
			Quantity
		</td>
		<td valign = "top" style = "padding:5px;">
			Rate
		</td>
		<td valign = "top" style = "padding:5px;">
			Amount
		</td>
		</tr>';

$machines_subtotal=0;
$machines = $vujade->get_machines_for_estimate($estimate['estimate_id']);
if($machines['error']=="0")
{
	unset($machines['error']);
	foreach($machines as $lt)
	{
		$llt = $lt['rate']*$lt['hours'];
		$machines_subtotal+=$llt;
		$pageHTML .=  '<tr>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top" width = "66%">';
		$pageHTML .=  $lt['type'];
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top">';
		$pageHTML .=  $lt['hours'];
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top">';
		$pageHTML .= @number_format($lt['rate'],2,'.',',');
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top">';
		$pageHTML .=  '$'.@number_format($llt,2,'.',',');
		unset($llt);
		$pageHTML .=  '&nbsp;';
		$pageHTML .=  '</td>';
		$pageHTML .=  '</tr>';
	}
}

$pageHTML.='</table>
		<table width = "100%">
		<tr style = "border:1px solid black;text-align:right; padding:5px;">
			<td width = "100%" style = "text-align:right;"><strong>Subtotal: $'.@number_format($machines_subtotal,2,'.',',').'<strong></td>
		</tr>
		</table>';

$pageHTML.='<p></p>';

# buyouts (vendors/outsourced)
$pageHTML.='<table width = "100%">
	<tr style = "background-color:#cecece;">
	<td width = "40%" valign = "top" style = "padding:5px;">
		Vendor
	</td>
	<td width = "50%" valign = "top" style = "padding:5px;">
		Description
	</td>
	<td width = "10%" valign = "top" style = "padding:5px;">
		Amount
	</td>
	</tr>';

$buyouts_total=0;
$buyouts = $vujade->get_buyouts($estimateid);
if($buyouts['error']=="0")
{
	unset($buyouts['error']);
	foreach($buyouts as $buyout)
	{	
		$pageHTML .=  '<tr>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top" width = "40%">';
		$pageHTML .=  $buyout['subcontractor'];
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top" width = "50%">';
		$pageHTML .=  $buyout['description'];
		$pageHTML .=  '</td>';
		$pageHTML .=  '<td style = "border:1px solid black;padding:5px;" valign = "top" width = "10%">';
		$buyout['cost']=str_replace(',','',$buyout['cost']);
		$pageHTML .=  '$'.@number_format($buyout['cost'],2,'.',',');
		$pageHTML .=  '</td>';
		$pageHTML .=  '</tr>';
		$buyouts_total=$buyouts_total+$buyout['cost'];
	}
}

$pageHTML.='</table>
<table width = "100%">
<tr style = "border:1px solid black;text-align:right; padding:5px;">
	<td width = "100%" style = "text-align:right;"><strong>Total: $'.@number_format($buyouts_total,2,'.',',').'</strong></td>
</tr>
</table>';

# totals
$pageHTML.='<div style = "width:300px;float:right"><table width = "100%">
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Materials: </td>
		<td style = "text-align:left;">$'.@number_format($mat_total,2,'.',',').'</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Labor Cost: </td>
		<td style = "text-align:left;">$'.@number_format($labor_subtotal,2,'.',',').'</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Machines Cost: </td>
		<td style = "text-align:left;">$'.@number_format($machines_subtotal,2,'.',',').'</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Outsource</td>
		<td style = "text-align:left;">$'.@number_format($buyouts_total,2,'.',',').'</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Subtotal: </td>';

$summary_subtotal = $machines_subtotal + $buyouts_total + $labor_subtotal + $mat_total; 
$pageHTML.='
		<td style = "text-align:left;">$'.@number_format($summary_subtotal,2,'.',',').'</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">General Overhead: </td>';
// 48.5% of Inventory + Indeterminent + Total Labor Cost
$go = $subtotal + $indet + $labor_subtotal;
$go = $go * $setup['general_overhead'];
$pageHTML.='<td style = "text-align:left;">$'.@number_format($go,2,'.',',').'</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Outsource Overhead: </td>';
$buyouts_overhead = $buyouts_total * $buyout_overhead_percentage;
$pageHTML.='
		<td style = "text-align:left;">$'.@number_format($buyouts_overhead,2,'.',',').'</td>
	</tr>';

$pageHTML.='<tr style = "border:1px solid black;text-align:right;">
		<td style = "text-align:right;">Machine Overhead: </td>';
$machines_overhead = $machines_subtotal * $machines_overhead_percentage;
$pageHTML.='
		<td style = "text-align:left;">$'.@number_format($machines_overhead,2,'.',',').'</td>
	</tr>';

$pageHTML.='<tr>
		<td colspan = "2">&nbsp;</td>
	</tr>
	<tr style = "border:1px solid black;text-align:right;">';
$factory_cost = $mat_total+$labor_subtotal+$machines_subtotal+$machines_overhead+$buyouts_total+$buyouts_overhead+$go;
$pageHTML.='<td style = "text-align:right;"><strong>Total Factory Cost: </strong></td>
		<td style = "text-align:left;"><strong>$'.@number_format($factory_cost,2,'.',',').'</strong></td>
	</tr>
</table></div>';

$pageHTML.='</div></div></body></html>';
$pageHTML = iconv("UTF-8","UTF-8//IGNORE",$pageHTML);

# header
$header_1 = '<div class = "heading">
 	<h1>Job Estimate Worksheet</h1>
	</div>
	<div class = "heading_box">
	<table width = "100%">
		<tr>
			<td width = "60%">
				<table width = "100%">
				<tr>
				<td valign = "top" width = "25%">Job Name
				</td>
				<td valign = "top" width = "75%"><strong>'.$project['site'].'</strong>
				</td>
				</tr>
				<tr>
				<td>Address
				</td>
				<td><strong>'.$project['address_1'];
				if(!empty($project['address_2']))
				{
					$header_1.=', '.$project['address_2'];
				}
				$header_1.='<br>'.$project['city'].', '.$project['state'].' '.$project['zip'].'</strong>
				</td>
				</tr>
				<tr>
				<td>Salesperson
				</td>
				<td><strong>'.$project['salesperson'].'</strong>
				</td>
				</tr>
				</table>
			</td>

			<td width = "40%">
				<table width = "100%">
				<tr>
				<td valign = "top" width = "45%">
				Estimate #<br>
				Design No.<br>
				Date Opened<br>
				Date Required<br>
				Date Revised
				</td>
				<td valign = "top" width = "55%"><strong>'.$estimate['estimate_id'].'<br>'.$estimate['design_id'].'<br>'.$project['open_date'].'<br>'.$estimate['required_date'].'<br>'.$estimate['revised_date'].'</strong>
				</td>
				</tr>
				</table>
			</td>
		</tr>
	</table>
	</div><br>';

# footer
$footer_1 = '<div class = "footer">Page {PAGENO}</div>';

# test only html output
//print $pageHTML;

# mpdf class (pdf output)
include("mpdf60/mpdf.php");
$mpdf = new mPDF('', 'LETTER', 0, 'Futura-Book', 10, 10, 10, 10, 10);
$mpdf->setAutoTopMargin = 'stretch';
$mpdf->setAutoBottomMargin = 'stretch';
$mpdf->DefHTMLHeaderByName('header_1',$header_1);
$mpdf->SetHTMLHeaderByName('header_1');
$mpdf->DefHTMLFooterByName('footer_1',$footer_1);
$mpdf->SetHTMLFooterByName('footer_1');
$mpdf->WriteHTML($pageHTML);
// download the pdf if phone or tablet
require_once('mobile_detect.php');
$detect = new Mobile_Detect;
// Any mobile device (phones or tablets).
if( ($detect->isMobile()) || ($detect->isTablet()) ) 
{
	$pdfts = strtotime('now');
	$pdfname = 'mobile_pdf/'.$pdfts.'-estimate.pdf';

	// set to mysql table (chron job deletes these files nightly after they are 1 day old)
	$vujade->create_row('mobile_pdf');
	$pdf_row_id = $vujade->row_id;
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'file_name',$pdfname);
	$s[]=$vujade->update_row('mobile_pdf',$pdf_row_id,'unix_ts',strtotime('now'));
 	$mpdf->Output($pdfname,'F');
 	print '<h1><a href = "'.$pdfname.'">Download PDF</a></h1>';
}
else
{
	$mpdf->Output('Estimate.pdf','I'); 
}
?>