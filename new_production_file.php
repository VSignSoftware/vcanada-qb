<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$files_permissions = $vujade->get_permission($_SESSION['user_id'],'Production Files');
if($files_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['project_id'];
$project_id = $id;

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$folder_id=$_REQUEST['folder_id'];

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
# 
if($action==1)
{

}

$pfs = $vujade->get_production_files($id);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$nav = 3;
$title = "Production Files - ";
?>
<?php require_once('tray_header.php'); ?><!-- Start: Content-Wrapper --><section id="content_wrapper"><!-- Begin: Content -->
	<header id="topbar">

		<div class="topbar-left">

			<ol class="breadcrumb">

				<li class="crumb-active">

					<a href="#">Project #: <?php print $project_id; ?> Project Name: <?php print $project['site']; ?></a>

				</li>

			</ol>

		</div>

	</header>
	<section id="content" class="/*table-layout*/ animated fadeIn">
		<!-- begin: .tray-center -->
		<div class="tray tray-center" style = "width:100%;">
			<div class="" style = "width:100%;">
				<?php //require_once('project_right_tray.php'); ?>

	<div id="projectDetailsContainer"  class="admin-form theme-primary">
		<div class="panel heading-border panel-primary">
		<div class="panel-body bg-light">
			<strong>Upload production files</strong>
			<div style = "padding:5px;width:100%;">
				<link rel="stylesheet" href="vendor/plugins/dropzone/css/dropzone.css">
				<script src="vendor/plugins/dropzone/dropzone.min.js"></script>

				<div style = "width:100%;margin-bottom:15px;height:375px;">
				<form action="upload.php?type=2&project_id=<?php print $project_id; ?>&folder_id=<?php print $folder_id; ?>" id="dropzone-area" class="dropzone"></form>
				</div>
			</div>

			<div style="margin-top:15px;">
				<div style="float:left">   </div>
				<div style = "float:right;">
					<a href = "project_production_files.php?id=<?php print $project_id; ?>" class = "button200 btn btn-lg btn-primary" style = "width:100px;">DONE</a>
				</div>

			</div>
	</div>
		</div>
		
</div>

<div class = "push">
</div>
</section></section><!-- BEGIN: PAGE SCRIPTS --><!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js">
</script><script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script><!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script><!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script><script src="assets/js/demo/demo.js">
</script><script src="assets/js/main.js"></script>
<script>
	
	Dropzone.options.dropzoneArea = 
	{ 
	  // The configuration we've talked about above
	  //autoProcessQueue: false,
	  //uploadMultiple: false,
	  //parallelUploads: 1,
	  //maxFiles: 100,
	  // url: "upload.php?project_id=<?php print $id; ?>&type=1&design_id=<?php print $design['design_id']; ?>",
	  init: function () 
	  {
	  		this.on("addedfile", function (file) 
	  		{
	        	var existing = [];
	        	// get existing files and add to array
	        	<?php
	        	if($pfs['error']=="0")
	        	{
	        		unset($pfs['error']);
	        		foreach($pfs as $df)
	        		{
	        			?>
	        			existing.push("<?php print $df['file_name']; ?>");
	        			<?php
	        		}
	        	}
	        	?>
	        	var in_array = existing.indexOf(file.name);
	    		if(in_array>=0)
	    		{
	    			if(confirm("A file with the same name " + file.name + " already exists for this design. Do you want to overwrite it?"))
	    			{
	    				this.processQueue();
	                	//alert('1');
	                	// user confirmed; do nothing further; upload.php handles overwriting
		            }
		            else
		            {
		            	this.removeFile(file);
	                	return false;
		            }
	    		}
	    		else
	    		{
	    			//alert('not in array: '+file.name);
	    			this.processQueue();
	    		}
	        });
	    }
	}

	$(document).ready(function()
	{
		Core.init();
	});

</script>
</body>
</html>