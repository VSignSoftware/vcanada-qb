<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
print '<meta charset="ISO-8859-1">';
$estimate_id = $_REQUEST['estimate_id'];
$estimate = $vujade->get_estimate($estimate_id);
if($estimate['error']!="0")
{
	$estimate['estimate_id']=$estimate_id;
}

$setup=$vujade->get_setup();

require_once('mobile_detect.php');
$detect = new Mobile_Detect;

$item_id = $_REQUEST['item_id'];
$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# add a new item to this estimate
if($action==1)
{
	# get item details
	$item = $vujade->get_item($item_id);
	if($item['error']=="0")
	{
		$s=array();
		$vujade->create_row('estimates_material');
		$row_id = $vujade->row_id;
		if(!empty($estimate['estimate_id']))
		{
			$s[]=$vujade->update_row('estimates_material',$row_id,'estimate_id',$estimate['estimate_id']);
		}
		else
		{
			$s[]=$vujade->update_row('estimates_material',$row_id,'estimate_id',$estimate['estimate_id_hash']);
		}
		$s[]=$vujade->update_row('estimates_material',$row_id,'inventory_id',$item['inventory_id']);
		$s[]=$vujade->update_row('estimates_material',$row_id,'cost',$item['cost']);
		$s[]=$vujade->update_row('estimates_material',$row_id,'description',$item['description']);
		$s[]=$vujade->update_row('estimates_material',$row_id,'qty',1);
	}
	$action = 0;
}

# update the item quantity (deprecated)
if($action==2)
{
	$db_id=$_POST['db_id'];
	$new_qty = $_POST['qty'];
	$s = $vujade->update_row('estimates_material',$db_id,'qty',$new_qty);
	$action = 0;
}

# delete the item quantity 
if($action==3)
{
	$db_id=$_POST['db_id'];
	$s = $vujade->delete_row('estimates_material',$db_id);
	$action = 0;
}

# default
# get the items that have been added to this estimate previously
if($action==0)
{
	?>
							
						<div class = "row little-small" style = "height:40px;background-color:#CADDF5;border:1px solid #5D9CEC;color:#2d6ca2;padding-top:10px;font-weight:bold;"
						>
							<div class="col-md-2 col-sm-2 col-xs-2">
								ID
							</div>
							<div class="col-md-3 col-sm-3 col-xs-3">
								Description
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								Cost
							</div>

							<!-- -->
							<div class="col-md-2 col-sm-2 col-xs-2 text-right">
								Meas
							</div>

							<div class="col-md-2 col-sm-2 col-xs-2">
								QTY
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								&nbsp;
							</div>
							<div class="col-md-1 col-sm-1 col-xs-1">
								&nbsp;
							</div>								
						</div>
	<?php
	$materials_cost = 0;
	$items = $vujade->get_materials_for_estimate($estimate_id);
	if($items['error']=="0")
	{
		unset($items['error']);
		print '<div style = "height:800px;overflow-y:auto; margin-top:3px;">';
		foreach($items as $i)
		{
			/*
			print '<div style = "width:485px;border-bottom:1px solid #cecece;clear:both;height:75px;">';
			print '<div style = "padding-left:10px;padding-top:5px;padding-bottom:5px;width:100px;float:left;">';
			print $i['inventory_id'];
			print '</div>';

			print '<div style = "padding-top:5px;padding-bottom:5px;width:150px;float:left;">';
			print $i['description'];
			print '</div>';

			print '<div style = "padding-top:5px;padding-bottom:5px;width:75px;float:left;">';
			print '$'. number_format($i['cost'],2,'.',',');
			print '</div>';

			print '<div style = "padding-top:5px;padding-bottom:5px;width:75px;float:left;">';
			print '<input type = "text" name = "item_qty_'.$i['database_id'].'" id = "item_qty_'.$i['database_id'].'" value = "'.$i['qty'].'" class = "quantity" style = "width:50px;">';
			print '</div>';

			$line = $i['cost']*$i['qty'];
			print '<div style = "padding-top:5px;padding-bottom:5px;float:left;">';
			print '$'.number_format($line,2,'.',',').' ';
			print '</div>';
			
			print '<div style = "padding-top:5px;padding-bottom:5px;padding-left:5px;">';
			print '<a style = "float:right;" href = "#" id = "'. $i['database_id'].'" class = "plus-delete btn btn-danger btn-xs">X</a>';
			print '<input type = "hidden" name = "item_id_'.$i['database_id'].'" id = "item_id_'.$i['database_id'].'">';
			print '</div>';
			print '</div>';
			$materials_cost = $materials_cost + $line;
			*/
			/* Old Code from Todd
			print '<div class="row" style = "border-bottom:1px solid #cecece;">';

			// id
			print '<div class="col-md-2 col-sm-2 col-xs-2">';
			print $i['inventory_id'];
			print '</div>';

			// description
			print '<div class="col-md-3 col-sm-3 col-xs-3">';
			print $i['description'];
			print '</div>';

			// item cost
			print '<div class="col-md-2 col-sm-2 col-xs-2">';
			print '$'. number_format($i['cost'],2,'.',',');
			print '</div>';

			// qty
			print '<div class="col-md-2 col-sm-2 col-xs-2">';
			print '<input type = "text" name = "item_qty_'.$i['database_id'].'" id = "item_qty_'.$i['database_id'].'" value = "'.$i['qty'].'" class = "quantity" style = "width:50px;">';
			print '</div>';

			// line cost
			$line = $i['cost']*$i['qty'];
			print '<div class="col-md-2 col-sm-2 col-xs-2">';
			print '$'.number_format($line,2,'.',',').' ';
			print '</div>';
			
			print '<div class="col-md-1 col-sm-1 col-xs-1">';
			print '<a style = "" href = "#" id = "'. $i['database_id'].'" class = "plus-delete btn btn-danger btn-xs">X</a>';
			print '<input type = "hidden" name = "item_id_'.$i['database_id'].'" id = "item_id_'.$i['database_id'].'">';
			print '</div>';
			print '</div>';
			$materials_cost = $materials_cost + $line;
			*/

			print '<div class="row little-small" style = "border-bottom:1px solid #cecece;">';

			// id
			print '<div class="col-md-2 col-sm-2 col-xs-2">';
			print $i['inventory_id'];
			print '</div>';

			// description
			print '<div class="col-md-3 col-sm-3 col-xs-3">';
			print $i['description'];
			print '</div>';

			// item cost
			print '<div class="col-md-1 col-sm-1 col-xs-1">';
			print '$'. number_format($i['cost'],2,'.',',');
			print '</div>';

			/*  */
			// unit of measure
			print '<div class="col-md-2 col-sm-2 col-xs-2 text-right">';
			if($setup['is_qb']==1)
			{
				$line_item = $vujade->get_item($i['inventory_id'],'Name');
			}
			else
			{
				$line_item = $vujade->get_item($i['inventory_id'],'inventory_id');
			}
			print $line_item['uom'];
			print '</div>';

			// qty
			print '<div class="col-md-1 col-sm-1 col-xs-1">';

			// qty width for mobile
			if( ($detect->isMobile()) || ($detect->isTablet()) ) 
			{
				$style_width = "20";
			}
			else
			{
				// desktop can be wider
				$style_width = "40";
			}

			print '<input type = "text" name = "item_qty_'.$i['database_id'].'" id = "item_qty_'.$i['database_id'].'" value = "'.$i['qty'].'" class = "quantity" style = "width: '.$style_width.'px; padding: 0px;">';
			print '</div>';

			// line cost
			$line = $i['cost']*$i['qty'];
			print '<div class="col-md-2 col-sm-2 col-xs-2 ">';
			print '$'.number_format($line,2,'.',',').' ';
			print '</div>';
			
			print '<div class="col-md-1 col-sm-1 col-xs-1 text-right cross-sign" style="padding-left: 10px;">';
			print '<a style = "" href = "#" id = "'. $i['database_id'].'" class = "plus-delete btn btn-danger btn-xs">X</a>';
			print '<input type = "hidden" name = "item_id_'.$i['database_id'].'" id = "item_id_'.$i['database_id'].'">';
			print '</div>';
			print '</div>';
			$materials_cost = $materials_cost + $line;
		}
		print '</div>';

		print '<div align = "right" style = "background-color:#FAFAFA;height:30px;padding-top:15px;">Materials Total: $'.number_format($materials_cost,2,'.',',');
		print '<a href = "#" class = "plus-update btn-primary btn-xs" style = "margin-left:5px;">Recalculate</a>';
		print '</div>';
	}
	else
	{
		print '<div class = "alert alert-warning">No materials have been added to this estimate yet.</div>';
	}
	?>	
	<?php
}
?>
<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
$(document).ready(function()
{

});
</script>