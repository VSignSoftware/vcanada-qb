<div class = "alert alert-micro <?php print $project['status_color']; ?>">
	<div class = "row">
		<div class = "col-md-6">
			<?php print $project['status']; ?>
		</div>
		<div class = "col-md-6">
			<?php 
			if(isset($shop_order))
			{
				// design
				if(!(empty($shop_order['design'])))
				{
					print 'Design #'.$shop_order['design'];
					print '<br>';
				}
				// estimates
				if(!(empty($shop_order['estimates'])))
				{
					print 'Estimate #';
					$soe = str_replace('^',',',$shop_order['estimates']);
					$soe = preg_replace("/,*$/", '', $soe);
					print $soe;
				}
			}
			?>
		</div>
	</div>
</div>

<div>
	<h1><?php print $project['site']; ?></h1>
</div>

<div class="panel">
  <div class="panel-heading">
    <span class="panel-title">
      <span class="glyphicon glyphicon-map-marker"></span>
		<?php
		print $project['address_1'];
		print ' | ';
		print $project['city'].', ';
		print $project['state'].' ';
		print $project['zip'];
		print '</strong>';
		?>
  	</span>

  	<?php
  	if($vendor_map_search)
  	{
  		print '<a class = "btn btn-primary btn-sm pull-right" href = "vendor_search.php?zip='.$project['zip'].'&project_id='.$project['project_id'].'" style = "margin-top:4px;">Nearby Vendors</a>';
  	}
  	?>

  </div>
  <?php
  if($menu==1)
  {
  	?>
      <div class="panel-body">
  		<div style = "float:left;width:150px">
		  <address>
		    	<strong>Project Contact:</strong><br>
		    	<?php 
				print $project['project_contact']; 
				?>
				<br>
				<?php print $project['project_contact_phone']; ?>
				<br>
				<?php print $project['project_contact_email']; ?>
		  </address>
		  <address>
		    <strong>Site Contact:</strong><br>
		    	<?php 
				print $project['site_contact']; 
				?>
				<br>
				<?php print $project['site_contact_phone']; ?>
				<br>
				<?php print $project['site_contact_email']; ?>
		  </address>
		</div>	
        <div id="map" style = "float:right;width:500px;height:250px;" class="map"></div>
      </div>
    <?php } ?>
</div>

<?php
  if($menu==1)
  {
  	?>
	<div class="well">
		<strong>Important Notes:</strong><br>
		<form method = "post" action = "project.php">
		<textarea class = "ta_notes form-control" id = "notes" name = "notes" rows = "2" style = "width:100%;">		
		<?php 
		print $project['notes'];
		?>
		</textarea>
		<br>
		<?php
		if($projects_permissions['read']==1)
		{
		?>
			<input type = "hidden" name = "id" value = "<?php print $id; ?>">
			<input type = "hidden" name = "action" value = "1">
			<input type = "submit" value = "Update Notes" class = "btn btn-primary btn-sm" style = "">
		<?php } ?>
		</form>
	</div>
<?php } ?>

<?php
  if($menu==7)
  {
  	?>
	<div class="well">
		<strong>Important Notes:</strong><br>
		<form method = "post" action = "project_job_status.php">
		<textarea class = "ta_notes form-control" id = "notes" name = "notes" rows = "2" style = "width:100%;">		
		<?php 
		print $project['notes'];
		?>
		</textarea>
		<br>
		<?php
		if($projects_permissions['read']==1)
		{
		?>
			<input type = "hidden" name = "id" value = "<?php print $id; ?>">
			<input type = "hidden" name = "action" value = "10">
			<input type = "submit" value = "Update Notes" class = "btn btn-primary btn-sm" style = "">
		<?php } ?>
		</form>
	</div>
<?php } ?>