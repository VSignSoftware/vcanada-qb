<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$so_permissions = $vujade->get_permission($_SESSION['user_id'],'Shop Orders');
if($so_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$tifb_permissions = $vujade->get_permission($_SESSION['user_id'],'Turned in for billing');

$setup = $vujade->get_setup();

$id = $_REQUEST['id'];
$s = array();
$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$shop_order = $vujade->get_shop_order($id, 'project_id');

// test if salesperson has been assigned
// if has, get sales person employee data
$semployee = $vujade->get_employee_by_name($project['salesperson']);

# determine if shop order exists
if($shop_order['error']!="0")
{
	unset($shop_order);
	# does not exist, create
	$vujade->create_row('shop_orders');
	$shop_order=$vujade->get_shop_order($vujade->row_id);
	$s[]=$vujade->update_row('shop_orders',$vujade->row_id,'project_id',$id);

	$customer = $vujade->get_customer($project['client_id']);
	if($customer['error']=="0")
	{
		// tax rate
		if($setup['is_qb']==1)
		{
			//$tdata = $vujade->QB_get_sales_tax($customer['city'].', '.$customer['state']);
			
			// check for group sales tax
			$td = $vujade->get_tax_groups($project['city'].', '.$project['state']);
			if($td['count']==1)
			{
				$tdata['rate'] = $td['rate'];
			}
			else
			{
				// does not have group sales tax
				$tdata = $vujade->QB_get_sales_tax($project['city'].', '.$project['state']);
			}
		}
		else
		{
			$tdata = $vujade->get_tax_for_city($customer['city'],$customer['state']);
		}
	}
	$s[]=$vujade->update_row('shop_orders',$vujade->row_id,'tax_rate',$tdata['rate']);
}

$js = $vujade->get_job_status($id);
if($js['error']!="0")
{
	$vujade->create_row('job_status');
	$row_id = $vujade->row_id;
	$s[]=$vujade->update_row('job_status',$row_id,'project_id',$id);
	$js = $vujade->get_job_status($id);
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# change order button was pressed
if($action==1)
{
	
	// erase all dates in the job status table
	$s[]=$vujade->delete_row('job_status',$id,1,'project_id');

	// erase all temporary dates
	$s[]=$vujade->delete_row('js_temp',$id,1,'project_id');

	// create a new job status record
	$js = $vujade->get_job_status($id);
	if($js['error']!="0")
	{
		$vujade->create_row('job_status');
		$row_id = $vujade->row_id;
		$s[]=$vujade->update_row('job_status',$row_id,'project_id',$id);
		$js = $vujade->get_job_status($id);
	}
	
	$shop_order = $vujade->get_shop_order($id, 'project_id');
	$project = $vujade->get_project($id,2);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Pending');
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',1);

	// update the shop order 
	$shop_order_id = $shop_order['database_id'];
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'type','');
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'due_date','');
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'or_week_of','');
	
	// remove previous selections - design and estimates
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'design','');
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'estimates','');

	# send task to the project manager
	if(!empty($project['project_manager']))
	{
		$emp1 = $vujade->get_employee($_SESSION['user_id']);
		$pm = $vujade->get_employee_by_name($project['project_manager']);
		if($pm['error']=="0")
		{
			$vujade->create_row('tasks');
			$taskid = $vujade->row_id;
			$s=array();
			$s[]=$vujade->update_row('tasks',$taskid,'project_id',$project['project_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'employee_id',$pm['employee_id']);
			$s[]=$vujade->update_row('tasks',$taskid,'task_due_date',date('m/d/Y'));
			$s[]=$vujade->update_row('tasks',$taskid,'task_message','Job was changed to Pending');
			$s[]=$vujade->update_row('tasks',$taskid,'task_subject','Job was changed to Pending');
			$s[]=$vujade->update_row('tasks',$taskid,'created_by',$emp1['employee_id']);
			$ts = strtotime('now');
			$s[]=$vujade->update_row('tasks',$taskid,'ts',$ts);

			// user task row
			$vujade->create_row('user_tasks');
			$nid = $vujade->row_id;
			$s[]=$vujade->update_row('user_tasks',$nid,'task_id',$taskid);
			$s[]=$vujade->update_row('user_tasks',$nid,'user_id',$pm['employee_id']);
			$s[]=$vujade->update_row('user_tasks',$nid,'project_id',$project['project_id']);
		}
	}
	$project = $vujade->get_project($id,2);
	$shop_order = $vujade->get_shop_order($id, 'project_id');
	$status="Pending";

	// remove from the calendar
	$s[]=$vujade->delete_row('calendar',$id,1,'project_id');

}

# turn in for billing button was pressed
if($action==2)
{
	$js = $vujade->get_job_status($id);
	if($js['error']=="0")
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'turned_in_for_billing',1);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'process_order',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'is_closed',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'did_not_sell',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date_ts','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_end_date','');
	}
	$shop_order = $vujade->get_shop_order($id, 'project_id');

	// update the shop order 
	$shop_order_id = $shop_order['database_id'];
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'type','');
	$project = $vujade->get_project($id,2);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Turned in for billing');
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',14);
	$project = $vujade->get_project($id,2);
	$status="Turned in for billing";

	// remove from the calendar
	$s[]=$vujade->delete_row('calendar',$id,0,'project_id');

}

# did not sell button was pressed
if($action==3)
{
	$js = $vujade->get_job_status($id);
	if($js['error']=="0")
	{
		$s[]=$vujade->update_row('job_status',$js['database_id'],'turned_in_for_billing',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'process_order',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'is_closed',0);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'did_not_sell',1);
		$s[]=$vujade->update_row('job_status',$js['database_id'],'pending_alt','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'service_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_date_ts','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_done','');
		$s[]=$vujade->update_row('job_status',$js['database_id'],'installation_end_date','');
	}
	$shop_order = $vujade->get_shop_order($id, 'project_id');

	// update the shop order 
	$shop_order_id = $shop_order['database_id'];
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'type','');
	$project = $vujade->get_project($id,2);
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status','Did not sell');
	$s[]=$vujade->update_row('projects',$project['database_id'],'project_status_number',15);

	// remove previous selections - design and estimates
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'design','');
	$s[]=$vujade->update_row('shop_orders',$shop_order_id,'estimates','');
	
	$project = $vujade->get_project($id,2);
	$shop_order = $vujade->get_shop_order($id, 'project_id');
	$status="Did not sell";

	// remove from the calendar
	$s[]=$vujade->delete_row('calendar',$id,0,'project_id');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;
$menu=6;
$title = $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left 
        <aside class="tray tray-left tray320 p20">
		-->
          <?php require_once('project_left_tray.php'); ?>

        <!-- end: .tray-left </aside>-->

        <!-- begin: .tray-center -->
        <div class="tray tray-center" style = "width:100%;border:0px solid red;">

            <div class="pl15 pr15" style = "width:100%;">

            	<?php require_once('project_right_tray.php'); ?>
            	
            	<div class="panel panel-primary panel-border top">
				  <div class="panel-heading">
				  	<?php
					if($so_permissions['edit']==1)
					{
						if($js['is_closed']!=1)
						{
						?>

							<a href = "project_shop_order_edit.php?project_id=<?php print $id; ?>" class = "btn btn-xs btn-success">Process Order</a> 
						<?php } ?>
						<a href = "#co_modal" class = "btn btn-xs btn-primary" id = "change_order">Change Order</a> 

					<?php
					}
					if($tifb_permissions['read']==1)
					{
					?>
						<a href = "#tifb_modal" class = "btn btn-xs btn-warning" id = "turn_in_for_billing">Turn in for billing</a>
					<?php
					}
					if($so_permissions['edit']==1)
					{
					?>
						<a href = "#dns_modal" class = "btn btn-xs btn-primary" id = "did_not_sell">Did Not Sell</a>
					<?php
					}
					?>

				    <div class="widget-menu pull-right">
				      <a href = "print_shop_order.php?project_id=<?php print $id; ?>" target = "_blank" class = "btn btn-xs btn-primary">Print Shop Copy</a>
				    </div>
				  </div>
				  <div class="panel-body">
					    <table class = "table">
						<tr>
							<td>Type</td>
							<td><b><?php print $shop_order['type']; ?></b></td>
							<td>Design</td>
							<td><b><?php print $shop_order['design']; ?></b></td>
							<td>Due Date</td>
							<td><b>
								<?php 
								if($shop_order['or_week_of']=="Firm Date")
								{
									print $shop_order['due_date'];
								}
								?></b>
							</td>
						</tr>

						<tr>
							<td>Date Opened</td>
							<td><b><?php print $shop_order['date_opened']; ?></b></td>
							<td>Proposal</td>
							<td><b><?php print $shop_order['proposal']; ?></b></td>
							<td>Or Week Of</td>
							<td><b>
								<?php
								if($shop_order['or_week_of']=="Or Week Of")
								{
									print $shop_order['due_date'];
								}
								?></b>
							</td>
						</tr>

						<tr>
							<td>Date Revised</td>
							<td><b><?php print $shop_order['date_revised']; ?></b></td>
							<td>Purchase Order #</td>
							<td><b><?php print $shop_order['po_number']; ?></b></td>
							<td><!--Invoice Date--></td>
							<td><?php //print $shop_order['invoice_date']; ?></td>
						</tr>

						<tr>
							<td colspan = "1" valign = "top">Estimates</td>
							<td colspan = "5"><b>
								<?php
								$estimates = str_replace('^',' ',$shop_order['estimates']);
								print $estimates;
								?></b>
							</td>
						</tr>
					</table>
				
					<div class = "well">
						<div style = "width:800px;overflow-x:auto;">
						
							<?php
							$items = $vujade->get_items_for_proposal($id);
							if($items['error']=='0')
							{
								print '<table width = "100%">';
								$tax_total=0;
								$tax_line=0;
								$subtotal = 0;
								foreach($items as $item)
								{

									if(!empty($item['tax_label_rate']))
			                     	{
			                     		$tax_line=$item['amount']*$item['tax_label_rate']*$firstproposal['tax_rate'];
			                     	}
			                     	$tax_total+=$tax_line;
			                     	$tax_line=0;

									$subtotal+=$item['amount'];
									print '<tr>';
									print '<td style = "word-wrap:normal;width:85%;overflow-x: auto;">';
									//$item['item'] = str_replace("&nbsp;", '&nbsp; ', $item['item']);
									print $item['item'];
									print '</td>';
									print '<td><span class = "pull-right">';
									if(!empty($item['amount']))
									{
										print '$'.@number_format($item['amount'],2,'.',',');
									}
									print '</span></td>';
									print '</tr>';
								}
								print '</table>';
							}
							else
							{
								// check if old description is in database (ie project is legacy)
								if(!empty($shop_order['description']))
								{
									print $shop_order['description'];
								}
							}
							?>

						</div>
					</div>

					<div class = "row">
						<div class = "col-md-10" style = "text-align:right;">
							<strong>Selling Price</strong> <em>(excluding tax and permits):</em>
						</div>
						<div class = "col-md-2">
							$<?php print @number_format($shop_order['selling_price'],2,'.',','); ?>
						</div>
					</div>

					<div class = "row">
						<div class = "col-md-10" style = "text-align:right;">
							<?php
							if($setup['country']!="Canada")
							{
								print '<strong>Tax Rate: </strong>';
								print $shop_order['tax_rate'].' '; 
							}
							?>
							<strong>Tax: </strong>
						</div>
						<div class = "col-md-2">
							<?php
							print '$'.@number_format($shop_order['tax_amount'],2,'.',','); 
							?>
						</div>
					</div>

					<?php
					$tot = $shop_order['tax_amount']+$shop_order['selling_price'];
					?>

					<div class = "row">
						<div class = "col-md-10" style = "text-align:right;">
						</div>
						<div class = "col-md-2">
							<h4>
								$<?php print @number_format($tot,2,'.',',');?>
							</h4>
						</div>
					</div>
				</div>
            </div>
      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

<!-- modal for change order -->
<div id="co_modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1></h1>
<p>Are you sure you want to change this job?</p>
<p><a id = "" class="btn btn-lg btn-danger" href="project_shop_orders.php?id=<?php print $id; ?>&action=1">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- modal for turn in for billing -->
<div id="tifb_modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1></h1>
<p>Are you sure you want to turn this job in for billing?</p>
<p><a id = "" class="btn btn-lg btn-danger" href="project_shop_orders.php?id=<?php print $id; ?>&action=2">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

<!-- modal for did not sell -->
<div id="dns_modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
<h1></h1>
<p>Are you sure you want to change this job to Did Not Sell?</p>
<p><a id = "" class="btn btn-lg btn-danger" href="project_shop_orders.php?id=<?php print $id; ?>&action=3">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
</div>

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- modal -->
  <script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  var map;
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    // modals

    // change order
    $('#change_order').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#co_modal',
		modal: true
	});

    // turn in for billing
    $('#turn_in_for_billing').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#tifb_modal',
		modal: true
	});

    // did not sell
    $('#did_not_sell').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#dns_modal',
		modal: true
	});

    // modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    /* fixes indent on textareas
    var n = $('#notes').html();
    $("#notes").html($.trim(n));
   	*/
		
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
