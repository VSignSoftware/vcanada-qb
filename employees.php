<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

if($accounting_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['delete']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$er_permissions = $vujade->get_permission($_SESSION['user_id'],'Employee Records');
if($er_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

/*
Show on the main page of employees how many users are website users and how many are left. ex . "Access used 22/25" . This will pull number from site setup, and then count the number yes's from the employee information page
*/
$setup = $vujade->get_setup();
$max_users = $setup['max_site_users'];
$active_users = $vujade->get_employees(3);
$au_cnt = 0;
if($active_users['error']=="0")
{
	unset($active_users['error']);
	foreach($active_users as $au)
	{
		if( ($au['is_admin']!=1) && ($au['access']==1) && ($au['website_user']==1) )
		{
			$au_cnt++;
		}
	}
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = 'Employees - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Employees</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
        	if($_REQUEST['m']==1)
        	{
        		$vujade->errors[]="Employee Deleted";
        	}
        	$vujade->messages[]="Access used ".$au_cnt."/".$max_users;
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
	        	<div class="panel-body bg-light">
	        		<?php
	        		//if($au_cnt<$max_users)
	        		//{
	        			print '<div><a id = "new-employee" href = "#new-employee-modal" class = "btn btn-primary">New</a>';
	        			?>

	        			<!-- new employee modal -->
	        			<div id="new-employee-modal" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
							<h1>New Employee</h1>
							
							<div style = "display:none;" class = "alert alert-danger" id = "errors">
							</div>

							<i>All fields are required</i>
							<br>
							<form id = "new-employee-form" method = "post" action = "employee.php">
								<input type = "hidden" name = "action" value = "11">
								<strong>First Name</strong><br>
								<input type = "text" name = "first_name" id = "first_name" class = "form-control">
								<br>
								<strong>Last Name</strong><br>
								<input type = "text" name = "last_name" id = "last_name" class = "form-control">
								<br>
								<strong>Username</strong><br>
								<input type = "text" name = "u" id = "u" class = "form-control">
								<br>
								<strong>Password</strong><br>
								<input type = "text" name = "p" id = "p" class = "form-control">
								<br>
								<p><i>Pressing the save button will check to make sure the username you entered doesn't already exist for another user. If the username is unique, the system will create the new user for you and configure the user to have basic access to the site. You will need to complete the user details -- including permissions -- on the next page.</i></p>

								<p><a id = "save-employee" class="btn btn-lg btn-success" href = "#">Save</a> <a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">Cancel</a></p>
							</form>
						</div>

	        			<?php
	        		//}
					$active = 1;
					if(isset($_REQUEST['active']))
					{
						$active = $_REQUEST['active'];
					}
					if($active == 1)
					{
						$employees = $vujade->get_employees(3);
					}
					else
					{
						$employees = $vujade->get_employees();
					}
					if($employees['error']==0)
					{
						unset($employees['error']);
						?>
						<?php
						if($active==1)
						{
							print '<a href = "employees.php?active=2" class = "btn btn-primary pull-right">Show All</a>';
						}
						else
						{
							print '<a href = "employees.php?active=1" class = "btn btn-primary pull-right">Show Active Only</a>';
						}
						?>
						</div>

						<table id="datatable" class="employees-table table table-striped table-hover" cellspacing="0" width="100%">
						<thead>
							<tr style = "border-bottom:1px solid black;">
								<td width = "15%" valign = "top"><strong>Employee #</strong></td>
								<td width = "85%" valign = "top">
									<strong>Name</strong>
								</td>
							</tr>
						</thead>

					    <tbody style = "font-size:14px;">
						<?php
						foreach($employees as $employee)
						{
							if($employee['is_admin']!=1)
							{

								$link = 'employee.php?id='.$employee['database_id'];
						        print '<tr class="clickableRow" href="'.$link.'">';
						        print '<td valign = "top" style = ""><a href = "'.$link.'">'.$employee['employee_id'].'</a></td>';
						        print '<td valign = "top" style = ""><a href = "'.$link.'">'.$employee['fullname'].'</a></td>';

						        //print '<td valign = "top" style = "">website user: '.$employee['website_user'].'</td>';
								print '</tr>';
							}
						}
						print '</tbody></table>';
					}
					else
					{
						$vujade->set_error($employees['error']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables -->
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- modals -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // modal: add new employee
    $('#new-employee').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#new-employee-modal',
		modal: true
	});

	// modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

    // save employee button click
    $('#save-employee').click(function(e)
    {
    	e.preventDefault();

    	$('#errors').html('');
    	$('#errors').hide();

    	// basic validation	
    	var errors = 0;
    	var error_text = '';
    	var first_name = $('#first_name').val();
    	var last_name = $('#last_name').val();
    	var u = $('#u').val();
    	var p = $('#p').val();

    	if(first_name=='')
    	{
    		errors++;
    		error_text='First Name is required.<br>';
    	}
    	if(last_name=='')
    	{
    		errors++;
    		error_text=error_text+'Last Name is required.<br>';
    	}
    	if(u=='')
    	{
    		errors++;
    		error_text=error_text+'Username is required.<br>';
    	}
    	if(p=='')
    	{
    		errors++;
    		error_text=error_text+'Password is required.<br>';
    	}

    	if(errors==0)
    	{
    		$.post("jq.check_username.php", { u: u})
			.done(function(r) 
			{
				//alert(r);
				if(r==1)
				{
					// success
					// submit form 
    				$('#new-employee-form').submit();
				}
				else
				{
					error_text='Userame already exists for another user. Username must be unique.<br>';
					$('#errors').html(error_text);
    				$('#errors').show();
				}
			});
    	}
    	else
    	{
    		$('#errors').html(error_text);
    		$('#errors').show();
    	}
    });

	// Init DataTables
    $('#datatable').dataTable({

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 50,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });

    /**/
    $(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    }); 
});
</script>

</body>
</html>