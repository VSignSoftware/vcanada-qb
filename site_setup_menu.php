<style>
.hidden_menu
{
	display:none;
}
</style>

<a class="glyphicons glyphicons-left_arrow" href="accounting.php" id="back" style="margin-bottom:10px;"></a>

<a id = "estimates-link" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Estimates</a>

<div class = "well ss-menu <?php if($ss_menu!=1){ print 'hidden_menu'; } ?>" id = "estimates-link-menu">
	<a href="labor_setup.php">Labor departments + labor dollar amount</a><br>
	<a href="machine_setup.php">Machine Setup</a><br>
	<a href="overhead.php">Overhead</a><br>
	<a href="commission_setup.php">Commission setup</a>
	<a href="indeterminate.php">Indeterminate</a>
</div>

<a href = "#" id = "proposals" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Proposals</a>

<div class = "well ss-menu <?php if($ss_menu!=2){ print 'hidden_menu'; } ?>" id = "proposals-menu">
	<a href="proposal_setup_logo.php">Logo on pdf</a><br>
	<a href="terms.php?id=1">Terms</a>
</div>

<a id = "po" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Purchase Orders</a>

<div class = "well ss-menu <?php if($ss_menu!=3){ print 'hidden_menu'; } ?>" id = "po-menu">
	<a href="po_company_setup.php">Company Address</a><br>
	<a href="terms.php?id=2">Material terms</a><br>
	<a href="terms.php?id=5">Outsource terms</a><br>
	<a href="terms.php?id=6">Subcontractor terms</a>
</div>

<a id = "documents" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Documents</a>

<div class = "well ss-menu <?php if($ss_menu!=4){ print 'hidden_menu'; } ?>" id = "documents-menu">
	<a href="document_type_setup.php">Pull down menu</a>
</div>

<a id = "sf" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Site Photos</a>

<div class = "well ss-menu <?php if($ss_menu!=5){ print 'hidden_menu'; } ?>" id = "sf-menu">
	<a href="site_photos_setup.php">Setup</a>
</div>

<a id = "invoices" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Invoices</a>

<div class = "well ss-menu <?php if($ss_menu!=6){ print 'hidden_menu'; } ?>" id = "invoices-menu">
	<a href="invoice_setup.php">Invoice Setup</a><br>
	<a href="invoice_setup_logo.php">Invoice / Shop Order Logo</a><br>
	<a href="invoice_company_setup.php">Address and License #</a><br>
	<a href="terms.php?id=3">Terms</a><br>
	<a href="advanced_deposit_setup.php">Advanced Deposit Percentage</a>
</div>

<a id = "ld" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Logo</a>

<div class = "well ss-menu <?php if($ss_menu!=7){ print 'hidden_menu'; } ?>" id = "ld-menu">
	<a href="cms.php?f=1">Site Logo</a>
	<!--
	<br>
	<a href="cms.php?f=2">Dashboard Photo 1</a>
	<br>
	<a href="cms.php?f=3">Dashboard Photo 2</a>
	<br>
	<a href="cms.php?f=4">Dashboard Photo 3</a>
	<br>
	<a href="cms.php?f=5">Dashboard Photo 4</a>
	<br>
	<a href="cms.php?f=6">Dashboard Photo 5</a>
	-->
</div>

<?php
if($setup['is_qb']==1)
{
?>

<a id = "qb" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">QuickBooks Settings</a>

<div class = "well ss-menu <?php if($ss_menu!=13){ print 'hidden_menu'; } ?>" id = "qb-menu">
	<a href="qb_settings.php">QuickBooks Settings</a>
</div>
<?php } ?>

<!--
<a id = "st" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Sales Tax</a>

<div class = "well ss-menu <?php //if($ss_menu!=8){ print 'hidden_menu'; } ?>" id = "st-menu">
	<a href="state_sales_tax.php">State Sales Tax</a>
</div>
-->

<a id = "cal" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Calendar</a>

<div class = "well ss-menu <?php if($ss_menu!=9){ print 'hidden_menu'; } ?>" id = "cal-menu">
	<a href="calendar_legend.php">Legend</a>
</div>

<a id = "cm" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Custom Messages</a>

<div class = "well ss-menu <?php if($ss_menu!=10){ print 'hidden_menu'; } ?>" id = "cm-menu">
	<a href="custom_messages.php?id=5">Design Email Message</a>
	<br>
	<a href="custom_messages.php?id=4">Proposal Message</a>
	<br>
	<a href="custom_messages.php?id=2">Proposal Email Message</a>
	<br>
	<a href="custom_messages.php?id=3">Purchase Order Email Message</a>
	<br>
	<a href="custom_messages.php?id=1">Invoice Email Message</a>
</div>

<a id = "fy" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:160px;margin-bottom:5px;">Company Information</a>

<div class = "well ss-menu <?php if($ss_menu!=11){ print 'hidden_menu'; } ?>" id = "fy-menu">
	<a href="fiscal_year.php">Edit Fiscal Year</a>
	<br>
	<a href="on_budget.php">On Budget Number</a>
	<br>
	<a href="groups.php">Group Tasks</a>
	<br>
</div>

<?php
if($_SESSION['username']=="Administrator") 
{
	?>

	<a id = "am" href = "#" class="btn-link btn btn-primary btn-sm" style = "width:158px;margin-bottom:5px;">Administrator Settings</a>

	<div class = "well ss-menu <?php if($ss_menu!=12){ print 'hidden_menu'; } ?>" id = "am-menu">
		<a href="admin.wsu.php">Web Site Users</a>
		<br>
		<!--
			<a href="admin.nisst.php">Non-illuminated sign sales tax</a>
			<br>
		-->
	</div>

<?php } ?>