<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');

if($_REQUEST['id']==1)
{
	$pdf = "blank_ca_form.php";
	$iframe = '<iframe class="frame" src="blank_ca_form.php?dt=I"></iframe>';
}
if($_REQUEST['id']==2)
{
	$pdf = "sc_form.php";
	$iframe = '<iframe class="frame" src="sc_form.php?dt=I"></iframe>';
}
if($_REQUEST['id']==3)
{
	$pdf = "tc_form.php";
	$iframe = '<iframe class="frame" src="tc_form.php?dt=I"></iframe>';
}
if($_REQUEST['id']==4)
{
	$pdf = "survey_form.php";
	$iframe = '<iframe class="frame" src="survey_form.php?dt=I"></iframe>';
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=8;
$title = "Forms - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Begin: Content -->
      <section id="content" class="table-layout animated fadeIn">

        <!-- begin: .tray-left -->
        <aside class="tray /*tray-left*/ tray100 p20" id = "left_tray" style = "width:320px;">

        	<a id = "iframe12" class="btn-link btn btn-primary btn-lg" style = "width:250px;margin-bottom:15px;" href = "forms.php?id=1">Customer Acceptance Form</a>

        	<a id = "iframe22" class="btn-link btn btn-primary btn-lg" style = "width:250px;margin-bottom:15px;" href = "forms.php?id=2">Service Completion Form</a>

        	<a id = "iframe22" class="btn-link btn btn-primary btn-lg" style = "width:250px;margin-bottom:15px;" href = "forms.php?id=3">Time Cards</a>

        	<a id = "iframe22" class="btn-link btn btn-primary btn-lg" style = "width:250px;margin-bottom:15px;" href = "forms.php?id=4">Survey</a>

        </aside>
        <!-- end: .tray-left -->

        <!-- begin: .tray-center -->
        <div class="tray tray-center">

            <div class="pl20 pr50">

            	<style>
	            	.wrap 
	            	{
					    width: 700px;
					    height: 950px;
					    padding: 0;
					    overflow: hidden;
					}
	            	.frame 
	            	{
	    				width: 1280px;
					    height: 1700px;
					    border: 0;
					    -ms-transform: scale(0.75);
					    -moz-transform: scale(0.75);
					    -o-transform: scale(0.75);
					    -webkit-transform: scale(0.75);
					    transform: scale(0.75);
					    
					    -ms-transform-origin: 0 0;
					    -moz-transform-origin: 0 0;
					    -o-transform-origin: 0 0;
					    -webkit-transform-origin: 0 0;
					    transform-origin: 0 0;
					}
            	</style>

            	<!--
            	<div id = "iframe" style = "width:800;height:800px;">
            		<div class = "frame" id = "frame">
            		</div>
            	</div>
				-->

				<?php 
				if($pdf!='')
				{
					?>	
					<div id = "print" style = "text-align:right;margin-right:59px;margin-bottom:5px;">
						<!--<a href = "<?php //print $pdf; ?>" target="_blank" class = "btn btn-primary">Print</a>-->
	            	</div>

					<div class="wrap">
					    <?php
					    print $iframe;
					    ?>
					</div>
				<?php } ?>

            	<!-- main content for this page 
				<div class = "well">
					
				</div>
				-->

            </div>

      </section>
      <!-- End: Content -->

    </section>

  </div>
  <!-- End: Main -->

  <!-- BEGIN: PAGE SCRIPTS -->

  <!-- jQuery -->
  <script src="vendor/jquery/jquery-1.11.1.min.js"></script>
  <script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

  <!-- Theme Javascript -->
  <script src="assets/js/utility/utility.js"></script>
  <script src="assets/js/demo/demo.js"></script>
  <script src="assets/js/main.js"></script>
  <script type="text/javascript">
  jQuery(document).ready(function() 
  {

    "use strict";

    // Init Theme Core    
    Core.init();

    /*
    $('#iframe1').click(function(e)
    {
    	e.preventDefault();
    	$('#frame').html('');
    	// load pdf into div
    	$.post( "jq.get_form.php", { id: 1})
		.done(function(pdf) 
		{
			$('#frame').html(pdf);
			var btn = '<a href = "blank_ca_form.php" target="_blank" class = "btn btn-primary">Print</a>';
			$('#print').html('');
			$('#print').show();
			$('#print').html(btn);
		});
    });

    $('#iframe2').click(function(e)
    {
    	e.preventDefault();
    	$('#frame').html('');
    	// load pdf into div
    	$.post( "jq.get_form.php", { id: 2})
		.done(function(pdf) 
		{
			$('#frame').html(pdf);
			var btn = '<a href = "sc_form.php" target="_blank" class = "btn btn-primary">Print</a>';
			$('#print').html('');
			$('#print').show();
			$('#print').html(btn);
		});
    });
	*/
	
  });
  </script>
  <!-- END: PAGE SCRIPTS -->

</body>

</html>
