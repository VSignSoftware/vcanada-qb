<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
$es_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Sort');
if($es_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
$s = array();

// clear out any existing sorts not = 80000000
$q = "UPDATE `estimates` SET `sort` = 80000000 WHERE `sort` != 80000000";
$s[]=$vujade->generic_query($q,1,2);

// get updated sort
$sorted = $_POST['sorted'];

// looks like this: 
// 0^2_11,1^1_4,2^3_12,3^4_17,4^5_19,5^6_23,6^7_25,7^8_30,8^9_44,9^10_45,10^11_52
// 0 = position
// 1 = ?
// ^ = 
// 4 = design id
// , = indicates new element

// clean up and set to array
// clean up 
$sorted = explode(',',$sorted);
//print '<br>';
// loop and update
foreach($sorted as $sort)
{
	// sort now looks like: 0^1_4
	//print $sort.'<br>';
	$pieces = explode('^',$sort); 		
	$new_sort = $pieces[0];
	//print 'new sort: '.$new_sort.'<br>';
	$element_and_designid = $pieces[1]; 	// 1_4
	$pieces2 = explode('_',$element_and_designid);
	$id = $pieces2[1];
	//print 'id: '.$id.'<hr>';
	$s[]=$vujade->update_row('estimates',$id,'sort',$new_sort);
}

// output
print '<div class = "alert alert-success">Estimates have been sorted.</div>';
?>