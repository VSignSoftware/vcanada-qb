<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// user must be admin to access this page
/*
$_SESSION['user_id'] = the user's id when they login; can be changed if admin
$_SESSION['admin_id'] = user's id; cannot be altered; can only be set if admin
$employee = $vujade->get_employee($_SESSION['user_id']);
*/

if(!isset($_SESSION['admin_id']))
{
	$test_uid = $_SESSION['user_id'];
	$test_emp = $vujade->get_employee($test_uid);
	if($test_emp['is_admin']!=1)
	{
		$vujade->page_redirect('logout.php');
	}
	else
	{
		$_SESSION['admin_id']=$test_uid;
		$emp=$test_emp;
	}
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}
// use site as ...
if($action==1)
{
	$employee_id = $_REQUEST['employee_id'];
	$test_emp = $vujade->get_employee($employee_id);
	if($test_emp['error']!=0)
	{
		die('Invalid employee');
	}
	else
	{
		$_SESSION['user_id']=$employee_id;
		$emp=$test_emp;
	}
}
// switch back to admin ...
if($action==2)
{
	$_SESSION['user_id']=$_SESSION['admin_id'];
	$test_emp = $vujade->get_employee($_SESSION['admin_id']);
	$emp=$test_emp;
}
$section=0;
$title = "Administrator Settings - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">My Account</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

			<?php
			// output session variables
			print '<div class = "alert alert-warning">Session Data:<br>';
			print_r($_SESSION);
			print '</div>';

			// show link to switch back to admin user
			if( (isset($_SESSION['admin_id'])) && ($_SESSION['admin_id']!=$_SESSION['user_id']) )
			{
				print '<p><a href = "admin.php?action=2" class = "btn btn-xs btn-success">Switch Back to Admininstrator</a></p>';
			}

			// errors / messages
			$vujade->show_messages();
			$vujade->show_errors();
			print '<hr>';

			// file manager
			if( ($_SESSION['username']=='Administrator') || ($_SESSION['username']=='vinit') )
			{
				print '<a href = "file_manager.php" class = "btn">File Manager</a><br>';
			}

			// deploy and access logs
			if($_SESSION['username']=='Administrator')
			{
				print '<a href = "deploy.php" class = "btn">Deployments</a><br>';
				print '<a href = "access_logs.php" class = "btn">Clear Access Logs</a><br>';
				print '<a href = "system_update.php" class = "btn">System Update</a><br>';
				print '<hr>';

				// list of employees
				$employees = $vujade->get_employees();
				if($employees['error']=="0")
				{
					unset($employees['error']);
					print '<strong>Use the site as: </strong>';
				?>
					<div class="panel heading-border panel-primary">
	                	<div class="panel-body bg-light">

	                		<div class = "row">
								<table id="datatable" class="employees-table table table-striped table-hover" cellspacing="0" width="100%">
									<thead>
										<tr style = "border-bottom:1px solid black;">
											<td width = "15%" valign = "top"><strong>Employee #</strong></td>
											<td width = "85%" valign = "top">
												<strong>Name</strong>
											</td>
										</tr>
									</thead>

								    <tbody style = "font-size:14px;">
									<?php
									foreach($employees as $employee)
									{
										$link = 'admin.php?action=1&employee_id='.$employee['database_id'];
								        print '<tr class="clickableRow" href="'.$link.'">';
								        print '<td valign = "top" style = ""><a href = "'.$link.'">'.$employee['employee_id'].'</a></td>';
								        print '<td valign = "top" style = ""><a href = "'.$link.'">'.$employee['fullname'].'</a></td>';
										print '</tr>';
									}
									print '</tbody></table>';
						}
						else
						{
							$vujade->set_error($employees['error']);
							$vujade->show_errors();
						}
						?>
							</div>
						</div>
					</div>	

			<?php } ?>
		  </div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

   	$(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    }); 
});
</script>

</body>

</html>