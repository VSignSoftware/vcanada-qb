<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');
//$id = explode('^',$_POST['id']);
//$vendor = $vujade->get_vendor($id[0]);
$id = $_POST['id'];
$vendor = $vujade->get_vendor($id);
if($vendor['error']=="0")
{
	print '<strong>';
	print $vendor['name'];
	print '</strong>';
	print '<br>';
	print $vendor['address_1'];
	if(!empty($vendor['address_2']))
	{
		print ', ' . $vendor['address_1'];
	}
	print '<br>';
	print $vendor['city'].', '.$vendor['state'].' '.$vendor['zip'];

	?>
	<br>
	<br>
	<strong>Vendor Contact: </strong><br>
	<select name = "vendor_contact_id" id = "vendor_contact_id" class = "form-control" style = "width:200px;">
		<?php
		if(!empty($id))
		{
			$vendor_contact = $vujade->get_vendor_contact($id);
			if($vendor_contact['error']=="0")
			{
				print '<option value = "'.$vendor_contact['database_id'].'">'.$vendor_contact['fullname'].'</option>';
			}
		}
		print '<option value = "">-Select-</option>';
		$contacts = $vujade->get_vendor_contacts($id);
		if($contacts['error']=="0")
		{
			unset($contacts['error']);
			foreach($contacts as $c)
			{
				print '<option value = "'.$c['database_id'].'">'.$c['fullname'].'</option>';
			}
		}
		?>
	</select>

	<?php
}
else
{
	print $vendor['error'];
}
?>