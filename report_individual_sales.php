<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

// set to pacific time zone
date_default_timezone_set('America/Los_Angeles');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$setup = $vujade->get_setup();

// sales person name
$sp = $_REQUEST['sp'];
// validation

// year
if(isset($_REQUEST['year']))
{
	$year = $_REQUEST['year'];
	// validate
}
else
{
	$year = date('Y');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=6;
$title = 'Individual Yearly Sales for '.$sp.' - ';
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Individual Yearly Sales for <?php print $sp.' - '.$year; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu">
						<a href = "report_monthly_sales.php" class = "btn btn-primary btn-sm" style = "width:100px;">Back</a>
						<select name = "year" id = "year" class = "pull-right form-control" style = "width:200px;">
							<option value = "">-Change Year-</option>
							<?php
							$x=2013;
							$ey = date('Y');
							while($x<=$ey)
							{ 
								print '<option value ="'.$x.'">'.$x.'</option>';
								$x++;
							}
							?>
						</select>
					</div>
				</div>
	        	<div class="panel-body bg-light">
	        		
	        		<?php 
	        		// fiscal year does not start on Jan. 1
					if($setup['fiscal_year_start']!=1)
					{	
						//$y=$year-2;
						//$y2=$year-1;
						$y=$year-1;
						$y2 = $year;
						$m=$setup['fiscal_year_start'];
						$m2=$setup['fiscal_year_end'];
						$start = strtotime($y.'-'.$m.'-01');
						$date1 = $y2.'-'.$m2; 
						$d = date_create_from_format('Y-m',$date1); 
						$last_day = date_format($d, 't');
						$end = strtotime($y2.'-'.$m2.'-'.$last_day);

						$projects = $vujade->get_individual_yearly_sales_report($start,$end,$sp);

						if($projects['error']=="0")
						{
							//unset($projects['error']);
						}

						//print $projects['error'];
						unset($projects['error']);

						$table.='<table class = "table table-bordered">';
						$js_data_points = '';
						while($start < $end)
						{

							$lmonth = date('m',$start);
							$lyear = date('Y',$start);
							$lstart = mktime(0, 0, 1, $lmonth, 1, $lyear);
							$lend   = mktime(23, 59, 00, $lmonth, date('t', $lmonth), $lyear);
						    $line_start = $start;
						    $line_end = $lend;

							foreach($projects as $p)
							{
								$date_opened = strtotime($p['open_date']);
								//print '<tr><td>line start: '.$line_start.'</td><td>line end: '.$line_end.'</td></tr>';
								//print '<tr><td>date opened: '.$date_opened.'</td><td></td></tr>';
								//print $date_opened.'<br>';
								//print $janstart.' - janend: '.$janend.'<br>';
								if(($date_opened>=$line_start) && ($date_opened<=$line_end))
								{
									$line = str_replace(',','',$p['line_total']);
									$line_total+=$line;
									$total+=$line;
								}
							}

							$table.='<tr>';
							$table.='<td>';
							$table.=date('F Y', $start);
							$table.='</td>';
							$table.='<td>$';
							$table.=@number_format($line_total,2); 
							$table.='</td>';
							$table.='</tr>';

							// javascript fix; line total can't be = ""
							if(empty($line_total))
							{
								$line_total=0;
							}
							$js_data_points.='{
						         x: new Date('.$lyear.','.($lmonth-1).', 1),
						         y: '.$line_total.',
						       },';

						    $start = strtotime("+1 month", $start);
						    $line_total=0;
						}

						// strip last comma from js data points
						rtrim($js_data_points, ",");

						$table.='<tr>
								<td>Total
								</td>
								<td>$'.@number_format($total,2).'
								</td>
							</tr></table>';

						?>

						<!-- chart -->
						<script type="text/javascript">
						window.onload = function () 
						{
						    var chart = new CanvasJS.Chart("chart",
						    {
						      title:{
						        text: "Individual Yearly Sales for <?php print $sp.' - '.$year; ?>"
						      },
						      axisX:{
						        interval: 1,
						        intervalType: "month"
						      },
						      data: [
						      {
						        type: "line",
						        dataPoints: [//array
						        <?php print $js_data_points; ?>
						       ]
						     }
						     ]
						    });

						    chart.render();
					    } 
						</script>
						<script type="text/javascript" src="vendor/plugins/canvas/canvasjs.min.js"></script>

						<div id = "chart" name = "chart" style = "height:200px;">
						</div>

						<?php
						print $table;
					}
					else
					{
						$jan=0;
		        		$feb = 0;
		        		$march = 0;
		        		$april = 0;
		        		$may = 0;
		        		$june = 0;
		        		$july = 0;
		        		$aug = 0;
		        		$sept = 0;
		        		$oct = 0;
		        		$nov = 0;
		        		$dec = 0;

		        		$start = strtotime($year.'-01-01');
						$end = strtotime($year.'-12-31');

						$janstart = $start;
						$janend = date($year.'-01-t');
						$janend = strtotime($janend);

						$febstart = strtotime($year.'-02-01');
						$febend = date($year.'-02-t');
						$febend = strtotime($febend);

						$marchstart = strtotime($year.'-03-01');
						$marchend = date($year.'-03-t');
						$marchend = strtotime($marchend);
						
						$aprilstart = strtotime($year.'-04-01');
						$aprilend = date($year.'-04-t');
						$aprilend = strtotime($aprilend);

						$maystart = strtotime($year.'-05-01');
						$mayend = date($year.'-05-t');
						$mayend = strtotime($mayend);

						$junestart = strtotime($year.'-06-01');
						$juneend = date($year.'-06-t');
						$juneend = strtotime($juneend);

						$julystart = strtotime($year.'-07-01');
						$julyend = date($year.'-07-t');
						$julyend = strtotime($julyend);

						$augstart = strtotime($year.'-08-01');
						$augend = date($year.'-08-t');
						$augend = strtotime($augend);

						$septstart = strtotime($year.'-09-01');
						$septend = date($year.'-09-t');
						$septend = strtotime($septend);

						$octstart = strtotime($year.'-10-01');
						$octend = date($year.'-10-t');
						$octend = strtotime($octend);

						$novstart = strtotime($year.'-11-01');
						$novend = date($year.'-11-t');
						$novend = strtotime($novend);

						$decstart = strtotime($year.'-12-01');
						$decend = $end;
						//print '<br>start: '.$start;
						//print '<br>end: '.$end.'<br>';

		        		$projects = $vujade->get_individual_yearly_sales_report($start,$end,$sp);
		        		
		        		if($projects['error']=="0")
						{
							unset($projects['error']);
							foreach($projects as $p)
							{
								$date_opened = strtotime($p['open_date']);
								//print $date_opened.'<br>';
								//print $janstart.' - janend: '.$janend.'<br>';
								if(($date_opened>=$janstart) && ($date_opened<=$janend))
								{
									$line = str_replace(',','',$p['line_total']);
									$jan+=$line;
								}
								if(($date_opened>=$febstart) && ($date_opened<=$febend))
								{
									$line = str_replace(',','',$p['line_total']);
									$feb+=$line;
								}
								if(($date_opened>=$marchstart) && ($date_opened<=$marchend))
								{
									$line = str_replace(',','',$p['line_total']);
									$march+=$line;
								}
								if(($date_opened>=$aprilstart) && ($date_opened<=$aprilend))
								{
									$line = str_replace(',','',$p['line_total']);
									$april+=$line;
								}
								if(($date_opened>=$maystart) && ($date_opened<=$mayend))
								{
									$line = str_replace(',','',$p['line_total']);
									$may+=$line;
								}
								if(($date_opened>=$junestart) && ($date_opened<=$juneend))
								{
									$line = str_replace(',','',$p['line_total']);
									$june+=$line;
								}
								if(($date_opened>=$julystart) && ($date_opened<=$julyend))
								{
									$line = str_replace(',','',$p['line_total']);
									$july+=$line;
								}
								if(($date_opened>=$augstart) && ($date_opened<=$augend))
								{
									$line = str_replace(',','',$p['line_total']);
									$aug+=$line;
								}
								if(($date_opened>=$septstart) && ($date_opened<=$septend))
								{
									$line = str_replace(',','',$p['line_total']);
									$sept+=$line;
								}
								if(($date_opened>=$octstart) && ($date_opened<=$octend))
								{
									$line = str_replace(',','',$p['line_total']);
									$oct+=$line;
								}
								if(($date_opened>=$novstart) && ($date_opened<=$novend))
								{
									$line = str_replace(',','',$p['line_total']);
									$nov+=$line;
								}
								if(($date_opened>=$decstart) && ($date_opened<=$decend))
								{
									$line = str_replace(',','',$p['line_total']);
									$dec+=$line;
								}
							}
						}
						$total = $jan + $feb + $march + $april + $may + $june + $july + $aug + $sept + $oct + $nov + $dec;
						?>

						<!-- chart -->
						<script type="text/javascript">
						window.onload = function () 
						{
						    var chart = new CanvasJS.Chart("chart",
						    {
						      title:{
						        text: "Individual Yearly Sales for <?php print $sp.' - '.$year; ?>"
						      },
						      axisX:{
						        interval: 1,
						        intervalType: "month"
						      },
						      data: [
						      {
						        type: "line",
						        dataPoints: [//array
						        {
						         x: new Date(<?php print $year; ?>, 00, 1),
						         y: <?php print $jan; ?>,
						       },
						       {
						         x: new Date(<?php print $year; ?>, 01, 1),
						         y: <?php print $feb; ?>,
						       }
						       , {
						         x: new Date(<?php print $year; ?>, 02, 1),
						         y: <?php print $march; ?>,

						       }, {
						         x: new Date(<?php print $year; ?>, 03, 1),
						         y: <?php print $april; ?>,

						       }, {
						         x: new Date(<?php print $year; ?>, 04, 1),
						         y: <?php print $may; ?>,

						       }, {
						         x: new Date(<?php print $year; ?>, 05, 1),
						         y: <?php print $june; ?>,

						       }, {
						         x: new Date(<?php print $year; ?>, 06, 1),
						         y: <?php print $july; ?>,

						       }, {
						         x: new Date(<?php print $year; ?>, 07, 1),
						         y: <?php print $aug; ?>,

						       },{
						         x: new Date(<?php print $year; ?>, 08, 1),
						         y: <?php print $sept; ?>,

						       },{
						         x: new Date(<?php print $year; ?>, 09, 1),
						         y: <?php print $oct; ?>,
						       
						       },{
						         x: new Date(<?php print $year; ?>, 10, 1),
						         y: <?php print $nov; ?>,
						       
						       },{
						         x: new Date(<?php print $year; ?>, 11, 1),
						         y: <?php print $dec; ?>,
						       }
						       ]
						     }
						     ]
						    });

						    chart.render();
					    } 
						</script>
						<script type="text/javascript" src="vendor/plugins/canvas/canvasjs.min.js"></script>

						<div id = "chart" name = "chart" style = "height:200px;">
						</div>

						<table class = "table table-bordered">
							<tr>
								<td>January
								</td>
								<td>$<?php print @number_format($jan,2); ?>
								</td>
							</tr>
							<tr>
								<td>February
								</td>
								<td>$<?php print @number_format($feb,2); ?>
								</td>
							</tr>
							<tr>
								<td>March
								</td>
								<td>$<?php print @number_format($march,2); ?>
								</td>
							</tr>
							<tr>
								<td>April
								</td>
								<td>$<?php print @number_format($april,2); ?>
								</td>
							</tr>
							<tr>
								<td>May
								</td>
								<td>$<?php print @number_format($may,2); ?>
								</td>
							</tr>
							<tr>
								<td>June
								</td>
								<td>$<?php print @number_format($june,2); ?>
								</td>
							</tr>
							<tr>
								<td>July
								</td>
								<td>$<?php print @number_format($july,2); ?>
								</td>
							</tr>
							<tr>
								<td>August
								</td>
								<td>$<?php print @number_format($aug,2); ?>
								</td>
							</tr>
							<tr>
								<td>September
								</td>
								<td>$<?php print @number_format($sept,2); ?>
								</td>
							</tr>
							<tr>
								<td>October
								</td>
								<td>$<?php print @number_format($oct,2); ?>
								</td>
							</tr>
							<tr>
								<td>November
								</td>
								<td>$<?php print @number_format($nov,2); ?>
								</td>
							</tr>
							<tr>
								<td>December
								</td>
								<td>$<?php print @number_format($dec,2); ?>
								</td>
							</tr>
							<tr>
								<td>Total
								</td>
								<td>$<?php print @number_format($total,2); ?>
								</td>
							</tr>
						</table>

					<?php
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // year was changed
    $('#year').change(function()
    {
    	var year = $(this).val();
    	if(year!='')
    	{
    		var href = "report_individual_sales.php?year="+year+"&sp=<?php print $sp; ?>";
    		window.location.href=href;
    	}
    });

});
</script>

</body>
</html>