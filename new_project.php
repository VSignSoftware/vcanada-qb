<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
  $vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['create']!=1)
{
  $vujade->page_redirect('error.php?m=1');
}

unset($_SESSION['project_search']);

$setup=$vujade->get_setup();

$action = 0;
if(isset($_POST['action']))
{
    $action = $_POST['action'];
}
# create new project
if($action==1)
{
    $site=$_POST['site'];
    $salesperson_id=$_POST['salesperson'];

    if($salesperson_id=="House")
	{
		$salesperson['fullname']="House";
	}
	else
	{
		$salesperson = $vujade->get_employee($salesperson_id,2);
	}

	$pm_id = $_POST['pm'];
	$project_manager = $vujade->get_employee($pm_id,2);

    $status=$_POST['status'];
    $address_1=$_POST['address_1'];
    $address_2=$_POST['address_2'];
    $city=$_POST['city'];
    $state=$_POST['state'];
    $zip=$_POST['zip'];
    $is_legacy = 0;

    $site_contact=$_POST['site_contact'];
    $site_contact_phone=$_POST['site_contact_phone'];
    $site_contact_email=$_POST['site_contact_email'];
    
    $project_contact = $_POST['project_contact'];
    $project_contact_phone = $_POST['project_contact_phone'];
    $project_contact_email = $_POST['project_contact_email'];

    if($is_legacy!=1)
    {
        $n = $vujade->get_new_project_id();
        if($n['error']=='0')
        {
            $project_id=$n['highest']+1;
        }
        else
        {
            $vujade->page_redirect('error.php?m=4');
        }
    }
    else
    {
        $project_id=$_POST['project_id'];
        $vujade->check_empty($project_id,'Legacy Project ID');
        $testproject = $vujade->get_project($project_id,2);
        if($testproject['error']=="0")
        {
            $vujade->errors[]='Project number already exists. Please choose a new number.';
        }
    }

    $e = $vujade->get_error_count();
    if($e<=0)
    {
        $vujade->create_row('projects');
        $id = $vujade->row_id;
        $s = array();
        $s[] = $vujade->update_row('projects',$id,'project_id',$project_id);
        $s[] = $vujade->update_row('projects',$id,'site',$site);
        $s[] = $vujade->update_row('projects',$id,'salesperson',$salesperson['fullname']);

        $s[] = $vujade->update_row('projects',$id,'project_manager',$project_manager['fullname']);

        $s[] = $vujade->update_row('projects',$id,'status',$status);
        $s[] = $vujade->update_row('projects',$id,'address_1',$address_1);
        $s[] = $vujade->update_row('projects',$id,'address_2',$address_2);
        $s[] = $vujade->update_row('projects',$id,'city',$city);
        $s[] = $vujade->update_row('projects',$id,'state',$state);
        $s[] = $vujade->update_row('projects',$id,'zip',$zip);
        $s[] = $vujade->update_row('projects',$id,'site_contact',$site_contact);
        $s[] = $vujade->update_row('projects',$id,'site_contact_phone',$site_contact_phone);
        $s[] = $vujade->update_row('projects',$id,'site_contact_email',$site_contact_email);
        $s[] = $vujade->update_row('projects',$id,'open_date',date('m/d/Y'));
        $s[] = $vujade->update_row('projects',$id,'is_legacy',$is_legacy);
        $s[] = $vujade->update_row('projects',$id,'project_contact',$project_contact);
        $s[] = $vujade->update_row('projects',$id,'project_contact_phone',$project_contact_phone);
        $s[] = $vujade->update_row('projects',$id,'project_contact_email',$project_contact_email);
        $s[] = $vujade->update_row('projects',$id,'project_status_number',1);
        $s[] = $vujade->update_row('projects',$id,'ts',strtotime('now'));
        $s[] = $vujade->update_row('projects',$id,'indeterminant',$setup['indeterminant']);
        $sum = array_sum($s);
        $vujade->page_redirect('new_project_customer.php?id='.$project_id);
    }
}
# cancel button was pressed
if($action==2)
{
    $vujade->page_redirect("dashboard.php");
}
$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=2;

$title = "New Project - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">New Project</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">

                  <p>
                    <span class="label label-primary">Step 1: Project Setup</span> <span class="label label-default">Step 2: Customer Selection</span> <span class="label label-default">Step 3: Scope of Work</span>
                  </p>

                  <form method="post" action="new_project.php" id="np">
                  
                    <input type = "hidden" name = "action" id = "action" value = "1">

                    <div class="row">

                      <!-- left side -->
                      <div class="col-md-6">

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "site" id = "site" value = "" placeholder="Project Name / Site">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field select">
                            <select id="salesperson" name="salesperson">
                              <?php
                              if(isset($salesperson_id))
                              {
                                print '<option value = "'.$salesperson_id.'" selected = "selected">'.$salesperson['fullname'].'</option>';
                                print '<option value = "">-Salesperson-</option>';
                              }
                              else
                              {
                                print '<option value = "">-Salesperson-</option>';
                              }
                              $sp = $vujade->get_salespeople();
                              if($sp['error']=='0')
                              {
                                unset($sp['error']);
                                foreach($sp as $s)
                                {
                                  print '<option value = "'.$s['employee_id'].'">'.$s['fullname'].'</option>';
                                }
                              }
                              // house
                              print '<option value = "House">House</option>';
                              ?>
                            </select>
                            <i class="arrow double"></i>
                          </label>
                        </div>

						<div class="section">
                          <label class="field select">
							<select name = "pm" id = "pm">
							<?php
							if(!empty($project['project_manager']))
							{
								$emp2 = $vujade->get_employee_by_name($project['project_manager']);
								print '<option value = "'.$emp2['employee_id'].'" selected = "selected">'.$project['project_manager'].'</option>';
								print '<option value = "">-Project Manager-</option>';
							}
							else
							{
								print '<option value = "">-Project Manager-</option>';
							}
							$pms = $vujade->get_project_managers();
							if($pms['error']=='0')
							{
								unset($pms['error']);
								foreach($pms as $pm)
								{
									print '<option value = "'.$pm['employee_id'].'">'.$pm['fullname'].'</option>';
								}
							}
							?>
							</select>
							<i class="arrow double"></i>
							</label>
						</div>

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "address_1" id = "address_1" value = "<?php print $address_1; ?>" placeholder="Address Line 1">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "address_2" id = "address_2" value = "<?php print $address_2; ?>" placeholder="Address Line 2">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field">
                            <input class="gui-input" type = "text" name = "city" id = "city" value = "<?php print $city; ?>" placeholder="City">
                          </label>
                        </div>

                        <div class="section">
                          <label class="field select">
                            <select id="state" name="state">
                              <?php
                              if(isset($state))
                              {
                                  print '<option value = "'.$state.'" selected = "selected">'.$state.'</option>';
                                  //print '<option value = "">-State-</option>';
                              }
                              else
                              {
                                  //print '<option value = "">-State-</option>';
                              }
                              if($setup['country']=="USA")
			              	  {
                              ?>
                              <option value="AL">Alabama</option>
                              <option value="AK">Alaska</option>
                              <option value="AZ">Arizona</option>
                              <option value="AR">Arkansas</option>
                              <option value="CA">California</option>
                              <option value="CO">Colorado</option>
                              <option value="CT">Connecticut</option>
                              <option value="DE">Delaware</option>
                              <option value="DC">District Of Columbia</option>
                              <option value="FL">Florida</option>
                              <option value="GA">Georgia</option>
                              <option value="HI">Hawaii</option>
                              <option value="ID">Idaho</option>
                              <option value="IL">Illinois</option>
                              <option value="IN">Indiana</option>
                              <option value="IA">Iowa</option>
                              <option value="KS">Kansas</option>
                              <option value="KY">Kentucky</option>
                              <option value="LA">Louisiana</option>
                              <option value="ME">Maine</option>
                              <option value="MD">Maryland</option>
                              <option value="MA">Massachusetts</option>
                              <option value="MI">Michigan</option>
                              <option value="MN">Minnesota</option>
                              <option value="MS">Mississippi</option>
                              <option value="MO">Missouri</option>
                              <option value="MT">Montana</option>
                              <option value="NE">Nebraska</option>
                              <option value="NV">Nevada</option>
                              <option value="NH">New Hampshire</option>
                              <option value="NJ">New Jersey</option>
                              <option value="NM">New Mexico</option>
                              <option value="NY">New York</option>
                              <option value="NC">North Carolina</option>
                              <option value="ND">North Dakota</option>
                              <option value="OH">Ohio</option>
                              <option value="OK">Oklahoma</option>
                              <option value="OR">Oregon</option>
                              <option value="PA">Pennsylvania</option>
                              <option value="RI">Rhode Island</option>
                              <option value="SC">South Carolina</option>
                              <option value="SD">South Dakota</option>
                              <option value="TN">Tennessee</option>
                              <option value="TX">Texas</option>
                              <option value="UT">Utah</option>
                              <option value="VT">Vermont</option>
                              <option value="VA">Virginia</option>
                              <option value="WA">Washington</option>
                              <option value="WV">West Virginia</option>
                              <option value="WI">Wisconsin</option>
                              <option value="WY">Wyoming</option>
                              <option value="">---------------</option>
				              <option value="">-Canadian Provinces-</option>
				              <option value="Alberta">Alberta</option>
				              <option value="British Columbia">British Columbia</option>
				              <option value="Manitoba">Manitoba</option>
				              <option value="New Brunswick">New Brunswick</option>
				              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
				              <option value="Northwest Territories">Northwest Territories</option>
				              <option value="Nova Scotia">Nova Scotia</option>
				              <option value="Nunavut">Nunavut</option>
				              <option value="Ontario">Ontario</option>
				              <option value="Prince Edward Island">Prince Edward Island</option>
				              <option value="Quebec">Quebec</option>
				              <option value="Saskatchewan">Saskatchewan</option>
				              <option value="Yukon">Yukon</option>
				            <?php 
					    	} 
					    	// canadian servers
					    	if($setup['country']=='Canada')
					    	{
					    		?>
					    		  <option value="">-Canadian Provinces-</option>
					              <option value="Alberta">Alberta</option>
					              <option value="British Columbia">British Columbia</option>
					              <option value="Manitoba">Manitoba</option>
					              <option value="New Brunswick">New Brunswick</option>
					              <option value="Newfoundland and Labrador">Newfoundland and Labrador</option>
					              <option value="Northwest Territories">Northwest Territories</option>
					              <option value="Nova Scotia">Nova Scotia</option>
					              <option value="Nunavut">Nunavut</option>
					              <option value="Ontario">Ontario</option>
					              <option value="Prince Edward Island">Prince Edward Island</option>
					              <option value="Quebec">Quebec</option>
					              <option value="Saskatchewan">Saskatchewan</option>
					              <option value="Yukon">Yukon</option>
					              <option value="">---------------</option>
					              <option value="">-US States-</option>
					              <option value="AL">Alabama</option>
					              <option value="AK">Alaska</option>
					              <option value="AZ">Arizona</option>
					              <option value="AR">Arkansas</option>
					              <option value="CA">California</option>
					              <option value="CO">Colorado</option>
					              <option value="CT">Connecticut</option>
					              <option value="DE">Delaware</option>
					              <option value="DC">District Of Columbia</option>
					              <option value="FL">Florida</option>
					              <option value="GA">Georgia</option>
					              <option value="HI">Hawaii</option>
					              <option value="ID">Idaho</option>
					              <option value="IL">Illinois</option>
					              <option value="IN">Indiana</option>
					              <option value="IA">Iowa</option>
					              <option value="KS">Kansas</option>
					              <option value="KY">Kentucky</option>
					              <option value="LA">Louisiana</option>
					              <option value="ME">Maine</option>
					              <option value="MD">Maryland</option>
					              <option value="MA">Massachusetts</option>
					              <option value="MI">Michigan</option>
					              <option value="MN">Minnesota</option>
					              <option value="MS">Mississippi</option>
					              <option value="MO">Missouri</option>
					              <option value="MT">Montana</option>
					              <option value="NE">Nebraska</option>
					              <option value="NV">Nevada</option>
					              <option value="NH">New Hampshire</option>
					              <option value="NJ">New Jersey</option>
					              <option value="NM">New Mexico</option>
					              <option value="NY">New York</option>
					              <option value="NC">North Carolina</option>
					              <option value="ND">North Dakota</option>
					              <option value="OH">Ohio</option>
					              <option value="OK">Oklahoma</option>
					              <option value="OR">Oregon</option>
					              <option value="PA">Pennsylvania</option>
					              <option value="RI">Rhode Island</option>
					              <option value="SC">South Carolina</option>
					              <option value="SD">South Dakota</option>
					              <option value="TN">Tennessee</option>
					              <option value="TX">Texas</option>
					              <option value="UT">Utah</option>
					              <option value="VT">Vermont</option>
					              <option value="VA">Virginia</option>
					              <option value="WA">Washington</option>
					              <option value="WV">West Virginia</option>
					              <option value="WI">Wisconsin</option>
					              <option value="WY">Wyoming</option>
					    		<?php
					    	}
					    	?>
                            </select>
                            <i class="arrow double"></i>
                            </label>
                        </div>

                        <div class="section">
                          <label class="field">
                            <input type = "text" name = "zip" id = "zip" class="gui-input" value = "<?php print $zip; ?>" maxlength="10" placeholder="Zip / Postal Code">
                          </label>
                        </div>

                      </div>

                      <!-- right side -->
                      <div class="col-md-6">
                        
                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "project_contact" name = "project_contact" value = "<?php print $project_site_contact; ?>" placeholder="Project Contact">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "project_contact_phone" name = "project_contact_phone" value = "<?php print $project_contact_phone; ?>" placeholder="Project Contact Phone">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "project_contact_email" name = "project_contact_email" value = "<?php print $project_contact_email; ?>" placeholder="Project Contact Email">
                            </label>
                          </div>

                          <div class="section">
                            <div class="checkbox-custom mb5">
                              <input type = "checkbox" name = "copy" id = "copy">
                              <label for="copy">Copy Project Contact To Site Contact</label>
                            </div>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "site_contact" name = "site_contact" value = "<?php print $site_contact; ?>" placeholder="Site Contact">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "site_contact_phone" name = "site_contact_phone" value = "<?php print $site_contact_phone; ?>" placeholder="Site Contact Phone">
                            </label>
                          </div>

                          <div class="section">
                            <label class="field">
                              <input class="gui-input" type = "text" id = "site_contact_email" name = "site_contact_email" value = "<?php print $site_contact_email; ?>" placeholder="Site Contact Email">
                            </label>
                          </div>

                      </div>

                    </div>

                    <div>
                      <div style = "float:left;">
                        <a href = "#" id = "cancel" class = "btn btn-lg btn-danger" style = "width:100px;">CANCEL</a> 
                        <a href = "#" id = "next" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND CONTINUE</a>
                      </div> 
                    </div>

                  </form>
                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();

    // Init Demo JS  
    // Demo.init();

    $('#is_legacy').change(function()
    {
      if(this.checked)
      { 
        $('#legacy').show();
      }
      else
      {
        $('#legacy').hide();
      }
    }); 

    $('#cancel').click(function(e)
    {
        e.preventDefault();
        $('#action').val('2');
        $('#np').submit();
    });

    $('#next').click(function(e)
    {
        e.preventDefault();
        $('#action').val('1');
        $('#np').submit();
    });

    // make the enter key function like a tab
    $('input').keydown( function(e) {
        var key = e.charCode ? e.charCode : e.keyCode ? e.keyCode : 0;
        if(key == 13) {
            e.preventDefault();
            var inputs = $(this).closest('form').find(':input:visible');
            inputs.eq( inputs.index(this)+ 1 ).focus();
        }
    });

    $('#copy').change(function()
    {
      var name = $('#project_contact').val();
      var phone = $('#project_contact_phone').val();
      var email = $('#project_contact_email').val();
      if($(this).is(':checked'))
      {
        $('#site_contact').val(name);
        $('#site_contact_phone').val(phone);
        $('#site_contact_email').val(email);
      }
      else
      {
        $('#site_contact').val('');
        $('#site_contact_phone').val('');
        $('#site_contact_email').val('');
      }
    });
 
});
</script>

</body>

</html>