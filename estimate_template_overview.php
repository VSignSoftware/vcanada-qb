<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// full access permissions
$estimates_fa_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Full Access');

$estimates = $vujade->get_estimates(3);

# active tab
if(isset($_REQUEST['tab']))
{
	$tab=$_REQUEST['tab'];
	if(!in_array($tab, array(0,1,2,3,4,5)))
	{
		$tab=0;
	}
}
else
{
	$tab=0;
}

if(isset($_REQUEST['estimateid']))
{
	$firstestimate = $vujade->get_estimate($_REQUEST['estimateid']);
	$estimateid = $_REQUEST['estimateid'];
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# delete estimate
if($action==1)
{

	// full access permissions
	if($estimates_fa_permissions['read'] != 1)
	{
		die;
	}

	$estimateid = $_REQUEST['estimateid'];

	# this has to be done in order to clear the database of materials, buyouts and labor in addition to deleting the estimate itself

	# get estimate info 
	$estimate = $vujade->get_estimate($estimateid);

	if($estimate['error']==0)
	{

		# delete materials
		$s1 = $vujade->delete_row('estimates_material',$estimate['estimate_id'],1,'estimate_id');

		# delete labor
		$s1 = $vujade->delete_row('estimates_time',$estimate['estimate_id'],1,'estimate_id');

		# delete machines
		$s1 = $vujade->delete_row('estimates_machines',$estimate['estimate_id'],1,'estimate_id');

		# delete buyouts
		$s1 = $vujade->delete_row('estimates_buyouts',$estimate['estimate_id'],1,'estimate_id');
		
	}
	else
	{
		print '<strong>System Error</strong><br>';
		print 'Message: '.$estimate['error'];
		print '<br>Please contact the system administrator and report this error.';
		die;
	}

	# delete the estimate
	$s4 = $vujade->delete_row('estimates',$estimateid);
	$vujade->page_redirect('estimate_templates.php?m=3');
}

$setup = $vujade->get_setup(1);
if($setup['design_email_message']=="")
{
	$default_msg = "Please find attached design for your review and processing.";
}
else
{
	$default_msg = $setup['design_email_message'];
}

// overhead percentages
$buyout_overhead_percentage=$firstestimate['overhead_buyout_rate'];
$general_overhead_percentage=$firstestimate['overhead_general_rate'];
$machine_overhead_percentage=$firstestimate['overhead_machines_rate'];

// estimate templates
$templates = $vujade->get_estimates(3);

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$t = "Estimate Template - ".$firstestimate['template_name'];
//$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $t; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<?php
					// permissions
					if($estimates_fa_permissions['read']==1)
					{
						?>

						<a style = "margin-right:5px;" class = "btn btn-xs btn-primary" href = "edit_estimate_template.php?project_id=<?php print $id; ?>&estimateid=<?php print $estimateid; ?>" title = "Edit Estimate">Edit</a> 

						<!-- transfer -->
						<a class = "btn btn-xs btn-primary" id = "transfer" href = "#transfer-form" title = "Transfer from other" style = "float:right;margin-top:5px;">Transfer</a>

						<!-- transfer modal -->
						<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
							<input type = "hidden" name ="transfer_to" id = "transfer_to" value = "<?php print $estimateid; ?>">
							<form id = "transfer_form" class="form-inline" style = "margin-bottom:15px;">

								<table>
									<tr>
										<td><label>Estimate Number</label></td>
										<td><input type = "text" name = "transfer_from" id = "transfer_from" class = "form-control input-sm" style = "width:230px;margin-left:5px;"></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td><label>OR</label></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td><label>Template</label></td>
										<td><select name = "transfer_from_template" id = "transfer_from_template" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
										<option value = "">-Select-</option>
										<?php
										if($templates['count']>0)
										{
											unset($templates['count']);
											unset($templates['error']);
											unset($templates['sql']);
											foreach($templates as $template)
											{
												print '<option value = "'.$template['estimate_id'].'">'.$template['template_name'].'</option>';
											}
										}
										?>
									</select></td>
									</tr>

									<tr>
										<td></td>
										<td></td>
									</tr>

									<tr>
										<td><label>Include Description?</label></td>
										<td>
											<select name = "copy_description" id = "copy_description" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
												<option value = "">-Select-</option>
												<option value = "1">Yes</option>
												<option value = "">No</option>
											</select>
									</td>
									</tr>

								</table>

								<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
								
								<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please enter an estimate number.</div>
								<div id = "error_2" style = "display:none;" class = "alert alert-danger">This estimate number is invalid.</div>

								<div class = "" style = "width:100%;margin-top:15px;">
									<a id = "transfer_btn" href = "#" class = "btn btn-lg btn-primary" style = "margin-right:15px;">Go</a><a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">Cancel</a>
								</div>
							</form>
						</div>

					<?php 
					}
					?>

					<a style = "" class = "btn btn-xs btn-primary" href = "print-estimate-html.php?id=<?php print $id; ?>&estimateid=<?php print $estimateid; ?>" target = "_blank" title = "Print Estimate">Print</a> 

					<a style = "" class = "btn btn-xs btn-primary" href = "sf-print-estimate-html.php?id=<?php print $id; ?>&estimateid=<?php print $estimateid; ?>" target = "_blank" title = "Print Estimate">Print Shop Friendly</a> 

					<?php
					if($estimates_fa_permissions['read']==1)
					{
					?>
						<a class = "btn btn-xs btn-danger" id = "delete_estimate" href = "#delete_estimate_form" title = "Delete Estimate">Delete</a>

						<!-- modal for delete estimate -->
						<div id="delete_estimate_form" class="popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
						<h1>Delete Estimate</h1>
						<p>Are you sure you want to delete this estimate?</p>
						<p><a id = "" class="btn btn-lg btn-danger" href="estimate_template_overview.php?estimateid=<?php print $estimateid; ?>&action=1">YES</a> <a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a></p>
						</div>
					<?php } ?> 

					<a style = "" class = "btn btn-xs btn-primary" href = "estimate_templates.php" title = "Back">Back to Templates</a> 

				</div>
	        	<div class="panel-body bg-light">
	        		<?php
					if( ($firstestimate['error']=='0') || ($firstestimate['error']=='') )
					{

						//$buyout_overhead_percentage=$firstestimate['overhead_buyout_rate'];
						//$general_overhead_percentage=$firstestimate['overhead_general_rate'];

						//print 'Buyout: '.$buyout_overhead_percentage.'<br>General: '.$general_overhead_percentage.'<br>';

						print '<div style = "padding-left:10px;">';
						print '<table width = "100%">';
						print '<tr>';
						print '<td valign = "top">Date Opened: ';
						print '</td>';
						print '<td valign = "top">';
						print $firstestimate['date'];
						print '</td>';
						print '<td valign = "top">';
						//print 'Date Required: ';
						print '</td>';
						print '<td valign = "top">';
						//print $firstestimate['required_date'];
						print '</td>';
						print '</tr>';

						// status
						print '<tr>';
						print '<td colspan = "4">';
						
						print '</td>';
						print '</tr>';
						print '<tr>';
						print '<td valign = "top" colspan = "4">Description: ';
						print '</td>';
						print '</tr>';
						print '<tr>';
						print '<td colspan = "4" valign = "top">';
						print '<div style = "overflow:auto;height:100px;">';
						print $firstestimate['description'];
						print '</div>';
						print '</td>';
						print '</tr>';
						print '</table>';
						?>
						<div class="panel">
						  <div class="panel-heading">
						    <ul class="nav panel-tabs-border panel-tabs panel-tabs-left">
						      <li class="active">
						        <a href="#tab1" data-toggle="tab">Materials</a>
						      </li>
						      <li>
						        <a href="#tab2" data-toggle="tab">Labor</a>
						      </li>
						      <li>
						        <a href="#tab6" data-toggle="tab">Machines</a>
						      </li>
						      <li>
						        <a href="#tab3" data-toggle="tab">Outsource</a>
						      </li>
						      <li>
						        <a href="#tab4" data-toggle="tab">Total Costs</a>
						      </li>
						      <?php
							  if($estimates_permissions['edit']==1)
							  {
								  ?>
							      <li>
							        <a href="#tab5" data-toggle="tab">Markup</a>
							      </li>
							  <?php } ?>
						    </ul>
						  </div>
						  <div class="panel-body">
						    <div class="tab-content pn br-n">

						      <!-- materials tab 1 -->
						      <div id="tab1" class="tab-pane active">
						        <div class="row">
						          <div class="col-md-12">
						          <table width = "100%" class = "table table-hover table-striped  table-bordered">
									<thead>
									<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
									<th>
										Item No.
									</th>
									<th>
										Description
									</th>
									<th>
										Qty
									</th>
									<th>
										Amount
									</th>
									</tr>
									</thead>

									<tbody>
									<?php
									if(!empty($firstestimate['estimate_id']))
									{
										$materials1 = $vujade->get_materials_for_estimate($firstestimate['database_id']);
									}
									$subtotal = 0;
									if($materials1['error']=="0")
									{
										unset($materials1['error']);
										foreach($materials1 as $m1)
										{
											$line = $m1['qty']*$m1['cost'];
											print '<tr>';
											print '<td style = "" valign = "top">';
											print $m1['inventory_id'];
											print '</td>';
											print '<td style = "" valign = "top">';
											print $m1['description'];
											print '</td>';
											print '<td style = "" valign = "top">';
											print $m1['qty'];
											print '</td>';
											print '<td style = "" valign = "top">';
											print '$'.@number_format($line,2,'.',',');
											print '</td>';
											print '</tr>';
											
											$subtotal = $subtotal+$line;
										}
										print '<tr class = "dark">';
										print '<td>Indeterminate';
										$indet = $subtotal * $firstestimate['indeterminant'];
										$mat_total = $subtotal+$indet;
										print '</td>';
										print '<td>$';
										print @number_format($indet,2,'.',',');
										print '</td>';
										print '<td>';
										print 'Subtotal';
										print '</td>';
										print '<td>';
										print '$'.@number_format($mat_total,2,'.',',');
										print '</td>';
										print '</tr>';
									}
									?>
									</tbody>
									</table>
						          </div>
						        </div>
						      </div>

						      <!-- labor tab2 -->
						      <div id="tab2" class="tab-pane">
						        <div class="row">
						          <div class="col-md-12">
									<table width = "100%" class = "table table-hover table-striped  table-bordered">
									<thead>
									<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
									<th>
										Description
									</th>
									<th>
										Hours
									</th>
									<th>
										Rate
									</th>
									<th>
										Amount
									</th>
									</tr>
									</thead>
									<?php
									$labor_subtotal=0;
									$labor = $vujade->get_labor_for_estimate($firstestimate['estimate_id'],1);
									if($labor['error']=="0")
									{
										unset($labor['error']);
										foreach($labor as $l)
										{
											$llt=$l['rate']*$l['hours'];
											$labor_subtotal+=$llt;

											print '<tr>';
											print '<td>';
											print $l['type'];
											print '</td>';

											print '<td>';
											print $l['hours'];
											print '</td>';

											print '<td>';
											print $l['rate'];
											print '</td>';

											print '<td>';
											print '$'.@number_format($llt,2,'.',',');
											unset($llt);
											print '&nbsp;';
											print '</td>';

											print '</tr>';
										}
										print '<tr class = "dark">';
										print '<td>';
										print '</td>';
										print '<td>';
										print '</td>';
										print '<td>';
										print 'Total';
										print '</td>';
										print '<td>';
										print '$'.@number_format($labor_subtotal,2,'.',',');
										print '</td>';
										print '</tr>';
									}
									else
									{
										//print $labor['error'];
									}
									?>
									</table> 
						          </div>
						        </div>
						      </div>

						      <!-- machines tab6 -->
						      <div id="tab6" class="tab-pane">
						        <div class="row">
						          <div class="col-md-12">
									<table width = "100%" class = "table table-hover table-striped  table-bordered">
									<thead>
									<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
									<th>
										Description
									</th>
									<th>
										Quantity
									</th>
									<th>
										Rate
									</th>
									<th>
										Amount
									</th>
									</tr>
									</thead>
									<?php
									$machines_subtotal=0;
									$machines = $vujade->get_machines_for_estimate($firstestimate['estimate_id']);
									if($machines['error']=="0")
									{
										unset($machines['error']);
										foreach($machines as $ma)
										{
											$mlt=$ma['rate']*$ma['hours'];
											$machines_subtotal+=$mlt;

											print '<tr>';
											print '<td>';
											print $ma['type'];
											print '</td>';

											print '<td>';
											print $ma['hours'];
											print '</td>';

											print '<td>';
											print $ma['rate'];
											print '</td>';

											print '<td>';
											print '$'.@number_format($mlt,2,'.',',');
											unset($mlt);
											print '&nbsp;';
											print '</td>';

											print '</tr>';
										}
										print '<tr class = "dark">';
										print '<td>';
										print '</td>';
										print '<td>';
										print '</td>';
										print '<td>';
										print 'Total';
										print '</td>';
										print '<td>';
										print '$'.@number_format($machines_subtotal,2,'.',',');
										print '</td>';
										print '</tr>';
									}
									else
									{
										//print $labor['error'];
									}
									?>
									</table> 
						          </div>
						        </div>
						      </div>

						      <!-- outsource tab 3 -->
						      <div id="tab3" class="tab-pane">
						        <div class="row">
						          <div class="col-md-12">
						          <table width = "100%" class = "table table-hover table-striped  table-bordered">
									<thead>
									<tr class = "primary" style = "border-top:1px solid #5D9CEC;">
									<th>
										Vendor
									</th>
									<th>
										Description
									</th>
									<th>
										Amount
									</th>
									</tr>
									</thead>

									<?php
									$buyouts_total=0;
									$buyouts = $vujade->get_buyouts($estimateid);
									if($buyouts['error']=="0")
									{
										unset($buyouts['error']);
										foreach($buyouts as $buyout)
										{	
											print '<tr>';
											print '<td>';
											print $buyout['subcontractor'];
											print '</td>';
											print '<td>';
											print $buyout['description'];
											print '</td>';
											print '<td>';
											//$boc = str_replace(",","",$buyout['cost']);
											$boc = str_replace("$","",$buyout['cost']);
											//print '$'.@number_format($boc,2);
											$boc=preg_replace("/,/",'',$boc);
											//print $boc.'<br>';
											//print $buyout['cost'];
											print '$'.@number_format($boc,2);
											print '</td>';
											print '</tr>';
											$buyouts_total=$buyouts_total+$boc;
										}
										print '<tr>';
										print '<td>';
										print '</td>';
										print '<td>';
										print 'Total';
										print '</td>';
										print '<td>';
										print '$'.@number_format($buyouts_total,2,'.',',');
										print '</td>';
										print '</tr>';
									}
									?>
									</table> 
						          </div>
						        </div>
						      </div>

						      <!-- total costs tab 4 -->
						      <div id="tab4" class="tab-pane">
						        <div class="row">
						          <div class="col-md-12">
						          <table width = "100%" class = "table table-hover table-striped  table-bordered">
						          <tr>
										<td>Materials</td>
										<td>$<?php print @number_format($mat_total,2,'.',','); ?></td>
									</tr>
									<tr>
										<td>Labor Cost: </td>
										<td>$<?php print @number_format($labor_subtotal,2,'.',','); ?></td>
									</tr>
									<tr>
										<td>Machines Cost: </td>
										<td>$<?php print @number_format($machines_subtotal,2,'.',','); ?></td>
									</tr>
									<tr>
										<td>Outsource</td>
										<td>$<?php print @number_format($buyouts_total,2,'.',','); ?></td>
									</tr>
									<tr>
										<td>Subtotal: </td>
										<?php
										$summary_subtotal = $buyouts_total + $labor_subtotal + $machines_subtotal + $mat_total; 
										?>
										<td>$<?php print @number_format($summary_subtotal,2,'.',','); ?></td>
									</tr>
									<tr>
										<td colspan = "2">&nbsp;</td>
									</tr>
									<tr>
										<td>General Overhead: </td>
										<?php
										// 48.5% of Inventory + Indeterminent + Total Labor Cost
										$go = $mat_total + $labor_subtotal;
										$go = $go * $general_overhead_percentage;
										?>
										<td>$<?php print @number_format($go,2,'.',','); ?>
										</td>
									</tr>

									<tr>
										<td>Machines Overhead: </td>
										<?php
										$machines_overhead = $machines_subtotal * $machine_overhead_percentage;
										?>
										<td>$<?php print @number_format($machines_overhead,2,'.',','); ?></td>
									</tr>
									
									<tr>
										<td>Outsource Overhead: </td>
										<?php
										$buyouts_overhead = $buyouts_total * $buyout_overhead_percentage;
										?>
										<td>$<?php print @number_format($buyouts_overhead,2,'.',','); ?></td>
									</tr>

									<tr>
										<td colspan = "2">&nbsp;</td>
									</tr>

									<tr>
										<?php
										$factory_cost = $mat_total+$labor_subtotal+$machines_subtotal+$buyouts_total+$buyouts_overhead+$machines_overhead+$go;
										?>
										<td>Total Factory Cost: </td>
										<td>$<?php print @number_format($factory_cost,2,'.',','); ?></td>
									</tr> 
									</table>   
						          </div>
						        </div>
						      </div>

						      <!-- markup tab 5 -->
						      <div id="tab5" class="tab-pane">
						        <div class="row">
						          <div class="col-md-12">
						          	<?php
									if($estimates_permissions['edit']==1)
									{
									?>
										<p>
										<?php
										$tiers = $vujade->get_commission_tiers();
										if($tiers['error']=="0")
										{
											unset($tiers['error']);
											print 'Commission Tier: <select name = "commission_tier" id = "commission_tier">';
											foreach($tiers as $tier)
											{
												print '<option value = "'.$tier['tier_id'].'">'.$tier['tier_label'].'</option>';
											}
											print '</select>';
										}
										?>
										</p>
										<div id = "markup">
										<?php
										# markups tab
										$tier = 1;
										if(!empty($firstestimate['commission_level']))
										{
											$tier = $firstestimate['commission_level'];
										}
										$markups = $vujade->get_markups($tier,$sort=1);
										if($markups['error']=='0')
										{
											unset($markups['error']);
											print '<table class = "table table-hover table-striped table-bordered">';
											print '<tr class = "primary">';
											print '<th>';
											print 'M/U';
											print '</th>';
											print '<th>';
											print 'Base Sale';
											print '</th>';
											print '<th>';
											print 'Comm %';
											print '</td>';
											print '<th>';
											print 'Commission';
											print '</th>';
											print '<th>';
											print 'Total Sale';
											print '</th>';
											print '<th>';
											print 'Profit';
											print '</th>';
											print '</tr>';
											foreach($markups as $markup)
											{
												print '<tr>';
												print '<td>';
												print @number_format($markup['markup'],2,'.',',');
												print '</td>';
												print '<td>';
												$bs = $markup['markup']*$factory_cost;
												//$bs = round($bs,2);
												print '$'.@number_format($bs,2,'.',',');
												print '</td>';
												print '<td>';
												print $markup['commission_percent'].'%';
												print '</td>';
												print '<td>';
												$commission = $markup['commission_percent']*$bs;
												//$commission=round($commission,2);
												print '$'.@number_format($commission,2,'.',',');
												print '</td>';
												print '<td>';
												$ts = $bs+$commission;
												print '$'.@number_format($ts,2,'.',',');
												print '</td>';
												print '<td>';
												$profit = $bs-$factory_cost;
												print '$'.@number_format($profit,2,'.',',');
												print '</td>';
												print '</tr>';
											}
											print '</table>';
										}
										?>
										</div>
									</div>

								<?php } ?>
						          </div>
						        </div>
						      </div>

						    </div>
						  </div>
						</div>

						<!-- total factory cost -->
						<div align = "right" id = "tfc" style = "font-size:14px;font-weight:bold;margin-right:15px;">
							Total Factory Cost: $<?php print @number_format($factory_cost,2,'.',','); ?>
							<input type = "hidden" name = "hfc" id = "hfc" value = "<?php print $factory_cost; ?>">
						</div>

					</div>
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // modal: delete estimate
    $('#delete_estimate').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#delete_estimate_form',
		modal: true
	});

	// modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

	$('#commission_tier').change(function()
	{
		var estimate_id = "<?php print $estimateid; ?>";
		var tier = $('#commission_tier').val();
		var fc = $('#hfc').val();
		$('#markup').html('');
		$.post( "jq.get_estimate_cost.php", { estimate_id: estimate_id, total: 2, tier: tier, fc: fc })
		  .done(function( data ) 
		  {
		  		$('#markup').html(data);
		  });
	});

	// modal: transfer estimate
    $('#transfer').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,		
	});
	
    // transfer button in the modal 
    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();
	
    	$('#working').hide();
    	$('#error_1').hide();
    	$('#error_2').hide();

		//var transfer_to = '';
		var transfer_to = $('#transfer_to').val();
		
		//var transfer_from = '';
		var transfer_from = $('#transfer_from').val();
		
		//var transfer_from_template = '';
		var transfer_from_template = $('#transfer_from_template').val();
		
		//var copy_description = '';
		var copy_description = $('#copy_description').val();

		//var id = '';
		var id = "<?php print $id; ?>";

		//var old_description="";

		var error = 0;
		if(transfer_from=="")
		{	
			error++;
		}
		if(transfer_from_template=="")
		{	
			error++;
		}
		if(error>1)
		{
			$('#error_1').show();
			return false;
		}
		else
		{
			if(transfer_from=='')
			{
				transfer_from=transfer_from_template;
			}
			$('#error_1').hide();
			$('#working').show();
			$('#working').html('Working...');
			$.post( "jq.transfer_estimate.php", { transfer_from: transfer_from, transfer_to: transfer_to, project_id: "Template", copy_description: copy_description, old_description: $('#od').html(), template: 1 })
			.done(function( response ) 
			{
				$('#working').show();
			    $('#working').html('');
			    $('#working').html(response);
			    if(response=="Success")
			    {
			    	$('#working').html('Success! This page will reload in 3 seconds...');

			    	setTimeout(function(){ window.location.reload(); }, 3000);
			    }
			    else
			    {
			    	$('#error_2').html(response);
			    	$('#error_2').show();
					$('#working').hide();
			    }
			});
		}
	});

	// press return on estimate transfer form
    $('#transfer_form').on('keypress', function(e) 
    {
		if (e.which == 13) 
		{
			//$('#form').submit();
			$('#transfer_btn').click();
			return false;
		}
	});

});
</script>

</body>
</html>