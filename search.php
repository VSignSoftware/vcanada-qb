<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

unset($_SESSION['list_url']);

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// type 1: user searched from the top search bar
if(!empty($_REQUEST['search']))
{
	$search = $_POST['search'];
	$search = trim($search);
	$search = strtolower($search);

	$search = array('number'=>$_POST['search'],'name'=>$_POST['search'],'billing_name'=>$_POST['search'],'address'=>$_POST['search'],'city'=>$_POST['search'],'state'=>$_POST['search'],'salesperson'=>$_POST['search'],'pm'=>$_POST['search'],'scope'=>$_POST['search']);

	// status
	if($search=='pending')
	{
		$_POST['search']=1;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='did not sell')
	{
		$_POST['search']=2;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='active ll approval')
	{
		$_POST['search']=3;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='active permits')
	{
		$_POST['search']=4;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='active manufacturing')
	{
		$_POST['search']=5;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='active install')
	{
		$_POST['search']=6;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='turned in for billing')
	{
		$_POST['search']=7;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='closed')
	{
		$_POST['search']=8;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='ready to be turned in')
	{
		$_POST['search']=10;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='active survey')
	{
		$_POST['search']=9;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='pending drawings')
	{
		$_POST['search']=11;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='pending estimates')
	{
		$_POST['search']=12;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='pending proposal')
	{
		$_POST['search']=13;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='pending proposal sent')
	{
		$_POST['search']=14;
		$search = array('status'=>$_POST['search']);
	}
	if($search=='service')
	{
		$_POST['search']=15;
		$search = array('status'=>$_POST['search']);
	}
	
	if(isset($_SESSION['search_id']))
	{
		unset($_SESSION['search_id']);
		// delete db row
	}

	// new db row
	$vujade->create_row('project_search');
	$row_id = $vujade->row_id;
	$_SESSION['search_id']=$row_id;
	$s=array();
	$s[]=$vujade->update_row('project_search',$row_id,'uid',$_SESSION['user_id']);
	$s[]=$vujade->update_row('project_search',$row_id,'number',$_POST['search']);
	$s[]=$vujade->update_row('project_search',$row_id,'name',$_POST['search']);
	$s[]=$vujade->update_row('project_search',$row_id,'scope',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'billing_name',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'address',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'status',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'city',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'state',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'sp',$_POST['search']);
	//$s[]=$vujade->update_row('project_search',$row_id,'pm',$_POST['search']);
	$vujade->page_redirect('projects.php?action=2');
}

// type 2: user searched from the projects.php page
/*
Array ( 
[number] => test 
[name] => 
[status] => 
[address] => 
[city] => 
[state] => 
[billing_name] => 
[sp] => 
[pm] => 
[action] => 1 
)
*/
if($_REQUEST['action']==1)
{
	$number=$_POST['number'];
	$name=$_POST['name'];
	$billing_name=$_POST['billing_name'];
	$address=$_POST['address'];
	$status=$_POST['status'];
	$city=$_POST['city'];
	$state=$_POST['state'];
	$sp=$_POST['sp'];
	$pm=$_POST['pm'];

	if( (empty($number)) && (empty($name)) && (empty($billing_name)) && (empty($address)) && (empty($status)) && (empty($city)) && (empty($state)) && (empty($sp)) && (empty($pm)) )
	{
		$vujade->page_redirect('projects.php?action=1');
	}
	else
	{
		$search = array('number'=>$number,'name'=>$name,'billing_name'=>$billing_name,'address'=>$address,'status'=>$status,'city'=>$city,'state'=>$state,'salesperson'=>$sp,'pm'=>$pm);

		// new db row
		$vujade->create_row('project_search');
		$row_id = $vujade->row_id;
		$_SESSION['search_id']=$row_id;
		$s=array();
		$s[]=$vujade->update_row('project_search',$row_id,'uid',$_SESSION['user_id']);
		$s[]=$vujade->update_row('project_search',$row_id,'number',$number);
		$s[]=$vujade->update_row('project_search',$row_id,'name',$name);
		$s[]=$vujade->update_row('project_search',$row_id,'billing_name',$billing_name);
		$s[]=$vujade->update_row('project_search',$row_id,'address',$address);
		$s[]=$vujade->update_row('project_search',$row_id,'status',$status);
		$s[]=$vujade->update_row('project_search',$row_id,'city',$city);
		$s[]=$vujade->update_row('project_search',$row_id,'state',$state);
		$s[]=$vujade->update_row('project_search',$row_id,'sp',$sp);
		$s[]=$vujade->update_row('project_search',$row_id,'pm',$pm);
		$vujade->page_redirect('projects.php?action=2');
	}
}
if($_REQUEST['action']==2)
{
	$sp=$_REQUEST['sp'];

	$search = array('number'=>$number,'name'=>$name,'billing_name'=>$billing_name,'address'=>$address,'status'=>$status,'city'=>$city,'state'=>$state,'salesperson'=>$sp,'pm'=>$pm);

	// new db row
	$vujade->create_row('project_search');
	$row_id = $vujade->row_id;
	$_SESSION['search_id']=$row_id;
	$s=array();
	$s[]=$vujade->update_row('project_search',$row_id,'uid',$_SESSION['user_id']);
	$s[]=$vujade->update_row('project_search',$row_id,'number',$number);
	$s[]=$vujade->update_row('project_search',$row_id,'name',$name);
	$s[]=$vujade->update_row('project_search',$row_id,'billing_name',$billing_name);
	$s[]=$vujade->update_row('project_search',$row_id,'address',$address);
	$s[]=$vujade->update_row('project_search',$row_id,'status',$status);
	$s[]=$vujade->update_row('project_search',$row_id,'city',$city);
	$s[]=$vujade->update_row('project_search',$row_id,'state',$state);
	$s[]=$vujade->update_row('project_search',$row_id,'sp',$sp);
	$s[]=$vujade->update_row('project_search',$row_id,'pm',$pm);
	$vujade->page_redirect('projects.php?action=2');
}

// type : user pressed the existing projects button
/*
if( (empty($_REQUEST)) || (empty($_REQUEST['search'])) )
{
	if(isset($_SESSION['search_id']))
	{
		unset($_SESSION['search_id']);
		// delete db row
	}
	$vujade->page_redirect('projects.php?action=1');
}
*/

if($_REQUEST['action']==3)
{
	unset($_SESSION['search_id']);
	$vujade->page_redirect('projects.php?action=1');
}

// type 4 user pressed back button on project detail page


//print_r($_REQUEST);
//die('This page is under development. Please check back soon.');
//*/
?>