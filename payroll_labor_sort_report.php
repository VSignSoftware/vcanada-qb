<?php 
session_start();

$time_start = microtime(true);

define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

# permissions
$accounting_permissions = $vujade->get_permission($_SESSION['user_id'],'Accounting');
if($accounting_permissions['read']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['edit']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}
if($accounting_permissions['create']!=1)
{
    $vujade->page_redirect('error.php?m=1');
}

$action = 0;
$s = array();
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# sort by employee and date range
if($action==1)
{
	$begin = $_POST['begin']; 
    $end = $_POST['end'];
    $employee_id = $_POST['employee_id'];

    # check empty
    $vujade->check_empty($begin,'Begin Date');
    $vujade->check_empty($end,'Begin End');
    $vujade->check_empty($employee_id,'Employee');

    # validate person id
    $person = $vujade->get_employee($employee_id);
    if($person['error']!="0")
    {
        $vujade->errors[]="Employee was not found.";
    }

    $rate = $vujade->get_employee_current_rate($employee_id);

    $e = $vujade->get_error_count();
    if($e>0)
    {
        $action=0;
    }
}

# sort by job and date range
if($action==2)
{
    $begin = $_POST['begin']; 
    $end = $_POST['end'];
    $project_id = $_POST['project_id'];

    # check empty
    $vujade->check_empty($begin,'Begin Date');
    $vujade->check_empty($end,'Begin End');
    $vujade->check_empty($project_id,'Project Number');

    # validate project number
    $project = $vujade->get_project($project_id,2);
    if($project['error']!="0")
    {
        $vujade->errors[]="Project Number is invalid.";
    }

    $e = $vujade->get_error_count();
    if($e>0)
    {
        $action=0;
    }
}

# sort by labor type and date range
if($action==3)
{
    $begin = $_POST['begin']; 
    $end = $_POST['end'];
    $labor_type = $_POST['labor_type'];

    # check empty
    $vujade->check_empty($begin,'Begin Date');
    $vujade->check_empty($end,'Begin End');
    $vujade->check_empty($labor_type,'Labor Type');

    $e = $vujade->get_error_count();
    if($e>0)
    {
        $action=0;
    }
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=9;
$title = "Labor Sort Report - ";
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

  <!-- Begin: Content -->
  <section id="content" class="table-layout animated fadeIn">

    <!-- begin: .tray-left -->
    <aside class="tray /*tray-left*/ tray250 p30" id = "left_tray">

	    <div id = "menu_2" style = "">
	    	<a class = "glyphicons glyphicons-left_arrow" href = "accounting.php" id = "back" style = "margin-bottom:10px;"></a>
			<br>
			
			<a href = "print_time_cards.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Print Time Cards</a>
	      	<br>

	      	<a href = "enter_time.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Enter Time</a>
	      	<br>

	      	<a href = "payroll_summary.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Payroll Summary</a>
	      	<br>

	      	<a href = "payroll_hourly_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Hourly Report</a>
	      	<br>

	      	<a href = "payroll_labor_sort_report.php" class = "btn btn-lg btn-primary" style = "width:200px;margin-bottom:15px;">Labor Report</a>
	      	<br>
		</div>

    </aside>
    <!-- end: .tray-left -->

    <!-- begin: .tray-center -->
    <div class="tray tray-center">

        <div class="pl20 pr50">

        <div class="admin-form theme-primary">
        	<?php
			$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				
	        	<div class="panel-body bg-light">

	        		<?php 
if($action==0)
{
    $vujade->show_errors();
    ?>
    <table>

        <tr>
            <td style = "border:1px solid #4A89DC;padding:10px;width:200px;height:200px;">
                <form method = "post" action "payroll_labor_sort_report.php">
                Begin Date:
                <br>    
                <input type = "text" name = "begin" id = "" class = "dp form-control">

                <br>
                End Date: 
                <br>
                <input type = "text" name = "end" id = "" class = "dp form-control">

                <br>
                Employee: 
                <br>
                <select name = "employee_id" class = "form-control">
                    <?php
                    $employees = $vujade->get_employees(1);
                    if($employees['error']=="0")
                    {
                        unset($employees['error']);
                        foreach($employees as $employee)
                        {
                            if($employee['is_admin']!=1)
                            {
                                print '<option value = "'.$employee['database_id'].'">'.$employee['fullname'].'</option>';
                            }
                        }
                    }
                    ?>
                </select>
                <br>
                <br>
                <input type = "hidden" name = "action" value = "1">
                <center>
                <input type = "submit" value = "Sort By Employee" class = "btn btn-primary">
                </center>
                </form>
            </td>

            <td style = "width:20px;height:200px;">&nbsp;</td>
            
            <td style = "border:1px solid #4A89DC;padding:10px;width:200px;height:200px;">
                <form method = "post" action "payroll_labor_sort_report.php">
                Begin Date: <br><input type = "text" name = "begin" id = "" class = "dp form-control"><br>
                End Date: <br><input type = "text" name = "end" id = "" class = "dp form-control"><br>
                Job Number: <br><input class = "form-control" type = "text" name = "project_id" id = "">
                <br><br>
                <input type = "hidden" name = "action" value = "2">
                <center>
                <input type = "submit" value = "Sort By Job" class = "btn btn-primary">
                </center>
                </form>
            </td>
            
            <td style = "width:20px;height:200px;">&nbsp;</td>
            
            <td style = "border:1px solid #4A89DC;padding:10px;width:200px;height:200px;">
                <form method = "post" action "payroll_labor_sort_report.php">
                Begin Date: <br><input type = "text" name = "begin" id = "" class = "dp form-control"><br>
                End Date: <br><input type = "text" name = "end" id = "" class = "dp form-control"><br>
                Labor Type: <br>
                <select name = "labor_type" class = "form-control">
                    <?php
                    $types = $vujade->get_labor_types();
                    unset($types['error']);
                    foreach($types as $type)
                    {
                        print '<option value = "'.$type['database_id'].'">'.$type['type'].'</option>';
                    }
                    ?>
                </select>
                <br><br>
                <input type = "hidden" name = "action" value = "3">
                <center>
                <input type = "submit" value = "Sort By Department" class = "btn btn-primary">
                </center>
                </form>
            </td>
        </tr>

    </table>

<?php } ?>

<?php
# output: get all hours worked for the selected dates, for the employee, order by the project
if($action==1)
{

    # person info
    $person = $vujade->get_employee($employee_id,1);
    $rate_data = $vujade->get_employee_current_rate($employee_id);

    # date/day info
	$startts = strtotime($begin);
    $endts = strtotime($end);
    $endlink = $end;
    $days = $vujade->get_dates_in_range($startts,$endts);

    # project ids
    $project_ids = array();

    # 1: this gets all projects where the employee had time worked in the selected date range; ordered by date
    $sql = "SELECT * FROM `timecards` WHERE `date_ts` >= '$startts' AND `date_ts` <= '$endts' AND `employee_id` = '$employee_id' ORDER BY `date_ts`"; 
    
    //print '<p>Query: '.$sql.'</p>';
    
    $mysqli = $vujade->mysqli;
    $result = $mysqli->query($sql);
    if( (!$result) || ($result->num_rows<1) )
    {
        $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
        //print '<p>Error: '.$error.'</p>';
    }
    else
    {
        while($rows = $result->fetch_assoc())
        {
            $project_ids[] = $rows['project_id'];
        }
    }
    $project_ids=array_unique($project_ids);
    $c1 = count($project_ids);
    if($c1>0)
    {

        $total_st=0;
        $total_ot=0;
        $total_dt=0;

        $total_vacation=0;
        $total_holiday=0;

        $total_paid;

        # heading row
        ?>

        <center><span style = "font-weight:bold;font-size:16px;">Labor Sort <?php print $begin.' to '.$end; ?></span></center>
        <table class = "table">
            <tr class = "header_graybg">
                <td>Job No</td>
                <td>Employee</td>
                <td>S.T.</td>
                <td>Rate</td>
                <td>O.T.</td>
                <td>Rate</td>
                <td>D.T.</td>
                <td>Rate</td>
                <td>Vac</td>
                <td>Hol</td>
                <td>Total Paid</td>
            </tr>

        <?php
        # get all time worked for this project; order by day
        foreach($project_ids as $pid)
        {

            # project id row heading;
            # all work types should now have a project number, even office time
            ?>
                <tr>
                    <td colspan = "11"><strong><?php print $pid; ?></strong></td>
                </tr>
            <?php

            # loop through each day in the range and get hours worked for the project
            foreach($days as $day)
            {
                
                $daytotal=0;
                $day_total = 0; // deprecated
                $day_total_paid; 

                $day_vacation=0;
                $day_holiday=0;

                $ts = strtotime($day);
                $sql = "SELECT * FROM `timecards` WHERE `date_ts` = '$ts' AND `employee_id` = '$employee_id' AND `project_id` = '$pid' ORDER BY `date_ts`"; 

                $line_total = 0;

                $result = $mysqli->query($sql);
                if( (!$result) || ($result->num_rows<1) )
                {
                    $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
                }
                else
                {
                    while($rows = $result->fetch_assoc())
                    {
                        $date = $rows['date'];
                        $date_ts = $rows['date_ts'];
                        $type = $rows['type'];
                        $project_id = $rows['project_id'];
                        $start = $rows['start'];
                        $end = $rows['end'];
                        $standard_time = $rows['standard_time'];
                        $over_time = $rows['over_time'];
                        $double_time = $rows['double_time'];

                        $line_st=0;
                        $line_ot=0;
                        $line_dt=0;
                        $line_vacation=0;
                        $line_holiday=0;

                        # hours were picked from the drop down
                        if(!empty($start))
                        {
                            # hours worked for this entry
                            $begints = strtotime($date.' '.$start);
                            $endts = strtotime($date.' '.$end);
                            $diff = $endts-$begints;
                            $diff = $diff / 60;
                            $diff = $diff / 60; // total time worked for this row

                            $line_total=$diff;
                            $daytotal+=$line_total;

                            if($daytotal<=8)
                            {
                                # hours worked for this entry (not the same as the date because dates can have multiple entries)
                                $st = $diff;
                                $st_total+=$diff;
                                $ot = 0;
                                $dt = 0;
                            }
                            if( ($daytotal > 8) && ($daytotal <= 12) )
                            {
                                $st = 0;
                                $ot = $daytotal-8;
                                $st = $diff-$ot;
                                $st_total+=$st;
                                $ot_total+=$ot;
                                $dt = 0;
                            }

                            if($daytotal > 12)
                            {
                                $ot = 0;
                                $st = 0;
                                $dt = $daytotal-12;
                                $dt_total+=$dt;
                                $ot = $diff-$dt;
                                $ot_total+=$ot;
                            }
                        }
                        else
                        {
                            # hours were typed in manually
                            $st = $standard_time;
                            $daytotal+=$standard_time;
                            $daytotal+=$over_time;
                            $ot = $over_time;
                            $daytotal+=$double_time;
                            $dt = $double_time;
                            if($type=="Holiday")
                            {
                                $day_holiday+=$standard_time;
                            }
                            if($type=="Vacation")
                            {
                                $day_vacation+=$standard_time;
                            } 
                        }

                        # row html output
                        print '<tr>';

                        # blank cells
                        print '<td>';
                        print '</td>';

                        # name and date
                        print '<td>';
                        print $person['fullname'];
                        print '</td>';

                        # standard hour running total
                        print '<td>';
                        print @number_format($st,2,'.',',');
                        print '</td>';

                        # standard rate
                        print '<td>';
                        print @number_format($rate_data['rate'],2,'.',',');
                        print '</td>';

                        # overtime
                        print '<td>';
                        print @number_format($ot,2,'.',',');
                        print '</td>';

                        # overtime rate
                        print '<td>';
                        $otrate = $rate_data['rate']*1.5;
                        print @number_format($otrate,2,'.',',');
                        print '</td>';

                        # double time
                        print '<td>';
                        print @number_format($dt,2,'.',',');
                        print '</td>';

                        # double time rate
                        print '<td>';
                        $dtrate = $rate_data['rate']*2;
                        print @number_format($dtrate,2,'.',',');
                        print '</td>';

                        # vacation
                        print '<td>';
                        print @number_format($day_vacation,2,'.',',');
                        print '</td>';

                        # holiday
                        print '<td>';
                        print @number_format($day_holiday,2,'.',',');
                        print '</td>';

                        # total paid
                        $st_pay = $rate_data['rate'] * $st;
                        $ot_pay = $otrate * $ot;
                        $dt_pay = $dtrate * $dt;
                        $h_pay = $rate_data['rate'] * $day_holiday;
                        $v_pay = $rate_data['rate'] * $day_vacation;
                        $day_total_paid=($st_pay+$ot_pay+$dt_pay+$h_pay+$v_pay);
                        print '<td>';
                        print @number_format($day_total_paid,2,'.',',');
                        print '</td>';
                        print '</tr>';

                        $total_st+=$st;
                        $total_ot+=$ot;
                        $total_dt+=$dt;

                        $total_vacation+=$day_vacation;
                        $total_holiday+=$day_holiday;

                        $total_paid+=$day_total_paid;
                    }
                }
            }
        }

        # summary rows
        print '<tr>';
        print '<td colspan = "11"><hr>';
        print '</td>';
        print '</tr>';
        print '<tr>';
        print '<td>';
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print @number_format($total_st,2,'.',',');
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print @number_format($total_ot,2,'.',',');
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print @number_format($total_dt,2,'.',',');
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print @number_format($total_vacation,2,'.',',');
        print '</td>';
        print '<td>';
        print @number_format($total_holiday,2,'.',',');
        print '</td>';
        print '<td>';
        print @number_format($total_paid,2,'.',',');
        print '</td>';
        print '</tr>';
        print '</table>';
    }

    // print and back buttons
    print '<div class="button-container-style" style = "margin-top:15px;"><center><a class="btn btn-primary" target="_blank" href="pdf.php?page=rpt1&begin='.$begin.'&end='.$endlink.'&employee_id='.$employee_id.'">Print</a> <a href = "payroll_labor_sort_report.php" class = "btn btn-primary">Back</a></center></div>';
}

# type 2
# show all hours for all employees in the date range for the selected project
if($action==2)
{
    # date/day info
    $startts = strtotime($begin);
    $endts = strtotime($end);
    $days = $vujade->get_dates_in_range($startts,$endts);
    $end_date = $end;
    $mysqli = $vujade->mysqli;
    
    $total_st=0;
    $total_ot=0;
    $total_dt=0;

    $total_vacation=0;
    $total_holiday=0;

    $total_paid;

    # heading row
    ?>

    <center><span style = "font-weight:bold;font-size:16px;">Labor Sort <?php print $begin.' to '.$end; ?></span></center>
    <table class = "table">
        <tr class = "header_graybg">
            <td>Job No</td>
            <td>Employee</td>
            <td>S.T.</td>
            <td>Rate</td>
            <td>O.T.</td>
            <td>Rate</td>
            <td>D.T.</td>
            <td>Rate</td>
            <td>Vac</td>
            <td>Hol</td>
            <td>Total Paid</td>
        </tr>
        <tr>
            <td colspan = "11"><strong><?php print $project_id; ?></strong></td>
        </tr>
    <?php
    $employee_ids = array();
    $sql = "SELECT * FROM `timecards` WHERE `date_ts` >= '$startts' AND `date_ts` <= '$endts' AND `project_id` = '$project_id' ORDER BY `date_ts`";    
    $result = $mysqli->query($sql);
    if( (!$result) || ($result->num_rows<1) )
    {
        $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
        //print '<p>Error: '.$error.'</p>';
    }
    else
    {
        while($rows = $result->fetch_assoc())
        {
            $employee_ids[]=$rows['employee_id'];
        }
        $employee_ids=array_unique($employee_ids);
        $c1 = count($employee_ids);
        if($c1>0)
        {
            foreach($employee_ids as $employee_id)
            {
                $person = $vujade->get_employee($employee_id,1);
                $rate_data = $vujade->get_employee_current_rate($employee_id);

                # loop through each day in the range and get hours worked for the project
                foreach($days as $day)
                {
                    $day_total = 0;
                    $day_total_paid; 

                    $day_vacation=0;
                    $day_holiday=0;

                    $day_st=0;
                    $day_ot=0;
                    $day_dt=0;

                    $ts = strtotime($day);
                    $sql = "SELECT * FROM `timecards` WHERE `date_ts` = '$ts' AND `employee_id` = '$employee_id' AND `project_id` = '$project_id' ORDER BY `date_ts`"; 

                    //print '<p>Query: '.$sql.'</p>';
                        
                    $result = $mysqli->query($sql);
                    if( (!$result) || ($result->num_rows<1) )
                    {
                        $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
                        //print '<p>Error: '.$error.'</p>';
                    }
                    else
                    {
                        while($rows = $result->fetch_assoc())
                        {
                            $date = $rows['date'];
                            $date_ts = $rows['date_ts'];
                            $type = $rows['type'];
                            $project_id = $rows['project_id'];
                            $start = $rows['start'];
                            $end = $rows['end'];
                            $standard_time = $rows['standard_time'];
                            $over_time = $rows['over_time'];
                            $double_time = $rows['double_time'];

                            $skip = array('Holiday','Vacation');
                            if(in_array($type, $skip))
                            {
                                if($type=="Holiday")
                                {
                                    $day_holiday+=$standard_time;
                                }
                                if($type=="Vacation")
                                {
                                    $day_vacation+=$standard_time;
                                } 
                            }
                            else
                            {
                                $day_st+=$standard_time;
                            }
                            
                            $day_ot+=$over_time;
                            $day_dt+=$double_time;

                            # 
                            $day_total+=$standard_time;
                            $day_total+=$over_time;
                            $day_total+=$double_time;
                            
                        }

                        # row html output
                        print '<tr>';

                        # blank cells
                        print '<td>';
                        print '</td>';

                        # name and date
                        print '<td>';
                        print $person['fullname'];
                        print '</td>';

                        # standard hour running total
                        print '<td>';
                        print number_format($day_st,2,'.',',');
                        print '</td>';

                        # standard rate
                        print '<td>';
                        print number_format($rate_data['rate'],2,'.',',');
                        print '</td>';

                        # overtime
                        print '<td>';
                        print number_format($day_ot,2,'.',',');
                        print '</td>';

                        # overtime rate
                        print '<td>';
                        $otrate = $rate_data['rate']*1.5;
                        print number_format($otrate,2,'.',',');
                        print '</td>';

                        # double time
                        print '<td>';
                        print number_format($day_dt,2,'.',',');
                        print '</td>';

                        # double time rate
                        print '<td>';
                        $dtrate = $rate_data['rate']*2;
                        print number_format($dtrate,2,'.',',');
                        print '</td>';

                        # vacation
                        print '<td>';
                        print number_format($day_vacation,2,'.',',');
                        print '</td>';

                        # holiday
                        print '<td>';
                        print number_format($day_holiday,2,'.',',');
                        print '</td>';

                        # total paid
                        $st_pay = $rate_data['rate'] * $day_st;
                        $ot_pay = $otrate * $day_ot;
                        $dt_pay = $dtrate * $day_dt;
                        $h_pay = $rate_data['rate'] * $day_holiday;
                        $v_pay = $rate_data['rate'] * $day_vacation;
                        $day_total_paid=($st_pay+$ot_pay+$dt_pay+$h_pay+$v_pay);
                        print '<td>';
                        print number_format($day_total_paid,2,'.',',');
                        print '</td>';
                        print '</tr>';

                        $total_st+=$day_st;
                        $total_ot+=$day_ot;
                        $total_dt+=$day_dt;

                        $total_vacation+=$day_vacation;
                        $total_holiday+=$day_holiday;

                        $total_paid+=$day_total_paid;
                    }
                }
            }

            # summary rows
            print '<tr>';
            print '<td colspan = "11"><hr>';
            print '</td>';
            print '</tr>';
            print '<tr>';
            print '<td>';
            print '</td>';
            print '<td>';
            print '</td>';
            print '<td>';
            print number_format($total_st,2,'.',',');
            print '</td>';
            print '<td>';
            print '</td>';
            print '<td>';
            print number_format($total_ot,2,'.',',');
            print '</td>';
            print '<td>';
            print '</td>';
            print '<td>';
            print number_format($total_dt,2,'.',',');
            print '</td>';
            print '<td>';
            print '</td>';
            print '<td>';
            print number_format($total_vacation,2,'.',',');
            print '</td>';
            print '<td>';
            print number_format($total_holiday,2,'.',',');
            print '</td>';
            print '<td>';
            print number_format($total_paid,2,'.',',');
            print '</td>';
            print '</tr>';
            print '</table>';
        }

        // print and back buttons
        print '<div class="button-container-style" style = "margin-top:15px;"><center><a class="btn btn-primary" target="_blank" href="pdf.php?page=rpt2&begin='.$begin.'&end='.$end_date.'&project_id='.$project_id.'">Print</a> <a href = "payroll_labor_sort_report.php" class = "btn btn-primary">Back</a></center></div>';
    }
}

# type 3
# show all hours worked in the timeframe for each employee who had the selected time type
# hours are to be grouped by project
if($action==3)
{
    # labor type data
    $labor_info = $vujade->get_labor_type_for_timecard($labor_type);
    
    //$step1_end = microtime(true);
	//$step1 = $step1_end - $time_start;

	//print "Time elapsed 1: ".$step1.'<hr>';

    # date/day info
    $startts = strtotime($begin);
    $endts = strtotime($end);
    $days = $vujade->get_dates_in_range($startts,$endts);
    $end_date = $end;
    $mysqli = $vujade->mysqli;
    
    $total_st=0;
    $total_ot=0;
    $total_dt=0;

    $total_vacation=0;
    $total_holiday=0;

    $total_paid;

    //$step2_end = microtime(true);
	//$step2 = $step2_end - $time_start;
	//print "Time elapsed 2: ".$step2.'<hr>';

    # heading row
    ?>

    <center><span style = "font-weight:bold;font-size:16px;">Labor Sort <?php print $begin.' to '.$end.' - '.$labor_info['type']; ?></span></center>
    <table class = "table">
        <tr class = "header_graybg">
            <td>Job No</td>
            <td>Employee</td>
            <td>S.T.</td>
            <td>Rate</td>
            <td>O.T.</td>
            <td>Rate</td>
            <td>D.T.</td>
            <td>Rate</td>
            <td>Vac</td>
            <td>Hol</td>
            <td>Total Paid</td>
        </tr>
    <?php
    # get all project ids in the time range
    $employee_ids = array();
    $project_ids = array();
    $sql = "SELECT * FROM `timecards` WHERE `date_ts` >= '$startts' AND `date_ts` <= '$endts' AND `type` = '$labor_type' ORDER BY `project_id`,`date_ts`";
    //print $sql;    
    //print '<hr>';
    //die;
    $result = $mysqli->query($sql);
    if( (!$result) || ($result->num_rows<1) )
    {
        $error="MYSQLI Error: (" . $mysqli->errno . ") " . $mysqli->error;
        //print '<p>Error: '.$error.'</p>';
    }
    else
    {
    	//$step3_end = microtime(true);
		//$step3 = $step3_end - $time_start;
		//print "Time elapsed 3: ".$step3.'<hr>';
		$table_data = array();
        while($rows = $result->fetch_assoc())
        {
        	$table_rows = array();
            $eid=$rows['employee_id'];
            $rate_data = $vujade->get_employee_current_rate($eid);
            $person = $vujade->get_employee($eid,1);

            $date = $rows['date'];
            $date_ts = $rows['date_ts'];
            $type = $rows['type'];
            $project_id = $rows['project_id'];
            $start = $rows['start'];
            $end = $rows['end'];
            $standard_time = $rows['standard_time'];
            $over_time = $rows['over_time'];
            $double_time = $rows['double_time'];

            $skip = array('Holiday','Vacation');
            if(in_array($type, $skip))
            {
                if($type=="Holiday")
                {
                    $day_holiday+=$standard_time;
                }
                if($type=="Vacation")
                {
                    $day_vacation+=$standard_time;
                } 
            }
            else
            {
                $day_st+=$standard_time;
            }
            
            $day_ot+=$over_time;
            $day_dt+=$double_time;

            # 
            $day_total+=$standard_time;
            $day_total+=$over_time;
            $day_total+=$double_time;

            /*
            # row html output
            print '<tr>';

            # blank cells
            print '<td>';
            //print $sql;
            print $rows['project_id'].'<br>';
            print $rows['date'];
            print '</td>';

            # name and date
            print '<td>';
            print $person['fullname'];
            print '</td>';

            # standard hour running total
            print '<td>';
            print number_format($day_st,2,'.',',');
            print '</td>';

            # standard rate
            print '<td>';
            print number_format($rate_data['rate'],2,'.',',');
            print '</td>';

            # overtime
            print '<td>';
            print number_format($day_ot,2,'.',',');
            print '</td>';

            # overtime rate
            print '<td>';
            $otrate = $rate_data['rate']*1.5;
            print number_format($otrate,2,'.',',');
            print '</td>';

            # double time
            print '<td>';
            print number_format($day_dt,2,'.',',');
            print '</td>';

            # double time rate
            print '<td>';
            $dtrate = $rate_data['rate']*2;
            print number_format($dtrate,2,'.',',');
            print '</td>';

            # vacation
            print '<td>';
            print number_format($day_vacation,2,'.',',');
            print '</td>';

            # holiday
            print '<td>';
            print number_format($day_holiday,2,'.',',');
            print '</td>';
			*/

            $table_rows['project_id']=$rows['project_id'];
            $table_rows['date']=$rows['date'];
            $table_rows['fullname']=$person['fullname'];
            $table_rows['st']=$day_st;
            $table_rows['st_rate']=$rate_data['rate'];
            $table_rows['st_pay']=$rate_data['rate'] * $day_st;
            $table_rows['ot']=$day_ot;
            $otrate = $rate_data['rate']*1.5;
            $table_rows['ot_rate']=$otrate;
            $table_rows['ot_pay']=$ot_pay = $otrate * $day_ot;
            $table_rows['dt']=$day_dt;
            $dtrate = $rate_data['rate']*2;
            $table_rows['dt_rate']=$dtrate;
            $table_rows['dt_pay']=$dt_pay = $dtrate * $day_dt;
            $table_rows['vacation']=$day_vacation;
            $v_pay = $rate_data['rate'] * $day_vacation;
            $table_rows['vacation_pay']=$v_pay;
            $h_pay = $rate_data['rate'] * $day_holiday;
            $table_rows['holiday']=$day_holiday;
            $table_rows['holiday_pay']=$h_pay;

            # total paid
            $st_pay = $rate_data['rate'] * $day_st;
            $ot_pay = $otrate * $day_ot;
            $dt_pay = $dtrate * $day_dt;
            $h_pay = $rate_data['rate'] * $day_holiday;
            $v_pay = $rate_data['rate'] * $day_vacation;
            $day_total_paid=($st_pay+$ot_pay+$dt_pay+$h_pay+$v_pay);
            $table_rows['day_total_paid']=$day_total_paid;
            $table_data[]=$table_rows;
            /*
            print '<td>';
            print number_format($day_total_paid,2,'.',',');
            print '</td>';
            print '</tr>';
			*/

            $total_st+=$day_st;
            $total_ot+=$day_ot;
            $total_dt+=$day_dt;

            $total_vacation+=$day_vacation;
            $total_holiday+=$day_holiday;

            $total_paid+=$day_total_paid; 
            $day_st=0;
            $day_ot=0;
            $day_dt=0;
            $day_vacation=0;
            $day_holiday=0;

            $day_total_paid=0;
            $day_total=0;
        }

       	$project_ids = array();
        foreach($table_data as $arr)
        {
        	$project_ids[]=$arr['project_id'];
        }

        $project_ids=array_unique($project_ids);
        //print_r($project_ids);
        foreach($project_ids as $project_id)
        {
            # row html output
            print '<tr>';

            # blank cells
            print '<td colspan = "11">';
            //print $sql;
            print $project_id.'<br>';
            print $rows['date'];
            print '</td>';
            print '</tr>';

            foreach($table_data as $arr)
            {
            	if($project_id==$arr['project_id'])
            	{
            		print '<tr>';
            		print '<td>&nbsp;';
            		print '</td>';

            		# name and date
		            print '<td>';
		            print $arr['fullname'];
		            print '</td>';

		            # standard hour running total
		            print '<td>';
		            print number_format($arr['st'],2,'.',',');
		            print '</td>';

		            # standard rate
		            print '<td>';
		            print number_format($arr['st_rate'],2,'.',',');
		            print '</td>';

		            # overtime
		            print '<td>';
		            print number_format($arr['ot'],2,'.',',');
		            print '</td>';

		            # overtime rate
		            print '<td>';
		            print number_format($arr['ot_rate'],2,'.',',');
		            print '</td>';

		            # double time
		            print '<td>';
		            print number_format($arr['dt'],2,'.',',');
		            print '</td>';

		            # double time rate
		            print '<td>';
		            print number_format($arr['dt_rate'],2,'.',',');
		            print '</td>';

		            # vacation
		            print '<td>';
		            print number_format($arr['vacation'],2,'.',',');
		            print '</td>';

		            # holiday
		            print '<td>';
		            print number_format($arr['holiday'],2,'.',',');
		            print '</td>';

		            # day paid
		            print '<td>';
		            print number_format($arr['day_total_paid'],2,'.',',');
		            print '</td>';

            		print '</tr>';
            	}
            }
        }
        //print '<hr>';

        # summary rows
        print '<tr>';
        print '<td colspan = "11"><hr>';
        print '</td>';
        print '</tr>';
        print '<tr>';
        print '<td>';
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print number_format($total_st,2,'.',',');
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print number_format($total_ot,2,'.',',');
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print number_format($total_dt,2,'.',',');
        print '</td>';
        print '<td>';
        print '</td>';
        print '<td>';
        print number_format($total_vacation,2,'.',',');
        print '</td>';
        print '<td>';
        print number_format($total_holiday,2,'.',',');
        print '</td>';
        print '<td>';
        print number_format($total_paid,2,'.',',');
        print '</td>';
        print '</tr>';
        print '</table>';
        // print and back buttons
        print '<div class="button-container-style" style = "margin-top:15px;"><center><a class="btn btn-primary" target="_blank" href="pdf.php?page=rpt3&begin='.$begin.'&end='.$end_date.'&labor_type='.$labor_type.'">Print</a> <a href = "payroll_labor_sort_report.php" class = "btn btn-primary">Back</a></center></div>';
    }
}
?>

</div>
</div>
</div>
</div>
</section>
</section>
</div>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/jquery.timepicker.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

    // datepickers
    $(".dp").datepicker();

});
</script>

</body>
</html>