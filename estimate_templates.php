<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2',1);

// permissions
$estimates_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimates');
if($estimates_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

// full access permissions
$estimates_fa_permissions = $vujade->get_permission($_SESSION['user_id'],'Estimate Full Access');

$estimates = $vujade->get_estimates(3);
$templates=$estimates;

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=7;
$title = "Estimate Templates - ";
//$charset="iso-8859-1";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">
      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#">Estimate Templates</a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">
        <div class="admin-form theme-primary">
        	<?php
        	if($_REQUEST['m']==1)
        	{
        		$vujade->messages[]="Template created.";
        	}
        	if($_REQUEST['m']==2)
        	{
        		$vujade->messages[]="Template updated.";
        	}
        	if($_REQUEST['m']==3)
        	{
        		$vujade->messages[]="Template deleted.";
        	}
        	$vujade->show_messages();
			$vujade->show_errors();
			?>
			<div class="panel heading-border panel-primary">
				<div class="panel-heading">
					<div class="widget-menu pull-right">
					<?php
					// create button / transfer button
					if($estimates_fa_permissions['read']==1)
					{
					?>
						<a href = "new_estimate_template.php" class = "btn btn-primary btn-sm" style = "margin-right:5px;">New</a> 
						<!--<a class = "btn btn-primary btn-sm" id = "transfer" href = "#transfer-form" style = "float:right;margin-top:5px;" title = "Transfer from other">Transfer</a>-->

						<!-- transfer modal -->
						<div id = "transfer-form" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "width:400px;">
							<input type = "hidden" name ="transfer_to" id = "transfer_to" value = "NEW">
							<form id = "transfer_form" class="form-inline" style = "margin-bottom:15px;">
								<table>

									<tr>
										<td><label>Template Name</label></td>
										<td><input type = "text" name = "name" id = "name" class = "form-control input-sm" style = "width:230px;margin-left:5px;"></td>
									</tr>

									<tr>
										<td></td>
										<td></td>
									</tr>

									<tr>
										<td><label>Estimate Number</label></td>
										<td><input type = "text" name = "estimate" id = "estimate" class = "form-control input-sm" style = "width:230px;margin-left:5px;"></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td><label>OR</label></td>
										<td></td>
									</tr>
									<tr>
										<td></td>
										<td></td>
									</tr>
									<tr>
										<td><label>Template</label></td>
										<td><select name = "template" id = "template" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
										<option value = "">-Select-</option>
										<?php
										if($templates['count']>0)
										{
											unset($templates['count']);
											unset($templates['error']);
											unset($templates['sql']);
											foreach($templates as $template)
											{
												print '<option value = "'.$template['estimate_id'].'">'.$template['template_name'].'</option>';
											}
										}
										?>
									</select></td>
									</tr>

									<tr>
										<td></td>
										<td></td>
									</tr>

									<tr>
										<td><label>Include Description?</label></td>
										<td>
											<select name = "copy_description" id = "copy_description" class = "form-control input-sm" style = "width:230px;margin-left:5px;">
												<option value = "">-Select-</option>
												<option value = "1">Yes</option>
												<option value = "">No</option>
											</select>
									</td>
									</tr>

								</table>

								<div id = "working" style = "display:none;" class = "alert alert-warning"></div>
								
								<div id = "error_1" style = "display:none;" class = "alert alert-danger">Please enter an estimate number.</div>
								
								<div id = "error_3" style = "display:none;" class = "alert alert-danger">Please enter an estimate number or select a template.</div>

								<div id = "error_2" style = "display:none;" class = "alert alert-danger">This estimate number is invalid.</div>

								<div class = "" style = "width:100%;margin-top:15px;">
									<a id = "transfer_btn" href = "#" class = "btn btn-lg btn-primary" style = "margin-right:15px;">Go</a><a class="popup-modal-dismiss btn btn-lg btn-danger" href="#">Cancel</a>
								</div>
							</form>
						</div>

					<?php } ?>
					</div>
				</div>
	        	<div class="panel-body bg-light">
					<?php 
					if($estimates['count']>0)
					{
						unset($estimates['error']);
						unset($estimates['count']);
						unset($estimates['sql']);
						?>
						<table id="datatable" class="table table-striped table-hover" cellspacing="0" width="100%">
							<thead>
								<tr style = "border-bottom:1px solid black;">
									<td valign = "top"><strong>ID</strong></td>
									<td valign = "top"><strong>Name</strong></td>
								</tr>
							</thead>

						    <tbody style = "font-size:14px;">
							<?php
							foreach($estimates as $i)
							{
								$link = 'estimate_template_overview.php?estimateid='.$i['database_id'];
						        print '<tr class = "clickableRow-row">';

						        print '<td valign = "top" style = "" class="clickableRow" href="'.$link.'"><a href = "'.$link.'">'.$i['database_id'].'</a></td>';

						        print '<td valign = "top" class="clickableRow" href="'.$link.'" style = "">'.$i['template_name'].'</td>';

								print '</tr>';
							}

							?>
							</tbody>
						</table>
					<?php
					}
					else
					{
						$vujade->set_error('Could not fetch estimate templates from database.');
						//$vujade->set_error('Mysql error: '.$estimates['error']);
						//$vujade->set_error('Count: '.$estimates['count']);
						$vujade->show_errors();
					}
					?>
				</div>
			</div>
		</div>
		</section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Datatables --> 
<script src="vendor/plugins/datatables/media/js/jquery.dataTables.js"></script>

<!-- Datatables Bootstrap Modifications  -->
<script src="vendor/plugins/datatables/media/js/dataTables.bootstrap.js"></script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">
jQuery(document).ready(function() 
{

	"use strict";

	// Init Theme Core    
    Core.init();

	// Init DataTables
	/* this is broken...
    $('#datatable').dataTable({

      "order": [[ 1, "desc" ]]

      "aoColumnDefs": [{
        'bSortable': false,
        'aTargets': [-1]
      }],
      "oLanguage": {
        "oPaginate": {
          "sPrevious": "",
          "sNext": ""
        }
      },
      "iDisplayLength": 25,
      "aLengthMenu": [
        [25, 50, 75, 100, -1],
        [25, 50, 75, 100, "All"]
      ],
      "sDom": '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
      "oTableTools": {
        "sSwfPath": "vendor/plugins/datatables/extensions/TableTools/swf/copy_csv_xls_pdf.swf"
      }

    });
	*/

    /**/
    $(".clickableRow").click(function() 
    {
        window.document.location = $(this).attr("href");
    }); 

    // modal: transfer estimate
    $('#transfer').magnificPopup({
		type: 'inline',
		preloader: false,
		focus: '#transfer_form input',
		modal: true,		
	});
	
    // transfer button in the modal 
    $('#transfer_btn').click(function(e)
    {
    	e.preventDefault();
		$('#working').hide();
    	$('#error_1').hide();
    	$('#error_2').hide();
    	$('#error_3').hide();

    	var name = $('#name').val();
    	var estimate = $('#estimate').val();
		var template = $('#template').val();
		var copy_description = $('#copy_description').val();

		var error = 0;
		if(name=='')
		{
			error++;
		}
		if(estimate=='')
		{
			error++;
		}
		if(template=='')
		{
			error++;
		}
		if(error>=3)
		{
			$('#error_3').show();
			return false
		}
		else
		{
			$('#error_3').hide();
			$('#working').show();
			$('#working').html('Working...');
			$.post( "jq.transfer_estimate_template.php", { estimate: estimate, template: template, copy_description: copy_description, name: name })
			.done(function( response ) 
			{
				$('#working').show();
			    $('#working').html('');
			    $('#working').html(response);
			    if(response=="Success")
			    {
			    	$('#working').html('Success! This page will reload in 3 seconds...');
			    	setTimeout(function(){ window.location.reload(); }, 3000);
			    }
			    else
			    {
			    	$('#error_2').html(response);
			    	$('#error_2').show();
					$('#working').hide();
			    }
			});
		}
    	
	});

    // press return on estimate transfer form
    $('#transfer_form').on('keypress', function(e) 
    {
		if (e.which == 13) 
		{
			//$('#form').submit();
			$('#transfer_btn').click();
			return false;
		}
	});

	// modal dismiss
    $(document).on('click', '.popup-modal-dismiss', function (e) {
		e.preventDefault();
		$.magnificPopup.close();
	});

});
</script>

</body>
</html>