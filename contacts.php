<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];
$project = $vujade->get_project($id,2);

if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

// delete a contact
if($action==4)
{
	$contact_id=$_REQUEST['contact_id'];
	$s = $vujade->delete_row('sales_contacts',$contact_id);
	$tab=2;
}

// show the status button error
$show_status_error=0;

if(!preg_match('/Pending/',$project['status']))
{
	$show_status_error=1;
}
else
{
	$show_status_error=0;
}

$shop_order = $vujade->get_shop_order($id, 'project_id');

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$setup = $vujade->get_setup(1);

$section=3;
$menu=16;
$title = 'Contacts - ' . $project['project_id'].' - '.$project['site'].' - ';
require_once('tray_header.php');
?>

<!-- Start: Content-Wrapper -->
<section id="content_wrapper">

	<!-- Begin: Content -->
	<section id="content" class="table-layout animated fadeIn">

		<!-- begin: .tray-left -->
		<?php require_once('project_left_tray.php'); ?>
		<!-- end: .tray-left -->

		<!-- begin: .tray-center -->
		<div class="tray tray-center" style = "width:100%;">

			<div class="pl15 pr15" style = "width:100%;">

				<?php require_once('project_right_tray.php'); ?>

				<!-- main content for this page -->
				<div style="width: 100%">
					<style>
						.conversation-date.active,
						.contact-name.active{
							background: #e5e5e5;
							text-decoration: none !important;
						}
					</style>

					<!-- contacts -->
					<div class="row">
						<div class="col-md-2" style="width:200px;">
							<div class="panel panel-primary panel-border top">
								<div class="panel-heading">
									<span class="panel-title">Contacts</span>
									<div class="widget-menu pull-right">
										<a href="new_sales_contact.php?project_id=<?php print $id; ?>" class="btn btn-primary btn-sm">New</a>
									</div>
								</div>
								<div class="panel-body">
									<?php
									$contacts = $vujade->get_sales_contacts($id);
									if($contacts['error']=="0")
									{
										$show_contacts=1;
										unset($contacts['error']);
										foreach($contacts as $c)
										{
											print '<a class = "contact-name" href = "contact-'.$c['id'].'" id = "'.$c['id'].'">';
											print $c['name'].'</a><br>';
										}
									}
									?>
								</div>
							</div>
						</div>

						<div id = "contact-list" class="col-md-9" style="margin-left:5px;border:0px solid red;padding:0px;">
							<?php
							if($show_contacts==1)
							{
								foreach($contacts as $c)
								{
									print '<div class="panel-contact panel panel-primary panel-border top" id = "contact-'.$c['id'].'">';

									print '<div class="panel-heading">';

									print '<div style = "float:left;width:200px;color:#2876A5;font-weight:bold;">';
									print $c['title'].' - '.$c['name'];
									print '</div>';

									print '<div class="widget-menu pull-right">';
									?>
										<a href = "edit_sales_contact.php?contact_id=<?php print $c['id']; ?>&project_id=<?php print $id; ?>" class = "btn btn-xs btn-primary">EDIT</a>
										
										<a class = "btn btn-xs btn-danger delete-contact" id="delete_contact" href="#delete_contact_form_<?= $c['id']; ?>" title = "Delete contact">Delete</a>

										<!-- modal for delete contact -->
										<div id="delete_contact_form_<?= $c['id']; ?>" class="delete-contact-form popup-modal popup-basic p25 mfp-zoomIn mfp-hide">
											<h1>Delete Contact</h1>
											<p>Are you sure you want to delete this contact?</p>
											<p>
												<a id = "" class="btn btn-lg btn-danger"
												   href="contacts.php?action=4&contact_id=<?= $c['id']; ?>&id=<?= $id; ?>&tab=3">YES</a>
												<a class="popup-modal-dismiss btn btn-lg btn-primary" href="#">NO</a>
											</p>
										</div>

									<?php
									print '</div>';
									print '</div>';

									print '<div class = "panel-body" id = "contact-box-'.$c['id'].'">';
									print '<div class = "row">';
									print '<div class = "col-md-4">';
									print '<strong>'.$c['label_1'].': </strong>'.$c['phone'].'<br>';
									//print "<strong>".$c['label_l'].": </strong>".$c['phone'].'<br>';
									print "<strong>".$c['label_2'].": </strong>".$c['cell'].'<br>';
									print "<strong>".$c['label_3'].": </strong>".$c['fax'].'<br>';
									print "<strong>".$c['label_4'].": </strong>".$c['other'].'<br>';
									print '</div>';
									
									print '<div class = "col-md-8">';
									print '<div class = "well">';
									print '<address><strong>';
									print $c['address'].'<br>';
									print $c['city'].', '.$c['state'].' '.$c['zip'];
									print '</strong></address>';
									print '</div>';
									print '</div>';

									print '</div>';
									print '</div>';

									print '</div>';
								}
							}
							?>
						</div>

					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End: Content -->

</section>
<!-- End: Main -->

<!-- BEGIN: PAGE SCRIPTS -->

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<script type="text/javascript">
	$(function() {
		// date picker
		$('#date').datepicker();
	});
</script>

<!-- Page Plugins -->
<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>

<script type="text/javascript">
	$(function()
	{
		"use strict";

		// Init Theme Core
		Core.init();

		var HEADER = 60; // header height

		// focus on a contact
		$('.contact-name').click(function(e){
			e.preventDefault();

			var contact = $("#contact-"+this.id);

			if( $(this).hasClass('active') ){
				$(this).removeClass('active');
				contact.removeClass('panel-warning');
			} else {
				$('.contact-name').not( $(this) ).removeClass('active');
				$(this).addClass('active');

				$('.panel-contact').not( contact ).removeClass('panel-warning');
				contact.addClass('panel-warning');

				$('html, body').animate({
					scrollTop: contact.offset().top - HEADER
				}, 500);
			}
		});

		$('.delete-contact').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '.delete-contact-form',
			modal: true
		});

		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	});
</script>
<!-- END: PAGE SCRIPTS -->

</body>

</html>
