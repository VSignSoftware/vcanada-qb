<?php
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['id'];

$project = $vujade->get_project($id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$employee = $vujade->get_employee($_SESSION['user_id']);
if($employee['error']!="0")
{
	$vujade->page_redirect('error.php?m=1');
}

$emp=$employee;

// change color for a project 
if($_REQUEST['action']==1)
{
	$new_color=$_REQUEST['new_color'];
	$project_id=$_REQUEST['project_id'];
	$cal_event = $vujade->get_data_for_one_event($project_id);
	$event_id=$cal_event['id'];
	$s[]=$vujade->update_row('calendar',$event_id,'bottom_border','#'.$new_color);
	unset($cal_event);
	unset($event_id);
}

// date for the legend
$legend = $vujade->get_legend_data();

$charset='utf-8';
$section=5;
$title = "Schedule - ";
$calendar=false;
require_once('h.php');
?>

<section id="content_wrapper">
<!-- Start: Topbar -->
<header id="topbar">
<div class="topbar-left">
<ol class="breadcrumb">
<li class="crumb-active">
<a href = "#" id = "page-title">Schedule</a>
</li>
</ol>
</div>
</header>
<!-- End: Topbar -->

<!-- Begin: Content -->
<section id="content" class="">
<div class="admin-form theme-primary">
<div class="panel heading-border panel-primary">
<div class="panel-heading" style="overflow:visible;">

	<div class="btn-group" style = "z-index:1;">
		<!--
		<button type="button" class="btn btn-primary">Schedule</button>
		<button type="button" class="btn btn-primary dark dropdown-toggle" data-toggle="dropdown">
		<span class="caret"></span>
		<span class="sr-only">Toggle Dropdown</span>
		</button>
		<ul class="dropdown-menu" role="menu" style = "z-index:1;">
		<li>
		<a href="schedule.php">Schedule</a>
		</li>
		<li>
		<a href="calendar.php">Calendar</a>
		</li>
		<li>
		<a href="manufacturing_schedule.php">Old Version of Schedule</a>
		</li>
		</ul>
		-->
		<a class = "btn btn-primary" href="schedule.php">Schedule</a> 
		<a class = "btn btn-primary" href="calendar.php" style = "margin-left:15px;">Calendar</a> 
		<a href = "#help-modal" id = "help" style = "margin-left:15px;" class = "">Color Codes and Help</a>
	</div>

</div>
<div class="panel-body bg-light">

	<!-- legend / help modal -->
	<div id = "help-modal" class = "popup-basic p25 mfp-zoomIn mfp-hide" style = "">
		<?php
		if($legend['error']=="0")
		{
		    unset($legend['error']);
		    foreach($legend as $l)
		    {
		        print '<div class="row" style="padding: 0px;"><div style = "border:1px solid #eeeeee; width:25px;height:15px;background-color:'.$l['hex'].';display:block;float:left">&nbsp;</div><div style = "float:left;margin-left: 2px; margin-right: 0px;font-size: 0.9em;">= '.$l['explanation'].'</div></div>';
		    }

		    print '<div class="row" style="padding: 0px;"><div style = "border:1px solid #eeeeee; width:25px;height:15px;background-color:#D0E4FD;display:block;float:left">&nbsp;</div><div style = "float:left;margin-left: 2px; margin-right: 0px;font-size: 0.9em;">= No color assigned yet</div></div>';

		    print '<div class="row" style="padding: 0px;"><div style = "border:1px solid #000000; width:25px;height:15px;background-color:#000000;display:block;float:left">&nbsp;</div><div style = "float:left;margin-left: 2px; margin-right: 0px;font-size: 0.9em;">= Manufacturing Completed</div></div>';
		}
		?>
		<br>
		<b>&bull; Click the job number in the left column to highlight the row
		<br>&bull; Double click the job number in the left column to change the job box color
		<br>&bull; Drag the job box to change the install date</b>

		<br>
		<br>
		<a class="popup-modal-dismiss btn btn-xs btn-danger" href="#">Close</a>
	</div>

	<!-- gantt iframe -->
	<div id = "gantt" style = "">
		<iframe src="gantt.php" width="100%" height = "2000"></iframe>
	</div>

	<!-- jQuery -->
	<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
	<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

	<!-- magnific popup for the modal -->
  	<script src="vendor/plugins/magnific/jquery.magnific-popup.js"></script>

	<!-- Theme Javascript -->
	<script src="assets/js/utility/utility.js"></script>
	<script src="assets/js/demo/demo.js"></script>
	<script src="assets/js/main.js"></script>
	<script type="text/javascript">
	jQuery(document).ready(function() 
	{

		"use strict";

		// Init Theme Core    
	    Core.init();

	    // help modal
		$('#help').magnificPopup({
			type: 'inline',
			preloader: false,
			focus: '#help-modal',
			modal: true
		});

		// dismiss modals
		$(document).on('click', '.popup-modal-dismiss', function (e) {
			e.preventDefault();
			$.magnificPopup.close();
		});
	   	 
	});
	</script>
</div>
	</div>
	</div>
	</section>

	</section>
</body>
</html>