<?php 
session_start();
define('SITE',1);
require_once('../library/class_library.php');
$vujade = new Vujade();
$vujade->connect();
$vujade->protect_page('login.php?m=2');

# permissions
$projects_permissions = $vujade->get_permission($_SESSION['user_id'],'Projects');
if($projects_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($projects_permissions['create']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
$designs_permissions = $vujade->get_permission($_SESSION['user_id'],'Designs');
if($designs_permissions['read']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}
if($designs_permissions['edit']!=1)
{
	$vujade->page_redirect('error.php?m=1');
}

$id = $_REQUEST['project_id'];
$project_id = $id;

$project = $vujade->get_project($project_id,2);
if($project['error']!=0)
{
	$vujade->page_redirect('error.php?m=3');
}

$designid = $_REQUEST['designid'];
$design = $vujade->get_design($designid);
if($design['error']!="0")
{
	$vujade->page_redirect('error.php?m=3');
}

$design_files = $vujade->get_design_files($design['design_id']);

$action = 0;
if(isset($_REQUEST['action']))
{
	$action = $_REQUEST['action'];
}

# cancel button was pressed
if($action==2)
{
	$project_id=$_POST['project_id'];
	$vujade->page_redirect('project_designs.php?id='.$project_id);
}

$employee = $vujade->get_employee($_SESSION['user_id']);
$emp=$employee;
$section=3;

$title = "Design File Upload - ";
require_once('h.php');
?>

<!-- Start: Content-Wrapper -->
    <section id="content_wrapper">

      <!-- Start: Topbar -->
      <header id="topbar">
        <div class="topbar-left">
          <ol class="breadcrumb">
            <li class="crumb-active">
              <a href = "#"><?php print $project['project_id'].' - '.$project['site']; ?></a>
            </li>
          </ol>
        </div>
      </header>
      <!-- End: Topbar -->

      <!-- Begin: Content -->
      <section id="content" class="">

        <div class="admin-form theme-primary">

            <?php 
            $vujade->show_errors();
            $vujade->show_messages();
            ?>

              <div class="panel heading-border panel-primary">
                <div class="panel-body bg-light">
                  <strong>#<?php print $design['design_id']; ?></strong>
                 
                  <div class = "alert alert-danger" id = "debug" style = "display:none;"></div>

                  <div style = "padding:5px;width:100%;">
					<link rel="stylesheet" href="vendor/plugins/dropzone/css/dropzone.css">	
					<script src="vendor/plugins/dropzone/dropzone.min.js"></script>	

					<div style = "width:100%;margin-bottom:15px;height:375px;">
					<form action="upload.php?project_id=<?php print $id; ?>&type=1&design_id=<?php print $design['design_id']; ?>" id="dropzone-area" class = "dropzone dropzone-sm"></form>
					<!--<div id = "dz-area"></div>-->
					</div>

					</div>

                    <div style = "margin-top:15px;">
                      <div style = "float:left">
                        <a href = "project_designs.php?tab=2&id=<?php print $project_id; ?>&designid=<?php print $designid; ?>" id = "done" class = "btn btn-lg btn-success" style = "width:200px;">SAVE AND COMPLETE</a>
                      </div>
                      <div style = "float:right;">
                        
                      </div>  
                    </div>

                </div>
              </div>

            </div>

	    </section>
	</section>

<!-- jQuery -->
<script src="vendor/jquery/jquery-1.11.1.min.js"></script>
<script src="vendor/jquery/jquery_ui/jquery-ui.min.js"></script>

<!-- Theme Javascript -->
<script src="assets/js/utility/utility.js"></script>
<script src="assets/js/demo/demo.js"></script>
<script src="assets/js/main.js"></script>
<script type="text/javascript">

Dropzone.options.dropzoneArea = { 

  // The configuration we've talked about above
  //autoProcessQueue: false,
  //uploadMultiple: false,
  //parallelUploads: 1,
  //maxFiles: 100,
  url: "upload.php?project_id=<?php print $id; ?>&type=1&design_id=<?php print $design['design_id']; ?>",
  init: function () 
  {
  		this.on("addedfile", function (file) 
  		{
        	var existing = [];
        	// get existing files and add to array
        	<?php
        	if($design_files['error']=="0")
        	{
        		unset($design_files['error']);
        		unset($design_files['sql']);
        		foreach($design_files as $df)
        		{
        			?>
        			existing.push("<?php print $df['file_name']; ?>");
        			<?php
        		}
        	}
        	?>
        	var in_array = existing.indexOf(file.name);
    		if(in_array>=0)
    		{
    			if(confirm("A file with the same name " + file.name + " already exists for this design. Do you want to overwrite it?"))
    			{
    				this.processQueue();
                	//alert('1');
                	// user confirmed; do nothing further; upload.php handles overwriting
	            }
	            else
	            {
	            	this.removeFile(file);
                	return false;
	            }
    		}
    		else
    		{
    			//alert('not in array: '+file.name);
    			this.processQueue();
    		}
        });

    }

}

jQuery(document).ready(function() 
{

    "use strict";

    // Init Theme Core    
    Core.init();
    
});
</script>

</body>

</html>